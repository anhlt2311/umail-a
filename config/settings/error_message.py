DEFAULT_ERROR_MESSAGE = 'サーバーエラーが発生しました。しばらく時間を置いてから再度お試しいただくか、サポートまでお問い合わせください。'

USER_DEACTIVATE_DEFAULT_ERROR_MESSAGE = '下記の関連リソースより、当該ユーザーを削除してから操作してください。'
USER_DEACTIVATE_CONTACT_ERROR_MESSAGE = '企業管理の企業担当者にて、ユーザーが登録されています。'
USER_DEACTIVATE_SCHEDULED_EMAIL_ERROR_MESSAGE = '配信メール管理の配信メール一覧にて、「下書き」「配信待ち」「配信中」ステータスのメールがあります。'
USER_DEACTIVATE_EMAIL_ERROR_MESSAGE = '共有メール管理の共有メール一覧にて、ユーザーが登録されています。'
TAG_REGISTERD_VALUE_ERROR_MESSAGE = '入力したタグ名は、既に登録されています。'
MAX_BYTE_SIZE_ERROR_MESSAGE = '2MBを超えるメールを配信することはできません。'
MAX_BYTE_SIZE_WITH_ADDON_ERROR_MESSAGE = '10MBを超えるメールを配信することはできません。'
TARGET_NOT_SELECTED_ERROR_MESSAGE = '宛先が選択されていません。宛先を選択してください。'
USER_ACTIVATE_LIMIT_ERROR_MESSAGE = 'ユーザー数の上限に達しているため、ユーザーの登録ができません。マスターユーザーにお問い合わせください。'
COMMENT_TEMPLATE_ADDON_REVOKE_ERROR_MESSAGE = '{0}で、{1}件以上のテンプレートが登録されています。'
SEARCH_TEMPLATE_ADDON_REVOKE_ERROR_MESSAGE = '{0}さんが{1}で、{2}件以上のテンプレートを使用しています。'

SCHEDULED_EMAIL_TARGET_COUNT_LIMIT_ERROR_MESSAGE = '配信件数の上限は{0}件です。'
SCHEDULED_EMAIL_RESERVE_ALLOWED_PERIOD_MONTH = 1
ALREADY_INVITED_EMAIL_ERROR_MESSAGE = '招待済みのメールアドレスです。'
TENANT_REGISTER_FINISH_ERROR_MESSAGE = '個人プロフィール情報の入力が完了していません。'

SCHEDULED_EMAIL_ATTACHMENT_DELETE_ERROR_MESSAGE = 'ファイルの削除に失敗しました。詳しくはコンソールログを確認ください。'
SCHEDULED_EMAIL_RESERVE_ERROR_MESSAGE = '必須項目の入力に不備があります。お手数ですが再度ご確認ください。'
SCHEDULED_EMAIL_ATTACHMENT_UPLOAD_ERROR_MESSAGE = '{uploadInfo.file.name} アップロードに失敗しました'
CSV_DOWNLOAD_ERROR_MESSAGE = 'CSVのダウンロードに失敗しました'
TEMPLATE_NAME_EMPTY_ERROR_MESSAGE = 'テンプレート名が空です。テンプレート名を入力してください。'
TEMPLATEALREADY_EXISTS_ERROR_MESSAGE = '{newTemplateName} と同一名称のテンプレートが既に存在します。別のテンプレート名を入力してください。'
ADDON_DELETE_ERROR_MESSAGE = 'アドオンの削除に失敗しました。'
ADDON_PURCHASE_ERROR_MESSAGE = 'アドオンの購入に失敗しました'
ADDON_HISTORY_ERROR_MESSAGE = 'アドオンの購入履歴を取得するのに失敗しました。'
ADDON_LIST_ERROR_MESSAGE = 'アドオンの一覧を取得するのに失敗しました。'
PLAN_INFO_ERROR_MESSAGE = 'お客様のご利用プラン情報を取得することができませんでした。'

USER_ADD_ERROR_MESSAGE = 'ユーザーを追加することができませんでした。'
USER_DELETE_ERROR_MESSAGE = 'ユーザーを削除することができませんでした。'
#_ERROR_MESSAGE = 'プランを選択することができませんでした。'
TENANT_REGISTER_ERROR_MESSAGE = '会員情報の登録に失敗しました。'
TENANT_MY_COMPANY_ERROR_MESSAGE = '自社プロフィール情報の登録に失敗しました。'
TENANT_MY_PROFILE_ERROR_MESSAGE = '個人プロフィール情報の登録に失敗しました。'
PAYMENT_ERROR_MESSAGE = 'お支払い情報の取得に失敗しました。'
PAYMENT_REGISTER_ERROR_MESSAGE = 'お支払い情報の登録に失敗しました。'
PASSWORD_RESET_NOT_MATCH_ERROR_MESSAGE = 'パスワードが一致していません。パスワードを正しく入力してください。'
PASSWORD_RESET_ERROR_MESSAGE = 'パスワードを変更に失敗しました'
CSV_UPLOAD_DATA_ERROR_MESSAGE = '{info.file.name} のファイルがアップロードされましたが、入力エラーによりデータの登録に失敗しました。'
CSV_UPLOAD_ERROR_MESSAGE = '{info.file.name} アップロードに失敗しました'
SCHEDULED_EMAIL_SETTING_CONNECTION_ERROR_MESSAGE = '接続テストに失敗しました。入力情報に誤りがないかご確認ください。'
#_ERROR_MESSAGE = '領収書のPDF出力に失敗しました。しばらく時間を置いてから再度お試しいただくか、サポートまでお問い合わせください。'
TENANT_REGISTER_STEP_ERROR_MESSAGE = 'データの登録に失敗しました。入力情報に誤りがないかご確認ください。'
ACCOUNT_DELETE_ERROR_MESSAGE = '退会に失敗しました。'

PLAN_PURCHASE_ERROR_MESSAGE = 'プランの購入に失敗しました。サポートまでお問い合わせください。'
PLAN_PURCHASE_PAYMENT_ERROR_MESSAGE = '支払い処理に失敗しました。'
PAYJP_CHARGE_ERROR_MESSAGES = {
    'card_declined': '支払い処理に失敗しました。カードのご利用状況についてご確認ください。',
    'expired_card': '支払い処理に失敗しました。カードの有効期限が切れています。',
}
PLAN_PURCHASE_PAYMENT_UNKNOWN_ERROR_MESSAGE = '予期せぬエラーにより、支払い処理に失敗しました。サポートまでお問い合わせください。'

SCHEDULED_EMAIL_RESERVE_ALLOWED_PERIOD_ERROR_MESSAGE = '{0} : {1}ヶ月以上先の予約を行うことはできません。配信時刻を変更してください'
SCHEDULED_EMAIL_EXISTS_BY_TERM_ERROR_MESSAGE = '{0} : 選択した時刻の前後{1}分以内に配信メールが予約されています。配信時刻を変更してください。'
SCHEDULED_EMAIL_NOT_WORKING_TIME_ERROR_MESSAGE = '{0} : 配信可能時刻は、{1} 〜 {2} です。配信時刻を変更してください'
SCHEDULED_EMAIL_HOLIDAY_ERROR_MESSAGE = '{0} : 土日祝に配信メールが予約されています。配信時刻を変更してください。'

CSV_REGISTER_LIMIT_ERROR_MESSAGE = '{0}行を超えるデータを登録することはできません。'

FILE_ATTACHMENT_ERROR_MESSAGE = '添付可能なファイル数は10件以下です。'

MAX_BYTE_SIZE_PERSONNEL_IMAGE_ERROR_MESSAGE = '2MBを超えるイメージをアップロードすることができません。'

MAX_BYTE_SIZE_PERSONNEL_SKILL_SHEET_ERROR_MESSAGE = '2MBを超えるファイルをアップロードすることができません。'

MAX_FILE_PERSONNEL_SKILL_SHEET_ERROR_MESSAGE = '添付可能なファイル数は10件以下です。'
