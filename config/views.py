import json
import hmac
import hashlib

from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.models import update_last_login
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from app_staffing.models import UserDisplaySetting, CompanyAttribute, Plan
from app_staffing.serializers import UserDisplaySettingSerializer
from app_staffing.utils.user_role import is_admin_or_master
from app_staffing.exceptions.user import UserInActivelException

@method_decorator(csrf_exempt, name='dispatch')
class CustomAuthTokenView(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        username = request.data.get('username')
        if username is None:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'username': '必須項目です。'})

        if len(str(username)) > settings.LENGTH_VALIDATIONS['user']['email']['max']: # ログイン時のusernameとはemail
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'username': settings.LENGTH_VALIDATIONS['user']['email']['message']
            })

        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        if user.is_active is False:
            raise UserInActivelException()
        # 所属テナントが退会状態の場合はログイン失敗
        if user.company and user.company.deactivated_time:
            return Response(status=status.HTTP_403_FORBIDDEN)

        # テナント登録が完了していない場合はログイン失敗
        try:
            company_attribute = CompanyAttribute.objects.get(company=user.company)
            if company_attribute and company_attribute.tenant_register_current_step != settings.TENANT_REGISTER_STEP_COMPLETED:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        # 古いトークンはログインのタイミングで削除、新しいトークンに切り替える
        Token.objects.filter(user=user).delete()
        new_token = Token.objects.create(user_id=user.id)
        # Update Login date of the user.
        update_last_login(sender=__name__, user=user)

        if hasattr(user, 'userdisplaysetting'):
            user_display_setting_model_data = user.userdisplaysetting
            user_display_setting = UserDisplaySettingSerializer(user_display_setting_model_data).data
        else:
            user_display_setting_model_data = UserDisplaySetting.objects.create(user=user, content=json.dumps(settings.USER_DISPLAY_SETTING_DEFAULT))
            user_display_setting = UserDisplaySettingSerializer(user_display_setting_model_data).data

        # NOTE(joshua-hashimoto): Intercomのドキュメントにあるやり方をそのまま使用。
        intercom_secret_key = settings.INTERCOM_SECRET_KEY
        intercom_user_hash = hmac.new(
            bytes(intercom_secret_key, 'utf-8'),
            bytes(str(user.id), 'utf-8'),
            digestmod=hashlib.sha256
        ).hexdigest()

        context = {
            'token': new_token.key,
            'user_id': user.id,
            'display_name': user.display_name,
            'avatar': user.avatar.url if user.avatar else "",
            'is_user_admin': True if is_admin_or_master(user) else False,
            'role': user.user_role_name,
            'authorized_actions': settings.ROLE_AUTHORIZED_ACTIONS.get(user.user_role_name, {}),
            'intercom_user_hash': intercom_user_hash,
            'user_display_setting': user_display_setting,
        }

        return Response(context)


class Logout(APIView):
    def get(self, request, *args, **kwargs):
        # Remove token
        request.user.auth_token.delete()
        # clear session
        logout(request)
        return Response(status=status.HTTP_200_OK, data={'message': settings.MESSAGE_LOGOUT_SUCCESS})
