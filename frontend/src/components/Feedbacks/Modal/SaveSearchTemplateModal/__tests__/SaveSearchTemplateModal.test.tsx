import React from "react";
import { renderWithRedux, screen, userEvent } from "~/test/utils";
import { organizationSearchPage } from "~/reducers/pages";
import { defaultInitialState as SearchPageInitialState } from "~/reducers/Factories/searchPage";
import SaveSearchTemplateModal from "../SaveSearchTemplateModal";
import { configureStore } from "@reduxjs/toolkit";

const mockOnOk = jest.fn();
const mockOnCancel = jest.fn();

const store = configureStore({
    reducer: {
        organizationSearchPage,
    },
    preloadedState: {
        organizationSearchPage: {
            ...SearchPageInitialState,
        },
    },
});

describe("SaveSearchTemplateModal.tsx", () => {
    test("render test", async () => {
        const templateReducerName = "organizationSearchPage";
        renderWithRedux(
            <SaveSearchTemplateModal
                templateReducerName={templateReducerName}
                isSaveTemplateModalVisible
                onOk={mockOnOk}
                onCancel={mockOnCancel}
            />,
            {
                store,
            }
        );
        const modalTitleElement = screen.getByText(
            /保存するテンプレートの名称を入力してください/
        );
        expect(modalTitleElement).toBeInTheDocument();
        const inputElement = screen.getByPlaceholderText(
            "テンプレート名称"
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).not.toBeDisabled();
        const cancelButtonElement = screen.getByRole("button", {
            name: /キャンセル/,
        });
        expect(cancelButtonElement).toBeInTheDocument();
        expect(cancelButtonElement).not.toBeDisabled();
        const okButtonElement = screen.getByRole("button", { name: /OK/ });
        expect(okButtonElement).toBeInTheDocument();
        expect(okButtonElement).toBeDisabled();
    });

    test("input test", async () => {
        const templateReducerName = "organizationSearchPage";
        renderWithRedux(
            <SaveSearchTemplateModal
                templateReducerName={templateReducerName}
                isSaveTemplateModalVisible
                onOk={mockOnOk}
                onCancel={mockOnCancel}
            />,
            {
                store,
            }
        );
        const inputElement = screen.getByPlaceholderText(
            "テンプレート名称"
        ) as HTMLInputElement;
        const okButtonElement = screen.getByRole("button", { name: /OK/ });

        await userEvent.type(inputElement, "new search template name");
        expect(inputElement.value).toBe("new search template name");
        expect(okButtonElement).not.toBeDisabled();

        await userEvent.clear(inputElement);
        expect(inputElement.value).toBeFalsy();
        expect(okButtonElement).toBeDisabled();
    });
});
