import React from "react";
import { render, screen } from "~/test/utils";
import GenericModalContent from "../GenericModalContent";
import { Button } from "antd";

describe("GenericModalContent.tsx", () => {
    test("render test with string content", () => {
        render(<GenericModalContent>string content</GenericModalContent>);
        const contentElement = screen.getByText(/string content/);
        expect(contentElement).toBeInTheDocument();
    });

    test("render test with ReactNode content", () => {
        render(
            <GenericModalContent>
                <Button>Button Content</Button>
            </GenericModalContent>
        );
        const contentElement = screen.getByText(/Button Content/);
        expect(contentElement).toBeInTheDocument();
    });
});
