import React, { CSSProperties, ReactNode } from "react";
import {
    Button,
    ButtonProps,
    Col,
    Modal,
    ModalProps,
    Row,
    Typography,
} from "antd";
import {
    CheckCircleTwoTone,
    CloseCircleTwoTone,
    InfoCircleTwoTone,
} from "@ant-design/icons";
import { dangerColor, primaryColor, warningColor } from "~/utils/constants";

const { Title } = Typography;

const iconColSpan = 2;

type Props = ModalProps & {
    okText?: string;
    onOk?: () => void;
    okButtonProps?: ButtonProps;
    cancelText?: string;
    onCancel?: () => void;
    cancelButtonProps?: ButtonProps;
    type?: "info" | "warning" | "success" | "error";
    children: ReactNode;
};

const GenericModal = ({
    okText = "OK",
    onOk,
    okButtonProps = {},
    cancelText = "キャンセル",
    onCancel,
    cancelButtonProps = {},
    title,
    type,
    children,
    ...props
}: Props) => {
    const renderDefaultButton = () => {
        return (
            <Col span={24} style={{ marginTop: 28 }}>
                <Row justify="end" gutter={6}>
                    {!!onCancel ? (
                        <Col>
                            {" "}
                            <Button
                                type="default"
                                onClick={onCancel}
                                {...cancelButtonProps}>
                                {cancelText}
                            </Button>
                        </Col>
                    ) : null}
                    {!!onOk ? (
                        <Col>
                            <Button
                                type="primary"
                                onClick={onOk}
                                {...okButtonProps}>
                                {okText}
                            </Button>
                        </Col>
                    ) : null}
                </Row>
            </Col>
        );
    };

    const renderIcon = () => {
        const iconCommonStyle: CSSProperties = {
            fontSize: 22,
        };

        if (type === "info") {
            return (
                <InfoCircleTwoTone
                    data-testid="info-icon"
                    style={iconCommonStyle}
                />
            );
        }
        if (type === "success") {
            return (
                <CheckCircleTwoTone
                    data-testid="success-icon"
                    twoToneColor={primaryColor}
                    style={iconCommonStyle}
                />
            );
        }
        if (type === "warning") {
            return (
                <InfoCircleTwoTone
                    data-testid="warning-icon"
                    twoToneColor={warningColor}
                    style={iconCommonStyle}
                />
            );
        }
        if (type === "error") {
            return (
                <CloseCircleTwoTone
                    data-testid="error-icon"
                    twoToneColor={dangerColor}
                    style={iconCommonStyle}
                />
            );
        }
        return type;
    };

    const renderModalContent = () => {
        return (
            <Col span={24}>
                <Row justify={type ? "start" : "center"}>
                    {title ? <Title level={5}>{title}</Title> : null}
                </Row>
                <Row justify={type ? "start" : "center"}>{children}</Row>
                {!!onOk || !!onCancel ? renderDefaultButton() : null}
            </Col>
        );
    };

    return (
        <Modal zIndex={1080} width={475} {...props} footer={null} closable={false}>
            {type ? (
                <Col span={24}>
                    <Row>
                        <Col span={iconColSpan}>{renderIcon()}</Col>
                        <Col span={24 - iconColSpan}>
                            {renderModalContent()}
                        </Col>
                    </Row>
                </Col>
            ) : (
                renderModalContent()
            )}
        </Modal>
    );
};

export default GenericModal;
