import React, { useState, MouseEvent } from "react";
import {
    Button,
    Col,
    FormProps,
    Modal,
    Tooltip,
    TransferProps,
    Row,
} from "antd";
import { InfoCircleTwoTone, SettingOutlined } from "@ant-design/icons";
import SearchMenuDisplaySettingForm from "~/components/Forms/DisplaySettingsForms/SearchMenuDisplaySettingForm/SearchMenuDisplaySettingForm";
import { TransferModel } from "~/models/transferModel";
import { DisplaySettingFieldTypes } from "~/models/displaySetting";
import { infoColor, warningColor } from "~/utils/constants";
import styles from "./SearchMenuDisplaySettingModal.scss";

type Props = FormProps & {
    fieldName: DisplaySettingFieldTypes;
    formId: string;
    transferProps?: TransferProps<TransferModel>;
    onAfterClose?: () => void;
};

const SearchMenuDisplaySettingModal = ({
    fieldName,
    transferProps,
    formId,
    onAfterClose = () => {},
    ...props
}: Props) => {
    const title = "検索表示項目設定";
    const infoMessage =
        "テンプレートはユーザー個人ごとに保存され、他のユーザーとは共有されません。";

    const warningMessage =
        "表示項目は、必ず1つは表示させる必要があります。";

    const [isModalOpen, setIsModalOpen] = useState(false);

    const onCloseModal = (event: MouseEvent) => {
        event.stopPropagation();
        setIsModalOpen(false);
    };

    const allItemSelected = () => {
        return transferProps?.dataSource?.length == transferProps?.targetKeys?.length
    }

    const renderTitle = () => {
        return (
            <Col span={24}>
                <Row wrap={false}>
                    <Col span={1}></Col>
                    <Col flex="auto">{title}</Col>
                    <Col span={1}>
                        <Tooltip title={infoMessage} color={infoColor}>
                            <InfoCircleTwoTone twoToneColor={infoColor} />
                        </Tooltip>
                    </Col>
                </Row>
            </Col>
        );
    };

    const renderUpdateButton = () => {
        return (
            <Button
                type="primary"
                form={formId}
                htmlType="submit"
                disabled={allItemSelected()}
                onClick={onCloseModal}
                style={{ marginLeft: 8 }}>
                更新
            </Button>
        );
    };

    return (
        <Tooltip title={title}>
            <Button
                icon={<SettingOutlined />}
                onClick={() => setIsModalOpen(true)}
                size="middle"></Button>
            <Modal
                title={renderTitle()}
                visible={isModalOpen}
                forceRender
                closable={false}
                afterClose={onAfterClose}
                footer={[
                    <Button onClick={onCloseModal}>キャンセル</Button>,
                    allItemSelected() ? (
                        <Tooltip title={warningMessage}>
                            {renderUpdateButton()}
                        </Tooltip>
                    ) : (
                        renderUpdateButton()
                    ),
                ]}>
                <SearchMenuDisplaySettingForm
                    id={formId}
                    fieldName={fieldName}
                    {...props}
                    transferProps={transferProps}
                />
            </Modal>
        </Tooltip>
    );
};

export default SearchMenuDisplaySettingModal;
