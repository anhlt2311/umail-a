import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import TableDisplaySettingModal from "../TableDisplaySettingModal";
import { DISPLAY_SETTING_ITEMS } from "~/utils/constants";

describe("TableDisplaySettingModal.tsx", () => {
    test("render test", async () => {
        render(
            <TableDisplaySettingModal
                fieldName="organizations"
                formId="form-id"
                transferProps={{
                    dataSource: DISPLAY_SETTING_ITEMS.organizations.table,
                }}
            />
        );
        const modalOpenButtonElement = screen.getByTestId(
            "table-display-setting-modal-open-button-icon"
        );
        expect(modalOpenButtonElement).toBeInTheDocument();
        await userEvent.hover(modalOpenButtonElement);
        const tooltipMessageElement = await screen.findByText(
            /検索結果表示項目設定/
        );
        expect(tooltipMessageElement).toBeInTheDocument();
        await userEvent.click(modalOpenButtonElement);
        const modalTitleElement = await screen.findByTestId("modal-title");
        expect(modalTitleElement).toBeInTheDocument();
        const modalInfoElement = await screen.findByTestId(
            "table-display-setting-modal-info-icon"
        );
        expect(modalInfoElement).toBeInTheDocument();
        await userEvent.hover(modalInfoElement);
        const modalInfoTooltipMessageElement = await screen.findByText(
            /この設定は個人ごとに保持されますので、別ユーザーと共有されません。/
        );
        expect(modalInfoTooltipMessageElement).toBeInTheDocument();
        const cancelButtonElement = await screen.findByRole("button", {
            name: /キャンセル/,
        });
        expect(cancelButtonElement).toBeInTheDocument();
        expect(cancelButtonElement).not.toBeDisabled();
        const updateButtonElement = await screen.findByRole("button", {
            name: /更 新/,
        });
        expect(updateButtonElement).toBeInTheDocument();
        expect(updateButtonElement).not.toBeDisabled();

        // TableDisplaySettingFormの簡易存在テスト
        const transferDisplayLabelElement = await screen.findByText(/^表示$/);
        expect(transferDisplayLabelElement).toBeInTheDocument();
        const transferUndisplayLabelElement = await screen.findByText(/非表示/);
        expect(transferUndisplayLabelElement).toBeInTheDocument();
        const transferPageSizeLabelElement = await screen.findByText(
            /表示件数/
        );
        expect(transferPageSizeLabelElement).toBeInTheDocument();
    });
});
