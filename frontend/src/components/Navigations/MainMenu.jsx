import React, { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { Menu, Tooltip } from "antd";
import Path from "../Routes/Paths";
import {
    MailOutlined,
    BankOutlined,
    DashboardOutlined,
    TableOutlined,
    FormOutlined,
    NotificationOutlined,
} from "@ant-design/icons";
import { usePlanSummaryAPIQuery } from "~/hooks/usePlan";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import styles from './MainMenu.scss';

const { SubMenu } = Menu;
const BlankDiv = () => <div/>

const MainMenu = ({ }) => {
    const location = useLocation()
    const [selectedKey, setSelectedKey] = useState("")

    const menuData = {
        "/organizations": "orgList",
        "/organizations/register": "registerOrg",
        "/contacts": "contactList",
        "/contacts/register": "registerContact",
        "/scheduledMails": "scheduledMailList",
        "/scheduledMails/register": "registerScheduledMail",
        "/sharedMails": "sharedEmailList",
        "/sharedMailNotifications": "sharedMailNotifications",
        "/personnel": "personnelBoard",
        "/project": "projectBoard",
    };

    const { create: organizationCreateAuthorized } =
        useAuthorizedActions("organizations");
    const { create: contactCreateAuthorized } =
        useAuthorizedActions("contacts");
    const { create: scheduledEmailCreateAuthorized } =
        useAuthorizedActions("scheduled_mails");
    const { _all: notificationRulesAuthorized } =
        useAuthorizedActions("notification_rules");
    const { data } = usePlanSummaryAPIQuery({});

    useEffect(() => {
        const selectedItem = menuData[location.pathname] || "";
        setSelectedKey(selectedItem)
    }, [location.pathname])

    const sharedEmailsAuthorized = data?.planId >= 2;

    return (
        <Menu
            mode="inline"
            style={{ height: "100%", borderRight: 0, textAlign: "left" }}
            selectedKeys={selectedKey}
        >
            <Menu.Item key="dashboard">
                <Link to={Path.dashboard}>
                    <DashboardOutlined />
                    <span>ダッシュボード</span>
                </Link>
            </Menu.Item>
            <SubMenu
                key="Organizations"
                title={
                    <span>
                        <BankOutlined />
                        <span>取引先管理</span>
                    </span>
                }>
                <Menu.Item key="orgList">
                    <Link to={Path.organizations}>
                        <TableOutlined />
                        <span>取引先一覧</span>
                    </Link>
                </Menu.Item>
                {organizationCreateAuthorized ? (
                    <Menu.Item key="registerOrg">
                        <Link to={Path.organizationsRegister}>
                            <FormOutlined />
                            <span>取引先登録</span>
                        </Link>
                    </Menu.Item>
                ) : (
                    <Menu.Item key="registerOrg" disabled className={styles.disabled}>
                        <Tooltip title={"特定の権限で操作できます"}>
                            <FormOutlined />
                            <span>取引先登録</span>
                        </Tooltip>
                    </Menu.Item>
                )}
                <Menu.Item key="contactList">
                    <Link to={Path.contacts}>
                        <TableOutlined />
                        <span>取引先担当者一覧</span>
                    </Link>
                </Menu.Item>
                {contactCreateAuthorized ? (
                    <Menu.Item key="registerContact">
                        <Link to={Path.contactRegister}>
                            <FormOutlined />
                            <span>取引先担当者登録</span>
                        </Link>
                    </Menu.Item>
                ) : (
                    <Menu.Item key="registerContact" disabled className={styles.disabled}>
                        <Tooltip title={"特定の権限で操作できます"}>
                            <FormOutlined />
                            <span>取引先担当者登録</span>
                        </Tooltip>
                    </Menu.Item>
                )}
            </SubMenu>
            <SubMenu
                key="scheduledMails"
                title={
                    <span>
                        <NotificationOutlined />
                        <span>配信メール管理</span>
                    </span>
                }>
                <Menu.Item key="scheduledMailList">
                    <Link to={Path.scheduledMails}>
                        <TableOutlined />
                        <span>配信メール一覧</span>
                    </Link>
                </Menu.Item>
                {scheduledEmailCreateAuthorized ? (
                    <Menu.Item key="registerScheduledMail">
                        <Link to={Path.scheduledMailsRegister}>
                            <FormOutlined />
                            <span>配信メール予約</span>
                        </Link>
                    </Menu.Item>
                ) : (
                    <Menu.Item key="registerScheduledMail" disabled className={styles.disabled}>
                        <Tooltip title={"特定の権限で操作できます"}>
                            <FormOutlined />
                            <span>配信メール予約</span>
                        </Tooltip>
                    </Menu.Item>
                )}
            </SubMenu>
            {sharedEmailsAuthorized ? (
                <SubMenu
                    key="sharedMails"
                    title={
                        <span>
                            <MailOutlined />
                            <span>共有メール管理</span>
                        </span>
                    }>
                    <Menu.Item key="sharedEmailList">
                        <Link to={Path.sharedMails}>
                            <TableOutlined />
                            <span>共有メール一覧</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="sharedMailNotifications">
                        {notificationRulesAuthorized ? (
                            <Link to={Path.sharedMailNotifications}>
                                <TableOutlined />
                                <span>共有メール通知条件一覧</span>
                            </Link>
                        ) : (
                            <Tooltip title={"特定の権限で操作できます"}>
                                <TableOutlined />
                                <span>共有メール通知条件一覧</span>
                            </Tooltip>
                        )}
                    </Menu.Item>
                </SubMenu>
            ) : (
                <BlankDiv />
            )}
            {sharedEmailsAuthorized && <SubMenu
                key="personnel"
                title={
                    <span>
                        <span>要員管理</span>
                    </span>
                }>
                <Menu.Item key="personnelBoard">
                    <Link to={Path.personnel}>
                        <span>要員ボード</span>
                    </Link>
                </Menu.Item>
            </SubMenu>}
            {sharedEmailsAuthorized && <SubMenu
                key="project"
                title={
                    <span>
                        <span>要員管理</span>
                    </span>
                }>
                <Menu.Item key="projectBoard">
                    <Link to={Path.project}>
                        <span>案件ボード</span>
                    </Link>
                </Menu.Item>
            </SubMenu>}
        </Menu>
    );
};

export default MainMenu;
