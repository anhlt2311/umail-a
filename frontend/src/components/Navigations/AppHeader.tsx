import React, { CSSProperties } from "react";
import { Link, useLocation } from "react-router-dom";
import { Layout, Image, Typography, Col, Row, Tooltip, message } from "antd";
import {
    QuestionOutlined,
    BellOutlined,
    SettingOutlined,
    SmileOutlined,
} from "@ant-design/icons";
import HeaderIcon from "./HeaderIcon/HeaderIcon";
import Path from "../Routes/Paths";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import {
    dangerColor,
    HEADER_HEIGHT,
    Links,
    SuccessMessages,
} from "~/utils/constants";
import PlanSummary from "../AppHeader/PlanSummary/PlanSummary";
import { useGuardPlanSummary } from "~/hooks/usePlan";
import PopoverMenuItem from "../Common/PopoverMenuItem/PopoverMenuItem";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import NotificationPopover from "../DataDisplay/NotificationPopover/NotificationPopover";
import { useLogoutAPIMutation } from "~/hooks/useAuth";
import { customSuccessMessage } from "~/components/Common/AlertMessage/AlertMessage";
import ProfileAvatar from "~/components/Common/ProfileAvatar/ProfileAvatar";
import { useSearchTemplateUtils } from "~/hooks/useSearchTemplate";
import PuzzleIcon from "../Common/PuzzleIcon/PuzzleIcon";
import BuildingIcon from "../Common/BuildingIcon/BuildingIcon";
import styles from "./AppHeader.scss";

const { Header } = Layout;
const { Link: AntdLink } = Typography;

const headerStyle: CSSProperties = {
    marginBottom: 4,
    padding: 0,
    position: "fixed",
    zIndex: 10,
    width: "100%",
    boxShadow: "0 0 10px 0 #dcdcdc",
};

const logoContainerStyle: CSSProperties = {
    float: "left",
    fontSize: 20,
    marginLeft: 8,
};

const logoStyle: CSSProperties = { width: 200 };

const iconStyle: CSSProperties = {
    fontSize: 16,
};

export type Props = {
    isLoggedIn: boolean;
};

const AppHeader = ({ isLoggedIn }: Props) => {
    const { mutate: logout } = useLogoutAPIMutation();
    const { removeSearchConditions } = useSearchTemplateUtils();
    const { token, avatar } = useSelector((state: RootState) => state.login);
    const { doesNewSystemNotificationExists, newNotificationCount } =
        useSelector((state: RootState) => state.systemNotificationReducer);
    const { _all: companyAuthorized } = useAuthorizedActions("my_company");
    const { _all: userAuthorized } = useAuthorizedActions("users");
    const { _all: tagAuthorized } = useAuthorizedActions("tags");
    const { _all: displaySettingAuthorized } =
        useAuthorizedActions("display_settings");
    const { _all: sharedEmailSettingAuthorized } = useAuthorizedActions(
        "shared_email_settings"
    );
    const { _all: scheduledEmailSettingAuthorized } = useAuthorizedActions(
        "scheduled_email_settings"
    );
    const { _all: addonsAuthorized } = useAuthorizedActions("addon");
    const { _all: planAuthorized } = useAuthorizedActions("plan");
    const { _all: billingAuthorized } = useAuthorizedActions("billing");
    const { _all: purchaseHistoryAuthorized } =
        useAuthorizedActions("purchase_history");
    const { isNotValidUser, isStandardPlan } = useGuardPlanSummary();

    const pathOfLogo = isLoggedIn ? Path.dashboard : Path.login;

    const onLogout = () => {
        logout(undefined, {
            onSuccess: () => {
                removeSearchConditions();
                customSuccessMessage(SuccessMessages.auth.logout);
            },
        });
    };

    const localtion = useLocation();

    const isAttachmentPage =
        localtion.pathname.indexOf("scheduledMail/files/download") !== -1;

    const renderHeaderLogo = () => {
        return (
            <div style={logoContainerStyle}>
                <Link
                    to={
                        isNotValidUser && isLoggedIn
                            ? (location) => location
                            : pathOfLogo
                    }>
                    <Image
                        data-testid="cmrb-logo"
                        width={200}
                        preview={false}
                        src="/static/app_staffing/cmrb.png"
                        style={logoStyle}
                    />
                </Link>
            </div>
        );
    };

    const renderNotificationPopover = () => {
        return <NotificationPopover />;
    };

    const renderExpansionSettingPopover = () => {
        return (
            <>
                <PopoverMenuItem>
                    {addonsAuthorized ? (
                        <Link to={Path.addons}>アドオン</Link>
                    ) : (
                        <Tooltip
                            title={"特定の権限で操作できます"}
                            className={styles.disabled}>
                            <span>アドオン</span>
                        </Tooltip>
                    )}
                </PopoverMenuItem>
            </>
        );
    };

    const renderFunctionSettingPopover = () => {
        return (
            <>
                <PopoverMenuItem>
                    {scheduledEmailSettingAuthorized ? (
                        <Link to={Path.scheduledEmailSettings}>
                            配信メール設定
                        </Link>
                    ) : (
                        <Tooltip
                            title={"特定の権限で操作できます"}
                            className={styles.disabled}>
                            <span>配信メール設定</span>
                        </Tooltip>
                    )}
                </PopoverMenuItem>
                {isStandardPlan ? (
                    <PopoverMenuItem>
                        {sharedEmailSettingAuthorized ? (
                            <Link to={Path.sharedEmailSettings}>
                                共有メール設定
                            </Link>
                        ) : (
                            <Tooltip
                                title={"特定の権限で操作できます"}
                                className={styles.disabled}>
                                <span>共有メール設定</span>
                            </Tooltip>
                        )}
                    </PopoverMenuItem>
                ) : (
                    <div />
                )}
            </>
        );
    };

    const renderCommonSettingPopover = () => {
        return (
            <>
                <PopoverMenuItem>
                    {companyAuthorized ? (
                        <Link to={Path.myCompany}>自社プロフィール</Link>
                    ) : (
                        <Tooltip
                            title={"特定の権限で操作できます"}
                            className={styles.disabled}>
                            <span>自社プロフィール</span>
                        </Tooltip>
                    )}
                </PopoverMenuItem>
                <PopoverMenuItem>
                    {userAuthorized ? (
                        <Link to={Path.users}>ユーザー設定</Link>
                    ) : (
                        <Tooltip
                            title={"特定の権限で操作できます"}
                            className={styles.disabled}>
                            <span>ユーザー設定</span>
                        </Tooltip>
                    )}
                </PopoverMenuItem>
                <PopoverMenuItem>
                    {displaySettingAuthorized ? (
                        <Link to={Path.displaySettings}>必須項目設定</Link>
                    ) : (
                        <Tooltip
                            title={"特定の権限で操作できます"}
                            className={styles.disabled}>
                            <span>必須項目設定</span>
                        </Tooltip>
                    )}
                </PopoverMenuItem>
                <PopoverMenuItem>
                    {tagAuthorized ? (
                        <Link to={Path.tags}>タグ設定</Link>
                    ) : (
                        <Tooltip
                            title={"特定の権限で操作できます"}
                            className={styles.disabled}>
                            <span>タグ設定</span>
                        </Tooltip>
                    )}
                </PopoverMenuItem>
                <PopoverMenuItem>
                    <Link to={Path.personnel}>要員設定</Link>
                </PopoverMenuItem>
                <PopoverMenuItem>
                    <Link to={Path.project}>案件設定</Link>
                </PopoverMenuItem>
            </>
        );
    };

    const renderAccountPopover = () => {
        return (
            <>
                {!isNotValidUser && (
                    <PopoverMenuItem>
                        <Link to={Path.myProfile}>個人プロフィール</Link>
                    </PopoverMenuItem>
                )}
                <PopoverMenuItem>
                    {planAuthorized ? (
                        <Link to={Path.plan}>ご利用プラン</Link>
                    ) : (
                        <Tooltip
                            title={"特定の権限で操作できます"}
                            className={styles.disabled}>
                            <span>ご利用プラン</span>
                        </Tooltip>
                    )}
                </PopoverMenuItem>
                <PopoverMenuItem>
                    {billingAuthorized ? (
                        <Link to={Path.payment}>お支払い</Link>
                    ) : (
                        <Tooltip
                            title={"特定の権限で操作できます"}
                            className={styles.disabled}>
                            <span>お支払い</span>
                        </Tooltip>
                    )}
                </PopoverMenuItem>
                <PopoverMenuItem>
                    {purchaseHistoryAuthorized ? (
                        <Link to={Path.purchaseHistory}>お支払い履歴</Link>
                    ) : (
                        <Tooltip
                            title={"特定の権限で操作できます"}
                            className={styles.disabled}>
                            <span>お支払い履歴</span>
                        </Tooltip>
                    )}
                </PopoverMenuItem>
                <PopoverMenuItem>
                    <a onClick={onLogout}>ログアウト</a>
                </PopoverMenuItem>
            </>
        );
    };

    return (
        <Header style={headerStyle}>
            <Col span={24}>
                <Row>
                    <Col span={6} style={{ height: HEADER_HEIGHT }}>
                        {renderHeaderLogo()}
                    </Col>
                    <Col span={12} style={{ height: HEADER_HEIGHT }}>
                        {!!token ? <PlanSummary /> : undefined}
                    </Col>
                    <Col span={6} style={{ height: HEADER_HEIGHT }}>
                        <div
                            style={{
                                float: "right",
                                display: "flex",
                                height: "100%",
                            }}>
                            {isLoggedIn && !isNotValidUser && (
                                <HeaderIcon
                                    title={null}
                                    content={
                                        <AntdLink
                                            target="_blank"
                                            rel="noopener noreferrer"
                                            href={Links.services.helpCenter}>
                                            ヘルプセンター
                                        </AntdLink>
                                    }
                                    icon={
                                        <QuestionOutlined style={iconStyle} />
                                    }
                                />
                            )}
                            {isLoggedIn && !isNotValidUser && (
                                <HeaderIcon
                                    title={null}
                                    content={renderNotificationPopover()}
                                    icon={
                                        doesNewSystemNotificationExists ? (
                                            <>
                                                <BellOutlined
                                                    style={iconStyle}
                                                />
                                                <div
                                                    style={{
                                                        position: "absolute",
                                                        top: -1,
                                                        right: 1,
                                                        width: 14,
                                                        height: 14,
                                                        background: dangerColor,
                                                        fontSize: 10,
                                                        borderRadius: 15,
                                                        color: "#ffffff",
                                                    }}>
                                                    {newNotificationCount}
                                                </div>
                                            </>
                                        ) : (
                                            <BellOutlined style={iconStyle} />
                                        )
                                    }
                                />
                            )}
                            {isLoggedIn && !isNotValidUser && (
                                <HeaderIcon
                                    title="拡張設定"
                                    content={renderExpansionSettingPopover()}
                                    icon={<PuzzleIcon style={iconStyle} />}
                                />
                            )}
                            {isLoggedIn && !isNotValidUser && (
                                <HeaderIcon
                                    title="機能設定"
                                    content={renderFunctionSettingPopover()}
                                    icon={<SettingOutlined style={iconStyle} />}
                                />
                            )}
                            {isLoggedIn && !isNotValidUser && (
                                <HeaderIcon
                                    title="共通設定"
                                    content={renderCommonSettingPopover()}
                                    icon={<BuildingIcon style={iconStyle} />}
                                />
                            )}
                            {isLoggedIn && (
                                <HeaderIcon
                                    title="その他"
                                    content={renderAccountPopover()}
                                    icon={
                                        !!avatar ? (
                                            <div
                                                className={
                                                    styles.avatarButtonWrapper
                                                }>
                                                <ProfileAvatar
                                                    style={iconStyle}
                                                    avatar={avatar}
                                                />
                                            </div>
                                        ) : (
                                            <SmileOutlined style={iconStyle} />
                                        )
                                    }
                                />
                            )}
                        </div>
                    </Col>
                </Row>
            </Col>
        </Header>
    );
};

export default AppHeader;
