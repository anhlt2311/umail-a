import React from "react";
import AppHeader from "../../AppHeader";
import { renderWithAllProviders, screen } from "~/test/utils";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import {
    systemNotificationReducer,
    SystemNotificationInitialState,
} from "~/reducers/notificationReducer";
import { PayloadAction } from "~/models/reduxModel";
import { generateRandomToken } from "~/utils/utils";

const randomToken = generateRandomToken();

describe("AppHeader", () => {
    test("render component", () => {
        renderWithAllProviders(<AppHeader isLoggedIn={true} />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: { login, systemNotificationReducer },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: randomToken,
                    },
                    systemNotificationReducer: {
                        ...SystemNotificationInitialState,
                    },
                },
            }),
        });
        const logoElement = screen.getByTestId(/cmrb-logo/);
        expect(logoElement).toBeInTheDocument();
    });
});
