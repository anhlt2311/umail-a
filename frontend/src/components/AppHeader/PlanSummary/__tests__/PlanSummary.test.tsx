import React from "react";
import {
    mockPlanSummaryFreeTrialFailureWithExpireTodayAPIRoute,
    mockPlanSummaryFreeTrialFailureWithNoRemainingDaysAPIRoute,
    mockPlanSummaryFreeTrialFailureWithRemainingDaysAPIRoute,
    mockPlanSummaryLightPlanFailureWithExpireTodayAPIRoute,
    mockPlanSummaryLightPlanFailureWithNoRemainingDaysAPIRoute,
    mockPlanSummaryLightPlanFailureWithRemainingDaysAPIRoute,
    mockPlanSummaryStandardPlanFailureWithExpireTodayAPIRoute,
    mockPlanSummaryStandardPlanFailureWithNoRemainingDaysAPIRoute,
    mockPlanSummaryStandardPlanFailureWithRemainingDaysAPIRoute,
    mockPlanSummaryProfessionalPlanFailureWithExpireTodayAPIRoute,
    mockPlanSummaryProfessionalPlanFailureWithNoRemainingDaysAPIRoute,
    mockPlanSummaryProfessionalPlanFailureWithRemainingDaysAPIRoute,
} from "~/test/mock/planAPIMock";
import { mockServer } from "~/test/setupTests";
import {
    renderHook,
    renderWithAllProviders,
    screen,
    waitForElementToBeRemoved,
} from "~/test/utils";
import login, { LoginInitialState } from "~/reducers/login";

import PlanSummary from "../PlanSummary";
import { configureStore } from "@reduxjs/toolkit";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";
import { HelpMessages } from "~/utils/constants";

describe("PlanSummary.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test: free trial with remaining days", async () => {
        mockServer.use(
            mockPlanSummaryFreeTrialFailureWithRemainingDaysAPIRoute
        );
        renderWithAllProviders(<PlanSummary />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        role: "master",
                    },
                },
            }),
        });
        await waitForElementToBeRemoved(() => screen.getByTestId("loading"));
        const messageText = HelpMessages.planSummary.freeTrial.expiresIn(9);
        const messageRegex = new RegExp(messageText);
        const messageElement = await screen.findByText(messageRegex);
        expect(messageElement).toBeInTheDocument();
        const freeTrialNavigateButtonElement = await screen.findByText(
            /有料プランへの切り替えはこちら/
        );
        expect(freeTrialNavigateButtonElement).toBeInTheDocument();
        const payedNavigateButtonElement =
            screen.queryByText(/お支払い設定を確認/);
        expect(payedNavigateButtonElement).not.toBeInTheDocument();
    });

    test("render test: free trial expires today", async () => {
        mockServer.use(mockPlanSummaryFreeTrialFailureWithExpireTodayAPIRoute);
        renderWithAllProviders(<PlanSummary />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        role: "master",
                    },
                },
            }),
        });
        await waitForElementToBeRemoved(() => screen.getByTestId("loading"));
        const messageText = HelpMessages.planSummary.freeTrial.expiresToday;
        const messageRegex = new RegExp(messageText);
        const messageElement = await screen.findByText(messageRegex);
        expect(messageElement).toBeInTheDocument();
        const freeTrialNavigateButtonElement = await screen.findByText(
            /有料プランへの切り替えはこちら/
        );
        expect(freeTrialNavigateButtonElement).toBeInTheDocument();
        const payedNavigateButtonElement =
            screen.queryByText(/お支払い設定を確認/);
        expect(payedNavigateButtonElement).not.toBeInTheDocument();
    });

    test("render test: free trial with no remaining days", async () => {
        mockServer.use(
            mockPlanSummaryFreeTrialFailureWithNoRemainingDaysAPIRoute
        );
        renderWithAllProviders(<PlanSummary />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        role: "master",
                    },
                },
            }),
        });
        await waitForElementToBeRemoved(() => screen.getByTestId("loading"));
        const messageOneText =
            "無料トライアルの期間が終了しました。終了から30日間が経過するとデータが削除されます。";
        const messageOneRegex = new RegExp(messageOneText);
        const messageOneElement = await screen.findByText(messageOneRegex);
        expect(messageOneElement).toBeInTheDocument();
        const messageTwoText =
            "データ削除後のプラン購入は可能ですが、削除されたデータの復元はできかねます。";
        const messageTwoRegex = new RegExp(messageTwoText);
        const messageTwoElement = await screen.findByText(messageTwoRegex);
        expect(messageTwoElement).toBeInTheDocument();
        const detailLinkElement = await screen.findByRole("link", {
            name: /詳細/,
        });
        expect(detailLinkElement).toBeInTheDocument();
    });

    describe("Light Plan", () => {
        test("render test: not master role", async () => {
            mockServer.use(
                mockPlanSummaryLightPlanFailureWithNoRemainingDaysAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "admin",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText =
                "お支払い期限を過ぎましたので、プランを停止いたしました。";
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const messageTwoText =
                "しばらくするとデータが削除されますので、お支払いをお急ぎ頂くか、お問合せください。";
            const messageTwoRegex = new RegExp(messageTwoText);
            const messageTwoElement = await screen.findByText(messageTwoRegex);
            expect(messageTwoElement).toBeInTheDocument();
            const settingButtonElement =
                screen.queryByText(/お支払い設定を確認/);
            expect(settingButtonElement).not.toBeInTheDocument();
        });

        test("render test: light plan with remaining days", async () => {
            mockServer.use(
                mockPlanSummaryLightPlanFailureWithRemainingDaysAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "master",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText = HelpMessages.planSummary.payed.expiresIn(9);
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const settingButtonElement = await screen.findByText(
                /お支払い設定を確認/
            );
            expect(settingButtonElement).toBeInTheDocument();
        });

        test("render test: light plan expires today", async () => {
            mockServer.use(
                mockPlanSummaryLightPlanFailureWithExpireTodayAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "master",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText = HelpMessages.planSummary.payed.expiresToday;
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const settingButtonElement = await screen.findByText(
                /お支払い設定を確認/
            );
            expect(settingButtonElement).toBeInTheDocument();
        });

        test("render test: light plan with no remaining days", async () => {
            mockServer.use(
                mockPlanSummaryLightPlanFailureWithNoRemainingDaysAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "master",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText =
                "お支払い期限を過ぎましたので、プランを停止いたしました。";
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const messageTwoText =
                "しばらくするとデータが削除されますので、お支払いをお急ぎ頂くか、お問合せください。";
            const messageTwoRegex = new RegExp(messageTwoText);
            const messageTwoElement = await screen.findByText(messageTwoRegex);
            expect(messageTwoElement).toBeInTheDocument();
        });
    });

    describe("Standard Plan", () => {
        test("render test: not master role", async () => {
            mockServer.use(
                mockPlanSummaryStandardPlanFailureWithNoRemainingDaysAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "admin",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText =
                "お支払い期限を過ぎましたので、プランを停止いたしました。";
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const messageTwoText =
                "しばらくするとデータが削除されますので、お支払いをお急ぎ頂くか、お問合せください。";
            const messageTwoRegex = new RegExp(messageTwoText);
            const messageTwoElement = await screen.findByText(messageTwoRegex);
            expect(messageTwoElement).toBeInTheDocument();
            const settingButtonElement =
                screen.queryByText(/お支払い設定を確認/);
            expect(settingButtonElement).not.toBeInTheDocument();
        });

        test("render test: standard plan with remaining days", async () => {
            mockServer.use(
                mockPlanSummaryStandardPlanFailureWithRemainingDaysAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "master",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText = HelpMessages.planSummary.payed.expiresIn(9);
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const settingButtonElement = await screen.findByText(
                /お支払い設定を確認/
            );
            expect(settingButtonElement).toBeInTheDocument();
        });

        test("render test: standard plan expires today", async () => {
            mockServer.use(
                mockPlanSummaryStandardPlanFailureWithExpireTodayAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "master",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText = HelpMessages.planSummary.payed.expiresToday;
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const settingButtonElement = await screen.findByText(
                /お支払い設定を確認/
            );
            expect(settingButtonElement).toBeInTheDocument();
        });

        test("render test: standard plan with no remaining days", async () => {
            mockServer.use(
                mockPlanSummaryStandardPlanFailureWithNoRemainingDaysAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "master",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText =
                "お支払い期限を過ぎましたので、プランを停止いたしました。";
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const messageTwoText =
                "しばらくするとデータが削除されますので、お支払いをお急ぎ頂くか、お問合せください。";
            const messageTwoRegex = new RegExp(messageTwoText);
            const messageTwoElement = await screen.findByText(messageTwoRegex);
            expect(messageTwoElement).toBeInTheDocument();
        });
    });

    describe("Professional Plan", () => {
        test("render test: not master role", async () => {
            mockServer.use(
                mockPlanSummaryProfessionalPlanFailureWithNoRemainingDaysAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "admin",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText =
                "お支払い期限を過ぎましたので、プランを停止いたしました。";
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const messageTwoText =
                "しばらくするとデータが削除されますので、お支払いをお急ぎ頂くか、お問合せください。";
            const messageTwoRegex = new RegExp(messageTwoText);
            const messageTwoElement = await screen.findByText(messageTwoRegex);
            expect(messageTwoElement).toBeInTheDocument();
            const settingButtonElement =
                screen.queryByText(/お支払い設定を確認/);
            expect(settingButtonElement).not.toBeInTheDocument();
        });

        test("render test: professional plan with remaining days", async () => {
            mockServer.use(
                mockPlanSummaryProfessionalPlanFailureWithRemainingDaysAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "master",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText = HelpMessages.planSummary.payed.expiresIn(9);
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const settingButtonElement = await screen.findByText(
                /お支払い設定を確認/
            );
            expect(settingButtonElement).toBeInTheDocument();
        });

        test("render test: professional plan expires today", async () => {
            mockServer.use(
                mockPlanSummaryProfessionalPlanFailureWithExpireTodayAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "master",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText = HelpMessages.planSummary.payed.expiresToday;
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const settingButtonElement = await screen.findByText(
                /お支払い設定を確認/
            );
            expect(settingButtonElement).toBeInTheDocument();
        });

        test("render test: professional plan with no remaining days", async () => {
            mockServer.use(
                mockPlanSummaryProfessionalPlanFailureWithNoRemainingDaysAPIRoute
            );
            renderWithAllProviders(<PlanSummary />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "master",
                        },
                    },
                }),
            });
            await waitForElementToBeRemoved(() =>
                screen.getByTestId("loading")
            );
            const messageOneText =
                "お支払い期限を過ぎましたので、プランを停止いたしました。";
            const messageOneRegex = new RegExp(messageOneText);
            const messageOneElement = await screen.findByText(messageOneRegex);
            expect(messageOneElement).toBeInTheDocument();
            const messageTwoText =
                "しばらくするとデータが削除されますので、お支払いをお急ぎ頂くか、お問合せください。";
            const messageTwoRegex = new RegExp(messageTwoText);
            const messageTwoElement = await screen.findByText(messageTwoRegex);
            expect(messageTwoElement).toBeInTheDocument();
        });
    });
});
