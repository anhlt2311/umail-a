import React, { ReactNode } from "react";
import { Button, Col, Row } from "antd";
import { usePlans } from "~/hooks/usePlan";
import { useHistory } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import {
    HelpMessages,
    PAYED_PLAN_TERMINATION_POSTPONEMENT_PERIOD,
} from "~/utils/constants";
import { useRole } from "~/hooks/useRole";
import { Links } from "~/utils/constants";
import styles from "./PlanSummary.scss";

const PlanSummary = () => {
    const router = useHistory();
    const { isMasterRole } = useRole();
    const { isLoading, isError, data, isPayedPlan, isFreeTrial } = usePlans();

    const toPlanPage = () => {
        router.push(Paths.plan);
    };

    if (isLoading || isError || !data) {
        return <span data-testid="loading"></span>;
    }

    const renderTrial = (message: ReactNode, isExpired: boolean) => {
        return (
            <Col span={24}>
                <Row justify="center" wrap={false}>
                    <Col>
                        <div style={{ width: "100%", height: "15%" }}>
                            <p
                                style={{
                                    margin: 0,
                                    padding: 0,
                                    lineHeight: "35px",
                                    color: "#E1585A",
                                }}>
                                {message}
                            </p>
                        </div>
                        {isExpired ? null : (
                            <div
                                style={{
                                    width: "100%",
                                    margin: 0,
                                    padding: 0,
                                    height: "80%",
                                }}>
                                <Button
                                    type="primary"
                                    size="small"
                                    className={styles.focusColor}
                                    onClick={toPlanPage}>
                                    有料プランへの切り替えはこちら
                                </Button>
                            </div>
                        )}
                    </Col>
                </Row>
            </Col>
        );
    };

    const renderPayed = (message: ReactNode, isExpired: boolean) => {
        const onClick = () => {
            router.push(Paths.payment);
        };
        return (
            <Col span={24}>
                <Row justify="center" wrap={false}>
                    <Col>
                        <div style={{ width: "100%", height: "15%" }}>
                            <p
                                style={{
                                    margin: 0,
                                    padding: 0,
                                    lineHeight: "35px",
                                    color: "#E1585A",
                                }}>
                                {message}
                            </p>
                        </div>
                        {!isMasterRole || isExpired ? null : (
                            <div
                                style={{
                                    width: "100%",
                                    margin: 0,
                                    padding: 0,
                                    height: "80%",
                                }}>
                                <Button
                                    type="primary"
                                    size="small"
                                    onClick={onClick}>
                                    お支払い設定を確認
                                </Button>
                            </div>
                        )}
                    </Col>
                </Row>
            </Col>
        );
    };

    if (isFreeTrial) {
        // 有効期限は文字列のためそれを数値型に変換
        const remainingDays = +data.remainingDays;
        // 残り日数が0日なら有効期限は当日
        const isExpiringToday = !remainingDays;
        // 有効期限が当日かどうかによって、表示されるメッセージが違う
        const messageForDaysRemaining = isExpiringToday
            ? HelpMessages.planSummary.freeTrial.expiresToday
            : HelpMessages.planSummary.freeTrial.expiresIn(remainingDays);
        // data.remainingDaysが「そもそも」false扱いできる場合、有効期間が切れているのでそれによってメッセージをさらに切り替える
        const message = !!data.remainingDays ? (
            messageForDaysRemaining
        ) : (
            <>
                <span>
                    <p
                        style={{
                            lineHeight: "20px",
                            margin: 0,
                            marginTop: "2%",
                        }}>
                        無料トライアルの期間が終了しました。終了から30日間が経過するとデータが削除されます。
                    </p>
                    <p style={{ lineHeight: "20px", margin: 0 }}>
                        データ削除後のプラン購入は可能ですが、削除されたデータの復元はできかねます。
                        <a
                            href={Links.helps.plan.trialExpired}
                            target="_blank"
                            rel="noopener noreferrer">
                            詳細
                        </a>
                    </p>
                </span>
            </>
        );
        return renderTrial(message, !data.remainingDays);
    }

    if (isPayedPlan) {
        // 決済エラーが発生していなければプラン停止のメッセージは表示しない
        if (!data.paymentErrorExists) {
            return null;
        }
        // 有効期限は文字列のためそれを数値型に変換
        const remainingDays = +data.remainingDays;
        if (remainingDays >= PAYED_PLAN_TERMINATION_POSTPONEMENT_PERIOD) {
            return null;
        }
        // 残り日数が0日なら有効期限は当日
        const isExpiringToday = !remainingDays;
        // 有効期限が当日かどうかによって、表示されるメッセージが違う
        const messageForDaysRemaining = isExpiringToday
            ? HelpMessages.planSummary.payed.expiresToday
            : HelpMessages.planSummary.payed.expiresIn(remainingDays);
        // data.remainingDaysが「そもそも」false扱いできる場合、有効期間が切れているのでそれによってメッセージをさらに切り替える
        const message = !!data.remainingDays ? (
            messageForDaysRemaining
        ) : (
            <span>
                <p style={{ lineHeight: "20px", margin: 0, marginTop: "1%" }}>
                    お支払い期限を過ぎましたので、プランを停止いたしました。
                </p>
                <p style={{ lineHeight: "20px", margin: 0 }}>
                    しばらくするとデータが削除されますので、お支払いをお急ぎ頂くか、お問合せください。
                </p>
            </span>
        );
        return renderPayed(message, !data.remainingDays);
    }

    return null;
};

export default PlanSummary;
