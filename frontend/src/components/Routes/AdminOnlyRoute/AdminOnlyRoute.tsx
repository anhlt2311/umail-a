import React from "react";
import { useSelector } from "react-redux";
import { Route, RouteProps } from "react-router-dom";
import LoginPage from "~/components/Pages/LoginPage/LoginPage";
import NotFoundPage from "~/components/Pages/NotFoundPage";
import { useGuardAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { RootState } from "~/models/store";

type Props = RouteProps & {
    path: string;
};

const AdminOnlyRoute = ({ ...props }: Props) => {
    const token = useSelector((state: RootState) => state.login.token);
    const isAdminUser = useSelector(
        (state: RootState) => state.login.isAdminUser
    );

    useGuardAuthorizedActions(props.path);

    if (!token) {
        return (
            <Route
                render={(givenProps) => (
                    <LoginPage redirectTo={props.path} {...givenProps} />
                )}
            />
        );
    }

    if (token && isAdminUser) {
        return <Route {...props} />;
    }

    return <Route component={NotFoundPage} />;
};

export default AdminOnlyRoute;
