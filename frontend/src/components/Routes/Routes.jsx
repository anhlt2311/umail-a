import React from 'react';
import PropTypes from 'prop-types';
import {
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import LoginPage from '~/components/Pages/LoginPage/LoginPage';
import PasswordResetPage from '~/components/Pages/PasswordResetPage/PasswordResetPage';
import PasswordChangePage from '~/components/Pages/PasswordChangePage/PasswordChangePage';
import DashboardPage from '~/components/Pages/DashboardPage/DashboardPage';
import MyProfilePage from '~/components/Pages/MyProfilePage/MyProfilePage';
import MyCompanyPage from '~/components/Pages/MyCompanyPage';
import UserSearchPage from '~/components/Pages/UserSearchPage';
import UserRegisterPage from '~/components/Pages/UserRegisterPage';
import UserInvitePage from '~/components/Pages/UserInvitePage';
import UserEditPage from '~/components/Pages/UserEditPage';
import SharedEmailPage from '~/components/Pages/SharedEmailPage';
import SharedEmailDetailPage from '~/components/Pages/SharedEmailDetailPage/SharedEmailDetailPage';
import SharedEmailNotificationSearchPage from '~/components/Pages/SharedEmailNotificationSearchPage';
import SharedEmailNotificationDetailPage from '~/components/Pages/SharedEmailNotificationDetailPage';
import SharedEmailNotificationRegisterPage from '~/components/Pages/SharedEmailNotificationRegisterPage';
import OrganizationSearchPage from '~/components/Pages/OrganizationSearchPage/OrganizationSearchPage';
import OrganizationRegisterPage from '~/components/Pages/OrganizationRegisterPage';
import OrganizationEditPage from '~/components/Pages/OrganizationEditPage';
import { OrganizationCsvUploadPage } from '~/components/Pages/OrganizationCsvUploadPage';
import ContactSearchPage from '~/components/Pages/ContactSearchPage/ContactSearchPage';
import ContactRegisterPage from '~/components/Pages/ContactRegisterPage';
import ContactEditPage from '~/components/Pages/ContactEditPage';
import ContactEmailPreferenceEditPage from '~/components/Pages/ContactMailPreferenceEditPage';
import { ContactCsvUploadPage } from '~/components/Pages/ContactCsvUploadPage';
import ScheduledEmailSearchPage from '~/components/Pages/ScheduledEmailSearchPage';
import { ScheduledEmailRegisterPage, ScheduledEmailEditPage, ScheduledEmailAttachementPage } from '~/components/Pages/ScheduledEmailPage';
import TagPage from '~/components/Pages/TagPage/TagPage';
import TagRegisterPage from '~/components/Pages/TagRegisterPage/TagRegisterPage';
import TagEditPage from '~/components/Pages/TagEditPage/TagEditPage';
import NotFoundPage from '~/components/Pages/NotFoundPage';
import Paths, { Suffixes } from './Paths';
import RequiredDisplaySettingPage from '~/components/Pages/RequiredDisplaySettingPage';
import SharedEmailSettingPage from '~/components/Pages/SharedEmailSettingPage';
import ScheduledEmailSettingPage from '~/components/Pages/ScheduledEmailSettingPage/ScheduledEmailSettingPage';
import AddonsPage from '~/components/Pages/AddonsPage/AddonsPage';
import PurchaseHistoryPage from '~/components/Pages/PurchaseHistoryPage/PurchaseHistoryPage';
import PaymentPage from '~/components/Pages/PaymentPage/PaymentPage';
import PlanPage from '~/components/Pages/PlanPage/PlanPage';
import MasterOnlyRoute from './MasterOnlyRoute/MasterOnlyRoute';
import PrivateRoute from './PrivateRoute/PrivateRoute';
import AdminOnlyRoute from './AdminOnlyRoute/AdminOnlyRoute';
import TenantCreatePage from '~/components/Pages/TenantCreatePage/TenantCreatePage';
import StandardPlanOnlyRoute from './StandardPlanOnlyRoute/StandardPlanOnlyRoute';
import PersonnelBoardPage from '../Pages/PersonnelBoardPage/PersonnelBoardPage';
import ProjectBoardPage from '../Pages/ProjectBoardPage/ProjectBoardPage';
import ScheduledEmailDownloadAttachmentPage from '../Pages/ScheduledEmailDownloadAttachmentPage/ScheduledEmailDownloadAttachmentPage';

const Pages = (props) => {
  const { isLoggedIn, isAdminUser } = props;

  return (
    <Switch>
      <Route exact path={Paths.index}>
        {isLoggedIn ? <Redirect push to={Paths.dashboard} /> : <Redirect push to={Paths.login} />}
      </Route>
      <Route exact path={Paths.maintenance}>
        {isLoggedIn ? <Redirect push to={Paths.dashboard} /> : <Redirect push to={Paths.login} />}
      </Route>
      <Route exact path={Paths.login} component={LoginPage} />
      <Route isLoggedIn={isLoggedIn} isAdminUser={isAdminUser} exact path={Paths.passwordReset} component={PasswordResetPage} />
      <Route isLoggedIn={isLoggedIn} isAdminUser={isAdminUser} exact path={`${Paths.passwordChange}/:id`} component={PasswordChangePage} />
      <Route isLoggedIn={isLoggedIn} isAdminUser={isAdminUser} exact path={`${Paths.userRegister}/:id`} component={UserRegisterPage} />
      <Route exact path="/tenant" component={TenantCreatePage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.dashboard} component={DashboardPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.userInvite} component={UserInvitePage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={`${Paths.users}/:id`} component={UserEditPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.myProfile} component={MyProfilePage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.myCompany} component={MyCompanyPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.contacts} component={ContactSearchPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.contactRegister} component={ContactRegisterPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.contactsCsvUpload} component={ContactCsvUploadPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={`${Paths.contacts}/:id`} component={ContactEditPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={`${Paths.contacts}/:id/${Suffixes.mailProfile}`} component={ContactEmailPreferenceEditPage} />
      <StandardPlanOnlyRoute exact path={Paths.sharedMails} component={SharedEmailPage} />
      <StandardPlanOnlyRoute exact path={`${Paths.sharedMails}/:id`} component={SharedEmailDetailPage} />
      <StandardPlanOnlyRoute exact path={Paths.sharedMailNotifications} component={SharedEmailNotificationSearchPage} />
      <StandardPlanOnlyRoute exact path={Paths.sharedMailNotificationRegister} component={SharedEmailNotificationRegisterPage} />
      <StandardPlanOnlyRoute exact path={`${Paths.sharedMailNotifications}/:id`} component={SharedEmailNotificationDetailPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.organizations} component={OrganizationSearchPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.organizationsRegister} component={OrganizationRegisterPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.organizationsCsvUpload} component={OrganizationCsvUploadPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={`${Paths.organizations}/:id`} component={OrganizationEditPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.scheduledMails} component={ScheduledEmailSearchPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.scheduledMailsRegister} component={ScheduledEmailRegisterPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={`${Paths.scheduledMails}/:id/attachement`} component={ScheduledEmailAttachementPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={`${Paths.scheduledMails}/:id`} component={ScheduledEmailEditPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.tags} component={TagPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.tagRegister} component={TagRegisterPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={`${Paths.tags}/:id`} component={TagEditPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.displaySettings} component={RequiredDisplaySettingPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.personnel} component={PersonnelBoardPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.project} component={ProjectBoardPage} />
      <StandardPlanOnlyRoute exact path={Paths.sharedEmailSettings} component={SharedEmailSettingPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.scheduledEmailSettings} component={ScheduledEmailSettingPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.addons} component={AddonsPage} />
      <PrivateRoute isLoggedIn={isLoggedIn} exact path={Paths.plan} component={PlanPage} />
      <Route exact path={`${Paths.scheduledMailsDownloadAttachment}/:id`} component={ScheduledEmailDownloadAttachmentPage} />
      <AdminOnlyRoute exact path={Paths.users} component={UserSearchPage} />
      <MasterOnlyRoute exact path={Paths.purchaseHistory} component={PurchaseHistoryPage} />
      <MasterOnlyRoute exact path={Paths.payment} component={PaymentPage} />
      <Route component={NotFoundPage} />
    </Switch>
  );
};

Pages.propTypes = {
  isLoggedIn: PropTypes.bool.isRequired,
  isAdminUser: PropTypes.bool.isRequired,
};

export default Pages;
