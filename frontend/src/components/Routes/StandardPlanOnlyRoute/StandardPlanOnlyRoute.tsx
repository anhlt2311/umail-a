import React from "react";
import { useSelector } from "react-redux";
import { Route, RouteProps } from "react-router-dom";
import NotFoundPage from "~/components/Pages/NotFoundPage";
import { usePlanSummaryAPIQuery } from "~/hooks/usePlan";
import { RootState } from "~/models/store";
import PrivateRoute from "../PrivateRoute/PrivateRoute";
import Loading from "~/components/Common/Loading/Loading";

type Props = RouteProps & {
    path: string;
};

const StandardPlanOnlyRoute = ({ ...props }: Props) => {
    const token = useSelector((state: RootState) => state.login.token);

    const { data, isLoading } = usePlanSummaryAPIQuery({});
    const isStandardPlan = (data?.planId ?? 0) >= 2;

    if (isLoading) {
        return <Loading isLoading={isLoading} />;
    }

    if (!isStandardPlan) {
        return <Route component={NotFoundPage} />;
    }

    return <PrivateRoute isLoggedIn={!!token} {...props} />;
};

export default StandardPlanOnlyRoute;
