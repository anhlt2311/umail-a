import React from 'react';
import PropTypes from 'prop-types';

import { List } from 'antd';
import DownloadLink from './DownloadLink';

const FileList = (props) => {
  const { Items, resourceBaseUrl } = props;
  return (
    <List
      size="small"
      dataSource={Items}
      renderItem={item => (
        <DownloadLink
          resourceBaseUrl={resourceBaseUrl}
          resourceId={item.id}
          fileName={item.name}
        />
      )}
    />
  );
};

FileList.propTypes = {
  Items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  })),
  resourceBaseUrl: PropTypes.string.isRequired,
};

FileList.defaultProps = {
  Items: [],
};

export default FileList;
