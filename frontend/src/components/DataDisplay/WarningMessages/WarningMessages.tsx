import React from "react";
import { Card, Col, Row, Typography } from "antd";
import { WarningFilled } from "@ant-design/icons";
import styles from "./WarningMessages.scss";
import { warningColor } from "~/utils/constants";

type Props = {
    messages?: string[];
};

const WarningMessages = ({ messages }: Props) => {
    return (
        <Col span={24} style={{ marginTop: "3%", marginBottom: "3%" }}>
            <Row justify="center">
                <Col span={24}>
                    <Card>
                        <Col span={24}>
                            <Row>
                                <Col span={4}>
                                    <WarningFilled
                                        style={{
                                            fontSize: "3rem",
                                            color: warningColor,
                                        }}
                                    />
                                </Col>
                                <Col span={20}>
                                    <Typography>
                                        <Typography.Paragraph>
                                            <ul
                                                style={{
                                                    listStyleType: "disc",
                                                    textAlign: "left",
                                                }}>
                                                {messages?.map((message) => (
                                                    <li
                                                        style={{
                                                            marginBottom: "5px",
                                                        }}>
                                                        {message}
                                                    </li>
                                                ))}
                                            </ul>
                                        </Typography.Paragraph>
                                    </Typography>
                                </Col>
                            </Row>
                        </Col>
                    </Card>
                </Col>
            </Row>
        </Col>
    );
};

export default WarningMessages;
