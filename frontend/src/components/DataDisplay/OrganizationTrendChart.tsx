import { ChartOptions } from "chart.js";
import React from "react";
import { ChartData, Line } from "react-chartjs-2";
import { GraphDataModel } from "~/models/graphModel";
import { CommonGraphDataSet } from "~/utils/constants";
import GraphCard from "../Pages/DashboardPage/GraphCard/GraphCard";

type Props = {
    data: GraphDataModel;
};

const OrganizationTrendChart = ({ data }: Props) => {
    const options: ChartOptions = {
        scales: {
            yAxes: [
                {
                    id: "sum",
                    type: "linear",
                    position: "left",
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
    };

    const getDataProps = (data: GraphDataModel): ChartData<any> => {
        const graphData = {
            labels: data.labels,
            datasets: [
                {
                    ...CommonGraphDataSet,
                    label: "新規登録数",
                    backgroundColor: "rgba(96,192,96,0.4)",
                    borderColor: "rgba(96,192,96,1)",
                    pointBorderColor: "rgba(96,192,96,1)",
                    pointHoverBackgroundColor: "rgba(96,192,96,1)",
                    data: data.values,
                    yAxisID: "sum",
                },
            ],
        };

        return graphData;
    };

    return (
        <GraphCard
            title="取引先の新規登録数"
            data={getDataProps(data)}
            options={options}
        />
    );
};

export default OrganizationTrendChart;
