import React from "react";
import { Tooltip, Button } from "antd";
import { InfoCircleTwoTone, DeleteFilled } from "@ant-design/icons";
import { warningColor } from "~/utils/constants";
import styles from "./SharedEmailSearchInfo.scss";

const SharedEmailSearchInfo = () => {
    return (
        <>
            <Tooltip
                placement="topRight"
                title="ブロックリストに設定されているメールは表示されません"
                color={warningColor}>
                <InfoCircleTwoTone twoToneColor={warningColor} />
            </Tooltip>
        </>
    );
};

export default SharedEmailSearchInfo;
