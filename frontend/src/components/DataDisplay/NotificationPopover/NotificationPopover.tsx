import React, { useEffect, useState } from "react";
import {
    Col,
    PageHeader,
    Row,
    Tabs,
    Typography,
    Switch,
    List,
    Button,
    Spin,
    Result, 
    Empty
} from "antd";
import InfiniteScroll from "react-infinite-scroll-component";
import { useSystemNotificationsAPI } from "~/hooks/useSystemNotificationsAPI";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "~/models/store";
import { SystemNotificationModel } from "~/models/notificationModel";
import {
    LOADING,
    RESET_COUNT,
    SYSTEM_NOTIFICATIONS,
} from "~/actions/actionTypes";
import moment from "moment";
import queryString from "query-string";
import { SYSTEM_NOTIFICATION_LAST_FETCHED_TIMESTAMP } from "~/utils/constants";
import { SystemNotificationInitialStateModel } from "~/reducers/notificationReducer";
import styles from "./NotificationPopover.scss";
import LinkIcon from "~/components/Common/LinkIcon/LinkIcon";
import { Links } from "~/utils/constants";

const { TabPane } = Tabs;
const { Text, Title, Link } = Typography;

type TabModel = "actions" | "systemNotifications";

const NotificationPopover = () => {
    const dispatch = useDispatch();
    const { fetchSystemNotifications, readNotifications } =
        useSystemNotificationsAPI();
    const {
        isLoading,
        systemNotifications,
        nextUrl,
    }: SystemNotificationInitialStateModel = useSelector(
        (state: RootState) => state.systemNotificationReducer
    );
    const { userId } = useSelector((state: RootState) => state.login);
    const [unread, setUnread] = useState(false);
    const [tab, setTab] = useState<TabModel>("systemNotifications");

    const getNextPage = (): number => {
        if (!nextUrl) {
            return 0;
        }
        const parsedNextUrl = queryString.parseUrl(nextUrl);
        const nextPageQuery = parsedNextUrl.query.page;
        if (nextPageQuery && +nextPageQuery) {
            const nextPage = +nextPageQuery;
            return nextPage;
        }
        return 0;
    };

    const fetchSystemNotificationsFromAPI = async (
        currentPage: number,
        unread = false
    ) => {
        dispatch({ type: SYSTEM_NOTIFICATIONS + LOADING });
        fetchSystemNotifications({
            currentPage,
            unread,
        });
    };

    const readNotificationsToAPI = async (ids: string[] | number[]) => {
        try {
            readNotifications(ids);
        } catch (err) {
            console.error(err);
        }
    };

    const onTabChange = (activeKey: TabModel) => {
        setTab(activeKey);
        if (activeKey === "actions") {
        } else if (activeKey === "systemNotifications") {
            // TODO(joshua-hashimoto): アクションを追加した場合にここが必要
            // fetchSystemNotificationsFromAPI(1, unread);
        }
    };

    const onUnreadSwitch = (checked: boolean) => {
        setUnread(checked);
        if (tab === "systemNotifications") {
            fetchSystemNotificationsFromAPI(1, checked);
        }
    };

    const renderUnreadSwitch = () => {
        return (
            <Row align="middle" justify="end" gutter={4}>
                <Col>
                    <Text>未読のみ表示</Text>
                </Col>
                <Col>
                    <Switch
                        size="small"
                        onChange={(checked) => onUnreadSwitch(checked)}
                    />
                </Col>
            </Row>
        );
    };

    const renderListHeader = () => {
        const unreadSystemNotifications = systemNotifications
            .filter((systemNotification) => !systemNotification.is_checked)
            .map((systemNotification) => systemNotification.id);
        return (
            <Col span={24}>
                <Row justify="space-between">
                    <Col style={{marginTop: 7}}>
                        <Link
                            href={Links.services.topics}
                            target="_blank"
                            className={styles.linkIcon}
                        >
                            <LinkIcon />
                            全てのお知らせ
                        </Link>
                    </Col>
                    <Col>
                        <Button
                            type="text"
                            onClick={() => {
                                if (unreadSystemNotifications.length) {
                                    readNotificationsToAPI(
                                        unreadSystemNotifications
                                    );
                                }
                            }}>
                            <Text type="secondary" style={{ fontSize: 12 }}>
                                全て既読
                            </Text>
                        </Button>
                    </Col>
                </Row>
            </Col>
        );
    };

    const renderSystemNotificationListItem = (
        item: SystemNotificationModel
    ) => {
        return (
            <List.Item
                extra={[
                    item.is_checked ? undefined : (
                        <span key="unread-badge" className={styles.unreadBadge}>
                            ●
                        </span>
                    ),
                ]}>
                <Row gutter={6}>
                    <Col>
                        <Link
                            onClick={() => {
                                if (!item.is_checked) {
                                    readNotificationsToAPI([item.id]);
                                }
                            }}
                            target="_blank"
                            rel="noopener noreferrer"
                            href={item.url}>
                            {item.title}
                        </Link>
                    </Col>
                    <Col>
                        <Text type="secondary" style={{ fontSize: 12 }}>
                            {moment(item.release_time).fromNow()}
                        </Text>
                    </Col>
                </Row>
            </List.Item>
        );
    };

    useEffect(() => {
        dispatch({ type: SYSTEM_NOTIFICATIONS + RESET_COUNT });
        localStorage.setItem(
            `${SYSTEM_NOTIFICATION_LAST_FETCHED_TIMESTAMP}_${userId}`,
            moment().toLocaleString()
        );
    }, []);

    return (
        <div
            style={{
                width: 300,
                height: 400,
            }}>
            <PageHeader
                title={<Title level={4}>通知</Title>}
                extra={[
                    <React.Fragment key="unread-switch">
                        {renderUnreadSwitch()}
                    </React.Fragment>,
                ]}
                style={{
                    height: 75,
                    paddingLeft: 0,
                    marginLeft: 0,
                }}
            />
            <Tabs
                defaultActiveKey={tab}
                onChange={(activeKey) => onTabChange(activeKey as TabModel)}>
                {/** NOTE(joshua-hashimoto): アクションはリリース後の対応。念の為コードを残しておく */}
                {/* <TabPane tab="アクション" key="actions"></TabPane> */}
                <TabPane
                    tab="お知らせ"
                    key="systemNotifications"
                    style={{ marginBottom: 0, paddingBottom: 0 }}>
                    <Spin spinning={isLoading}>
                        <div
                            id="infinite-scroll-div"
                            style={{
                                height: 250,
                                width: 300,
                                overflow: "auto",
                            }}>
                            <InfiniteScroll
                                scrollableTarget="infinite-scroll-div"
                                dataLength={systemNotifications.length}
                                next={() => {
                                    const nextPage = getNextPage();
                                    if (nextPage) {
                                        fetchSystemNotificationsFromAPI(
                                            nextPage,
                                            unread
                                        );
                                    }
                                }}
                                loader={<Spin spinning={isLoading} />}
                                hasMore={!!getNextPage()}
                                scrollThreshold={0.9}>
                                {systemNotifications && systemNotifications.length ? (
                                    <List
                                        style={{ marginTop: 0, paddingTop: 0 }}
                                        header={renderListHeader()}
                                        dataSource={systemNotifications}
                                        renderItem={(item) => {
                                            return (
                                                <React.Fragment key={item.id}>
                                                    {renderSystemNotificationListItem(
                                                        item
                                                    )}
                                                </React.Fragment>
                                            );
                                        }}
                                    />
                                    ) : (
                                        <Result
                                            icon={<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={false} />}
                                            subTitle="データがありません"
                                        />
                                    )
                                }
                            </InfiniteScroll>
                        </div>
                    </Spin>
                </TabPane>
            </Tabs>
        </div>
    );
};

export default NotificationPopover;
