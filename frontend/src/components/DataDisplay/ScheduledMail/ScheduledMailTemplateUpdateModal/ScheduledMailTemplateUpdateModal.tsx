import React, { MutableRefObject } from "react";
import {
  ScheduledMailTemplateFormModel
} from "~/models/scheduledEmailModel";
import ScheduledMailTemplateFormModal from "../ScheduledMailTemplateFormModal/ScheduledMailTemplateFormModal";
import {
  FormInstance
} from "antd";

type Props = {
    currentUserId: string;
    initialData: ScheduledMailTemplateFormModel;
    isOpen: boolean;
    onOk: (values: ScheduledMailTemplateFormModel) => void;
    onCancel: () => void;
    formRef: MutableRefObject<FormInstance>;
};

const ScheduledMailTemplateUpdateModal = ({ ...props }: Props) => {
    return <ScheduledMailTemplateFormModal titleModal="テンプレート更新" {...props} />;
};

export default ScheduledMailTemplateUpdateModal;
