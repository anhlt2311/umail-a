import React, { MutableRefObject, useEffect, useState } from "react";
import { Button, Form, Input, Col, Row, Tooltip, Radio, Typography, Switch, FormInstance } from "antd";
import GenericModal from "~/components/Modals/GenericModal/GenericModal";
import { TooltipContentLink } from "~/components/Common/TooltipContentLink/TooltipContentLink";
import Path from "~/components/Routes/Paths";
import AjaxSelect from "~/components/Forms/ajax/AjaxSelect";
import { Link } from "react-router-dom";
import { usePlanSummaryAPIQuery } from "~/hooks/usePlan";
import { ErrorMessages, Links, ONLY_HANKAKU_REGEX, iconCustomColor, SCHEDULED_EMAIL_TEXT_FORMAT } from "~/utils/constants";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import {
  QuestionCircleFilled,
  CheckOutlined,
  CloseOutlined,
  InfoCircleTwoTone,
} from "@ant-design/icons";
import { Endpoint } from "~/domain/api";
import validateJapaneseMessages from "~/components/Forms/validateMessages";
import {
  ScheduledMailTemplateFormModel
} from "~/models/scheduledEmailModel";
import styles from "./ScheduledMailTemplateFormModal.scss";
import ValidateTextArea from "~/components/Common/ValidateTextArea/ValidateTextArea";

const { TextArea } = Input;
const { Text } = Typography;

const emptyInitialData: ScheduledMailTemplateFormModel = {
  sender: undefined,
  sender__name: undefined,
  subject: undefined,
  title: undefined,
  text_format: "text",
  text: undefined,
  send_copy_to_sender: false,
  send_copy_to_share: false,
};

const labelColSpanSm = 8;
const labelColSpanXl = 7;

const formItemLayout = {
  labelCol: {
      sm: { span: labelColSpanSm },
      xl: { span: labelColSpanXl },
  },
  wrapperCol: {
      sm: { span: 24 - labelColSpanSm },
      xl: { span: 24 - labelColSpanXl },
  },
};

type Props = {
    currentUserId: string;
    titleModal: string;
    initialData?: ScheduledMailTemplateFormModel;
    isOpen: boolean;
    onOk: (values: ScheduledMailTemplateFormModel) => void;
    onCancel: () => void;
    fieldErrors?: {
      sender?: string[];
      subject?: string[];
      title?: string[];
      text_format?: "text" | "html";
      text?: string[];
      send_copy_to_sender?: string[];
      send_copy_to_share?: string[];
    };
    formRef: MutableRefObject<FormInstance>;
};

const ScheduledMailTemplateFormModal = ({
    currentUserId,
    titleModal,
    initialData = emptyInitialData,
    isOpen,
    fieldErrors,
    formRef,
    onOk,
    onCancel,
}: Props) => {
    const newTitleFieldName = "title";
    const textFormats = SCHEDULED_EMAIL_TEXT_FORMAT;
    
    const requiredFields = ["title", "text", "subject", "sender"];

    const [form] = Form.useForm<ScheduledMailTemplateFormModel>();
    const { select_sender: selectSenderAuthorized } =
    useAuthorizedActions("scheduled_mails");
    const [isHTMLFormat, setIsHTMLFormat] = useState(false);
    const [sendCopyToSenderVisible, setSendCopyToSenderVisible] =
        useState(initialData.send_copy_to_sender);
    const [isSendCopyToShareChecked, setIsSendCopyToShareChecked] =
        useState(initialData.send_copy_to_share);
    const [contentTextArea, setContentTextArea] = useState<string | undefined>(undefined);
    const [isCheckErrorTextArea, setIsCheckErrorTextArea] = useState<boolean | undefined>(false);

    const { data } = usePlanSummaryAPIQuery({});
    const sharedEmailsAuthorized = (data?.planId ?? 0) >= 2;

    useEffect(() => {
        if (isOpen) {
            form.setFieldsValue(initialData);
            setSendCopyToSenderVisible(!!initialData?.send_copy_to_sender);
            setIsSendCopyToShareChecked(!!initialData?.send_copy_to_share);
            setIsHTMLFormat(initialData.text_format === "html");
            setContentTextArea(initialData.text);
            if(currentUserId) {
              form.setFieldsValue({sender: currentUserId});
            }
        }
    }, [isOpen]);

    const handleSwitchsendCopyToSender = async (
      checked: boolean,
  ) => {
      try {
          const values = await formRef.current.validateFields();
          values["send_copy_to_sender"] = checked;
      } catch (err) {
          console.error(err);
      }
      setSendCopyToSenderVisible(!sendCopyToSenderVisible);
  };

  const handleSwitchSendCopyToShare = async (
    checked: boolean
  ) => {
      try {
          const values = await formRef.current.validateFields();
          values["send_copy_to_share"] = checked;
      } catch (err) {
          console.error(err);
      }
      setIsSendCopyToShareChecked(!isSendCopyToShareChecked);
  };

  const onFormValuesChange = (changedValues: any, allValues: any) => {
    setIsHTMLFormat(allValues.text_format === "html");
};

    return (
        <GenericModal
            title={titleModal}
            visible={isOpen}
            zIndex={200}
            width={775}
            closable={false}
            afterClose={() => {
                form.resetFields();
            }}
            >
          <div
            className={styles.container}>
            <Col span={24}>
                <Form form={form} onFinish={onOk}
                  ref={formRef}
                  validateMessages={validateJapaneseMessages}
                  labelAlign="right"
                  onValuesChange={onFormValuesChange}>
                    <Form.Item noStyle>
                        <Form.Item
                            {...formItemLayout}
                            label="テンプレート名"
                            className={styles.field}
                            validateStatus={fieldErrors?.title ? "error" : undefined}
                            help={fieldErrors?.title}
                            name={newTitleFieldName}
                            rules={[
                                {
                                    required: true,
                                    message:
                                        ErrorMessages.commentTemplate
                                            .titleEmpty,
                                },
                                {
                                    max: 50,
                                    message:
                                        ErrorMessages.validation.length.max50,
                                },
                            ]}>
                            <Input placeholder="タイトル" autoFocus/>
                        </Form.Item>
                      <Form.Item
                      {...formItemLayout}
                      label={
                      <span>
                        フォーマット&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    HTMLを選択した場合、改行タグ（br）は自動挿入されます。
                                    <br />
                                    <br />
                                    また、HTMLで配信をすることで開封情報を取得することが可能です。
                                    <br />
                                    ※
                                    <TooltipContentLink
                                        to={`${Path.addons}`}
                                        title="アドオン"
                                    />
                                    を購入する必要があります。
                                    <br />
                                    <a
                                        href={
                                            Links.helps.addon.scheduledEmails
                                                .htmlOpenInfo
                                        }
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </span>
                }
                className={styles.field}
                name="text_format"
                required={true}>
                <Radio.Group>
                {textFormats.map((textFormat) => {
                  return (
                    <Radio  key={textFormat.value} value={textFormat.value}>{textFormat.title}</Radio>
                  );
                })}
                </Radio.Group>
                    </Form.Item>
            <Form.Item
                {...formItemLayout}
                label={
                    <span>
                        配信者&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    無効化されているユーザーは選択表示されません。
                                    <br />
                                    <a
                                        href={Links.helps.users.activeToggle}
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </span>
                }
                className={styles.field}
                help={fieldErrors?.sender}
                name="sender"
                rules={[{ required: true }]}>
                <AjaxSelect
                    pageSize={1000}
                    resourceUrl={`${Endpoint.getBaseUrl()}/${
                        Endpoint.users
                    }?is_active=true`}
                    displayKey="display_name"
                    disabled={!selectSenderAuthorized}
                    searchParam="full_name"
                    defaultSelect={
                        initialData &&
                        initialData.sender__name &&
                        initialData.sender
                            ? {
                                  displayText: initialData.sender__name,
                                  foreignKey: initialData.sender,
                              }
                            : undefined
                    }
                />
            </Form.Item>
            <Form.Item
                {...formItemLayout}
                label="件名"
                className={styles.field}
                validateStatus={fieldErrors?.subject ? "error" : undefined}
                help={fieldErrors?.subject}
                name="subject"
                rules={[
                    { required: true },
                    {
                        max: 100,
                        message: ErrorMessages.validation.length.max100,
                    },
                ]}>
                <Input />
            </Form.Item>
            <Form.Item
                {...formItemLayout}
                label={
                    <span>
                        挿入文&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    宛先の取引先名と取引先担当者名、配信者の署名は自動挿入されます。
                                    <br />
                                    <a
                                        href={
                                            Links.helps.scheduledEmails
                                                .autoInsert
                                        }
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </span>
                }
                className={contentTextArea ? styles.contentField + ' validate-textarea' : styles.contentField}
                validateStatus={fieldErrors?.subject ? "error" : undefined}
                help={fieldErrors?.text}
                name="text"
                rules={[
                    { required: true },
                    {
                        validator: (_, value) => {
                            const errorMessage =
                                "全角5000文字または半角10000文字以内で入力してください。";
                            if (ONLY_HANKAKU_REGEX.test(value)) {
                                if (!!value && value.length > 10000) {
                                    setIsCheckErrorTextArea(true)
                                    return Promise.reject(
                                        new Error(errorMessage)
                                    );
                                }
                                setIsCheckErrorTextArea(false)
                                return Promise.resolve();
                            }
                            if (!!value && value.length > 5000) {
                                setIsCheckErrorTextArea(true)
                                return Promise.reject(new Error(errorMessage));
                            }
                            setIsCheckErrorTextArea(false)
                            return Promise.resolve();
                        },
                    },
                ]}>
                <TextArea autoSize={{ minRows: 4 }} rows={8} 
                        onChange={(inputValue) => {
                        const commentValue:string =
                            inputValue.target.value;
                        setContentTextArea(commentValue);
                    }}/>
            </Form.Item>
            { contentTextArea?.length ? 
                (<Form.Item>
                    <Row>
                        <Col {...formItemLayout.labelCol}></Col>
                        <ValidateTextArea contentTextArea={contentTextArea} errorMessages={ErrorMessages.scheduledEmail.maxsizeTextArea} margin="0px" className="validate-textarea-error ant-col ant-col-sm-16 ant-col-xl-17" isScheduledMails={true} isCheckErrorTextArea={isCheckErrorTextArea}/>
                    </Row>
                </Form.Item>
                ) : (
                <></>
                )
            }
            {isHTMLFormat && (
                <Form.Item
                    {...formItemLayout}
                    label=" "
                    colon={false}
                    style={{ margin: 0, padding: 0 }}>
                    <Col span={24}>
                        <Row justify="space-between" align="middle">
                            <Col>
                                <Row gutter={3}>
                                    <Col>
                                        <InfoCircleTwoTone />
                                    </Col>
                                    <Col>
                                        <Text>
                                            改行タグ（br）は自動で挿入されます。
                                            <a
                                                href={
                                                    Links.helps.scheduledEmails
                                                        .propertyDetail
                                                }
                                                target="_blank"
                                                rel="noopener noreferrer">
                                                詳細
                                            </a>
                                        </Text>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <Button
                                    style={{ marginTop: "2%" }}
                                    href={
                                        Links.helps.scheduledEmails.writableHTML
                                    }
                                    target="_blank">
                                    記入ヘルプ
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Form.Item>
            )}
            <Form.Item
                {...formItemLayout}
                label="控えを配信者に送信"
                className={styles.field}
                help={fieldErrors?.send_copy_to_sender}
                name="send_copy_to_sender"
                >
                <Switch
                    checkedChildren={<CheckOutlined />}
                    unCheckedChildren={<CloseOutlined />}
                    onChange={handleSwitchsendCopyToSender}
                    checked={sendCopyToSenderVisible}
                />
            </Form.Item>
            {sharedEmailsAuthorized ? (
                <Form.Item
                    {...formItemLayout}
                    label={
                        <span>
                            控えを共有メールに送信&nbsp;
                            <Tooltip
                                title={
                                    <span>
                                        チェックを入れると
                                        <Link to={`${Path.sharedMails}`}>
                                            共有メール一覧
                                        </Link>
                                        に、控えが送信されます。
                                    </span>
                                }>
                                <QuestionCircleFilled style={{ color: iconCustomColor }} />
                            </Tooltip>
                        </span>
                    }
                    className={styles.field}
                    help={fieldErrors?.send_copy_to_share}
                    name="send_copy_to_share"
                    >
                    <Switch
                        checkedChildren={<CheckOutlined />}
                        unCheckedChildren={<CloseOutlined />}
                        onChange={handleSwitchSendCopyToShare}
                        checked={isSendCopyToShareChecked}
                    />
                </Form.Item>
            ) : (
                <div />
            )}
                </Form.Item>
                <Form.Item noStyle>
                    <Col span={24}>
                        <Row justify="end" gutter={6}>
                            <Col>
                                <Button type="default" onClick={onCancel}>
                                    キャンセル
                                </Button>
                            </Col>
                            <Col>
                                <Form.Item shouldUpdate noStyle>
                                    {() => (
                                        <Button
                                            htmlType="submit"
                                            type="primary"
                                            disabled={
                                              !form.isFieldsTouched(
                                                requiredFields,
                                                true
                                              ) ||
                                              !!form
                                                  .getFieldsError()
                                                  .filter(
                                                      ({ errors }) =>
                                                          errors.length
                                                  ).length
                                              }
                                             >
                                            OK
                                        </Button>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                </Form.Item>
            </Form>
            </Col>
            </div>
        </GenericModal>
    );
};

export default ScheduledMailTemplateFormModal;
