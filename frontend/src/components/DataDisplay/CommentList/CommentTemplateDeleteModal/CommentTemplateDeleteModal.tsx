import React, { MouseEvent } from "react";
import { Modal } from "antd";
import styles from "./CommentTemplateDeleteModal.scss";

type Props = {
    isOpen: boolean;
    onOk: (event: MouseEvent<HTMLElement>) => void;
    onCancel: (event: MouseEvent<HTMLElement>) => void;
};

const CommentTemplateDeleteModal = ({ isOpen, onOk, onCancel }: Props) => {
    return (
        <Modal
            title="テンプレート削除"
            visible={isOpen}
            closable={false}
            onOk={onOk}
            onCancel={onCancel}
            cancelText="キャンセル"
            zIndex={200}>
            <p>このテンプレートを削除してもよろしいですか？</p>
        </Modal>
    );
};

export default CommentTemplateDeleteModal;
