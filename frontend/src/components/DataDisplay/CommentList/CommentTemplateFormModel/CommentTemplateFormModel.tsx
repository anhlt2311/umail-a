import React, { MouseEvent, ChangeEventHandler, useEffect } from "react";
import { Button, Form, Modal, Input, Col, Row } from "antd";
import GenericModal from "~/components/Modals/GenericModal/GenericModal";
import { ErrorMessages } from "~/utils/constants";

const { TextArea } = Input;

export type CommentTemplateFormModel = {
    newTitle: string;
    newContent: string;
};

const emptyInitialData: CommentTemplateFormModel = {
    newTitle: "",
    newContent: "",
};

type Props = {
    title: string;
    initialData?: CommentTemplateFormModel;
    isOpen: boolean;
    onOk: (values: CommentTemplateFormModel) => void;
    onCancel: () => void;
};

const CommentTemplateFormModal = ({
    title,
    initialData = emptyInitialData,
    isOpen,
    onOk,
    onCancel,
}: Props) => {
    const newTitleFieldName = "newTitle";
    const newContentFieldName = "newContent";
    const [form] = Form.useForm<CommentTemplateFormModel>();

    useEffect(() => {
        if (initialData && initialData.newContent && initialData.newTitle && form) {
            form.setFieldsValue(initialData);
        }
    }, [initialData]);

    return (
        <GenericModal
            title={title}
            visible={isOpen}
            zIndex={200}
            destroyOnClose
            closable={false}
            afterClose={() => {
                form.resetFields();
            }}>
            <Col span={24}>
                <Form form={form} onFinish={onOk}>
                    <Form.Item noStyle>
                        <Form.Item
                            name={newTitleFieldName}
                            rules={[
                                {
                                    required: true,
                                    message:
                                        ErrorMessages.commentTemplate
                                            .titleEmpty,
                                },
                                {
                                    max: 50,
                                    message:
                                        ErrorMessages.validation.length.max50,
                                },
                            ]}>
                            <Input placeholder="タイトル" autoFocus/>
                        </Form.Item>
                        <Form.Item
                            name={newContentFieldName}
                            rules={[
                                {
                                    required: true,
                                    message:
                                        ErrorMessages.commentTemplate
                                            .contentEmpty,
                                },
                                {
                                    max: 500,
                                    message:
                                        ErrorMessages.validation.length.max500,
                                },
                            ]}>
                            <TextArea placeholder="本文" rows={5} />
                        </Form.Item>
                    </Form.Item>
                    <Form.Item noStyle>
                        <Col span={24}>
                            <Row justify="end" gutter={6}>
                                <Col>
                                    <Button type="default" onClick={onCancel}>
                                        キャンセル
                                    </Button>
                                </Col>
                                <Col>
                                    <Form.Item shouldUpdate noStyle>
                                        {() => (
                                            <Button
                                                htmlType="submit"
                                                type="primary"
                                                disabled={
                                                    !form.getFieldValue(
                                                        newTitleFieldName
                                                    ) ||
                                                    !form.getFieldValue(
                                                        newContentFieldName
                                                    ) ||
                                                    !!form
                                                        .getFieldsError()
                                                        .filter(
                                                            ({ errors }) =>
                                                                errors.length
                                                        ).length
                                                }>
                                                OK
                                            </Button>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Col>
                    </Form.Item>
                </Form>
            </Col>
        </GenericModal>
    );
};

export default CommentTemplateFormModal;
