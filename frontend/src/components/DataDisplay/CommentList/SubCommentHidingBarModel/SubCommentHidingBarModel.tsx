import React from "react";
import styles from "./SubCommentHidingBarModel.scss";
import { MAX_VISIBLE_AVATARS } from "~/utils/constants";
import ProfileAvatar from "~/components/Common/ProfileAvatar/ProfileAvatar";
import { SmileOutlined } from "@ant-design/icons";

type Props = {
    subCommentUsersAvatar: any[];
};

const SubCommentHidingBarModel = ({ subCommentUsersAvatar }: Props) => {

  const renderMaxAvatarSubComment = () => {
    return (
      <div className={styles.avatarList}>
          {
              subCommentUsersAvatar?.map((avatar: string, index: any) => (
                  index < MAX_VISIBLE_AVATARS
                  ? (
                      index === MAX_VISIBLE_AVATARS - 1
                      ? (<div className={styles.lastImageWrapper}>
                          {!!avatar ? <ProfileAvatar avatar={avatar}/> : <SmileOutlined className={styles.lastIconBlur} />}
                          <div className={styles.hiddenItemCount}>+{subCommentUsersAvatar?.length - MAX_VISIBLE_AVATARS}</div>
                      </div>)
                      : !!avatar ? <ProfileAvatar avatar={avatar}/> : <SmileOutlined className={styles.defaultAvatarIcon} />
                  )
                  : ""
              ))
          }
      </div>
    );
  }

  const renderAvatarSubComment = () => {
    return (
      <div className={styles.avatarList}>
          {
              subCommentUsersAvatar?.map((avatar: string) => (
                  !!avatar ? <ProfileAvatar avatar={avatar}/>
                  : <SmileOutlined className={styles.defaultAvatarIcon} />
              ))
          }
      </div>
    );
  }
    return (
        <div className={styles.hidingBarWrapper}>
            {
                Math.max(subCommentUsersAvatar?.length - MAX_VISIBLE_AVATARS, 0) > 0
                ? renderMaxAvatarSubComment()
                : renderAvatarSubComment()
            }
        </div>
    );
};

export default SubCommentHidingBarModel;
