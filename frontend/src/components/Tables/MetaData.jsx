import React from 'react';
import PropTypes from 'prop-types';
import { Descriptions } from 'antd';

const MetaData = (props) => {
  const {
    created_time: createdTime, modified_time: modifiedTime, created_user: createdUser, modified_user: modifiedUser,
  } = props;
  return (
    <Descriptions title="このデータについて" column={{ xs: 1, sm: 2 }}>
      <Descriptions.Item label="作成者">{createdUser}</Descriptions.Item>
      <Descriptions.Item label="作成日時">{createdTime}</Descriptions.Item>
      <Descriptions.Item label="更新者">{modifiedUser}</Descriptions.Item>
      <Descriptions.Item label="更新日時">{modifiedTime}</Descriptions.Item>
    </Descriptions>
  );
};

MetaData.propTypes = {
  created_time: PropTypes.string,
  modified_time: PropTypes.string,
  created_user: PropTypes.string,
  modified_user: PropTypes.string,
};

MetaData.defaultProps = {
  created_time: '',
  modified_time: '',
  created_user: '',
  modified_user: '',
};

export default MetaData;
