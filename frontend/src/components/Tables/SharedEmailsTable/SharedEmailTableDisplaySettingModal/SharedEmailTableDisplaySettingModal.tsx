import React, { useEffect, useState } from "react";
import { Form } from "antd";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import TableDisplaySettingModal from "~/components/Modals/TableDisplaySettingModal/TableDisplaySettingModal";
import { DISPLAY_SETTING_ITEMS } from "~/utils/constants";
import { useDisplaySettingAPI } from "~/hooks/useDisplaySettingAPI";
import {
    DisplaySettingAPIModel,
    DisplaySettingModel,
} from "~/models/displaySetting";
import _ from "lodash";
import styles from "./SharedEmailTableDisplaySettingModal.scss";

const SharedEmailTableDisplaySettingModal = () => {
    const fieldName = "shared_emails";
    const formId = "sharedEmailsTableDisplaySettingForm";

    const [form] = Form.useForm();
    const { displaySetting }: { displaySetting?: DisplaySettingAPIModel } =
        useSelector((state: RootState) => state.displaySettingPage);

    const [targetKeys, setTargetKeys] = useState<string[]>([]);
    const [selectedKeys, setSelectedKeys] = useState<string[]>([]);

    const { updateDisplaySetting } = useDisplaySettingAPI();

    const setUnshowFields = () => {
        const doesDisplaySettingExist =
            !_.isEmpty(displaySetting) &&
            !_.isEmpty(displaySetting?.shared_emails);
        const tableHiddenColumns =
            doesDisplaySettingExist && displaySetting?.shared_emails.table;
        if (tableHiddenColumns) {
            setTargetKeys(tableHiddenColumns);
        }
        if (
            doesDisplaySettingExist &&
            displaySetting?.shared_emails.page_size
        ) {
            form.setFieldsValue({
                content: {
                    shared_emails: {
                        page_size: displaySetting.shared_emails.page_size,
                    },
                },
            });
        }
    };

    const onUpdate = (values: Partial<DisplaySettingModel>) => {
        updateDisplaySetting(values);
    };

    const onAfterClose = () => {
        setUnshowFields();
        setSelectedKeys([]);
        form.setFieldsValue({
            content: {
                shared_emails: {
                    table: displaySetting?.shared_emails.table,
                    page_size: displaySetting?.shared_emails.page_size,
                },
            },
        });
    };

    useEffect(() => {
        setUnshowFields();
    }, [displaySetting]);

    return (
        <TableDisplaySettingModal
            fieldName={fieldName}
            formId={formId}
            form={form}
            onFinish={onUpdate}
            transferProps={{
                dataSource: DISPLAY_SETTING_ITEMS.shared_emails.table,
                targetKeys,
                selectedKeys,
                onChange: (nextTargetKeys) => {
                    setTargetKeys(nextTargetKeys);
                },
                onSelectChange: (sourceSelectedKeys, targetSelectedKeys) => {
                    setSelectedKeys([
                        ...sourceSelectedKeys,
                        ...targetSelectedKeys,
                    ]);
                },
            }}
            onAfterClose={onAfterClose}
        />
    );
};

export default SharedEmailTableDisplaySettingModal;
