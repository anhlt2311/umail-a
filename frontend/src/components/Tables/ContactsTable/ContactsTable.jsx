import React from "react";
import PropTypes from "prop-types";
import { Tag, Tooltip, Space, Skeleton } from "antd";
import { withRouter } from "react-router-dom";
import Path from "~/components/Routes/Paths";
import GenericTable from "~/components/Tables/GenericTable";
import TooltipCopy from "~/components/Common/TooltipCopy/TooltipCopy";
import { HeartTwoTone, FrownTwoTone } from "@ant-design/icons";
import { CONTACT_SEND_TYPE, WANTS_LOCATION } from "~/utils/constants";
import { useSelector } from "react-redux";
import ListCommentsPopover from "~/components/Common/ListCommentsPopover/ListCommentsPopover";
import { stringBreak } from "~/utils/utils";
import styles from "./ContactsTable.scss";

const columns = [
    {
        title: "取引先担当者名",
        dataIndex: "last_name",
        key: "last_name",
        width: 160,
        className: styles.tooltipCopy,
        render: (last_name, record, column_index) => {
            return (
                <span>
                    {record.display_name}{" "}
                    <ListCommentsPopover
                        comments={record.comments}
                        columnIndex={column_index}
                    />
                    <TooltipCopy copyContent={record.display_name} />
                </span>
            );
        },
    },
    {
        title: "所属取引先",
        key: "organization__name",
        dataIndex: "organization__name",
        width: 240,
    },
    {
        title: "メールアドレス(TO)",
        dataIndex: "email",
        key: "email",
        width: 280,
        className: styles.tooltipCopy,
        render: (email, record, column_index) => {
            return (
                <>
                    {email}
                    <TooltipCopy copyContent={email} />
                </>
            );
        },
    },
    {
        title: "メールアドレス(CC)",
        dataIndex: "cc_addresses__email",
        key: "cc_addresses__email",
        width: 280,
        className: styles.tooltipCopy,
        render: (cc_addresses__email, record, column_index) => {
            return (
                <>
                    {cc_addresses__email}
                    <TooltipCopy copyContent={cc_addresses__email} />
                </>
            );
        },
    },
    {
        title: "TEL",
        dataIndex: "tel1",
        key: "tel1",
        width: 140,
        className: styles.tooltipCopy,
        render: (tel1, record, column_index) => {
            return (
                <>
                    {record.tel1 && record.tel2 && record.tel3 && (
                        <>
                            {record.tel1 +
                                "-" +
                                record.tel2 +
                                "-" +
                                record.tel3}
                            <TooltipCopy
                                copyContent={
                                    record.tel1 +
                                    "-" +
                                    record.tel2 +
                                    "-" +
                                    record.tel3
                                }
                            />
                        </>
                    )}
                </>
            );
        },
    },
    {
        title: "役職",
        key: "position",
        dataIndex: "position",
        width: 150,
    },
    {
        title: "部署",
        key: "department",
        dataIndex: "department",
        width: 150,
    },
    {
        title: "自社担当者",
        key: "staff__last_name",
        dataIndex: "staff__last_name",
        sorter: true,
        width: 160,
    },
    {
        title: "最終訪問日",
        key: "last_visit",
        dataIndex: "last_visit",
        sorter: true,
        width: 160,
    },
    {
        title: "タグ",
        key: "tag_objects",
        dataIndex: "tag_objects",
        width: 250,
        render: (tag_objects, record, column_index) => {
            if (!record["tag_objects"] || record["tag_objects"].length == 0) {
                return <span />;
            }
            var content_list = tag_objects.map((tag, index) => {
                const breakNumber = 18;
                if (tag.value.length > breakNumber) {
                    const valueList = stringBreak(tag.value, breakNumber);
                    return (
                        <Tag
                            key={column_index + "_" + index}
                            color={tag.color}
                            style={{ marginTop: "5px" }}>
                            {valueList.map((val, idx) => {
                                if (idx + 1 === valueList.length) {
                                    return val;
                                }
                                return (
                                    <React.Fragment key={idx}>
                                        {val}
                                        <br />
                                    </React.Fragment>
                                );
                            })}
                        </Tag>
                    );
                }
                return (
                    <Tag
                        key={column_index + "_" + index}
                        color={tag.color}
                        style={{ marginTop: "5px" }}>
                        {tag.value}
                    </Tag>
                );
            });

            return <span>{content_list}</span>;
        },
    },
    {
        title: "相性",
        key: "category",
        dataIndex: "category",
        width: 70,
        render: (category, record) => {
            if (category == "heart") {
                return (
                    <Tooltip title="良い">
                        <span>
                            <HeartTwoTone
                                data-testid="contact-table-category-heart"
                                twoToneColor="#eb2f96"
                            />
                        </span>
                    </Tooltip>
                );
            } else if (category == "frown") {
                return (
                    <Tooltip title="悪い">
                        <span>
                            <FrownTwoTone data-testid="contact-table-category-frown" />
                        </span>
                    </Tooltip>
                );
            } else {
                return <span />;
            }
        },
    },
    {
        title: "配信種別",
        key: "contactjobtypepreferences",
        dataIndex: "contactjobtypepreferences",
        width: 130,
        render: (contactjobtypepreferences, record) => {
            const tags = CONTACT_SEND_TYPE;
            const job = tags.find((tag) => tag.value === "job");
            const personnel = tags.find((tag) => tag.value === "personnel");
            if (
                record["contactjobtypepreferences"] &&
                record["contactjobtypepreferences"].length > 0 &&
                record["contactpersonneltypepreferences"] &&
                record["contactpersonneltypepreferences"].length > 0
            ) {
                return (
                    <span>
                        <Tag color={job.color}>{`${job.title}`}</Tag>
                        <Tag color={personnel.color} style={{ marginTop: 4 }}>
                            {`${personnel.title}`}
                        </Tag>
                    </span>
                );
            } else if (
                record["contactjobtypepreferences"] &&
                record["contactjobtypepreferences"].length > 0 &&
                !(
                    record["contactpersonneltypepreferences"] &&
                    record["contactpersonneltypepreferences"].length > 0
                )
            ) {
                return <Tag color={job.color}>{`${job.title}`}</Tag>;
            } else if (
                !(
                    record["contactjobtypepreferences"] &&
                    record["contactjobtypepreferences"].length > 0
                ) &&
                record["contactpersonneltypepreferences"] &&
                record["contactpersonneltypepreferences"].length > 0
            ) {
                return (
                    <Tag color={personnel.color}>{`${personnel.title}`}</Tag>
                );
            }
        },
    },
    {
        title: "希望エリア",
        key: "wants_location_hokkaido_japan",
        width: 150,
        dataIndex: "contactpreference",
        render: (contactpreference, record, column_index) => {
            var location_list = WANTS_LOCATION.map((location, index) => {
                if (contactpreference[location.value]) {
                    return (
                        <Tag
                            style={{ marginTop: 4 }}
                            color={location.color}
                            key={"location_" + column_index + "_" + index}>
                            {location.title}
                        </Tag>
                    );
                }
            });

            return <span>{location_list}</span>;
        },
    },
    {
        title: "作成日時",
        dataIndex: "created_time",
        key: "created_time",
        sorter: true,
        width: 160,
    },
    {
        title: "更新日時",
        dataIndex: "modified_time",
        key: "modified_time",
        sorter: true,
        width: 160,
    },
];

const onMinify = {
    renderTitle: (item) => item.display_name,
    renderDescription: (item) => (
        <div>
            <p>{`メールアドレス: ${item.email ? item.email : "未登録"}`}</p>
            <p>{`所属取引先: ${
                item.organization__name ? item.organization__name : "未登録"
            }`}</p>
            <p>{`自社担当者: ${
                item.staff__name ? `${item.staff__name}` : "未登録"
            }`}</p>
        </div>
    ),
};

const contactTable = (props) => {
    const { history, loading } = props;
    const data = JSON.parse(JSON.stringify(props.data));
    const isDisplaySettingLoading = useSelector(
        (state) => state.displaySettingPage.isDisplaySettingLoading
    );
    const { displaySetting } = useSelector((state) => state.displaySettingPage);

    const onRowClick = (record) =>
        history.push(`${Path.contacts}/${record.id}`);

    data.forEach((e) => (e.cc_mails = e.cc_mails.join("\n")));

    return (
        <>
            {isDisplaySettingLoading ? (
                <Space className={styles.container}>
                    <Skeleton.Input
                        style={{ height: 200, width: 1200 }}
                        active
                        size="large"
                    />
                </Space>
            ) : (
                <GenericTable
                    {...props}
                    data={data}
                    columns={columns}
                    onMinify={onMinify}
                    onRowClick={onRowClick}
                    loading={loading}
                    withSelection={true}
                    pageSize={displaySetting?.contacts?.page_size ?? 10}
                />
            )}
        </>
    );
};

contactTable.propTypes = {
    currentPage: PropTypes.number.isRequired,
    pageSize: PropTypes.number.isRequired,
    totalCount: PropTypes.number.isRequired,
    sortKey: PropTypes.string,
    sortOrder: PropTypes.string,
    onPageChange: PropTypes.func.isRequired,
    onPageSizeChange: PropTypes.func.isRequired,
    onTableChange: PropTypes.func.isRequired,
    onCheckColumn: PropTypes.func.isRequired,
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
    }).isRequired,
    loading: PropTypes.bool,
    data: PropTypes.arrayOf(
        PropTypes.shape({
            key: PropTypes.number.isRequired,
            id: PropTypes.string.isRequired,
            display_name: PropTypes.string.isRequired,
            email: PropTypes.string.isRequired,
            cc_mails: PropTypes.arrayOf(PropTypes.string),
            tel: PropTypes.string,
            position: PropTypes.string,
            department: PropTypes.string,
            organization__name: PropTypes.string,
            staff__name: PropTypes.string,
            last_visit: PropTypes.string,
            score: PropTypes.number,
            created_time: PropTypes.string,
            modified_time: PropTypes.string,
            comments: PropTypes.arrayOf(
                PropTypes.shape({
                    created_user__name: PropTypes.string,
                    created_time: PropTypes.string,
                    content: PropTypes.string,
                })
            ),
        })
    ),
};

contactTable.defaultProps = {
    loading: false,
    data: [],
    sortKey: undefined,
    sortOrder: undefined,
};

export default withRouter(contactTable);
