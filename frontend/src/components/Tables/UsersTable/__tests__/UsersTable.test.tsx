import React from "react";
import { USER_SEARCH_PAGE } from "~/components/Pages/pageIds";
import { fixedUsersModelList } from "~/test/mock/userAPIMock";
import { renderWithAllProviders, screen } from "~/test/utils";
import login, { LoginInitialState } from "~/reducers/login";
import { displaySettingPage } from "~/reducers/pages";
import UsersTable from "../UsersTable";
import { configureStore } from "@reduxjs/toolkit";
import { ROLES } from "~/utils/constants";
import { generateRandomToken } from "~/utils/utils";

describe("UsersTable.jsx", () => {
    test("render test", () => {
        renderWithAllProviders(
            <UsersTable
                pageId={USER_SEARCH_PAGE}
                loading
                data={fixedUsersModelList}
            />,
            {
                store: configureStore({
                    reducer: {
                        login,
                        displaySettingPage,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            token: generateRandomToken(),
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                _editable_roles: [...ROLES],
                            },
                        },
                        displaySettingPage: {
                            canNotDelete: false,
                            loading: false,
                            updated: false,
                            deleted: false,
                            message: "",
                            errorMessage: "",
                            data: {},
                            fieldErrors: {},
                            displaySetting: undefined,
                            isDisplaySettingLoading: false,
                            initialData: {},
                        },
                    },
                }),
            }
        );
        const userNameColumnTitleElement = screen.getByText(/ユーザー名/);
        expect(userNameColumnTitleElement).toBeInTheDocument();
        const roleColumnTitleElement = screen.getByText(/権限/);
        expect(roleColumnTitleElement).toBeInTheDocument();
        const emailColumnTitleElement = screen.getByText(/メールアドレス/);
        expect(emailColumnTitleElement).toBeInTheDocument();
        const telColumnTitleElement = screen.getByText(/TEL/);
        expect(telColumnTitleElement).toBeInTheDocument();
        const lastLoginColumnTitleElement = screen.getByText(/最終ログイン/);
        expect(lastLoginColumnTitleElement).toBeInTheDocument();
        const tableRowElements = screen.getAllByRole("row");
        expect(tableRowElements.length).toBe(fixedUsersModelList.length);
        for (const data of fixedUsersModelList) {
            const userNameDataElement = screen.getByText(
                new RegExp(data.display_name)
            );
            expect(userNameDataElement).toBeInTheDocument();
            const roleDataElements = screen.getAllByText(
                new RegExp(ROLES.find((val) => val.value === data.role)?.title!)
            );
            expect(roleDataElements.length).toBeGreaterThanOrEqual(1);
            const emailDataElement = screen.getByText(new RegExp(data.email));
            expect(emailDataElement).toBeInTheDocument();
            if (data.tel1) {
                const telDataElement = screen.getByText(new RegExp(data.tel1));
                expect(telDataElement).toBeInTheDocument();
            }
            const lastLoginDataElement = screen.getByText(
                new RegExp(data.last_login)
            );
            expect(lastLoginDataElement).toBeInTheDocument();
        }
    });
});
