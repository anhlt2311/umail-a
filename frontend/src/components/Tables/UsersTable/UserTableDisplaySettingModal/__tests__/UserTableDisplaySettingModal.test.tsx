import React from "react";
import { configureStore } from "@reduxjs/toolkit";
import { renderWithAllProviders, screen, userEvent } from "~/test/utils";
import login, { LoginInitialState } from "~/reducers/login";
import { displaySettingPage } from "~/reducers/pages";
import { defaultInitialState as EditPageInitialState } from "~/reducers/Factories/editPage";
import UserTableDisplaySettingModal from "../UserTableDisplaySettingModal";
import { generateRandomToken } from "~/utils/utils";

describe("UserTableDisplaySettingModal.tsx", () => {
    test("render test", async () => {
        renderWithAllProviders(<UserTableDisplaySettingModal />, {
            store: configureStore({
                reducer: {
                    login,
                    displaySettingPage,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: generateRandomToken(),
                    },
                    displaySettingPage: {
                        ...EditPageInitialState,
                        isDisplaySettingLoading: false,
                    },
                },
            }),
        });
        const titleElement = screen.getByText(/検索結果表示項目設定/);
        expect(titleElement).toBeInTheDocument();
        const visibleLabelElement = screen.getByText(/^表示$/);
        expect(visibleLabelElement).toBeInTheDocument();
        const usernameTransferOptionElement = screen.getByText(/ユーザー名/);
        expect(usernameTransferOptionElement).toBeInTheDocument();
        const roleTransferOptionElement = screen.getByText(/権限/);
        expect(roleTransferOptionElement).toBeInTheDocument();
        const emailTransferOptionElement = screen.getByText(/メールアドレス/);
        expect(emailTransferOptionElement).toBeInTheDocument();
        const telTransferOptionElement = screen.getByText(/TEL/);
        expect(telTransferOptionElement).toBeInTheDocument();
        const lastLoginTransferOptionElement = screen.getByText(/最終ログイン/);
        expect(lastLoginTransferOptionElement).toBeInTheDocument();
        const invisibleLabelElement = screen.getByText(/^非表示$/);
        expect(invisibleLabelElement).toBeInTheDocument();
        const pageSizeLabelElement = screen.getByText(/表示件数/);
        expect(pageSizeLabelElement).toBeInTheDocument();
        const defaultPageSizeElement = screen.getByText(/10/);
        expect(defaultPageSizeElement).toBeInTheDocument();
        await userEvent.click(defaultPageSizeElement);
        const pageSizeOption10Element = screen.getByTestId(
            "table-display-setting-page-size-option-10"
        );
        expect(pageSizeOption10Element).toBeInTheDocument();
        const pageSizeOption50Element = screen.getByTestId(
            "table-display-setting-page-size-option-50"
        );
        expect(pageSizeOption50Element).toBeInTheDocument();
        const pageSizeOption100Element = screen.getByTestId(
            "table-display-setting-page-size-option-100"
        );
        expect(pageSizeOption100Element).toBeInTheDocument();
        const updateButtonElement = screen.getByText(/更 新/);
        expect(updateButtonElement).toBeInTheDocument();
        expect(updateButtonElement).not.toBeDisabled();
        const cancelButtonElement = screen.getByText(/キャンセル/);
        expect(cancelButtonElement).toBeInTheDocument();
        expect(cancelButtonElement).not.toBeDisabled();
    });
});
