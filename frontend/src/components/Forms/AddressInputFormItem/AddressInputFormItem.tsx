import React from "react";
import { Form, FormItemProps, Input } from "antd";
import { ErrorMessages, RESTRICT_SPACE_REGEX } from "~/utils/constants";
import styles from "./AddressInputFormItem.scss";
const { TextArea } = Input;

type Props = FormItemProps & {
    addressProps?: FormItemProps;
    buildingProps?: FormItemProps;
};

const AddressInputFormItem = ({
    addressProps,
    buildingProps,
    ...props
}: Props) => {
    return (
        <Form.Item label="住所" className={styles.field} {...props}>
            <Input.Group>
                <Form.Item
                    name="address"
                    style={{ marginBottom: 0 }}
                    {...addressProps}>
                    <TextArea autoSize={{minRows:1}} placeholder="市区町村・町名・番地" />
                </Form.Item>
                <Form.Item
                    name="building"
                    style={{ marginBottom: 0 }}
                    dependencies={["address"]}
                    {...buildingProps}
                    rules={[
                        ({ getFieldValue }) => ({
                            validator: (_, value) => {
                                const addressValue = getFieldValue("address");
                                const address = !!addressValue
                                    ? addressValue
                                    : null;
                                if (!RESTRICT_SPACE_REGEX.test(address)) {
                                    return Promise.reject(
                                        new Error(
                                            ErrorMessages.validation.regex.space
                                        )
                                    );
                                }

                                const building = !!value ? value : null;
                                if (!RESTRICT_SPACE_REGEX.test(building)) {
                                    return Promise.reject(
                                        new Error(
                                            ErrorMessages.validation.regex.space
                                        )
                                    );
                                }

                                if (
                                    ((address ?? "") + (building ?? ""))
                                        .length > 100
                                ) {
                                    return Promise.reject(
                                        new Error(
                                            "建物名を合わせて" +
                                                ErrorMessages.validation.length
                                                    .max100
                                        )
                                    );
                                }

                                return Promise.resolve();
                            },
                        }),
                        ...(buildingProps?.rules ?? []),
                    ]}>
                    <TextArea autoSize={{minRows:1}} placeholder="建物名" />
                </Form.Item>
            </Input.Group>
        </Form.Item>
    );
};

export default AddressInputFormItem;
