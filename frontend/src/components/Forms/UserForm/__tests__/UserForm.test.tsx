import React from "react";
import { renderWithAllProviders, screen } from "~/test/utils";
import UserForm from "../UserForm";
import { UserModel } from "~/models/userModel";
import { Button } from "antd";
import { showDeleteModal } from "~/components/Feedbacks/Modal/Modal";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { v4 as uuidv4 } from "uuid";

const emptyInitialData: UserModel = {
    email: "",
    email_signature: undefined,
    first_name: "",
    last_name: "",
    user_service_id: "",
    modified_time: "",
    modified_user: "",
    role: undefined,
    tel1: undefined,
    tel2: undefined,
    tel3: undefined,
    is_active: false,
};

const filledInitialData: UserModel = {
    id: uuidv4(),
    email: "example@example.com",
    email_signature: "===========\nThe admin\n===========",
    first_name: "user",
    last_name: "example",
    user_service_id: "user1",
    modified_time: "2022-03-30T11:24:20.851136+09:00",
    modified_user: "example@example.com",
    role: "admin",
    tel1: "001",
    tel2: "0002",
    tel3: "0003",
    password: "Abcd12345%",
    is_active: true,
};

const resourceURL = "";
const mockSubmitHandler = jest.fn();
const mockOnDelete = jest.fn(() => {
    showDeleteModal(() => {}, "userEditPage");
});

const DeleteButton = (
    <Button danger onClick={mockOnDelete} htmlType="button">
        削除
    </Button>
);

describe("UserForm.tsx", () => {
    const emailSignatureTextAreaTestId = "user-form-email-signature";
    const userServiceIdInputTestId = "user-service-id-input";

    describe("without data", () => {
        test("render test", async () => {
            const { container } = renderWithAllProviders(
                <UserForm
                    resourceURL={resourceURL}
                    fieldErrors={emptyInitialData}
                    submitHandler={mockSubmitHandler}
                    deleteButton={DeleteButton}
                />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    _editable_roles: ["admin"],
                                },
                            },
                        },
                    }),
                }
            );
            const requiredElements = container.getElementsByClassName(
                "ant-form-item-required"
            );
            expect(requiredElements.length).toBe(3);
            // expect(requiredElements.length).toBe(4);
            // const profileLabelElement = screen.getByText(/^プロフィール画像$/);
            // expect(profileLabelElement).toBeInTheDocument();
            const userNameLabelElement = screen.getByText("ユーザー名");
            expect(userNameLabelElement).toBeInTheDocument();
            const userLastNameElement = screen.getByPlaceholderText("姓");
            expect(userLastNameElement).toBeInTheDocument();
            expect(userLastNameElement).not.toHaveValue();
            const userFirstNameElement = screen.getByPlaceholderText("名");
            expect(userFirstNameElement).toBeInTheDocument();
            expect(userFirstNameElement).not.toHaveValue();
            // const userServiceIdLabelElement = screen.getByText("ユーザーID");
            // expect(userServiceIdLabelElement).toBeInTheDocument();
            // const userServiceIdInputElement = screen.getByTestId(
            //     userServiceIdInputTestId
            // );
            // expect(userServiceIdInputElement).toBeInTheDocument();
            // expect(userServiceIdInputElement).not.toHaveValue();
            const emailAddressLabelElement =
                screen.getByLabelText("メールアドレス");
            expect(emailAddressLabelElement).toBeInTheDocument();
            const emailAddressElement =
                screen.getByPlaceholderText(/^you@example.com$/);
            expect(emailAddressElement).toBeInTheDocument();
            expect(emailAddressElement).not.toHaveValue();
            const roleLabelElement = screen.getByText("権限");
            expect(roleLabelElement).toBeInTheDocument();
            const masterRadioElement = screen.getByRole("radio", {
                name: "マスター",
            });
            expect(masterRadioElement).toBeInTheDocument();
            expect(masterRadioElement).not.toBeChecked();
            const adminRadioElement = screen.getByRole("radio", {
                name: "管理者",
            });
            expect(adminRadioElement).toBeInTheDocument();
            expect(adminRadioElement).not.toBeChecked();
            const managerRadioElement = screen.getByRole("radio", {
                name: "責任者",
            });
            expect(managerRadioElement).toBeInTheDocument();
            expect(managerRadioElement).not.toBeChecked();
            const leaderRadioElement = screen.getByRole("radio", {
                name: "リーダー",
            });
            expect(leaderRadioElement).toBeInTheDocument();
            expect(leaderRadioElement).not.toBeChecked();
            const memberRadioElement = screen.getByRole("radio", {
                name: "メンバー",
            });
            expect(memberRadioElement).toBeInTheDocument();
            expect(memberRadioElement).not.toBeChecked;
            const guestRadioElement = screen.getByRole("radio", {
                name: "ゲスト",
            });
            expect(guestRadioElement).toBeInTheDocument();
            expect(guestRadioElement).not.toBeChecked();
            const telLabelElement = screen.getByText(/^TEL$/);
            expect(telLabelElement).toBeInTheDocument();
            const tel1InputElement = screen.getByTestId("tel1");
            expect(tel1InputElement).toBeInTheDocument();
            expect(tel1InputElement).not.toHaveValue();
            const tel2InputElement = screen.getByTestId("tel2");
            expect(tel2InputElement).toBeInTheDocument();
            expect(tel2InputElement).not.toHaveValue();
            const tel3InputElement = screen.getByTestId("tel3");
            expect(tel3InputElement).toBeInTheDocument();
            expect(tel3InputElement).not.toHaveValue();
            const passwordLabelElement = screen.getByText(/^パスワード$/);
            expect(passwordLabelElement).toBeInTheDocument();
            const passwordElement =
                screen.getByPlaceholderText("大小英数字記号混在で10-50桁");
            expect(passwordElement).toBeInTheDocument();
            expect(passwordElement).not.toHaveValue();
            const passwordHelpTextElement = screen.getByText(
                "空欄の場合、パスワードは変更されません"
            );
            expect(passwordHelpTextElement).toBeInTheDocument();
            const emailSignatureLabelElement =
                screen.getByLabelText("メール署名");
            expect(emailSignatureLabelElement).toBeInTheDocument();
            const emailSignatureElement = screen.getByTestId(
                emailSignatureTextAreaTestId
            );
            expect(emailSignatureElement).not.toHaveValue();
            const isActiveSwitchLabelElement = screen.getByText("有効");
            expect(isActiveSwitchLabelElement).toBeInTheDocument();
            const isActiveSwitchElement = screen.getByRole("switch");
            expect(isActiveSwitchElement).toBeInTheDocument();
            expect(isActiveSwitchElement).not.toBeChecked();
            const backButtonElement = screen.getByRole("button", {
                name: /^戻る$/,
            });
            expect(backButtonElement).toBeInTheDocument();
            expect(backButtonElement).not.toBeDisabled();
            // NOTE(joshua-hashimoto): initialData.idがない場合は「更新」ではなく「登録」になる
            const createButtonElement = screen.getByRole("button", {
                name: /^登 録$/,
            });
            expect(createButtonElement).toBeInTheDocument();
            // NOTE(joshua-hashimoto): フロントからはdisabledになることを確認。ここではなぜかエラーになってしまうので一旦コメントアウト
            // expect(createButtonElement).toBeDisabled();
        });
    });

    describe("with data", () => {
        test("render test", () => {
            const { container } = renderWithAllProviders(
                <UserForm
                    resourceURL={resourceURL}
                    initialData={filledInitialData}
                    fieldErrors={emptyInitialData}
                    submitHandler={mockSubmitHandler}
                    deleteButton={DeleteButton}
                />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    _editable_roles: ["admin"],
                                },
                            },
                        },
                    }),
                }
            );
            const requiredElements = container.getElementsByClassName(
                "ant-form-item-required"
            );
            expect(requiredElements.length).toBe(3);
            // expect(requiredElements.length).toBe(4);
            // const profileLabelElement = screen.getByText(/^プロフィール画像$/);
            // expect(profileLabelElement).toBeInTheDocument();
            const userNameLabelElement = screen.getByText("ユーザー名");
            expect(userNameLabelElement).toBeInTheDocument();
            const userLastNameElement = screen.getByPlaceholderText("姓");
            expect(userLastNameElement).toBeInTheDocument();
            expect(userLastNameElement).toHaveValue();
            const userFirstNameElement = screen.getByPlaceholderText("名");
            expect(userFirstNameElement).toBeInTheDocument();
            expect(userFirstNameElement).toHaveValue();
            // const userServiceIdLabelElement = screen.getByText("ユーザーID");
            // expect(userServiceIdLabelElement).toBeInTheDocument();
            // const userServiceIdInputElement = screen.getByTestId(
            //     userServiceIdInputTestId
            // );
            // expect(userServiceIdInputElement).toBeInTheDocument();
            // expect(userServiceIdInputElement).toHaveValue();
            const emailAddressLabelElement =
                screen.getByLabelText("メールアドレス");
            expect(emailAddressLabelElement).toBeInTheDocument();
            const emailAddressElement =
                screen.getByPlaceholderText(/^you@example.com$/);
            expect(emailAddressElement).toBeInTheDocument();
            expect(emailAddressElement).toHaveValue();
            const roleLabelElement = screen.getByText("権限");
            expect(roleLabelElement).toBeInTheDocument();
            const masterRadioElement = screen.getByRole("radio", {
                name: "マスター",
            });
            expect(masterRadioElement).toBeInTheDocument();
            expect(masterRadioElement).not.toBeChecked();
            const adminRadioElement = screen.getByRole("radio", {
                name: "管理者",
            });
            expect(adminRadioElement).toBeInTheDocument();
            expect(adminRadioElement).toBeChecked();
            const managerRadioElement = screen.getByRole("radio", {
                name: "責任者",
            });
            expect(managerRadioElement).toBeInTheDocument();
            expect(managerRadioElement).not.toBeChecked();
            const leaderRadioElement = screen.getByRole("radio", {
                name: "リーダー",
            });
            expect(leaderRadioElement).toBeInTheDocument();
            expect(leaderRadioElement).not.toBeChecked();
            const memberRadioElement = screen.getByRole("radio", {
                name: "メンバー",
            });
            expect(memberRadioElement).toBeInTheDocument();
            expect(memberRadioElement).not.toBeChecked;
            const guestRadioElement = screen.getByRole("radio", {
                name: "ゲスト",
            });
            expect(guestRadioElement).toBeInTheDocument();
            expect(guestRadioElement).not.toBeChecked();
            const telLabelElement = screen.getByText(/^TEL$/);
            expect(telLabelElement).toBeInTheDocument();
            const tel1InputElement = screen.getByTestId("tel1");
            expect(tel1InputElement).toBeInTheDocument();
            expect(tel1InputElement).toHaveValue();
            const tel2InputElement = screen.getByTestId("tel2");
            expect(tel2InputElement).toBeInTheDocument();
            expect(tel2InputElement).toHaveValue();
            const tel3InputElement = screen.getByTestId("tel3");
            expect(tel3InputElement).toBeInTheDocument();
            expect(tel3InputElement).toHaveValue();
            const passwordLabelElement = screen.getByText(/^パスワード$/);
            expect(passwordLabelElement).toBeInTheDocument();
            const passwordElement =
                screen.getByPlaceholderText("大小英数字記号混在で10-50桁");
            expect(passwordElement).toBeInTheDocument();
            expect(passwordElement).toHaveValue();
            const passwordHelpTextElement = screen.getByText(
                "空欄の場合、パスワードは変更されません"
            );
            expect(passwordHelpTextElement).toBeInTheDocument();
            const emailSignatureLabelElement =
                screen.getByLabelText("メール署名");
            expect(emailSignatureLabelElement).toBeInTheDocument();
            const emailSignatureElement = screen.getByTestId(
                emailSignatureTextAreaTestId
            );
            expect(emailSignatureElement).toHaveValue();
            const isActiveSwitchLabelElement = screen.getByText("有効");
            expect(isActiveSwitchLabelElement).toBeInTheDocument();
            const isActiveSwitchElement = screen.getByRole("switch");
            expect(isActiveSwitchElement).toBeInTheDocument();
            expect(isActiveSwitchElement).toBeChecked();
            const backButtonElement = screen.getByRole("button", {
                name: /^戻る$/,
            });
            expect(backButtonElement).toBeInTheDocument();
            expect(backButtonElement).not.toBeDisabled();
            const updateButtonElement = screen.getByRole("button", {
                name: /^更 新$/,
            });
            expect(updateButtonElement).toBeInTheDocument();
            expect(updateButtonElement).not.toBeDisabled();
        });
    });
});
