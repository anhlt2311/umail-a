import React from "react";
import login, { LoginInitialState } from "~/reducers/login";
import { displaySettingPage, userSearchPage } from "~/reducers/pages";
import { renderWithAllProviders, screen, userEvent } from "~/test/utils";
import UserSearchForm from "../UserSearchForm";
import { defaultInitialState as SearchPageInitialState } from "~/reducers/Factories/searchPage";
import { configureStore } from "@reduxjs/toolkit";
import { generateRandomToken } from "~/utils/utils";

describe("UserSearchForm.jsx", () => {
    test("render test", () => {
        renderWithAllProviders(<UserSearchForm />, {
            store: configureStore({
                reducer: {
                    login,
                    displaySettingPage,
                    userSearchPage,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: generateRandomToken(),
                    },
                    displaySettingPage: {
                        requireRefresh: false,
                        canNotDelete: false,
                        loading: false,
                        updated: false,
                        deleted: false,
                        message: "",
                        errorMessage: "",
                        data: {},
                        fieldErrors: {},
                        displaySetting: {
                            users: {
                                search: [],
                            },
                        },
                        isDisplaySettingLoading: false,
                        initialData: {},
                    },
                    userSearchPage: {
                        ...SearchPageInitialState,
                    },
                },
            }),
        });
        const searchButtonElement = screen.getByText(/絞り込み検索/);
        expect(searchButtonElement).toBeInTheDocument();
        expect(searchButtonElement).not.toBeDisabled();
        userEvent.click(searchButtonElement);
        const searchFieldShowAreaTitleElement = screen.getByText(/^検索対象$/);
        expect(searchFieldShowAreaTitleElement).toBeInTheDocument();
        const searchFieldUnshowAreaTitleElement =
            screen.getByText(/^検索対象外$/);
        expect(searchFieldUnshowAreaTitleElement).toBeInTheDocument();
        const drawerCancelButtonElement = screen.getByText(/キャンセル/);
        expect(drawerCancelButtonElement).toBeInTheDocument();
        expect(drawerCancelButtonElement).not.toBeDisabled();
    });
});
