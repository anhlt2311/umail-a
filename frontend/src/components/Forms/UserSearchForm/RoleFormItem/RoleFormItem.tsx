import React from "react";
import { Col, Form, Select, Row, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { ROLES, NOT_ROLES, Links, iconCustomColor } from "~/utils/constants";
import CustomSelectTag from "~/components/Common/CustomSelectTag/CustomSelectTag";
import styles from "./RoleFormItem.scss";

type Props = {
    disabled?: boolean;
};

const RoleFormItem = ({ disabled }: Props) => {
    const fieldName = "role";
    const statuses = [...ROLES, ...NOT_ROLES];

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Form.Item wrapperCol={{ span: 20 }} noStyle>
                <Row className={styles.container}>
                    <Col span={23}>
                        <Form.Item colon={false} name={fieldName} noStyle>
                            <Select
                                className={styles.container}
                                placeholder="権限"
                                mode="multiple"
                                allowClear
                                tagRender={(props) => {
                                    const status = statuses.find(
                                        (status) => status.value === props.value
                                    );
                                    return (
                                        <CustomSelectTag
                                            color={status?.color}
                                            title={status?.title}
                                            {...props}
                                        />
                                    );
                                }}
                                disabled={disabled}>
                                {statuses.map((item) => {
                                    return (
                                        <Select.Option
                                            key={item.value}
                                            value={item.value}>
                                            {item.title}
                                        </Select.Option>
                                    );
                                })}
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={1} className={styles.infoIcon}>
                        <Tooltip
                            title={
                                <span>
                                    複数選択をすると
                                    <a
                                        href={Links.helps.filter.or}
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        OR検索
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </Col>
                </Row>
            </Form.Item>
        </Col>
    );
};

export default RoleFormItem;
