import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import RoleFormItem from "../RoleFormItem";

describe("RoleFormItem.tsx", () => {
    test("render test", async () => {
        render(<RoleFormItem />);
        const roleSelectPlaceholderElement = screen.getByText(/権限/);
        expect(roleSelectPlaceholderElement).toBeInTheDocument();
        const roleSelectInputElement = screen.getByRole("combobox");
        expect(roleSelectInputElement).toBeInTheDocument();
        await userEvent.click(roleSelectInputElement);
        const masterSelectOptionElement = screen.getByText(/^マスター$/);
        expect(masterSelectOptionElement).toBeInTheDocument();
        const notMasterSelectOptionElement = screen.getByText(/^not:マスター$/);
        expect(notMasterSelectOptionElement).toBeInTheDocument();
        const adminSelectOptionElement = screen.getByText(/^管理者$/);
        expect(adminSelectOptionElement).toBeInTheDocument();
        const notAdminSelectOptionElement = screen.getByText(/^not:管理者$/);
        expect(notAdminSelectOptionElement).toBeInTheDocument();
        const managerSelectOptionElement = screen.getByText(/^責任者$/);
        expect(managerSelectOptionElement).toBeInTheDocument();
        const notManagerSelectOptionElement = screen.getByText(/^not:責任者$/);
        expect(notManagerSelectOptionElement).toBeInTheDocument();
        const leaderSelectOptionElement = screen.getByText(/^リーダー$/);
        expect(leaderSelectOptionElement).toBeInTheDocument();
        const notLeaderSelectOptionElement = screen.getByText(/^not:リーダー$/);
        expect(notLeaderSelectOptionElement).toBeInTheDocument();
        const memberSelectOptionElement = screen.getByText(/^メンバー$/);
        expect(memberSelectOptionElement).toBeInTheDocument();
        const notMemberSelectOptionElement = screen.getByText(/^not:メンバー$/);
        expect(notMemberSelectOptionElement).toBeInTheDocument();
        const guestSelectOptionElement = screen.getByText(/^ゲスト$/);
        expect(guestSelectOptionElement).toBeInTheDocument();
        const notGuestSelectOptionElement = screen.getByText(/^not:ゲスト$/);
        expect(notGuestSelectOptionElement).toBeInTheDocument();
    });
    test("disabled render test", () => {
        render(<RoleFormItem disabled />);
        const selectElement = screen.getByRole("combobox");
        expect(selectElement).toBeDisabled();
    });
});
