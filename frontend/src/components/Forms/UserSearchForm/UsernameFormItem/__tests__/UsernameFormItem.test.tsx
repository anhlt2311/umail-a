import React from "react";
import { render, screen } from "~/test/utils";
import UsernameFormItem from "../UsernameFormItem";

const mockOnClear = jest.fn();

describe("UsernameFormItem.tsx", () => {
    test("enable render test", () => {
        render(<UsernameFormItem onClear={mockOnClear} />);
        const lastNameInputElement =
            screen.getByPlaceholderText("ユーザー名(姓)");
        expect(lastNameInputElement).toBeInTheDocument();
        expect(lastNameInputElement).not.toBeDisabled();
        const firstNameInputElement =
            screen.getByPlaceholderText("ユーザー名(名)");
        expect(firstNameInputElement).toBeInTheDocument();
        expect(firstNameInputElement).not.toBeDisabled();
    });
    test("disable render test", () => {
        render(<UsernameFormItem disabled onClear={mockOnClear} />);
        const lastNameInputElement =
            screen.getByPlaceholderText("ユーザー名(姓)");
        expect(lastNameInputElement).toBeInTheDocument();
        expect(lastNameInputElement).toBeDisabled();
        const firstNameInputElement =
            screen.getByPlaceholderText("ユーザー名(名)");
        expect(firstNameInputElement).toBeInTheDocument();
        expect(firstNameInputElement).toBeDisabled();
    });
});
