import React from "react";
import { render, screen } from "~/test/utils";
import LastLoginFormItem from "../LastLoginFormItem";

describe("LastLoginFormItem.tsx", () => {
    test("enabled render test", () => {
        render(<LastLoginFormItem />);
        const startDateInputElement =
            screen.getByPlaceholderText("最終ログイン(開始)");
        expect(startDateInputElement).toBeInTheDocument();
        expect(startDateInputElement).not.toBeDisabled();
        const endDateInputElement =
            screen.getByPlaceholderText("最終ログイン(終了)");
        expect(endDateInputElement).toBeInTheDocument();
        expect(endDateInputElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(<LastLoginFormItem disabled />);
        const startDateInputElement =
            screen.getByPlaceholderText("最終ログイン(開始)");
        expect(startDateInputElement).toBeInTheDocument();
        expect(startDateInputElement).toBeDisabled();
        const endDateInputElement =
            screen.getByPlaceholderText("最終ログイン(終了)");
        expect(endDateInputElement).toBeInTheDocument();
        expect(endDateInputElement).toBeDisabled();
    });
});
