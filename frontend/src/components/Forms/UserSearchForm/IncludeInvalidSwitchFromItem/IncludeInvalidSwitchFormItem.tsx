import React from "react";
import { Col, Row, Form, Switch, Tooltip } from "antd";
import {
    QuestionCircleFilled,
    CheckOutlined,
    CloseOutlined,
} from "@ant-design/icons";
import { iconCustomColor } from "~/utils/constants";
import styles from "./IncludeInvalidSwitchFormItem.scss";

const IncludeInvalidSwitchFormItem = () => {
    const fieldName = "inactive_filter";

    return (
        <Col span={24}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Row gutter={6} justify="end">
                        <Col>
                            <label className={styles.switchLabel}>無効含</label>
                        </Col>
                        <Col>
                            <Form.Item
                                labelCol={{}}
                                wrapperCol={{}}
                                name={fieldName}
                                valuePropName="checked"
                                noStyle>
                                <Switch
                                    checkedChildren={<CheckOutlined />}
                                    unCheckedChildren={<CloseOutlined />}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip title="無効化されているユーザーを含めて表示させるには有効にしてください。">
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default IncludeInvalidSwitchFormItem;
