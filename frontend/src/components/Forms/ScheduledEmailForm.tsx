import React, { MutableRefObject, useEffect, useState } from "react";
import { Moment } from "moment";
import {
    Form,
    Input,
    Tooltip,
    Switch,
    Radio,
    FormInstance,
    Col,
    Row,
    Button,
    Typography,
    Empty,
    Result,
    Select,
} from "antd";
import AjaxSelect from "./ajax/AjaxSelect";
import { Endpoint } from "../../domain/api";
import {
    QuestionCircleFilled,
    CheckOutlined,
    CloseOutlined,
    InfoCircleTwoTone,
    SnippetsOutlined,
    SaveOutlined,
    DeleteOutlined
} from "@ant-design/icons";
import validateJapaneseMessages from "./validateMessages";
import Path from "../Routes/Paths";
import { Link } from "react-router-dom";
import { usePlanSummaryAPIQuery } from "~/hooks/usePlan";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { TooltipContentLink } from "~/components/Common/TooltipContentLink/TooltipContentLink";
import { ErrorMessages, Links, ONLY_HANKAKU_REGEX, iconCustomColor, InfoMessages } from "~/utils/constants";
import { ValidateErrorEntity } from "rc-field-form/lib/interface";
import ScheduledMailTemplateModal from "~/components/Modals/ScheduledMailTemplateModal/ScheduledMailTemplateModal";
import ScheduledMailTemplateCreateModal from "~/components/DataDisplay/ScheduledMail/ScheduledMailTemplateCreateModal/ScheduledMailTemplateCreateModal";
import ScheduledMailTemplateUpdateModal from "~/components/DataDisplay/ScheduledMail/ScheduledMailTemplateUpdateModal/ScheduledMailTemplateUpdateModal";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import {
  ScheduledMailTemplateFormModel,
  ScheduledMailTemplateModel
} from "~/models/scheduledEmailModel";
import styles from "./ScheduledEmailForm.scss";
import { useSelector } from "react-redux";
import { useForm } from "antd/lib/form/Form";
import ValidateTextArea from "~/components/Common/ValidateTextArea/ValidateTextArea";
import TemplateControlButton from "~/components/Forms/SearchTemplateSelectFormItem/TemplateControlButton/TemplateControlButton";
import SaveScheduledMailTemplateModal from "~/components/Feedbacks/Modal/SaveScheduledMailTemplateModal/SaveScheduledMailTemplateModal";
import { customSuccessMessage, customErrorMessage } from "~/components/Common/AlertMessage/AlertMessage";

const { TextArea } = Input;
const { Text } = Typography;

const labelColSpanSm = 8;
const labelColSpanXl = 7;

const formItemLayout = {
    labelCol: {
        sm: { span: labelColSpanSm },
        xl: { span: labelColSpanXl },
    },
    wrapperCol: {
        sm: { span: 24 - labelColSpanSm },
        xl: { span: 24 - labelColSpanXl },
    },
};

type Props = {
    currentUserId: string;
    resourceURL: string;
    initialData?: {
        // Corresponds to backend API.
        id?: string;
        sender?: string; // Foreign key
        sender__name?: string;
        subject?: string;
        text_format?: "text" | "html";
        text?: string;
        status?: string;
        date_to_send?: Moment;
        send_copy_to_sender?: boolean;
        send_copy_to_share?: boolean;
        sent_date?: string;
    }; // Override in child class and use shape instead.
    fieldErrors?: {
        name?: string[];
        sender?: string[];
        subject?: string[];
        text_format?: "text" | "html";
        text?: string[];
        status?: string[];
        date_to_send?: string[];
        send_copy_to_sender?: string[];
        send_copy_to_share?: string[];
    };
    submitHandler: (values: any) => void;
    handleFormChange: () => void;
    isEdit?: boolean;
    formRef: MutableRefObject<FormInstance>;
    templateRef: MutableRefObject<FormInstance>;
    dataTemplate: ScheduledMailTemplateModel;
    createTemplate: (values: any) => void;
    updateTemplate: (values: any, selectedIndex: number) => void;
    deleteTemplate: (selectedIndex: number) => void;
    disabledSubmit: boolean;
};

const ScheduledEmailForm = ({
    currentUserId,
    resourceURL,
    initialData = {},
    fieldErrors,
    submitHandler,
    isEdit,
    formRef,
    templateRef,
    handleFormChange,
    dataTemplate,
    createTemplate,
    updateTemplate,
    deleteTemplate,
    disabledSubmit
}: Props) => {
    const [additional_count, setAdditionalCount] = useState(0);
    const [form] = useForm();
    const [sendCopyToSenderVisible, setSendCopyToSenderVisible] =
        useState(false);
    const [errorFields, setErrorFields] = useState<any[]>([]);
    const [isSendCopyToShareChecked, setIsSendCopyToShareChecked] =
        useState(false);
    const [isHTMLFormat, setIsHTMLFormat] = useState(false);
    const [listVisible, setListVisible] = useState(false);
    const [registerVisible, setRegisterVisible] = useState(false);
    const [updateVisible, setUpdateVisible] = useState(false);
    const [selectedIndex, setSelectedIndex] = useState(0);
    const [dataUpdateTemplate, setDataUpdateTemplate] = useState({});
    const valuesPersonnel = useSelector((state: any) => state.scheduledEmailRegisterPage.valuesPersonnel)

    const [isCheckErrorTextArea, setIsCheckErrorTextArea] = useState<boolean | undefined>(false);
    const [isValidationError, setIsValidationError] = useState(false);
    const [messageTootipTemplateButton, setMessageTootipTemplateButton] = useState("");
    const [isAddable, setIsAddable] = useState(false);
    const [isSaveTemplateModalVisible, setIsSaveTemplateModalVisible] =
        useState(false);
    const { select_sender: selectSenderAuthorized } =
        useAuthorizedActions("scheduled_mails");
    
    const [selectedTemplate, setSelectedTemplate] =
    useState(-1);
    const [selectedTemplateName, setSelectedTemplateName] =
        useState(null);
    const [isValidateForm, setIsValidateForm] =
        useState(true);
    const [isDisableDeleteButton, setIsDisableDeleteButton] =
        useState(false);
    const [isStatusScheduleMail, setIsStatusScheduleMail] =
        useState('');

    const { data } = usePlanSummaryAPIQuery({});
    const sharedEmailsAuthorized = (data?.planId ?? 0) >= 2;
    const { create: createAuthorization, update: updateAuthorization } =
        useAuthorizedActions("scheduled_mails");


    const [contentTextArea, setContentTextArea] = useState<string | undefined>(undefined);
    const handleSwitchsendCopyToSender = async (
        checked: boolean,
        event: MouseEvent
    ) => {
        try {
            const values = await formRef.current.validateFields();
            values["send_copy_to_sender"] = checked;
        } catch (err) {
            console.error(err);
        }
        setSendCopyToSenderVisible(!sendCopyToSenderVisible);
    };

    const onSelectScheduledMailTemplate = () => {
      setListVisible(true);
    };

    const showRegister = () => {
      setRegisterVisible(true);
    }

    const templateValueToCommentForm = (index: number) => {
        let value_templates: ScheduledMailTemplateFormModel = {
            sender: "",
            subject: "",
            title: "",
            text_format: "text",
            text: "",
            send_copy_to_sender: false,
            send_copy_to_share: false
        }
        dataTemplate?.templates?.map((template: any, indexT: number) => {
            if (indexT === index) {
              value_templates = template;
            }
        });
        if(!!value_templates && formRef && formRef.current) {
          formRef.current.setFieldsValue(value_templates);
          setSendCopyToSenderVisible(!!value_templates?.send_copy_to_sender);
          setIsSendCopyToShareChecked(!!value_templates?.send_copy_to_share);
          setIsHTMLFormat(value_templates.text_format === "html");
          handleFormChange();
        }
    }

    const showUpdate = (index: number) => {
      setUpdateVisible(true);
      setSelectedIndex(index);
      setDataUpdateTemplate(dataTemplate?.templates[index]);
    }

    const showDelete = (index: number) => {
      confirmModal({
          title: "このテンプレートを削除しますか？",
          content: (
              <div>
                  <p>OKを押すと、削除が実行されます。</p>
                  <p>元には戻せません。</p>
              </div>
          ),
          onOk: () => deleteOnOk(index),
          onCancel: deleteOnCancel,
      });
    }

    const deleteOnOk = (index: number) => {
      deleteTemplate(index);
    }

    const deleteOnCancel = () => {
      setSelectedIndex(0);
    }

    const registerOnOk = (dataTemplate: ScheduledMailTemplateFormModel) => {
      setRegisterVisible(false);
      createTemplate(dataTemplate);
    }

    const registerOnCancel = () => {
      setRegisterVisible(false);
    }

    const updateOnOk = (dataTemplate: ScheduledMailTemplateFormModel) => {
      setUpdateVisible(false);
      updateTemplate(dataTemplate, selectedIndex);
    }

    const updateOnCancel = () => {
      setUpdateVisible(false);
    }

    const handleSwitchSendCopyToShare = async (
        checked: boolean,
        event: MouseEvent
    ) => {
        try {
            const values = await formRef.current.validateFields();
            values["send_copy_to_share"] = checked;
        } catch (err) {
            console.error(err);
        }
        setIsSendCopyToShareChecked(!isSendCopyToShareChecked);
    };

    const handleSubmit = async () => {
        try {
            const values = await formRef.current.validateFields();
            submitHandler(values);
        } catch (err) {
            console.error(err);
        }
    };

    const handleSubmitError = ({ errorFields }: ValidateErrorEntity<any>) => {
        if (errorFields) {
            let errorFieldNames = errorFields.map((field) => {
                return field["name"][0];
            });
            setErrorFields(errorFieldNames);
        }
    };

    const onFormValuesChange = (changedValues: any, allValues: any) => {
        setIsHTMLFormat(allValues.text_format === "html");
    };

    useEffect(() => {
        if (initialData && initialData.status && formRef && formRef.current) {
            formRef.current.setFieldsValue(initialData);
            setSendCopyToSenderVisible(!!initialData.send_copy_to_sender);
            setIsSendCopyToShareChecked(!!initialData.send_copy_to_share);
            setIsHTMLFormat(initialData.text_format === "html");
        }
    }, [initialData]);

    useEffect(() => {
        formRef.current.setFieldsValue(valuesPersonnel);
        setSendCopyToSenderVisible(valuesPersonnel.send_copy_to_sender);
        setIsSendCopyToShareChecked(valuesPersonnel.send_copy_to_share);
    }, [valuesPersonnel])
    
    useEffect(()=>{
        dataTemplate &&
        dataTemplate.templates &&
        dataTemplate.total_available_count &&
        dataTemplate.total_available_count >
        dataTemplate.templates.length ? setIsAddable(true) : setIsAddable(false);
        if (!createAuthorization) {
            setMessageTootipTemplateButton(ErrorMessages.isNotAuthorized);
        }
        if (!isAddable) {
            setMessageTootipTemplateButton(InfoMessages.scheduledEmails.messageTootipTemplateButtonDelete);
        }
    }, [dataTemplate]);

    useEffect(() => {
      const errorFields =
                formRef.current &&
                !!formRef.current
                    .getFieldsError()
                    .filter(({ errors }) => errors.length).length;
      errorFields ? setIsValidateForm(false) : setIsValidateForm(true);
      onValidateForm();
    }, [formRef.current]);

    const checkRole = isStatusScheduleMail === 'sending' || isStatusScheduleMail === 'sent' || isStatusScheduleMail === 'error';

    const isCheckRole = () => {
        if(checkRole || !createAuthorization){
            return true;
        }
    }

    const messageTooltipSelect = () => {
        if(!createAuthorization){
            return ErrorMessages.isNotAuthorized;
        }else if(checkRole){
            return InfoMessages.scheduledEmails.messageTootipTemplateButtonSavedSendingAndSentAndErorrStatus;
        }else{
            return messageTootipTemplateButton;
        }
    }

    const authorizedControls = [
        {
            message: messageTooltipSelect(),
            icon: (
                <DeleteOutlined data-testid="delete-search-template-button-icon" />
            ),
            isDanger: true,
            disabled: selectedTemplateName === null || isCheckRole() ? true : false,
            onClick: () => {
                confirmModal({
                    title: "このテンプレートを削除しますか？",
                    content: (
                        <>
                            <p>OKを押すと、削除が実行されます。</p>
                            <p>元には戻せません。</p>
                        </>
                    ),
                    onOk: () => {
                        deleteOnOk(selectedTemplate);
                    },
                });
            },
        },
        {
            message: !createAuthorization ? ErrorMessages.isNotAuthorized : InfoMessages.scheduledEmails.messageTootipTemplateButtonSaved,
            icon: (
                <SaveOutlined data-testid="save-search-template-button-icon" />
            ),
            isDanger: false,
            disabled: !isAddable ||
                isValidateForm ||
                disabledSubmit ||
                (!createAuthorization || isCheckErrorTextArea ? true : false),
            onClick: () => setIsSaveTemplateModalVisible(true),
        },
    ];

    const onCancel = () => {
        setIsSaveTemplateModalVisible(false);
    };

    const onOk = async (newTemplateName:any) => {
        try {
            const values = await formRef.current.validateFields();
            const title = newTemplateName.newTemplateName;
            values.title = title;
            let index = dataTemplate?.templates?.findIndex((item:any) => item.title === title);
            if(index >= 0){
                customErrorMessage(`${title} と同一名称のテンプレートが既に存在します。別のテンプレート名を入力してください。`);
            }else{
                setSelectedTemplate(dataTemplate?.templates?.length);
                createTemplate(values);
                setIsSaveTemplateModalVisible(false);
                setSelectedTemplateName(title);
            }
        } catch (err) {
            console.error(err);
        }
    };

    const onValidateForm = async () => {
      const text = await formRef.current.getFieldValue('text');
      const subject = await formRef.current.getFieldValue('subject');
      const sender = await formRef.current.getFieldValue('sender');
      const text_format = await formRef.current.getFieldValue('text_format');
      if(text && subject && sender && text_format){
        setIsValidateForm(false);
      }else{
        setIsValidateForm(true);
      }
    }

    return (
        <Form
            onFinish={handleSubmit}
            onFinishFailed={handleSubmitError}
            className={styles.container}
            ref={formRef}
            validateMessages={validateJapaneseMessages}
            onFieldsChange={handleFormChange}
            onValuesChange={onFormValuesChange}>
                       <Row justify="end" align="middle">
                <Col>
                    {isCheckRole() ?
                    <Form.Item style={{marginBottom: 0}} colon={false}
                        label={
                        <Tooltip title={messageTooltipSelect()}>
                            <Select
                            placeholder="基本情報テンプレートを選択"
                            notFoundContent={<Result
                                icon={<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="データがありません"/>}
                            />}
                            onChange={(value:any) => {
                                setSelectedTemplate(value);
                                if (value === undefined) {
                                    setSelectedTemplateName(null)
                                    return;
                                }else{
                                    templateValueToCommentForm(value);
                                }
                            }}
                            onSelect={(value:any) => {
                                const template = dataTemplate?.templates?.find(
                                    (template:any, index:number) => index === value
                                );
                                template.title ? setSelectedTemplateName(template.title) : setSelectedTemplateName(null);
                            }}
                            allowClear
                            value={selectedTemplateName}
                            style={{ textAlign: "left", width: 300 }}
                            disabled={true}>
                            {dataTemplate?.templates?.map((entry:any, index:any) => {
                                return (
                                    <Select.Option
                                        key={index}
                                        onChange={ () => showUpdate(index)}
                                        value={index}>
                                        {entry.title}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                        </Tooltip>
                    }>
                    </Form.Item>
                    :
                    <Form.Item style={{marginBottom: 0, width: 300}}>
                        <Select
                            className={styles.selectContainer}
                            placeholder="基本情報テンプレートを選択"
                            notFoundContent={<Result
                                icon={<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="データがありません"/>}
                            />}
                            onChange={(value:any) => {
                                setSelectedTemplate(value);
                                if (value === undefined) {
                                    setSelectedTemplateName(null)
                                    return;
                                }else{
                                    templateValueToCommentForm(value);
                                }
                            }}
                            onSelect={(value:any) => {
                                const template = dataTemplate?.templates?.find(
                                    (template:any, index:number) => index === value
                                );
                                template.title ? setSelectedTemplateName(template.title) : setSelectedTemplateName(null);
                            }}
                            allowClear
                            value={selectedTemplateName}
                            style={{ textAlign: "left" }}
                            disabled={false}>
                            {dataTemplate?.templates?.map((entry:any, index:any) => {
                                return (
                                    <Select.Option
                                        key={index}
                                        onChange={ () => showUpdate(index)}
                                        value={index}>
                                        {entry.title}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                }
                </Col>
                <Col>
                    <>
                        {authorizedControls.map((control, index) => {
                            return (
                                <TemplateControlButton
                                    key={index}
                                    message={control.message}
                                    icon={control.icon}
                                    isDanger={control.isDanger}
                                    onClick={control.onClick}
                                    disabled={control.disabled}
                                />
                            );
                        })}
                        <SaveScheduledMailTemplateModal
                            templateReducerName='organizationSearchPage'
                            isSaveTemplateModalVisible={
                                isSaveTemplateModalVisible
                            }
                            dataTemplate={dataTemplate}
                            onCancel={onCancel}
                            onOk={onOk}
                        />
                        <Tooltip title="テンプレートはユーザー個人ごとに保存され、他のユーザーとは共有されません。">
                            <QuestionCircleFilled
                                style={{ color: iconCustomColor }}
                                className={styles.tooltip}
                            />
                        </Tooltip>
                    </>
                </Col>
            </Row>
            <Form.Item
                {...formItemLayout}
                wrapperCol={{ span: 9 }}
                label={
                    <span>
                        フォーマット&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    HTMLを選択した場合、改行タグ（br）は自動挿入されます。
                                    <br />
                                    <br />
                                    また、HTMLで配信をすることで開封情報を取得することが可能です。
                                    <br />
                                    ※
                                    <TooltipContentLink
                                        to={`${Path.addons}`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        title="アドオン"
                                    />
                                    を購入する必要があります。
                                    <br />
                                    <a
                                        href={
                                            Links.helps.addon.scheduledEmails
                                                .htmlOpenInfo
                                        }
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </span>
                }
                className={styles.field}
                name="text_format"
                initialValue={"text"}
                required={true}>
                <Radio.Group>
                    <Radio value="text">テキスト</Radio>
                    <Radio value="html">HTML</Radio>
                </Radio.Group>
            </Form.Item>
            <Form.Item
                {...formItemLayout}
                wrapperCol={{ span: 9 }}
                label={
                    <span>
                        配信者&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    無効化されているユーザーは選択表示されません。
                                    <br />
                                    <a
                                        href={Links.helps.users.activeToggle}
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </span>
                }
                className={styles.field}
                help={fieldErrors?.sender}
                name="sender"
                initialValue={
                    initialData.sender ? initialData.sender : currentUserId
                }
                rules={[{ required: true }]}>
                <AjaxSelect
                    pageSize={1000}
                    resourceUrl={`${Endpoint.getBaseUrl()}/${
                        Endpoint.users
                    }?is_active=true`}
                    displayKey="display_name"
                    disabled={!selectSenderAuthorized}
                    searchParam="full_name"
                    defaultSelect={
                        initialData &&
                        initialData.sender__name &&
                        initialData.sender
                            ? {
                                  displayText: initialData.sender__name,
                                  foreignKey: initialData.sender,
                              }
                            : undefined
                    }
                />
            </Form.Item>
            <Form.Item
                {...formItemLayout}
                label="件名"
                className={styles.field}
                validateStatus={fieldErrors?.subject ? "error" : undefined}
                help={fieldErrors?.subject}
                name="subject"
                initialValue={initialData.subject}
                rules={[
                    { required: true },
                    {
                        max: 100,
                        message: ErrorMessages.validation.length.max100,
                    },
                ]}>
                <Input />
            </Form.Item>
            <Form.Item
                {...formItemLayout}
                label={
                    <span>
                        挿入文&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    宛先の取引先名と取引先担当者名、配信者の署名は自動挿入されます。
                                    <br />
                                    <a
                                        href={
                                            Links.helps.scheduledEmails
                                                .autoInsert
                                        }
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </span>
                }
                className={contentTextArea ? styles.contentField + ' validate-textarea' : styles.contentField}
                validateStatus={fieldErrors?.subject ? "error" : undefined}
                help={fieldErrors?.text}
                name="text"
                initialValue={initialData.text}
                style={{ display: "flex !important", justifyContent: "space-between !important" }}
                rules={[
                    {
                      required: true,
                    },
                    { max: 10000 },
                    {
                        validator: (_, value) => {
                            const errorMessage = "全角5000文字または半角10000文字以内で入力してください。";
                            if (ONLY_HANKAKU_REGEX.test(value)) {
                                if (!!value && value.length > 10000) {
                                    setIsCheckErrorTextArea(true)
                                    return Promise.reject(
                                        new Error(errorMessage)
                                    );
                                }
                                setIsCheckErrorTextArea(false)
                                return Promise.resolve();
                            }
                            if (!!value && value.length > 5000) {
                                setIsCheckErrorTextArea(true)
                                return Promise.reject(new Error(errorMessage));
                            }
                            setIsCheckErrorTextArea(false)
                            return Promise.resolve();
                        },
                    },

                ]}>

                <TextArea
                    autoSize={{ minRows: 4 }}
                    rows={8}
                    onChange={(inputValue) => {
                        const commentValue:string =
                            inputValue.target.value;
                        setContentTextArea(commentValue);
                    }}
                 />
            </Form.Item>
            { !!contentTextArea ?
                (<Form.Item>
                    <Row>
                        <Col {...formItemLayout.labelCol}></Col>
                        <ValidateTextArea
                            contentTextArea={contentTextArea}
                            errorMessages={ErrorMessages.scheduledEmail.maxsizeTextArea}
                            margin="0px"
                            className="validate-textarea-error ant-col ant-col-sm-16 ant-col-xl-17"
                            isScheduledMails={true}
                            isCheckErrorTextArea={isCheckErrorTextArea}
                        />
                    </Row>
                </Form.Item>
                ) : (
                <></>
                )
            }
            {isHTMLFormat && (
                <Form.Item
                    {...formItemLayout}
                    label=" "
                    colon={false}
                    style={{ margin: 0, padding: 0 }}>
                    <Col span={24}>
                        <Row justify="space-between" align="middle">
                            <Col>
                                <Row gutter={3}>
                                    <Col>
                                        <InfoCircleTwoTone />
                                    </Col>
                                    <Col>
                                        <Text>
                                            改行タグ（br）は自動で挿入されます。
                                            <a
                                                href={
                                                    Links.helps.scheduledEmails
                                                        .propertyDetail
                                                }
                                                target="_blank"
                                                rel="noopener noreferrer">
                                                詳細
                                            </a>
                                        </Text>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <Button
                                    style={{ marginTop: "2%" }}
                                    href={
                                        Links.helps.scheduledEmails.writableHTML
                                    }
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    記入ヘルプ
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Form.Item>
            )}
            <Form.Item
                {...formItemLayout}
                label="控えを配信者に送信"
                className={styles.field}
                help={fieldErrors?.send_copy_to_sender}
                name="send_copy_to_sender"
                initialValue={initialData.send_copy_to_sender}>
                <Switch
                    checkedChildren={<CheckOutlined />}
                    unCheckedChildren={<CloseOutlined />}
                    onChange={handleSwitchsendCopyToSender}
                    checked={sendCopyToSenderVisible}
                />
            </Form.Item>
            {sharedEmailsAuthorized ? (
                <Form.Item
                    {...formItemLayout}
                    label={
                        <span>
                            控えを共有メールに送信&nbsp;
                            <Tooltip
                                title={
                                    <span>
                                        チェックを入れると
                                        <Link to={`${Path.sharedMails}`} target="_blank" rel="noopener noreferrer">
                                            共有メール一覧
                                        </Link>
                                        に、控えが送信されます。
                                    </span>
                                }>
                                <QuestionCircleFilled style={{ color: iconCustomColor }} />
                            </Tooltip>
                        </span>
                    }
                    className={styles.field}
                    help={fieldErrors?.send_copy_to_share}
                    name="send_copy_to_share"
                    initialValue={initialData.send_copy_to_share}>
                    <Switch
                        checkedChildren={<CheckOutlined />}
                        unCheckedChildren={<CloseOutlined />}
                        onChange={handleSwitchSendCopyToShare}
                        checked={isSendCopyToShareChecked}
                    />
                </Form.Item>
            ) : (
                <div />
            )}
            <Form.Item
              {...formItemLayout}
              label={<span></span>}
              className={styles.fieldTemplate}>
              <Tooltip title="テンプレートを選択">
                  <Button
                      className={styles.controlButton}
                      size="small"
                      type="primary"
                      onClick={() => onSelectScheduledMailTemplate()}
                      icon={<SnippetsOutlined />}
                  />
              </Tooltip>
            </Form.Item>
            <ScheduledMailTemplateModal
                isModalVisible={listVisible}
                onModalClose={() =>
                    setListVisible(false)
                }
                onCreateTemplate={() => showRegister()}
                onInsertTemplate={(index) =>
                    templateValueToCommentForm(index)
                }
                onEditTemplate={(index) => showUpdate(index)}
                onDeleteTemplate={(index) => showDelete(index)}
                dataTemplate={dataTemplate}
            />
            <ScheduledMailTemplateCreateModal
                        currentUserId={currentUserId}
                        formRef={templateRef}
                        isOpen={registerVisible}
                        onOk={(dataTemplate) => registerOnOk(dataTemplate)}
                        onCancel={() => registerOnCancel()}
                    />
            <ScheduledMailTemplateUpdateModal
                currentUserId={currentUserId}
                initialData={
                  dataUpdateTemplate
                }
                formRef={templateRef}
                isOpen={updateVisible}
                onOk={(dataTemplate) => updateOnOk(dataTemplate)}
                onCancel={() => updateOnCancel()}
            />
        </Form>
    );
};

export default ScheduledEmailForm;
