import React, { useState, useEffect, useMemo } from "react";
import { connect, useSelector, useDispatch } from "react-redux";
import { Col, Row, Form, Select, Tooltip, message } from "antd";
import {
    DeleteOutlined,
    StarOutlined,
    SaveOutlined,
    QuestionCircleFilled,
} from "@ant-design/icons";
import styles from "./SearchTemplateSelectFormItem.scss";
import TemplateControlButton from "./TemplateControlButton/TemplateControlButton";
import { showDeleteModal } from "~/components/Feedbacks/Modal/Modal";
import { fetchApi, deleteApi, updateApi, createApi } from "~/actions/data";
import {
    SEARCH_TEMPLATE_COMMITTED,
    SEARCH_TEMPLATE_DELETED,
    SEARCH_TEMPLATE_UPDATED,
    SEARCH_TEMPLATE_CREATED,
    SEARCH_TEMPLATE_LOADING,
    SEARCH_TEMPLATE_LOADED,
    RESET_CURRENT_TEMPLATES,
    SET_DEFAULT_TEMPLATE,
    SEARCH_TEMPLATE_COMMITTED_FINISH,
    SEARCH_TEMPLATE_UPDATING,
    SEARCH_TEMPLATE_DELETING,
} from "~/actions/actionTypes";
import { convertSearchTemplateResponseDataEntry } from "~/domain/data";
import SaveSearchTemplateModal from "~/components/Feedbacks/Modal/SaveSearchTemplateModal/SaveSearchTemplateModal";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import { iconCustomColor } from "~/utils/constants";
import { useLocation } from "react-router-dom";
import {
    customSuccessMessage,
    customErrorMessage,
} from "~/components/Common/AlertMessage/AlertMessage";

/**
 * NOTE(joshua-hashimoto):
 *
 * [TSXリファクタリングメモ]
 * - 現時点でreducerNameは以下がオプション
 *     - organizationSearchPage
 *     - contactSearchPage
 *     - scheduledEmailSearchPage
 * - tableNameとisDefaultTemplateAttachedを削除する
 */

const SearchTemplateSelectFormItem = ({
    pageId,
    searchTemplateUrl,
    tableName,
    reducerName,
    onSelect,
    onDelete,
    isDefaultTemplateAttached,
    selectedTemplateName,
    setSelectedTemplateName,
}) => {
    // NOTE(joshua-hashimoto): テンプレートのデフォルト設定時のみAPIからテンプレート一覧を再取得したいためこれを設定する必要がある
    const location = useLocation();
    const urlPrefix = location.pathname.slice(1);
    const [preventUpdate, setPreventUpdate] = useState(false);
    const [templates, setTemplates] = useState([]);
    const dispatch = useDispatch();
    const { token } = useSelector((state) => state.login);
    const {
        currentSearchConditions,
        currentSearchTemplates,
        searchTemplateTotalAvailableCount,
        pageSize,
        requireRefreshTemplate,
        loading,
        message: successMessage,
    } = useSelector((state) => state[reducerName]);
    const [isSaveTemplateModalVisible, setIsSaveTemplateModalVisible] =
        useState(false);
    const [isValidationError, setIsValidationError] = useState(false);

    const onOk = ({ newTemplateName }) => {
        if (!newTemplateName || !newTemplateName.length) {
            customErrorMessage(
                "テンプレート名が空です。テンプレート名を入力してください。"
            );
            return;
        }
        const nameDoesExist = templates.some(
            (template) => template.name == newTemplateName
        );

        if (nameDoesExist) {
            customErrorMessage(
                `${newTemplateName} と同一名称のテンプレートが既に存在します。別のテンプレート名を入力してください。`
            );
            return;
        }
        setSelectedTemplateName(newTemplateName);
        onTemplateSave(newTemplateName);
        setIsSaveTemplateModalVisible(false);
    };

    const onCancel = () => {
        setIsSaveTemplateModalVisible(false);
        setIsValidationError(false);
    };

    const fetchSearchTemplateData = () => {
        const status = {
            before: SEARCH_TEMPLATE_LOADING,
            after: SEARCH_TEMPLATE_LOADED,
        };
        dispatch(
            fetchApi(
                pageId,
                token,
                searchTemplateUrl,
                null,
                status,
                convertSearchTemplateResponseDataEntry
            )
        );
    };

    const onTemplateSave = (templateName) => {
        setPreventUpdate(true);
        localStorage.setItem(urlPrefix + "_selected", templateName);
        const formValues = currentSearchConditions;
        const postData = {
            templateName,
            formValues,
            pageSize: pageSize,
            sortKey: "",
            sortOrder: "",
            selectedColumnKeys: [],
        };
        const status = {
            after: SEARCH_TEMPLATE_CREATED,
            commit: SEARCH_TEMPLATE_COMMITTED,
            afterCommit: SEARCH_TEMPLATE_COMMITTED_FINISH,
        };
        dispatch(createApi(pageId, token, searchTemplateUrl, postData, status));
        const isOverAvailableTemplateCount =
            searchTemplateTotalAvailableCount &&
            searchTemplateTotalAvailableCount <= templates.length;
        if (isOverAvailableTemplateCount) {
            setSelectedTemplateName(undefined);
            return;
        }
        const newTemplate = {
            name: templateName,
            values: formValues,
            star: false,
            display_name: templateName,
            template_name: templateName,
        };
        const updatedTemplates = [...templates, newTemplate];
        setTemplates(updatedTemplates);
        dispatch({
            type: pageId + RESET_CURRENT_TEMPLATES,
            payload: { data: updatedTemplates },
        });
    };

    const onTemplateDefault = (templateName) => {
        setPreventUpdate(false);
        const templateIndex = templates.findIndex(
            (template) => template.name == templateName
        );
        if (templateIndex === -1) {
            return;
        }
        const postData = {};
        const status = {
            before: SEARCH_TEMPLATE_UPDATING,
            after: SEARCH_TEMPLATE_UPDATED,
            commit: SEARCH_TEMPLATE_COMMITTED,
            afterCommit: SEARCH_TEMPLATE_COMMITTED_FINISH,
        };
        dispatch(
            updateApi(
                pageId,
                token,
                searchTemplateUrl,
                templateIndex,
                postData,
                status
            )
        );
    };

    const onTemplateDelete = (templateName) => {
        setPreventUpdate(true);
        const templateIndex = templates.findIndex(
            (template) => template.name === templateName
        );
        if (templateIndex === -1) {
            return;
        }
        const status = {
            before: SEARCH_TEMPLATE_DELETING,
            after: SEARCH_TEMPLATE_DELETED,
            commit: SEARCH_TEMPLATE_COMMITTED,
            afterCommit: SEARCH_TEMPLATE_COMMITTED_FINISH,
        };
        dispatch(
            deleteApi(pageId, token, searchTemplateUrl, templateIndex, status)
        );
        onDelete();
        const updatedTemplates = templates.filter(
            (_, index) => index !== templateIndex
        );
        dispatch({
            type: pageId + RESET_CURRENT_TEMPLATES,
            payload: { data: updatedTemplates },
        });
        setTemplates(updatedTemplates);
    };

    const onDeleteOk = () => {
        onTemplateDelete(selectedTemplateName);
        localStorage.removeItem(urlPrefix + "_" + selectedTemplateName);
        setSelectedTemplateName(undefined);
    };

    const authorizedControls = [
        {
            message: "選択中の検索条件テンプレートを削除します。",
            icon: (
                <DeleteOutlined data-testid="delete-search-template-button-icon" />
            ),
            isDanger: true,
            disabled: !selectedTemplateName || loading,
            onClick: () => {
                confirmModal({
                    title: "このテンプレートを削除しますか？",
                    content: (
                        <>
                            <p>OKを押すと、削除が実行されます。</p>
                            <p>元には戻せません。</p>
                        </>
                    ),
                    onOk: () => {
                        onDeleteOk();
                    },
                });
            },
        },
        {
            message:
                "選択中の検索条件テンプレートをデフォルトに設定／解除します。",
            icon: (
                <StarOutlined data-testid="favorite-search-template-button-icon" />
            ),
            isDanger: false,
            disabled: !selectedTemplateName || loading,
            onClick: (_) => onTemplateDefault(selectedTemplateName),
        },
        {
            message:
                templates &&
                searchTemplateTotalAvailableCount &&
                searchTemplateTotalAvailableCount <= templates.length
                    ? "登録可能な上限件数に達しています。"
                    : "現在の検索条件を保存します。",
            icon: (
                <SaveOutlined data-testid="save-search-template-button-icon" />
            ),
            isDanger: false,
            disabled:
                (templates &&
                    searchTemplateTotalAvailableCount &&
                    searchTemplateTotalAvailableCount <= templates.length) ||
                loading,
            onClick: (_) => setIsSaveTemplateModalVisible(true),
        },
    ];

    useEffect(() => {
        fetchSearchTemplateData();
    }, []);

    useEffect(() => {
        const incomingTemplates = currentSearchTemplates || [];
        if (incomingTemplates.length > 0) {
            incomingTemplates.forEach((element) => {
                if (!localStorage.getItem(urlPrefix + "_" + element.name)) {
                    localStorage.setItem(
                        urlPrefix + "_" + element.name,
                        JSON.stringify(element.values)
                    );
                }
            });
        }
        setTemplates(incomingTemplates);

        var templateWithStar;
        if (localStorage.getItem(urlPrefix + "_selected") !== "") {
            templateWithStar = incomingTemplates.find(
                (template) =>
                    template.name ===
                    localStorage.getItem(urlPrefix + "_selected")
            );
        } else if (localStorage.getItem(urlPrefix + "_selected") === "") {
            if (localStorage.getItem(urlPrefix + "_default")) {
                templateWithStar = null;
            } else {
                templateWithStar = incomingTemplates.find(
                    (template) => template.star
                );
            }
        } else {
            templateWithStar = incomingTemplates.find(
                (template) => template.star
            );
        }

        if (!selectedTemplateName && templateWithStar) {
            const templateValuesLocalStorage = localStorage.getItem(
                urlPrefix + "_" + templateWithStar.name
            );
            localStorage.setItem(
                "star_" + urlPrefix + "_selected",
                templateWithStar.name
            );
            setSelectedTemplateName(templateWithStar.name);
            onSelect(
                templateWithStar.name,
                templateValuesLocalStorage
                    ? JSON.parse(templateValuesLocalStorage)
                    : templateWithStar.values
            );
            dispatch({
                type: pageId + SET_DEFAULT_TEMPLATE,
                data: templateValuesLocalStorage
                    ? JSON.parse(templateValuesLocalStorage)
                    : templateWithStar.values,
            });
        }
    }, [currentSearchTemplates]);

    useEffect(() => {
        if (requireRefreshTemplate && !preventUpdate) {
            fetchSearchTemplateData();
            setPreventUpdate(false);
        }
    }, [requireRefreshTemplate]);

    useEffect(() => {
        if (loading) {
            setIsValidationError(loading);
        }
    }, [loading]);

    useEffect(() => {
        if (successMessage) {
            customSuccessMessage(successMessage);
        }
    }, [successMessage]);

    return (
        <Form>
            <Row span={24} align="middle">
                <Col>
                    <Form.Item span={24} noStyle>
                        <Select
                            className={styles.selectContainer}
                            placeholder="検索条件テンプレートを選択"
                            onChange={(value) => {
                                setSelectedTemplateName(value);
                                if (!value) {
                                    onSelect(value);
                                    return;
                                }
                            }}
                            onSelect={(value) => {
                                const template = templates.find(
                                    (template) => template.name === value
                                );
                                const templateValues = template.values;
                                onSelect(template.name, templateValues);
                            }}
                            allowClear
                            value={selectedTemplateName}
                            style={{ textAlign: "left" }}>
                            {templates.map((entry, index) => {
                                return (
                                    <Select.Option
                                        key={index}
                                        value={entry.name}>
                                        {entry.display_name}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                </Col>
                <Col>
                    <>
                        {authorizedControls.map((control, index) => {
                            return (
                                <TemplateControlButton
                                    key={index}
                                    message={control.message}
                                    icon={control.icon}
                                    isDanger={control.isDanger}
                                    onClick={control.onClick}
                                    disabled={control.disabled}
                                />
                            );
                        })}
                        <SaveSearchTemplateModal
                            templateReducerName={reducerName}
                            isSaveTemplateModalVisible={
                                isSaveTemplateModalVisible
                            }
                            isValidationError={isValidationError}
                            onCancel={onCancel}
                            onOk={onOk}
                        />
                        <Tooltip title="テンプレートはユーザー個人ごとに保存され、他のユーザーとは共有されません。">
                            <QuestionCircleFilled
                                style={{ color: iconCustomColor }}
                                className={styles.tooltip}
                            />
                        </Tooltip>
                    </>
                </Col>
            </Row>
        </Form>
    );
};

const mapStateToProps = (state) => {
    return {
        token: state.login.token,
        authorizedActions: state.login.authorizedActions,
    };
};

export default connect(mapStateToProps)(SearchTemplateSelectFormItem);
