import React from "react";
import {
    Col,
    Form,
    FormProps,
    Row,
    Select,
    Transfer,
    TransferProps,
} from "antd";
import { DisplaySettingFieldTypes } from "~/models/displaySetting";
import { TransferModel } from "~/models/transferModel";
import styles from "./TableDisplaySettingForm.scss";

const { Option } = Select;

const formItemLayout = {
    labelCol: {
        span: 0,
    },
    wrapperCol: {
        span: 24,
    },
};

const formItemWithPageSizeLayout = {
    labelCol: {
        span: 4,
    },
};

type Props = FormProps & {
    fieldName: DisplaySettingFieldTypes;
    transferProps?: TransferProps<TransferModel>;
};

const TableDisplaySettingForm = ({
    fieldName,
    transferProps,
    ...props
}: Props) => {
    return (
        <Form {...props}>
            <Col span={24}>
                <Row justify="center">
                    <Col>
                        <Form.Item
                            {...formItemLayout}
                            label=" "
                            className={styles.field}
                            name={["content", fieldName, "table"]}
                            colon={false}
                            style={{ textAlign: "left" }}>
                            <Transfer
                                {...transferProps}
                                titles={["表示", "非表示"]}
                                render={(item) => item.title}
                                className={styles.transfer}
                            />
                        </Form.Item>
                        <Form.Item
                            {...formItemWithPageSizeLayout}
                            label="表示件数"
                            className={styles.field}
                            name={["content", fieldName, "page_size"]}
                            initialValue={10}
                            style={{ textAlign: "left" }}>
                            <Select style={{ width: 80 }}>
                                <Select.Option
                                    value={10}
                                    data-testid="table-display-setting-page-size-option-10">
                                    10
                                </Select.Option>
                                <Select.Option
                                    value={50}
                                    data-testid="table-display-setting-page-size-option-50">
                                    50
                                </Select.Option>
                                <Select.Option
                                    value={100}
                                    data-testid="table-display-setting-page-size-option-100">
                                    100
                                </Select.Option>
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Form>
    );
};

export default TableDisplaySettingForm;
