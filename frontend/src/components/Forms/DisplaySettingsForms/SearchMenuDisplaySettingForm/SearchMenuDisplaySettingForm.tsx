import React from "react";
import { Col, Form, FormProps, Row, Transfer, TransferProps } from "antd";
import { TransferModel } from "~/models/transferModel";
import { DisplaySettingFieldTypes } from "~/models/displaySetting";
import styles from "./SearchMenuDisplaySettingForm.scss";

const formItemLayout = {
    labelCol: {
        span: 0,
    },
    wrapperCol: {
        span: 24,
    },
};

type Props = FormProps & {
    fieldName: DisplaySettingFieldTypes;
    transferProps?: TransferProps<TransferModel>;
};

const SearchMenuDisplaySettingForm = ({
    fieldName,
    transferProps,
    ...props
}: Props) => {
    return (
        <Form {...props}>
            <Col span={24}>
                <Row justify="center">
                    <Col>
                        <Form.Item
                            {...formItemLayout}
                            name={["content", fieldName, "search"]}
                            colon={false}
                            style={{ textAlign: "left" }}>
                            <Transfer
                                {...transferProps}
                                titles={["表示", "非表示"]}
                                render={(item) => item.title}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Form>
    );
};

export default SearchMenuDisplaySettingForm;
