import React from "react";
import { renderWithAllProviders, screen, userEvent } from "~/test/utils";
import TagForm from "../TagForm";
import { Button } from "antd";
import { tagResponseModelList } from "~/test/mock/tagAPIMock";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { ErrorMessages, TAG_COLORS } from "~/utils/constants";

const mockOnSubmit = jest.fn();

const deleteButton = (
    <Button htmlType="button" type="primary" danger onClick={() => {}}>
        削除
    </Button>
);

describe("TagForm.tsx", () => {
    const tagFormColorSelectTestId = "tag-form-color-select";
    const maxLengthRegex = new RegExp(ErrorMessages.validation.length.max50);
    const requiredRegex = new RegExp(ErrorMessages.form.required);

    describe("register form", () => {
        test("render test", async () => {
            const { container } = renderWithAllProviders(
                <TagForm onSubmit={mockOnSubmit} />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    tags: {
                                        _all: true,
                                    },
                                },
                            },
                        },
                    }),
                }
            );
            const tagNameLabelElement = screen.getByText(/タグ名/);
            expect(tagNameLabelElement).toBeInTheDocument();
            const tagNameInputElement = screen.getByRole(
                "textbox"
            ) as HTMLTextAreaElement;
            expect(tagNameInputElement).toBeInTheDocument();
            expect(tagNameInputElement.value).toBeFalsy();
            const tagColorSelectElement = screen.getByTestId(
                tagFormColorSelectTestId
            );
            expect(tagColorSelectElement).toBeInTheDocument();
            // NOTE(joshua-hashimoto): Select要素のdisabledは取得できないので、クリックしても何も表示されないことでSelect要素をクリックできないことを保証する
            await userEvent.click(tagColorSelectElement);
            for (const tagColor of TAG_COLORS) {
                if (tagColor.value === "default") {
                    expect(
                        container.getElementsByClassName(
                            `ant-tag-${tagColor.value}`
                        ).length
                    ).toBe(1);
                    continue;
                }
                expect(
                    container.getElementsByClassName(
                        `ant-tag-${tagColor.value}`
                    ).length
                ).toBe(0);
            }
            const backButtonElement = screen.getByRole("button", {
                name: /戻る/,
            });
            expect(backButtonElement).toBeInTheDocument();
            expect(backButtonElement).not.toBeDisabled();
            const registerButtonElement = screen.getByRole("button", {
                name: /登 録/,
            });
            expect(registerButtonElement).toBeInTheDocument();
            expect(registerButtonElement).toBeDisabled();
            await userEvent.type(tagNameInputElement, "input example");
            expect(tagNameInputElement.value).toBe("input example");
            expect(registerButtonElement).not.toBeDisabled();
        });
    });

    describe("edit form", () => {
        const mockData = tagResponseModelList[0];

        test("render test", async () => {
            renderWithAllProviders(
                <TagForm
                    initialData={mockData}
                    onSubmit={mockOnSubmit}
                    deleteButton={deleteButton}
                />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    tags: {
                                        _all: true,
                                    },
                                },
                            },
                        },
                    }),
                }
            );
            const tagNameLabelElement = screen.getByText(/タグ名/);
            expect(tagNameLabelElement).toBeInTheDocument();
            const tagNameInputElement = screen.getByRole(
                "textbox"
            ) as HTMLTextAreaElement;
            expect(tagNameInputElement).toBeInTheDocument();
            expect(tagNameInputElement.value).toBe(mockData.value);
            const tagColorSelectElement = screen.getByTestId(
                tagFormColorSelectTestId
            );
            expect(tagColorSelectElement).toBeInTheDocument();
            // TODO(joshua-hashimoto): Selectを開いた時の動作を現時点ではテストできていない。テスト方法確立次第ここに記載すること
            const backButtonElement = screen.getByRole("button", {
                name: /戻る/,
            });
            expect(backButtonElement).toBeInTheDocument();
            expect(backButtonElement).not.toBeDisabled();
            const editButtonElement = screen.getByRole("button", {
                name: /更 新/,
            });
            expect(editButtonElement).toBeInTheDocument();
            expect(editButtonElement).not.toBeDisabled();
            await userEvent.type(tagNameInputElement, "input example");
            expect(tagNameInputElement.value).toBe(
                `${mockData.value}input example`
            );
            expect(editButtonElement).not.toBeDisabled();
            await userEvent.clear(tagNameInputElement);
            expect(tagNameInputElement).not.toHaveValue();
            expect(editButtonElement).toBeDisabled();
            // NOTE(joshua-hashimoto): 削除ボタンは渡す形なので、ここでは存在確認だけする
            const deleteButtonElement = screen.getByRole("button", {
                name: /削 除/,
            });
            expect(deleteButtonElement).toBeInTheDocument();
        });
    });

    describe("input validation test", () => {
        test("normal input pattern", async () => {
            renderWithAllProviders(
                <TagForm onSubmit={mockOnSubmit} deleteButton={deleteButton} />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    tags: {
                                        _all: true,
                                    },
                                },
                            },
                        },
                    }),
                }
            );
            const tagNameInputElement = screen.getByRole(
                "textbox"
            ) as HTMLTextAreaElement;
            const registerButtonElement = screen.getByRole("button", {
                name: /登 録/,
            });
            await userEvent.type(tagNameInputElement, "example tag");
            expect(tagNameInputElement.value).toBe("example tag");
            expect(registerButtonElement).not.toBeDisabled();
            const maxLengthErrorMessageElement =
                screen.queryByText(maxLengthRegex);
            expect(maxLengthErrorMessageElement).not.toBeInTheDocument();
            const requiredErrorMessageElement =
                screen.queryByText(requiredRegex);
            expect(requiredErrorMessageElement).not.toBeInTheDocument();
        });

        test("clear input validation test", async () => {
            renderWithAllProviders(
                <TagForm onSubmit={mockOnSubmit} deleteButton={deleteButton} />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    tags: {
                                        _all: true,
                                    },
                                },
                            },
                        },
                    }),
                }
            );
            const tagNameInputElement = screen.getByRole(
                "textbox"
            ) as HTMLTextAreaElement;
            await userEvent.type(tagNameInputElement, "random text");
            await userEvent.clear(tagNameInputElement);
            const registerButtonElement = screen.getByRole("button", {
                name: /登 録/,
            });
            expect(registerButtonElement).toBeDisabled();
            const maxLengthErrorMessageElement =
                screen.queryByText(maxLengthRegex);
            expect(maxLengthErrorMessageElement).not.toBeInTheDocument();
            const requiredErrorMessageElement = await screen.findByText(
                requiredRegex
            );
            expect(requiredErrorMessageElement).toBeInTheDocument();
        });

        test("over input limit validation test", async () => {
            renderWithAllProviders(
                <TagForm onSubmit={mockOnSubmit} deleteButton={deleteButton} />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    tags: {
                                        _all: true,
                                    },
                                },
                            },
                        },
                    }),
                }
            );
            const tagNameInputElement = screen.getByRole(
                "textbox"
            ) as HTMLTextAreaElement;
            await userEvent.type(tagNameInputElement, "a".repeat(51));
            const registerButtonElement = screen.getByRole("button", {
                name: /登 録/,
            });
            expect(registerButtonElement).toBeDisabled();
            const maxLengthErrorMessageElement = await screen.findByText(
                maxLengthRegex
            );
            expect(maxLengthErrorMessageElement).toBeInTheDocument();
            const requiredErrorMessageElement =
                screen.queryByText(requiredRegex);
            expect(requiredErrorMessageElement).not.toBeInTheDocument();
        });
    });
});
