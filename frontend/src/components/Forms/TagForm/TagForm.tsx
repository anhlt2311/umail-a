import React, { ReactNode, useEffect, useState } from "react";
import { Button, Form, Input, Row, Col, Select, Tag, Switch, Tooltip } from "antd";
import validateJapaneseMessages from "../validateMessages";
import BackButton from "../../Common/BackButton/BackButton";
import Paths from "../../Routes/Paths";
import { ErrorMessages, TAG_COLORS, iconCustomColor, Links } from "~/utils/constants";
import { TagFormModel } from "~/models/tagModel";
import { ValidateErrorEntity } from "rc-field-form/lib/interface";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import styles from "./TagForm.scss";
import { CloseOutlined, CheckOutlined, QuestionCircleFilled } from '@ant-design/icons';
import { usePlans } from "~/hooks/usePlan";
const { TextArea } = Input;

const formItemLayoutLabelColSpan = 5;
const formItemLayout = {
    labelCol: {
        span: formItemLayoutLabelColSpan,
    },
    wrapperCol: {
        span: 24 - formItemLayoutLabelColSpan,
    },
};

type Props = {
    initialData?: TagFormModel;
    fieldErrors?: TagFormModel;
    onSubmit: (values: TagFormModel) => void;
    deleteButton?: ReactNode;
};

const TagForm = ({
    initialData,
    fieldErrors,
    onSubmit,
    deleteButton,
}: Props) => {
    const requiredFields = ["value", "color"];
    const [form] = Form.useForm<TagFormModel>();
    const { _all: tagAuthorization } = useAuthorizedActions("tags");
    const [errorFields, setErrorFields] = useState<any[]>([]);
    const { isStandardPlan } = usePlans();

    const handleSubmit = (values: TagFormModel) => {
        onSubmit(values);
    };

    const handleSubmitError = ({
        values,
        errorFields,
        outOfDate,
    }: ValidateErrorEntity<TagFormModel>) => {
        if (errorFields) {
            let errorFieldNames = errorFields.map((field) => {
                return field["name"][0];
            });
            setErrorFields(errorFieldNames);
        }
    };

    useEffect(() => {
        if (initialData) {
            form.setFieldsValue(initialData);
        } else {
            form.setFieldsValue({ color: "default" });
        }
    }, [initialData]);

    return (
        <Col span={11}>
            <Form
                onFinish={handleSubmit}
                onFinishFailed={handleSubmitError}
                className={styles.container}
                form={form}
                validateMessages={validateJapaneseMessages}
                labelAlign="right">
                <Form.Item
                    {...formItemLayout}
                    label="タグ名"
                    className={styles.field}
                    required={true}>
                    <Col span={24}>
                        <Row>
                            <Col flex="auto">
                                <Form.Item
                                    validateStatus={
                                        fieldErrors?.value ? "error" : undefined
                                    }
                                    help={fieldErrors?.value}
                                    name="value"
                                    rules={[
                                        { required: true },
                                        {
                                            max: 50,
                                            message:
                                                ErrorMessages.validation.length
                                                    .max50,
                                        },
                                    ]}>
                                    <TextArea autoSize={{minRows:1}} />
                                </Form.Item>
                            </Col>
                            <Col>
                                <Form.Item shouldUpdate>
                                    {() => (
                                        <Form.Item name="color">
                                            <Select
                                                disabled={
                                                    !form.getFieldValue("value")
                                                }
                                                style={{
                                                    height: "100%",
                                                }}
                                                data-testid="tag-form-color-select">
                                                {TAG_COLORS.map((tag) => (
                                                    <Select.Option
                                                        key={tag.value}
                                                        value={tag.value}>
                                                        <Tag
                                                            color={tag.value}
                                                            style={{
                                                                marginTop:
                                                                    "3px",
                                                                marginBottom:
                                                                    "3px",
                                                                marginInlineEnd:
                                                                    "4.8px",
                                                                paddingInlineStart:
                                                                    "4px",
                                                                paddingInlineEnd:
                                                                    "4px",
                                                                whiteSpace:
                                                                    "normal",
                                                            }}>
                                                            {form.getFieldValue(
                                                                "value"
                                                            )}
                                                        </Tag>
                                                    </Select.Option>
                                                ))}
                                            </Select>
                                        </Form.Item>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                </Form.Item>
                {isStandardPlan ? (
                    <Form.Item
                        {...formItemLayout}
                        label={
                            <span>
                                スキル兼用&nbsp;
                                <Tooltip
                                    title={
                                        <span>
                                            有効にすることでスキルタグとして登録されます。タグの一覧での表示に加え、
                                            <a  href='#'
                                                target="_blank"
                                                rel="noopener noreferrer"
                                            >
                                                要員ボード</a>
                                            要員情報 スキルの一覧にも表示されます。
                                            <br />
                                            <a
                                                href={Links.helps.tags.details}
                                                target="_blank"
                                                rel="noopener noreferrer">
                                                詳細
                                            </a>
                                        </span>
                                    }>
                                    <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                                </Tooltip>
                            </span>
                        }
                        className={styles.field}
                        >
                        <Col span={24}>
                            <Row>
                                <Col flex="auto">
                                    <Form.Item name="is_skill" valuePropName="checked">
                                        <Switch
                                            checkedChildren={
                                                <CheckOutlined />
                                            }
                                            unCheckedChildren={
                                                <CloseOutlined />
                                            }
                                        />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Col>
                    </Form.Item>
                    ) : (
                    <></>
                )}
                <Form.Item style={{ marginTop: "3%" }}>
                    <Col span={24}>
                        <Row justify="space-between">
                            <Col>
                                <Row>
                                    <Col>
                                        <BackButton to={Paths.tags} />
                                    </Col>
                                    <Col>
                                        <Form.Item shouldUpdate>
                                            {() => (
                                                <Button
                                                    type="primary"
                                                    htmlType="submit"
                                                    className={styles.button}
                                                    disabled={
                                                        !tagAuthorization ||
                                                        !!form
                                                            .getFieldsError()
                                                            .filter(
                                                                ({ errors }) =>
                                                                    errors.length
                                                            ).length ||
                                                        !form.isFieldsTouched(
                                                            requiredFields,
                                                            true
                                                        )
                                                    }>
                                                    {initialData?.id
                                                        ? "更新"
                                                        : "登録"}
                                                </Button>
                                            )}
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>{deleteButton}</Col>
                        </Row>
                    </Col>
                </Form.Item>
            </Form>
        </Col>
    );
};

export default TagForm;
