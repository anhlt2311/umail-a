import React from "react";
import { Form, FormItemProps } from "antd";
import AjaxSelect from "../../ajax/AjaxSelect";
import { Endpoint } from "~/domain/api";
import { DisplayTextAndForeignKeyModel } from "~/models/displayTextAndForeignKeyModel";
import styles from "./ExceptionalOrganizationFormItem.scss";

type Props = FormItemProps & {
    defaultSelect: DisplayTextAndForeignKeyModel;
};

const ExceptionalOrganizationFormItem = ({
    defaultSelect,
    ...props
}: Props) => {
    return (
        <Form.Item label="" className={styles.field} {...props} noStyle>
            <AjaxSelect
                resourceUrl={`${Endpoint.getBaseUrl()}/${
                    Endpoint.organizations
                }/names`}
                displayKey="name"
                searchParam="name"
                defaultSelect={defaultSelect}
            />
        </Form.Item>
    );
};

export default ExceptionalOrganizationFormItem;
