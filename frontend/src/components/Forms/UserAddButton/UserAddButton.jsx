import React from "react";
import { Button } from "antd";
import { UserAddOutlined } from "@ant-design/icons";
import Paths from "../../Routes/Paths";

const UserAddButton = () => {
    return (
        <Button
            type="primary"
            icon={<UserAddOutlined />}
            href={Paths.userInvite}
            size="small">
            ユーザー招待
        </Button>
    );
};

export default UserAddButton;
