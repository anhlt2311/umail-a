import React from "react";
import { render, screen } from "~/test/utils";
import ContactDepartmentFormItem from "../ContactDepartmentFormItem";

const mockOnClear = jest.fn();

describe("ContactDepartmentFormItem", () => {
    test("enabled render test", () => {
        render(<ContactDepartmentFormItem onClear={mockOnClear} />);
        const inputElement = screen.getByPlaceholderText(
            /部署/
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(<ContactDepartmentFormItem disabled onClear={mockOnClear} />);
        const inputElement = screen.getByPlaceholderText(
            /部署/
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).toBeDisabled();
    });
});
