import React from "react";
import { Col, Form, Input, Row, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";

import { Links, iconCustomColor } from "~/utils/constants";
import styles from "./ContactDepartmentFormItem.scss";

type Props = {
    disabled?: boolean;
    onClear: (fieldName: string, value: string) => void;
};

const ContactDepartmentFormItem = ({ disabled, onClear }: Props) => {
    const fieldName = "department";

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <Input
                            className={styles.userInput}
                            placeholder="部署"
                            allowClear
                            onChange={(event) => {
                                const value = event.target.value;
                                if (value === "") {
                                    onClear(fieldName, "");
                                }
                            }}
                            disabled={disabled}
                        />
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                <a
                                    href={Links.helps.filter.partialMatch}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    部分一致検索
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ContactDepartmentFormItem;
