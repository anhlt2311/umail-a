import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Badge, Button, Col, Form, Row, Tooltip } from "antd";
import { useDebouncedCallback } from "use-debounce";
import { CONTACT_SEARCH_PAGE } from "~/components/Pages/pageIds";
import { Endpoint } from "~/domain/api";
import ContactNameFormItem from "./ContactNameFormItem/ContactNameFormItem";
import ContactTelFormItem from "./ContactTelFormItem/ContactTelFormItem";
import ContactEmailToFormItem from "./ContactEmailToFormItem/ContactEmailToFormItem";
import ContactEmailCcFormItem from "./ContactEmailCcFormItem/ContactEmailCcFormItem";
import ContactOrganizationNameFormItem from "./ContactOrganizationNameFormItem/ContactOrganizationNameFormItem";
import ContactDepartmentFormItem from "./ContactDepartmentFormItem/ContactDepartmentFormItem";
import ContactPositionFormItem from "./ContactPositionFormItem/ContactPositionFormItem";
import ContactStaffFormItem from "./ContactStaffFormItem/ContactStaffFormItem";
import ContactLastVisitFormItem from "./ContactLastVisitFormItem/ContactLastVisitFormItem";
import ContactCategoryFormItem from "./ContactCategoryFormItem/ContactCategoryFormItem";
import ContactTagFormItem from "./ContactTagFormItem/ContactTagFormItem";
import ContactPreferenceFormItem from "./ContactPreferenceFormItem/ContactPreferenceFormItem";
import ContactCommentUserFormItem from "./ContactCommentUserFormItem/ContactCommentUserFormItem";
import ContactCreatedUserFormItem from "./ContactCreatedUserFormItem/ContactCreatedUserFormItem";
import ContactModifiedUserFormItem from "./ContactModifiedUserFormItem/ContactModifiedUserFormItem";
import ContactWantsLocationFormItem from "./ContactWantsLocationFormItem/ContactWantsLocationFormItem";
import moment from "moment";
import { CloseOutlined, SearchOutlined } from "@ant-design/icons";
import SearchTemplateSelectFormItem from "../SearchTemplateSelectFormItem/SearchTemplateSelectFormItem";
import IncludeInvalidSwitchFormItem from "./IncludeInvalidSwitchFromItem/IncludeInvalidSwitchFormItem";
import IncludeBlocklistSwitchFormItem from "./IncludeBlocklistSwitchFromItem/IncludeBlocklistSwitchFormItem";
import { useDisplaySettingAPI } from "~/hooks/useDisplaySettingAPI";
import SearchMenuDrawer from "~/components/Common/SearchMenuDrawer/SearchMenuDrawer";
import { useSearchTemplateUtils } from "~/hooks/useSearchTemplate";
import { useSearchSync } from "~/hooks/useSearch";
import styles from "./ContactSearchForm.scss";
import {useFetchTagsAPIQuery} from "~/hooks/useTag";

const pageId = CONTACT_SEARCH_PAGE;
const filterType = "search";
const searchTemplateURL = `${Endpoint.getBaseUrl()}/${
    Endpoint.contactSearchTemplate
}`;
const templateReducerName = "contactSearchPage";
const contacts = "contacts";

const emptyData = {
    category: undefined,
    category_inequality: "eq",
    cc_mails: "",
    contact_preference: [],
    date_range: [],
    department: "",
    email: "",
    first_name: "",
    last_name: "",
    organization__name: "",
    position: "",
    staff: [],
    tags: [],
    tags__suffix: "and",
    tel1: "",
    tel2: "",
    tel3: "",
    wants_location: [],
    ignore_filter: false,
    ignore_blocklist_filter: false,
    comment_user: [],
    created_user: [],
    modified_user: [],
};

const ContactSearchForm = React.forwardRef(
    (
        {
            tableName,
            initialData = emptyData,
            submitHandler,
            isDefaultTemplateAttached,
            searchConditionSanitizer,
        },
        ref
    ) => {
        const [form] = Form.useForm();
        const [isSearchDrawerOpen, setIsSearchDrawerOpen] = useState(false);
        const { displaySetting } = useSelector(
            (state) => state.displaySettingPage
        );
        const [unshowSearchFields, setUnshowSearchFields] = useState([]);
        const [filledSearchFieldCount, setFilledSearchFieldCount] = useState(0);
        const [selectedTemplateName, setSelectedTemplateName] =
            useState(undefined);
        const { updateDisplaySetting } = useDisplaySettingAPI();
        const { saveCurrentSearchCondition } = useSearchTemplateUtils();
        const { syncToUrl, queryParamsToObj } = useSearchSync();
        const { data: dataTags, isLoading, isError } = useFetchTagsAPIQuery({});

        const countFilterField = (formValues) => {
            let count = 0;
            if (!!formValues.ignore_filter) {
                count += 1;
            }
            if (!!formValues.ignore_blocklist_filter) {
                count += 1;
            }
            if (!!formValues.first_name || !!formValues.last_name) {
                count += 1;
            }
            if (!!formValues.organization__name) {
                count += 1;
            }
            if (!!formValues.email) {
                count += 1;
            }
            if (!!formValues.cc_mails) {
                count += 1;
            }
            if (!!formValues.tel1 || !!formValues.tel2 || !!formValues.tel3) {
                count += 1;
            }
            if (!!formValues.position) {
                count += 1;
            }
            if (!!formValues.department) {
                count += 1;
            }
            if (!!formValues.staff && !!formValues.staff.length) {
                count += 1;
            }
            if (!!formValues.date_range && !!formValues.date_range.length) {
                count += 1;
            }
            if (
                !!formValues.tags__suffix &&
                !!formValues.tags &&
                !!formValues.tags.length
            ) {
                count += 1;
            }
            if (!!formValues.category && !!formValues.category_inequality) {
                count += 1;
            }
            if (
                !!formValues.contact_preference &&
                !!formValues.contact_preference.length
            ) {
                count += 1;
            }
            if (
                !!formValues.wants_location &&
                !!formValues.wants_location.length
            ) {
                count += 1;
            }
            if (!!formValues.comment_user && !!formValues.comment_user.length) {
                count += 1;
            }
            if (!!formValues.created_user && !!formValues.created_user.length) {
                count += 1;
            }
            if (
                !!formValues.modified_user &&
                !!formValues.modified_user.length
            ) {
                count += 1;
            }
            return count;
        };

        const submitToFilter = useDebouncedCallback((values) => {
            syncToUrl(values);
            saveCurrentSearchCondition(contacts, selectedTemplateName, values);
            submitHandler(values);
            const count = countFilterField(values);
            setFilledSearchFieldCount(count);
        }, 600);

        const onReset = () => {
            form.setFieldsValue(emptyData);
            setSelectedTemplateName(undefined);
            submitHandler(emptyData);
            syncToUrl({});
            const count = countFilterField({});
            setFilledSearchFieldCount(count);
            saveCurrentSearchCondition(contacts, selectedTemplateName, {});
        };

        const onClear = async (fieldKey, clearValue) => {
            try {
                const values = await form.validateFields();
                values[fieldKey] = clearValue;
                submitHandler(values);
            } catch (err) {
                console.error(err);
            }
        };

        const onTemplateSelect = (templateName, templateValues) => {
            if (!templateValues) {
                let retrievedObject = localStorage.getItem(
                    contacts + "_default"
                );
                localStorage.setItem(contacts + "_selected", "");
                const parsedObj = JSON.parse(retrievedObject);
                const newObject = {
                    ...emptyData,
                    ...parsedObj,
                };
                submitHandler(newObject);
                return;
            }
            // NOTE(joshua-hahsimoto): 設立期間がある場合は文字列からDateに変換してあげる必要がある
            if (templateValues && templateValues.date_range) {
                templateValues.date_range = templateValues.date_range.map(
                    (date) => moment(date)
                );
            }
            const formValues = templateValues
                ? {
                      ...emptyData,
                      ...templateValues,
                  }
                : emptyData;
            var retrievedObject = localStorage.getItem(
                contacts + "_" + templateName
            );
            localStorage.setItem(contacts + "_selected", templateName);
            const parsedObj = JSON.parse(retrievedObject);
            parsedObj.tags = onRemoveTagsUrl(parsedObj);;
            const newObject = {
                ...formValues,
                ...parsedObj,
            };
            const newData = searchConditionSanitizer(
                unshowSearchFields,
                newObject
            );
            form.setFieldsValue(newData);
            submitHandler(newData);
            syncToUrl(newData);
            const count = countFilterField(newData);
            setFilledSearchFieldCount(count);
        };

        const menuItems = [
            {
                fieldKey: "name",
                searchField: ContactNameFormItem,
                fieldNames: ["first_name", "last_name"],
                onClear: onClear,
            },
            {
                fieldKey: "organizations",
                searchField: ContactOrganizationNameFormItem,
                fieldNames: ["organization__name"],
                onClear: onClear,
            },
            {
                fieldKey: "email_to",
                searchField: ContactEmailToFormItem,
                fieldNames: ["email"],
                onClear: onClear,
            },
            {
                fieldKey: "email_cc",
                searchField: ContactEmailCcFormItem,
                fieldNames: ["cc_mails"],
                onClear: onClear,
            },
            {
                fieldKey: "tel",
                searchField: ContactTelFormItem,
                fieldNames: ["tel1", "tel2", "tel3"],
                onClear: onClear,
            },
            {
                fieldKey: "position",
                searchField: ContactPositionFormItem,
                fieldNames: ["position"],
                onClear: onClear,
            },
            {
                fieldKey: "department",
                searchField: ContactDepartmentFormItem,
                fieldNames: ["department"],
                onClear: onClear,
            },
            {
                fieldKey: "staff",
                searchField: ContactStaffFormItem,
                fieldNames: ["staff"],
                onClear: undefined,
            },
            {
                fieldKey: "last_visit",
                searchField: ContactLastVisitFormItem,
                fieldNames: ["date_range"],
                onClear: undefined,
            },
            {
                fieldKey: "tag",
                searchField: ContactTagFormItem,
                fieldNames: ["tags", "tags__suffix"],
                onClear: undefined,
            },
            {
                fieldKey: "category",
                searchField: ContactCategoryFormItem,
                fieldNames: ["category", "category_inequality"],
                onClear: undefined,
            },
            {
                fieldKey: "preference",
                searchField: ContactPreferenceFormItem,
                fieldNames: ["contact_preference"],
                onClear: undefined,
            },
            {
                fieldKey: "wants_location",
                searchField: ContactWantsLocationFormItem,
                fieldNames: ["wants_location"],
                onClear: undefined,
            },
            {
                fieldKey: "comment_user",
                searchField: ContactCommentUserFormItem,
                fieldNames: ["comment_user"],
                onClear: undefined,
            },
            {
                fieldKey: "created_user",
                searchField: ContactCreatedUserFormItem,
                fieldNames: ["created_user"],
                onClear: undefined,
            },
            {
                fieldKey: "modified_user",
                searchField: ContactModifiedUserFormItem,
                fieldNames: ["modified_user"],
                onClear: undefined,
            },
        ];

        const onUpdateSearchField = () => {
            const postData = {
                content: {
                    contacts: {
                        search: [...unshowSearchFields],
                    },
                },
            };
            updateDisplaySetting(postData);
        };

        const onDrawerClose = () => {
            setIsSearchDrawerOpen(false);
        };

        const onFieldAdd = (newUnshowFields, fieldKey) => {
            setUnshowSearchFields(newUnshowFields);
        };

        const onFieldRemove = (newUnshowFields, fieldNames) => {
            setUnshowSearchFields(newUnshowFields);
            form.resetFields(fieldNames);
            const values = form.getFieldsValue();
            submitHandler(values);
            syncToUrl(values);
        };

        useEffect(() => {
            const tempData = { ...emptyData, ...initialData };
            if (tempData.tags === undefined) {
                tempData.tags = [];
            }
            if (tempData.tags__suffix === undefined) {
                tempData.tags__suffix = "and";
            }
            const dateRange = tempData.date_range ?? [];
            if (dateRange.length) {
                tempData.date_range = dateRange.map((date) => moment(date));
            }
            const queryParamsObj = queryParamsToObj();
            queryParamsObj.tags = onRemoveTagsUrl(queryParamsObj);
            const formValues = {
                ...tempData,
                ...queryParamsObj,
            };
            form.setFieldsValue(formValues);
            const count = countFilterField(formValues);
            setFilledSearchFieldCount(count);
            submitHandler(formValues);
            syncToUrl(formValues);
        }, [dataTags]);

        useEffect(() => {
            setInitialFormSearch();
        }, [displaySetting]);

        const onRemoveTagsUrl = (queryParamsObj) => {
            if (queryParamsObj?.tags && typeof queryParamsObj?.tags === "string") {
                if (dataTags) {
                    if (!dataTags.results.some(tag => tag.id === queryParamsObj.tags)) {
                        queryParamsObj.tags = '';
                    }
                }
            }
            if (queryParamsObj?.tags && Array.isArray(queryParamsObj.tags)) {
                if (dataTags) {
                    queryParamsObj.tags = queryParamsObj.tags.filter(tagId => dataTags.results.some(tag => tag.id === tagId))
                }
            }
            return queryParamsObj?.tags;
        }

        const setInitialFormSearch = () => {
            const dataExist =
                displaySetting &&
                displaySetting[tableName] &&
                displaySetting[tableName][filterType];
            const unshowSearchFields = dataExist
                ? displaySetting[tableName][filterType]
                : [];
            setUnshowSearchFields(unshowSearchFields);
        };

        return (
            <>
                <Col>
                    <Row justify="end">
                        <Button.Group>
                            <Button
                                type="primary"
                                size="small"
                                icon={<SearchOutlined />}
                                onClick={() => setIsSearchDrawerOpen(true)}>
                                絞り込み検索
                            </Button>
                            {filledSearchFieldCount ? (
                                <Tooltip title="現在検索中のフィールドを全てクリアします">
                                    <Badge
                                        count={filledSearchFieldCount}
                                        size="small">
                                        <Button
                                            type="primary"
                                            size="small"
                                            icon={<CloseOutlined />}
                                            onClick={onReset}
                                        />
                                    </Badge>
                                </Tooltip>
                            ) : undefined}
                        </Button.Group>
                    </Row>
                </Col>
                <SearchMenuDrawer
                    isDrawerOpen={isSearchDrawerOpen}
                    onDrawerClose={onDrawerClose}
                    form={form}
                    menuItems={menuItems}
                    unshowList={unshowSearchFields}
                    setInitialFormSearch={setInitialFormSearch}
                    setUnshowList={setUnshowSearchFields}
                    onReset={onReset}
                    onUpdateUnshowList={onUpdateSearchField}
                    otherControls={[
                        <IncludeInvalidSwitchFormItem
                            initialValue={initialData.ignore_filter}
                        />,
                        <IncludeBlocklistSwitchFormItem
                            initialValue={initialData.ignore_blocklist_filter}
                        />,
                    ]}
                    onFilter={submitToFilter}
                    searchTemplate={
                        <SearchTemplateSelectFormItem
                            pageId={pageId}
                            searchTemplateUrl={searchTemplateURL}
                            tableName={tableName}
                            reducerName={templateReducerName}
                            onSelect={onTemplateSelect}
                            onDelete={onReset}
                            isDefaultTemplateAttached={
                                isDefaultTemplateAttached
                            }
                            selectedTemplateName={selectedTemplateName}
                            setSelectedTemplateName={setSelectedTemplateName}
                        />
                    }
                    onFieldAdd={onFieldAdd}
                    onFieldRemove={onFieldRemove}
                />
            </>
        );
    }
);

export default ContactSearchForm;
