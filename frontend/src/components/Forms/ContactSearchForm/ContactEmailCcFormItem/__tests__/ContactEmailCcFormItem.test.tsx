import React from "react";
import { render, screen } from "~/test/utils";
import ContactEmailCcFormItem from "../ContactEmailCcFormItem";

const mockOnClear = jest.fn();

describe("ContactEmailCcFormItem", () => {
    test("enabled render test", () => {
        render(<ContactEmailCcFormItem onClear={mockOnClear} />);
        const inputElement = screen.getByPlaceholderText(
            /メールアドレス\(CC\)/
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(<ContactEmailCcFormItem disabled onClear={mockOnClear} />);
        const inputElement = screen.getByPlaceholderText(
            /メールアドレス\(CC\)/
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).toBeDisabled();
    });
});
