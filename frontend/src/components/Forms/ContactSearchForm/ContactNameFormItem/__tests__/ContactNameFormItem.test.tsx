import React from "react";
import { render, screen } from "~/test/utils";
import ContactNameFormItem from "../ContactNameFormItem";

const mockOnClear = jest.fn();

describe("ContactNameFormItem.tsx", () => {
    test("enabled render test", () => {
        render(<ContactNameFormItem onClear={mockOnClear} />);
        const lastNameInputElement = screen.getByPlaceholderText(
            /取引先担当者名\(姓\)/
        ) as HTMLInputElement;
        expect(lastNameInputElement).toBeInTheDocument();
        expect(lastNameInputElement.value).toBeFalsy();
        expect(lastNameInputElement).not.toBeDisabled();
        const firstNameInputElement = screen.getByPlaceholderText(
            /取引先担当者名\(名\)/
        ) as HTMLInputElement;
        expect(firstNameInputElement).toBeInTheDocument();
        expect(firstNameInputElement.value).toBeFalsy();
        expect(firstNameInputElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(<ContactNameFormItem disabled onClear={mockOnClear} />);
        const lastNameInputElement = screen.getByPlaceholderText(
            /取引先担当者名\(姓\)/
        ) as HTMLInputElement;
        expect(lastNameInputElement).toBeInTheDocument();
        expect(lastNameInputElement.value).toBeFalsy();
        expect(lastNameInputElement).toBeDisabled();
        const firstNameInputElement = screen.getByPlaceholderText(
            /取引先担当者名\(名\)/
        ) as HTMLInputElement;
        expect(firstNameInputElement).toBeInTheDocument();
        expect(firstNameInputElement.value).toBeFalsy();
        expect(firstNameInputElement).toBeDisabled();
    });
});
