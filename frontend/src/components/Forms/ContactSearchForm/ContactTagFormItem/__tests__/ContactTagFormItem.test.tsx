import React from "react";
import {
    renderHook,
    renderWithQueryClient,
    screen,
    userEvent,
} from "~/test/utils";
import ContactTagFormItem from "../ContactTagFormItem";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";

describe("ContactTagFormItem.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("enabled render test", async () => {
        renderWithQueryClient(<ContactTagFormItem />);
        const tagSelectPlaceholderElement = screen.getByText(/タグ/);
        expect(tagSelectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(tagSelectPlaceholderElement);
        const tagSelectOptionElement = await screen.findByText(/django/);
        expect(tagSelectOptionElement).toBeInTheDocument();
        const conditionSelectPlaceholderElement = screen.getByText(/条件/);
        expect(conditionSelectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(conditionSelectPlaceholderElement);
        const conditionOrSelectOptionElement = screen.getByText(/OR/);
        expect(conditionOrSelectOptionElement).toBeInTheDocument();
        const conditionAndSelectOptionElement = screen.getByText(/AND/);
        expect(conditionAndSelectOptionElement).toBeInTheDocument();
    });

    test("disabled render test", async () => {
        renderWithQueryClient(<ContactTagFormItem disabled />);
        const tagSelectPlaceholderElement = screen.getByText(/タグ/);
        expect(tagSelectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(tagSelectPlaceholderElement);
        const tagSelectOptionElement = screen.queryByText(/django/);
        expect(tagSelectOptionElement).not.toBeInTheDocument();
        const conditionSelectPlaceholderElement = screen.getByText(/条件/);
        expect(conditionSelectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(conditionSelectPlaceholderElement);
        const conditionOrSelectOptionElement = screen.queryByText(/OR/);
        expect(conditionOrSelectOptionElement).not.toBeInTheDocument();
        const conditionAndSelectOptionElement = screen.queryByText(/AND/);
        expect(conditionAndSelectOptionElement).not.toBeInTheDocument();
    });
});
