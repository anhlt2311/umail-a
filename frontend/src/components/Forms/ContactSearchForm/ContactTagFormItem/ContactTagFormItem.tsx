import React from "react";
import { Col, Form, Row, Tooltip, Select } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { Links, iconCustomColor } from "~/utils/constants";
import TagAjaxSelect from "~/components/Common/TagAjaxSelect/TagAjaxSelect";
import styles from "./ContactTagFormItem.scss";

type Props = {
    disabled?: boolean;
};

const ContactTagFormItem = ({ disabled }: Props) => {
    const tagsSuffixFieldName = "tags__suffix";
    const tagsFieldName = "tags";

    const searchConditions = [
        {
            value: "and",
            title: "AND",
        },
        {
            value: "or",
            title: "OR",
        },
    ];

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item className={styles.field} colon={false} noStyle>
                        <Row className={styles.container}>
                            <Col span={24}>
                                <Row>
                                    <Col span={18}>
                                        <Form.Item
                                            label=" "
                                            className={styles.inlineField}
                                            colon={false}
                                            name={tagsFieldName}
                                            noStyle>
                                            <TagAjaxSelect
                                                placeholder="タグ"
                                                disabled={disabled}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col span={6}>
                                        <Form.Item
                                            colon={false}
                                            name={tagsSuffixFieldName}
                                            noStyle>
                                            <Select
                                                placeholder="条件"
                                                className={styles.container}
                                                disabled={disabled}>
                                                {searchConditions.map(
                                                    (condition) => {
                                                        return (
                                                            <Select.Option
                                                                key={
                                                                    condition.value
                                                                }
                                                                value={
                                                                    condition.value
                                                                }>
                                                                {
                                                                    condition.title
                                                                }
                                                            </Select.Option>
                                                        );
                                                    }
                                                )}
                                            </Select>
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                左側の入力欄には検索対象とするタグを入力します。
                                <br />
                                右側の入力欄には検索方式を入力します。
                                <br />
                                ANDを入力した場合の例は
                                <a
                                    href={Links.helps.filter.and}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    こちら
                                </a>
                                <br />
                                ORを入力した場合の例は
                                <a
                                    href={Links.helps.filter.or}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    こちら
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ContactTagFormItem;
