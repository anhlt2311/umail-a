import React from "react";
import { render, screen } from "~/test/utils";
import IncludeBlocklistSwitchFormItem from "../IncludeBlocklistSwitchFormItem";

describe("IncludeBlocklistSwitchFormItem.tsx", () => {
    test("render test", () => {
        render(<IncludeBlocklistSwitchFormItem initialValue={undefined} />);
        const includeInvalidLabelElement =
            screen.getByText(/ブロックリスト対象含/);
        expect(includeInvalidLabelElement).toBeInTheDocument();
        const includeINvalidSwitchElement = screen.getByRole("switch");
        expect(includeINvalidSwitchElement).toBeInTheDocument();
    });
});
