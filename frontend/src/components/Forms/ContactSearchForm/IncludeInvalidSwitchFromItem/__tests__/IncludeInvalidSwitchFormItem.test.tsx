import React from "react";
import { render, screen } from "~/test/utils";
import IncludeInvalidSwitchFormItem from "../IncludeInvalidSwitchFormItem";

describe("IncludeInvalidSwitchFormItem.tsx", () => {
    test("render test", () => {
        render(<IncludeInvalidSwitchFormItem initialValue={undefined} />);
        const includeInvalidLabelElement =
            screen.getByText(/自社取引条件対象外含/);
        expect(includeInvalidLabelElement).toBeInTheDocument();
        const includeINvalidSwitchElement = screen.getByRole("switch");
        expect(includeINvalidSwitchElement).toBeInTheDocument();
    });
});
