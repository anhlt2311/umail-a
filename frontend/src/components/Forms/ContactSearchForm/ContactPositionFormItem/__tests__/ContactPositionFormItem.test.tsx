import React from "react";
import { render, screen } from "~/test/utils";
import ContactPositionFormItem from "../ContactPositionFormItem";

const mockOnClear = jest.fn();

describe("ContactPositionFormItem", () => {
    test("enabled render test", () => {
        render(<ContactPositionFormItem onClear={mockOnClear} />);
        const inputElement = screen.getByPlaceholderText(
            /役職/
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(<ContactPositionFormItem disabled onClear={mockOnClear} />);
        const inputElement = screen.getByPlaceholderText(
            /役職/
        ) as HTMLInputElement;
        expect(inputElement).toBeInTheDocument();
        expect(inputElement.value).toBeFalsy();
        expect(inputElement).toBeDisabled();
    });
});
