import React from "react";
import { render, screen } from "~/test/utils";
import ContactLastVisitFormItem from "../ContactLastVisitFormItem";

describe("ContactLastVisitFormItem.tsx", () => {
    test("enabled render test", () => {
        render(<ContactLastVisitFormItem />);
        const startDateInputElement =
            screen.getByPlaceholderText("最終訪問日(開始)");
        expect(startDateInputElement).toBeInTheDocument();
        expect(startDateInputElement).not.toBeDisabled();
        const endDateInputElement =
            screen.getByPlaceholderText("最終訪問日(終了)");
        expect(endDateInputElement).toBeInTheDocument();
        expect(endDateInputElement).not.toBeDisabled();
    });
    test("disabled render test", () => {
        render(<ContactLastVisitFormItem disabled />);
        const startDateInputElement =
            screen.getByPlaceholderText("最終訪問日(開始)");
        expect(startDateInputElement).toBeInTheDocument();
        expect(startDateInputElement).toBeDisabled();
        const endDateInputElement =
            screen.getByPlaceholderText("最終訪問日(終了)");
        expect(endDateInputElement).toBeInTheDocument();
        expect(endDateInputElement).toBeDisabled();
    });
});
