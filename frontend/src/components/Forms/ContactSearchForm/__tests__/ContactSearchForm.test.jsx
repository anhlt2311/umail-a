import React from "react";
import { configureStore } from "@reduxjs/toolkit";
import { generateRandomToken } from "~/utils/utils";
import ContactSearchForm from "../ContactSearchForm";
import { renderWithAllProviders, screen, userEvent } from "~/test/utils";
import login, { LoginInitialState } from "~/reducers/login";
import { contactSearchPage } from "~/reducers/pages";
import { defaultInitialState as SearchPageInitialState } from "~/reducers/Factories/searchPage";
import { displaySettingPage } from "~/reducers/pages";
import { searchConditionSanitizer } from "~/components/Pages/ContactSearchPage/ContactSearchPage";

const mockSubmitHandler = jest.fn();

describe("ContactSearchForm.jsx", () => {
    test("render test", async () => {
        renderWithAllProviders(
            <ContactSearchForm
                tableName="contactSearchPage"
                submitHandler={mockSubmitHandler}
                isDefaultTemplateAttached={false}
                searchConditionSanitizer={searchConditionSanitizer}
            />,
            {
                store: configureStore({
                    reducer: {
                        login,
                        displaySettingPage,
                        contactSearchPage,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            token: generateRandomToken(),
                        },
                        displaySettingPage: {
                            requireRefresh: false,
                            canNotDelete: false,
                            loading: false,
                            updated: false,
                            deleted: false,
                            message: "",
                            errorMessage: "",
                            data: {},
                            fieldErrors: {},
                            displaySetting: {
                                users: {
                                    search: [],
                                },
                            },
                            isDisplaySettingLoading: false,
                        },
                        contactSearchPage: {
                            ...SearchPageInitialState,
                        },
                    },
                }),
            }
        );
        const searchButtonElement = screen.getByText(/絞り込み検索/);
        expect(searchButtonElement).toBeInTheDocument();
        expect(searchButtonElement).not.toBeDisabled();
        await userEvent.click(searchButtonElement);
        const searchFieldShowAreaTitleElement = screen.getByText(/^検索対象$/);
        expect(searchFieldShowAreaTitleElement).toBeInTheDocument();
        const searchFieldUnshowAreaTitleElement =
            screen.getByText(/^検索対象外$/);
        expect(searchFieldUnshowAreaTitleElement).toBeInTheDocument();
        const resetButtonElement = screen.getByRole("button", {
            name: /検索条件をリセット/,
        });
        expect(resetButtonElement).toBeInTheDocument();
        const outOfConditionSwitchLabelElement =
            screen.getByText(/自社取引条件対象外含/);
        expect(outOfConditionSwitchLabelElement).toBeInTheDocument();
        const blockListSwitchLabelElement =
            screen.getByText(/ブロックリスト対象含/);
        expect(blockListSwitchLabelElement).toBeInTheDocument();
        const openButtonElement = screen.getByTestId("open-button");
        await userEvent.click(openButtonElement);
        const contactLastNameInputElement =
            screen.getByPlaceholderText(/取引先担当者名\(姓\)/);
        expect(contactLastNameInputElement).toBeInTheDocument();
        const contactFirstNameInputElement =
            screen.getByPlaceholderText(/取引先担当者名\(名\)/);
        expect(contactFirstNameInputElement).toBeInTheDocument();
        const assignedOrganizationInputElement =
            screen.getByPlaceholderText(/所属取引先/);
        expect(assignedOrganizationInputElement).toBeInTheDocument();
        const emailToInputElement =
            screen.getByPlaceholderText(/メールアドレス\(TO\)/);
        expect(emailToInputElement).toBeInTheDocument();
        const emailCcInputElement =
            screen.getByPlaceholderText(/メールアドレス\(CC\)/);
        expect(emailCcInputElement).toBeInTheDocument();
        const tel1InputElement = screen.getByPlaceholderText(/TEL/);
        expect(tel1InputElement).toBeInTheDocument();
        const tel2InputElement = screen.getByTestId(/tel2/);
        expect(tel2InputElement).toBeInTheDocument();
        const tel3InputElement = screen.getByTestId(/tel3/);
        expect(tel3InputElement).toBeInTheDocument();
        const positionInputElement = screen.getByPlaceholderText(/役職/);
        expect(positionInputElement).toBeInTheDocument();
        const departmentInputElement = screen.getByPlaceholderText(/部署/);
        expect(departmentInputElement).toBeInTheDocument();
        const staffSelectPlaceholderElement = screen.getByText(/自社担当者/);
        expect(staffSelectPlaceholderElement).toBeInTheDocument();
        const startDateInputElement =
            screen.getByPlaceholderText("最終訪問日(開始)");
        expect(startDateInputElement).toBeInTheDocument();
        const endDateInputElement =
            screen.getByPlaceholderText("最終訪問日(終了)");
        expect(endDateInputElement).toBeInTheDocument();
        const tagSelectPlaceholderElement = screen.getByText(/タグ/);
        expect(tagSelectPlaceholderElement).toBeInTheDocument();
        const categorySelectPlaceholderElement = screen.getByText(/相性/);
        expect(categorySelectPlaceholderElement).toBeInTheDocument();
        const contactPreferenceSelectPlaceholderElement =
            screen.getByText(/配信種別/);
        expect(contactPreferenceSelectPlaceholderElement).toBeInTheDocument();
        const areaSelectPlaceholderElement = screen.getByText(/希望エリア/);
        expect(areaSelectPlaceholderElement).toBeInTheDocument();
        const commenterSelectPlaceholderElement =
            screen.getByText(/コメント作成者/);
        expect(commenterSelectPlaceholderElement).toBeInTheDocument();
        const pageCreatorSelectPlaceholderElement =
            screen.getByText(/ページ作成者/);
        expect(pageCreatorSelectPlaceholderElement).toBeInTheDocument();
        const pageEditorSelectPlaceholderElement =
            screen.getByText(/ページ編集者/);
        expect(pageEditorSelectPlaceholderElement).toBeInTheDocument();
        const drawerCancelButtonElement = screen.getByText(/キャンセル/);
        expect(drawerCancelButtonElement).toBeInTheDocument();
        expect(drawerCancelButtonElement).not.toBeDisabled();
    });
});
