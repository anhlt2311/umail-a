import React from "react";
import { Form, Input, Tooltip, FormItemProps } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import {
    ErrorMessages,
    RESTRICT_SPACE_REGEX,
    iconCustomColor,
    HANKAKU_USERID_REGEX,
    SPECIAL_CHARACTER_USERID,
} from "~/utils/constants";
import styles from "./UserServiceIdFormItem.scss";

type Props = FormItemProps & {};

const UserServiceIdFormItem = ({ ...props }: Props) => {
    return (
        <Form.Item
            name="user_service_id"
            {...props}
            label={
                <span>
                    ユーザーID&nbsp;
                    <Tooltip
                        title={
                            <span>
                                ここで設定したユーザーIDはメンション機能で使用されます。
                                <br />
                                15文字以内の大小英数字とアンダースコア(_)のみ使用可能です。
                                <br />
                                また、テナント内で同一のユーザーIDを設定することはできません。
                            </span>
                        }>
                        <QuestionCircleFilled
                            style={{ color: iconCustomColor }}
                            data-testid="user-service-id-tooltip"
                        />
                    </Tooltip>
                </span>
            }
            className={styles.field}
            rules={[
                {
                    required: true,
                    message: ErrorMessages.form.required,
                },
                {
                    pattern: HANKAKU_USERID_REGEX,
                    message:
                        ErrorMessages.validation.regex
                            .onlyHankakuNumberAndUnderScore,
                },
                {
                    max: 15,
                    message: ErrorMessages.validation.length.maxUserId15,
                },
                {
                    pattern: RESTRICT_SPACE_REGEX,
                    message: ErrorMessages.validation.regex.space,
                },
                {
                    validator(_, value) {
                        if (value && SPECIAL_CHARACTER_USERID.includes(value)) {
                            return Promise.reject(
                                new Error(`${value}は使用できません。`)
                            );
                        }
                        return Promise.resolve();
                    },
                },
            ]}>
            <Input allowClear data-testid="user-service-id-input" />
        </Form.Item>
    );
};

export default UserServiceIdFormItem;
