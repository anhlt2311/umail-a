import React from "react";
import { Col, Row, Form, Switch, Tooltip } from "antd";
import {
    QuestionCircleFilled,
    CheckOutlined,
    CloseOutlined,
} from "@ant-design/icons";
import { iconCustomColor } from "~/utils/constants";
import styles from "./EmailSearchIncludeInvalidSwitchFormItem.scss";
import { Link } from 'react-router-dom';
import Path from '../../../Routes/Paths';

const EmailSearchIncludeInvalidSwitchFormItem = () => {
    const fieldName = "ignore_filter";
    const tooltipMessage = <Link to={`${Path.myCompany}`} target="_blank" rel="noopener noreferrer">自社プロフィール</Link>

    return (
        <Col span={24}>
            <Row className={styles.container} justify="end">
                <Col span={23}>
                    <Row justify="end">
                        <Form.Item
                            colon={false}
                            name={fieldName}
                            valuePropName="checked"
                            noStyle>
                            <Switch
                                style={{ alignSelf: "center" }}
                                checkedChildren={<CheckOutlined />}
                                unCheckedChildren={<CloseOutlined />}
                            />
                        </Form.Item>
                        <label className={styles.switchLabel}>
                            自社取引条件対象外含
                        </label>
                    </Row>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip title={
                        <span>
                            {tooltipMessage}の取引条件を満たしていない取引先は、チェックがないと結果表示されません。
                        </span>
                    }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default EmailSearchIncludeInvalidSwitchFormItem;
