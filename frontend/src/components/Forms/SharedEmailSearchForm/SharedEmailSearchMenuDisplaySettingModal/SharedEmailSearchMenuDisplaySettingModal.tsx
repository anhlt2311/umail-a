import React, { useEffect, useState } from "react";
import { Form } from "antd";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import SearchMenuDisplaySettingModal from "~/components/Modals/SearchMenuDisplaySettingModal/SearchMenuDisplaySettingModal";
import { DISPLAY_SETTING_ITEMS } from "~/utils/constants";
import { useDisplaySettingAPI } from "~/hooks/useDisplaySettingAPI";
import {
    DisplaySettingAPIModel,
    DisplaySettingModel,
} from "~/models/displaySetting";
import _ from "lodash";
import styles from "./SharedEmailSearchMenuDisplaySettingModal.scss";

const SharedEmailSearchMenuDisplaySettingModal = () => {
    const fieldName = "shared_emails";
    const formId = "sharedEmailsSearchDisplaySettingForm";

    const [form] = Form.useForm();
    const { displaySetting }: { displaySetting?: DisplaySettingAPIModel } =
        useSelector((state: RootState) => state.displaySettingPage);

    const [targetKeys, setTargetKeys] = useState<string[]>([]);
    const [selectedKeys, setSelectedKeys] = useState<string[]>([]);

    const { updateDisplaySetting } = useDisplaySettingAPI();

    const setUnshowFields = () => {
        const doesDisplaySettingExist =
            !_.isEmpty(displaySetting) &&
            !_.isEmpty(displaySetting?.shared_emails);
        const searchHiddenItems =
            doesDisplaySettingExist && displaySetting?.shared_emails.search;
        if (searchHiddenItems) {
            setTargetKeys(searchHiddenItems);
        }
    };

    const onUpdate = (values: Partial<DisplaySettingModel>) => {
        updateDisplaySetting(values);
    };

    const onAfterClose = () => {
        setUnshowFields();
        setSelectedKeys([]);
        form.resetFields();
    };

    useEffect(() => {
        setUnshowFields();
    }, [displaySetting]);

    return (
        <SearchMenuDisplaySettingModal
            fieldName={fieldName}
            formId={formId}
            form={form}
            onFinish={onUpdate}
            transferProps={{
                dataSource: DISPLAY_SETTING_ITEMS.shared_emails.search,
                targetKeys,
                selectedKeys,
                onChange: (nextTargetKeys) => {
                    setTargetKeys(nextTargetKeys);
                },
                onSelectChange: (sourceSelectedKeys, targetSelectedKeys) => {
                    setSelectedKeys([
                        ...sourceSelectedKeys,
                        ...targetSelectedKeys,
                    ]);
                },
            }}
            onAfterClose={onAfterClose}
        />
    );
};

export default SharedEmailSearchMenuDisplaySettingModal;
