import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Col, Form, Row, Spin, Select, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { simpleFetch } from "~/actions/data";
import { Endpoint } from "~/domain/api";
import { DisplayTextAndForeignKeyModel } from "~/models/displayTextAndForeignKeyModel";
import { RootState } from "~/models/store";
import styles from "./EmailSearchStaffFormItem.scss";
import { iconCustomColor } from "~/utils/constants";

const { Option } = Select;

const EmailSearchStaffFormItem = () => {
    const fieldName = "staff";
    const resourceUrl = `${Endpoint.getBaseUrl()}/${
        Endpoint.users
    }?is_active=true`;

    const token = useSelector((state: RootState) => state.login.token);
    const [staffs, setStaffs] = useState<DisplayTextAndForeignKeyModel[]>([]);
    const [isLoading, setIsLoading] = useState(false);

    const fetchStaffs = async () => {
        setIsLoading(true);
        try {
            const response = await simpleFetch(token, resourceUrl);
            const results = response.results || [];
            const staffs = results.map((result: any) => {
                return {
                    displayText: result["display_name"],
                    foreignKey: result.id,
                };
            });
            setStaffs(staffs);
        } catch (err) {
            console.error(err);
        }
        setIsLoading(false);
    };
    const partialMatchFilter = (input: string, option: any) => {
        return (
            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >=
            0
        );
    };

    useEffect(() => {
        fetchStaffs();
    }, []);

    const renderFormItem = () => {
        return (
            <Form.Item colon={false} noStyle>
                <Row className={styles.container}>
                    <Col span={24}>
                        <Spin size="small" spinning={isLoading}>
                            <Form.Item
                                className={styles.inlineField}
                                colon={false}
                                name={fieldName}
                                noStyle>
                                <Select
                                    className={styles.container}
                                    showSearch
                                    filterOption={partialMatchFilter}
                                    allowClear
                                    placeholder="自社担当者"
                                    mode="multiple">
                                    {staffs.map((staff) => {
                                        return (
                                            <Select.Option
                                                key={staff.foreignKey}
                                                value={staff.foreignKey}>
                                                {staff.displayText}
                                            </Select.Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        </Spin>
                    </Col>
                </Row>
            </Form.Item>
        );
    };

    return (
        <Col span={7} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={22}>
                    <Row>
                        <Col span={23}>{renderFormItem()}</Col>
                        <Col span={1} className={styles.infoIcon}>
                            <Tooltip
                                title={
                                    <span>
                                        複数選択するとOR検索となります。
                                        <br />
                                        また、無効化しているユーザーは選択表示されません。
                                    </span>
                                }>
                                <QuestionCircleFilled
                                    style={{ color: iconCustomColor }}
                                    className={styles.tooltip}
                                />
                            </Tooltip>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Col>
    );
};

export default EmailSearchStaffFormItem;
