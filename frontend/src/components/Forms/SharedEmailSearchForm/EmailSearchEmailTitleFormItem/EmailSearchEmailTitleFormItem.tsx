import React from "react";
import { Col, Form, Input, Tooltip, Row } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import styles from "./EmailSearchEmailTitleFormItem.scss";
import { iconCustomColor } from "~/utils/constants";

type Props = {
    onClear: (fieldName: string, value: string) => void;
};

const EmailSearchEmailTitleFormItem = ({ onClear }: Props) => {
    const fieldName = "subject";

    return (
        <Col span={7} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <Input
                            className={styles.userInput}
                            placeholder="件名"
                            allowClear
                            onChange={(event) => {
                                const value = event.target.value;
                                if (value === "") {
                                    onClear(fieldName, "");
                                }
                            }}
                        />
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                スペース区切りの部分一致検索。スペース区切りはANDとなります。
                                <br />
                                例：Java 開発
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default EmailSearchEmailTitleFormItem;
