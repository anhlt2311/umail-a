import React from "react";
import { Col, Form, Row } from "antd";
import styles from "./EmailSearchRecieveDateFormItem.scss";
import CustomRangePicker from "~/components/Common/CustomRangePicker/CustomRangePicker";

const EmailSearchRecieveDateFormItem = () => {
    const fieldName = "date_range";

    return (
        <Col span={7} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={24}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <CustomRangePicker
                            className={styles.container}
                            placeholder={["受信日(開始)", "受信日(終了)"]}
                        />
                    </Form.Item>
                </Col>
            </Row>
        </Col>
    );
};

export default EmailSearchRecieveDateFormItem;
