import React, { useEffect, useState } from "react";
import { Select, SelectProps, Spin } from "antd";
import { simpleFetch } from "../../../actions/data";
import { DisplayTextAndForeignKeyModel } from "~/models/displayTextAndForeignKeyModel";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";
import { Endpoint } from "~/domain/api";

const { Option } = Select;

type BranchModel = {
    id: string;
    name: string;
};

type OrganizationModel = {
    id: string;
    name: string;
    branches: BranchModel[];
};

type Props = SelectProps<any> & {
    defaultSelect?: DisplayTextAndForeignKeyModel;
};

const OrganizationAjaxSelect = ({
    defaultSelect,
    placeholder = "クリックして選択",
    ...props
}: Props) => {
    const pageSize = 100;
    const token = useSelector((state: RootState) => state.login.token);
    const [items, setItems] = useState<DisplayTextAndForeignKeyModel[]>([]);
    const [isFetching, setIsFetching] = useState(true);
    const [searchValue, setSearchValue] = useState("");
    const [value, setValues] = useState<string | undefined>(undefined);
    const [branchCount, setBranchCount] = useState(0);

    const currentPage = (
        items: DisplayTextAndForeignKeyModel[],
        defaultSelect?: DisplayTextAndForeignKeyModel
    ) => {
        let easyPage = 0;
        if (defaultSelect) {
            easyPage = (items.length - 1 - branchCount) / pageSize;
        } else {
            easyPage = items.length / pageSize;
        }
        if (!easyPage) {
            return 1;
        }
        const page = Math.ceil(easyPage) + 1;
        return page;
    };

    const fetchData = async (page: number, value = "") => {
        const resourceURL = `${Endpoint.getBaseUrl()}/${
            Endpoint.organizations
        }/names`;
        try {
            let url = `${resourceURL}?page_size=${pageSize}&page=${page}&name=${value}`;
            const response: { results: OrganizationModel[] } =
                await simpleFetch(token, url);
            const data = response.results;

            const flatOrganizationDatas: DisplayTextAndForeignKeyModel[] = [];

            for (const organization of data) {
                flatOrganizationDatas.push({
                    displayText: organization.name,
                    foreignKey: organization.id,
                });
                if (organization.branches.length) {
                    setBranchCount(branchCount + organization.branches.length);
                    for (const branch of organization.branches) {
                        flatOrganizationDatas.push({
                            displayText: `${organization.name} - ${branch.name}`,
                            foreignKey: `${organization.id}/${branch.id}`,
                        });
                    }
                }
            }
            const isSearchChanged = searchValue !== value;
            let newItems = [];
            if (isSearchChanged) {
                newItems = [...flatOrganizationDatas];
            } else {
                newItems = [...items, ...flatOrganizationDatas];
            }
            if (defaultSelect === undefined) {
                setItems(newItems);
            } else {
                const apiValues = newItems.map((item) => item.foreignKey);
                if (
                    defaultSelect.displayText &&
                    defaultSelect.foreignKey &&
                    !apiValues.includes(defaultSelect.foreignKey)
                ) {
                    newItems = [defaultSelect, ...newItems];
                }

                setItems(newItems);
            }
        } catch (err) {
            // NOTE(joshua-hashimoto): あえて何もしない
        }
        setIsFetching(false);
    };

    const fetchDataFromAPI = useCustomDebouncedCallback(
        (value) => fetchData(currentPage(items, defaultSelect), value),
        1500
    );
    const searchAPIValue = useCustomDebouncedCallback((value) => {
        if (searchValue !== value) {
            fetchData(1, value);
        } else {
            fetchData(currentPage(items, defaultSelect), value);
        }
        setSearchValue(value);
    });

    const onScroll = (event: any) => {
        const target = event.target;
        if (target.scrollTop + target.offsetHeight === target.scrollHeight) {
            setIsFetching(true);
            fetchDataFromAPI(searchValue);
        }
    };

    useEffect(() => {
        fetchDataFromAPI();
        if (defaultSelect) {
            setValues(defaultSelect.foreignKey);
        }
    }, [defaultSelect]);

    return (
        <Spin size="small" spinning={isFetching}>
            <Select
                showSearch
                filterOption={(input, option) => {
                    return (
                        option?.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                    );
                }} // Search by displayName, partial match.
                style={{ width: "100%" }}
                {...props}
                value={value}
                placeholder={placeholder}
                onPopupScroll={onScroll}
                dropdownRender={(originNode) => (
                    <Spin spinning={isFetching}>{originNode}</Spin>
                )}
                onSearch={searchAPIValue}
                onBlur={(_) => {
                    if (searchValue) {
                        searchAPIValue("");
                    }
                }}
                
                onSelect={setValues}>
                {items.map((entry) => (
                    <Select.Option key={entry.foreignKey} value={entry.foreignKey}>
                        {entry.displayText}
                    </Select.Option>
                ))}
            </Select>
        </Spin>
    );
};

export default OrganizationAjaxSelect;
