import React, { Component } from "react";
import PropTypes from "prop-types";
import { Row, Col, Select, Form, Button, Tooltip } from "antd";

import {
    DeleteOutlined,
    StarOutlined,
    SaveOutlined,
    QuestionCircleFilled,
} from "@ant-design/icons";
import SaveSearchTemplateModal from "~/components/Feedbacks/Modal/SaveSearchTemplateModal/SaveSearchTemplateModal";
import {iconCustomColor} from "~/utils/constants";
import { connect } from "react-redux";

class SearchTemplate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSaveTemplateModalVisible: false,
        };
    }

    componentDidUpdate() {
        const {
            scheduledEmailEditPageContactSearchFormState:
                editSearchTemplateState,
            scheduledEmailRegisterPageContactSearchFormState:
                registerSearchTemplateState,
        } = this.props;
        const { loading: editLoading } = editSearchTemplateState;
        const { loading: registerLoading } = registerSearchTemplateState;
    }

    onOk = ({ newTemplateName }) => {
        this.props.onClickCreate(newTemplateName),
            this.setState({
                isSaveTemplateModalVisible: false,
            });
    };

    onCancel = () => {
        this.setState({
            isSaveTemplateModalVisible: false,
        });
    };

    render() {
        const { isSaveTemplateModalVisible } = this.state;
        const {
            selectedValue,
            scheduledEmailEditPageContactSearchFormState,
            scheduledEmailRegisterPageContactSearchFormState,
            onChangeSearchTemplate,
            initialValues,
            onClickDelete,
            onClickUpdate,
            totalAvailableCount,
            templateReducerName,
        } = this.props;
        const { loading: editLoading } =
            scheduledEmailEditPageContactSearchFormState;
        const { loading: registerLoading } =
            scheduledEmailRegisterPageContactSearchFormState;
        return (
            <Col span={24} style={{ textAlign: "left" }}>
                <Row justify="end">
                    <Col>
                        <Form.Item noStyle>
                            <Select
                                placeholder="検索条件テンプレートを選択"
                                style={
                                    selectedValue
                                        ? { width: 300 }
                                        : { width: 300, color: "#bfbfbf" }
                                }
                                onChange={onChangeSearchTemplate}
                                allowClear
                                value={
                                    selectedValue ||
                                    "検索条件テンプレートを選択"
                                }>
                                {(initialValues || []).map((entry, index) => {
                                    return (
                                        <Select.Option
                                            key={index}
                                            value={entry.name}>
                                            {entry.display_name}
                                        </Select.Option>
                                    );
                                })}
                            </Select>
                            <Tooltip
                                title={
                                    "選択中の検索条件テンプレートを削除します。"
                                }>
                                <Button
                                    style={{ margin: 3 }}
                                    type="primary"
                                    danger
                                    icon={<DeleteOutlined />}
                                    onClick={onClickDelete}
                                    disabled={
                                        !selectedValue ||
                                        editLoading ||
                                        registerLoading
                                    }
                                />
                            </Tooltip>
                            <Tooltip
                                title={
                                    "選択中の検索条件テンプレートをデフォルトに設定／解除します。"
                                }>
                                <Button
                                    style={{ margin: 3 }}
                                    type="primary"
                                    icon={<StarOutlined />}
                                    onClick={onClickUpdate}
                                    disabled={!selectedValue || editLoading || registerLoading}
                                />
                            </Tooltip>
                            <Tooltip
                                title={
                                    initialValues &&
                                    totalAvailableCount &&
                                    totalAvailableCount <= initialValues.length
                                        ? "登録可能な上限件数に達しています。"
                                        : "現在の検索条件を保存します。"
                                }>
                                <Button
                                    style={{ margin: 3 }}
                                    type="primary"
                                    icon={<SaveOutlined />}
                                    onClick={(_) =>
                                        this.setState({
                                            isSaveTemplateModalVisible: true,
                                        })
                                    }
                                    disabled={
                                        editLoading ||
                                        registerLoading ||
                                        (initialValues &&
                                            totalAvailableCount &&
                                            totalAvailableCount <=
                                                initialValues.length)
                                    }
                                />
                            </Tooltip>
                            <Tooltip
                                title={
                                    <span>
                                        テンプレートはユーザー個人ごとに保存され、他のユーザーとは共有されません。
                                    </span>
                                }>
                                <QuestionCircleFilled style={{ color: iconCustomColor }} />
                            </Tooltip>
                            <SaveSearchTemplateModal
                                templateReducerName={templateReducerName}
                                isSaveTemplateModalVisible={
                                    isSaveTemplateModalVisible
                                }
                                onCancel={this.onCancel}
                                onOk={this.onOk}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        scheduledEmailRegisterPageContactSearchFormState:
            state.scheduledEmailRegisterPageContactSearchForm,
        scheduledEmailEditPageContactSearchFormState:
            state.scheduledEmailEditPageContactSearchForm,
    };
};

SearchTemplate.propTypes = {
    // will be used in child class.
    templateReducerName: PropTypes.string.isRequired,
    onClickDelete: PropTypes.func.isRequired,
    onClickUpdate: PropTypes.func.isRequired,
    onClickCreate: PropTypes.func.isRequired,
    onChangeSearchTemplate: PropTypes.func.isRequired,
    selectedValue: PropTypes.string,
    initialValues: PropTypes.arrayOf(PropTypes.object),
    totalAvailableCount: PropTypes.number,
};

export default connect(mapStateToProps)(SearchTemplate);
