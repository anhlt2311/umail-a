import React, { ChangeEvent, ReactNode, useEffect, useState } from "react";
import { Button, Col, Form, Input, Row, Tooltip } from "antd";
import {
    PushpinFilled,
    PushpinOutlined,
    SnippetsOutlined,
} from "@ant-design/icons";
import styles from "./CommentForm.scss";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages, primaryColor } from "~/utils/constants";
import ValidateTextArea from "~/components/Common/ValidateTextArea/ValidateTextArea";
const { TextArea } = Input;

type Props = {
    value: string;
    onInputChange: (event: ChangeEvent<HTMLTextAreaElement>) => void;
    isPinned: boolean;
    onPinnedChange: () => void;
    onSelectCommentTemplate: () => void;
    commentError?: string;
    children: ReactNode;
    disableIconPin: boolean;
};

const CommentForm = ({
    value,
    onInputChange,
    isPinned,
    onPinnedChange,
    onSelectCommentTemplate,
    commentError = "",
    children,
    disableIconPin,
}: Props) => {
    const [form] = Form.useForm();
    const {
        create: createAuthorization,
        update: updateAuthorization,
        pin: pinAuthorization,
    } = useAuthorizedActions("comment");
    const disabled = !createAuthorization || !updateAuthorization;
    const [contentTextArea, setContentTextArea] = useState("");
    useEffect(() => {
        form.setFieldsValue({
            comment: value,
        });
        setContentTextArea(value);
    }, [value]);

    const renderTextArea = () => {
        return (
          <>
            <Form.Item
                name="comment"
                className="validate-textarea"
                validateStatus={!!commentError ? "error" : undefined}
                help={!!commentError ? commentError : undefined}
                rules={[
                    {
                        max: 1000,
                        message: ErrorMessages.validation.length.max1000,
                    },
                ]}>
                <TextArea
                    rows={5}
                    autoSize={{ minRows: 5 }}
                    onChange={(inputValue) => {
                        const commentValue = inputValue.target.value;
                        onInputChange(inputValue);
                        setContentTextArea(commentValue);
                    }}
                    disabled={disabled}
                    autoFocus={!!value}
                />
            </Form.Item>
            <ValidateTextArea
                contentTextArea={contentTextArea}
                errorMessages={ErrorMessages.validation.length.max1000}
                margin="0px"
                className="validate-textarea-error"
                extraErrorMessage={commentError}
                />
            </>
        );
    };
    return (
        <Col span={24} className={styles.editor}>
            <Row>
                <Col span={24}>
                    <Form form={form} component="div">
                        {disabled ? (
                            <Tooltip title={"特定の権限で操作できます"}>
                                {renderTextArea()}
                            </Tooltip>
                        ) : (
                            <>{renderTextArea()}</>
                        )}

                        <Form.Item>
                            {!disableIconPin && (
                                <Tooltip
                                    title={
                                        pinAuthorization
                                            ? "コメントを固定"
                                            : ErrorMessages.isNotAuthorized
                                    }>
                                    <Button
                                        className={
                                            isPinned
                                                ? styles.controlButton
                                                : styles.colorFocus
                                        }
                                        size="small"
                                        onClick={onPinnedChange}
                                        icon={
                                            isPinned ? (
                                                <PushpinFilled />
                                            ) : (
                                                <PushpinOutlined />
                                            )
                                        }
                                        type={"primary"}
                                        disabled={!pinAuthorization}
                                    />
                                </Tooltip>
                            )}
                            <Tooltip title="コメントのテンプレート選択">
                                <Button
                                    className={styles.snippetsButton}
                                    size="small"
                                    type="primary"
                                    onClick={() => onSelectCommentTemplate()}
                                    icon={<SnippetsOutlined />}
                                />
                            </Tooltip>
                        </Form.Item>
                        {children}
                    </Form>
                </Col>
            </Row>
        </Col>
    );
};

export default CommentForm;
