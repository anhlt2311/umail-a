import React from "react";
import { Col, Form, Row, Tooltip } from "antd";
import styles from "./ScheduledEmailReservationDateFormItem.scss";
import CustomRangePicker from "~/components/Common/CustomRangePicker/CustomRangePicker";
import { QuestionCircleFilled } from "@ant-design/icons";
import { iconCustomColor } from "~/utils/constants";

type Props = {
    disabled?: boolean;
};

const ScheduledEmailReservationDateFormItem = ({ disabled }: Props) => {
    const fieldName = "date_range";

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <CustomRangePicker
                            className={styles.container}
                            placeholder={[
                                "配信予定日(開始)",
                                "配信予定日(終了)",
                            ]}
                            disabled={disabled}
                        />
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                左の入力欄には開始日を入力します。<br />
                                例：「2020-02-04」を入力した場合、2020年2月4日以降の日付で配信予約を設定した配信メールが対象となります。<br />
                                右の入力欄には終了日を入力します。<br />
                                例：「2020-02-04」を入力した場合、2020年2月4日以前の日付で配信予約を設定した配信メールが対象となります。
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ScheduledEmailReservationDateFormItem;
