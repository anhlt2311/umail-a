import React from "react";
import { Col, Form, Row, Input, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { iconCustomColor } from "~/utils/constants";
import styles from "./ScheduledEmailSendTotalCountFormItem.scss";

const ScheduledEmailSendTotalCountFormItem = ({
    disabled = false,
    onClear,
}) => {
    const maxSendTotalCountFieldName = "send_total_count_gt";
    const minSendTotalCountFieldName = "send_total_count_lt";

    return (
        <Col
            span={24}
            style={{
                marginBottom: "1%",
            }}>
            <Form.Item
                colon={false}
                name="send_total_count"
                wrapperCol={{ span: 24 }}
                noStyle>
                <Row className={styles.container} align="middle">
                    <Col span={11}>
                        <Form.Item
                            colon={false}
                            name={maxSendTotalCountFieldName}
                            noStyle>
                            <Input
                                className={styles.userInput}
                                placeholder="配信数(下限)"
                                allowClear
                                onChange={(event) => {
                                    // NOTE(joshua-hashimoto): This does not interrupt Forms onValuesChange
                                    const value = event.target.value;
                                    if (value === "") {
                                        onClear(maxSendTotalCountFieldName, "");
                                    }
                                }}
                                disabled={disabled}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={1}>
                        <Row justify="center">
                            <span>〜</span>
                        </Row>
                    </Col>
                    <Col span={11}>
                        <Form.Item
                            colon={false}
                            name={minSendTotalCountFieldName}
                            noStyle>
                            <Input
                                className={styles.userInput}
                                placeholder="配信数(上限)"
                                allowClear
                                onChange={(event) => {
                                    // NOTE(joshua-hashimoto): This does not interrupt Forms onValuesChange
                                    const value = event.target.value;
                                    if (value === "") {
                                        onClear(minSendTotalCountFieldName, "");
                                    }
                                }}
                                disabled={disabled}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={1} className={styles.infoIcon}>
                        <Tooltip
                            title={
                                <span>
                                    左の入力欄には下限値を入力します。<br />
                                    例：「10」を入力した場合、配信数10件以上が対象となります。<br />
                                    右の入力欄には上限値を入力します。<br />
                                    例：「10」を入力した場合、配信数10件以下が対象となります。
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </Col>
                </Row>
            </Form.Item>
        </Col>
    );
};

export default ScheduledEmailSendTotalCountFormItem;
