import React from "react";
import { Col, Form, Input, Row, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { Links, iconCustomColor } from "~/utils/constants";
import styles from "./ScheduledEmailTextFormItem.scss";

type Props = {
    disabled?: boolean;
    onClear: (fieldName: string, value: string) => void;
};

const ScheduledEmailTextFormItem = ({ disabled, onClear }: Props) => {
    const fieldName = "text";

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <Input
                            className={styles.userInput}
                            placeholder="本文"
                            allowClear
                            onChange={(event) => {
                                const value = event.target.value;
                                if (value === "") {
                                    onClear(fieldName, "");
                                }
                            }}
                            disabled={disabled}
                        />
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                スペース区切りなし：
                                <a
                                    href={Links.helps.filter.partialMatch}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    部分一致検索
                                </a>
                                <br />
                                スペース区切りあり：
                                <a
                                    href={Links.helps.filter.and}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    AND検索
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ScheduledEmailTextFormItem;
