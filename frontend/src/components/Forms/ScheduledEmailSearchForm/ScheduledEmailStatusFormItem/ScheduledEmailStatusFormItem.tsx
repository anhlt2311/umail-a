import React from "react";
import { Col, Form, Row, Tooltip, Select } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import {
    Links,
    SCHEDULED_EMAIL_NOT_STATUS,
    SCHEDULED_EMAIL_STATUS,
    iconCustomColor,
} from "~/utils/constants";
import CustomSelectTag from "~/components/Common/CustomSelectTag/CustomSelectTag";
import styles from "./ScheduledEmailStatusFormItem.scss";

const { Option } = Select;

type Props = {
    disabled?: boolean;
};

const ScheduledEmailStatusFormItem = ({ disabled }: Props) => {
    const fieldName = "status";
    const statuses = [...SCHEDULED_EMAIL_STATUS, ...SCHEDULED_EMAIL_NOT_STATUS];

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <Select
                            className={styles.container}
                            mode="multiple"
                            placeholder="配信ステータス"
                            allowClear
                            tagRender={(props) => {
                                const status = statuses.find(
                                    (status) => status.value === props.value
                                );
                                if (status?.value.includes("draft")) {
                                    return (
                                        <CustomSelectTag
                                            title={status.title}
                                            {...props}
                                        />
                                    );
                                }
                                return (
                                    <CustomSelectTag
                                        color={status?.color}
                                        title={status?.title}
                                        {...props}
                                    />
                                );
                            }}
                            disabled={disabled}>
                            {statuses.map((status) => {
                                return (
                                    <Select.Option
                                        key={status.value}
                                        value={status.value}>
                                        {status.title}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                複数選択をすると
                                <a
                                    href={Links.helps.filter.or}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    OR検索
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ScheduledEmailStatusFormItem;
