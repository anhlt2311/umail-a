import React from "react";
import { Col, Form, Row, Input, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { iconCustomColor } from "~/utils/constants";
import styles from "./ScheduledEmailSendSuccessCountFormItem.scss";

const ScheduledEmailSendSuccessCountFormItem = ({
    disabled = false,
    onClear,
}) => {
    const maxSendSuccessCountFieldName = "send_success_count_gt";
    const minSendSuccessCountFieldName = "send_success_count_lt";

    return (
        <Col
            span={24}
            style={{
                marginBottom: "1%",
            }}>
            <Form.Item
                colon={false}
                name="send_success_count"
                wrapperCol={{ span: 24 }}
                noStyle>
                <Row className={styles.container} align="middle">
                    <Col span={11}>
                        <Form.Item
                            colon={false}
                            name={maxSendSuccessCountFieldName}
                            noStyle>
                            <Input
                                className={styles.userInput}
                                placeholder="成功数(下限)"
                                allowClear
                                onChange={(event) => {
                                    // NOTE(joshua-hashimoto): This does not interrupt Forms onValuesChange
                                    const value = event.target.value;
                                    if (value === "") {
                                        onClear(
                                            maxSendSuccessCountFieldName,
                                            ""
                                        );
                                    }
                                }}
                                disabled={disabled}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={1}>
                        <Row justify="center">
                            <span>〜</span>
                        </Row>
                    </Col>
                    <Col span={11}>
                        <Form.Item
                            colon={false}
                            name={minSendSuccessCountFieldName}
                            noStyle>
                            <Input
                                className={styles.userInput}
                                placeholder="成功数(上限)"
                                allowClear
                                onChange={(event) => {
                                    // NOTE(joshua-hashimoto): This does not interrupt Forms onValuesChange
                                    const value = event.target.value;
                                    if (value === "") {
                                        onClear(
                                            minSendSuccessCountFieldName,
                                            ""
                                        );
                                    }
                                }}
                                disabled={disabled}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={1} className={styles.infoIcon}>
                        <Tooltip
                            title={
                                <span>
                                    左の入力欄は、下限を指定します。
                                    <br />
                                    例：「10」だと成功数10件以上が対象となります。
                                    <br />
                                    右の入力欄は、上限を指定します。
                                    <br />
                                    例：「10」だと成功数10件以下が対象となります。
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </Col>
                </Row>
            </Form.Item>
        </Col>
    );
};

export default ScheduledEmailSendSuccessCountFormItem;
