import React, { useEffect, useState } from "react";
import { Form } from "antd";
import { useSelector } from "react-redux";
import { RootState } from "~/models/store";
import SearchMenuDisplaySettingModal from "~/components/Modals/SearchMenuDisplaySettingModal/SearchMenuDisplaySettingModal";
import { DISPLAY_SETTING_ITEMS } from "~/utils/constants";
import { useDisplaySettingAPI } from "~/hooks/useDisplaySettingAPI";
import {
    DisplaySettingAPIModel,
    DisplaySettingModel,
} from "~/models/displaySetting";
import _ from "lodash";
import styles from "./ScheduledEmailSearchMenuDisplaySettingModal.scss";

const ScheduledEmailSearchMenuDisplaySettingModal = () => {
    const fieldName = "scheduled_mails";
    const formId = "scheduledMailsSearchDisplaySettingForm";

    const [form] = Form.useForm();
    const { displaySetting }: { displaySetting?: DisplaySettingAPIModel } =
        useSelector((state: RootState) => state.displaySettingPage);

    const [targetKeys, setTargetKeys] = useState<string[]>([]);
    const [selectedKeys, setSelectedKeys] = useState<string[]>([]);

    const { updateDisplaySetting } = useDisplaySettingAPI();

    const setUnshowFields = () => {
        const doesDisplaySettingExist =
            !_.isEmpty(displaySetting) &&
            !_.isEmpty(displaySetting?.scheduled_mails);
        const searchHiddenItems =
            doesDisplaySettingExist && displaySetting?.scheduled_mails.search;
        if (searchHiddenItems) {
            setTargetKeys(searchHiddenItems);
        }
    };

    const onUpdate = (values: Partial<DisplaySettingModel>) => {
        updateDisplaySetting(values);
    };

    const onAfterClose = () => {
        setUnshowFields();
        setSelectedKeys([]);
        form.resetFields();
    };

    useEffect(() => {
        setUnshowFields();
    }, [displaySetting]);

    return (
        <SearchMenuDisplaySettingModal
            fieldName={fieldName}
            formId={formId}
            form={form}
            onFinish={onUpdate}
            transferProps={{
                dataSource: DISPLAY_SETTING_ITEMS.scheduled_mails.search,
                targetKeys,
                selectedKeys,
                onChange: (nextTargetKeys) => {
                    setTargetKeys(nextTargetKeys);
                },
                onSelectChange: (sourceSelectedKeys, targetSelectedKeys) => {
                    setSelectedKeys([
                        ...sourceSelectedKeys,
                        ...targetSelectedKeys,
                    ]);
                },
            }}
            onAfterClose={onAfterClose}
        />
    );
};

export default ScheduledEmailSearchMenuDisplaySettingModal;
