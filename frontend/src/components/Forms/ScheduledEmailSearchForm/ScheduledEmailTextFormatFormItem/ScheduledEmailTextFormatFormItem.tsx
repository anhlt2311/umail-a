import React from "react";
import { Col, Form, Select, Row, Tooltip } from "antd";
import { Links, SCHEDULED_EMAIL_TEXT_FORMAT, iconCustomColor } from "~/utils/constants";
import CustomSelectTag from "~/components/Common/CustomSelectTag/CustomSelectTag";
import { QuestionCircleFilled } from "@ant-design/icons";
import styles from "./ScheduledEmailTextFormatFormItem.scss";

const { Option } = Select;

type Props = {
    disabled?: boolean;
};

const ScheduledEmailTextFormatFormItem = ({ disabled }: Props) => {
    const fieldName = "text_format";
    const textFormats = SCHEDULED_EMAIL_TEXT_FORMAT;

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <Select
                            className={styles.container}
                            placeholder="フォーマット"
                            mode="multiple"
                            allowClear
                            tagRender={(props) => {
                                const textFormat = textFormats.find(
                                    (textFormat) =>
                                        textFormat.value === props.value
                                );
                                return (
                                    <CustomSelectTag
                                        color={textFormat?.color}
                                        title={textFormat?.title}
                                        {...props}
                                    />
                                );
                            }}
                            disabled={disabled}>
                            {textFormats.map((textFormat) => {
                                return (
                                    <Select.Option
                                        key={textFormat.value}
                                        value={textFormat.value}>
                                        {textFormat.title}
                                    </Select.Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                複数選択をすると
                                <a
                                    href={Links.helps.filter.or}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    OR検索
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default ScheduledEmailTextFormatFormItem;
