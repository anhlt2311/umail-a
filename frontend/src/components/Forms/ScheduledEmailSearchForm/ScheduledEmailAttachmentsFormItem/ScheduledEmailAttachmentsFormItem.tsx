import React, { ReactNode } from "react";
import { Col, Form, Row, Select } from "antd";
import styles from "./ScheduledEmailAttachmentsFormItem.scss";

const { Option } = Select;

type AttachmentsModel = {
    value: "1" | "2";
    title: string;
    icon?: JSX.Element;
};

type Props = {
    disabled?: boolean;
};

const ScheduledEmailAttachmentsFormItem = ({ disabled }: Props) => {
    const fieldName = "attachments";
    const attachments: AttachmentsModel[] = [
        {
            value: "1",
            title: "ダウンロードURL",
        },
        {
            value: "2",
            title: "添付",
        },
    ];

    return (
        <Col span={23} style={{ marginBottom: "1%" }}>
            <Form.Item colon={false} name={fieldName} noStyle>
                <Select
                    mode="multiple"
                    className={styles.container}
                    placeholder="ファイル形式"
                    allowClear
                    disabled={disabled}>
                    {attachments.map((attachment) => {
                        return (
                            <Select.Option
                                key={attachment.value}
                                value={attachment.value}>
                                {attachment.icon} {attachment.title}
                            </Select.Option>
                        );
                    })}
                </Select>
            </Form.Item>
        </Col>
    );
};

export default ScheduledEmailAttachmentsFormItem;
