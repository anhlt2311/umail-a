import React from "react";
import { Col, Form, FormItemProps, Input, Row } from "antd";
import { ErrorMessages, HANKAKU_NUMBER_REGEX } from "~/utils/constants";
import styles from "./TelInputFormItem.scss";
import { NamePath } from "antd/lib/form/interface";

type Props = FormItemProps & {};

const TelInputFormItem = ({ ...props }: Props) => {
    const validator = (
        value: any,
        getFieldValue: (name: NamePath) => any
    ): Promise<any> => {
        const tel2 = getFieldValue("tel2");
        const tel3 = getFieldValue("tel3");
        const values = [value, tel2, tel3];

        const isAllUnfilled = values.every((value) => !value);

        // NOTE(joshua-hashimoto): 必須ではないかつ全ての文字列が入力されてない場合はOKなので、真っ先に返す
        if (!props.required && isAllUnfilled) {
            return Promise.resolve();
        }

        const isAllFilled = values.every((value) => !!value);

        // NOTE(joshua-hashimoto): 必須かつ全ての入力欄が埋まっていない場合
        if (props.required && !isAllFilled) {
            return Promise.reject(new Error(ErrorMessages.form.required));
        }

        // NOTE(joshua-hashimoto): isAllUnfilledのチェックを通過し、かつ全ての項目が埋まっていない、かつ必須項目ではない場合
        if (!isAllFilled) {
            return Promise.reject(new Error(ErrorMessages.form.telNotComplete));
        }

        // NOTE(joshua-hashimoto): 全ての項目が埋まっているが、番号の合計文字列数が15より多い場合
        if (isAllFilled && values.join("").length > 15) {
            return Promise.reject(
                new Error(
                    "3つの入力欄を合わせて" +
                        ErrorMessages.validation.length.max15
                )
            );
        }

        const isNumber = values
            .map((value) => {
                const isNum = HANKAKU_NUMBER_REGEX.test(value);
                return !value || isNum;
            })
            .every((value) => value);

        // NOTE(joshua-hashimoto): 入力されている値が数値ではない場合
        if (!isNumber) {
            return Promise.reject(new Error(ErrorMessages.form.numberOnly));
        }

        // NOTE(joshua-hashimoto): 全てのチェックを通過
        return Promise.resolve();
    };

    return (
        <Form.Item label="TEL" className={styles.field} {...props}>
            <Col span={24}>
                <Row>
                    <Col span={7}>
                        <Form.Item
                            className={styles.field}
                            name="tel1"
                            dependencies={["tel2", "tel3"]}
                            rules={[
                                ({ getFieldValue }) => ({
                                    validator: (_, value) =>
                                        validator(value, getFieldValue),
                                }),
                            ]}
                            noStyle>
                            <Input data-testid="tel1" />
                        </Form.Item>
                    </Col>
                    <Col flex="auto">
                        <Row
                            align="middle"
                            justify="center"
                            style={{ height: "100%" }}>
                            <div>-</div>
                        </Row>
                    </Col>
                    <Col span={7}>
                        <Form.Item
                            label=" "
                            className={styles.field}
                            name="tel2"
                            noStyle>
                            <Input data-testid="tel2" />
                        </Form.Item>
                    </Col>
                    <Col flex="auto">
                        <Row
                            align="middle"
                            justify="center"
                            style={{ height: "100%" }}>
                            -
                        </Row>
                    </Col>
                    <Col span={7}>
                        <Form.Item
                            label=" "
                            className={styles.field}
                            name="tel3"
                            noStyle>
                            <Input data-testid="tel3" />
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Form.Item>
    );
};

export default TelInputFormItem;
