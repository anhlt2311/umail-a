import React from "react";
import { Form } from "antd";
import TagSelectFormItem from "../TagSelectFormItem";
import { tagResponseModelList } from "~/test/mock/tagAPIMock";
import {
    renderHook,
    renderWithAllProviders,
    screen,
    userEvent,
    waitFor,
} from "~/test/utils";
import login, { LoginInitialState } from "~/reducers/login";
import { configureStore } from "@reduxjs/toolkit";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";

const Wrapper = () => {
    const [form] = Form.useForm();

    const mockOnTagAdd = jest.fn();

    return (
        <Form form={form}>
            <TagSelectFormItem
                defaults={tagResponseModelList}
                onTagAdd={mockOnTagAdd}
            />
        </Form>
    );
};

describe("TagSelectFormItem.tsx", () => {
    const tooltipIconTestId = "tag-select-form-item-tooltip-icon";
    const newTagIconTestId = "new-tag-icon";

    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test", async () => {
        renderWithAllProviders(<Wrapper />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: true,
                            },
                        },
                    },
                },
            }),
        });
        const labelElement = screen.getByText(/^タグ$/);
        expect(labelElement).toBeInTheDocument();
        const tooltipIconElement = screen.getByTestId(tooltipIconTestId);
        expect(tooltipIconElement);
        await userEvent.hover(tooltipIconElement);
        const tooltipMainContentElement = await screen.findByText(
            /^タグを設定することで開発言語名やスキルなど、登録項目にない詳細な条件を付与することができます。$/
        );
        expect(tooltipMainContentElement);
        const tooltipLinkElement = await screen.findByRole("link", {
            name: /^詳細$/,
        });
        expect(tooltipLinkElement).toBeInTheDocument();
        const newTagIconElement = screen.getByTestId(newTagIconTestId);
        expect(newTagIconElement).toBeInTheDocument();
        // NOTE(joshua-hashimoto): TagAjaxSelectの簡単なテスト
        const selectPlaceholderElement = screen.getByText(/クリックして選択/);
        expect(selectPlaceholderElement).toBeInTheDocument();
        await userEvent.click(selectPlaceholderElement);
        const selectOptionElement = await screen.findByText(/django/);
        expect(selectOptionElement).toBeInTheDocument();
        // NOTE(joshua-hashimoto): タグ新規作成モーダルの簡単なテスト
        await userEvent.click(newTagIconElement);
        const newTagModalTitleElement = await screen.findByText(
            /^タグ新規作成$/
        );
        expect(newTagModalTitleElement).toBeInTheDocument();
        const newTagModalCancelButtonElement = await screen.findByText(
            /^キャンセル$/
        );
        expect(newTagModalCancelButtonElement).toBeInTheDocument(); // NOTE(joshua-hashimoto): モーダルを閉じる
        await userEvent.click(newTagModalCancelButtonElement);
        const helpTextElement = screen.getByText(/^最大10個選択できます。$/);
        expect(helpTextElement).toBeInTheDocument();
    });
});
