import React, { useState } from "react";
import {
    Button,
    Col,
    Form,
    FormItemProps,
    Row,
    Tooltip,
    Typography,
} from "antd";
import { PlusOutlined, QuestionCircleFilled } from "@ant-design/icons";
import TagAjaxSelect from "~/components/Common/TagAjaxSelect/TagAjaxSelect";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages, Links, iconCustomColor } from "~/utils/constants";
import { TagContactSelectModel, TagModel } from "~/models/tagModel";
import NewTagModal from "~/components/Common/NewTagModal/NewTagModal";
import styles from "./TagSelectFormItem.scss";

const { Text } = Typography;

type Props = FormItemProps & {
    defaults: TagContactSelectModel[];
    onTagAdd: (newTag: TagModel) => void;
};

const TagSelectFormItem = ({ onTagAdd, ...props }: Props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const { _all: tagAuthorized } = useAuthorizedActions("tags");

    const onNewTagModalOpen = () => {
        setIsModalVisible(true);
    };

    const onNewTagModalClose = () => {
        setIsModalVisible(false);
    };

    return (
        <Form.Item
            {...props}
            className={styles.field}
            label={
                <span>
                    タグ&nbsp;
                    <Tooltip
                        title={
                            <span>
                                タグを設定することで開発言語名やスキルなど、登録項目にない詳細な条件を付与することができます。
                                <br />
                                <a
                                    href={Links.helps.tags.settings}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    詳細
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled
                            data-testid="tag-select-form-item-tooltip-icon"
                            style={{ color: iconCustomColor }}
                            className={styles.tooltip}
                        />
                    </Tooltip>
                </span>
            }
            style={{ margin: "8px 0 0" }}>
            <Row align="middle">
                <Col span={24}>
                    <Row align="middle">
                        <Col span={22}>
                            <Form.Item
                                name="tags"
                                rules={[
                                    {
                                        validator: (rule, value, callback) => {
                                            if (value) {
                                                if (value.length > 10) {
                                                    value.pop();
                                                    return Promise.resolve();
                                                } else if (value.length <= 10) {
                                                    if (
                                                        props.required &&
                                                        value.length == 0
                                                    ) {
                                                        return Promise.reject(
                                                            new Error(
                                                                ErrorMessages.generic.selectRequired
                                                            )
                                                        );
                                                    } else {
                                                        return Promise.resolve();
                                                    }
                                                }
                                            } else {
                                                if (props.required) {
                                                    return Promise.reject(
                                                        new Error(
                                                            ErrorMessages.generic.selectRequired
                                                        )
                                                    );
                                                } else {
                                                    return Promise.resolve();
                                                }
                                            }
                                        },
                                    },
                                ]}
                                noStyle>
                                <TagAjaxSelect />
                            </Form.Item>
                        </Col>
                        <Col
                            span={2}
                            style={{
                                textAlign: "right",
                            }}>
                            {tagAuthorized ? (
                                <Button
                                    className={styles.button}
                                    type="primary"
                                    onClick={onNewTagModalOpen}
                                    hidden={false}
                                    icon={
                                        <PlusOutlined data-testid="new-tag-icon" />
                                    }
                                />
                            ) : (
                                <Tooltip title={ErrorMessages.isNotAuthorized}>
                                    <Button
                                        className={styles.button}
                                        size="small"
                                        type="link"
                                        icon={<PlusOutlined />}
                                        style={{
                                            margin: "8px 0 0",
                                            cursor: "not-allowed",
                                        }}
                                    />
                                </Tooltip>
                            )}
                        </Col>
                    </Row>
                    <Row align="top" justify="start">
                        <Col>
                            <Text type="secondary">最大10個選択できます。</Text>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Form.Item noStyle>
                <NewTagModal
                    isVisible={isModalVisible}
                    onFinish={onTagAdd}
                    onModalClose={onNewTagModalClose}
                />
            </Form.Item>
        </Form.Item>
    );
};

export default TagSelectFormItem;
