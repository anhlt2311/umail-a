import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Col, Form, Row, Tooltip, Select, Spin } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { Endpoint } from "~/domain/api";
import { RootState } from "~/models/store";
import { simpleFetch } from "~/actions/data";
import styles from "./CorporateCommentUserFormItem.scss";
import AjaxSelect from "../../ajax/AjaxSelect";
import { Links, iconCustomColor } from "~/utils/constants";

type Props = {
    disabled?: boolean;
};

const CorporateCommentUserFormItem = ({ disabled }: Props) => {
    const fieldName = "comment_user";

    return (
        <Col
            span={24}
            style={{
                marginBottom: "1%",
            }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item colon={false} name={fieldName} noStyle>
                        <AjaxSelect
                            pageSize={1000}
                            resourceUrl={`${Endpoint.getBaseUrl()}/${
                                Endpoint.users
                            }?is_active=true`}
                            displayKey="display_name"
                            searchParam="full_name"
                            mode="multiple"
                            placeholder="コメント作成者"
                            disabled={disabled}
                        />
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                複数選択をすると
                                <a
                                    href={Links.helps.filter.or}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    OR検索
                                </a>
                                <br />
                                なお、無効化されているユーザーは選択表示されません。
                                <br />
                                <a
                                    href={Links.helps.users.activeToggle}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    詳細
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default CorporateCommentUserFormItem;
