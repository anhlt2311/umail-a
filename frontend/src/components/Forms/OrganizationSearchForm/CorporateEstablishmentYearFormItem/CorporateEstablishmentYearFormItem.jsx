import React from "react";
import { Col, Form, Row, Input, Tooltip } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import { iconCustomColor } from "~/utils/constants";
import styles from "./CorporateEstablishmentYearFormItem.scss";

const CorporateEstablishmentYearFormItem = ({ disabled = false, onClear }) => {
    const maxEstablishmentYearFieldName = "establishment_year_gt";
    const minEstablishmentYearFieldName = "establishment_year_lt";

    return (
        <Col span={24} style={{ marginBottom: "1%" }}>
            <Form.Item
                colon={false}
                name="establishment_year"
                wrapperCol={{ span: 24 }}
                noStyle>
                <Row className={styles.container} align="middle">
                    <Col span={11}>
                        <Form.Item
                            colon={false}
                            name={maxEstablishmentYearFieldName}
                            noStyle>
                            <Input
                                className={styles.userInput}
                                placeholder="取引に必要な設立年数(下限)"
                                allowClear
                                onChange={(event) => {
                                    // NOTE(joshua-hashimoto): This does not interrupt Forms onValuesChange
                                    const value = event.target.value;
                                    if (value === "") {
                                        onClear(
                                            maxEstablishmentYearFieldName,
                                            ""
                                        );
                                    }
                                }}
                                disabled={disabled}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={1}>
                        <Row justify="center">
                            <span>〜</span>
                        </Row>
                    </Col>
                    <Col span={11}>
                        <Form.Item
                            colon={false}
                            name={minEstablishmentYearFieldName}
                            noStyle>
                            <Input
                                className={styles.userInput}
                                placeholder="取引に必要な設立年数(上限)"
                                allowClear
                                onChange={(event) => {
                                    // NOTE(joshua-hashimoto): This does not interrupt Forms onValuesChange
                                    const value = event.target.value;
                                    if (value === "") {
                                        onClear(
                                            minEstablishmentYearFieldName,
                                            ""
                                        );
                                    }
                                }}
                                disabled={disabled}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={1} className={styles.infoIcon}>
                        <Tooltip
                            title={
                                <span>
                                    左の入力欄には下限値を入力します。<br />
                                    例：「3」を入力した場合、取引に必要な設立年数3年以上が対象となります。<br />
                                    右の入力欄には上限値を入力します。<br />
                                    例：「3」を入力した場合、取引に必要な設立年数3年以下が対象となります。
                                </span>
                            }>
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </Col>
                </Row>
            </Form.Item>
        </Col>
    );
};

export default CorporateEstablishmentYearFormItem;
