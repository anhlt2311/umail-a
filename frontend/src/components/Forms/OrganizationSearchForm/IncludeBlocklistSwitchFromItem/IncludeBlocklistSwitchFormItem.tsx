import React, { useState } from "react";
import { Col, Row, Form, Switch, Tooltip } from "antd";
import {
    QuestionCircleFilled,
    CheckOutlined,
    CloseOutlined,
} from "@ant-design/icons";
import styles from "./IncludeBlocklistSwitchFormItem.scss";
import { Links, iconCustomColor } from "~/utils/constants";

type Props = {
    initialValue: any;
};

const IncludeBlocklistSwitchFormItem = ({ initialValue }: Props) => {
    const [isIgnoreBlocklistFilter, setIsIgnoreBlocklistFilter] = useState(
        !!initialValue
    );

    return (
        <Col span={24}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Row gutter={6} justify="end">
                        <Col>
                            <label className={styles.switchLabel}>
                                ブロックリスト対象含
                            </label>
                        </Col>
                        <Col>
                            <Form.Item
                                labelCol={{}}
                                wrapperCol={{}}
                                name="ignore_blocklist_filter"
                                valuePropName="checked"
                                initialValue={isIgnoreBlocklistFilter}
                                noStyle>
                                <Switch
                                    checkedChildren={<CheckOutlined />}
                                    unCheckedChildren={<CloseOutlined />}
                                    onChange={setIsIgnoreBlocklistFilter}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip
                        title={
                            <span>
                                ブロックリスト登録されている取引先を表示させる場合は有効にしてください。
                                <br />
                                <a
                                    href={Links.helps.filter.includingBlockList}
                                    target="_blank"
                                    rel="noopener noreferrer">
                                    詳細
                                </a>
                            </span>
                        }>
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default IncludeBlocklistSwitchFormItem;
