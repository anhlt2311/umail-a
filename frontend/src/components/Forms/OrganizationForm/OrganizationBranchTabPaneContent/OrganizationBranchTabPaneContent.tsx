import React from "react";
import { Button, Empty, Form, FormInstance, Modal, Result, Row } from "antd";
import OrganizationBranchField from "./OrganizationBranchField/OrganizationBranchField";
import { useOrganizationAPI } from "~/hooks/useOrganizationAPI";
import styles from "./OrganizationBranchTabPaneContent.scss";

const emptyBranchData = {
    name: "",
    address: "",
    building: "",
    tel1: "",
    tel2: "",
    tel3: "",
    fax1: "",
    fax2: "",
    fax3: "",
};

type Props = {
    form: FormInstance;
};

const OrganizationBranchTabPaneContent = ({ form }: Props) => {
    const { removeOrganizationBranch } = useOrganizationAPI();

    const onRemove = (
        fieldName: number,
        remove: (index: number | number[]) => void
    ) => {
        Modal.confirm({
            title: "この支店を削除しますか？",
            okText: "OK",
            onOk: () => {
                const removingBranch = form.getFieldValue([
                    "branches",
                    fieldName,
                ]);
                if (removingBranch.id) {
                    removeOrganizationBranch(removingBranch.id);
                }
                remove(fieldName);
            },
            cancelText: "キャンセル",
            onCancel: () => {},
            bodyStyle: {
                textAlign: "left",
            },
        });
    };

    return (
        <>
            <Form.List name="branches">
                {(fields, { add, remove }) => {
                    if (!fields.length) {
                        return (
                            <Result
                                icon={<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={false}/>}
                                subTitle="データがありません"
                                extra={[
                                    <Button
                                        size="small"
                                        key="1"
                                        onClick={() => add(emptyBranchData)}
                                        type="primary">
                                        支店を登録する
                                    </Button>,
                                ]}
                            />
                        );
                    }
                    return (
                        <Row>
                            {fields.map((field, index) => {
                                // TODO(joshua-hashimoto): アドオンで数を増やせるようにする
                                const isAddable =
                                    fields.length < 3 &&
                                    fields.length - 1 === index;
                                return (
                                    <OrganizationBranchField
                                        {...field}
                                        onAdd={() => add(emptyBranchData)}
                                        onRemove={(fieldName) =>
                                            onRemove(fieldName, remove)
                                        }
                                        isAddable={isAddable}
                                    />
                                );
                            })}
                        </Row>
                    );
                }}
            </Form.List>
        </>
    );
};

export default OrganizationBranchTabPaneContent;
