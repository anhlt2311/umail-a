import React from "react";
import { Col, Row, Form, Tooltip, Input } from "antd";
import { QuestionCircleFilled } from "@ant-design/icons";
import {iconCustomColor} from "~/utils/constants";
import styles from "./NameFormItem.scss";

const NameFormItem = ({ name, onBlur, onChange }) => {
    return (
        <Col span={7} style={{ marginBottom: "1%" }}>
            <Row className={styles.container}>
                <Col span={23}>
                    <Form.Item
                        wrapperCol={{ span: 20 }}
                        noStyle
                        colon={false}
                        name="name"
                        initialValue={name}>
                        <Input
                            className={styles.userInput}
                            placeholder="ルール名"
                            onBlur={onBlur}
                            onChange={onChange}
                            allowClear
                        />
                    </Form.Item>
                </Col>
                <Col span={1} className={styles.infoIcon}>
                    <Tooltip title="部分一致検索">
                        <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                    </Tooltip>
                </Col>
            </Row>
        </Col>
    );
};

export default NameFormItem;
