import React from "react";
import { Col, Row, Form, Tooltip, Input, Select, Button } from "antd";
import {
    QuestionCircleFilled,
    PlusOutlined,
    MinusOutlined,
} from "@ant-design/icons";
import styles from "./ConditionFormItem.scss";
import {iconCustomColor} from "~/utils/constants";
import {
    target_types,
    condition_types,
} from "../../SharedEmailNotificationForm";

const inputColXs = 16;
const inputColSm = 17;
const inputColMd = 18;
const inputColLg = 19;
const inputColXl = 20;
const inputColXxl = 21;

const inputCol = {
    xs: inputColXs,
    sm: inputColSm,
    md: inputColMd,
    lg: inputColLg,
    xl: inputColXl,
    xxl: inputColXxl,
};

const tooltipCol = 1;

const controlButtonCol = {
    xs: 24 - tooltipCol - inputColXs,
    sm: 24 - tooltipCol - inputColSm,
    md: 24 - tooltipCol - inputColMd,
    lg: 24 - tooltipCol - inputColLg,
    xl: 24 - tooltipCol - inputColXl,
    xxl: 24 - tooltipCol - inputColXxl,
};

const ConditionFormItem = ({
    target_type,
    condition_type,
    value,
    onBlur,
    onChangeTarget,
    onChangeCondition,
    onChangeValue,
    getCurrentFieldValue,
    maxCount,
    currentCount,
    add,
    remove,
}) => {
    return (
        <Col span={16} style={{ marginBottom: "1%" }}>
            <Form.Item colon={false} noStyle>
                <Row className={styles.container}>
                    <Col {...inputCol}>
                        <Row>
                            <Col span={8}>
                                <Form.Item
                                    label=" "
                                    className={styles.inlineField}
                                    colon={false}
                                    name={`rules[${currentCount}].target_type`}
                                    initialValue={target_type}
                                    noStyle>
                                    <Select
                                        className={styles.container}
                                        placeholder="対象"
                                        onChange={onChangeTarget}
                                        allowClear>
                                        {target_types.map((type) => (
                                            <Select.Option
                                                key={type.value}
                                                value={type.value}>
                                                {type.name}
                                            </Select.Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col span={8}>
                                <Form.Item
                                    label=" "
                                    className={styles.inlineField}
                                    colon={false}
                                    name={`rules[${currentCount}].condition_type`}
                                    initialValue={condition_type}
                                    noStyle>
                                    <Select
                                        className={styles.container}
                                        placeholder="条件"
                                        onChange={onChangeCondition}
                                        allowClear>
                                        {condition_types
                                            .filter(
                                                (condition_type) =>
                                                    condition_type.target_type ===
                                                    getCurrentFieldValue(
                                                        `rules[${currentCount}].target_type`
                                                    )
                                            )
                                            .map((type) => (
                                                <Select.Option
                                                    key={type.value}
                                                    value={type.value}>
                                                    {type.name}
                                                </Select.Option>
                                            ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col span={8}>
                                <Form.Item
                                    label=" "
                                    className={styles.inlineField}
                                    colon={false}
                                    name={`rules[${currentCount}].value`}
                                    initialValue={value}
                                    noStyle>
                                    <Input
                                        className={styles.input}
                                        placeholder="値"
                                        onBlur={onBlur}
                                        onChange={onChangeValue}
                                        disabled={
                                            getCurrentFieldValue(
                                                `rules[${currentCount}].target_type`
                                            ) == "attachment"
                                        }
                                        allowClear
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={tooltipCol} className={styles.infoIcon}>
                        <Tooltip title="部分一致検索">
                            <QuestionCircleFilled style={{ color: iconCustomColor }} className={styles.tooltip} />
                        </Tooltip>
                    </Col>
                    <Col {...controlButtonCol}>
                        <Row gutter={4}>
                            <Col span={12} className={styles.infoIcon}>
                                <div
                                    hidden={
                                        maxCount != currentCount ||
                                        currentCount == 3
                                    }>
                                    <Button
                                        type="primary"
                                        icon={<PlusOutlined />}
                                        style={{ margin: "auto 6px" }}
                                        onClick={add}
                                    />
                                </div>
                            </Col>
                            <Col span={12} className={styles.infoIcon}>
                                <div
                                    hidden={
                                        maxCount != currentCount ||
                                        currentCount == 1
                                    }>
                                    <Button
                                        type="primary"
                                        danger
                                        icon={<MinusOutlined />}
                                        style={{ margin: "auto 6px" }}
                                        onClick={() => remove(currentCount)}
                                    />
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Form.Item>
        </Col>
    );
};

export default ConditionFormItem;
