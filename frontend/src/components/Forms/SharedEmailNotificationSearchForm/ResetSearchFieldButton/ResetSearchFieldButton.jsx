import React from "react";
import { Button, Col, Tooltip } from "antd";
import { UndoOutlined } from "@ant-design/icons";
import styles from "./ResetSearchFieldButton.scss";

const ResetSearchFieldButton = ({ onReset }) => {
    return (
        <Col>
            <Tooltip title="検索条件をリセット">
                <Button
                    type="primary"
                    icon={<UndoOutlined />}
                    style={{ margin: "auto 6px" }}
                    onClick={onReset}
                />
            </Tooltip>
        </Col>
    );
};

export default ResetSearchFieldButton;
