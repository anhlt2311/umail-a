import React from "react";
import PropTypes from "prop-types";
import { Button, Row, Col, Form, Card } from "antd";
import BaseForm from "../base/BaseForm";
import styles from "./SharedEmailNotificationSearchForm.scss";
import { finalized } from "../helpers";
import { UpOutlined, DownOutlined } from "@ant-design/icons";
import validateJapaneseMessages from "../validateMessages";
import IncludeInvalidSwitchFormItem from "./IncludeInvalidSwitchFromItem/IncludeInvalidSwitchFormItem";
import NameFormItem from "./NameFormItem/NameFormItem";
import MasterRuleFormItem from "./MasterRuleFormItem/MasterRuleFormItem";
import ConditionFormItem from "./ConditionFormItem/ConditionFormItem";
import ResetSearchFieldButton from "./ResetSearchFieldButton/ResetSearchFieldButton";

const formLayout = {
    labelCol: {
        sm: { span: 24 },
        md: { span: 6 },
    },
    wrapperCol: {
        sm: { span: 24 },
        md: { span: 18 },
    },
};

class SharedEmailNotificationSearchForm extends BaseForm {
    state = {
        searchExpand: true,
        conditionFormCount: 1,
    };

    submitWithClearedValue = (key, value) => {
        this.baseform.current.validateFields().then((values) => {
            values[key] = value;
            values["rules"] = this.getCurrentRules();
            this.submitHandler(values);
        });
    };

    clearInputValue = (e) => {
        if (e.target && e.target.value === "" && e.type === "click") {
            if (typeof e.preventDefault === "function") {
                e.preventDefault();
            }
            this.submitWithClearedValue(e.target.id, "");
        }
    };

    clearNameValue = (e) => {
        if (e.target && e.target.value === "" && e.type === "click") {
            this.submitWithClearedValue("name", "");
        }
    };

    clearMasterRuleSelectValue = (value) => {
        this.submitWithClearedValue("master_rule", value);
    };

    handleSwitchInactiveFilter = (checked, event) => {
        this.submitWithClearedValue("inactive_filter", checked);
    };

    toggle = () => {
        const { searchExpand } = this.state;
        this.setState({ searchExpand: !searchExpand });
    };

    handleChangeTarget(index, val) {
        const { setFieldsValue } = this.baseform.current;
        setFieldsValue({ [`rules[${index + 1}].condition_type`]: undefined });
        setFieldsValue({ [`rules[${index + 1}].value`]: undefined });
        this.forceUpdate(); // 再描画しないと選択肢の内容が切り替わらないため
        this.handleSubmitWithRules();
    }

    getCurrentFieldValue = (name) => {
        return this.baseform.current &&
            this.baseform.current.getFieldValue(name)
            ? this.baseform.current.getFieldValue(name)
            : "";
    };

    add = () => {
        const { conditionFormCount } = this.state;
        if (conditionFormCount < 3) {
            this.setState({ conditionFormCount: conditionFormCount + 1 });
        }
    };

    remove = () => {
        const { setFieldsValue } = this.baseform.current;
        const { conditionFormCount } = this.state;
        if (1 < conditionFormCount) {
            this.setState({ conditionFormCount: conditionFormCount - 1 });
        }
        setFieldsValue({
            [`rules[${conditionFormCount}].target_type`]: undefined,
            [`rules[${conditionFormCount}].condition_type`]: undefined,
            [`rules[${conditionFormCount}].value`]: undefined,
        });
    };

    handleSubmitWithRules = () => {
        this.baseform.current.validateFields().then((values) => {
            values["rules"] = this.getCurrentRules();
            this.submitHandler(values);
        });
    };

    getCurrentRules = () => {
        const { conditionFormCount } = this.state;
        return [...Array(conditionFormCount).keys()].map((key, index) => {
            return {
                target_type: this.getCurrentFieldValue(
                    `rules[${index + 1}].target_type`
                ),
                condition_type: this.getCurrentFieldValue(
                    `rules[${index + 1}].condition_type`
                ),
                value: this.getCurrentFieldValue(`rules[${index + 1}].value`),
            };
        });
    };

    render() {
        const {
            initialData,
            selectedSearchItemKeys,
            onSearchMenu,
            searchMenuOpen,
        } = this.props;

        const { conditionFormCount } = this.state;

        let searchItemsForDisplay = [];

        searchItemsForDisplay.push(
            <NameFormItem
                name={initialData.name}
                onBlur={this.handleSubmitWithRules}
                onChange={this.clearNameValue}
            />
        );

        searchItemsForDisplay.push(
            <MasterRuleFormItem
                masterRule={initialData.master_rule}
                onBlur={this.handleSubmitWithRules}
                onChange={this.clearMasterRuleSelectValue}
            />
        );

        // ここをループで回して検索条件を作る感じになりそう
        [...Array(conditionFormCount).keys()].forEach((count) => {
            searchItemsForDisplay.push(
                <ConditionFormItem
                    target_type={initialData.target_type}
                    condition_type={initialData.condition_type}
                    value={initialData.value}
                    onBlur={this.handleSubmitWithRules}
                    onChangeTarget={this.handleChangeTarget.bind(this, count)}
                    onChangeCondition={this.handleSubmitWithRules}
                    onChangeValue={this.clearInputValue}
                    getCurrentFieldValue={this.getCurrentFieldValue}
                    maxCount={conditionFormCount}
                    currentCount={count + 1}
                    add={this.add}
                    remove={this.remove}
                />
            );
        });

        return (
            <>
                <Col>
                    <Row justify="end">
                        <Button
                            type="primary"
                            onClick={onSearchMenu}
                            size="small"
                            icon={
                                !searchMenuOpen ? (
                                    <UpOutlined />
                                ) : (
                                    <DownOutlined />
                                )
                            }>
                            検索メニュー
                        </Button>
                    </Row>
                </Col>
                <Card className={styles.container} hidden={!searchMenuOpen}>
                    <Form
                        labelCol={{ span: 0 }}
                        wrapperCol={{ span: 24 }}
                        layout="horizontal"
                        onSubmit={this.handleSubmitWithRules}
                        ref={this.baseform}
                        initialValues={initialData}
                        validateMessages={validateJapaneseMessages}>
                        <Row>
                            <Col span={21}>
                                <Row gutter={24}>
                                    {searchItemsForDisplay.map(
                                        (item, index) => (
                                            <React.Fragment key={index}>
                                                {item}
                                            </React.Fragment>
                                        )
                                    )}
                                </Row>
                            </Col>
                            <Col span={3}>
                                <Row justify="end">
                                    <ResetSearchFieldButton
                                        onReset={this.handleReset}
                                    />
                                </Row>
                                <Row justify="end" style={{ marginTop: "7%" }}>
                                    <IncludeInvalidSwitchFormItem
                                        inactiveFilter={
                                            initialData.inactive_filter
                                        }
                                        onChange={
                                            this.handleSwitchInactiveFilter
                                        }
                                    />
                                </Row>
                            </Col>
                        </Row>
                    </Form>
                </Card>
            </>
        );
    }
}

SharedEmailNotificationSearchForm.propTypes = {
    initialData: PropTypes.shape({
        display_name: PropTypes.string,
        email: PropTypes.string,
        role: PropTypes.string,
        is_acrive: PropTypes.bool,
    }),
    submitHandler: PropTypes.func.isRequired,
    resetFormHandler: PropTypes.func.isRequired,
    selectedSearchItemKeys: PropTypes.arrayOf(PropTypes.string),
    onSearchMenu: PropTypes.func.isRequired,
    searchMenuOpen: PropTypes.bool,
};

SharedEmailNotificationSearchForm.defaultProps = {
    initialData: {},
};

const SearchFormWrapper = finalized(SharedEmailNotificationSearchForm);
export default SearchFormWrapper;
