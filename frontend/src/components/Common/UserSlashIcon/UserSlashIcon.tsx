import { UserOutlined } from '@ant-design/icons';
import React from 'react'
import styles from "./UserSlashIcon.scss";
import { FaSlash } from 'react-icons/fa';
interface Props {
    disabled:boolean
}
function UserSlashIcon({disabled}:Props) {
    return (
        <div className={styles.imgIconUserContainer}>
            <UserOutlined />
            <FaSlash color={disabled ? '#C0C0C0' : undefined} className={styles.slashIcon}/>
        </div>     
    )
}

export default UserSlashIcon
