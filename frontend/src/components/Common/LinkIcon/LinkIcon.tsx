import React from "react";
import Icon from "@ant-design/icons";

const LinkSvg = () => {
    return (
        <svg
            width="1rem"
            height="1rem"
            viewBox="0 0 50 50"
            fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path
                d="M38.2878 10.297L39.7019 11.712L24.7119 26.702L23.2979 25.288L38.2878 10.297Z"
                fill="#82b435"
            />
            <path d="M40 20H38V12H30V10H40V20Z" fill="#82b435" />
            <path
                d="M35 38H15C13.3 38 12 36.7 12 35V15C12 13.3 13.3 12 15 12H26V14H15C14.4 14 14 14.4 14 15V35C14 35.6 14.4 36 15 36H35C35.6 36 36 35.6 36 35V24H38V35C38 36.7 36.7 38 35 38Z"
                fill="#82b435"
            />
        </svg>
    );
};

const LinkIcon = (props: any) => {
    return <Icon component={LinkSvg} {...props} />;
};

export default LinkIcon;
