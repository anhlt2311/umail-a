import React from "react";
import { Pagination, PaginationProps } from "antd";
import { PaginationConfig } from "antd/lib/pagination";
import { isMobileDevices } from "~/components/helpers";
import { DEFAULT_PAGE_SIZE } from "~/utils/constants";
import styles from "./CustomPagination.scss";

type Props = PaginationConfig & {};

const CustomPagination = ({ ...props }: Props) => {
    const paginationConfig: PaginationConfig = {
        pageSize: DEFAULT_PAGE_SIZE,
        simple: isMobileDevices(),
        defaultPageSize: DEFAULT_PAGE_SIZE,
        showSizeChanger: false,
        showTotal: (total, range) =>
            `合計${total}件中, ${range[0]}-${range[1]}を表示`,
        ...props,
    };

    return <Pagination className={styles.paginator} {...paginationConfig} />;
};

export default CustomPagination;
