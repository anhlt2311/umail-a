import React from "react";
import { MemoryRouter } from "react-router-dom";
import {
    mockHistoryGoBack,
    mockHistoryPush,
    renderWithBrowserRouter,
    screen,
    userEvent,
} from "~/test/utils";
import BackButton from "../BackButton";
import Paths from "~/components/Routes/Paths";

type WrapperProps = {
    to?: string;
};

const Wrapper = ({ to }: WrapperProps) => {
    return (
        <MemoryRouter>
            <BackButton to={to} />
        </MemoryRouter>
    );
};

describe("BackButton.tsx", () => {
    test("render test", () => {
        renderWithBrowserRouter(<Wrapper />);
        const buttonElement = screen.getByText(/戻る/);
        expect(buttonElement).toBeInTheDocument();
    });

    test("plain back button action", async () => {
        renderWithBrowserRouter(<Wrapper />);
        const buttonElement = screen.getByText(/戻る/);
        await userEvent.click(buttonElement);
        expect(mockHistoryGoBack).toHaveBeenCalled();
    });

    test("back button action", async () => {
        renderWithBrowserRouter(<Wrapper to={Paths.login} />);
        const buttonElement = screen.getByText(/戻る/);
        await userEvent.click(buttonElement);
        expect(mockHistoryPush).toHaveBeenCalledWith(Paths.login);
    });
});
