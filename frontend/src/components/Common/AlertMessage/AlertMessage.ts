import { ReactNode } from "react";
import { message } from "antd";

type MessageProps = {
    content?: ReactNode;
    duration?: number;
    onClose?: () => void;
};

export const customSuccessMessage = (
  content: string,
  config?: MessageProps
  ) => {
    message.success({
        content,
        duration: 3,
        ...config,
    });
};

export const customErrorMessage = (
  content: string,
  config?: MessageProps
  ) => {
    message.error({
        content,
        duration: 4,
        ...config,
    });
};

export const customInfoMessage = (
  content: string,
  config?: MessageProps
  ) => {
    message.info({
        content,
        duration: 3,
        ...config,
    });
};
