import React from 'react'
import { PersonnelBoardListCardModel, SkillType, skillSheetsType } from '~/models/personnelBoardModel';
import { BoardCommentModel, BoardChecklistModel, BoardPeriodModel } from '~/models/boardCommonModel';
import { formatDate } from '~/utils/utils';
import styles from "./BoardBaseItemTable.scss";
import BaseCard from "../../BoardCommon/BaseCard/BaseCard";
import { Moment } from 'moment';

type Props = {
    isSimpleCard: boolean;
    itemCard: PersonnelBoardListCardModel;
    onDetailCard: (cardId: string) => () => void;
    handleChange: (value: string[], itemCard: PersonnelBoardListCardModel) => void;
    handleChangeStartDate: (value: Moment|null, itemCard: PersonnelBoardListCardModel) => void;
    handleChangeEndDate: (value: Moment|null, itemCard: PersonnelBoardListCardModel) => void;
}
function BoardBaseItemTable({ isSimpleCard, itemCard, onDetailCard, handleChange, handleChangeStartDate, handleChangeEndDate }: Props) {
    const {
        id,
        firstName,
        lastName,
        age,
        gender,
        trainStation,
        affiliation,
        operatePeriod,
        skills,
        firstNameInitial,
        lastNameInitial,
        parallel,
        price,
        image,
        assignees,
        priority,
        comments,
        checklist,
        period,
        skillSheets
    } = itemCard;

    const handleChangeUser = (values: string[]) => {
        handleChange(values, itemCard);
    };

    const onRemoveAssign = (value: string) => {
        const listAssignUser = assignees.filter(x => x.id !== value).map(x => x.id);
        handleChange(listAssignUser, itemCard);
    };

    const onStartDateSelect = (date: Moment|null) => {
        handleChangeStartDate(date, itemCard);
    };

    const onEndDateSelect = (date: Moment|null) => {
        handleChangeEndDate(date, itemCard);
    };

    const renderTitle = () => {
      if(firstName) return lastName + " " + firstName;
      
      return lastNameInitial + " " + firstNameInitial;
    };

    const ConvertAffiliation = (value: string) => {
        switch (value) {
          case 'proper':
              return "弊社プロパー";
          case 'freelancer':
              return "弊社フリーランス";
          case 'one_company_proper':
              return "1社先プロパー";
          case 'one_company_freelancer':
              return "1社先フリーランス";
          case 'two_company_proper':
              return "2社先プロパー";
          case 'two_company_freelancer':
              return "2社先フリーランス";
          case 'three_company_proper':
              return "3社先以上プロパー";
          case 'three_company_freelancer':
              return "3社先以上フリーランス";
          default:
              return;
        }
    };

    const ConvertParallel = (value: string) => {
        switch (value) {
          case 'none':
              return "なし";
          case 'parallel':
              return "あり";
          case 'parallel_one':
              return "1件";
          case 'parallel_two':
              return "2件";
          case 'parallel_three':
              return "3件以上";
          default:
              return;
        }
    };

    return (
      <BaseCard
        isSimpleCard={isSimpleCard}
        onStartDateSelect={onStartDateSelect}
        onEndDateSelect={onEndDateSelect}
        onDetailCard={onDetailCard(id)}
        handleChange={handleChangeUser}
        onRemoveAssign={onRemoveAssign}
        data={{
            id: id,
            title: renderTitle(),
            priority: priority?.value,
            thumb: image,
            affiliation: ConvertAffiliation(affiliation),
            info: [
                {
                    label: '年齢',
                    value:  age ? age + "歳" : age
                },
                {
                    label: '性別',
                    value: gender === 'male' ? '男性' : gender === 'female' ? '女性' : gender
                },
                {
                    label: '最寄駅',
                    value: trainStation
                },
                {
                    label: '稼働',
                    value: formatDate(operatePeriod)
                },
                {
                    label: '並行',
                    value: ConvertParallel(parallel)
                },
                {
                    label: '単金',
                    value: price ? price + "万円" : price
                }
            ],
            assignees,
            skills: skills as SkillType[],
            comments: comments as BoardCommentModel[],
            checklist: checklist as BoardChecklistModel[],
            period: period as BoardPeriodModel,
            skillSheets: skillSheets as skillSheetsType[]
        }}
      />
    );
}

export default BoardBaseItemTable;

