import { PlusOutlined, SettingOutlined } from "@ant-design/icons";
import Board, { addCard, moveCard } from "@asseinfo/react-kanban";
import { Button, Col, Popover, Row, Typography } from "antd";
import React, { useEffect, useState } from "react";
import HeaderMenuBoard from "~/components/Common/BoardCommon/HeaderMenuBoard/HeaderMenuBoard";
import PersonnelBoardCreatePopover from "~/components/Pages/PersonnelBoardPage/PersonnelBoardCreatePopover/PersonnelBoardCreatePopover";
import ProjectBoardCreatePopover from "~/components/Pages/ProjectBoardPage/ProjectBoardCreatePopover/ProjectBoardCreatePopover";
import {
  usePersonnelBoard,
  usePersonnelBoardDetailCard,
} from "~/hooks/usePersonnelBoard";
import { useProjectBoard } from "~/hooks/useProjectBoard";
import {
  defaultPersonnelBoard,
  defaultProjectBoard,
} from "~/models/boardCommonModel";
import MenuBoardSettings from "./MenuBoardSettingsComponent/MenuBoardSettingsComponent";

const { Title } = Typography;

const iconButtonStyle = {
  backgroundColor: "#eeeeee",
};

const BoardBaseComponent = ({
  typeCard,
  boardData,
  renderCard,
  onSettings,
}) => {
  const [board, setBoard] = useState({ columns: [] });
  const { personnelBoardUpdateCardOrder } = usePersonnelBoardDetailCard();
  const { personnelBoardCreateCardMutate } = usePersonnelBoard();
  const { projectBoardCreateCardMutate, projectBoardChangeCardPosition } =
    useProjectBoard();

  const renderColumnHeader = ({ id, title }) => {
    const [isDropDownPlus, setIsDropDownPlus] = useState(false);
    const [isVisibleSetting, setVisibleSetting] = useState(false);
    const onCreatePersonnel = (values) => {
      const { last_name_initial, first_name_initial } = values;
      setIsDropDownPlus(false);
      personnelBoardCreateCardMutate(
        {
          list_id: id,
          last_name_initial,
          first_name_initial,
        },
        {
          onSuccess: (res) => {
            const newBoard = addCard(
              board,
              {
                id,
              },
              {
                ...defaultPersonnelBoard,
                id: res.data.id,
                lastNameInitial: last_name_initial,
                firstNameInitial: first_name_initial,
              },
              {
                on: "top",
              }
            );
            setBoard({ ...newBoard });
          },
          onError: () => {},
        }
      );
    };

    const onCreateProject = (values) => {
      const { detail } = values;
      setIsDropDownPlus(false);
      projectBoardCreateCardMutate(
        {
          list_id: id,
          detail,
        },
        {
          onSuccess: (res) => {
            const newBoard = addCard(
              board,
              {
                id,
              },
              {
                ...defaultProjectBoard,
                id: res.data.id,
                detail: detail,
              },
              {
                on: "top",
              }
            );
            setBoard({ ...newBoard });
          },
          onError: () => {},
        }
      );
    };

    const onCreateCard = () => {
      setIsDropDownPlus(true);
    };

    const onClosePlus = () => {
      setIsDropDownPlus(false);
    };

    const handleVisiblePlusChange = (newVisible) => {
      setIsDropDownPlus(newVisible);
    };

    return (
      <Col key={id} span={24} style={{ marginBottom: "1%" }}>
        <Row justify="space-between" align="middle">
          <Col>
            <Row justify="start" align="middle">
              <Col span={24}>
                <Title level={5}>{title}</Title>
              </Col>
            </Row>
          </Col>
          <Col>
            <Row gutter={5}>
              <Col>
                <Popover
                  key={id}
                  content={
                    typeCard === "personnel" ? (
                      <PersonnelBoardCreatePopover
                        list_id={id}
                        onClose={onClosePlus}
                        onCreate={onCreatePersonnel}
                      />
                    ) : (
                      <ProjectBoardCreatePopover
                        list_id={id}
                        onClose={onClosePlus}
                        onCreate={onCreateProject}
                      />
                    )
                  }
                  visible={isDropDownPlus}
                  onVisibleChange={handleVisiblePlusChange}
                  trigger={["click"]}
                  placement="bottomRight"
                >
                  <Button
                    type="text"
                    icon={<PlusOutlined />}
                    onClick={onCreateCard}
                    style={iconButtonStyle}
                  />
                </Popover>
              </Col>
              <Col>
                <Popover
                  title={
                    <HeaderMenuBoard
                      title="リスト操作"
                      onClose={() => setVisibleSetting(false)}
                    />
                  }
                  content={
                    <MenuBoardSettings
                      onSettings={onSettings}
                      setVisibleSetting={setVisibleSetting}
                      listId={id}
                    />
                  }
                  trigger={["click"]}
                  visible={isVisibleSetting}
                  placement="topRight"
                  onVisibleChange={(v) => setVisibleSetting(v)}
                >
                  <Button
                    type="text"
                    icon={<SettingOutlined />}
                    onClick={() => setVisibleSetting(true)}
                    style={iconButtonStyle}
                  />
                </Popover>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    );
  };

  useEffect(() => {
    if (boardData) {
      setBoard(boardData);
    }
  }, [boardData]);

  const onCardDragEnd = (_board, card, source) => {
    const updatedBoard = moveCard(board, card, source);
    setBoard(updatedBoard);
    if (typeCard === "personnel") {
      personnelBoardUpdateCardOrder(
        _board.id,
        source.toColumnId,
        source.toPosition
      );
    } else {
      projectBoardChangeCardPosition(
        _board.id,
        source.toColumnId,
        source.toPosition
      );
    }
  };

  return (
    <Board
      renderColumnHeader={renderColumnHeader}
      renderCard={renderCard}
      allowRemoveColumn={false}
      allowRenameColumn={false}
      disableColumnDrag={true}
      onCardDragEnd={onCardDragEnd}
    >
      {board}
    </Board>
  );
};

export default BoardBaseComponent;
