import React, { useState } from "react";
import { SmileOutlined } from '@ant-design/icons';
import { useHistory } from "react-router-dom";
import Path from "~/components/Routes/Paths";
import HeaderMenuBoard from "~/components/Common/BoardCommon/HeaderMenuBoard/HeaderMenuBoard";
import { Avatar, Dropdown, Menu } from 'antd';
import styles from "./UserAvatar.scss";

type Props = {
    userId: string;
    src: string;
    size?: 'default' | 'large' | 'small';
    removeAssignClick: (assignId: string) => void;
}

type visibleMenu = {
    visible: boolean;
}

function UserAvatar({ src, size = 'default', userId, removeAssignClick }: Props) {
  const [visibleDropDown, setVisibleDropDown] = useState<visibleMenu>({ visible: false });
  const router = useHistory();

  const onClosePopup = () => {
    setVisibleDropDown({ visible: false });
  };

  const onVisibleChange =() => {
      setVisibleDropDown({ visible: !visibleDropDown.visible });
  };

  const editProfileClick = (userId: string) =>
        router.push(`${Path.users}/${userId}`);

  const avatarIcon = (
      <Avatar
        src={src}
        size={size}
        onError={() => true}
        icon={<SmileOutlined className={styles.styleIconAvatar}/>}
      />
  );

  const renderMenu = () => (
    <Menu className={styles.styleMenu}>
        <HeaderMenuBoard
          title="ユーザー操作"
          onClose={() => onClosePopup()}
        />
        <Menu.Item
            onClick={() => editProfileClick(userId)}>
                <>個人プロフィール編集</>
        </Menu.Item>
        <Menu.Item
            onClick={() => {removeAssignClick(userId), onClosePopup()}}>
                <>担当から外す</>
        </Menu.Item>
    </Menu>
  );

    return <Dropdown overlay={renderMenu()} trigger={['click']} visible={visibleDropDown.visible} onVisibleChange={onVisibleChange}>{avatarIcon}</Dropdown>;
    
}

export default UserAvatar;
