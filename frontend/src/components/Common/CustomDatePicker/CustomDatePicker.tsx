import React from "react";
import { DatePicker, DatePickerProps } from "antd";
import { pickerLocaleConfig } from "~/utils/constants";

type Props = DatePickerProps & {};

const CustomDatePicker = ({ ...props }: Props) => {
    return <DatePicker {...props} locale={pickerLocaleConfig} />;
};

export default CustomDatePicker;
