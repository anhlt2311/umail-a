import React from "react";
import { Form, FormItemProps, Select, Spin } from "antd";
import { BoardColumnModel } from "~/models/boardCommonModel";
import styles from "./BoardListFormItem.scss";

type Props = FormItemProps & {
    options?: BoardColumnModel[];
    isLoading: boolean;
    label?: string;
};

const BoardListFormItem = ({ options, isLoading, label ="リスト", ...props }: Props) => {
    return (
        <Spin spinning={isLoading} wrapperClassName={styles.wapper}>
            <Form.Item name="listId" {...props} label={label} colon={false}>
                <Select
                    data-testid="board-list-select"
                    placeholder="クリックして選択">
                    {options?.map((option) => (
                        <Select.Option key={option.id} value={option.id}>
                            {option.title}
                        </Select.Option>
                    ))}
                </Select>
            </Form.Item>
        </Spin>

    );
};

export default BoardListFormItem;
