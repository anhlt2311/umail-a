import React from "react";
import { screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { renderWithQueryClient } from "~/test/utils";
import { renderHook } from "@testing-library/react-hooks";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";
import PersonnelBoardListFormItem from "../PersonnelBoardListFormItem";
import { listsResponse } from "~/test/mock/personnelBoardAPIMock";

describe("PersonnelBoardListFormItem.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render options test", async () => {
        renderWithQueryClient(<PersonnelBoardListFormItem />);
        const selectElement = screen.getByText(/クリックして選択/);
        await userEvent.click(selectElement);
        for (const data of listsResponse) {
            const optionElement = await screen.findByText(
                new RegExp(data.title)
            );
            expect(optionElement).toBeInTheDocument();
        }
    });
});
