import React from "react";
import { render, screen } from "@testing-library/react";
import BoardListFormItem from "../BoardListFormItem";

describe("BoardListFormItem.tsx", () => {
    test("render test", () => {
        render(<BoardListFormItem isLoading={false} />);
        const labelElement = screen.getByText(/リスト/);
        expect(labelElement).toBeInTheDocument();
        const selectElement = screen.getByTestId("board-list-select");
        expect(selectElement).toBeInTheDocument();
        const placeholderElement = screen.getByText(/クリックして選択/);
        expect(placeholderElement).toBeInTheDocument();
    });
});
