import React from "react";
import { FormItemProps } from "antd";
import BoardListFormItem from "./BoardListFormItem";
import { useProjectBoardFetchListsAPIQuery } from "~/hooks/useProjectBoard";
import styles from "./BoardListFormItem.scss";

type Props = FormItemProps & {};

const ProjectBoardListFormItem = ({ ...props }: Props) => {
    const { data, isLoading } = useProjectBoardFetchListsAPIQuery({});

    return (
        <BoardListFormItem {...props} options={data} isLoading={isLoading} />
    );
};

export default ProjectBoardListFormItem;
