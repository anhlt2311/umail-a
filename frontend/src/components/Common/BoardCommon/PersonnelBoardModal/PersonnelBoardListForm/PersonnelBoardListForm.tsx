import React, { useState } from "react";
import { Button, Col, Row, Popover } from "antd";
import BoardAssigneeFormItem from "~/components/Common/BoardCommon/BoardAssigneeFormItem/BoardAssigneeFormItem";
import PersonnelBoardListFormItem from "~/components/Common/BoardCommon/BoardListFormItem/PersonnelBoardListFormItem";
import BoardPeriodEndFormItem from "~/components/Common/BoardCommon/BoardPeriodEndFormItem/BoardPeriodEndFormItem";
import BoardPeriodIsFinishedFormItem from "~/components/Common/BoardCommon/BoardPeriodIsFinishedFormItem/BoardPeriodIsFinishedFormItem";
import BoardPeriodStartFormItem from "~/components/Common/BoardCommon/BoardPeriodStartFormItem/BoardPeriodStartFormItem";
import BoardPrioritySelectFormItem from "~/components/Common/BoardCommon/BoardPrioritySelectFormItem/BoardPrioritySelectFormItem";
import {
  PersonnelBoardDetailModel,
  PersonnelBoardCopyCardFormModel,
} from "~/models/personnelBoardModel";
import PersonnelBoardCopyCardPopover from "~/components/Common/BoardCommon/PersonnelBoardCopyCardPopover/PersonnelBoardCopyCardPopover";
import { confirmModal } from "~/components/Modals/ConfirmModal";

type Props = {
  personnelValues?: PersonnelBoardDetailModel;
  onDuplicateCard: (data: PersonnelBoardCopyCardFormModel) => void;
  onDeleteCard: (cardid: string) => void;
};

const PersonnelBoardListForm = ({ personnelValues, onDuplicateCard, onDeleteCard }: Props) => {
  const [isCopyCardPopover, setCopyCardPopover] = useState(false);

  const onConfirmDelete = () => {
    confirmModal({
      title: "このカードを削除しますか？",
      content: (
        <div>
          <p>{`OKを押すと、削除が実行されます。元には戻せません。`}</p>
        </div>
      ),
      onOk: () => onDeleteCard(personnelValues?.id as string),
      onCancel: () => {},
    });
  };

  const onCopyCard = (data: PersonnelBoardCopyCardFormModel) => {
    onDuplicateCard(data);
    setCopyCardPopover(false);
  };

  const onToggleCopyPopover = () => {
    setCopyCardPopover(!isCopyCardPopover);
  }

  return (
    <Col>
      <PersonnelBoardListFormItem labelCol={{ span: 24 }} />
      <BoardAssigneeFormItem labelCol={{ span: 24 }} />
      <BoardPrioritySelectFormItem labelCol={{ span: 24 }} />
      <BoardPeriodStartFormItem labelCol={{ span: 24 }} />
      <BoardPeriodEndFormItem
        labelCol={{ span: 24 }}
        personnelValues={personnelValues}
      />
      <Row justify="end">
        <BoardPeriodIsFinishedFormItem
          noStyle
          personnelValues={personnelValues}
        />
      </Row>
      <Row justify="space-between" style={{ marginTop: 30, marginBottom: 10 }}>
        <Popover
          key={"copy-card"}
          content={
            <PersonnelBoardCopyCardPopover
              personnelValues={personnelValues}
              onCoppy={onCopyCard}
              onClose={onToggleCopyPopover}
            />
          }
          visible={isCopyCardPopover}
          onVisibleChange={onToggleCopyPopover}
          trigger={["click"]}
          placement="bottomRight"
        >
          <Button style={{ width: "45%" }} onClick={onToggleCopyPopover}>
            コピーを作成
          </Button>
        </Popover>
        <Button
          style={{ width: "45%" }}
          type="primary"
          danger
          onClick={onConfirmDelete}
        >
          削除
        </Button>
      </Row>
      <Button htmlType="submit" type="primary" style={{ width: "100%" }}>
        更新
      </Button>
    </Col>
  );
};

export default PersonnelBoardListForm;
