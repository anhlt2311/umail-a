
import React, { useEffect, useState, useMemo } from "react";
import { Spin, Switch, Tabs, Row, Col, Typography } from "antd";
import { useForm } from 'antd/lib/form/Form';
import moment from 'moment';
import { useRecoilState } from 'recoil';
import BoardBaseModal from '~/components/Common/BoardCommon/BoardBaseModal/BoardBaseModal';
import { useUpdatePersonnelCardBoard } from '~/hooks/usePersonnelBoard';
import { BoardChecklistItemUpdateFormModel, BoardChecklistModel, BoardChecklistUpdateFormModel, BoardCommentModel } from '~/models/boardCommonModel';
import {
  PersonnelBoardDetailModel,
  PersonnelBoardCopyCardFormModel,
} from "~/models/personnelBoardModel";
import { personnelBoard } from '~/recoil/atom';
import BoardScheduledEmailTemplate from '../BoardScheduledEmailTemplate/BoardScheduledEmailTemplate';
import TabsPanePersonnelForm from '../TabsPanePersonnelForm/TabsPanePersonnelForm';
import FormPersonnelInfo from './TabsPanePersonnelBoard/ListFormPersonnelInfo/FormPersonnelInfo';
import FormContractInformation from './TabsPanePersonnelBoard/ListFormContractInformation/FormContractInformation';
import DeliveryHistoryTable from './TabsPanePersonnelBoard/DeliveryHistoryTable/DeliveryHistoryTable';
import AboutThisData from "~/components/Common/AboutThisData/AboutThisData";
import _ from 'lodash'
import styles from './PersonnelBoardModal.scss'

const { Text } = Typography;

type Props = {
    dataCheckLists?: BoardChecklistModel[];
    initialData?: PersonnelBoardDetailModel;
    isLoading: boolean;
    dataComments: BoardCommentModel[];
    dataContacts: any;
    dataOrganizations: any;
    onDeleteCheckList: (idCheckList: string) => void;
    onAddItem: (value: string, idCheckList: string) => () => void;
    onChecklistItemSubmit: (values: BoardChecklistItemUpdateFormModel, checklistId: string) => void;
    onSubmitFinishContent: (values: BoardChecklistUpdateFormModel, checkListId: string) => void;
    onCreateCheckList: () => void;
    onDropdownVisibleChangeOrganizations: (open: boolean) => void;
    onDropdownVisibleChangeContacts: (open: boolean) => void;
    onDuplicateCard: (data: PersonnelBoardCopyCardFormModel) => void;
    onDeleteCard: (cardId: string) => void;
    onRefetchBoard: () => void;
}

const { TabPane } = Tabs;
const PersonnelBoardModal = (props: Props) => {
    const {
        initialData,
        dataCheckLists,
        isLoading,
        dataContacts,
        dataOrganizations,
        onDeleteCheckList,
        onAddItem,
        onChecklistItemSubmit,
        onSubmitFinishContent,
        onCreateCheckList,
        onDropdownVisibleChangeOrganizations,
        onDropdownVisibleChangeContacts,
        onDuplicateCard,
        onDeleteCard,
        onRefetchBoard
    } = props;

    const { updatePersonnelCardBoard, dataUpdates } = useUpdatePersonnelCardBoard();
    const [personnelBoardState, setPersonnelBoardState] = useRecoilState(personnelBoard);

    const _isArchived = useMemo(() => initialData?.isArchived || false,[initialData?.isArchived]);
    const _initialData = useMemo(() => initialData ,[initialData]);


    const [isArchived, setIsArchived] = useState<boolean>(false);

    const [form] = useForm<PersonnelBoardDetailModel>();

    useEffect(() => {
        if (_initialData) {
            form.setFieldsValue(_initialData);
        }
    }, [_initialData])

    useEffect(() => setIsArchived(_isArchived), [_isArchived]);

    useEffect(()=> {
      if(dataUpdates) {
        setPersonnelBoardState({
          ...personnelBoardState,
          isUpdatePersonnel: false,
        });
      }
    }, [dataUpdates])

    const onFinishPersonnelInfo = (values: PersonnelBoardDetailModel) => {
        let _data = { ...values, isArchived };
        _data = _.omit(_data, "image");
        _data = _.omit(_data, "skillSheet");
        console.log("data-->", _data);
        
        updatePersonnelCardBoard(_data, initialData?.id || "");
    }


    const onChangeBirthday = (_: any, dateString: string) => {
        const age = moment().diff(dateString, 'years');
        const data = form.getFieldsValue();
        form.setFieldsValue({
            ...data,
            age
        })
    }

    const onCloseModal = () => {
      setPersonnelBoardState({
        ...personnelBoardState,
        isUpdatePersonnel: false,
      });
    }

    const onDuplicate = (body: PersonnelBoardCopyCardFormModel) => {
      onDuplicateCard(body);
      onCloseModal()
    };

    const onDeletePersonnelCard = (cardId: string) => {
      onDeleteCard(cardId);
      onCloseModal();
    };

    useEffect(() => {
      if (_initialData) {
        form.setFieldsValue(_initialData);
      }
    }, [_initialData]);

    useEffect(() => setIsArchived(_isArchived), [_isArchived]);

    useEffect(() => {
      if (dataUpdates) {
        onRefetchBoard();
        onCloseModal();
      }
    }, [dataUpdates]);

    return (
      <Spin spinning={isLoading}>
        <BoardBaseModal
          mask={false}
          footer={false}
          visible={personnelBoardState.isUpdatePersonnel}
          onCancel={onCloseModal}
          isArchived={isArchived}
        >
          <Col className={styles.modifiedWrapper}>
            <AboutThisData
              data={{
                created_user: initialData?.createdUserName,
                created_time: moment(initialData?.createdTime).format(
                  "YYYY-MM-DD HH:mm"
                ),
                modified_user: initialData?.modifiedUserName,
                modified_time: moment(initialData?.modifiedTime).format(
                  "YYYY-MM-DD HH:mm"
                ),
              }}
            />
          </Col>
          <Tabs defaultActiveKey="1">
            <TabPane tab="要員情報" key="1">
              <TabsPanePersonnelForm
                initialData={initialData}
                onChecklistItemSubmit={onChecklistItemSubmit}
                dataCheckLists={dataCheckLists || []}
                onSubmitFinishContent={onSubmitFinishContent}
                form={form}
                onDeleteCheckList={onDeleteCheckList}
                onFinish={onFinishPersonnelInfo}
                onAddItem={onAddItem}
                onCreateCheckList={onCreateCheckList}
                onDuplicateCard={onDuplicate}
                onDeleteCard={onDeletePersonnelCard}
              >
                <FormPersonnelInfo
                  onChangeBirthday={onChangeBirthday}
                  initialData={initialData}
                />
              </TabsPanePersonnelForm>
            </TabPane>
            <TabPane tab="契約情報" key="2">
              <TabsPanePersonnelForm
                initialData={initialData}
                onChecklistItemSubmit={onChecklistItemSubmit}
                onSubmitFinishContent={onSubmitFinishContent}
                onDeleteCheckList={onDeleteCheckList}
                dataCheckLists={dataCheckLists || []}
                form={form}
                onFinish={onFinishPersonnelInfo}
                onAddItem={onAddItem}
                onCreateCheckList={onCreateCheckList}
                onDuplicateCard={onDuplicate}
                onDeleteCard={onDeletePersonnelCard}
              >
                <FormContractInformation
                  initialData={initialData}
                  dataContacts={dataContacts}
                  dataOrganizations={dataOrganizations}
                  onDropdownVisibleChangeOrganizations={
                    onDropdownVisibleChangeOrganizations
                  }
                  onDropdownVisibleChangeContacts={
                    onDropdownVisibleChangeContacts
                  }
                />
              </TabsPanePersonnelForm>
            </TabPane>
            <TabPane tab="配信メールテンプレート" key="3">
              <TabsPanePersonnelForm
                initialData={initialData}
                onChecklistItemSubmit={onChecklistItemSubmit}
                onSubmitFinishContent={onSubmitFinishContent}
                onDeleteCheckList={onDeleteCheckList}
                dataCheckLists={dataCheckLists || []}
                form={form}
                onFinish={onFinishPersonnelInfo}
                onAddItem={onAddItem}
                onCreateCheckList={onCreateCheckList}
                onDuplicateCard={onDuplicate}
                onDeleteCard={onDeletePersonnelCard}
              >
                <BoardScheduledEmailTemplate form={form} idCard={initialData?.id}/>
              </TabsPanePersonnelForm>
            </TabPane>
            <TabPane tab="配信履歴" key="4">
              <TabsPanePersonnelForm
                initialData={initialData}
                onChecklistItemSubmit={onChecklistItemSubmit}
                onSubmitFinishContent={onSubmitFinishContent}
                onDeleteCheckList={onDeleteCheckList}
                dataCheckLists={dataCheckLists || []}
                form={form}
                onFinish={onFinishPersonnelInfo}
                onAddItem={onAddItem}
                onCreateCheckList={onCreateCheckList}
                onDuplicateCard={onDuplicate}
                onDeleteCard={onDeletePersonnelCard}
              >
                <DeliveryHistoryTable />
              </TabsPanePersonnelForm>
            </TabPane>
            <TabPane
              disabled
              tab={
                <Row>
                  <Text>アーカイブ</Text>
                  <Switch checked={isArchived} onChange={setIsArchived} />
                </Row>
              }
            />
          </Tabs>
        </BoardBaseModal>
      </Spin>
    );
}

export default PersonnelBoardModal;
