import {
  MinusOutlined
} from "@ant-design/icons";
import { Button, Col, Form, Input, Row, Select, Typography } from 'antd';
import React, { CSSProperties, useEffect, useState } from 'react';
import CustomRangePicker from "~/components/Common/CustomRangePicker/CustomRangePicker";
import { disabledFutureDates } from "~/utils/utils";
import styles from "./FormContractInformation.scss";
import { useUpdateContractInformation } from '~/hooks/usePersonnelBoard';
import { useRecoilState } from "recoil";
import { personnelBoard } from "~/recoil/atom";
import { PersonnelBoardContractUpdateFormModel } from '~/models/personnelBoardModel';
import { useForm } from 'antd/lib/form/Form';
import { result } from "lodash";

const { Text } = Typography;
const { TextArea } = Input;

const marginNone: CSSProperties = {
  marginBottom: 0
}

const marginFormStyle: CSSProperties = {
  marginBottom: 10,
  width: '100%'
}

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};

type Props = {
  initialData: any;
  dataContacts: any;
  dataOrganizations: any;
  onDropdownVisibleChangeOrganizations: (open: boolean) => void;
  onDropdownVisibleChangeContacts: (open: boolean) => void;
}
function FormContractInformation({
  initialData,
  dataContacts,
  dataOrganizations,
  onDropdownVisibleChangeOrganizations,
  onDropdownVisibleChangeContacts
}: Props) {
  const { updateContractInformation } = useUpdateContractInformation();
  const [personnelBoardState, setPersonnelBoardState] = useRecoilState(personnelBoard);
  const [form] = useForm<PersonnelBoardContractUpdateFormModel>(); 
  const [hightContactList, setHightContactList] = useState(dataContacts?.results);
  const [hightOrganizationList, setHightOrganizationList] = useState(dataOrganizations?.results);
  const [lowerContactList, setLowerContactList] = useState(dataContacts?.results);
  const [lowerOrganizationList, setLowerOrganizationList] = useState(dataOrganizations?.results);
  const [higherOrganization, setHigherOrganization] = useState<string | undefined>(undefined);

  const onFinish = (values: any) => {
      values.cardId = personnelBoardState.cardId;
      updateContractInformation({
          ...values,
      });
  };

  const onChangeOrganization = (value:any, _:any) => {
      const nameSelect = _.name;
      if(nameSelect === 'higherOrganization'){
          const contactsFilter:any = dataContacts?.results?.filter((item:any) => item.organization == value)
          setHightContactList([...contactsFilter])
          if(form.getFieldValue("higherContact")){
              form.setFieldsValue({
                  higherContact: undefined
              })
          }
      }else{
          const contactsFilter:any = dataContacts?.results?.filter((item:any) => item.organization == value)
          setLowerContactList([...contactsFilter])
          if(form.getFieldValue("higherContact")){
              form.setFieldsValue({
                  lowerContact: undefined
              })
          }
      }
  }

  const onChangeContact = (value:any, _:any) => {
      const nameSelect = _.name;
      if(nameSelect === 'higherContact'){
          const contact:any = dataContacts?.results?.find((item:any) => item.id == value)
          const contactsFilter:any = dataOrganizations?.results?.find((item:any) => item.id == contact.organization)
          const higherOrganization = contactsFilter.id
          form.setFieldsValue({
              higherOrganization: higherOrganization
          })
      }else{
          const contact:any = dataContacts?.results?.find((item:any) => item.id == value)
          const contactsFilter:any = dataOrganizations?.results?.find((item:any) => item.id == contact.organization)
          form.setFieldsValue({lowerOrganization: contactsFilter.id})
          const lowerOrganization = contactsFilter.id
          form.setFieldsValue({
              lowerOrganization: lowerOrganization
          })
      }
  }

  return (
      <Form onFinish={onFinish} form={form}>
          <Col span={22}>
              <Row justify='center'>
                  <Button type="primary" style={{marginBottom: 20}} htmlType="submit">契約情報を追加</Button>
              </Row>
              <Form.Item
                  label="案件概要"
                  style={marginNone}
                  {...formItemLayout}
              >
                  <Row>
                      <Col span={24}>
                          <Form.Item name="detail" style={marginFormStyle}>
                              <Input/>
                          </Form.Item>
                      </Col>
                  </Row>
              </Form.Item>
              <Form.Item
                  label="契約期間"
                  style={marginNone}
                  {...formItemLayout}
              >
                  <Row>
                      <Col span={24}>
                      <Form.Item
                          colon={false}
                          name="projectPeriod"
                          noStyle>
                          <CustomRangePicker
                              className={styles.container}
                              placeholder={["設立年月(開始)", "設立年月(終了)"]}
                              picker="month"
                              disabled={false}
                              inputReadOnly
                              disabledDate={disabledFutureDates}
                          />
                      </Form.Item>
                      </Col>
                  </Row>
              </Form.Item>
              <Form.Item
                  label="契約単金"
                  style={marginNone}
                  {...formItemLayout}
              >
                  <Row>
                      <Col span={22}>
                          <Form.Item name="price" style={{marginTop: 10, marginBottom: 10}}>
                              <Input/>
                          </Form.Item>
                      </Col>
                      <Col span={2}>
                          <Text style={{ marginLeft: 10, marginTop: 17, display: 'block' }}>万円</Text>
                      </Col>
                  </Row>
              </Form.Item>
              <Form.Item
                  label="上位"
                  style={marginNone}
                  {...formItemLayout}
              >
                  <Row>
                      <Col span={24}>
                          <Form.Item name="higherOrganization" style={marginFormStyle}>
                              <Select placeholder="クリックして選択" onChange={onChangeOrganization} >
                                  {(hightOrganizationList || []).map(
                                      (entry:any, index:number) => {
                                          return (
                                              <Select.Option
                                                  key={index}
                                                  name="higherOrganization"
                                                  value={entry.id}>
                                                  {entry.name}
                                              </Select.Option>
                                          );
                                      }
                                  )}
                              </Select>
                          </Form.Item>
                      </Col>
                  </Row>
              </Form.Item>
              <Form.Item
                  label="上位担当者"
                  style={marginNone}
                  {...formItemLayout}
              >
                  <Row>
                      <Col span={24}>
                          <Form.Item name="higherContact" style={marginFormStyle}>
                              <Select placeholder="クリックして選択" onChange={onChangeContact}>
                                  {(hightContactList || []).map(
                                      (entry:any, index:number) => {
                                          return (
                                              <Select.Option
                                                  key={index}
                                                  name="higherContact"
                                                  value={entry.id}>
                                                  {entry.display_name}
                                              </Select.Option>
                                          );
                                      }
                                  )}
                              </Select>
                          </Form.Item>
                      </Col>
                  </Row>
              </Form.Item>
              <Form.Item
                  label="下位"
                  style={marginNone}
                  {...formItemLayout}
              >
                  <Row>
                      <Col span={24}>
                          <Form.Item name="lowerOrganization" style={marginFormStyle}>
                              <Select placeholder="クリックして選択" onChange={onChangeOrganization}>
                                  {(lowerOrganizationList || []).map(
                                      (entry:any, index:number) => {
                                          return (
                                              <Select.Option
                                                  key={index}
                                                  value={entry.id}>
                                                  {entry.name}
                                              </Select.Option>
                                          );
                                      }
                                  )}
                              </Select>
                          </Form.Item>
                      </Col>
                  </Row>
              </Form.Item>
              <Form.Item
                  label="下位担当者"
                  style={marginNone}
                  {...formItemLayout}
              >
                  <Row>
                      <Col span={24}>
                          <Form.Item name="lowerContact" style={marginFormStyle}>
                              <Select placeholder="クリックして選択" onChange={onChangeContact}>
                                  {(lowerContactList || []).map(
                                      (entry:any, index:number) => {
                                          return (
                                              <Select.Option
                                                  key={index}
                                                  name="lowerContact"
                                                  value={entry.id}>
                                                  {entry.display_name}
                                              </Select.Option>
                                          );
                                      }
                                  )}
                              </Select>
                          </Form.Item>
                      </Col>
                  </Row>
              </Form.Item>
              <Row justify='end'>
                  <Button
                      style={{marginBottom: 60}}
                      type="primary"
                      danger
                      icon={<MinusOutlined />}
                  ></Button>
              </Row>
          </Col>
      </Form>
  )
}

export default FormContractInformation;

