import { Spin, Switch, Tabs, Row, Typography } from "antd";
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { useForm } from 'antd/lib/form/Form';
import React, { useEffect, useState } from 'react';
import { useRecoilState } from 'recoil';
import BoardBaseModal from '~/components/Common/BoardCommon/BoardBaseModal/BoardBaseModal';
import { useUpdateProjectCardBoard } from '~/hooks/useProjectBoard';
import { BoardChecklistItemUpdateFormModel, BoardChecklistModel, BoardChecklistUpdateFormModel, BoardCommentModel } from '~/models/boardCommonModel';
import { ProjectBoardDetailModel } from '~/models/projectBoardModel';
import { PersonnelBoardDetailModel } from "~/models/personnelBoardModel";
import { projectBoard } from '~/recoil/atom';
import BoardScheduledEmailTemplate from '../BoardScheduledEmailTemplate/BoardScheduledEmailTemplate';
import TabsPaneProjectForm from '../TabsPaneProjectForm/TabsPaneProjectForm';
import FormProjectInfo from './TabsPaneProjectBoard/FormProjectInfo/FormProjectInfo';
import FormContractInformation from './TabsPaneProjectBoard/FormContractInformation/FormContractInformation';
import DeliveryHistoryTable from './TabsPaneProjectBoard/DeliveryHistoryTable/DeliveryHistoryTable';
import _ from 'lodash'

const { Text } = Typography;

type Props = {
    dataCheckLists?: BoardChecklistModel[];
    initialData?: ProjectBoardDetailModel;
    isLoading: boolean;
    dataComments: BoardCommentModel[];
    dataContacts: any;
    dataOrganizations: any;
    onDeleteCheckList: (idCheckList: string) => void;
    onAddItem: (value: string, idCheckList: string) => () => void;
    onChecklistItemSubmit: (values: BoardChecklistItemUpdateFormModel, checklistId: string) => void;
    onSubmitFinishContent: (values: BoardChecklistUpdateFormModel, checkListId: string) => void;
    onCreateCheckList: () => void;
    onDropdownVisibleChangeOrganizations: (open: boolean) => void;
    onDropdownVisibleChangeContacts: (open: boolean) => void;
}

const { TabPane } = Tabs;
function UpdateProjectBoardModal(props: Props) {
    const {
        initialData,
        dataCheckLists,
        isLoading,
        dataComments,
        dataContacts,
        dataOrganizations,
        onDeleteCheckList,
        onAddItem,
        onChecklistItemSubmit,
        onSubmitFinishContent,
        onCreateCheckList,
        onDropdownVisibleChangeOrganizations,
        onDropdownVisibleChangeContacts
    } = props;
    
    const typeCard = 'project';
    const { updateProjectCardBoard } = useUpdateProjectCardBoard();
    const [projectBoardState, setProjectBoardState] = useRecoilState(projectBoard);
    const [isArchived,setIsArchived]=useState<boolean>(false);
    const [form] = useForm<ProjectBoardDetailModel>();
    const [formPersonnel] = useForm<PersonnelBoardDetailModel>();

    useEffect(() => {
        if (initialData) {
            form.setFieldsValue(initialData)
            setIsArchived(initialData.isArchived || false);
        }
    }, [initialData])
    
    const onFinishProjectInfo = (values: ProjectBoardDetailModel) => {
        let _data = { ...values, isArchived };
        _data = _.omit(_data, "image");
        _data = _.omit(_data, "skillSheet");
        updateProjectCardBoard(_data, initialData?.id || "");
    }
    

    const onChangeBirthday = (_: any, dateString: string) => {
        const data = form.getFieldsValue();
        form.setFieldsValue({
            ...data,
        })
    }

    const onChangeArchived = (e: CheckboxChangeEvent) => {
        setIsArchived(e.target.checked)
    }

    const onCancel = () => {
        setProjectBoardState({
            ...projectBoardState,
            isUpdateProject: false
        })
    }
    return (
      <Spin spinning={isLoading}>
        <BoardBaseModal
          mask={false}
          footer={false}
          visible={projectBoardState.isUpdateProject}
          onCancel={onCancel}
        >
          <Tabs defaultActiveKey="1">
            <TabPane tab="要員情報" key="1">
              <TabsPaneProjectForm
                initialData={initialData}
                onChecklistItemSubmit={onChecklistItemSubmit}
                dataCheckLists={dataCheckLists || []}
                onSubmitFinishContent={onSubmitFinishContent}
                form={form}
                onDeleteCheckList={onDeleteCheckList}
                onFinish={onFinishProjectInfo}
                dataComments={dataComments}
                onAddItem={onAddItem}
                onCreateCheckList={onCreateCheckList}
              >
                <FormProjectInfo
                  onChangeArchived={onChangeArchived}
                  onChangeBirthday={onChangeBirthday}
                  initialData={initialData}
                  isArchived={isArchived}
                />
              </TabsPaneProjectForm>
            </TabPane>
            <TabPane tab="契約情報" key="2">
              <TabsPaneProjectForm
                initialData={initialData}
                onChecklistItemSubmit={onChecklistItemSubmit}
                onSubmitFinishContent={onSubmitFinishContent}
                onDeleteCheckList={onDeleteCheckList}
                dataCheckLists={dataCheckLists || []}
                form={form}
                onFinish={onFinishProjectInfo}
                dataComments={dataComments}
                onAddItem={onAddItem}
                onCreateCheckList={onCreateCheckList}
              >
                <FormContractInformation
                  initialData={initialData}
                  dataContacts={dataContacts}
                  dataOrganizations={dataOrganizations}
                  onDropdownVisibleChangeOrganizations={
                    onDropdownVisibleChangeOrganizations
                  }
                  onDropdownVisibleChangeContacts={
                    onDropdownVisibleChangeContacts
                  }
                />
              </TabsPaneProjectForm>
            </TabPane>
            <TabPane tab="配信メールテンプレート" key="3">
              <TabsPaneProjectForm
                initialData={initialData}
                onChecklistItemSubmit={onChecklistItemSubmit}
                onSubmitFinishContent={onSubmitFinishContent}
                onDeleteCheckList={onDeleteCheckList}
                dataCheckLists={dataCheckLists || []}
                form={form}
                onFinish={onFinishProjectInfo}
                dataComments={dataComments}
                onAddItem={onAddItem}
                onCreateCheckList={onCreateCheckList}
              >
                <BoardScheduledEmailTemplate form={formPersonnel} idCard={initialData?.id}/>
              </TabsPaneProjectForm>
            </TabPane>
            <TabPane tab="配信履歴" key="4">
              <TabsPaneProjectForm
                initialData={initialData}
                onChecklistItemSubmit={onChecklistItemSubmit}
                onSubmitFinishContent={onSubmitFinishContent}
                onDeleteCheckList={onDeleteCheckList}
                dataCheckLists={dataCheckLists || []}
                form={form}
                onFinish={onFinishProjectInfo}
                dataComments={dataComments}
                onAddItem={onAddItem}
                onCreateCheckList={onCreateCheckList}
              >
                <DeliveryHistoryTable />
              </TabsPaneProjectForm>
            </TabPane>
            <TabPane
              disabled
              tab={
                <Row>
                  <Text>アーカイブ</Text>
                  <Switch checked={isArchived} onChange={setIsArchived} />
                </Row>
              }
            />
          </Tabs>
        </BoardBaseModal>
      </Spin>
    );
}

export default UpdateProjectBoardModal;

