import { Button, Col, Row } from 'antd'
import React from 'react'
import BoardAssigneeFormItem from '~/components/Common/BoardCommon/BoardAssigneeFormItem/BoardAssigneeFormItem'
import ProjectBoardListFormItem from '~/components/Common/BoardCommon/BoardListFormItem/ProjectBoardListFormItem'
import BoardPeriodEndFormItem from '~/components/Common/BoardCommon/BoardPeriodEndFormItem/BoardPeriodEndFormItem'
import BoardPeriodIsFinishedFormItem from '~/components/Common/BoardCommon/BoardPeriodIsFinishedFormItem/BoardPeriodIsFinishedFormItem'
import BoardPeriodStartFormItem from '~/components/Common/BoardCommon/BoardPeriodStartFormItem/BoardPeriodStartFormItem'
import BoardPrioritySelectFormItem from '~/components/Common/BoardCommon/BoardPrioritySelectFormItem/BoardPrioritySelectFormItem'

function ProjectBoardListForm() {
    return (
            <Col>
                <ProjectBoardListFormItem labelCol={{ span: 24 }} />
                <BoardAssigneeFormItem labelCol={{ span: 24 }}/>
                <BoardPrioritySelectFormItem labelCol={{ span: 24 }}/>
                <BoardPeriodStartFormItem labelCol={{ span: 24 }}/>
                <BoardPeriodEndFormItem labelCol={{ span: 24 }}/>
                <Row justify="end">
                    <BoardPeriodIsFinishedFormItem noStyle/>
                </Row>
                <Row justify="space-between" style={{ marginTop: 30, marginBottom: 10 }}>
                    <Button style={{ width: '45%' }}>コピーを作成</Button>
                    <Button style={{ width: '45%' }} type="primary" danger>削除</Button>
                </Row>
                <Button htmlType="submit" type="primary" style={{ width: '100%' }}>更新</Button>
            </Col>
    )
}

export default ProjectBoardListForm;

