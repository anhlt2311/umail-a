import React from "react";
import { Col, Form, Input, Row } from "antd";
const { TextArea } = Input;
import { FormListFieldData } from "antd/lib/form/FormList";
import { ErrorMessages } from "~/utils/constants";

type Props = FormListFieldData & {
    key: number;
};

type DynamicRow = {
    title?: string;
    content?: string;
};

const DescriptionUserInfoFormItem = ({ name, ...props }: Props) => {
    return (
        <Form.Item
            {...props}
            style={{
                marginBottom: 10,
            }}
        >
            <Row gutter={10}>
                <Col span={4}>
                    <Row justify="space-between">
                        <Col span={21}>
                            <Form.Item
                                name={[name, "title"]}
                                noStyle
                                rules={[
                                    ({ getFieldValue }) => ({
                                        validator: (_, value) => {
                                            const title = value || null;

                                            const dynamicRows: DynamicRow[] =
                                                getFieldValue("dynamicRows") || [];

                                            if (!!title && dynamicRows.length) {
                                                const checkDuplication = dynamicRows.filter(
                                                    (item) => item?.title === title
                                                );
                                                if (!!checkDuplication && checkDuplication.length > 1) {
                                                    return Promise.reject(
                                                        new Error(
                                                            `${title}と同一名称の項目が既に存在します。別の項目名を入力してください。`
                                                        )
                                                    );
                                                }
                                                return Promise.resolve();
                                            }

                                            return Promise.resolve();
                                        },
                                    }),
                                    {
                                        max: 10,
                                        message: ErrorMessages.validation.length.max10,
                                    },
                                ]}
                            >
                                <TextArea placeholder="項目名" autoSize={{ minRows: 1 }} />
                            </Form.Item>
                        </Col>
                        <Col>
                            <Row align="middle">
                                <span>:</span>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col span={20}>
                    <Form.Item
                        name={[name, "content"]}
                        noStyle
                        rules={[
                            {
                                max: 300,
                                message: ErrorMessages.validation.length.max300,
                            },
                        ]}
                    >
                        <TextArea placeholder="内容" autoSize={{ minRows: 1 }} />
                    </Form.Item>
                </Col>
            </Row>
        </Form.Item>
    );
};

export default DescriptionUserInfoFormItem;
