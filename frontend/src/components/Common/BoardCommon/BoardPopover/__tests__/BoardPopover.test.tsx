import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import BoardPopover from "../BoardPopover";

const mockOnClose = jest.fn();

describe("BoardPopover.tsx", () => {
    const closeButtonTestId = "close-button";

    test("render test", () => {
        render(
            <BoardPopover title="Header Title" onClose={mockOnClose}>
                content
            </BoardPopover>
        );
        const titleElement = screen.getByText(/Header Title/);
        expect(titleElement).toBeInTheDocument();
        const closeButtonElement = screen.getByTestId(closeButtonTestId);
        expect(closeButtonElement).toBeInTheDocument();
        const contentElement = screen.getByText(/content/);
        expect(contentElement).toBeInTheDocument();
    });

    test("onClose test", async () => {
        render(
            <BoardPopover title="Header Title" onClose={mockOnClose}>
                content
            </BoardPopover>
        );
        const closeButtonElement = screen.getByTestId(closeButtonTestId);
        await userEvent.click(closeButtonElement);
        expect(mockOnClose).toHaveBeenCalled();
    });
});
