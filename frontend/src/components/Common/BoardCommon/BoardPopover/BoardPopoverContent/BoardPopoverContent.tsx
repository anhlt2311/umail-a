import React, { ReactNode } from "react";
import { Col, Row } from "antd";
import styles from "./BoardPopoverContent.scss";

type Props = {
    children: ReactNode;
};

const BoardPopoverContent = ({ children }: Props) => {
    return (
        <Col span={24}>
            <Row>{children}</Row>
        </Col>
    );
};

export default BoardPopoverContent;
