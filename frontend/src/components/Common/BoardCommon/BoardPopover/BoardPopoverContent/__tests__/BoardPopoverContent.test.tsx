import React from "react";
import { render, screen } from "@testing-library/react";
import BoardPopoverContent from "../BoardPopoverContent";

describe("BoardPopoverContent.tsx", () => {
    test("render test", () => {
        render(<BoardPopoverContent>content</BoardPopoverContent>);
        const contentElement = screen.getByText(/content/);
        expect(contentElement).toBeInTheDocument();
    });
});
