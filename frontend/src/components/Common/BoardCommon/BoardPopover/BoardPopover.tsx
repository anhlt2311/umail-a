import React, { ReactNode } from "react";
import { Col, Row } from "antd";
import BoardPopoverHeader from "./BoardPopoverHeader/BoardPopoverHeader";
import styles from "./BoardPopover.scss";
import BoardPopoverContent from "./BoardPopoverContent/BoardPopoverContent";

type Props = {
    title: string;
    onClose: () => void;
    children: ReactNode;
};

const BoardPopover = ({ title, onClose, children }: Props) => {
    return (
        <Col span={24} className={styles.container}>
            <Row>
                <BoardPopoverHeader title={title} onClose={onClose} />
            </Row>
            <Row>
                <BoardPopoverContent>{children}</BoardPopoverContent>
            </Row>
        </Col>
    );
};

export default BoardPopover;
