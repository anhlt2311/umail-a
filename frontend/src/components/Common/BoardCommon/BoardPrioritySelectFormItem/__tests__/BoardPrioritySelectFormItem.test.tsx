import React from "react";
import { render, screen } from "~/test/utils";
import userEvent from "@testing-library/user-event";
import BoardPrioritySelectFormItem from "../BoardPrioritySelectFormItem";
import { Form } from "antd";

const Wrapper = () => {
    return (
        <Form initialValues={{ priority: { value: "critical" } }}>
            <BoardPrioritySelectFormItem />
        </Form>
    );
};

describe("BoardPrioritySelectFormItem.ts", () => {
    describe("render test", () => {
        test("without initial value", async () => {
            render(<BoardPrioritySelectFormItem />);
            const labelElement = screen.getByText(/優先度/);
            expect(labelElement).toBeInTheDocument();
            const selectElement = screen.getByText(/クリックして選択/);
            expect(selectElement).toBeInTheDocument();
        });

        test("with initial value", async () => {
            render(<Wrapper />);
            const labelElement = screen.getByText(/優先度/);
            expect(labelElement).toBeInTheDocument();
            const selectElement = await screen.findByText(/重大/);
            expect(selectElement).toBeInTheDocument();
        });

        test("options", async () => {
            render(<BoardPrioritySelectFormItem />);
            const selectElement = screen.getByText(/クリックして選択/);
            await userEvent.click(selectElement);
            const urgentOptionElement = await screen.findByText(/緊急/);
            expect(urgentOptionElement).toBeInTheDocument();
            const criticalOptionElement = await screen.findByText(/重大/);
            expect(criticalOptionElement).toBeInTheDocument();
            const highOptionElement = await screen.findByText(/高/);
            expect(highOptionElement).toBeInTheDocument();
            const mediumOptionElement = await screen.findByText(/中/);
            expect(mediumOptionElement).toBeInTheDocument();
            const lowOptionElement = await screen.findByText(/低/);
            expect(lowOptionElement).toBeInTheDocument();
        });
    });
});
