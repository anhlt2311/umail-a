import { Col, FormInstance, Row } from "antd";
import React from "react";
import { PersonnelBoardDetailModel } from "~/models/personnelBoardModel";
import { ProjectBoardDetailModel } from '~/models/projectBoardModel';
import styles from "./BoardScheduledEmailTemplate.scss";
import BoardScheduledEmailTemplateForm from "./BoardScheduledEmailTemplateForm/BoardScheduledEmailTemplateForm";

type Props = {
    idCard: string | undefined;
    form: FormInstance<PersonnelBoardDetailModel>;
}
const BoardScheduledEmailTemplate = ({ idCard, form }: Props) => {
    return (
        <Col span={24}>
            <Row>
                <Col span={24}>
                    <BoardScheduledEmailTemplateForm formPersonnel={form} idCard={idCard}/>
                </Col>
            </Row>
        </Col>
    );
};

export default BoardScheduledEmailTemplate;
