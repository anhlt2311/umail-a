import { DownOutlined, RightOutlined } from "@ant-design/icons";
import { Button, Form, Input, Radio, Row, Switch } from "antd";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import { FormInstance, useForm } from "antd/lib/form/Form";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import { PersonnelBoardDetailModel, PersonnelScheduledMailTemplateForm } from "~/models/personnelBoardModel";
import { dataDevelop, dataInfrastructure, dataOther } from "~/utils/constants";
import BoardScheduledEmailTemplateSubCheckbox from "../BoardScheduledEmailTemplateSubCheckbox/BoardScheduledEmailTemplateSubCheckbox";

const { TextArea } = Input;

const formItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 },
};

const formJobTypeLayout = {
    labelCol: { span: 6 },
};

const SCHEDULED_MAIL_REGISTER_PAGE__REDIRECT = "SCHEDULED_MAIL_REGISTER_PAGE__REDIRECT";

type Props = {
    idCard: string | undefined;
    formPersonnel: FormInstance<PersonnelBoardDetailModel>;
}

const BoardScheduledEmailTemplateForm = ({ idCard, formPersonnel }: Props) => {
    const history = useHistory();
    const [form] = useForm<PersonnelScheduledMailTemplateForm>();
    const [openFormCheck, setOpenFormCheck] = useState<boolean>(false);
    const [valuesInfo,setValuesInfo]=useState<string>('')
    const dispatch = useDispatch();

    const genTextInfo = (values: PersonnelBoardDetailModel) => {
      const _data = [
        {
          label: "名前",
          value: `${values.lastNameInitial}${values.firstNameInitial}`,
        },
        {
          label: "年齢",
          value: values.age,
        },
        {
          label: "性別",
          value: values.gender,
        },
        {
          label: "最寄駅",
          value: values.trainStation,
        },
        {
          label: "所属",
          value: values.affiliation,
        },
        {
          label: "スキル",
          value: values.skills && values.skills.toString(),
        },
        {
          label: "稼働",
          value: values.operatePeriod,
        },
        {
          label: "並行",
          value: values.parallel,
        },
        {
          label: "単金",
          value: values.price,
        },
        {
          label: "希望",
          value: values.request,
        },
      ];
      const result = _data.map(item => !!item.value ? `${item.label}: ${item.value}\n`: '')
      return result.join("")
    };

    const onOpen = () => {
        setOpenFormCheck(!openFormCheck);
    };
    const onFinish = (values: PersonnelScheduledMailTemplateForm) => {
        dispatch({
            type: SCHEDULED_MAIL_REGISTER_PAGE__REDIRECT,
            payload: {
                valuesPersonnel:values
            }
        })
        history.push(Paths.scheduledMailsRegister + '?personnelId=' + idCard);
    };

    const onCheckAllChangeDevelop = (event: CheckboxChangeEvent) => {
        const values = form.getFieldsValue();
        form.setFieldsValue({
            ...values,
            personneltype_dev_designer:event.target.checked,
            personneltype_dev_front:event.target.checked,
            personneltype_dev_server:event.target.checked,
            personneltype_dev_pm:event.target.checked,
            personneltype_dev_other:event.target.checked,
            personnelskill_dev_youken:event.target.checked,
            personnelskill_dev_kihon:event.target.checked,
            personnelskill_dev_syousai:event.target.checked,
            personnelskill_dev_seizou:event.target.checked,
            personnelskill_dev_test:event.target.checked,
            personnelskill_dev_hosyu:event.target.checked,
            personnelskill_dev_beginner:event.target.checked,
        })
    };

    const onCheckAllChangeInfra = (event: CheckboxChangeEvent) => {
        const values = form.getFieldsValue();
        form.setFieldsValue({
            ...values,
            personneltype_infra_server:event.target.checked,
            personneltype_infra_network:event.target.checked,
            personneltype_infra_security:event.target.checked,
            personneltype_infra_database:event.target.checked,
            personneltype_infra_sys:event.target.checked,
            personneltype_infra_other:event.target.checked,
            jobskill_dev_youken:event.target.checked,
            jobskill_dev_kihon:event.target.checked,
            jobskill_dev_syousai:event.target.checked,
            jobskill_dev_seizou:event.target.checked,
            jobskill_dev_test:event.target.checked,
            jobskill_dev_hosyu:event.target.checked,
            jobskill_infra_kanshi:event.target.checked,
            jobskill_dev_beginner:event.target.checked,
        })
    }

    const onCheckAllChangeOther = (event: CheckboxChangeEvent) => {
        const values = form.getFieldsValue();
        form.setFieldsValue({
            ...values,
            jobtype_other_eigyo:event.target.checked,
            jobtype_other_kichi:event.target.checked,
            jobtype_other_support:event.target.checked,
            jobtype_other_other:event.target.checked,
        })
    }

    const onChangeCheckBoxDevelop = (event: CheckboxChangeEvent) => {
        const values = form.getFieldsValue();
        form.setFieldsValue({
            ...values,
            personneltype_dev: event.target.checked
        })
    }

    const onChangeCheckBoxInfra = (event: CheckboxChangeEvent) => {
        const values = form.getFieldsValue();
        form.setFieldsValue({
            ...values,
            personneltype_infra: event.target.checked
        })
    }

    const onChangeCheckBoxOther = (event: CheckboxChangeEvent) => {
        const values = form.getFieldsValue();
        form.setFieldsValue({
            ...values,
            personneltype_other: event.target.checked
        })
    }

    useEffect(() => {
        const values = formPersonnel.getFieldsValue();
        const textInfo:string = genTextInfo(values);
        setValuesInfo(textInfo)
        form.setFieldsValue({
            text_format: 'text',
            values_info: textInfo,
        });
    },[])

    return (
        <Form
            form={form}
            onFinish={onFinish}
        >
            <Form.Item
                {...formItemLayout}
                label="フォーマット"
                name="text_format">
                <Radio.Group>
                    <Radio value="text">テキスト</Radio>
                    <Radio value="html">HTML</Radio>
                </Radio.Group>
            </Form.Item>
            <Form.Item label="件名" {...formItemLayout} name="subject">
                <Input size="large" />
            </Form.Item>
            <Form.Item {...formItemLayout} label="挿入文">
                <Form.Item name="text">
                    <TextArea autoSize={{ minRows: 3 }} />
                </Form.Item>
                <Form.Item name="values_info">
                    <TextArea disabled={true} autoSize={{ minRows: 5 }} />
                </Form.Item>
                <Form.Item>
                    <TextArea autoSize={{ minRows: 3 }} />
                </Form.Item>
                <Row justify="space-between">
                    <Form.Item
                        label="控えを配信者に送信"
                        name="send_copy_to_sender">
                        <Switch />
                    </Form.Item>
                    <Form.Item
                        label="控えを共有メールに送信"
                        name="send_copy_to_share">
                        <Switch />
                    </Form.Item>
                </Row>
            </Form.Item>
            <Form.Item {...formJobTypeLayout} label="配信職種">
                {!openFormCheck ? (
                    <RightOutlined onClick={onOpen} />
                ) : (
                    <DownOutlined onClick={onOpen} style={{ marginTop: 10 }} />
                )}
                {openFormCheck && (
                    <React.Fragment>
                        <BoardScheduledEmailTemplateSubCheckbox
                            title="開発"
                            onCheckAllChange={onCheckAllChangeDevelop}
                            onChangeCheckBox={onChangeCheckBoxDevelop}
                            data={dataDevelop}
                            name="personneltype_dev"
                            form={form}
                            label="配信職種詳細"
                        />
                        <BoardScheduledEmailTemplateSubCheckbox
                            title="インフラ"
                            form={form}
                            data={dataInfrastructure}
                            onChangeCheckBox={onChangeCheckBoxInfra}
                            name="personneltype_infra"
                            label="配信職種詳細"
                            onCheckAllChange={onCheckAllChangeInfra}
                        />
                        <BoardScheduledEmailTemplateSubCheckbox
                            title="その他"
                            form={form}
                            data={dataOther}
                            name="personneltype_other"
                            label="配信職種詳細"
                            onCheckAllChange={onCheckAllChangeOther}
                            onChangeCheckBox={onChangeCheckBoxOther}
                        />
                    </React.Fragment>
                )}
            </Form.Item>
            <Row justify="center">
                <Button style={{ width: 230 }} type="primary" htmlType="submit">
                    配信メール予約へ
                </Button>
            </Row>
        </Form>
    );
};

export default BoardScheduledEmailTemplateForm;
