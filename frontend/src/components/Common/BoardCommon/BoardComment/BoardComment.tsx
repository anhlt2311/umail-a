import React from 'react'
import { Col, Row, Typography } from 'antd'
import { useRecoilState } from 'recoil'
import CommentListCreator from '~/components/DataDisplay/CommentList/CommentList'
import { PERSONEL_DETAIL_BOARD_PAGE } from '~/components/Pages/pageIds'
import { Endpoint } from '~/domain/api'
import { personnelBoard } from '~/recoil/atom'

type Props = {}

const pageId = PERSONEL_DETAIL_BOARD_PAGE;
const baseURL = Endpoint.getBaseUrl();
const resourceURL = `${baseURL}/${Endpoint.personnelBoard}`;
const commentsReducerName = "personelBoardDetailPageComments";
const newCommentsReducerName = "personelBoardDetailPageNewComments";
const editCommentsReducerName = "personelBoardDetailPageEditComments";
const replyCommentReducerName = "personelBoardDetailPageReplyComments";
const commentTemplateUrl = `${Endpoint.getBaseUrl()}/${
    Endpoint.commentTemplatePersonelBoard
}`;
const { Title } = Typography;

const BoardComment = ({}: Props) => {

    const [personnelBoardState] = useRecoilState(personnelBoard);

    const { cardId } = personnelBoardState;

    const CommentList = CommentListCreator(
        pageId,
        commentsReducerName,
        commentTemplateUrl,
        newCommentsReducerName,
        editCommentsReducerName,
        replyCommentReducerName
    );
    return (
        <Col span={24} style={{ marginTop: "5%" }}>
            <Row justify="start">
                <Title level={5}>コメント一覧</Title>
            </Row>
            <CommentList
                resourceUrl={`${resourceURL}/${cardId}/${Endpoint.commentUrlSuffix}`}
            />
        </Col>
    )
}

export default React.memo(BoardComment)
