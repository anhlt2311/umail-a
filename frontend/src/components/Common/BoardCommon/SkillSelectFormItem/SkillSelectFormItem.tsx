
import React, { useState } from "react";
import { PlusOutlined, QuestionCircleOutlined } from "@ant-design/icons";
import {
    Button,
    Col,
    Form,
    FormItemProps,
    Row,
    Tooltip,
    Typography
} from "antd";
import NewTagModal from "~/components/Common/NewTagModal/NewTagModal";
import TagAjaxSelect from "~/components/Common/TagAjaxSelect/TagAjaxSelect";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { TagModel, TagSkillsSelectModel } from "~/models/tagModel";
import { ErrorMessages } from "~/utils/constants";
import { TYPE_SELECT } from "~/utils/types";
import styles from "./SkillSelectFormItem.scss";

const { Text } = Typography;

type Props = FormItemProps & {
    defaults: TagSkillsSelectModel[];
    onTagAdd: (newTag: TagModel) => void;
};

const SkillSelectFormItem = ({ onTagAdd, ...props }: Props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const { _all: tagAuthorized } = useAuthorizedActions("tags");

    const onNewTagModalOpen = () => {
        setIsModalVisible(true);
    };

    const onNewTagModalClose = () => {
        setIsModalVisible(false);
    };

    return (
        <Form.Item
            {...props}
            className={styles.field}
            label={
                <span>
                    スキル&nbsp;
                    <Tooltip title="スキルタグから選択してください。">
                        <QuestionCircleOutlined />
                    </Tooltip>
                </span>
            }
            style={{ margin: "8px 0 0" }}>
            <Row align="middle">
                <Col span={24}>
                    <Row align="middle">
                        <Col span={22}>
                            <Form.Item
                                name="skills"
                                rules={[
                                    {
                                        validator: (rule, value, callback) => {
                                            if (value) {
                                                if (value.length > 10) {
                                                    value.pop();
                                                    return Promise.resolve();
                                                } else if (value.length <= 10) {
                                                    if (
                                                        props.required &&
                                                        value.length == 0
                                                    ) {
                                                        return Promise.reject(
                                                            new Error(
                                                                ErrorMessages.generic.selectRequired
                                                            )
                                                        );
                                                    } else {
                                                        return Promise.resolve();
                                                    }
                                                }
                                            } else {
                                                if (props.required) {
                                                    return Promise.reject(
                                                        new Error(
                                                            ErrorMessages.generic.selectRequired
                                                        )
                                                    );
                                                } else {
                                                    return Promise.resolve();
                                                }
                                            }
                                        },
                                    },
                                ]}
                                noStyle>
                                <TagAjaxSelect type={TYPE_SELECT.SKILL} />
                            </Form.Item>
                        </Col>
                        <Col
                            span={2}
                            style={{
                                textAlign: "right",
                            }}>
                            {tagAuthorized ? (
                                <Button
                                    className={styles.button}
                                    type="primary"
                                    onClick={onNewTagModalOpen}
                                    hidden={false}
                                    icon={
                                        <PlusOutlined data-testid="new-tag-icon" />
                                    }
                                />
                            ) : (
                                <Tooltip title={ErrorMessages.isNotAuthorized}>
                                    <Button
                                        className={styles.button}
                                        size="small"
                                        type="link"
                                        icon={<PlusOutlined />}
                                        style={{
                                            margin: "8px 0 0",
                                            cursor: "not-allowed",
                                        }}
                                    />
                                </Tooltip>
                            )}
                        </Col>
                    </Row>
                    <Row align="top" justify="start">
                        <Col>
                            <Text type="secondary">最大10個選択できます。</Text>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Form.Item noStyle>
                <NewTagModal
                    type={TYPE_SELECT.SKILL}
                    isVisible={isModalVisible}
                    onFinish={onTagAdd}
                    onModalClose={onNewTagModalClose}
                />
            </Form.Item>
        </Form.Item>
    );
};

export default SkillSelectFormItem;
