import { Form } from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { FormInstance, useForm } from 'antd/lib/form/Form';
import React, { ChangeEvent, useEffect, useState } from 'react';
import BoardTabPanesForms from '~/components/Common/BoardCommon/BoardTabPanesForms/BoardTabPanesForms';
import { confirmModal } from '~/components/Modals/ConfirmModal';
import { useCommentPersonnelCardBoard } from '~/hooks/usePersonnelBoard';
import { BoardChecklistItemUpdateFormModel, BoardChecklistModel, BoardChecklistUpdateFormModel, BoardCommentModel } from '~/models/boardCommonModel';
import { ProjectBoardDetailModel } from '~/models/projectBoardModel';

type Props = {
    children: React.ReactNode;
    onFinish: (values:ProjectBoardDetailModel) => void;
    initialData?: ProjectBoardDetailModel;
    dataComments: BoardCommentModel[];
    form: FormInstance<ProjectBoardDetailModel>;
    onChecklistItemSubmit: (values: BoardChecklistItemUpdateFormModel, checklistId: string) => void;
    dataCheckLists: BoardChecklistModel[];
    onDeleteCheckList: (idCheckList: string) => void;
    onAddItem: (value: string, idCheckList: string) => () => void;
    onSubmitFinishContent: (values: BoardChecklistUpdateFormModel, checkListId: string) => void;
    onCreateCheckList: () => void;
}
const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 24 }, 
};
function TabsPaneProjectForm(props: Props) {
    const {
        onChecklistItemSubmit,
        onFinish,
        initialData,
        dataCheckLists,
        children,
        dataComments,
        onDeleteCheckList,
        form,
        onAddItem,
        onSubmitFinishContent,
        onCreateCheckList
    } = props;

    const typeCard = 'project';
    const [isImportant, setIsImportant] = useState<boolean>(false);
    

    const {
        comments,
        setComments,
        mutateCreateComment,
        valueTextAreaComment,
        setValueTextAreaComment,
        mutateDeleteComment,
        mutatePinComment,
        mutateEditComment
    } = useCommentPersonnelCardBoard();

    useEffect(() => {
        if (dataComments) {
            setComments(dataComments)
        }
    },[dataComments])


    useEffect(() => {
        if (initialData) {
            form.setFieldsValue(initialData)
        }
    }, [initialData, form])

    const onChangeTextArea = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        setValueTextAreaComment(event.target.value)
    }

    const onCreateComment = () => {
        mutateCreateComment({
            cardId: initialData?.id || '',
            content: valueTextAreaComment,
            is_important:isImportant,
        })
    };

    const onPin = () => {
        setIsImportant(!isImportant)
    };

    const onPinComment = (item: BoardCommentModel) => () => {
        mutatePinComment(item.id, {
            id: initialData?.id || '',
            content: item.content,
            is_important: !item.isImportant,
        });
    }

    const onSaveEditCommentContent = (item: BoardCommentModel) => {
        mutateEditComment(item.id, {
            id: initialData?.id || '',
            content: item.content,
            is_important: item.isImportant,
        })
    }

    const onDeleteComment = (idComment: string) => () => {
        confirmModal({
            title: "このコメントを削除しますか？",
            content: (
                <div>
                    <p>{`OKを押すと、削除が実行されます。`}</p>
                </div>
            ),
            onOk: () => {
                onDelete(idComment)
            },
            onCancel: () => {},
        });
    }

    const onDelete = (idComment: string) => {
        mutateDeleteComment(idComment, initialData?.id || '');
    }
    
    return (
        <Form
            {...layout}
            form={form}
            onFinish={onFinish}
        >
            <BoardTabPanesForms
                typeCard={typeCard}
                dataComments={comments}
                onDelete={onDeleteCheckList}
                onSubmitFinishContent={onSubmitFinishContent}
                onChecklistItemSubmit={onChecklistItemSubmit}
                dataCheckLists={dataCheckLists}
                valueTextAreaComment={valueTextAreaComment}
                onCreateCheckList={onCreateCheckList}
                onChangeTextArea={onChangeTextArea}
                onCreateComment={onCreateComment}
                onPin={onPin}
                isImportant={isImportant}
                onPinComment={onPinComment}
                onDeleteComment={onDeleteComment}
                onSaveEditCommentContent={onSaveEditCommentContent}
                onAddItem={onAddItem}
            >
                {children}
            </BoardTabPanesForms>
        </Form>

    )
}

export default TabsPaneProjectForm;

