import React, { useState } from "react";
import { Form, FormItemProps, AutoComplete } from "antd";
import {
  PersonnelBoardTrainStationModel,
} from "~/models/personnelBoardModel";
import {
  usePersonnelBoardFetchTrainStationsAPIQuery
} from "~/hooks/usePersonnelBoard";
import { useCustomDebouncedCallback } from "~/hooks/useCustomDebouncedCallback";
import { ErrorMessages, RESTRICT_SPACE_REGEX } from "~/utils/constants";

type Props = FormItemProps & {};
type Deps = { name: string };

const BoardTrainStationSelectFormItem = ({ ...props }: Props) => {
    const [deps, setDeps] = useState<Deps>({ name: "" });
    const { data } = usePersonnelBoardFetchTrainStationsAPIQuery({ deps })
    
    const onSearch = useCustomDebouncedCallback((value: string) => {
      setDeps({
        name: value,
      });
    });

    return (
      <Form.Item
        name={"trainStation"}
        {...props}
        label="最寄駅"
        rules={[
          {
            max: 50,
            message: ErrorMessages.validation.length.max50,
          },
          {
            pattern: RESTRICT_SPACE_REGEX,
            message: ErrorMessages.validation.regex.space,
          },
        ]}
      >
        <AutoComplete
          showSearch
          placeholder="最寄駅"
          onSearch={onSearch}
          filterOption={(input, option) =>
            (option!.children as unknown as string)
              .toLowerCase()
              .includes(input.toLowerCase())
          }
          allowClear
        >
          {data?.results.map(
            (tranStation: PersonnelBoardTrainStationModel, index: number) => (
              <AutoComplete.Option
                key={`${tranStation.id}-${index}`}
                value={tranStation.name}
              >
                {tranStation.name}
              </AutoComplete.Option>
            )
          )}
        </AutoComplete>
      </Form.Item>
    );
};

export default BoardTrainStationSelectFormItem;
