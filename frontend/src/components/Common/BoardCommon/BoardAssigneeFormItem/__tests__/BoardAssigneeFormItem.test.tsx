import React from "react";
import { Form } from "antd";
import { screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import BoardAssigneeFormItem from "../BoardAssigneeFormItem";
import { fixedUsersAPIResponseContent } from "~/test/mock/userAPIMock";
import { renderWithQueryClient } from "~/test/utils";
import { renderHook } from "@testing-library/react-hooks";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";

const targetUser = fixedUsersAPIResponseContent.filter(
    (user) => user.is_active
)[0];

const Wrapper = () => {
    return (
        <Form
            initialValues={{
                assignees: [targetUser.id],
            }}>
            <BoardAssigneeFormItem />
        </Form>
    );
};

describe("BoardAssigneeFormItem.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    describe("render test", () => {
        test("without initial value", () => {
            renderWithQueryClient(<BoardAssigneeFormItem />);
            const labelElement = screen.getByText(/自社担当者/);
            expect(labelElement).toBeInTheDocument();
            const selectElement = screen.getByTestId("user-ajax-select");
            expect(selectElement).toBeInTheDocument();
        });

        test("with initial value", async () => {
            renderWithQueryClient(<Wrapper />);
            const labelElement = screen.getByText(/自社担当者/);
            expect(labelElement).toBeInTheDocument();
            const selectElement = await screen.findByText(
                new RegExp(targetUser.display_name)
            );
            expect(selectElement).toBeInTheDocument();
        });
    });
});
