import React from "react";
import { Form, FormItemProps, Typography, Row, Tooltip } from "antd";
import { QuestionCircleOutlined } from "@ant-design/icons";
import UserAjaxSelect from "../../UserAjaxSelect/UserAjaxSelect";
import styles from "./BoardAssigneeFormItem.scss";
const { Text } = Typography;

type Props = FormItemProps & {};

const BoardAssigneeFormItem = ({ ...props }: Props) => {
    
    return (
      <Form.Item
        name="assignees"
        {...props}
        label={
          <Row className={styles.labelWrapper}>
            <Text className={styles.labelText}>自社担当者</Text>
            <Tooltip title="無効化しているユーザーは選択表示されません。">
              <QuestionCircleOutlined />
            </Tooltip>
          </Row>
        }
        colon={false}
      >
        <UserAjaxSelect mode="multiple" />
      </Form.Item>
    );
};

export default BoardAssigneeFormItem;
