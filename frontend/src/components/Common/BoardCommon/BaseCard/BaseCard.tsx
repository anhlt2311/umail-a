import React, { CSSProperties, useEffect, useState, useMemo } from "react";
import { Link } from "react-router-dom";
import {
  DownSquareOutlined,
  PaperClipOutlined,
  MailOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import { Card, Col, Image, Row, Tag, Avatar, Tooltip, Button, Dropdown, Menu, Select, Empty } from "antd";
import CustomDatePicker from "~/components/Common/CustomDatePicker/CustomDatePicker";
import Text from "antd/lib/typography/Text";
import Path from "~/components/Routes/Paths";
import UserAvatar from "~/components/Common/UserAvatar/UserAvatar";
import {
  SkillType,
  skillSheetsType
} from "~/models/personnelBoardModel";
import { iconPrimaryColor } from "~/utils/constants";
import ListCommentsPopover from "~/components/Common/ListCommentsPopover/ListCommentsPopover";
import { 
    BoardAssigneeModel,
    BoardPriorityValueModel,
    BoardCommentModel,
    BoardChecklistModel,
    BoardPeriodModel
} from "~/models/boardCommonModel";
import moment, { Moment } from "moment";
import HeaderMenuBoard from "../HeaderMenuBoard/HeaderMenuBoard";
import { UserSpecificModel } from "~/models/userModel";
import { PaginationRequestModel } from "~/models/requestModel";
import { useFetchUsersAPIQuery } from "~/hooks/useUser";
import styles from "./BaseCard.scss";

const { Option } = Select;

const marginIconStyle: CSSProperties = {
  marginRight: 10,
  fontSize: 17
};

const iconDownSquareStyle: CSSProperties = {
  marginRight: 10,
  fontSize: 17,
  color: iconPrimaryColor
};

const marginIconLinkStyle: CSSProperties = {
  marginRight: 10,
  marginLeft: 10,
  fontSize: 17
};

type Info = {
    label: string
    value?: string | number | null
}

type Data = {
  id: string;
  title: string;
  priority: BoardPriorityValueModel;
  affiliation?: string;
  info?: Info[];
  assignees?: BoardAssigneeModel[];
  thumb?: string;
  skills?: SkillType[];
  comments?: BoardCommentModel[];
  checklist?: BoardChecklistModel[];
  period?: BoardPeriodModel;
  skillSheets?: skillSheetsType[];
};

type Deps = UserSpecificModel & PaginationRequestModel & { display_name?: string };

type visibleMenu = {
    visible: boolean;
}

type Props = {
  isSimpleCard: boolean;
  onDetailCard: () => void;
  handleChange: (value: string[]) => void;
  data: Data;
  onStartDateSelect: (value: Moment | null) => void;
  onEndDateSelect: (value: Moment | null) => void;
  onRemoveAssign: (value: string) => void;
}
const BaseCard = ({ isSimpleCard, data, onDetailCard ,handleChange, onStartDateSelect, onEndDateSelect, onRemoveAssign }: Props) => {
  
  const [deps, setDeps] = useState<Deps>({
    is_active: true,
    page: 1,
    pageSize: 1000,
    display_name: undefined,
  });
  const [defaultValue, setDefaultValue] = useState<string[]>([]);
  const [visibleDropDown, setVisibleDropDown] = useState<visibleMenu>({ visible: false });
  const [showMessageExpired, setShowMessageExpired] = useState<Boolean>(false);

  const { data: users } = useFetchUsersAPIQuery({ deps });
  
  useEffect(() => {
    if (!data || !data.assignees || !data.assignees.length) {
      return;
    }
    const arr =  data.assignees.map((item) => item.id);
    setDefaultValue(arr);
  },[data, data.assignees]);

  const onClosePopup = () => {
      setVisibleDropDown({ visible: false });
  };

  const onVisibleChange = () => {
      setVisibleDropDown({ visible: !visibleDropDown.visible });
  };

  const renderMenu = () => (
    <Menu
      className={styles.styleMenu}
    >
      <HeaderMenuBoard
        title="自社担当者を追加"
        onClose={() => onClosePopup()}
      />
      <Col>
        <Row justify="center">
          <Select
            notFoundContent={<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="データがありません"/>}
            mode="multiple"
            size="small"
            placeholder="クリックして選択"
            showArrow
            defaultValue={defaultValue}
            onChange={handleChange}
            style={{ width: '90%' }}
          >
            {
              users?.results.map((item) => (
                <Option key={item.id}>{item.display_name}</Option>
              ))
            }
          </Select>
        </Row>
      </Col>
    </Menu>
  )
  const renderAssignUsers = () => {
    const { assignees }  = data;
    return (
      <Row className={styles.assignUsers}>
        <Avatar.Group maxCount={5}>
          {assignees?.map((assignee, index) => (
            <Tooltip
              title="User 1"
              placement="top"
              key={`assign-user-${index}`}
            >
              <UserAvatar src={assignee?.avatar as string} userId={assignee?.id} removeAssignClick={onRemoveAssign}/>
            </Tooltip>
          ))}
        </Avatar.Group>
        <Dropdown overlay={renderMenu} trigger={['click']} visible={visibleDropDown.visible} onVisibleChange={onVisibleChange}>
            <Button shape="circle" icon={<PlusOutlined />} />
        </Dropdown>
      </Row>
    );
  };

  const classDateEnd = useMemo(() => {
      setShowMessageExpired(false);
      if (!data?.period) {
          return ;
      }
      const { end, isFinished } = data?.period;
      if(isFinished) {
          return styles.dateSuccess;
      }
      else if(moment(end).isSame(moment(), 'd') && moment(end).isAfter(moment(), 'm')) {
          return styles.dateWarning;
      }
      else if(moment(end).isBefore(moment())) {
          setShowMessageExpired(true);
          return styles.dateError;
      }
      else {
        return styles.dateInfo;
      }
  }, [data]);

  function disabledStartDates(current : Moment) {
      const { period }  = data;
      const endDate = moment(period?.end,'YYYY-MM-DD'); 
      return current && current > endDate;
  }

  function disabledEndDates(current : Moment) {
      const { period }  = data;
      const startDate = moment(period?.start,'YYYY-MM-DD'); 
      return current && current < startDate;
  }

  const renderDatePicker = () => {
    const { period }  = data;
    if(!period?.start && !period?.end) {
      return;
    }
    return (
        <Row className={styles.datePickerCard}>
            {period?.start && <Col>
              <CustomDatePicker
                  className={styles.customDatePicker}
                  format="YYYY-MM-DD"
                  showTime={false}
                  allowClear
                  inputReadOnly
                  size={"small"}
                  defaultValue={moment(period?.start)}
                  onChange={onStartDateSelect}
                  disabledDate={disabledStartDates}
              />
            </Col>}
            <Col>
              <Row justify="center">
                  <span className={styles.padding3px}>〜</span>
              </Row>
            </Col>
            {period?.end &&<Col>
                <CustomDatePicker
                    className={classDateEnd}
                    format="YYYY-MM-DD"
                    showTime={false}
                    allowClear
                    inputReadOnly
                    size={"small"}
                    defaultValue={moment(period?.end)}
                    onChange={onEndDateSelect}
                    disabledDate={disabledEndDates}
                />
            </Col>}
      </Row>
    )
  };

  const renderInfo = () => {
    const {info} = data
    if(!info || !info.length) {
      return null
    }

    return (
      <>
        {info.map((item, index) =>
          !!item.value && (
            <Col className={styles.textContent} key={`info-item-${index}`}>
              <Text>{`${item.label}: ${item.value}`}</Text>
            </Col>
          )
        )}
      </>
    );
  };

  const renderSkills = () => {
    const { skills } = data;
    if (!skills || !skills.length) return null;

    return (
      <>
        {skills.map((item, index) => (
          <Tag
            className={styles.tagContent}
            color={item.color}
            key={`skill-${index}`}
          >
            {item.name}
          </Tag>
        ))}
      </>
    );
  };

  const onDownloadFile = (url: string) => {
      if(url) {
         window.open(url, '_blank');
      }
  }

  const renderTitleToolTipDownload = () => {
    const { skillSheets } = data;
    if (!skillSheets || !skillSheets.length) return null;

    return (
        <>
          {skillSheets?.map((item, index) => (
            <div key={`skillSheet-${index}`}>
                <a onClick={() => onDownloadFile(item.file)}>{item.name}</a>
            </div>
          ))}
        </>
    );
  };
 
  const renderComments = (comments : BoardCommentModel[]) => {
      return comments.map((comment) => ({
          content: comment.content,
          created_time: comment.createdTime,
          created_user__name: comment.createdUser,
          modified_time: comment.modifiedTime,
          modified_user__name: comment.modifiedUser,
    }));
  }

  const onCheckListCheckAll = () => {
    const { checklist }  = data;
    if(checklist?.every(i => i.isFinished === true)) {
        return true;
    }
    return false;
  }

  return (
    <Card
      size="small"
      style={{ width: 300, padding: 0 }}
      className={styles.baseCard}
    >
        <Col>
            <Col className={styles.cursor}>
              {data?.thumb && (
                  <Image src={data?.thumb} className={styles.image} preview={false} />
              )}
              <Col className={styles.contentBoard}>
                <Col onClick={onDetailCard}>
                    <Row justify="space-between" align="middle">
                      <Text className={styles.titleBoard}>{data.title}</Text>
                      {data.priority&&<Tag color="processing">{data.priority}</Tag>}
                    </Row>
                    {!!data.affiliation && 
                        <Col className={styles.textContent}>
                            <Text>所属: {data.affiliation}</Text>
                        </Col>
                    }
                    {!isSimpleCard && 
                        <Col>{renderInfo()}</Col>
                    }
                </Col>
                {!isSimpleCard && data?.skillSheets && data?.skillSheets?.length > 0 &&
                    <Col className={styles.skillsBoard}>
                        {renderSkills()}
                    </Col>
                }
                <Col>
                  <Row>
                      <Tooltip
                          title={
                              <span>
                                  <Link target="_blank" rel="noopener noreferrer" to={`${Path.scheduledMailsRegister}` + "?" + "personnelId" + "=" + data?.id}>
                                    配信予約へ
                                  </Link>
                              </span>
                          }>
                          <MailOutlined 
                              style={marginIconStyle}
                          />
                      </Tooltip>
                      <ListCommentsPopover
                          comments={renderComments(data?.comments || [])}
                          columnIndex={0}
                          fontSizeIcon={17}
                      />
                      <Tooltip
                          title={
                            <React.Fragment>
                                {renderTitleToolTipDownload()}
                            </React.Fragment>
                          }
                      >
                      {data?.skillSheets && data?.skillSheets?.length !== 0 ?
                          <PaperClipOutlined 
                              style={marginIconLinkStyle}
                          />
                          : <></>
                      }
                      </Tooltip>
                      {data?.checklist && data?.checklist?.length !== 0 ? 
                          <DownSquareOutlined 
                              style={onCheckListCheckAll() ? iconDownSquareStyle : marginIconStyle}
                          /> 
                          : <></>
                      }
                  </Row>
                </Col>
                <Col>
                    {showMessageExpired && <Text className={styles.messageExpired}>期限切れ</Text>}
                </Col>
                <Col> 
                   {renderDatePicker()}
                </Col>
                <Col>
                  {renderAssignUsers()}
                </Col>
                </Col>
            </Col>
        </Col>
    </Card>
  );
};

export default BaseCard;
