import React from "react";
import { Skeleton, Space, Table, TableProps, Tag, Tooltip } from "antd";
import styles from "./BoardScheduledEmailHistoryTable.scss";
import { BoardScheduledEmailHistoryModel } from "~/models/boardCommonModel";
import { ColumnsType } from "antd/lib/table";
import ScheduledEmailOpenerListModal from "~/components/Tables/ScheduledMailsTable/ScheduledEmailOpenerListModal/ScheduledEmailOpenerListModal";
import { MailTwoTone } from "@ant-design/icons";
import {
    iconPrimaryColor,
    SCHEDULED_EMAIL_TEXT_FORMAT,
} from "~/utils/constants";
import { ScheduledEmailTextFormatValueModel } from "~/models/scheduledEmailModel";
import { useHistory } from "react-router-dom";
import Paths from "~/components/Routes/Paths";

type Props = TableProps<BoardScheduledEmailHistoryModel> & {
    data: BoardScheduledEmailHistoryModel[];
    loading?: boolean;
};

const BoardScheduledEmailHistoryTable = ({ data, loading }: Props) => {
    const router = useHistory();

    const renderTextFormat = (format: ScheduledEmailTextFormatValueModel) => {
        const displayTag = SCHEDULED_EMAIL_TEXT_FORMAT.find(
            (textFormat) => textFormat.value === format
        );
        if (!displayTag) {
            return "";
        }
        return <Tag color={displayTag.color}>{displayTag.title}</Tag>;
    };

    const onCellClick = (record: BoardScheduledEmailHistoryModel) => {
        const url = Paths.scheduledMails + "/" + record.id;
        router.push(url);
    };

    const columns: ColumnsType<BoardScheduledEmailHistoryModel> = [
        {
            key: "sendCompleteDate",
            dataIndex: "sendCompleteDate",
            title: "配信完了日時",
            width: 160,
            ellipsis: true,
        },
        {
            title: "フォーマット",
            dataIndex: "format",
            key: "format",
            render: (text_format, record) => {
                if (record.format) {
                    return renderTextFormat(record.format);
                } else {
                    return "";
                }
            },
            onCell: (record) => ({
                onClick: () => onCellClick(record),
            }),
            width: 140,
            ellipsis: true,
        },
        {
            title: "配信数",
            dataIndex: "sendCount",
            key: "sendCount",
            width: 100,
            ellipsis: true,
            onCell: (record) => ({
                onClick: () => onCellClick(record),
            }),
            sorter: true,
        },
        {
            title: "開封数",
            dataIndex: "openCount",
            key: "openCount",
            width: 100,
            ellipsis: true,
            sorter: true,
            render: (text, record, index) => {
                const renderTooltipTitle = () => {
                    return (
                        <>
                            <ScheduledEmailOpenerListModal
                                scheduledEmailId={record?.id ?? ""}
                            />
                        </>
                    );
                };

                if (
                    record.format === "text" ||
                    record.status == "draft" ||
                    record.status == "queued"
                ) {
                    return "";
                }
                if (record.openCount > 0) {
                    return (
                        <span>
                            {text}{" "}
                            {/** NOTE(shintaro-suzuki): スタイリングのためスペースを入れている */}
                            <Tooltip title={renderTooltipTitle}>
                                <MailTwoTone twoToneColor={iconPrimaryColor} />
                            </Tooltip>
                        </span>
                    );
                } else {
                    return record.openCount;
                }
            },
        },
        {
            title: "開封率",
            dataIndex: "open_ratio",
            key: "open_ratio",
            sorter: true,
            render: (_, record) => !!record.openRatio && `${record.openRatio}%`,
            width: 100,
        },
    ];

    if (loading) {
        return (
            <Space>
                <Skeleton.Input
                    style={{ height: 200, width: 1200 }}
                    active
                    size="large"
                />
            </Space>
        );
    }

    return (
        <Table
            columns={columns}
            dataSource={data}
            pagination={false}
            rowKey={(record) => record.id}
        />
    );
};

export default BoardScheduledEmailHistoryTable;
