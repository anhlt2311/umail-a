import React from "react";
import { BackTop, BackTopProps } from "antd";
import styles from "./CustomBackToTop.scss";

type Props = BackTopProps & {};

const CustomBackToTop = ({ ...props }: Props) => {
    return (
        <BackTop className={styles.toTop} {...props}>
            <div className={styles.content}>
                <span>Top</span>
            </div>
        </BackTop>
    );
};

export default CustomBackToTop;
