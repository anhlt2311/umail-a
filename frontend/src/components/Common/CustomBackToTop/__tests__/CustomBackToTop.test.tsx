import React from "react";
import { render, screen } from "~/test/utils";
import CustomBackToTop from "../CustomBackToTop";

describe("CustomBackToTop.tsx", () => {
    test("render test", async () => {
        render(<CustomBackToTop visibilityHeight={-1} visible />);
        const textElement = screen.getByText(/Top/);
        expect(textElement).toBeInTheDocument();
    });
});
