import React from "react";
import { renderWithQueryClient, screen, userEvent } from "~/test/utils";
import NewTagModal from "../NewTagModal";
import { ErrorMessages, TAG_COLORS } from "~/utils/constants";

const mockOnModalClose = jest.fn();
const mockOnFinish = jest.fn();

describe("NewTagModal.tsx", () => {
    test("render test", async () => {
        const { container } = renderWithQueryClient(
            <NewTagModal
                isVisible
                onFinish={mockOnFinish}
                onModalClose={mockOnModalClose}
            />
        );
        const titleElement = screen.getByText(/^タグ新規作成$/);
        expect(titleElement).toBeInTheDocument();
        const tagNameInputElement = screen.getByPlaceholderText(
            /^タグ名$/
        ) as HTMLInputElement;
        expect(tagNameInputElement).toBeInTheDocument();
        expect(tagNameInputElement).not.toHaveValue();
        const tagColorSelectElement = screen.getByTestId("tag-color-select");
        expect(tagColorSelectElement).toBeInTheDocument();
        // NOTE(joshua-hashimoto): Select要素のdisabledは取得できないので、クリックしても何も表示されないことでSelect要素をクリックできないことを保証する
        await userEvent.click(tagColorSelectElement);
        for (const tagColor of TAG_COLORS) {
            expect(
                container.getElementsByClassName(`ant-tag-${tagColor.value}`)
                    .length
            ).toBe(0);
        }
        const cancelButtonElement = screen.getByRole("button", {
            name: /^キャンセル$/,
        });
        expect(cancelButtonElement).toBeInTheDocument();
        expect(cancelButtonElement).not.toBeDisabled();
        const okButtonElement = screen.getByRole("button", { name: /^OK$/ });
        expect(okButtonElement).toBeInTheDocument();
        expect(okButtonElement).toBeDisabled();
    });

    describe("test validation", () => {
        const maxLengthRegex = new RegExp(
            ErrorMessages.validation.length.max50
        );
        const requiredRegex = new RegExp(ErrorMessages.form.required);

        test("default validation test", async () => {
            renderWithQueryClient(
                <NewTagModal
                    isVisible
                    onFinish={mockOnFinish}
                    onModalClose={mockOnModalClose}
                />
            );
            const tagNameInputElement = screen.getByPlaceholderText(
                /^タグ名$/
            ) as HTMLInputElement;
            const okButtonElement = screen.getByRole("button", {
                name: /^OK$/,
            });
            expect(okButtonElement).toBeDisabled();
            const maxLengthErrorMessageElement =
                screen.queryByText(maxLengthRegex);
            expect(maxLengthErrorMessageElement).not.toBeInTheDocument();
            const requiredErrorMessageElement =
                screen.queryByText(requiredRegex);
            expect(requiredErrorMessageElement).not.toBeInTheDocument();
        });

        test("normal input validation test", async () => {
            renderWithQueryClient(
                <NewTagModal
                    isVisible
                    onFinish={mockOnFinish}
                    onModalClose={mockOnModalClose}
                />
            );
            const tagNameInputElement = screen.getByPlaceholderText(
                /^タグ名$/
            ) as HTMLInputElement;
            const okButtonElement = screen.getByRole("button", {
                name: /^OK$/,
            });
            await userEvent.type(tagNameInputElement, "example tag name");
            expect(okButtonElement).not.toBeDisabled();
            expect(screen.queryByText(maxLengthRegex)).not.toBeInTheDocument();
            expect(screen.queryByText(requiredRegex)).not.toBeInTheDocument();
        });

        test("empty input validation test", async () => {
            renderWithQueryClient(
                <NewTagModal
                    isVisible
                    onFinish={mockOnFinish}
                    onModalClose={mockOnModalClose}
                />
            );
            const tagNameInputElement = screen.getByPlaceholderText(
                /^タグ名$/
            ) as HTMLInputElement;
            const okButtonElement = screen.getByRole("button", {
                name: /^OK$/,
            });
            await userEvent.type(tagNameInputElement, "random text");
            await userEvent.clear(tagNameInputElement);
            expect(okButtonElement).toBeDisabled();
            expect(screen.queryByText(maxLengthRegex)).not.toBeInTheDocument();
            expect(screen.queryByText(requiredRegex)).toBeInTheDocument();
        });

        test("over max length validation test", async () => {
            renderWithQueryClient(
                <NewTagModal
                    isVisible
                    onFinish={mockOnFinish}
                    onModalClose={mockOnModalClose}
                />
            );
            const tagNameInputElement = screen.getByPlaceholderText(
                /^タグ名$/
            ) as HTMLInputElement;
            const okButtonElement = screen.getByRole("button", {
                name: /^OK$/,
            });
            await userEvent.type(tagNameInputElement, "a".repeat(51));
            expect(okButtonElement).toBeDisabled();
            expect(screen.queryByText(maxLengthRegex)).toBeInTheDocument();
            expect(screen.queryByText(requiredRegex)).not.toBeInTheDocument();
        });

        test("exact max length validation test", async () => {
            renderWithQueryClient(
                <NewTagModal
                    isVisible
                    onFinish={mockOnFinish}
                    onModalClose={mockOnModalClose}
                />
            );
            const tagNameInputElement = screen.getByPlaceholderText(
                /^タグ名$/
            ) as HTMLInputElement;
            const okButtonElement = screen.getByRole("button", {
                name: /^OK$/,
            });
            await userEvent.type(tagNameInputElement, "a".repeat(50));
            expect(okButtonElement).not.toBeDisabled();
            expect(screen.queryByText(maxLengthRegex)).not.toBeInTheDocument();
            expect(screen.queryByText(requiredRegex)).not.toBeInTheDocument();
        });
    });
});
