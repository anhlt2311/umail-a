import React, { useMemo } from "react";
import { Button, Col, Form, Input, Row, Select, Tag, Switch, Tooltip } from "antd";
import styles from "./NewTagModal.scss";
import GenericModal from "~/components/Modals/GenericModal/GenericModal";
import { NewTagFormModel, TagModel } from "~/models/tagModel";
import {
    convertTagResponseModelToTagModel,
    useCreateTagFromContactAPIMutation,
} from "~/hooks/useTag";
import { ErrorMessages, TAG_COLORS, iconCustomColor, Links } from "~/utils/constants";
import { CloseOutlined, CheckOutlined, QuestionCircleFilled } from '@ant-design/icons';
import { usePlans } from "~/hooks/usePlan";
import { TYPE_SELECT } from "~/utils/types";

type Props = {
    isVisible: boolean;
    onFinish: (newTag: TagModel) => void;
    onModalClose: () => void;
    type?: TYPE_SELECT;
};

const NewTagModal = ({ isVisible, onFinish, onModalClose, type }: Props) => {
    const requiredFields = ["newTag"];
    const [form] = Form.useForm<NewTagFormModel>();
    const { mutate: createTag } = useCreateTagFromContactAPIMutation();
    const { isStandardPlan } = usePlans();

    const isSkillType = useMemo(() => type === TYPE_SELECT.SKILL, [type]);

    const onClose = () => {
        form.resetFields();
        onModalClose();
    };

    const onSubmit = ({ newTag, newColor, isSkill }: NewTagFormModel) => {
        if (!newTag && !newColor) {
            return;
        }
        createTag(
            { value: newTag, color: newColor, is_skill: isSkill },
            {
                onSuccess: (response) => {
                    const result = response.data;
                    onFinish(convertTagResponseModelToTagModel(result));
                },
            }
        );
        onClose();
    };

    return (
        <GenericModal
            title="タグ新規作成"
            visible={isVisible}
            destroyOnClose={true}>
            <Col span={24}>
                <Form
                    form={form}
                    onFinish={onSubmit}
                    name="new_tag_create_modal"
                    initialValues={{ newColor: "default", isSkill: isSkillType }}>
                    <Form.Item noStyle>
                        <Col span={24}>
                            <Row>
                                <Col flex="auto">
                                    <Form.Item
                                        className={styles.field}
                                        style={{ textAlign: "left" }}
                                        name="newTag"
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    ErrorMessages.form.required,
                                            },
                                            {
                                                max: 50,
                                                message:
                                                    ErrorMessages.validation
                                                        .length.max50,
                                            },
                                        ]}>
                                        <Input placeholder="タグ名" autoFocus />
                                    </Form.Item>
                                </Col>
                                <Col style={{margin: '8px 0'}}>
                                    <Form.Item shouldUpdate>
                                        {() => (
                                            <Form.Item name="newColor" noStyle>
                                                <Select
                                                    disabled={
                                                        !form.getFieldValue(
                                                            "newTag"
                                                        )
                                                    }
                                                    data-testid="tag-color-select">
                                                    {TAG_COLORS.map((tag) => (
                                                        <Select.Option
                                                            key={tag.value}
                                                            value={tag.value}>
                                                            <Tag
                                                                color={
                                                                    tag.value
                                                                }
                                                                style={{
                                                                    marginTop:
                                                                        "3px",
                                                                    marginBottom:
                                                                        "3px",
                                                                    marginInlineEnd:
                                                                        "4.8px",
                                                                    paddingInlineStart:
                                                                        "8px",
                                                                    paddingInlineEnd:
                                                                        "4px",
                                                                    whiteSpace:
                                                                        "normal",
                                                                }}>
                                                                {form.getFieldValue(
                                                                    "newTag"
                                                                )}
                                                            </Tag>
                                                        </Select.Option>
                                                    ))}
                                                </Select>
                                            </Form.Item>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Col>
                    </Form.Item>
                    {isStandardPlan ? (
                        <Form.Item
                            colon={false}
                            label={
                                <span>
                                    スキル兼用&nbsp;
                                    <Tooltip
                                        title={
                                            <span>
                                                有効にすることでスキルタグとして登録されます。タグの一覧での表示に加え、
                                                <a  href='#'
                                                    target="_blank"
                                                    rel="noopener noreferrer"
                                                >
                                                    要員ボード</a>
                                                要員情報 スキルの一覧にも表示されます。
                                                <br />
                                                <a
                                                    href={Links.helps.tags.details}
                                                    target="_blank"
                                                    rel="noopener noreferrer">
                                                    詳細
                                                </a>
                                            </span>
                                        }>
                                        <QuestionCircleFilled style={{ color: iconCustomColor }}/>
                                    </Tooltip>
                                </span>
                            }>
                            <Col span={24}>
                                <Row>
                                    <Col>
                                         <Form.Item name="isSkill">
                                            <Switch
                                                checkedChildren={
                                                    <CheckOutlined />
                                                }
                                                unCheckedChildren={
                                                    <CloseOutlined />
                                                }
                                                defaultChecked={isSkillType}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                        </Form.Item>
                        ) : (
                        <></>
                    )}
                    <Form.Item noStyle>
                        <Col span={24}>
                            <Row justify="end" gutter={6}>
                                <Col>
                                    <Button onClick={onClose}>
                                        キャンセル
                                    </Button>
                                </Col>
                                <Col>
                                    <Form.Item shouldUpdate>
                                        {() => (
                                            <Button
                                                type="primary"
                                                htmlType="submit"
                                                disabled={
                                                    !form.isFieldsTouched(
                                                        requiredFields,
                                                        true
                                                    ) ||
                                                    !!form
                                                        .getFieldsError()
                                                        .filter(
                                                            ({ errors }) =>
                                                                errors.length
                                                        ).length
                                                }>
                                                OK
                                            </Button>
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Col>
                    </Form.Item>
                </Form>
            </Col>
        </GenericModal>
    );
};

export default NewTagModal;
