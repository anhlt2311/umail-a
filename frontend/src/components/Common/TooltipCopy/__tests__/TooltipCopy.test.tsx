import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import TooltipCopy from "../TooltipCopy";

describe("TooltipCopy.tsx", () => {
    test("render test", async () => {
        render(<TooltipCopy copyContent="copy me" placement="top" />);
        const iconElement = screen.getByTestId("copy-icon");
        expect(iconElement).toBeInTheDocument();
        await userEvent.click(iconElement);
        const tooltipMessageElement = await screen.findByText(
            /クリップボードにコピ/
        );
        expect(tooltipMessageElement).toBeInTheDocument();
    });
});
