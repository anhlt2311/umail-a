import React, { useEffect, useState } from "react";
import { Tooltip } from "antd";
import { CopyOutlined } from "@ant-design/icons";
import styles from "./TooltipCopy.scss";
import { TooltipPlacement } from "antd/lib/tooltip";

type Props = {
    copyContent: string;
    placement: TooltipPlacement;
};

const TooltipCopy = ({ copyContent = "", placement = "top" }: Props) => {
    if (!copyContent) {
        return null;
    }

    const handelCopy = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
        e.preventDefault();
        e.stopPropagation();
        navigator.clipboard.writeText(copyContent);
    };

    return (
        <Tooltip
            arrowPointAtCenter
            placement={placement}
            title={"クリップボードにコピー"}
            className={styles.tooltip}>
            <CopyOutlined data-testid="copy-icon" onClick={handelCopy} />
        </Tooltip>
    );
};

export default TooltipCopy;
