import React from "react";
import { Upload, UploadProps, Slider, Button } from "antd";
import ImgCrop from "antd-img-crop";
import { UploadOutlined  } from "@ant-design/icons";
import "antd/lib/slider/style";

type Props = UploadProps & {};

const ImageCropper = ({ ...props }: Props) => {
    return (
        <ImgCrop
            modalTitle="画像をトリミング"
            modalCancel="キャンセル"
            aspect={1}
            shape="round"
            rotate>
            <Upload
                showUploadList={false}
                name="avatar"
                {...props}
                data-testid="image-cropper">
                <Button icon={<UploadOutlined />} style={{ display: 'flex', justifyContent: 'center'}}>画像をアップロード</Button>
            </Upload>
        </ImgCrop>
    );
};

export default ImageCropper;
