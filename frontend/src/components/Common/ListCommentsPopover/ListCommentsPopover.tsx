import { MessageTwoTone } from "@ant-design/icons";
import moment from "moment";
import React from "react";
import { TableCommentModel } from "~/models/commentModel";
import { iconPrimaryColor } from "~/utils/constants";
import CustomToolTipComment from "../CustomTooltipComment/CustomTooltipComment";
import styles from "./ListCommentsPopover.scss";

type Props = {
    comments?: TableCommentModel[];
    columnIndex: number;
    fontSizeIcon?: number;
};

const ListCommentsPopover = ({ comments, columnIndex, fontSizeIcon }: Props) => {
    if (!comments || !comments.length) {
        return null;
    }

    const convertedComments = comments.map(
        (comment: any, comment_idx: number) => {
            const content = comment.content;
            const lineBreakText = content
                .split("\n")
                .map((line: string, index: number) => (
                    <React.Fragment key={index}>
                        {line}
                        <br />
                    </React.Fragment>
                ));
            const modifiedTime = moment(comment.modified_time).format(
                "YYYY/MM/DD HH:mm"
            );
            const createdUsername = comment.created_user__name;
            return (
                <div
                    key={columnIndex + "_" + comment_idx}
                    className={
                        comment_idx < comments.length - 1
                            ? styles.commentContainer
                            : ""
                    }>
                    <div className={styles.commentContent}>
                        [{modifiedTime} {createdUsername}]
                        <br />
                        {lineBreakText}
                    </div>
                </div>
            );
        }
    );

    const title = (
        <React.Fragment>
            <div className={styles.headerToolTip}>固定されたコメント</div>
            {convertedComments}
        </React.Fragment>
    );

    return (
        <CustomToolTipComment title={title}>
            <MessageTwoTone
                data-testid="comment-icon"
                twoToneColor={iconPrimaryColor}
                style={{fontSize: fontSizeIcon}}
            />
        </CustomToolTipComment>
    );
};

export default ListCommentsPopover;
