import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import { TableCommentModel } from "~/models/commentModel";
import ListCommentsPopover from "../ListCommentsPopover";

const mockData: TableCommentModel[] = [
    {
        content: "comment 1",
        created_time: "2022/03/23 05:31:22",
        created_user__name: "master@example.com",
        modified_time: "2022-03-23T14:31:22.453227+09:00",
        modified_user__name: "master@example.com",
    },
    {
        content: "comment 2",
        created_time: "2022/03/23 05:31:22",
        created_user__name: "master@example.com",
        modified_time: "2022-03-23T14:31:22.453227+09:00",
        modified_user__name: "master@example.com",
    },
];

describe("ListCommentsPopover.tsx", () => {
    test("render test", async () => {
        render(<ListCommentsPopover comments={mockData} columnIndex={1} />);
        const iconElement = screen.getByTestId("comment-icon");
        expect(iconElement).toBeInTheDocument();
        await userEvent.hover(iconElement);
        const tooltipTitleElement = await screen.findByText(
            /固定されたコメント/
        );
        expect(tooltipTitleElement).toBeInTheDocument();
        const tooltipComment1Element = await screen.findByText(/comment 1/);
        expect(tooltipComment1Element).toBeInTheDocument();
        const tooltipComment2Element = await screen.findByText(/comment 2/);
        expect(tooltipComment2Element).toBeInTheDocument();
    });

    test("will not be rendered without data", () => {
        render(<ListCommentsPopover columnIndex={1} />);
        const iconElement = screen.queryByTestId("comment-icon");
        expect(iconElement).not.toBeInTheDocument();
    });
});
