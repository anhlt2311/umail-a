import React from "react";
import { Button, Tooltip } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
import {
    ErrorMessages,
    TABLE_OPERATION_SELECTABLE_LIMIT,
    TooltipMessages,
} from "~/utils/constants";
import styles from "./TableDeleteButton.scss";

type Props<T> = {
    selectedRows: T[];
    isLoading?: boolean;
    isAuthorized: boolean;
    onClick: (selectedRows: T[]) => void;
};

const TableDeleteButton = <T extends object>({
    selectedRows,
    isLoading = false,
    isAuthorized,
    onClick,
}: Props<T>) => {
    const isOverLimit = TABLE_OPERATION_SELECTABLE_LIMIT < selectedRows.length;
    const isNotSelected = 0 === selectedRows.length;
    const button = (
        <Button
            className={styles.tableControlButton}
            type="primary"
            danger
            icon={<DeleteOutlined />}
            size="small"
            onClick={() => onClick(selectedRows)}
            loading={isLoading}
            disabled={isOverLimit || isNotSelected || !isAuthorized}
        />
    );

    if (isOverLimit) {
        return (
            <Tooltip title={TooltipMessages.table.overLimit}>{button}</Tooltip>
        );
    }

    if (isNotSelected) {
        return (
            <Tooltip title={TooltipMessages.table.notSelected}>
                {button}
            </Tooltip>
        );
    }

    return (
        <Tooltip
            title={
                isAuthorized
                    ? TooltipMessages.table.deleteSelected
                    : ErrorMessages.isNotAuthorized
            }>
            {button}
        </Tooltip>
    );
};

export default TableDeleteButton;
