import React, { ReactNode } from "react";
import { Divider } from "antd";

type Props = {
    currentCount: number;
    limitCount: number;
    unit?: string;
    prefix?: ReactNode;
    suffix?: ReactNode;
};

const LimitDivider = ({
    currentCount,
    limitCount,
    unit = "件",
    prefix,
    suffix,
}: Props) => {
    return (
        <Divider style={{ alignSelf: "center" }}>
            {prefix}
            &nbsp;
            {`${currentCount}${unit}`}&nbsp;
            <span style={{ fontSize: 12, color: "#8D8D8D" }}>
                ／{limitCount}
                {unit}
            </span>
            &nbsp;
            {suffix}
        </Divider>
    );
};

export default LimitDivider;
