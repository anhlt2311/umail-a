import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import ProfileThumbnailFormItem from "../ProfileThumbnailFormItem";

const mockOnRemoveThumbnail = jest.fn();

describe("ProfileThumbnailFormItem.tsx", () => {
    test("render test", () => {
        render(
            <ProfileThumbnailFormItem
                onRemoveThumbnail={mockOnRemoveThumbnail}
            />
        );
        const formItemLabelElement = screen.getByText(/^プロフィール画像$/);
        expect(formItemLabelElement).toBeInTheDocument();
        const deleteButtonElement = screen.queryByText(/^画像を削除する$/);
        expect(deleteButtonElement).not.toBeInTheDocument();
        // NOTE(joshua-hashimoto): 簡易存在確認
        const uploaderTitleElement = screen.getByText(/^画像をアップロード$/);
        expect(uploaderTitleElement).toBeInTheDocument();
    });

    test("test can override form item props", () => {
        render(
            <ProfileThumbnailFormItem
                label="OVERRIDE"
                onRemoveThumbnail={mockOnRemoveThumbnail}
            />
        );
        const formItemLabelElement = screen.getByText(/^OVERRIDE$/);
        expect(formItemLabelElement).toBeInTheDocument();
        const deleteButtonElement = screen.queryByText(/^画像を削除する$/);
        expect(deleteButtonElement).not.toBeInTheDocument();
    });

    test("test component with image data", () => {
        render(
            <ProfileThumbnailFormItem
                thumbnailUrl="https://picsum.photos/200/300"
                onRemoveThumbnail={mockOnRemoveThumbnail}
            />
        );
        const deleteButtonElement = screen.getByText(/^画像を削除する$/);
        expect(deleteButtonElement).toBeInTheDocument();
        expect(deleteButtonElement).not.toBeDisabled();
        // NOTE(joshua-hashimoto): 簡易存在確認
        const uploaderTitleElement = screen.getByText(/^画像をアップロード$/);
        expect(uploaderTitleElement).toBeInTheDocument();
    });

    test("test delete button can open delete modal", async () => {
        render(
            <ProfileThumbnailFormItem
                thumbnailUrl="https://picsum.photos/200/300"
                onRemoveThumbnail={mockOnRemoveThumbnail}
            />
        );
        const deleteButtonElement = screen.getByText(/^画像を削除する$/);
        await userEvent.click(deleteButtonElement);
        const deleteModalTitleElement = await screen.findByText(
            /^現在のプロフィール画像を削除しますか？$/
        );
        expect(deleteModalTitleElement).toBeInTheDocument();
    });

    // TODO(joshua-hashimoto): モーダルを閉じるテストを追加する
});
