import React from "react";
import {
    mockHistoryPush,
    renderWithAllProviders,
    screen,
    waitFor,
    act,
    fireEvent,
} from "~/test/utils";
import LoginForm from "../LoginForm";
import { configureStore, PayloadAction } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import Paths from "~/components/Routes/Paths";
import { mockServer } from "~/test/setupTests";
import { mockLoginFailsWithInvalidCredentials } from "~/test/mock/authAPIMock";
import { ErrorMessages } from "~/utils/constants";

describe("LoginForm", () => {
    test("render test", async () => {
        const { container } = renderWithAllProviders(<LoginForm />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                    },
                },
            }),
        });
        const emailInput = screen.getByPlaceholderText(/メールアドレス/);
        expect(emailInput).toBeInTheDocument();
        const passwordInput = screen.getByPlaceholderText(/パスワード/);
        expect(passwordInput).toBeInTheDocument();
        const loginButton = screen.getByText(/ログイン/);
        expect(loginButton).toBeInTheDocument();
        const passwordRestLink = screen.getByRole("link", {
            name: /パスワードをお忘れの方/,
        });
        expect(passwordRestLink).toBeInTheDocument();
        const newTenantLink = screen.getByRole("link", {
            name: /新しく企業アカウントを作成する/,
        });
        expect(newTenantLink).toBeInTheDocument();
        await waitFor(() =>
            expect(container.querySelector("#recaptcha")).toBeNull()
        );
    });

    test("render test with ReCAPTCHA", async () => {
        const { container } = renderWithAllProviders(<LoginForm />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        threeLogin: 3,
                    },
                },
            }),
        });
        const emailInput = screen.getByPlaceholderText(/メールアドレス/);
        expect(emailInput).toBeInTheDocument();
        const passwordInput = screen.getByPlaceholderText(/パスワード/);
        expect(passwordInput).toBeInTheDocument();
        const loginButton = screen.getByText(/ログイン/);
        expect(loginButton).toBeInTheDocument();
        const passwordRestLink = screen.getByRole("link", {
            name: /パスワードをお忘れの方/,
        });
        expect(passwordRestLink).toBeInTheDocument();
        const newTenantLink = screen.getByRole("link", {
            name: /新しく企業アカウントを作成する/,
        });
        expect(newTenantLink).toBeInTheDocument();
        await waitFor(() =>
            expect(container.querySelector("#recaptcha")).not.toBeNull()
        );
    });

    test("can submit login", async () => {
        act(() => {
            renderWithAllProviders(<LoginForm />, {
                store: configureStore<any, PayloadAction<any>>({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                        },
                    },
                }),
            });
        });
        const emailInput = screen.getByPlaceholderText(
            /メールアドレス/
        ) as HTMLInputElement;
        await act(async () => {
            fireEvent.change(emailInput, {
                target: { value: "example@example.com" },
            });
        });
        const passwordInput = screen.getByPlaceholderText(
            /パスワード/
        ) as HTMLInputElement;
        await act(async () => {
            fireEvent.change(passwordInput, {
                target: { value: "Abcd12345%" },
            });
        });
        const loginButton = screen.getByText(/ログイン/);
        await act(async () => {
            fireEvent.click(loginButton);
        });
        await waitFor(() =>
            expect(mockHistoryPush).toHaveBeenCalledWith(Paths.dashboard)
        );
        await waitFor(() => {
            const errorMessage = screen.queryByText(
                ErrorMessages.auth.invalidCredentials
            );
            expect(errorMessage).not.toBeInTheDocument();
        });
    });

    test("login fails", async () => {
        mockServer.use(mockLoginFailsWithInvalidCredentials);
        act(() => {
            renderWithAllProviders(<LoginForm />, {
                store: configureStore<any, PayloadAction<any>>({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                        },
                    },
                }),
            });
        });
        const emailInput = screen.getByPlaceholderText(
            /メールアドレス/
        ) as HTMLInputElement;
        await act(async () => {
            fireEvent.change(emailInput, {
                target: { value: "example@example.com" },
            });
        });
        const passwordInput = screen.getByPlaceholderText(
            /パスワード/
        ) as HTMLInputElement;
        await act(async () => {
            fireEvent.change(passwordInput, {
                target: { value: "Abcd12345%" },
            });
        });
        const loginButton = screen.getByText(/ログイン/);
        await act(async () => {
            fireEvent.click(loginButton);
        });
        await waitFor(() => {
            const errorMessage = screen.getByText(
                ErrorMessages.auth.invalidCredentials
            );
            expect(errorMessage).toBeInTheDocument();
        });
    });
});
