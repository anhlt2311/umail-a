import React from "react";
import { ChartOptions } from "chart.js";
import { ChartData } from "react-chartjs-2";
import { OrganizationStatsGraphModel } from "~/models/dashboardModel";
import { GraphDataModel } from "~/models/graphModel";
import { CommonGraphDataSet } from "~/utils/constants";
import GraphCard from "../GraphCard/GraphCard";
import styles from "./OrganizationStatsGraph.scss";

type Props = {
    data: GraphDataModel<OrganizationStatsGraphModel>;
};

const OrganizationStatsGraph = ({ data }: Props) => {
    const options: ChartOptions = {
        scales: {
            yAxes: [
                {
                    id: "organization_stats",
                    type: "linear",
                    position: "left",
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
    };

    const getChartData = (
        data: GraphDataModel<OrganizationStatsGraphModel>
    ): ChartData<any> => {
        return {
            labels: data.labels,
            datasets: [
                {
                    ...CommonGraphDataSet,
                    label: "見込み客",
                    backgroundColor: "rgba(30,56,195,0.4)",
                    borderColor: "rgba(30,56,195,1)",
                    pointBorderColor: "rgba(30,56,195,1)",
                    pointHoverBackgroundColor: "rgba(30,56,195,1)",
                    data: data.values.prospective,
                    yAxisID: "organization_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "アプローチ済",
                    backgroundColor: "rgba(212,108,11,0.4)",
                    borderColor: "rgba(212,108,11,1)",
                    pointBorderColor: "rgba(212,108,11,1)",
                    pointHoverBackgroundColor: "rgba(212,108,11,1)",
                    data: data.values.approached,
                    yAxisID: "organization_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "情報交換済",
                    backgroundColor: "rgba(125,190,99,0.4)",
                    borderColor: "rgba(125,190,99,1)",
                    pointBorderColor: "rgba(125,190,99,1)",
                    pointHoverBackgroundColor: "rgba(125,190,99,1)",
                    data: data.values.exchanged,
                    yAxisID: "organization_stats",
                },
                {
                    ...CommonGraphDataSet,
                    label: "契約実績有",
                    backgroundColor: "rgba(212,56,12,0.4)",
                    borderColor: "rgba(212,56,12,1)",
                    pointBorderColor: "rgba(212,56,12,1)",
                    pointHoverBackgroundColor: "rgba(212,56,12,1)",
                    data: data.values.client,
                    yAxisID: "organization_stats",
                },
            ],
        };
    };

    return (
        <GraphCard
            title="取引先ステータス（累計）"
            subTitle="取引先一覧"
            data={getChartData(data)}
            options={options}
        />
    );
};

export default OrganizationStatsGraph;
