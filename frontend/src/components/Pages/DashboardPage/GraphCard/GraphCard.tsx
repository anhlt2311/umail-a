import React, { useState } from "react";
import { Button, Card, Col, Row } from "antd";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import { BarChartOutlined, LineChartOutlined } from "@ant-design/icons";
import { Bar, ChartComponentProps, Line } from "react-chartjs-2";

type GraphType = "line" | "bar";

type Props = ChartComponentProps & {
    title: string;
    subTitle?: string;
};

const GraphCard = ({ title, subTitle, ...props }: Props) => {
    const [graphType, setGraphType] = useState<GraphType>("line");

    const renderExtraButtons = () => {
        return [
            <Button
                type={graphType === "line" ? "default" : "primary"}
                icon={<LineChartOutlined />}
                onClick={() => setGraphType("line")}
                key="extraButtonLineChart"
            />,
            <Button
                type={graphType === "bar" ? "default" : "primary"}
                icon={<BarChartOutlined />}
                onClick={() => setGraphType("bar")}
                key="extraButtonBarChart"
            />,
        ];
    };

    return (
        <Row justify="center" style={{ marginBottom: 20 }}>
            <Col span={24}>
                <Card>
                    <CustomPageHeader
                        title={title}
                        subTitle={subTitle}
                        extra={renderExtraButtons()}
                    />
                    <Col span={24}>
                        <Row justify="center" align="middle">
                            <Col flex="auto">
                                {graphType === "line" ? (
                                    <Line {...props} />
                                ) : null}
                                {graphType === "bar" ? (
                                    <Bar {...props} />
                                ) : null}
                            </Col>
                        </Row>
                    </Col>
                </Card>
            </Col>
        </Row>
    );
};

export default GraphCard;
