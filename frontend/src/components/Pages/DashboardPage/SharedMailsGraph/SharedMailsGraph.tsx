import React from "react";
import { ChartOptions } from "chart.js";
import { ChartData } from "react-chartjs-2";
import { ScheduledEmailStatsGraphModel } from "~/models/dashboardModel";
import { GraphDataModel } from "~/models/graphModel";
import { CommonGraphDataSet } from "~/utils/constants";
import GraphHorizontalBar from "../GraphCard/GraphHorizontalBar";

type Props = {
  data: GraphDataModel<ScheduledEmailStatsGraphModel>;
};

const SharedMailsGraph = ({ data }: Props) => {
  const options: ChartOptions = {
    responsive: true,
    legend: {
      display: true,
    },
  };

  const getChartData = (
    data: GraphDataModel<ScheduledEmailStatsGraphModel>
  ): ChartData<any> => {
    return {
      labels: ["Jan", "Feb", "Mar", "Apr", "May"],
      datasets: [
        {
          indexAxis: 'y',
          label: "red",
          borderWidth: 1,
          borderColor: "red",
          data: [120, 23, 24, 45, 51],
          backgroundColor: ["#73BFB8", "red", "green", "blue", "black", "ping", "grey"], 
        },
      ],
    };
  };

  return (
    <GraphHorizontalBar
      title="差出人ごとの受信件数（月別）"
      subTitle="配信メール一覧"
      data={getChartData(data)}
      options={options}
    />
  );
};

export default SharedMailsGraph;
