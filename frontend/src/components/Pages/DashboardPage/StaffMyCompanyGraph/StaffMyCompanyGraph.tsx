import React, {useEffect, useState} from "react";
import { ChartOptions } from "chart.js";
import { ChartData } from "react-chartjs-2";
import { ScheduledEmailStatsGraphModel } from "~/models/dashboardModel";
import { GraphDataModel } from "~/models/graphModel";
import { CommonGraphDataSet } from "~/utils/constants";
import GraphHorizontalBar from "../GraphCard/GraphHorizontalBar";
import { pickerLocaleConfig } from "~/utils/constants";
import { DatePicker } from "antd";
import moment from "moment";

const StaffMyCompanyGraph = ({ data }: any) => {
  const dateNow = moment().subtract(1, 'month').format('YYYY/M')
  const [key, setKey] = useState(dateNow);
  const dataChart:any = data[key];
  const topUsersStats = dataChart ? dataChart?.top_values : '';
  const [dataNew, setDataNew] = useState<any[]>([]);
  const [barHide, setBarHide] = useState<any[]>([]); 
  const [isHover, setIsHover] = useState(false)
  const [isNameHover, setIsNameHover] = useState('');
  const [isLeft, setIsLeft] = useState(0);
  const backgroundColor = ["rgba(212,56,12,1)", "rgba(212,108,11,1)", "rgba(12,212,100,1)", "rgba(30,56,195,1)", "rgba(123,141,236,1)", "rgba(126,206,238,1)"];
  function onChange(date:any) {
    const dateNew = moment(date).format('YYYY/M');
    setKey(dateNew);
  }

  function disabledDate(current:any) {
    return current && (current >= moment().subtract(1, 'month') || current <= moment().subtract(6, 'month'));
  }

  const renderExtraDate = () => {
    return [
        <DatePicker 
          placeholder={moment().subtract(1, 'month').format('YYYY/M')} 
          onChange={onChange} picker="month" 
          locale={pickerLocaleConfig} 
          disabledDate={disabledDate}
          defaultValue={moment(moment().subtract(1, 'month'), 'YYYY/M')} 
        />,
    ]
  }

  useEffect(() => {
    if(topUsersStats != ''){
      setBarHide([-1]);
      setDataNew([...topUsersStats]);
    }
  },[key])

  useEffect(() => {
    if(topUsersStats != ''){
      const dataNewClone = [...topUsersStats];
      barHide.map((item)=>{
        dataNewClone[item] = 0;
      })
      setDataNew([...dataNewClone])
    }
  },[barHide])

  const options: ChartOptions = {
    responsive: true,
    tooltips: {
      titleFontSize: 0,
      callbacks: {
          label: function(tooltipItem) {
              return tooltipItem.label + ": " + tooltipItem.value;
          }
      }
    },
    scales: {
      xAxes: [{
          ticks: {
              beginAtZero: true
          }
      }],
      yAxes: [{
        gridLines: {
          drawOnChartArea: false
        },
      }],
    },
    legend: {
      display: true,
      position: 'top',
      onHover: (event:any, legendItem:any) => {
        const label = legendItem.text === 'その他' ?  'other' : legendItem.text
        const index = dataChart?.from_name?.indexOf(label)
        setIsNameHover(dataChart?.from_address[index])
        setIsLeft(event.x)
        setIsHover(label === 'other' ? false : true);
      },
      onLeave: function() {
        setIsHover(false);
      },
      onClick: (event:any, legendItem:any) => {
        const label = legendItem.text === 'その他' ?  'other' : legendItem.text
        const index = dataChart?.staff?.indexOf(label)
        if(barHide.indexOf(index) !== -1){
          setBarHide(barHide.filter(data => data !== index));
        }else{
          setBarHide(oldArray => [...oldArray, index]);
        }
      },
      labels: {
        generateLabels: function(chart:any) {
          if(topUsersStats != ''){
            return dataChart?.staff.map((label:any, index:number) => ({
              text: label === 'other' ? 'その他' : label,
              hidden: dataNew[index] === 0 ? true : false,
              fillStyle: backgroundColor[index],
              strokeStyle: backgroundColor[index],
            }));
          }

        }
      }
    }
  };

  const getChartData = (
    data: GraphDataModel<ScheduledEmailStatsGraphModel>
  ): ChartData<any> => {
    return {
      labels: topUsersStats != '' ? dataChart?.staff.map((item:string)=>{ return item === 'other' ? 'その他' : item}) : [],
      datasets: [
        {
          indexAxis: 'y',
          label: '',
          borderWidth: 1,
          data: topUsersStats.length > 0 ? dataNew : [],
          backgroundColor: backgroundColor
        },
      ],
    };
  };

  return (
    <>
      <GraphHorizontalBar
        title="共有メールの自社担当者（月別）"
        subTitle="共有メール一覧"
        extra = {renderExtraDate()}
        data={getChartData(data)}
        options={options}
      />
      {isHover ? (
        <div style={{ top: 65, left: isLeft - 330, height: 30, backgroundColor: 'black', position:'absolute', color: 'white', padding: '5px 10px' }}>{isNameHover}</div>
      ) : (
        <div></div>
      )}

    </>
  );
};

export default StaffMyCompanyGraph;