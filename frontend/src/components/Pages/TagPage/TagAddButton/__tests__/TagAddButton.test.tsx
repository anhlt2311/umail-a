import React from "react";
import TagAddButton from "../TagAddButton";
import Paths from "~/components/Routes/Paths";
import {
    mockHistoryPush,
    render,
    renderWithMemoryRouter,
    screen,
    userEvent,
} from "~/test/utils";

describe("TagAddButton", () => {
    test("render test", () => {
        render(<TagAddButton />);
        const buttonElement = screen.getByText(/タグ追加/);
        expect(buttonElement).toBeInTheDocument();
    });
    test("button fire event test", async () => {
        renderWithMemoryRouter(<TagAddButton />);
        const buttonElement = screen.getByText(/タグ追加/);
        await userEvent.click(buttonElement);
        expect(mockHistoryPush).toHaveBeenCalledWith(Paths.tagRegister);
    });
});
