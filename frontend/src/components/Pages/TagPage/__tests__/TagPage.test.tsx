import React from "react";
import { renderWithAllProviders, screen, userEvent } from "~/test/utils";
import TagPage from "../TagPage";
import { configureStore, PayloadAction } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import {
    contactSearchPage,
    scheduledEmailEditPageContactSearchForm,
} from "~/reducers/pages";
import { generateRandomToken } from "~/utils/utils";

const randomToken = generateRandomToken();

describe("TagPage", () => {
    test("render test", () => {
        renderWithAllProviders(<TagPage />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                    contactSearchPage,
                    scheduledEmailEditPageContactSearchForm,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: randomToken,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: true,
                            },
                        },
                    },
                    contactSearchPage: {
                        currentSearchConditions: {},
                    },
                    scheduledEmailEditPageContactSearchForm: {
                        currentSearchConditions: {},
                    },
                },
            }),
        });
        const headingElement = screen.getByText(/タグ設定/);
        expect(headingElement).toBeInTheDocument();
        const buttonElement = screen.getByText(/タグ追加/);
        expect(buttonElement).toBeInTheDocument();
        const searchButtonElement = screen.getByText(/絞り込み検索/);
        expect(searchButtonElement).toBeInTheDocument();
        const tableElement = screen.getByRole("table");
        expect(tableElement).toBeInTheDocument();
        const trElements = screen.getAllByRole("row");
        expect(trElements.length).toEqual(2);
    });

    test("open search popover", async () => {
        renderWithAllProviders(<TagPage />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                    contactSearchPage,
                    scheduledEmailEditPageContactSearchForm,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: randomToken,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: true,
                            },
                        },
                    },
                    contactSearchPage: {
                        currentSearchConditions: {},
                    },
                    scheduledEmailEditPageContactSearchForm: {
                        currentSearchConditions: {},
                    },
                },
            }),
        });
        const searchButtonElement = screen.getByText(/絞り込み検索/);
        await userEvent.click(searchButtonElement);
        const inputElement = await screen.findByPlaceholderText(/タグ名/);
        expect(inputElement).toBeInTheDocument();
    });

    test("authorized test", () => {
        renderWithAllProviders(<TagPage />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                    contactSearchPage,
                    scheduledEmailEditPageContactSearchForm,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: randomToken,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: true,
                            },
                        },
                    },
                    contactSearchPage: {
                        currentSearchConditions: {},
                    },
                    scheduledEmailEditPageContactSearchForm: {
                        currentSearchConditions: {},
                    },
                },
            }),
        });
        const notFoundTitleElement =
            screen.queryByText(/^ページが見つかりません$/);
        expect(notFoundTitleElement).not.toBeInTheDocument();
    });

    test("unauthorized test", () => {
        renderWithAllProviders(<TagPage />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                    contactSearchPage,
                    scheduledEmailEditPageContactSearchForm,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: randomToken,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: false,
                            },
                        },
                    },
                    contactSearchPage: {
                        currentSearchConditions: {},
                    },
                    scheduledEmailEditPageContactSearchForm: {
                        currentSearchConditions: {},
                    },
                },
            }),
        });
        const notFoundTitleElement =
            screen.getByText(/^ページが見つかりません$/);
        expect(notFoundTitleElement).toBeInTheDocument();
    });
});
