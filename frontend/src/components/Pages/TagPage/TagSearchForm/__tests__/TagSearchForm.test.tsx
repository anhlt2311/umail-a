import React from "react";
import { render, screen, userEvent } from "~/test/utils";
import TagSearchForm from "../TagSearchForm";
import { MemoryRouter } from "react-router-dom";

const mockedOnSearchValueChange = jest.fn();

describe("TagSearchForm", () => {
    test("render test", () => {
        render(
            <MemoryRouter initialEntries={["/tags"]}>
                <TagSearchForm
                    onSearchValueChange={mockedOnSearchValueChange}
                />
            </MemoryRouter>
        );
        const inputElement = screen.getByPlaceholderText(/タグ名/);
        expect(inputElement).toBeInTheDocument();
    });

    test("input test", async () => {
        render(
            <MemoryRouter initialEntries={["/tags"]}>
                <TagSearchForm
                    onSearchValueChange={mockedOnSearchValueChange}
                />
            </MemoryRouter>
        );
        // NOTE(joshua-hashimoto): .valueを通すために、あえて型をanyに設定
        const inputElement: any = screen.getByPlaceholderText(/タグ名/);
        await userEvent.type(inputElement, "New Tag Name");
        expect(inputElement.value).toContain("New Tag Name");
    });
});
