import React from "react";
import { Col, Row, Table, Tag } from "antd";
import { ColumnType, TableProps } from "antd/lib/table";
import { TagModel } from "~/models/tagModel";
import { useHistory } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import TooltipCopy from "~/components/Common/TooltipCopy/TooltipCopy";
import styles from "./TagTable.scss";

const columns: ColumnType<TagModel>[] = [
    {
        title: "タグ名",
        dataIndex: "value",
        key: "value",
        className: styles.tooltipCopy,
        render: (value, record, column_index) => {
            return (
                <Row>
                    <Tag color={record.color}>{value}</Tag>
                    <TooltipCopy copyContent={value} placement="topRight" />
                </Row>
            );
        },
    },
];

type Props = TableProps<TagModel> & {
    onRowSelected: (
        selectedRowKeys: React.Key[],
        selectedRows: TagModel[]
    ) => void;
};

const TagTable = ({ onRowSelected, ...props }: Props) => {
    const router = useHistory();

    const onRowClick = (record: TagModel) => {
        router.push(`${Paths.tags}/${record.id}`);
    };

    return (
        <Table
            {...props}
            columns={columns}
            onRow={(record) => ({
                onClick: () => onRowClick(record),
            })}
            bordered
            rowSelection={{
                type: "checkbox",
                fixed: true,
                columnWidth: 35,
                onChange: onRowSelected,
            }}
            pagination={false}
            rowKey="id"
        />
    );
};

export default TagTable;
