import React, { useEffect, useState } from "react";
import {
    Button,
    Card,
    Col,
    Form,
    Row,
    Spin,
    PageHeader,
    Typography,
    Select,
    Tooltip,
} from "antd";
import { useDispatch, useSelector } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import moment from "moment";
import { RootState } from "~/models/store";
import {
    deleteAction,
    fetchAction,
    fetchApi,
    simpleAction,
    simpleFetch,
} from "~/actions/data";
import { SHARED_EMAIL_DETAIL_PAGE } from "~/components/Pages/pageIds";
import { Endpoint } from "~/domain/api";
import getDateStr from "~/domain/date";
import {
    AUTHORIZED_ACTION_LOADED,
    AUTHORIZED_ACTION_LOADING,
    PARENT_CANNOT_SUBMIT_WHEN_NEW_COMMENT_EXISTS,
    PARENT_CANNOT_SUBMIT_WHEN_EDIT_COMMENT_EXISTS
} from "~/actions/actionTypes";
import EmailHead from "~/components/Pages/EmailComponents/EmailHead";
import EmailBody from "~/components/Pages/EmailComponents/EmailBody";
import ErrorScreen from "~/components/Screens/ErrorScreen";
import BackButton from "~/components/Common/BackButton/BackButton";
import { showDeleteModal } from "~/components/Feedbacks/Modal/Modal";
import Paths from "~/components/Routes/Paths";
import styles from "~/components/Pages/page.scss";
import { DisplayTextAndForeignKeyModel } from "~/models/displayTextAndForeignKeyModel";
import CommentListCreator from "../DataDisplay/CommentList/CommentList";
import CustomBackToTop from "../Common/CustomBackToTop/CustomBackToTop";
import { successMessageCustom, errorMessageCustom, infoMessageCustom } from "~/components/Common/AlertMessage/AlertMessage";
import {
    InfoMessages,
    SuccessMessages
} from "~/utils/constants";
import { SharedEmailAction } from "~/actionCreators/sharedEmailAction";


const pageId = SHARED_EMAIL_DETAIL_PAGE;
const baseURL = Endpoint.getBaseUrl();
const resourceURL = `${baseURL}/${Endpoint.sharedEmails}`;
const staffResourceURL = `${Endpoint.getBaseUrl()}/${
    Endpoint.users
}?is_active=true`;
const reducerName = "sharedEmailDetailPage";
const commentsReducerName = "sharedEmailDetailPageComments";
const newCommentsReducerName = "sharedEmailDetailPageNewComments";
const editCommentsReducerName = "sharedEmailDetailPageEditComments";
const replyCommentReducerName = "sharedEmailDetailPageReplyComments";
const commentTemplateUrl = `${Endpoint.getBaseUrl()}/${
    Endpoint.commentTemplateSharedEmail
}`;

const convertData = (responseData: any) => ({
    sender: responseData.from_name,
    email: responseData.from_address,
    subject: responseData.subject,
    body: responseData.html ? responseData.html : responseData.text,
    category: responseData.estimated_category,
    staff_in_charge__name: responseData.staff_in_charge__name,
    staff_in_charge__id: responseData.staff_in_charge__id,
    sentDate: getDateStr(responseData.sent_date),
    attachments: responseData.attachments,
    format: responseData.html ? "html" : "text",
});

const CommentList = CommentListCreator(
    pageId,
    commentsReducerName,
    commentTemplateUrl,
    newCommentsReducerName,
    editCommentsReducerName,
    replyCommentReducerName
);

const { Title, Paragraph } = Typography;
const { Option } = Select;

const commentsLayout = {
    xs: 24,
    sm: 24,
    md: 21,
    lg: 18,
    xl: 15,
    xxl: 10,
};

type RouterParams = {
    id: string;
};

type Props = RouteComponentProps<RouterParams> & {};

const SharedEmailDetailPage = ({ history, match }: Props) => {
    const dispatch = useDispatch();
    const token = useSelector((state: RootState) => state.login.token);
    const userId = useSelector((state: RootState) => state.login.userId);
    const authorizedActions = useSelector(
        (state: RootState) => state.login.authorizedActions
    );
    const deleteAuthorized =
        authorizedActions &&
        authorizedActions["shared_emails"] &&
        authorizedActions["shared_emails"]["delete"];
    const selectSttaffAuthorized =
        authorizedActions &&
        authorizedActions["shared_emails"] &&
        authorizedActions["shared_emails"]["select_staff"];
    const selectAllSttaffAuthorized =
        authorizedActions &&
        authorizedActions["shared_emails"] &&
        authorizedActions["shared_emails"]["select_all_staff"];

    const { loading, data, errorMessage, canNotDelete, deleted } = useSelector(
        (state: RootState) => state.sharedEmailDetailPage
    );
    const newCommentState = useSelector(
      (state: RootState) => state.sharedEmailDetailPageNewComments);
    const editCommentState = useSelector(
      (state: RootState) => state.sharedEmailDetailPageEditComments);
    const [staffs, setStaffs] = useState<DisplayTextAndForeignKeyModel[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [selectedStaff, setSelectedStaff] =  useState<string | undefined>(undefined);

    const fetchData = (resourceId: string) => {
        dispatch(
            fetchAction(pageId, token, resourceURL, resourceId, convertData)
        );
    };

    const forwardEmail = async () => {
      if (newCommentState.commentValue) {
        dispatch({
            type: PARENT_CANNOT_SUBMIT_WHEN_NEW_COMMENT_EXISTS,
        });
        return;
      }
      if (editCommentState.commentValue) {
        dispatch({
            type: PARENT_CANNOT_SUBMIT_WHEN_EDIT_COMMENT_EXISTS,
        });
        return;
      }
      const {
          params: { id },
      } = match;
      infoMessageCustom(InfoMessages.scheduledEmails.transfer);
      try {
          const url = `${resourceURL}/${id}/forward`;
          const postData = { staff_in_charge_id: selectedStaff };
          await simpleAction(pageId, token, url, postData);
          successMessageCustom(SuccessMessages.scheduledEmails.transfer);
          history.push(Paths.sharedMails);
      } catch (err) {
          errorMessageCustom("メールを転送できませんでした。");
      }
    };

    const onDelete = () => {
        const {
            params: { id },
        } = match;
        dispatch(deleteAction(pageId, token, resourceURL, id));
    };

    const fetchStaffs = async () => {
        setIsLoading(true);
        try {
            const response = await simpleFetch(token, staffResourceURL);
            const results = response.results || [];
            const staffs = results.map((result: any) => {
                return {
                    displayText: result["display_name"],
                    foreignKey: result.id,
                };
            });
            setStaffs(staffs);
        } catch (err) {
            console.error(err);
        }
        setIsLoading(false);
    };

    const renderEmailDate = () => {
        if (!data || !data.sentDate) {
            return null;
        }
        const today = moment();
        const emailDate = moment(data.sentDate, "YYYYMMDD HH:mm");
        let emailDateString = data.sentDate;
        if (
            today.year() === emailDate.year() &&
            today.month() === emailDate.month() &&
            today.date() === emailDate.date()
        ) {
            emailDateString = data.sentDate.split(" ")[1];
        }
        return <Paragraph>{emailDateString}</Paragraph>;
    };

    const partialMatchFilter = (input: string, option: any) => {
        return (
            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >=
            0
        );
    };

    const renderStaffs = () => {
        return (
            <Form.Item colon={false} noStyle>
                <Col span={10} style={{ marginTop: "3%" }}>
                    <Spin size="small" spinning={isLoading}>
                        <Form.Item
                            label="自社担当者"
                            rules={[
                                {
                                    validator: (rule, value, callback) => {
                                        if (value) {
                                            if (value.length > 5) {
                                                value.pop();
                                                callback();
                                            } else if (value.length <= 5) {
                                                callback();
                                            }
                                        } else {
                                            callback();
                                        }
                                    },
                                },
                            ]}>
                            <Select
                                style={{ textAlign: "left" }}
                                showSearch
                                filterOption={partialMatchFilter}
                                allowClear
                                placeholder="クリックして選択"
                                onChange={(value: string) => {
                                    setSelectedStaff(value);
                                }}
                                disabled={
                                    data &&
                                    data.staff_in_charge__id &&
                                    !selectAllSttaffAuthorized
                                }
                                value={selectedStaff}>
                                {staffs.map((staff) => {
                                    if (
                                        selectAllSttaffAuthorized ||
                                        staff.foreignKey == userId ||
                                        staff.foreignKey == selectedStaff
                                    ) {
                                        return (
                                            <Select.Option
                                                key={staff.foreignKey}
                                                value={staff.foreignKey}>
                                                {staff.displayText}
                                            </Select.Option>
                                        );
                                    }
                                })}
                            </Select>
                        </Form.Item>
                    </Spin>
                </Col>
            </Form.Item>
        );
    };
    const renderComments = () => {
        const {
            params: { id },
        } = match;
        return (
            <Col {...commentsLayout} style={{ marginTop: "5%" }}>
                <Row justify="start">
                    <Title level={5}>コメント一覧</Title>
                </Row>
                <CommentList
                    resourceUrl={`${resourceURL}/${id}/${Endpoint.commentUrlSuffix}`}
                />
            </Col>
        );
    };

    useEffect(() => {
        const {
            params: { id },
        } = match;
        fetchData(id);
        fetchStaffs();
    }, []);

    useEffect(() => {
        if (data && data.staff_in_charge__id) {
            setSelectedStaff(data.staff_in_charge__id);
        }
    }, [data]);

    useEffect(() => {
        if (deleted) {
            history.push(Paths.sharedMails);
        }
    }, [deleted]);

    useEffect(() => {
        if (editCommentState.commentError) {
            errorMessageCustom(editCommentState.commentError);
            dispatch(
                SharedEmailAction.clearErrorMessageEditComment(
                    {
                        commentValue: editCommentState.commentValue
                    }
                ));
        }
    }, [editCommentState.commentError]);

    useEffect(() => {
        if (newCommentState.commentError) {
            errorMessageCustom(newCommentState.commentError);
            dispatch(SharedEmailAction.clearErrorMessageNewComment(
                {
                    commentValue: newCommentState.commentValue
                }
            ));
        }
    }, [newCommentState.commentError]);

    return (
        <div className={styles.container}>
            <CustomBackToTop />
            <Spin spinning={loading}>
                <PageHeader
                    className={styles.pageHeader}
                    title="共有メール 詳細"
                />
                {errorMessage && Object.keys(data).length === 0 ? (
                    <ErrorScreen message={errorMessage} />
                ) : (
                    <Col span={24}>
                        <Row justify="center">
                            <Col span={19}>
                                <Row justify="end">{renderEmailDate()}</Row>
                                <Row justify="center">
                                    <Col span={24}>
                                        <Card>
                                            <EmailHead data={data} />
                                            <EmailBody
                                                text={data.body}
                                                text_format={data.format}
                                                mark={false}
                                            />
                                        </Card>
                                    </Col>
                                </Row>
                                <Row justify="center">
                                    <Col span={24}>{renderStaffs()}</Col>
                                </Row>
                                <Row justify="center">
                                    <Col span={24}>
                                        <Row justify="start">
                                            {renderComments()}
                                        </Row>
                                    </Col>
                                </Row>
                                <Row justify="center">
                                    <Col span={24} style={{ marginTop: "5%" }}>
                                        <Row justify="start">
                                            <BackButton
                                                to={Paths.sharedMails}
                                            />
                                            {selectSttaffAuthorized ? (
                                                <Button
                                                    type="primary"
                                                    onClick={forwardEmail}
                                                    htmlType="button"
                                                    disabled={!selectedStaff}>
                                                    自社担当者にメール転送
                                                </Button>
                                            ) : (
                                                <Tooltip
                                                    title={
                                                        "特定の権限で操作できます"
                                                    }>
                                                    <Button
                                                        type="primary"
                                                        htmlType="button"
                                                        disabled={true}>
                                                        自社担当者にメール転送
                                                    </Button>
                                                </Tooltip>
                                            )}
                                        </Row>
                                        <Row justify="end">
                                            {deleteAuthorized ? (
                                                <Button
                                                    type="primary"
                                                    danger
                                                    onClick={showDeleteModal(
                                                        onDelete,
                                                        reducerName
                                                    )}
                                                    htmlType="button"
                                                    hidden={canNotDelete}>
                                                    削除
                                                </Button>
                                            ) : (
                                                <Tooltip
                                                    title={
                                                        "特定の権限で操作できます"
                                                    }>
                                                    <Button
                                                        type="primary"
                                                        danger
                                                        htmlType="button"
                                                        hidden={canNotDelete}
                                                        disabled={true}>
                                                        削除
                                                    </Button>
                                                </Tooltip>
                                            )}
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                )}
            </Spin>
        </div>
    );
};

export default SharedEmailDetailPage;
