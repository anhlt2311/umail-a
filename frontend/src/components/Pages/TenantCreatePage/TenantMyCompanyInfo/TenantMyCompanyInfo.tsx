import React, { useEffect } from "react";
import {
    Button,
    Col,
    Form,
    FormItemProps,
    Input,
    InputNumber,
    message,
    Radio,
    Row,
    Spin,
    Tooltip,
    Select
} from "antd";
import CustomMonthPicker from "~/components/Common/CustomMonthPicker/CustomMonthPicker";
import {
    TenantMyCompanyFormModel,
    TenantMyCompanyRequestModel,
} from "~/models/tenantModel";
import {
    useTenantFetchMyCompanyAPIQuery,
    useTenantMyCompanyAPIMutate,
} from "~/hooks/useTenant";
import moment from "moment";
import { formatMoneyNumberString } from "~/components/helpers";
import { disabledFutureDates } from "~/utils/utils";
import { QuestionCircleFilled } from "@ant-design/icons";
import { Links, iconCustomColor } from "~/utils/constants";
import styles from "./TenantMyCompanyInfo.scss";
import {
    ErrorMessages,
    ONLY_HANKAKU_REGEX,
    HANKAKU_NUMBER_REGEX,
    RESTRICT_SPACE_REGEX,
} from "~/utils/constants";
import { customErrorMessage } from "~/components/Common/AlertMessage/AlertMessage";
import AddressInputFormItem from "~/components/Forms/AddressInputFormItem/AddressInputFormItem";

const formItemIndent1LayoutSpan = 4;
const formItemIndent1Layout = {
    labelCol: {
        span: formItemIndent1LayoutSpan,
    },
    wrapperCol: {
        span: 24 - formItemIndent1LayoutSpan,
    },
};

const formItemIndent2LayoutSpan = formItemIndent1LayoutSpan + 5;
const formItemIndent2Layout = {
    labelCol: {
        span: formItemIndent2LayoutSpan,
    },
    wrapperCol: {
        span: 24 - formItemIndent2LayoutSpan,
    },
};

const commonFormItemProps: FormItemProps = {
    labelAlign: "right",
};

const formItemIndent1Props: FormItemProps = {
    ...commonFormItemProps,
    ...formItemIndent1Layout,
};

const formItemIndent2Props: FormItemProps = {
    ...commonFormItemProps,
    ...formItemIndent2Layout,
};

type Props = {
    authToken: string;
};

const TenantMyCompanyInfo = ({ authToken }: Props) => {
    const requiredFields = [
        "name",
        "establishment_date",
        "address",
        "domain_name",
        "capital_man_yen",
        "has_distribution",
        "has_invoice_system",
        "has_p_mark_or_isms",
        "has_haken",
    ];
    const [form] = Form.useForm<TenantMyCompanyFormModel>();
    const { mutate: register, isLoading: isMutateLoading } =
        useTenantMyCompanyAPIMutate();
    const { data, isLoading } = useTenantFetchMyCompanyAPIQuery({
        authToken,
    });

    const onRegisterMyCompanyInfo = async () => {
        try {
            const values = await form.validateFields();
            const postData: TenantMyCompanyRequestModel = {
                ...values,
                establishment_date: moment(values.establishment_date).format(
                    "YYYY-MM-DD"
                ),
                authToken,
            };
            register(postData);
        } catch (err) {
            customErrorMessage(
                "データの登録に失敗しました。入力情報に誤りがないかご確認ください。"
            );
        }
    };

    useEffect(() => {
        // NOTE(joshua-hashimoto): nameは必須項目なので、nameプロパティが存在するということは一度登録したということ
        if (data && data.name) {
            form.setFieldsValue(data);
        } else {
            form.setFieldsValue({
                establishment_date: moment(),
            });
        }
    }, [data]);

    return (
        <Col span={24}>
            <Spin spinning={isLoading || isMutateLoading}>
                <Col span={24} style={{ marginTop: "3%" }}>
                    <Row justify="center">
                        <Col span={12}>
                            <Form
                                form={form}
                                onFinish={onRegisterMyCompanyInfo}
                                style={{ textAlign: "left" }}>
                                <Form.Item
                                    {...formItemIndent1Props}
                                    className={styles.formField}
                                    label="自社名"
                                    name="name"
                                    rules={[
                                        { required: true },
                                        {
                                            pattern: RESTRICT_SPACE_REGEX,
                                            message:
                                                ErrorMessages.validation.regex
                                                    .space,
                                        },
                                        {
                                            max: 100,
                                            message:
                                                ErrorMessages.validation.length
                                                    .max100,
                                        },
                                    ]}>
                                    <Input placeholder="サンプル株式会社" />
                                </Form.Item>
                                <Form.Item
                                    {...formItemIndent1Props}
                                    className={styles.formField}
                                    label="設立年月"
                                    name="establishment_date"
                                    rules={[{ required: true }]}>
                                    <CustomMonthPicker
                                        inputReadOnly
                                        disabledDate={disabledFutureDates}
                                    />
                                </Form.Item>
                                <Form.Item
                                    {...formItemIndent1Props}
                                    label="決算期"
                                    className={styles.field}
                                    name="settlement_month"
                                    rules={[{ required: true }]}>
                                    <Select
                                        style={{ width: 120 }}
                                        placeholder="月を選択"
                                        allowClear>
                                        <Select.Option value={1}>
                                            1月
                                        </Select.Option>
                                        <Select.Option value={2}>
                                            2月
                                        </Select.Option>
                                        <Select.Option value={3}>
                                            3月
                                        </Select.Option>
                                        <Select.Option value={4}>
                                            4月
                                        </Select.Option>
                                        <Select.Option value={5}>
                                            5月
                                        </Select.Option>
                                        <Select.Option value={6}>
                                            6月
                                        </Select.Option>
                                        <Select.Option value={7}>
                                            7月
                                        </Select.Option>
                                        <Select.Option value={8}>
                                            8月
                                        </Select.Option>
                                        <Select.Option value={9}>
                                            9月
                                        </Select.Option>
                                        <Select.Option value={10}>
                                            10月
                                        </Select.Option>
                                        <Select.Option value={11}>
                                            11月
                                        </Select.Option>
                                        <Select.Option value={12}>
                                            12月
                                        </Select.Option>
                                    </Select>
                                </Form.Item>
                                <AddressInputFormItem
                                    {...formItemIndent1Props}
                                    required
                                    addressProps={{
                                        rules: [
                                            {
                                                required: true,
                                                message:
                                                    "市区町村・町名・番地を入力してください",
                                            },
                                        ],
                                        style: {
                                            marginBottom: 0,
                                            textAlign: "left",
                                        },
                                    }}
                                    buildingProps={{
                                        style: {
                                            marginBottom: 0,
                                            textAlign: "left",
                                        },
                                    }}
                                />
                                <Form.Item
                                    {...formItemIndent1Props}
                                    className={styles.formField}
                                    label="URL"
                                    name="domain_name"
                                    rules={[
                                        { required: true },
                                        {
                                            pattern: ONLY_HANKAKU_REGEX,
                                            message:
                                                ErrorMessages.validation.regex
                                                    .onlyHankaku,
                                        },
                                        {
                                            pattern: RESTRICT_SPACE_REGEX,
                                            message:
                                                ErrorMessages.validation.regex
                                                    .space,
                                        },
                                        {
                                            max: 50,
                                            message:
                                                ErrorMessages.validation.length
                                                    .max50,
                                        },
                                    ]}>
                                    <Input placeholder="https://" />
                                </Form.Item>
                                <Form.Item
                                    {...formItemIndent1Props}
                                    className={styles.formField}
                                    label={
                                        <span>
                                            商流&nbsp;
                                            <Tooltip
                                                title={
                                                    <span>
                                                        取引先が指定する取引条件を自社プロフィールの内容が満たしていない場合、
                                                        自社が商流を抜けることで取引対象となる場合があります。
                                                        <br />
                                                        <a
                                                            href={
                                                                Links.helps
                                                                    .commercialDistribution
                                                                    .exit
                                                            }
                                                            target="_blank"
                                                            rel="noopener noreferrer">
                                                            詳細
                                                        </a>
                                                    </span>
                                                }>
                                                <QuestionCircleFilled
                                                    style={{
                                                        color: iconCustomColor,
                                                    }}
                                                />
                                            </Tooltip>
                                        </span>
                                    }
                                    name="has_distribution"
                                    rules={[{ required: true }]}>
                                    <Radio.Group>
                                        <Radio value={false}>抜けない</Radio>
                                        <Radio value={true}>抜ける</Radio>
                                    </Radio.Group>
                                </Form.Item>
                                <Form.Item
                                    {...formItemIndent1Props}
                                    className={styles.formField}
                                    label="資本金"
                                    required>
                                    <Row align="middle" gutter={4}>
                                        <Col>
                                            <Form.Item
                                                name="capital_man_yen"
                                                rules={[
                                                    {
                                                        required: true,
                                                        type: "number",
                                                        min: 1,
                                                        message:
                                                            "資本金を入力してください",
                                                    },
                                                    {
                                                        pattern:
                                                            HANKAKU_NUMBER_REGEX,
                                                        message:
                                                            ErrorMessages
                                                                .validation
                                                                .regex
                                                                .onlyHankakuNumber,
                                                    },
                                                    {
                                                        validator: (
                                                            _,
                                                            value
                                                        ) => {
                                                            const convertedValue =
                                                                String(
                                                                    value ?? ""
                                                                );
                                                            if (
                                                                convertedValue.length >
                                                                9
                                                            ) {
                                                                return Promise.reject(
                                                                    new Error(
                                                                        ErrorMessages.validation.length.max9
                                                                    )
                                                                );
                                                            }
                                                            return Promise.resolve();
                                                        },
                                                    },
                                                ]}
                                                noStyle>
                                                <InputNumber
                                                    min={0}
                                                    step={100}
                                                    formatter={
                                                        formatMoneyNumberString
                                                    }
                                                />
                                            </Form.Item>
                                        </Col>
                                        <Col>
                                            <span>万円</span>
                                        </Col>
                                    </Row>
                                </Form.Item>
                                <Form.Item
                                    {...formItemIndent1Props}
                                    className={styles.formField}
                                    label="保有資格"
                                    style={{ marginBottom: "0" }}></Form.Item>
                                <Form.Item>
                                    <Form.Item
                                        {...formItemIndent2Props}
                                        className={styles.formField}
                                        label="Pマーク／ISMS"
                                        name="has_p_mark_or_isms"
                                        rules={[{ required: true }]}>
                                        <Radio.Group>
                                            <Radio value={false}>なし</Radio>
                                            <Radio value={true}>あり</Radio>
                                        </Radio.Group>
                                    </Form.Item>
                                    <Form.Item
                                        {...formItemIndent2Props}
                                        className={styles.formField}
                                        label="インボイス登録事業者"
                                        name="has_invoice_system"
                                        rules={[{ required: true }]}>
                                        <Radio.Group>
                                            <Radio value={false}>なし</Radio>
                                            <Radio value={true}>あり</Radio>
                                        </Radio.Group>
                                    </Form.Item>
                                    <Form.Item
                                        {...formItemIndent2Props}
                                        className={styles.formField}
                                        label="労働者派遣事業"
                                        name="has_haken"
                                        rules={[{ required: true }]}>
                                        <Radio.Group>
                                            <Radio value={false}>なし</Radio>
                                            <Radio value={true}>あり</Radio>
                                        </Radio.Group>
                                    </Form.Item>
                                </Form.Item>
                                <Col span={24}>
                                    <Row justify="center">
                                        <Col
                                            span={12}
                                            style={{ textAlign: "center" }}>
                                            <Form.Item shouldUpdate>
                                                {() => (
                                                    <Button
                                                        type="primary"
                                                        htmlType="submit"
                                                        disabled={
                                                            !form.isFieldsTouched(
                                                                requiredFields,
                                                                true
                                                            ) ||
                                                            !!form
                                                                .getFieldsError()
                                                                .filter(
                                                                    ({
                                                                        errors,
                                                                    }) =>
                                                                        errors.length
                                                                ).length
                                                        }>
                                                        自社プロフィール情報を登録する
                                                    </Button>
                                                )}
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                </Col>
                            </Form>
                        </Col>
                    </Row>
                </Col>
            </Spin>
        </Col>
    );
};

export default TenantMyCompanyInfo;
