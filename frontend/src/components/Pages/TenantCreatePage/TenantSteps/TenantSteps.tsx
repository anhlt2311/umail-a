import React, { useState } from "react";
import { Steps } from "antd";
import styles from "./TenantSteps.scss";

const { Step } = Steps;

type Props = {
    currentStep: number;
};

const TenantSteps = ({ currentStep }: Props) => {
    return (
        <Steps size="small" current={currentStep}>
            <Step title="会員情報" />
            <Step title="自社プロフィール情報" />
            <Step title="個人プロフィール情報" />
            <Step title="お支払い情報(スキップ可)" />
        </Steps>
    );
};

export default TenantSteps;
