import React from "react";
import {
    renderWithQueryClient,
    screen,
    waitForElementToBeRemoved,
    userEvent,
    renderHook,
} from "~/test/utils";
import { generateRandomToken } from "~/utils/utils";
import TenantMyProfileInfo from "../TenantMyProfileInfo";
import { ErrorMessages } from "~/utils/constants";
import { useClient } from "~/hooks/useClient";

const authToken = generateRandomToken();

describe("TenantMyProfileInfo.tsx", () => {
    const emailSignatureTextAreaTestId =
        "tenant-my-profile-info-email-signature";
    const userServiceIdInputTestId = "user-service-id-input";

    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test", async () => {
        const { container } = renderWithQueryClient(
            <TenantMyProfileInfo authToken={authToken} />
        );
        await waitForElementToBeRemoved(() =>
            container.querySelector(".ant-spin-spinning")
        );
        const requiredElements = container.getElementsByClassName(
            "ant-form-item-required"
        );
        expect(requiredElements.length).toBe(1);
        // expect(requiredElements.length).toBe(2);
        // const profileLabelElement = screen.getByText(/^プロフィール画像$/);
        // expect(profileLabelElement).toBeInTheDocument();
        const userNameLabelElement = screen.getByText("ユーザー名");
        expect(userNameLabelElement).toBeInTheDocument();
        const userLastNameElement = screen.getByPlaceholderText("姓");
        expect(userLastNameElement).toBeInTheDocument();
        expect(userLastNameElement).not.toHaveValue();
        const userFirstNameElement = screen.getByPlaceholderText("名");
        expect(userFirstNameElement).toBeInTheDocument();
        expect(userFirstNameElement).not.toHaveValue();
        // const userServiceIdLabelElement = screen.getByText("ユーザーID");
        // expect(userServiceIdLabelElement).toBeInTheDocument();
        // const userServiceIdInputElement = screen.getByTestId(
        //     userServiceIdInputTestId
        // );
        // expect(userServiceIdInputElement).toBeInTheDocument();
        // expect(userServiceIdInputElement).not.toHaveValue();
        const emailAddressLabelElement =
            screen.getByLabelText("メールアドレス");
        expect(emailAddressLabelElement).toBeInTheDocument();
        const emailAddressElement =
            screen.getByPlaceholderText(/^you@example.com$/);
        expect(emailAddressElement).toBeInTheDocument();
        expect(emailAddressElement).not.toHaveValue();
        const roleLabelElement = screen.getByText("権限");
        expect(roleLabelElement).toBeInTheDocument();
        const masterRadioElement = screen.getByRole("radio", {
            name: "マスター",
        });
        expect(masterRadioElement).toBeInTheDocument();
        expect(masterRadioElement).not.toBeChecked();
        const adminRadioElement = screen.getByRole("radio", {
            name: "管理者",
        });
        expect(adminRadioElement).toBeInTheDocument();
        expect(adminRadioElement).not.toBeChecked();
        const managerRadioElement = screen.getByRole("radio", {
            name: "責任者",
        });
        expect(managerRadioElement).toBeInTheDocument();
        expect(managerRadioElement).not.toBeChecked();
        const leaderRadioElement = screen.getByRole("radio", {
            name: "リーダー",
        });
        expect(leaderRadioElement).toBeInTheDocument();
        expect(leaderRadioElement).not.toBeChecked();
        const memberRadioElement = screen.getByRole("radio", {
            name: "メンバー",
        });
        expect(memberRadioElement).toBeInTheDocument();
        expect(memberRadioElement).not.toBeChecked;
        const guestRadioElement = screen.getByRole("radio", {
            name: "ゲスト",
        });
        expect(guestRadioElement).toBeInTheDocument();
        expect(guestRadioElement).not.toBeChecked();
        const telLabelElement = screen.getByText(/^TEL$/);
        expect(telLabelElement).toBeInTheDocument();
        const tel1InputElement = screen.getByTestId("tel1");
        expect(tel1InputElement).toBeInTheDocument();
        expect(tel1InputElement).not.toHaveValue();
        const tel2InputElement = screen.getByTestId("tel2");
        expect(tel2InputElement).toBeInTheDocument();
        expect(tel2InputElement).not.toHaveValue();
        const tel3InputElement = screen.getByTestId("tel3");
        expect(tel3InputElement).toBeInTheDocument();
        expect(tel3InputElement).not.toHaveValue();
        const passwordLabelElement = screen.getByText(/^パスワード$/);
        expect(passwordLabelElement).toBeInTheDocument();
        const passwordElement =
            screen.getByPlaceholderText("大小英数字記号混在で10-50桁");
        expect(passwordElement).toBeInTheDocument();
        expect(passwordElement).not.toHaveValue();
        const passwordHelpTextElement = await screen.getByText(
            /^変更時のみ入力$/
        );
        expect(passwordHelpTextElement).toBeInTheDocument();
        const emailSignatureLabelElement = screen.getByLabelText("メール署名");
        expect(emailSignatureLabelElement).toBeInTheDocument();
        const emailSignatureElement = screen.getByTestId(
            emailSignatureTextAreaTestId
        );
        expect(emailSignatureElement).not.toHaveValue();
        const createButtonElement = screen.getByRole("button", {
            name: /^個人プロフィール情報を登録する$/,
        });
        expect(createButtonElement).toBeInTheDocument();
        expect(createButtonElement).toBeDisabled();
    });

    describe("user name validation test", () => {
        test("last name input", async () => {
            const { container } = renderWithQueryClient(
                <TenantMyProfileInfo authToken={authToken} />
            );
            await waitForElementToBeRemoved(() =>
                container.querySelector(".ant-spin-spinning")
            );
            const userLastNameElement = screen.getByPlaceholderText("姓");
            expect(userLastNameElement).not.toHaveValue();
            const userFirstNameElement = screen.getByPlaceholderText("名");
            expect(userFirstNameElement).not.toHaveValue();
            await userEvent.type(userLastNameElement, "last");
            const errorMessageRegex = new RegExp(ErrorMessages.form.required);
            const errorMessageElement = await screen.findByText(
                errorMessageRegex
            );
            expect(errorMessageElement).toBeInTheDocument();
        });

        // NOTE(joshua-hashimoto): 以前からある問題で、名を先に入力するとエラーが発生しないというものがある。これは修正しないといけないため、修正後にこちらのテストのコメントアウトを外す
        // test("first name input", async () => {
        //     renderWithQueryClient(
        //         <TenantMyProfileInfo authToken={authToken} />
        //     );
        //     const userLastNameElement = screen.getByPlaceholderText("姓");
        //     expect(userLastNameElement).not.toHaveValue();
        //     const userFirstNameElement = screen.getByPlaceholderText("名");
        //     expect(userFirstNameElement).not.toHaveValue();
        //     await userEvent.type(userFirstNameElement, "first");
        //     const errorMessageRegex = new RegExp(ErrorMessages.form.required);
        //     const errorMessageElement = await screen.findByText(
        //         errorMessageRegex
        //     );
        //     expect(errorMessageElement).toBeInTheDocument();
        // });
    });

    test("register button turns active after user name input is filled", async () => {
        const { container } = renderWithQueryClient(
            <TenantMyProfileInfo authToken={authToken} />
        );
        await waitForElementToBeRemoved(() =>
            container.querySelector(".ant-spin-spinning")
        );
        const createButtonElement = screen.getByRole("button", {
            name: /^個人プロフィール情報を登録する$/,
        });
        expect(createButtonElement).toBeDisabled();
        const userLastNameElement = screen.getByPlaceholderText("姓");
        await userEvent.type(userLastNameElement, "last");
        expect(createButtonElement).toBeDisabled();
        const userFirstNameElement = screen.getByPlaceholderText("名");
        await userEvent.type(userFirstNameElement, "first");
        expect(createButtonElement).not.toBeDisabled();
    });
});
