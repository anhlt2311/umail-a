import React, { useEffect, useState } from "react";
import {
    Button,
    Col,
    Form,
    FormItemProps,
    Input,
    Radio,
    Row,
    Spin,
    Tooltip,
} from "antd";
import {
    ErrorMessages,
    Links,
    PASSWORD_REGEX,
    RESTRICT_SPACE_REGEX,
    ROLES,
    iconCustomColor,
    SuccessMessages,
} from "~/utils/constants";
import { QuestionCircleFilled } from "@ant-design/icons";
import { TenantMyProfileFormModel } from "~/models/tenantModel";
import {
    useTenantFetchMyProfileAPIQuery,
    useTenantMyProfileAPIMutate,
} from "~/hooks/useTenant";
import TelInputFormItem from "~/components/Forms/TelInputFormItem/TelInputFormItem";
import ProfileThumbnailFormItem from "~/components/Common/ProfileThumbnailFormItem/ProfileThumbnailFormItem";
import { useDeleteThumbnailAPIMutation } from "~/hooks/useThumbnail";
import styles from "./TenantMyProfileInfo.scss";
import UserServiceIdFormItem from "~/components/Forms/UserServiceIdFormItem/UserServiceIdFormItem";
import { customSuccessMessage } from "~/components/Common/AlertMessage/AlertMessage";
import { useHistory } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import ValidateTextArea from "~/components/Common/ValidateTextArea/ValidateTextArea";

const { TextArea } = Input;

const formItemIndent1LayoutSpan = 5;
const formItemIndent1Layout = {
    labelCol: {
        span: formItemIndent1LayoutSpan,
    },
    wrapperCol: {
        span: 24 - formItemIndent1LayoutSpan,
    },
};

const commonFormItemProps: FormItemProps = {
    labelAlign: "right",
};

const formItemIndent1Props: FormItemProps = {
    ...commonFormItemProps,
    ...formItemIndent1Layout,
};

type Props = {
    authToken: string;
};

const TenantMyProfileInfo = ({ authToken }: Props) => {
    const requiredFields = ["first_name", "last_name"];
    const [form] = Form.useForm<TenantMyProfileFormModel>();
    const [thumbnailUrl, setThumbnailUrl] = useState<string | undefined>(
        undefined
    );
    const router = useHistory();
    const {
        mutate: register,
        isLoading: isRegisterLoading,
        error,
    } = useTenantMyProfileAPIMutate();
    const { mutate: deleteThumbnail } = useDeleteThumbnailAPIMutation();
    const { data, isLoading } = useTenantFetchMyProfileAPIQuery({
        authToken,
    });
    const [contentTextArea, setContentTextArea] = useState("");
    const onFinish = (values: TenantMyProfileFormModel) => {
        const postData = {
            ...values,
            authToken,
        };
        register(postData, {
            onSuccess: (result) => {
                const data = result.data;
                if (!data.is_completed) {
                    return;
                }
                customSuccessMessage(SuccessMessages.tenant.finish);
                router.push(Paths.login);
            },
        });
    };

    const onRemoveThumbnail = () => {
        const email = form.getFieldValue("email");
        deleteThumbnail(
            { email },
            {
                onSuccess: () => {
                    form.setFieldsValue({ avatar: undefined });
                    setThumbnailUrl(undefined);
                },
            }
        );
    };

    useEffect(() => {
        if (data) {
            form.setFieldsValue(data);
            setThumbnailUrl(data.avatar);
        } else {
            // NOTE(joshua-hashimoto): 初期状態で登録ボタンが非活性になるように修正
            form.setFieldsValue({ first_name: "", last_name: "" });
        }
    }, [data]);

    if (!data) {
        return <Spin spinning={isLoading} />;
    }

    return (
        <Col span={24}>
            <Spin spinning={isLoading || isRegisterLoading}>
                <Col span={24} style={{ marginTop: "3%" }}>
                    <Row justify="center">
                        <Col span={14}>
                            <Form
                                form={form}
                                onFinish={onFinish}>
                                {/* <ProfileThumbnailFormItem
                                    {...formItemIndent1Layout}
                                    className={styles.field}
                                    onRemoveThumbnail={onRemoveThumbnail}
                                    thumbnailUrl={thumbnailUrl}
                                    uploadProps={{
                                        beforeUpload: (file) => {
                                            form.setFieldsValue({
                                                avatar: file,
                                            });
                                            // NOTE(joshua-hashimoto): falseを返却することでAntdのUploadの自動送信を阻止
                                            return false;
                                        },
                                    }}
                                /> */}
                                <Form.Item
                                    {...formItemIndent1Props}
                                    label="ユーザー名"
                                    className={styles.formField}
                                    required>
                                    <Col span={24}>
                                        <Row>
                                            <Col span={12}>
                                                <Form.Item
                                                    name="last_name"
                                                    dependencies={[
                                                        "first_name",
                                                    ]}
                                                    rules={[
                                                        ({
                                                            getFieldValue,
                                                        }) => ({
                                                            validator: (
                                                                _,
                                                                value
                                                            ) => {
                                                                const lastName =
                                                                    !!value
                                                                        ? value
                                                                        : null;
                                                                // NOTE(joshua-hashimoto): スペース制御
                                                                if (
                                                                    !RESTRICT_SPACE_REGEX.test(
                                                                        lastName
                                                                    )
                                                                ) {
                                                                    return Promise.reject(
                                                                        new Error(
                                                                            ErrorMessages.validation.regex.space
                                                                        )
                                                                    );
                                                                }

                                                                const firstNameValue =
                                                                    getFieldValue(
                                                                        "first_name"
                                                                    );
                                                                const firstName =
                                                                    !!firstNameValue
                                                                        ? firstNameValue
                                                                        : null;
                                                                // NOTE(joshua-hashimoto): スペース制御
                                                                if (
                                                                    !RESTRICT_SPACE_REGEX.test(
                                                                        firstName
                                                                    )
                                                                ) {
                                                                    return Promise.reject(
                                                                        new Error(
                                                                            ErrorMessages.validation.regex.space
                                                                        )
                                                                    );
                                                                }

                                                                const fullName =
                                                                    lastName +
                                                                    firstName;

                                                                // NOTE(joshua-hashimoto): 必須項目制御
                                                                if (
                                                                    !lastName ||
                                                                    !firstName ||
                                                                    !fullName
                                                                ) {
                                                                    return Promise.reject(
                                                                        new Error(
                                                                            ErrorMessages.form.required
                                                                        )
                                                                    );
                                                                }

                                                                // NOTE(joshua-hashimoto): 50文字制御
                                                                if (
                                                                    fullName.length >
                                                                    50
                                                                ) {
                                                                    return Promise.reject(
                                                                        new Error(
                                                                            ErrorMessages.validation.length.max50
                                                                        )
                                                                    );
                                                                }
                                                                return Promise.resolve();
                                                            },
                                                        }),
                                                    ]}
                                                    noStyle>
                                                    <Input
                                                        placeholder="姓"
                                                        allowClear
                                                    />
                                                </Form.Item>
                                            </Col>
                                            <Col span={12}>
                                                <Form.Item
                                                    name="first_name"
                                                    noStyle>
                                                    <Input
                                                        placeholder="名"
                                                        allowClear
                                                    />
                                                </Form.Item>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Form.Item>
                                {/* <UserServiceIdFormItem
                                    {...formItemIndent1Props}
                                /> */}
                                <Form.Item
                                    {...formItemIndent1Props}
                                    label="メールアドレス"
                                    className={styles.formField}
                                    name="email">
                                    <TextArea
                                        autoSize={{ minRows: 1 }}
                                        disabled
                                        placeholder="you@example.com"
                                    />
                                </Form.Item>
                                <Form.Item
                                    {...formItemIndent1Props}
                                    label="権限"
                                    className={styles.formField}
                                    name="role">
                                    <Radio.Group disabled>
                                        {ROLES.map((role, index) => (
                                            <Radio
                                                key={index}
                                                value={role.value}>
                                                {role.title}
                                            </Radio>
                                        ))}
                                    </Radio.Group>
                                </Form.Item>
                                <TelInputFormItem
                                    {...formItemIndent1Props}
                                    validateStatus={
                                        error?.response?.data.tel
                                            ? "error"
                                            : undefined
                                    }
                                    help={error?.response?.data.tel}
                                />
                                <Form.Item shouldUpdate noStyle>
                                    {() => (
                                        <Form.Item
                                            {...formItemIndent1Props}
                                            label="パスワード"
                                            className={styles.formField}
                                            help={
                                                form.getFieldError("password")
                                                    .length
                                                    ? null
                                                    : "変更時のみ入力"
                                            }
                                            name="password"
                                            rules={[
                                                {
                                                    required: false,
                                                    pattern: PASSWORD_REGEX,
                                                    message:
                                                        ErrorMessages.validation
                                                            .regex.password,
                                                },
                                            ]}>
                                            <Input.Password
                                                placeholder="大小英数字記号混在で10-50桁"
                                                autoComplete="new-password"
                                            />
                                        </Form.Item>
                                    )}
                                </Form.Item>
                                <Form.Item
                                    {...formItemIndent1Props}
                                    label={
                                        <span>
                                            メール署名&nbsp;
                                            <Tooltip
                                                title={
                                                    <span>
                                                        ここで設定をしたメール署名は配信メール予約機能で
                                                        　　　　　　　　　　　　　　　　　　　　　　　　　　　　作成したメールに自動挿入されます。
                                                        <br />
                                                        <a
                                                            href={
                                                                Links.helps
                                                                    .scheduledEmails
                                                                    .autoInsert
                                                            }
                                                            target="_blank"
                                                            rel="noopener noreferrer">
                                                            詳細
                                                        </a>
                                                        <br />
                                                        <br />
                                                        なお、メール署名の内容はあとから変更することが可能です。
                                                    </span>
                                                }>
                                                <QuestionCircleFilled
                                                    style={{
                                                        color: iconCustomColor,
                                                    }}
                                                />
                                            </Tooltip>
                                        </span>
                                    }
                                    className={
                                        styles.formField + " validate-textarea"
                                    }
                                    name="email_signature"
                                    rules={[
                                        {
                                            max: 1000,
                                            message:
                                                ErrorMessages.validation.length
                                                    .max1000,
                                        },
                                    ]}>
                                    <TextArea
                                        autoSize={{ minRows: 16 }}
                                        data-testid="tenant-my-profile-info-email-signature"
                                    />
                                </Form.Item>
                                <Form.Item>
                                    <Row>
                                        <Col
                                            {...formItemIndent1Props.labelCol}></Col>
                                        <ValidateTextArea
                                            contentTextArea={contentTextArea}
                                            errorMessages={
                                                ErrorMessages.validation.length
                                                    .max1000
                                            }
                                            margin="0px"
                                            className="validate-textarea-error ant-col ant-col-19"
                                        />
                                    </Row>
                                </Form.Item>
                                <Col span={24}>
                                    <Row justify="center">
                                        <Form.Item shouldUpdate>
                                            {() => (
                                                <Button
                                                    type="primary"
                                                    htmlType="submit"
                                                    disabled={
                                                        !form.isFieldsTouched(
                                                            requiredFields,
                                                            true
                                                        ) ||
                                                        !!form
                                                            .getFieldsError()
                                                            .filter(
                                                                ({ errors }) =>
                                                                    errors.length
                                                            ).length
                                                    }>
                                                    個人プロフィール情報を登録する
                                                </Button>
                                            )}
                                        </Form.Item>
                                    </Row>
                                </Col>
                            </Form>
                        </Col>
                    </Row>
                </Col>
            </Spin>
        </Col>
    );
};

export default TenantMyProfileInfo;
