import React, { useState } from "react";
import { Button, Col, Row, Spin } from "antd";
import {
    PaymentCardDataModel,
    PaymentCreateModel,
} from "~/models/paymentModel";
import PaymentCard from "../../PaymentPage/PaymentCard/PaymentCard";
import PaymentEmptyCard from "../../PaymentPage/PaymentEmptyCard/PaymentEmptyCard";
import PayjpModal from "../../PaymentPage/PayjpModal/PayjpModal";
import {
    useTenantFetchPaymentAPIQuery,
    useTenantFinishAPIMutate,
    useTenantPaymentAPIMutate,
} from "~/hooks/useTenant";
import { useHistory } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import { TenantPaymentRequestModel } from "~/models/tenantModel";
import { customSuccessMessage } from "~/components/Common/AlertMessage/AlertMessage";
import { SuccessMessages } from "~/utils/constants";

type Props = {
    authToken: string;
};

const TenantPaymentInfo = ({ authToken }: Props) => {
    const router = useHistory();
    const [isPayjpModalOpen, setIsPayjpModalOpen] = useState(false);
    const { mutate: registerCardToPayjp, isLoading: isRegisterCardLoading } =
        useTenantPaymentAPIMutate();
    const { mutate: finish } = useTenantFinishAPIMutate();
    const { data: result, isLoading } = useTenantFetchPaymentAPIQuery({
        authToken,
    });
    const [paymentID, setPaymentID] = useState("");
    const [cardOrder, setCardOrder] = useState(0);

    const onPaymentTokenGenerate = (data: PaymentCreateModel) => {
        const postData: TenantPaymentRequestModel = {
            ...data,
            authToken,
        };
        registerCardToPayjp(postData, {});
    };

    const onFinishRegister = () => {
        finish(
            { authToken },
            {
                onSuccess: (result) => {
                    const data = result && result.data && result?.data;
                    if (data.is_completed) {
                        customSuccessMessage(SuccessMessages.tenant.finish);
                        router.push(Paths.login);
                    }
                },
            }
        );
    };

    const renderPaymentCard = (payment: PaymentCardDataModel, cardOrder: number) => {
        return (
            <Col span={10} key={payment.id}>
                <PaymentCard
                    payment={payment}
                    isMainCard={payment.isMainCard}
                    showSwitchDelete={false}
                    onOpenUpdateModal={() => {setIsPayjpModalOpen(true);setPaymentID(payment.id);setCardOrder(cardOrder);}}
                    onDeleteModal={() => {}} // Deleteはテナント登録では未対応
                    onUpdate={() => {}} // カード情報の更新(メインカードの切り替え)はテナント登録では未対応
                    />
            </Col>
        );
    };

    const renderMainPaymentEmptyCard = (isMainCard: boolean, cardOrder: number) => {
        return (
            <Col span={10}>
                <PaymentEmptyCard
                    isMainCard={isMainCard}
                    onOpenRegisterModal={() => {setIsPayjpModalOpen(true);setPaymentID("");setCardOrder(cardOrder);}}
                />
            </Col>
        );
    };

    return (
        <Col span={24}>
            <Spin spinning={isLoading || isRegisterCardLoading}>
                <Col span={24} style={{ marginTop: "3%" }}>
                    <Row gutter={4} justify="center">
                        {result && result.data && result?.data?.length
                            ? renderPaymentCard(result.data[0], 0)
                            : renderMainPaymentEmptyCard(true, 0)}
                    </Row>
                    <Row style={{ marginTop: "10%" }}>
                        <Col span={24}>
                            <Row justify="center">
                                <Button
                                    type="primary"
                                    disabled={!result?.data?.length}
                                    onClick={onFinishRegister}>
                                    テナント登録を完了する
                                </Button>
                            </Row>
                        </Col>
                    </Row>
                    <Row justify="center">
                        <Col>
                            {!result?.data?.length && (
                                <Button
                                    type="link"
                                    onClick={onFinishRegister}
                                    style={{ marginTop: "20%" }}>
                                    あとで登録する
                                </Button>
                            )}
                        </Col>
                    </Row>
                </Col>
            </Spin>
            <PayjpModal
                visible={isPayjpModalOpen}
                onPaymentTokenGenerate={onPaymentTokenGenerate}
                onCancel={() => setIsPayjpModalOpen(false)}
                onFinish={() => setIsPayjpModalOpen(false)}
                oldCardId={paymentID}
                cardOrder={cardOrder}
            />
        </Col>
    );
};

export default TenantPaymentInfo;
