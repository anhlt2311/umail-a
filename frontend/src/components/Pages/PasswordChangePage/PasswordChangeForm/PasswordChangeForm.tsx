import React from "react";
import { Button, Col, Form, Input, Row } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { LockOutlined } from "@ant-design/icons";
import { Endpoint } from "~/domain/api";
import validateJapaneseMessages from "~/components/Forms/validateMessages";
import { updateApi } from "~/actions/data";
import { PasswordResetFormModel } from "~/models/authModel";
import { RootState } from "~/models/store";
import { ErrorMessages, PASSWORD_REGEX } from "~/utils/constants";
import styles from "./PasswordChangeForm.scss";

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
    },
};

type Props = {
    pageId: string;
    resourceId: any;
};

const PasswordChangeForm = ({ pageId, resourceId }: Props) => {
    const [form] = Form.useForm<PasswordResetFormModel>();
    const token = useSelector((state: RootState) => state.login.token);
    const dispatch = useDispatch();

    const handleSubmit = (values: PasswordResetFormModel) => {
        const resourceURL = `${Endpoint.getBaseUrl()}/${Endpoint.passwordChange
            }`;

        dispatch(updateApi(pageId, token, resourceURL, resourceId, values));
    };
    return (
        <Col span={24} style={{ margin: "32px auto" }}>
            <Row justify="center">
                <Col span={24}>
                    <Form
                        onFinish={handleSubmit}
                        form={form}
                        validateMessages={validateJapaneseMessages}>
                        <Form.Item
                            name="password"
                            rules={[
                                { required: true },
                                {
                                    pattern: PASSWORD_REGEX,
                                    message:
                                        ErrorMessages.validation.regex.password,
                                },
                            ]}>
                            <Input.Password
                                prefix={
                                    <LockOutlined
                                        style={{ color: "rgba(0,0,0,.25)" }}
                                    />
                                }
                                placeholder="パスワード"
                            />
                        </Form.Item>
                        <Form.Item
                            name="password_confirm"
                            dependencies={['password']}
                            rules={[
                                {
                                    required: true,
                                },
                                {
                                    pattern: PASSWORD_REGEX,
                                    message:
                                        ErrorMessages.validation.regex.password,
                                },
                                ({ getFieldValue }) => ({
                                    validator: (_, value) => {
                                        const password = getFieldValue("password");
                                        if (password !== value) {
                                            return Promise.reject(
                                                new Error(
                                                    ErrorMessages.form.passwordMismatch
                                                )
                                            );
                                        }
                                        return Promise.resolve();
                                    },
                                }),
                            ]}>
                            <Input.Password
                                prefix={
                                    <LockOutlined
                                        style={{ color: "rgba(0,0,0,.25)" }}
                                    />
                                }
                                placeholder="パスワード確認"
                            />
                        </Form.Item>
                        <Form.Item shouldUpdate>
                            {() => (
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    disabled={!form.isFieldsTouched(["password", "password_confirm"], true) || !!form.getFieldsError().filter(({ errors }) => errors.length).length}
                                    className={styles.button}>
                                    パスワードを変更する
                                </Button>
                            )}
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </Col>
    );
};

export default PasswordChangeForm;
