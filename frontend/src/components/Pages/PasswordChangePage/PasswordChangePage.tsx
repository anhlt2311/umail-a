import React, { Component, useEffect } from "react";
import PropTypes from "prop-types";
import { connect, useDispatch, useSelector } from "react-redux";
import Paths from "~/components/Routes/Paths";
import PasswordChangeForm from "~/components/Pages/PasswordChangePage/PasswordChangeForm/PasswordChangeForm";
import { Endpoint } from "~/domain/api";
import { Col, Spin, message, Row } from "antd";
import { fetchApi } from "~/actions/data";
import { PASSWORD_CHANGE_PAGE } from "~/components/Pages/pageIds";
import { customErrorMessage, customInfoMessage } from "~/components/Common/AlertMessage/AlertMessage";
import { VALIDATED_TOKEN, INVALID_TOKEN } from "~/actions/actionTypes";
import styles from "./PasswordChangePage.scss";
import { useHistory, useParams } from "react-router-dom";
import { RootState } from "~/models/store";

const pageId = PASSWORD_CHANGE_PAGE;
const resourceURL = `${Endpoint.getBaseUrl()}/${Endpoint.passwordChange}`;

const PasswordChangePage = () => {
    const { id: resourceId } = useParams<{ id: any }>();
    const router = useHistory();
    const dispatch = useDispatch();
    const token = useSelector((state: RootState) => state.login.token);
    const {
        loading,
        showPage,
        message: successMessage,
        errorMessage,
    } = useSelector((state: RootState) => state.passwordChangePage);

    useEffect(() => {
        dispatch(
            fetchApi(pageId, token, resourceURL, resourceId, {
                after: VALIDATED_TOKEN,
                error: INVALID_TOKEN,
            })
        );
    }, []);

    useEffect(() => {
        if (successMessage) {
            customInfoMessage(successMessage);
            router.push(Paths.login);
        }
    }, [successMessage]);

    useEffect(() => {
        if (errorMessage) {
            customErrorMessage(errorMessage);
        }
    }, [errorMessage]);

    if (!showPage) {
        return (
            <div className={styles.container}>
                <h1>パスワードを再設定できません。</h1>
            </div>
        );
    }

    return (
        <Spin spinning={loading}>
            <Col span={24} style={{ padding: "16px 0 0 0" }}>
                <Row justify="center">
                    <Col style={{ width: "300px" }}>
                        <h1>パスワードの再設定</h1>
                        <PasswordChangeForm
                            pageId={pageId}
                            resourceId={resourceId}
                        />
                    </Col>
                </Row>
            </Col>
        </Spin>
    );
};

export default PasswordChangePage;
