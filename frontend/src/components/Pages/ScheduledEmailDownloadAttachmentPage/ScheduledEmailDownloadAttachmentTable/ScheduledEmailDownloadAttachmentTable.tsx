import React from "react";
import { Table, TableColumnsType } from "antd";
import { ScheduledEmailAttachmentModel } from "~/models/scheduledEmailModel";
import styles from "./ScheduledEmailDownloadAttachmentTable.scss";

type Props = {
    data: ScheduledEmailAttachmentModel[];
    setSelectedFiles: (value: string[]) => void;
};

const ScheduledEmailDownloadAttachmentTable = ({
    data,
    setSelectedFiles,
}: Props) => {
    const columns: TableColumnsType<ScheduledEmailAttachmentModel> = [
        {
            title: "ファイル名",
            dataIndex: "name",
            key: "name",
        },
        {
            title: "拡張子",
            dataIndex: "format",
            key: "format",
            render: (value: string, record) => {
                const name = record.name;
                return name.slice(name.lastIndexOf("."), name.length);
            },
        },
        {
            title: "ファイルサイズ",
            dataIndex: "size",
            key: "size",
            render: (value: string, record) => {
                return (parseInt(value) / 1048576).toFixed(3) + "MB";
            },
        },
    ];

    return (
        <Table
            rowSelection={{
                type: "checkbox",
                onChange: (
                    selectedRowKeys: React.Key[],
                    selectedRows: ScheduledEmailAttachmentModel[]
                ) => {
                    const selectedIds = selectedRows.map((item) => item.id);
                    setSelectedFiles(selectedIds);
                },
                fixed: "left",
                columnWidth: 35,
            }}
            dataSource={data}
            columns={columns}
            rowKey={(record) => record.id}
            pagination={false}
        />
    );
};

export default ScheduledEmailDownloadAttachmentTable;
