import React, { useState } from "react";
import { Row, Table, Col, Button, Spin } from "antd";
import { useParams } from "react-router-dom";
import moment from "moment";
import ConfirmPasswordForm from "./ConfirmPasswordForm/ConfirmPasswordForm";
import { DownloadOutlined } from "@ant-design/icons";
import NotFoundPage from "~/components/Pages/NotFoundPage";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import CustomDetailPageContent from "~/components/Common/CustomDetailPageContent/CustomDetailPageContent";
import ScheduledEmailDownloadAttachmentTable from "./ScheduledEmailDownloadAttachmentTable/ScheduledEmailDownloadAttachmentTable";
import {
    useScheduledEmailAuthorizeDownloadAPIMutation,
    useScheduledEmailFileDownloadAPIMutation,
} from "~/hooks/useScheduledEmail";
import {
    ScheduledEmailAttachmentsDownloadRequestModel,
    ScheduledEmailAttachmentAuthorizeFromModel,
} from "~/models/scheduledEmailModel";
import styles from "./ScheduledEmailDownloadAttachmentPage.scss";

const ScheduledEmailDownloadAttachmentPage = () => {
    const [selectedFiles, setSelectedFiles] = useState<string[]>([]);
    const [threeLogin, setThreeLogin] = useState(0);
    const { id } = useParams<{ id: string }>();
    const {
        mutate: authorizeDownload,
        data,
        error,
        isLoading: isAuthorizing,
    } = useScheduledEmailAuthorizeDownloadAPIMutation();
    const { mutate: downloadFiles, isLoading: isDownloading } =
        useScheduledEmailFileDownloadAPIMutation();

    const onConfirmPassword = (
        values: ScheduledEmailAttachmentAuthorizeFromModel
    ) => {
        authorizeDownload(
            { id, password: values.password },
            {
                onError: (err) => {
                    setThreeLogin(threeLogin + 1);
                },
            }
        );
    };

    const onDownload = () => {
        const postData: ScheduledEmailAttachmentsDownloadRequestModel = {
            attachment_ids: selectedFiles,
        };
        downloadFiles(postData);
    };

    if (error?.request.status === 404) {
        return <NotFoundPage />;
    }

    return (
        <Spin spinning={isAuthorizing || isDownloading}>
            <Col span={24}>
                <Row>
                    <Col span={24}>
                        <CustomPageHeader title="ファイルダウンロード " />
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        {data?.data.attachments.length ? (
                            <CustomDetailPageContent>
                                <Row
                                    align="middle"
                                    justify="center"
                                    className={styles.tableContainer}>
                                    <Col span={18}>
                                        件名: {data.data.subject}
                                    </Col>
                                    <Col span={18}>
                                        ダウンロード期限:{" "}
                                        {moment(data.data.expiredDate).format(
                                            "YYYY-MM-DD"
                                        )}
                                    </Col>
                                    <Col span={18}>
                                        <ScheduledEmailDownloadAttachmentTable
                                            data={data.data.attachments}
                                            setSelectedFiles={setSelectedFiles}
                                        />
                                        <Button
                                            size="small"
                                            type="primary"
                                            className={styles.buttonDownloadCSV}
                                            disabled={!selectedFiles.length}
                                            onClick={onDownload}
                                            icon={<DownloadOutlined />}
                                        />
                                    </Col>
                                </Row>
                            </CustomDetailPageContent>
                        ) : (
                            <ConfirmPasswordForm
                                onConfirmPassword={onConfirmPassword}
                                threeLogin={threeLogin}
                            />
                        )}
                    </Col>
                </Row>
            </Col>
        </Spin>
    );
};

export default ScheduledEmailDownloadAttachmentPage;
