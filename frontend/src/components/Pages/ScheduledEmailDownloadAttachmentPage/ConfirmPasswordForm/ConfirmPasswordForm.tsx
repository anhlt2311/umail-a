import React, { useEffect, useState } from "react";
import { Button, Col, Form, Input, Row } from "antd";
import { LockOutlined } from "@ant-design/icons";
import validateJapaneseMessages from "~/components/Forms/validateMessages";
import {
    useTenantFetchRecaptchaAPIMutation,
    useTenantFetchRecaptchaAPIQuery,
} from "~/hooks/useTenant";
import ReCAPTCHA from "react-google-recaptcha";
import styles from "./ConfirmPasswordForm.scss";
import { ScheduledEmailAttachmentAuthorizeFromModel } from "~/models/scheduledEmailModel";

const formCol = {
    xxl: 5,
    xl: 5,
    lg: 5,
    md: 5,
    sm: 7,
    xs: 7,
};

type Props = {
    onConfirmPassword: (
        values: ScheduledEmailAttachmentAuthorizeFromModel
    ) => void;
    threeLogin: number;
};

const ConfirmPasswordForm = ({ onConfirmPassword, threeLogin }: Props) => {
    const requiredFields = ["recaptcha"];
    const [form] = Form.useForm<ScheduledEmailAttachmentAuthorizeFromModel>();
    const { mutate: fetchRecaptcha } = useTenantFetchRecaptchaAPIMutation();
    const { data } = useTenantFetchRecaptchaAPIQuery({
        options: { enabled: false },
    });

    const onFinish = (values: ScheduledEmailAttachmentAuthorizeFromModel) => {
        onConfirmPassword(values);
    };

    useEffect(() => {
        if (threeLogin >= 3) {
            fetchRecaptcha();
        }
        form.setFieldsValue({
            password: "",
        });
    }, [threeLogin]);

    return (
        <Row justify="center">
            <Col
                {...formCol}
                style={{
                    marginTop: "32px",
                }}>
                <Form
                    onFinish={onFinish}
                    form={form}
                    validateMessages={validateJapaneseMessages}>
                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: "パスワードを入力してください",
                            },
                        ]}>
                        <Input.Password
                            prefix={
                                <LockOutlined
                                    style={{ color: "rgba(0,0,0,.25)" }}
                                />
                            }
                            placeholder="パスワード"
                        />
                    </Form.Item>
                    {threeLogin >= 3 && data?.sitekey && (
                        <Form.Item noStyle>
                            <Col span={24}>
                                <Row justify="center">
                                    <Col>
                                        {!data.testMode && (
                                            <Form.Item name="recaptcha" noStyle>
                                                <ReCAPTCHA
                                                    sitekey={data.sitekey}
                                                />
                                            </Form.Item>
                                        )}
                                    </Col>
                                </Row>
                            </Col>
                        </Form.Item>
                    )}
                    <Form.Item shouldUpdate>
                        {() => (
                            <Button
                                type="primary"
                                htmlType="submit"
                                className={styles.button}
                                disabled={
                                    !form.isFieldsTouched(
                                        threeLogin >= 3 && data?.sitekey
                                            ? requiredFields.filter((field) => {
                                                  if (data.testMode) {
                                                      return (
                                                          field !== "recaptcha"
                                                      );
                                                  }
                                                  return !data.testMode;
                                              })
                                            : [],
                                        true
                                    ) ||
                                    !!form
                                        .getFieldsError()
                                        .filter(({ errors }) => errors.length)
                                        .length
                                }>
                                OK
                            </Button>
                        )}
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export default ConfirmPasswordForm;
