import React from "react";
import { v4 as uuidv4 } from "uuid";
import { configureStore } from "@reduxjs/toolkit";
import { screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { renderWithAllProviders } from "~/test/utils";
import login, { LoginInitialState } from "~/reducers/login";
import SharedEmailDetailPageForwardButton from "../SharedEmailDetailPageForwardButton";
import { Form } from "antd";

const mockOnFinish = jest.fn();

const Wrapper = () => {
    const [form] = Form.useForm();

    return (
        <Form form={form} onFinish={mockOnFinish}>
            <Form.Item>
                <SharedEmailDetailPageForwardButton />
            </Form.Item>
        </Form>
    );
};

describe("SharedEmailDetailPageForwardButton.tsx", () => {
    test("enabled render test", () => {
        renderWithAllProviders(<SharedEmailDetailPageForwardButton />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: uuidv4(),
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            shared_emails: {
                                delete: true,
                                select_staff: true,
                                select_all_staff: true,
                            },
                        },
                    },
                },
            }),
        });
        const buttonElement = screen.getByRole("button", {
            name: /自社担当者にメール転送/,
        });
        expect(buttonElement).toBeInTheDocument();
        expect(buttonElement).not.toBeDisabled();
    });

    describe("disabled render test", () => {
        test("via authorization", () => {
            renderWithAllProviders(<SharedEmailDetailPageForwardButton />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            token: uuidv4(),
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                shared_emails: {
                                    delete: true,
                                    select_staff: false,
                                    select_all_staff: true,
                                },
                            },
                        },
                    },
                }),
            });
            const buttonElement = screen.getByRole("button", {
                name: /自社担当者にメール転送/,
            });
            expect(buttonElement).toBeInTheDocument();
            expect(buttonElement).toBeDisabled();
        });

        test("via props", () => {
            renderWithAllProviders(
                <SharedEmailDetailPageForwardButton disabled />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                token: uuidv4(),
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    shared_emails: {
                                        delete: true,
                                        select_staff: true,
                                        select_all_staff: true,
                                    },
                                },
                            },
                        },
                    }),
                }
            );
            const buttonElement = screen.getByRole("button", {
                name: /自社担当者にメール転送/,
            });
            expect(buttonElement).toBeInTheDocument();
            expect(buttonElement).toBeDisabled();
        });

        test("via both", () => {
            renderWithAllProviders(
                <SharedEmailDetailPageForwardButton disabled />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                token: uuidv4(),
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    shared_emails: {
                                        delete: true,
                                        select_staff: false,
                                        select_all_staff: true,
                                    },
                                },
                            },
                        },
                    }),
                }
            );
            const buttonElement = screen.getByRole("button", {
                name: /自社担当者にメール転送/,
            });
            expect(buttonElement).toBeInTheDocument();
            expect(buttonElement).toBeDisabled();
        });
    });

    test("can submit", async () => {
        renderWithAllProviders(<Wrapper />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: uuidv4(),
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            shared_emails: {
                                delete: true,
                                select_staff: true,
                                select_all_staff: true,
                            },
                        },
                    },
                },
            }),
        });
        const buttonElement = screen.getByRole("button", {
            name: /自社担当者にメール転送/,
        });
        await userEvent.click(buttonElement);
        expect(mockOnFinish).toHaveBeenCalled();
    });
});
