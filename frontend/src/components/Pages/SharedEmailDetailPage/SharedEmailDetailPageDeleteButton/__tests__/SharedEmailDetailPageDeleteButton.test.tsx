import React from "react";
import { v4 as uuidv4 } from "uuid";
import { configureStore } from "@reduxjs/toolkit";
import { screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { renderWithAllProviders } from "~/test/utils";
import SharedEmailDetailPageDeleteButton from "../SharedEmailDetailPageDeleteButton";
import login, { LoginInitialState } from "~/reducers/login";

const mockOnDelete = jest.fn();

describe("SharedEmailDetailPageDeleteButton.tsx", () => {
    test("enabled render test", () => {
        renderWithAllProviders(
            <SharedEmailDetailPageDeleteButton onDelete={mockOnDelete} />,
            {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            token: uuidv4(),
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                shared_emails: {
                                    delete: true,
                                    select_staff: true,
                                    select_all_staff: true,
                                },
                            },
                        },
                    },
                }),
            }
        );
        const buttonElement = screen.getByRole("button", { name: /削 除/ });
        expect(buttonElement).toBeInTheDocument();
        expect(buttonElement).not.toBeDisabled();
    });

    describe("disabled render test", () => {
        test("select_staff is false", () => {
            renderWithAllProviders(
                <SharedEmailDetailPageDeleteButton onDelete={mockOnDelete} />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                token: uuidv4(),
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    shared_emails: {
                                        delete: true,
                                        select_staff: false,
                                        select_all_staff: true,
                                    },
                                },
                            },
                        },
                    }),
                }
            );
            const buttonElement = screen.queryByRole("button", {
                name: /削 除/,
            });
            expect(buttonElement).not.toBeInTheDocument();
        });

        test("select_all_staff is false", () => {
            renderWithAllProviders(
                <SharedEmailDetailPageDeleteButton onDelete={mockOnDelete} />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                token: uuidv4(),
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    shared_emails: {
                                        delete: true,
                                        select_staff: true,
                                        select_all_staff: false,
                                    },
                                },
                            },
                        },
                    }),
                }
            );
            const buttonElement = screen.queryByRole("button", {
                name: /削 除/,
            });
            expect(buttonElement).not.toBeInTheDocument();
        });

        test("select_staff && select_all_staff is false", () => {
            renderWithAllProviders(
                <SharedEmailDetailPageDeleteButton onDelete={mockOnDelete} />,
                {
                    store: configureStore({
                        reducer: {
                            login,
                        },
                        preloadedState: {
                            login: {
                                ...LoginInitialState,
                                token: uuidv4(),
                                authorizedActions: {
                                    ...LoginInitialState.authorizedActions,
                                    shared_emails: {
                                        delete: true,
                                        select_staff: false,
                                        select_all_staff: false,
                                    },
                                },
                            },
                        },
                    }),
                }
            );
            const buttonElement = screen.queryByRole("button", {
                name: /削 除/,
            });
            expect(buttonElement).not.toBeInTheDocument();
        });
    });

    test("deleteAuthorized is false", async () => {
        renderWithAllProviders(
            <SharedEmailDetailPageDeleteButton onDelete={mockOnDelete} />,
            {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            token: uuidv4(),
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                shared_emails: {
                                    delete: false,
                                    select_staff: true,
                                    select_all_staff: true,
                                },
                            },
                        },
                    },
                }),
            }
        );
        const buttonElement = screen.getByRole("button", { name: /削 除/ });
        await userEvent.hover(buttonElement);
        const tooltipElement = await screen.findByText(
            /特定の権限で操作できます/
        );
        expect(tooltipElement).toBeInTheDocument();
    });

    test("button can be pressed", async () => {
        renderWithAllProviders(
            <SharedEmailDetailPageDeleteButton onDelete={mockOnDelete} />,
            {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            token: uuidv4(),
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                shared_emails: {
                                    delete: true,
                                    select_staff: true,
                                    select_all_staff: true,
                                },
                            },
                        },
                    },
                }),
            }
        );
        const buttonElement = screen.getByRole("button", { name: /削 除/ });
        await userEvent.click(buttonElement);
        expect(mockOnDelete).toHaveBeenCalled();
    });
});
