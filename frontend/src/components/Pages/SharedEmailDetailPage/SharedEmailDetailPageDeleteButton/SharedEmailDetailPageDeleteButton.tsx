import React from "react";
import { Button, Tooltip } from "antd";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages } from "~/utils/constants";
import styles from "./SharedEmailDetailPageDeleteButton.scss";

type Props = {
    onDelete: () => void;
};

const SharedEmailDetailPageDeleteButton = ({ onDelete }: Props) => {
    const {
        delete: deleteAuthorized,
        select_staff: selectStaffAuthorized,
        select_all_staff: selectAllStaffAuthorized,
    } = useAuthorizedActions("shared_emails");

    const renderButton = () => {
        return (
            <Button
                type="primary"
                danger
                onClick={onDelete}
                htmlType="button"
                hidden={!selectStaffAuthorized || !selectAllStaffAuthorized}>
                削除
            </Button>
        );
    };

    if (!deleteAuthorized) {
        return (
            <Tooltip title={ErrorMessages.isNotAuthorized}>
                {renderButton()}
            </Tooltip>
        );
    }

    return renderButton();
};

export default SharedEmailDetailPageDeleteButton;
