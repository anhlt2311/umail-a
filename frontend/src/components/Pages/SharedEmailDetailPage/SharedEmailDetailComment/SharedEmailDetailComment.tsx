import React from "react";
import { Col, Row, Typography } from "antd";
import { useParams } from "react-router-dom";
import CommentListCreator from "~/components/DataDisplay/CommentList/CommentList";
import { Endpoint } from "~/domain/api";
import { SHARED_EMAIL_DETAIL_PAGE } from "../../pageIds";
import styles from "./SharedEmailDetailComment.scss";

const pageId = SHARED_EMAIL_DETAIL_PAGE;
const baseURL = Endpoint.getBaseUrl();
const resourceURL = `${baseURL}/${Endpoint.sharedEmails}`;
const commentsReducerName = "sharedEmailDetailPageComments";
const newCommentsReducerName = "sharedEmailDetailPageNewComments";
const editCommentsReducerName = "sharedEmailDetailPageEditComments";
const replyCommentReducerName = "sharedEmailDetailPageReplyComments";
const commentTemplateUrl = `${Endpoint.getBaseUrl()}/${
    Endpoint.commentTemplateSharedEmail
}`;

const CommentList = CommentListCreator(
    pageId,
    commentsReducerName,
    commentTemplateUrl,
    newCommentsReducerName,
    editCommentsReducerName,
    replyCommentReducerName
);

const { Title } = Typography;

const commentsLayout = {
    xs: 24,
    sm: 24,
    md: 21,
    lg: 18,
    xl: 15,
    xxl: 10,
};

const SharedEmailDetailComment = () => {
    const { id } = useParams<{ id: string }>();

    return (
        <Col {...commentsLayout} style={{ marginTop: "5%" }}>
            <Row justify="start">
                <Title level={5}>コメント一覧</Title>
            </Row>
            <CommentList
                resourceUrl={`${resourceURL}/${id}/${Endpoint.commentUrlSuffix}`}
            />
        </Col>
    );
};

export default SharedEmailDetailComment;
