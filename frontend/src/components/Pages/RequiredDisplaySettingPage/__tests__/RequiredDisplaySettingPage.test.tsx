import React from "react";
import { renderWithAllProviders, screen } from "~/test/utils";
import RequiredDisplaySettingPage from "../../RequiredDisplaySettingPage";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { displaySettingPage } from "~/reducers/pages";
import { defaultInitialStateOfUndeletableResource as EditPageUndeletableInitialState } from "~/reducers/Factories/editPage";
import { generateRandomToken } from "~/utils/utils";
import { mockDisplaySettingAPIData } from "~/test/mock/displaySettingAPIMock";
import { convertDisplaySettingAPIModelToDisplaySetting } from "~/hooks/useDisplaySetting";

describe("RequiredDisplaySettingPage.jsx", () => {
    test("render test", () => {
        // NOTE(joshua-hashimoto): Reduxのミューテーション違反でcontent_hashをpreloadedStateに渡せていない。これは直したいが、今のredux/componentの構成では無理。リファクタリングが待たれる。
        const data = convertDisplaySettingAPIModelToDisplaySetting(
            mockDisplaySettingAPIData
        );
        renderWithAllProviders(
            <RequiredDisplaySettingPage
                match={{ params: { id: generateRandomToken() } }}
            />,
            {
                store: configureStore({
                    reducer: {
                        login,
                        displaySettingPage,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                display_settings: {
                                    _all: true,
                                },
                            },
                        },
                        displaySettingPage: {
                            ...EditPageUndeletableInitialState,
                            data: {
                                company: data.company,
                                modified_time: data.modified_time,
                                modified_user: data.modified_user,
                            },
                        },
                    },
                }),
            }
        );
        const pageTitleElement = screen.getByText(/必須項目設定/);
        expect(pageTitleElement).toBeInTheDocument();
        const modifiedTimeLabelElement = screen.getByText(/更新日時:/);
        expect(modifiedTimeLabelElement).toBeInTheDocument();
        const modifiedTimeElement = screen.getByText(
            new RegExp(data.modified_time)
        );
        expect(modifiedTimeElement).toBeInTheDocument();
        // NOTE(joshua-hashimoto): Form側の簡易存在確認
        const organizationTransferLabelElement = screen.getByText(/取引先情報/);
        expect(organizationTransferLabelElement).toBeInTheDocument();
        const contactTransferLabelElement =
            screen.getByText(/取引先担当者情報/);
        expect(contactTransferLabelElement).toBeInTheDocument();
        const backButtonElement = screen.getByRole("button", { name: /戻る/ });
        expect(backButtonElement).toBeInTheDocument();
        const updateButtonElement = screen.getByRole("button", {
            name: /更 新/,
        });
        expect(updateButtonElement).toBeInTheDocument();
    });
    test("render test without authorization", async () => {
        renderWithAllProviders(
            <RequiredDisplaySettingPage
                match={{ params: { id: generateRandomToken() } }}
            />,
            {
                store: configureStore({
                    reducer: {
                        login,
                        displaySettingPage,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            role: "master",
                        },
                        displaySettingPage: {
                            ...EditPageUndeletableInitialState,
                            isDisplaySettingLoading: false,
                        },
                    },
                }),
            }
        );
        const notFoundElement = screen.getByText(/^ページが見つかりません$/);
        expect(notFoundElement).toBeInTheDocument();
        const notFoundSubtextElement = screen.getByText(
            /^ご指定のURLに対応するページが見つかりませんでした。$/
        );
        expect(notFoundSubtextElement).toBeInTheDocument();
        const backButtonElement = screen.getByRole("button", { name: /戻る/ });
        expect(backButtonElement).toBeInTheDocument();
        expect(backButtonElement).not.toBeDisabled();
    });
});
