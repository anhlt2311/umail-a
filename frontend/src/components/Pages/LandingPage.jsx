import React from 'react';
import { Icon, Result } from 'antd';
import { SmileTwoTone } from '@ant-design/icons'

const LandingPage = () => (
  <Result
    status="info"
    icon={<SmileTwoTone />}
    title="コモレビ へ ようこそ"

    subTitle="左のメニューより、機能を選択してください。まずは個人プロフィールを設定してください。"
  />
);

export default LandingPage;
