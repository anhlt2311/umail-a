import React from "react";
import PropTypes from "prop-types";
import { Typography } from "antd";
import styles from "./EmailBody.scss";

const { Text } = Typography;

type Props = {
    text: string;
    text_format: string;
    mark: boolean;
    file_type?: number,
    resourceId?: string,
    password?: string
};

const MailBody = ({ text = "", text_format = "", mark = false }: Props) => {
    const lines = text.split("\n").map(item => item.replace("\n", "\r"));

    if (text_format == 'html') {
        const preview_lines = lines.map((line, index) => {
            if (index === 2 || index === 3) {
                return ['<span class="ant-typography"><mark>', line, '</mark></span>'].join('')
            } else {
                return line
            }
        })

        return (
          <div className={styles.container}>
            <span dangerouslySetInnerHTML={{__html: preview_lines.join("\n")}} />
          </div>
        );
    } else {
      return (
        <div className={styles.container}>
            {lines.map((line, index) => (
                <React.Fragment key={index}>
                    {mark && (index === 0 || index === 1) ? (
                        <Text mark>{line}</Text>
                    ) : (
                        <Text>{line}</Text>
                    )}
                    <br />
                </React.Fragment>
            ))}
        </div>
      );
    }
};

export default MailBody;
