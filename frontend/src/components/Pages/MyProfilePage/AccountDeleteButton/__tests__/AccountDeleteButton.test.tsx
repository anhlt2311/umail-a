import React from "react";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import {
    mockHistoryPush,
    renderHook,
    renderWithAllProviders,
    screen,
    userEvent,
    waitForElementToBeRemoved,
} from "~/test/utils";
import AccountDeleteButton from "../AccountDeleteButton";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";
import { ErrorMessages, SuccessMessages } from "~/utils/constants";
import { MemoryRouter } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import { mockServer } from "~/test/setupTests";
import { mockDeleteAccountFailsWithBadRequest } from "~/test/mock/accountAPIMock";

describe("AccountDeleteButton.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("enabled render test", () => {
        renderWithAllProviders(<AccountDeleteButton />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            account: {
                                delete: true,
                            },
                        },
                    },
                },
            }),
        });
        const buttonElement = screen.getByRole("button", { name: /退会/ });
        expect(buttonElement).toBeInTheDocument();
        expect(buttonElement).not.toBeDisabled();
    });

    test("disabled render test", () => {
        renderWithAllProviders(<AccountDeleteButton />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            account: {
                                delete: false,
                            },
                        },
                    },
                },
            }),
        });
        const buttonElement = screen.getByRole("button", { name: /退会/ });
        expect(buttonElement).toBeInTheDocument();
        expect(buttonElement).toBeDisabled();
    });

    test("render confirm modal test", async () => {
        renderWithAllProviders(<AccountDeleteButton />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            account: {
                                delete: true,
                            },
                        },
                    },
                },
            }),
        });
        const buttonElement = screen.getByRole("button", { name: /退会/ });
        await userEvent.click(buttonElement);
        const modalTitleElement = await screen.findByText(/本当に退会しますか/);
        expect(modalTitleElement).toBeInTheDocument();
        const modalContentElement = await screen.findByText(
            /「はい」を押すと、退会が確定いたします。/
        );
        expect(modalContentElement).toBeInTheDocument();
        const modalOkButtonElement = await screen.findByRole("button", {
            name: /はい/,
        });
        expect(modalOkButtonElement).toBeInTheDocument();
        const modalCancelButtonElement = await screen.findByRole("button", {
            name: /キャンセル/,
        });
        expect(modalCancelButtonElement).toBeInTheDocument();
    });

    test("account delete api can be called", async () => {
        renderWithAllProviders(
            <MemoryRouter>
                <AccountDeleteButton />
            </MemoryRouter>,
            {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                account: {
                                    delete: true,
                                },
                            },
                        },
                    },
                }),
            }
        );
        const buttonElement = screen.getByRole("button", { name: /退会/ });
        await userEvent.click(buttonElement);
        const modalOkButtonElement = await screen.findByRole("button", {
            name: /はい/,
        });
        await userEvent.click(modalOkButtonElement);
        // NOTE(joshua-hashimoto): 退会成功メッセージの確認
        const successMessageRegex = new RegExp(SuccessMessages.account.disable);
        const accountDeleteSuccessMessageElement = await screen.findByText(
            successMessageRegex
        );
        expect(accountDeleteSuccessMessageElement).toBeInTheDocument();
    });

    test("test account delete fail", async () => {
        mockServer.use(mockDeleteAccountFailsWithBadRequest);
        renderWithAllProviders(<AccountDeleteButton />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            account: {
                                delete: true,
                            },
                        },
                    },
                },
            }),
        });
        const buttonElement = screen.getByRole("button", { name: /退会/ });
        await userEvent.click(buttonElement);
        const modalOkButtonElement = await screen.findByRole("button", {
            name: /はい/,
        });
        await userEvent.click(modalOkButtonElement);
        // NOTE(joshua-hashimoto): 退会失敗メッセージの確認
        const accountDeleteSuccessMessageElement = await screen.findByText(
            new RegExp(ErrorMessages.account.disable)
        );
        expect(accountDeleteSuccessMessageElement).toBeInTheDocument();
    });
});
