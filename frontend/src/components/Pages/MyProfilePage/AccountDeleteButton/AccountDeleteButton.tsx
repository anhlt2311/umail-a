import React from "react";
import { Button } from "antd";
import { useAccountDeleteAPIMutation } from "~/hooks/useAccount";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import styles from "./AccountDeleteButton.scss";

type Props = {};

const AccountDeleteButton = ({}: Props) => {
    const accountAuthorizedActions = useAuthorizedActions("account");
    const { mutate: deleteAccount } = useAccountDeleteAPIMutation();

    const onClick = () => {
        confirmModal({
            title: "本当に退会しますか？",
            content: "「はい」を押すと、退会が確定いたします。",
            okText: "はい",
            onOk: () => {
                deleteAccount();
            },
        });
    };

    return (
        <Button
            type="link"
            onClick={onClick}
            disabled={!accountAuthorizedActions.delete}>
            退会
        </Button>
    );
};

export default AccountDeleteButton;
