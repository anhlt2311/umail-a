import createSearchPage from './Factories/createSearchPage';
import { SHARED_EMAIL_NOTIFICATION_SEARCH_PAGE } from './pageIds';
import Paths from '../Routes/Paths';
import SharedEmailNotificationsTable from '../Tables/SharedEmailNotificationsTable';
import SharedEmailNotificationSearchForm from '../Forms/SharedEmailNotificationSearchForm/SharedEmailNotificationSearchForm'
import { Endpoint } from '../../domain/api';
import { convertSharedEmailNotificationResponseDataEntry, sharedEmailNotificationSearchParamToAPI } from '../../domain/data';
import SharedEmailNotificationAddButton from '../Forms/SharedEmailNotificationAddButton/SharedEmailNotificationAddButton';

const pageId = SHARED_EMAIL_NOTIFICATION_SEARCH_PAGE;
const resourceURL = `${Endpoint.getBaseUrl()}/${Endpoint.sharedEmailNotifications}`;
const linkToRegisterPage = Paths.sharedMailNotificationRegister;

const resourceName = 'notification_rules'
const accessAuthorized = (authorizedActions) => { return authorizedActions && authorizedActions[resourceName] && authorizedActions[resourceName]['_all'] }

const SharedEmailNotificationSearchPage = createSearchPage(
  pageId,
  'sharedEmailNotificationSearchPage',
  '共有メール通知条件 一覧',
  SharedEmailNotificationSearchForm,
  SharedEmailNotificationsTable,
  resourceURL,
  convertSharedEmailNotificationResponseDataEntry,
  sharedEmailNotificationSearchParamToAPI,
  false,
  undefined,
  undefined,
  true,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  undefined,
  accessAuthorized,
  undefined,
  undefined,
  undefined,
  false,
  [SharedEmailNotificationAddButton],
);

export default SharedEmailNotificationSearchPage;
