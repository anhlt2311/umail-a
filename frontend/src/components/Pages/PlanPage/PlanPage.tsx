import React from "react";
import { Col, Row, Spin } from "antd";
import { useFetchUserPlanInfoAPIQuery } from "~/hooks/usePlan";
import PlanCurrentUserInfo from "./PlanCurrentUserInfo/PlanCurrentUserInfo";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import { planCards } from "~/components/Pages/PlanPage/data";
import PlanSummaryCard from "./PlanSummaryCard/PlanSummaryCard";
import PlanFunctionTable from "./PlanFunctionTable/PlanFunctionTable";
import styles from "./PlanPage.scss";

const PlanPage = () => {
    const { isLoading } = useFetchUserPlanInfoAPIQuery({});

    return (
        <Spin spinning={isLoading}>
            <Col span={24}>
                <Row>
                    <CustomPageHeader title="ご利用プラン" />
                </Row>
                <Row>
                    <PlanCurrentUserInfo />
                </Row>
                <Row className={styles.planCardsContainer}>
                    {planCards.map((planCard) => (
                        <Col span={8} key={planCard.planId}>
                            <PlanSummaryCard plan={planCard} />
                        </Col>
                    ))}
                </Row>
                <Row className={styles.planFunctionTableContainer}>
                    <PlanFunctionTable />
                </Row>
            </Col>
        </Spin>
    );
};

export default PlanPage;
