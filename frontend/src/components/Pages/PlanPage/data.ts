import { PlanCardModel, PlanTableRowModel } from "~/models/planModel";

export const planCards: PlanCardModel[] = [
    {
        planId: 1,
        planTitle: "ライト",
        defaultCount: 5,
        monthlyPrice: 30000,
        planSummary: [
            "初期費用無料",
            "5ユーザーまで無料",
            "ユーザー追加：月額3,000円(税抜)",
            "サポート利用無制限",
        ],
        aimedTo: [
            "顧客管理をオンライン上で行いたい",
            "メール配信を効率化したい",
        ],
        functions: [
            "ダッシュボード機能",
            "取引先管理機能",
            "取引先担当者管理機能",
            "配信メール管理機能",
            "アドオン機能",
        ],
    },
    {
        planId: 2,
        planTitle: "スタンダード",
        defaultCount: 10,
        monthlyPrice: 50000,
        planSummary: [
            "初期費用無料",
            "10ユーザーまで無料",
            "ユーザー追加：月額5,000円(税抜)",
            "サポート利用無制限",
        ],
        aimedTo: [
            "インバウンドセールスを強化したい",
            "要員管理、案件管理を一元化したい",
        ],
        functions: [
            "ダッシュボード機能",
            "取引先管理機能",
            "取引先担当者管理機能",
            "配信メール管理機能",
            "アドオン機能",
            "その他、Coming soon...",
        ],
    },
    {
        planId: 3,
        planTitle: "プロフェッショナル",
        defaultCount: 20,
        monthlyPrice: 100000,
        planSummary: [
            "初期費用無料",
            "20ユーザーまで無料",
            "ユーザー追加：月額10,000円(税抜)",
            "サポート利用無制限",
        ],
        aimedTo: [
            "営業活動を数字で管理、効率化したい",
            "より戦略的な営業施策を実施したい",
        ],
        functions: [
            "ダッシュボード機能",
            "取引先管理機能",
            "取引先担当者管理機能",
            "配信メール管理機能",
            "アドオン機能",
            "その他、Coming soon...",
        ],
    },
];

export const planTableRows: PlanTableRowModel[] = [
    {
        functionName: "ダッシュボード",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先一覧／検索",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先情報",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先支店情報",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先取引条件",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先コメント",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先CSVダウンロード",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先CSVアップロード",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先担当者一覧／検索",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先担当者情報",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先担当者配信条件",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先担当者コメント",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先担当者CSVダウンロード",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "取引先担当者CSVアップロード",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "配信メール一覧／検索",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "配信メール予約\n(TO配信)",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "配信メール宛先検索",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "テキスト／HTML配信",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "配信宛先最大1000件\n(アドオン最大5000件)",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "配信添付容量最大2MB\n(アドオン最大10MB)",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "配信データ保持期間最大半年\n(アドオン上限緩和)",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "検索条件テンプレート",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "コメントテンプレート",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "自社情報",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "自社取引条件",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "アドオン",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "ユーザー設定",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "必須項目設定",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "タグ設定",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "配信メール設定",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "個人プロフィール",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "クレジットカード決済",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "お支払い履歴",
        isLightPlanAvailable: "⚪︎",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
    {
        functionName: "その他、Coming soon...",
        isLightPlanAvailable: "-",
        isStandardPlanAvailable: "⚪︎",
        isProfessionalPlanAvailable: "⚪︎",
    },
];
