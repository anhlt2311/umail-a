import React from "react";
import { Col, Row, Table, Typography } from "antd";
import { ColumnsType } from "antd/lib/table";
import { PlanTableRowModel } from "~/models/planModel";
import { DISABLED_COLOR } from "~/utils/constants";
import { planTableRows } from "../data";
import styles from "./PlanFunctionTable.scss";

const { Text } = Typography;

const PlanFunctionTable = () => {
    const columns: ColumnsType<PlanTableRowModel> = [
        {
            title: "機能",
            dataIndex: "functionName",
            key: "functionName",
            render: (value: string, record, index) => {
                return (
                    <Col span={24}>
                        <Row justify="start">
                            <Col>
                                {value.includes("\n") ? (
                                    value.split("\n").map((val, index) => (
                                        <Text
                                            key={index}
                                            style={{
                                                fontSize: 14,
                                                width: "100%",
                                                display: "block",
                                            }}>
                                            {val}
                                        </Text>
                                    ))
                                ) : (
                                    <Text style={{ fontSize: 14 }}>
                                        {value}
                                    </Text>
                                )}
                            </Col>
                        </Row>
                    </Col>
                );
            },
            width: "25%",
        },
        {
            title: "ライト",
            dataIndex: "isLightPlanAvailable",
            key: "isLightPlanAvailable",
            render: (value, record, index) => {
                return <Text style={{ fontSize: 14 }}>{value}</Text>;
            },
            align: "center",
            width: "25%",
        },
        {
            title: <Text style={{ color: DISABLED_COLOR }}>スタンダード</Text>,
            dataIndex: "isStandardPlanAvailable",
            key: "isStandardPlanAvailable",
            render: (value, record, index) => {
                return (
                    <Text style={{ color: DISABLED_COLOR, fontSize: 14 }}>
                        {value}
                    </Text>
                );
            },
            align: "center",
            width: "25%",
        },
        {
            title: (
                <Text style={{ color: DISABLED_COLOR }}>
                    プロフェッショナル
                </Text>
            ),
            dataIndex: "isProfessionalPlanAvailable",
            key: "isProfessionalPlanAvailable",
            render: (value, record, index) => {
                return (
                    <Text style={{ color: DISABLED_COLOR, fontSize: 14 }}>
                        {value}
                    </Text>
                );
            },
            align: "center",
            width: "25%",
        },
    ];

    return (
        <Col span={24}>
            <Row justify="center" style={{ marginBottom: "2%" }}>
                <Col>
                    <Text
                        style={{
                            fontFamily: "Avenir",
                            fontStyle: "normal",
                            fontWeight: 500,
                            fontSize: 24,
                        }}>
                        機能比較
                    </Text>
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <Table
                        className="tableRowDisableHover"
                        rowKey={(record) => record.functionName}
                        dataSource={planTableRows}
                        columns={columns}
                        tableLayout="auto"
                        pagination={false}
                        style={{ margin: "0 5%" }}
                    />
                </Col>
            </Row>
        </Col>
    );
};

export default PlanFunctionTable;
