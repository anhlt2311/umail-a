import React from "react";
import { Button, Tooltip } from "antd";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import {
    useFetchUserPlanInfoAPIQuery,
    usePlanRemoveAccountLimitAPIMutation,
} from "~/hooks/usePlan";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { ErrorMessages, TooltipMessages } from "~/utils/constants";
import styles from "./PlanDeleteUserButton.scss";

const PlanDeleteUserButton = () => {
    const { user_delete: planUserDeleteAuthorization } =
        useAuthorizedActions("plan");
    const {
        mutate: removeAccountLimit,
        isLoading: isRemoveAccountLimitLoading,
    } = usePlanRemoveAccountLimitAPIMutation();
    const { data, isLoading } = useFetchUserPlanInfoAPIQuery({});

    if (isLoading || !data) {
        return <span data-testid="loading"></span>;
    }

    const userNumber = data.currentUserCount;
    const userMaxNumber = data.userRegistrationLimit;
    const planMasterId = data.planMasterId;
    const isDeletable = userNumber < userMaxNumber;
    const isDefaultCount = userMaxNumber - data.defaultUserCount <= 0;

    const onTryDelete = () => {
        confirmModal({
            title: "ユーザーを削除しますか？",
            content: "ユーザーの上限数を-1いたします。",
            okText: "ユーザーを削除",
            onOk: () => {
                removeAccountLimit();
            },
        });
    };

    const renderButton = () => {
        let tooltipMessage = null;
        if (!isDeletable) {
            tooltipMessage = TooltipMessages.plan.removeUser;
        }
        if (!planUserDeleteAuthorization) {
            tooltipMessage = ErrorMessages.isNotAuthorized;
        }

        return (
            <Tooltip title={tooltipMessage}>
                <Button
                    onClick={onTryDelete}
                    disabled={
                        !planUserDeleteAuthorization ||
                        !isDeletable ||
                        !planMasterId ||
                        isRemoveAccountLimitLoading ||
                        isDefaultCount
                    }
                    size="small"
                    danger
                    type="primary">
                    上限を削除
                </Button>
            </Tooltip>
        );
    };

    return renderButton();
};

export default PlanDeleteUserButton;
