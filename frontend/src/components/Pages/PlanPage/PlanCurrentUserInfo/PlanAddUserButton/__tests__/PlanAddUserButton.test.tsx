import React from "react";
import { configureStore } from "@reduxjs/toolkit";
import {
    renderHook,
    renderWithAllProviders,
    screen,
    userEvent,
} from "~/test/utils";
import login, { LoginInitialState } from "~/reducers/login";
import PlanAddUserButton from "../PlanAddUserButton";
import {
    AuthorizedActionsModel,
    PlanAuthorizedActionsModel,
} from "~/models/authModel";
import { ErrorMessages, SuccessMessages } from "~/utils/constants";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";
import { mockServer } from "~/test/setupTests";
import { mockPlanAddAccountLimitFailureAPIRoute } from "~/test/mock/planAPIMock";

describe("PlanAddUserButton.tsx", () => {
    const planAuthorizedActions: PlanAuthorizedActionsModel = {
        _all: true,
        purchase: true,
        user_add: true,
        user_delete: true,
    };

    const planUnauthorizedActions: PlanAuthorizedActionsModel = {
        _all: false,
        purchase: false,
        user_add: false,
        user_delete: false,
    };

    const authorizedActions: AuthorizedActionsModel = {
        ...LoginInitialState.authorizedActions,
        plan: planAuthorizedActions,
    };

    const unauthorizedActions: AuthorizedActionsModel = {
        ...LoginInitialState.authorizedActions,
        plan: planUnauthorizedActions,
    };

    const preloadedState = {
        login: {
            ...LoginInitialState,
            authorizedActions,
        },
    };

    const unauthorizedPreloadedState = {
        login: {
            ...LoginInitialState,
            authorizedActions: unauthorizedActions,
        },
    };

    test("render test", () => {
        renderWithAllProviders(<PlanAddUserButton />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    ...preloadedState,
                },
            }),
        });
        const buttonElement = screen.getByRole("button", {
            name: /上限を追加/,
        });
        expect(buttonElement).toBeInTheDocument();
        expect(buttonElement).not.toBeDisabled();
    });

    test("render test without authorization", async () => {
        renderWithAllProviders(<PlanAddUserButton />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    ...unauthorizedPreloadedState,
                },
            }),
        });
        const buttonElement = screen.getByRole("button", {
            name: /上限を追加/,
        });
        expect(buttonElement).toBeInTheDocument();
        expect(buttonElement).toBeDisabled();
    });

    test("tooltip does not show when user has authorization", () => {
        const { container } = renderWithAllProviders(<PlanAddUserButton />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    ...preloadedState,
                },
            }),
        });
        const tooltipContainerElement = container.querySelector(
            ".ant-tooltip-disabled-compatible-wrapper"
        );
        expect(tooltipContainerElement).not.toBeInTheDocument();
    });

    test("tooltip shows when authorization is false", async () => {
        const { container } = renderWithAllProviders(<PlanAddUserButton />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    ...unauthorizedPreloadedState,
                },
            }),
        });
        const tooltipContainerElement = container.querySelector(
            ".ant-tooltip-disabled-compatible-wrapper"
        );
        await userEvent.hover(tooltipContainerElement!);
        const tooltipMessage = ErrorMessages.isNotAuthorized;
        const messageRegex = new RegExp(tooltipMessage);
        const tooltipElement = await screen.findByText(messageRegex);
        expect(tooltipElement).toBeInTheDocument();
    });

    test("add button click shows add modal", async () => {
        renderWithAllProviders(<PlanAddUserButton />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    ...preloadedState,
                },
            }),
        });
        const buttonElement = screen.getByRole("button", {
            name: /上限を追加/,
        });
        await userEvent.click(buttonElement);
        const modalTitleElement = await screen.findByText(
            /ユーザーを追加しますか？/
        );
        expect(modalTitleElement).toBeInTheDocument();
        const modalContentElement = await screen.findByText(
            /ユーザーの上限数を\+1いたします。追加料金がかかります。/
        );
        expect(modalContentElement).toBeInTheDocument();
        const modalOkButtonElement = await screen.findByText(
            /^ユーザーを追加$/
        );
        expect(modalOkButtonElement).toBeInTheDocument();
        expect(modalOkButtonElement).not.toBeDisabled();
    });

    describe("test add user action", () => {
        beforeAll(async () => {
            const { result, waitFor } = renderHook(() => useClient());
            await waitFor(() =>
                result.current.setTokenToClient(generateRandomToken())
            );
        });

        afterAll(async () => {
            const { result, waitFor } = renderHook(() => useClient());
            await waitFor(() => result.current.removeTokenFromClient());
        });

        test("success", async () => {
            renderWithAllProviders(<PlanAddUserButton />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            const buttonElement = screen.getByRole("button", {
                name: /上限を追加/,
            });
            await userEvent.click(buttonElement);
            const okButtonElement = await screen.findByText(/^ユーザーを追加$/);
            await userEvent.click(okButtonElement);
            const messageText = SuccessMessages.plan.addUser;
            const messageRegex = new RegExp(messageText);
            const messageElement = await screen.findByText(messageRegex);
            expect(messageElement).toBeInTheDocument();
        });

        test("error", async () => {
            mockServer.use(mockPlanAddAccountLimitFailureAPIRoute);
            renderWithAllProviders(<PlanAddUserButton />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            const buttonElement = screen.getByRole("button", {
                name: /上限を追加/,
            });
            await userEvent.click(buttonElement);
            const okButtonElement = await screen.findByText(/^ユーザーを追加$/);
            await userEvent.click(okButtonElement);
            const messageText = ErrorMessages.plan.addUser;
            const messageRegex = new RegExp(messageText);
            const messageElement = await screen.findByText(messageRegex);
            expect(messageElement).toBeInTheDocument();
        });
    });
});
