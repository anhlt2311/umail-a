import React from "react";
import { screen, render } from "~/test/utils";
import PlanCurrentPlanInfo from "../PlanCurrentPlanInfo";

describe("PlanCurrentPlanInfo.tsx", () => {
    test("render test", () => {
        render(<PlanCurrentPlanInfo planTitle="ライト" />);
        const titleElement = screen.getByText(/現在のご利用プラン/);
        expect(titleElement).toBeInTheDocument();
        const contentElement = screen.getByText(/ライト/);
        expect(contentElement).toBeInTheDocument();
    });
});
