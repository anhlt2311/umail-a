import React from "react";
import { renderHook, renderWithAllProviders, screen } from "~/test/utils";
import PlanCurrentUserInfo from "../PlanCurrentUserInfo";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";

describe("PlanCurrentUserInfo.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test", async () => {
        renderWithAllProviders(<PlanCurrentUserInfo />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            plan: {
                                _all: true,
                                purchase: true,
                                user_add: true,
                                user_delete: true,
                            },
                        },
                    },
                },
            }),
        });
        const currentPlanTitleElement = await screen.findByText(
            /現在のご利用プラン/
        );
        expect(currentPlanTitleElement).toBeInTheDocument();
        const currentPlanElement = await screen.findByText(/スタンダード/);
        expect(currentPlanElement).toBeInTheDocument();
        const currentUserCountTitleElement = await screen.findByText(
            /利用ユーザー数/
        );
        expect(currentUserCountTitleElement).toBeInTheDocument();
        const currentUserCountElement = await screen.findByText("6 / 11");
        expect(currentUserCountElement).toBeInTheDocument();
        const currentUserCountSubText = await screen.findByText(
            "内、追加購入分+1"
        );
        expect(currentUserCountSubText).toBeInTheDocument();
        const addUserButtonElement = await screen.findByRole("button", {
            name: "上限を追加",
        });
        expect(addUserButtonElement).toBeInTheDocument();
        const deleteUserButtonElement = await screen.findByRole("button", {
            name: "上限を削除",
        });
        expect(deleteUserButtonElement).toBeInTheDocument();
    });
});
