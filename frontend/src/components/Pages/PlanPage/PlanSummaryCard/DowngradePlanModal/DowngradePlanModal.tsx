import React, {Dispatch, SetStateAction, useState} from "react";
import { Checkbox, Col, Row, Typography } from "antd";
import { CheckboxValueType } from "antd/lib/checkbox/Group";
import { PlanCardModel } from "~/models/planModel";
import GenericModal from "~/components/Modals/GenericModal/GenericModal";
import GenericModalContent from "~/components/Modals/GenericModal/GenericModalContent/GenericModalContent";
import { useFetchUserPlanInfoAPIQuery, usePlanSummaryAPIQuery } from "~/hooks/usePlan";
import styles from "./DowngradePlanModal.scss";

type Props = {
    visible: boolean;
    setVisible: Dispatch<SetStateAction<boolean>>;
    plan: PlanCardModel;
    handleDowngrade: (plan_master_id: number) => void;
};

const { Text, Paragraph } = Typography;

const DowngradePlanModal = ({ plan, visible, setVisible }: Props) => {
    const { data: planSummary } = usePlanSummaryAPIQuery({ options: { enabled: false }});
    const { data: userPlanInfo } = useFetchUserPlanInfoAPIQuery({
        options: { enabled: false },
    });
    const currentPlan = {
        currentUserCount: userPlanInfo?.currentUserCount || 0,
        userRegistrationLimit: userPlanInfo?.userRegistrationLimit || 0,
        defaultUserCount: userPlanInfo?.defaultUserCount || 0,
    };
    const downgradePlan = {
        defaultCount: plan?.defaultCount || 0,
    }
    const [isAgree, setAgree] = useState<boolean>(false);

    const isNotAddUser = () => {
        const registerLimit = currentPlan.userRegistrationLimit - currentPlan.defaultUserCount;
        const isRegisterLimitLeft = registerLimit <= 0;
        const downgradeLimit = currentPlan.userRegistrationLimit - downgradePlan.defaultCount;
        const isDowngradeLimitLeft = downgradeLimit <= 0;

        return isRegisterLimitLeft && isDowngradeLimitLeft;
    }

    const onChangeCheckbox = (checkedValue: CheckboxValueType[], max: number) => {
        setAgree(checkedValue.length === max);
    };

    const onModalOk = () => {
        setVisible(false);
    }

    const onModalCancel = () => {
        setVisible(false);
    }

    const renderContentCaseDontNeedRegisterUser = () => {
        return (
            <GenericModalContent ellipsis={false}>
                <Row className={styles.contentNotRegisterUser}>
                    <Paragraph className={styles.margin0}>購入を押すとライトが即時反映されます。</Paragraph>
                    <Paragraph className={styles.margin0}>プラン料金のお支払いは、次回の月次決済より新プランのご利用料金に切り替わります。</Paragraph>
                </Row>
                <Row className={styles.contentNotRegisterUser}>
                    <Paragraph className={styles.margin0}>新プランでお使いいただけない機能については関連データが削除されます。</Paragraph>
                    <Paragraph className={styles.margin0}>新プランでお使いいただけないアドオンを購入している場合は自動で解約されます。</Paragraph>
                </Row>
                <Row className={styles.contentNotRegisterUser}>
                    <Paragraph className={styles.margin0}>なお、現在のご利用状況より、プラン変更に伴う課金対象ユーザーに変更はございません。</Paragraph>
                </Row>
                <Row className={styles.checkboxContainer}>
                    <Checkbox.Group onChange={(checkedValue) => onChangeCheckbox(checkedValue, 2)}>
                        <Checkbox className={styles.checkboxItem} value={0}>
                            <span className="label">関連データの削除に同意する</span>
                        </Checkbox>
                        <Checkbox className={styles.checkboxItem} value={1}>
                            <span className="label">新プランで使用できないアドオンの解約に同意する</span>
                        </Checkbox>
                    </Checkbox.Group>
                </Row>
            </GenericModalContent>
        );
    }

    const renderContentCaseNeedRegisterUser = () => {
        return (
            <GenericModalContent ellipsis={false}>
                <Row>
                    <Text>購入を押すとライトが即時反映されます。</Text>
                    <Text>プラン料金のお支払いは、次回の月次決済より新プランのご利用料金に切り替わります。</Text>
                </Row>
                <Row className={styles.description}>
                    <Text>新プランでお使いいただけない機能については関連データが削除されます。</Text>
                    <Text>新プランでお使いいただけないアドオンを購入している場合は自動で解約されます。</Text>
                </Row>
                <Row className={styles.description}>
                    <Text>新プラン適用後に課金対象のユーザー上限数が変更されます。</Text>
                </Row>
                <Row className={styles.numberUser}>
                    <Text>{`現在のユーザー上限数: ${currentPlan.userRegistrationLimit} / ${currentPlan.defaultUserCount} 名`}</Text>
                    <Text>{`ダウングレード後のユーザー上限数: ${currentPlan.userRegistrationLimit} / ${downgradePlan.defaultCount} 名`}</Text>
                </Row>
                <Row className={styles.numberUser}>課金対象ユーザー上限数:</Row>
                <Row className={styles.numberUserPaid}>
                    <Row>
                        <Col className={styles.numberUserPaidItem}>
                            <Text>{planSummary?.planName}</Text>
                            <Text>{`${Math.max(currentPlan.userRegistrationLimit - currentPlan.defaultUserCount, 0)}名`}</Text>
                        </Col>
                        <Col className={styles.numberUserPaidItem}>
                            <Text>{plan.planTitle}</Text>
                            <Text>{`${Math.max(currentPlan.userRegistrationLimit - downgradePlan.defaultCount, 0)}名`}</Text>
                        </Col>
                    </Row>
                </Row>
                <Row className={styles.checkboxContainer}>
                    <Checkbox.Group onChange={(checkedValue) => onChangeCheckbox(checkedValue, 3)}>
                        <Checkbox className={styles.checkboxItem} value={0}>
                            <span className="label">関連データの削除に同意する</span>
                        </Checkbox>
                        <Checkbox className={styles.checkboxItem} value={1}>
                            <span className="label">新プランで使用できないアドオンの解約に同意する</span>
                        </Checkbox>
                        <Checkbox className={styles.checkboxItem} value={2}>
                            <span className="label">課金対象のユーザー上限数変更に同意する</span>
                        </Checkbox>
                    </Checkbox.Group>
                </Row>
            </GenericModalContent>
        );
    }

    const renderModalContent = () => {
        const planId = plan.planId;
        const isPayedPlan = planId > 0;
        if (!isPayedPlan) {
            return <></>;
        }

        const isAddUserNotAvailable = isNotAddUser();
        if (isAddUserNotAvailable) {
            return renderContentCaseDontNeedRegisterUser();
        }
        return renderContentCaseNeedRegisterUser();
    }

    return (
        <GenericModal
            visible={visible}
            type="warning"
            title={`「${plan.planTitle}」にダウングレードしますか？`}
            okText="購入"
            onOk={onModalOk}
            onCancel={onModalCancel}
            okButtonProps={{
                disabled: !isAgree,
            }}
            destroyOnClose>
            {renderModalContent()}
        </GenericModal>
    );
}

export default DowngradePlanModal;
