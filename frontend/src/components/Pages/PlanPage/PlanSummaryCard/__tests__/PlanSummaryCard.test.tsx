import React from "react";
import {
    renderHook,
    renderWithAllProviders,
    screen,
    waitForElementToBeRemoved,
} from "~/test/utils";
import login, { LoginInitialState } from "~/reducers/login";
import PlanSummaryCard from "../PlanSummaryCard";
import { planCards } from "../../data";
import { configureStore } from "@reduxjs/toolkit";
import {
    mockPlanFetchFreeTrialUserInfoSuccessAPIRoute,
    mockPlanFetchLightPlanUserInfoSuccessAPIRoute,
    mockPlanFetchProfessionalPlanUserInfoSuccessAPIRoute,
    mockPlanFetchStandardPlanUserInfoSuccessAPIRoute,
} from "~/test/mock/planAPIMock";
import { mockServer } from "~/test/setupTests";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";
import {
    AuthorizedActionsModel,
    PlanAuthorizedActionsModel,
} from "~/models/authModel";

const loading = async () => {
    const loadingTestId = "loading";
    await waitForElementToBeRemoved(() => screen.getByTestId(loadingTestId));
};

describe("PlanSummaryCard.tsx", () => {
    const planAuthorizedActions: PlanAuthorizedActionsModel = {
        _all: true,
        purchase: true,
        user_add: true,
        user_delete: true,
    };

    const planUnauthorizedActions: PlanAuthorizedActionsModel = {
        _all: false,
        purchase: false,
        user_add: false,
        user_delete: false,
    };

    const authorizedActions: AuthorizedActionsModel = {
        ...LoginInitialState.authorizedActions,
        plan: planAuthorizedActions,
    };

    const unauthorizedActions: AuthorizedActionsModel = {
        ...LoginInitialState.authorizedActions,
        plan: planUnauthorizedActions,
    };

    const preloadedState = {
        login: {
            ...LoginInitialState,
            authorizedActions,
        },
    };

    const unauthorizedPreloadedState = {
        login: {
            ...LoginInitialState,
            authorizedActions: unauthorizedActions,
        },
    };

    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    describe("Light Plan", () => {
        test("unselected render test", async () => {
            mockServer.use(mockPlanFetchFreeTrialUserInfoSuccessAPIRoute);
            renderWithAllProviders(<PlanSummaryCard plan={planCards[0]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            await loading();
            const titleElement = screen.getByText("ライト");
            expect(titleElement).toBeInTheDocument();
            const priceElement = screen.getByText("30,000");
            expect(priceElement).toBeInTheDocument();
            const priceSubtextElement = screen.getByText("円(税抜)/月");
            expect(priceSubtextElement).toBeInTheDocument();
            const planSummaryElement1 = screen.getByText("初期費用無料");
            expect(planSummaryElement1).toBeInTheDocument();
            const planSummaryElement2 = screen.getByText("5ユーザーまで無料");
            expect(planSummaryElement2).toBeInTheDocument();
            const planSummaryElement3 = screen.getByText(
                "ユーザー追加：月額3,000円(税抜)"
            );
            expect(planSummaryElement3).toBeInTheDocument();
            const planSummaryElement4 = screen.getByText("サポート利用無制限");
            expect(planSummaryElement4).toBeInTheDocument();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "このプランを購入",
            });
            expect(purchaseButtonElement).toBeInTheDocument();
            const aimedToTitleElement = screen.getByText("このような方に");
            expect(aimedToTitleElement).toBeInTheDocument();
            const aimedToElement1 =
                screen.getByText("顧客管理をオンライン上で行いたい");
            expect(aimedToElement1).toBeInTheDocument();
            const aimedToElement2 =
                screen.getByText("メール配信を効率化したい");
            expect(aimedToElement2).toBeInTheDocument();
            const functionsTitleElement = screen.getByText("機能");
            expect(functionsTitleElement).toBeInTheDocument();
            const functionElement1 = screen.getByText("ダッシュボード機能");
            expect(functionElement1).toBeInTheDocument();
            const functionElement2 = screen.getByText("取引先管理機能");
            expect(functionElement2).toBeInTheDocument();
            const functionElement3 = screen.getByText("取引先担当者管理機能");
            expect(functionElement3).toBeInTheDocument();
            const functionElement4 = screen.getByText("配信メール管理機能");
            expect(functionElement4).toBeInTheDocument();
            const functionElement5 = screen.getByText("アドオン機能");
            expect(functionElement5).toBeInTheDocument();
        });

        test("selected render test", async () => {
            mockServer.use(mockPlanFetchLightPlanUserInfoSuccessAPIRoute);
            renderWithAllProviders(<PlanSummaryCard plan={planCards[0]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            await loading();
            const titleElement = screen.getByText("ライト");
            expect(titleElement).toBeInTheDocument();
            const priceElement = screen.getByText("30,000");
            expect(priceElement).toBeInTheDocument();
            const priceSubtextElement = screen.getByText("円(税抜)/月");
            expect(priceSubtextElement).toBeInTheDocument();
            const planSummaryElement1 = screen.getByText("初期費用無料");
            expect(planSummaryElement1).toBeInTheDocument();
            const planSummaryElement2 = screen.getByText("5ユーザーまで無料");
            expect(planSummaryElement2).toBeInTheDocument();
            const planSummaryElement3 = screen.getByText(
                "ユーザー追加：月額3,000円(税抜)"
            );
            expect(planSummaryElement3).toBeInTheDocument();
            const planSummaryElement4 = screen.getByText("サポート利用無制限");
            expect(planSummaryElement4).toBeInTheDocument();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "ご利用中",
            });
            expect(purchaseButtonElement).toBeInTheDocument();
            const aimedToTitleElement = screen.getByText("このような方に");
            expect(aimedToTitleElement).toBeInTheDocument();
            const aimedToElement1 =
                screen.getByText("顧客管理をオンライン上で行いたい");
            expect(aimedToElement1).toBeInTheDocument();
            const aimedToElement2 =
                screen.getByText("メール配信を効率化したい");
            expect(aimedToElement2).toBeInTheDocument();
            const functionsTitleElement = screen.getByText("機能");
            expect(functionsTitleElement).toBeInTheDocument();
            const functionElement1 = screen.getByText("ダッシュボード機能");
            expect(functionElement1).toBeInTheDocument();
            const functionElement2 = screen.getByText("取引先管理機能");
            expect(functionElement2).toBeInTheDocument();
            const functionElement3 = screen.getByText("取引先担当者管理機能");
            expect(functionElement3).toBeInTheDocument();
            const functionElement4 = screen.getByText("配信メール管理機能");
            expect(functionElement4).toBeInTheDocument();
            const functionElement5 = screen.getByText("アドオン機能");
            expect(functionElement5).toBeInTheDocument();
        });

        test("unselected purchase button is active", async () => {
            mockServer.use(mockPlanFetchFreeTrialUserInfoSuccessAPIRoute);
            renderWithAllProviders(<PlanSummaryCard plan={planCards[0]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            await loading();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "このプランを購入",
            });
            expect(purchaseButtonElement).not.toBeDisabled();
        });

        test("selected purchase button is not active", async () => {
            mockServer.use(mockPlanFetchLightPlanUserInfoSuccessAPIRoute);
            renderWithAllProviders(<PlanSummaryCard plan={planCards[0]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                plan: {
                                    _all: true,
                                    purchase: true,
                                    user_add: true,
                                    user_delete: true,
                                },
                            },
                        },
                    },
                }),
            });
            await loading();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "ご利用中",
            });
            expect(purchaseButtonElement).toBeDisabled();
        });
    });

    describe("Standard Plan", () => {
        test("unselected render test", async () => {
            mockServer.use(mockPlanFetchFreeTrialUserInfoSuccessAPIRoute);
            renderWithAllProviders(<PlanSummaryCard plan={planCards[1]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            await loading();
            const titleElement = screen.getByText("スタンダード");
            expect(titleElement).toBeInTheDocument();
            const priceElement = screen.getByText("50,000");
            expect(priceElement).toBeInTheDocument();
            const priceSubtextElement = screen.getByText("円(税抜)/月");
            expect(priceSubtextElement).toBeInTheDocument();
            const planSummaryElement1 = screen.getByText("初期費用無料");
            expect(planSummaryElement1).toBeInTheDocument();
            const planSummaryElement2 = screen.getByText("10ユーザーまで無料");
            expect(planSummaryElement2).toBeInTheDocument();
            const planSummaryElement3 = screen.getByText(
                "ユーザー追加：月額5,000円(税抜)"
            );
            expect(planSummaryElement3).toBeInTheDocument();
            const planSummaryElement4 = screen.getByText("サポート利用無制限");
            expect(planSummaryElement4).toBeInTheDocument();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "Coming Soon...",
            });
            expect(purchaseButtonElement).toBeInTheDocument();
            const aimedToTitleElement = screen.getByText("このような方に");
            expect(aimedToTitleElement).toBeInTheDocument();
            const aimedToElement1 =
                screen.getByText("インバウンドセールスを強化したい");
            expect(aimedToElement1).toBeInTheDocument();
            const aimedToElement2 =
                screen.getByText("要員管理、案件管理を一元化したい");
            expect(aimedToElement2).toBeInTheDocument();
            const functionsTitleElement = screen.getByText("機能");
            expect(functionsTitleElement).toBeInTheDocument();
            const functionElement1 = screen.getByText("ダッシュボード機能");
            expect(functionElement1).toBeInTheDocument();
            const functionElement2 = screen.getByText("取引先管理機能");
            expect(functionElement2).toBeInTheDocument();
            const functionElement3 = screen.getByText("取引先担当者管理機能");
            expect(functionElement3).toBeInTheDocument();
            const functionElement4 = screen.getByText("配信メール管理機能");
            expect(functionElement4).toBeInTheDocument();
            const functionElement5 = screen.getByText("アドオン機能");
            expect(functionElement5).toBeInTheDocument();
            const functionElement6 = screen.getByText("その他、Coming soon...");
            expect(functionElement6).toBeInTheDocument();
        });

        test("selected render test", async () => {
            mockServer.use(mockPlanFetchStandardPlanUserInfoSuccessAPIRoute);
            renderWithAllProviders(<PlanSummaryCard plan={planCards[1]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            await loading();
            const titleElement = screen.getByText("スタンダード");
            expect(titleElement).toBeInTheDocument();
            const priceElement = screen.getByText("50,000");
            expect(priceElement).toBeInTheDocument();
            const priceSubtextElement = screen.getByText("円(税抜)/月");
            expect(priceSubtextElement).toBeInTheDocument();
            const planSummaryElement1 = screen.getByText("初期費用無料");
            expect(planSummaryElement1).toBeInTheDocument();
            const planSummaryElement2 = screen.getByText("10ユーザーまで無料");
            expect(planSummaryElement2).toBeInTheDocument();
            const planSummaryElement3 = screen.getByText(
                "ユーザー追加：月額5,000円(税抜)"
            );
            expect(planSummaryElement3).toBeInTheDocument();
            const planSummaryElement4 = screen.getByText("サポート利用無制限");
            expect(planSummaryElement4).toBeInTheDocument();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "ご利用中",
            });
            expect(purchaseButtonElement).toBeInTheDocument();
            const aimedToTitleElement = screen.getByText("このような方に");
            expect(aimedToTitleElement).toBeInTheDocument();
            const aimedToElement1 =
                screen.getByText("インバウンドセールスを強化したい");
            expect(aimedToElement1).toBeInTheDocument();
            const aimedToElement2 =
                screen.getByText("要員管理、案件管理を一元化したい");
            expect(aimedToElement2).toBeInTheDocument();
            const functionsTitleElement = screen.getByText("機能");
            expect(functionsTitleElement).toBeInTheDocument();
            const functionElement1 = screen.getByText("ダッシュボード機能");
            expect(functionElement1).toBeInTheDocument();
            const functionElement2 = screen.getByText("取引先管理機能");
            expect(functionElement2).toBeInTheDocument();
            const functionElement3 = screen.getByText("取引先担当者管理機能");
            expect(functionElement3).toBeInTheDocument();
            const functionElement4 = screen.getByText("配信メール管理機能");
            expect(functionElement4).toBeInTheDocument();
            const functionElement5 = screen.getByText("アドオン機能");
            expect(functionElement5).toBeInTheDocument();
            const functionElement6 = screen.getByText("その他、Coming soon...");
            expect(functionElement6).toBeInTheDocument();
        });

        test("unselected purchase button is not active", async () => {
            mockServer.use(mockPlanFetchFreeTrialUserInfoSuccessAPIRoute);
            renderWithAllProviders(<PlanSummaryCard plan={planCards[1]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            await loading();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "Coming Soon...",
            });
            expect(purchaseButtonElement).toBeInTheDocument();
            expect(purchaseButtonElement).toBeDisabled();
        });

        test("selected purchase button is not active", async () => {
            mockServer.use(mockPlanFetchStandardPlanUserInfoSuccessAPIRoute);
            renderWithAllProviders(<PlanSummaryCard plan={planCards[1]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                plan: {
                                    _all: true,
                                    purchase: true,
                                    user_add: true,
                                    user_delete: true,
                                },
                            },
                        },
                    },
                }),
            });
            await loading();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "ご利用中",
            });
            expect(purchaseButtonElement).toBeInTheDocument();
            expect(purchaseButtonElement).toBeDisabled();
        });
    });

    describe("ProfessionalPlan", () => {
        test("unselected render test", async () => {
            mockServer.use(mockPlanFetchFreeTrialUserInfoSuccessAPIRoute);
            renderWithAllProviders(<PlanSummaryCard plan={planCards[2]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            await loading();
            const titleElement = screen.getByText("プロフェッショナル");
            expect(titleElement).toBeInTheDocument();
            const priceElement = screen.getByText("100,000");
            expect(priceElement).toBeInTheDocument();
            const priceSubtextElement = screen.getByText("円(税抜)/月");
            expect(priceSubtextElement).toBeInTheDocument();
            const planSummaryElement1 = screen.getByText("初期費用無料");
            expect(planSummaryElement1).toBeInTheDocument();
            const planSummaryElement2 = screen.getByText("20ユーザーまで無料");
            expect(planSummaryElement2).toBeInTheDocument();
            const planSummaryElement3 = screen.getByText(
                "ユーザー追加：月額10,000円(税抜)"
            );
            expect(planSummaryElement3).toBeInTheDocument();
            const planSummaryElement4 = screen.getByText("サポート利用無制限");
            expect(planSummaryElement4).toBeInTheDocument();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "Coming Soon...",
            });
            expect(purchaseButtonElement).toBeInTheDocument();
            const aimedToTitleElement = screen.getByText("このような方に");
            expect(aimedToTitleElement).toBeInTheDocument();
            const aimedToElement1 =
                screen.getByText("営業活動を数字で管理、効率化したい");
            expect(aimedToElement1).toBeInTheDocument();
            const aimedToElement2 =
                screen.getByText("より戦略的な営業施策を実施したい");
            expect(aimedToElement2).toBeInTheDocument();
            const functionsTitleElement = screen.getByText("機能");
            expect(functionsTitleElement).toBeInTheDocument();
            const functionElement1 = screen.getByText("ダッシュボード機能");
            expect(functionElement1).toBeInTheDocument();
            const functionElement2 = screen.getByText("取引先管理機能");
            expect(functionElement2).toBeInTheDocument();
            const functionElement3 = screen.getByText("取引先担当者管理機能");
            expect(functionElement3).toBeInTheDocument();
            const functionElement4 = screen.getByText("配信メール管理機能");
            expect(functionElement4).toBeInTheDocument();
            const functionElement5 = screen.getByText("アドオン機能");
            expect(functionElement5).toBeInTheDocument();
            const functionElement6 = screen.getByText("その他、Coming soon...");
            expect(functionElement6).toBeInTheDocument();
        });

        test("selected render test", async () => {
            mockServer.use(
                mockPlanFetchProfessionalPlanUserInfoSuccessAPIRoute
            );
            renderWithAllProviders(<PlanSummaryCard plan={planCards[2]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            await loading();
            const titleElement = screen.getByText("プロフェッショナル");
            expect(titleElement).toBeInTheDocument();
            const priceElement = screen.getByText("100,000");
            expect(priceElement).toBeInTheDocument();
            const priceSubtextElement = screen.getByText("円(税抜)/月");
            expect(priceSubtextElement).toBeInTheDocument();
            const planSummaryElement1 = screen.getByText("初期費用無料");
            expect(planSummaryElement1).toBeInTheDocument();
            const planSummaryElement2 = screen.getByText("20ユーザーまで無料");
            expect(planSummaryElement2).toBeInTheDocument();
            const planSummaryElement3 = screen.getByText(
                "ユーザー追加：月額10,000円(税抜)"
            );
            expect(planSummaryElement3).toBeInTheDocument();
            const planSummaryElement4 = screen.getByText("サポート利用無制限");
            expect(planSummaryElement4).toBeInTheDocument();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "ご利用中",
            });
            expect(purchaseButtonElement).toBeInTheDocument();
            const aimedToTitleElement = screen.getByText("このような方に");
            expect(aimedToTitleElement).toBeInTheDocument();
            const aimedToElement1 =
                screen.getByText("営業活動を数字で管理、効率化したい");
            expect(aimedToElement1).toBeInTheDocument();
            const aimedToElement2 =
                screen.getByText("より戦略的な営業施策を実施したい");
            expect(aimedToElement2).toBeInTheDocument();
            const functionsTitleElement = screen.getByText("機能");
            expect(functionsTitleElement).toBeInTheDocument();
            const functionElement1 = screen.getByText("ダッシュボード機能");
            expect(functionElement1).toBeInTheDocument();
            const functionElement2 = screen.getByText("取引先管理機能");
            expect(functionElement2).toBeInTheDocument();
            const functionElement3 = screen.getByText("取引先担当者管理機能");
            expect(functionElement3).toBeInTheDocument();
            const functionElement4 = screen.getByText("配信メール管理機能");
            expect(functionElement4).toBeInTheDocument();
            const functionElement5 = screen.getByText("アドオン機能");
            expect(functionElement5).toBeInTheDocument();
            const functionElement6 = screen.getByText("その他、Coming soon...");
            expect(functionElement6).toBeInTheDocument();
        });

        test("unselected purchase button is not active", async () => {
            renderWithAllProviders(<PlanSummaryCard plan={planCards[2]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            await loading();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "Coming Soon...",
            });
            expect(purchaseButtonElement).toBeInTheDocument();
            expect(purchaseButtonElement).toBeDisabled();
        });

        test("selected purchase button is not active", async () => {
            mockServer.use(
                mockPlanFetchProfessionalPlanUserInfoSuccessAPIRoute
            );
            renderWithAllProviders(<PlanSummaryCard plan={planCards[2]} />, {
                store: configureStore({
                    reducer: { login },
                    preloadedState: {
                        ...preloadedState,
                    },
                }),
            });
            await loading();
            const purchaseButtonElement = screen.getByRole("button", {
                name: "ご利用中",
            });
            expect(purchaseButtonElement).toBeInTheDocument();
            expect(purchaseButtonElement).toBeDisabled();
        });
    });
});
