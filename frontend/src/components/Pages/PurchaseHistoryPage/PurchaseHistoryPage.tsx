import React, { useState } from "react";
import { Col, Row } from "antd";
import PurchaseHistoryTable from "./PurchaseHistoryTable/PurchaseHistoryTable";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import { useFetchPurchaseHistoryAPIQuery } from "~/hooks/usePurchaseHistory";
import { PaginationRequestModel } from "~/models/requestModel";
import CustomPagination from "~/components/Common/CustomPagination/CustomPagination";
import { DEFAULT_PAGE_SIZE } from "~/utils/constants";
import styles from "./PurchaseHistoryPage.scss";

const PurchaseHistoryPage = () => {
    const [deps, setDeps] = useState<PaginationRequestModel>({
        page: 1,
        pageSize: DEFAULT_PAGE_SIZE,
    });
    const { data, isLoading } = useFetchPurchaseHistoryAPIQuery({ deps });

    return (
        <Col span={24}>
            <Row>
                <CustomPageHeader title="お支払い履歴 一覧" />
            </Row>
            <Row style={{ marginTop: "1%", marginBottom: "1%" }}>
                <Col span={24}>
                    <PurchaseHistoryTable
                        data={data?.results ?? []}
                        loading={isLoading}
                    />
                </Col>
            </Row>
            <Row justify="end">
                <Col>
                    <CustomPagination
                        current={deps.page}
                        total={data?.count ?? 0}
                        defaultCurrent={deps.page}
                        onChange={(selectedPage, _) => {
                            setDeps({
                                ...deps,
                                page: selectedPage,
                            });
                        }}
                    />
                </Col>
            </Row>
        </Col>
    );
};

export default PurchaseHistoryPage;
