import React, { useEffect } from "react";
import { Button, Col, Form, Row, Input } from "antd";
import BoardPopover from "~/components/Common/BoardCommon/BoardPopover/BoardPopover";
import styles from "./PersonnelBoardCreatePopover.scss";
import { PersonnelBoardNewCardFormModel } from "~/models/personnelBoardModel";
import BoardPersonnelInitialNameInput from "~/components/Common/BoardCommon/BoardPersonnelInitialNameInput/BoardPersonnelInitialNameInput";

type Props = {
    list_id: string;
    onCreate: (values: PersonnelBoardNewCardFormModel) => void;
    onClose: () => void;
};

const PersonnelBoardCreatePopover = ({ list_id, onCreate, onClose }: Props) => {
    const [form] = Form.useForm<PersonnelBoardNewCardFormModel>();

    useEffect(() => {
        form.setFieldsValue({ list_id });
    }, []);

    const onCreateCard = (values: PersonnelBoardNewCardFormModel) => {
        form.resetFields();
        onCreate(values);
    }

    return (
        <BoardPopover title="要員を作成" onClose={onClose}>
            <Row justify="center" style={{ width: '100%' }}>
                <Form form={form} onFinish={onCreateCard} className={styles.form} >
                    <BoardPersonnelInitialNameInput />
                    <Row>
                        <Col span={24}>
                            <Button
                                style={{
                                    width: "100%",
                                    marginTop: 10,
                                    marginBottom: 10,
                                }}
                                type="primary"
                                htmlType="submit">
                                作成
                            </Button>
                        </Col>
                    </Row>
                    <Form.Item name="listId" hidden>
                        <Input />
                    </Form.Item>
                </Form>
            </Row>
        </BoardPopover>
    );
};

export default PersonnelBoardCreatePopover;
