import React, { useCallback, useEffect, useState } from "react";
import {
  CheckOutlined,
  CloseOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { Button, Col, Row, Spin, Switch, Typography } from "antd";
import { Moment } from "moment";
import { useRecoilState } from "recoil";
import PersonnelBoardModal from "~/components/Common/BoardCommon/PersonnelBoardModal/PersonnelBoardModal";
import {
  SettingFn,
  SETTING_ENUM,
} from "~/components/Common/BoardComponent/BoardBaseComponent/MenuBoardSettingsComponent/MenuBoardSettingsComponent";
import BoardBaseItemTable from "~/components/Common/BoardComponent/BoardBaseItemTable/BoardBaseItemTable";
import BoardComponent from "~/components/Common/BoardComponent/BoardComponent";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import {
  useDuplicatePersonnelCardBoard,
  useDeletePersonnelCardBoard,
  usePersonnelBoardDetailCard,
  usePersonnelSettingBoard,
  useUpdatePersonnelCardBoard,
} from "~/hooks/usePersonnelBoard";
import {
  BoardChecklistItemUpdateFormModel,
  BoardChecklistUpdateFormModel,
} from "~/models/boardCommonModel";
import {
  PersonnelBoardListCardModel,
  PersonnelBoardCopyCardFormModel,
} from "~/models/personnelBoardModel";
import { personnelBoard } from "~/recoil/atom";
const { Text } = Typography;

const SIMPLE_CARD_KEY = "personnel/simple-card";

const PersonnelBoardPage = () => {
  const [isActivatedSimpleCard, setActivatedSimpleCard] = useState(false);
  const {
    boardData,
    onSortAllCard,
    onArchiveAllCard,
    onMoveAllCard,
    onRefetchBoard,
  } = usePersonnelSettingBoard();
  const [personnelBoardState, setPersonnelBoardState] =
    useRecoilState(personnelBoard);
  const typeCard = "personnel";

  const {
    isLoading,
    dataDetail,
    dataComments,
    checkLists,
    dataContacts,
    dataOrganizations,
    personnelBoardDeleteChecklist,
    personnelBoardCreateChecklistItem,
    personnelBoardUpdateChecklistItem,
    personnelBoardUpdateChecklist,
    personnelBoardCreateChecklist,
    refetchFetchOrganizations,
    refetchFetchContacts,
  } = usePersonnelBoardDetailCard();

  const { updateAssigneesCardBoard, updateDateTimeCardBoard, dataUpdates } =
    useUpdatePersonnelCardBoard();

  const { duplicatePersonnelCardBoard, dupliateCardData } =
    useDuplicatePersonnelCardBoard();

  const { deletePersonnelCardBoard, deleteCardData } =
    useDeletePersonnelCardBoard();

  const onDetailCard = (cardId: string) => () => {
    setPersonnelBoardState({
      ...personnelBoardState,
      isUpdatePersonnel: true,
      cardId,
    });
  };

  const handleChange = (
    values: string[],
    itemCard: PersonnelBoardListCardModel
  ) => {
    updateAssigneesCardBoard(values, itemCard.id);
  };

  const handleChangeStartDate = (
    value: Moment | null,
    itemCard: PersonnelBoardListCardModel
  ) => {
    const endDate = itemCard?.period?.end ? itemCard?.period?.end : "";
    if (value) {
      updateDateTimeCardBoard(value, endDate, itemCard.id);
    }
  };

  const handleChangeEndDate = (
    value: Moment | null,
    itemCard: PersonnelBoardListCardModel
  ) => {
    const startDate = itemCard?.period?.start ? itemCard?.period?.start : "";
    if (value) {
      updateDateTimeCardBoard(startDate, value, itemCard.id);
    }
  };

  const renderCard = (itemCard: PersonnelBoardListCardModel) => {
    return (
      <Col key={itemCard.id} span={24}>
        <BoardBaseItemTable
          isSimpleCard={isActivatedSimpleCard}
          itemCard={itemCard}
          handleChange={handleChange}
          onDetailCard={onDetailCard}
          handleChangeStartDate={handleChangeStartDate}
          handleChangeEndDate={handleChangeEndDate}
        />
      </Col>
    );
  };

  const onSettings: SettingFn = useCallback(
    (type, params) => {
      switch (type) {
        case SETTING_ENUM.SORT:
          if (onSortAllCard && params?.orderBy)
            onSortAllCard(params?.listId, params.orderBy);

          break;
        case SETTING_ENUM.MOVE_ALL_CARD:
          if (onMoveAllCard && params?.newListId)
            onMoveAllCard(params?.listId, params.newListId);

          break;
        case SETTING_ENUM.ARCHIVE_ALL_CARD:
          if (onArchiveAllCard) onArchiveAllCard(params?.listId);

          break;
        default:
          break;
      }
    },
    [onSortAllCard, onMoveAllCard, onArchiveAllCard]
  );

  const onDeleteCheckList = (idCheckList: string) => {
    personnelBoardDeleteChecklist(idCheckList);
  };

  const onAddItem = (value: string, idCheckList: string) => () => {
    personnelBoardCreateChecklistItem(value, idCheckList);
  };

  const onChecklistItemSubmit = (
    values: BoardChecklistItemUpdateFormModel,
    checklistId: string
  ) => {
    personnelBoardUpdateChecklistItem(values, checklistId);
  };

  const onSubmitFinishContent = (
    values: BoardChecklistUpdateFormModel,
    checkListId: string
  ) => {
    personnelBoardUpdateChecklist(values, checkListId);
  };

  const onCreateCheckList = () => {
    const { cardId } = personnelBoardState;
    personnelBoardCreateChecklist(cardId);
  };

  const onDropdownVisibleChangeOrganizations = (open: boolean) => {
    if (open) {
      refetchFetchOrganizations();
    }
  };

  const onDropdownVisibleChangeContacts = (open: boolean) => {
    if (open) {
      refetchFetchContacts();
    }
  };

  const onDuplicateCard = (data: PersonnelBoardCopyCardFormModel) => {
    duplicatePersonnelCardBoard(data);
  };

  const onDeleteCard = (cardId: string) => {
    deletePersonnelCardBoard(cardId);
  };

  const _simpleCardLoad = useCallback(() => {
    const _activated = localStorage.getItem(SIMPLE_CARD_KEY);

    setActivatedSimpleCard(_activated === "activated");
  }, []);

  const _simpleCardHandler = (isActivated: boolean) => {
    if (isActivated) {
      localStorage.setItem(SIMPLE_CARD_KEY, "activated");
    } else {
      localStorage.removeItem(SIMPLE_CARD_KEY);
    }

    setActivatedSimpleCard(isActivated);
  };

  useEffect(_simpleCardLoad, []);

  useEffect(()=> {
    if (dupliateCardData || deleteCardData) {
      onRefetchBoard();
    }
  }, [dupliateCardData, deleteCardData])

  return (
    <Spin spinning={false}>
      <Col span={24}>
        <Row justify="space-between" align="middle">
          <Col span={12}>
            <CustomPageHeader title="要員ボード" />
          </Col>
          <Col span={12}>
            <Row justify="end">
              <Text>簡易表示</Text>

              <Switch
                checkedChildren={<CheckOutlined />}
                unCheckedChildren={<CloseOutlined />}
                onChange={_simpleCardHandler}
                checked={isActivatedSimpleCard}
              />

              <Button
                type="primary"
                style={{ marginLeft: "10px" }}
                size="small"
                icon={<SearchOutlined />}
              >
                絞り込み検索
              </Button>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <BoardComponent
              typeCard={typeCard}
              boardData={boardData}
              renderCard={renderCard}
              onSettings={onSettings}
            />
          </Col>
        </Row>
        <PersonnelBoardModal
          isLoading={isLoading}
          dataCheckLists={checkLists}
          onSubmitFinishContent={onSubmitFinishContent}
          initialData={dataDetail}
          onDeleteCheckList={onDeleteCheckList}
          dataComments={dataComments || []}
          dataContacts={dataContacts || []}
          dataOrganizations={dataOrganizations || []}
          onAddItem={onAddItem}
          onCreateCheckList={onCreateCheckList}
          onChecklistItemSubmit={onChecklistItemSubmit}
          onDropdownVisibleChangeOrganizations={
            onDropdownVisibleChangeOrganizations
          }
          onDropdownVisibleChangeContacts={onDropdownVisibleChangeContacts}
          onDuplicateCard={onDuplicateCard}
          onDeleteCard={onDeleteCard}
          onRefetchBoard={onRefetchBoard}
        />
      </Col>
    </Spin>
  );
};

export default PersonnelBoardPage;
