import React from "react";
import { Modal, ModalProps } from "antd";
import PayjpForm from "../PayjpForm/PayjpForm";
import styles from "./PayjpModal.scss";
import { PaymentCreateModel } from "~/models/paymentModel";

type Props = ModalProps & {
    onPaymentTokenGenerate: (data: PaymentCreateModel) => void;
    onFinish: () => void;
    oldCardId: string;
    cardOrder: number;
};

const PayjpModal = ({ onPaymentTokenGenerate, onFinish, oldCardId, cardOrder, ...props }: Props) => {
    return (
        <Modal {...props} footer={null} title="お支払い情報">
            <PayjpForm
                onFinish={onFinish}
                onPaymentTokenGenerate={onPaymentTokenGenerate}
                oldCardId={oldCardId}
                cardOrder={cardOrder}
            />
        </Modal>
    );
};

export default PayjpModal;
