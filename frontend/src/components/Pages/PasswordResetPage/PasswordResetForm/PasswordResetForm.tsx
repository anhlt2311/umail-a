import React, { CSSProperties } from "react";
import { UserOutlined } from "@ant-design/icons";
import { Button, Col, Form, Input, Result, Row, Spin } from "antd";
import { Link } from "react-router-dom";
import Paths from "~/components/Routes/Paths";
import validateJapaneseMessages from "~/components/Forms/validateMessages";
import { PasswordResetRequestFormModel } from "~/models/authModel";
import { usePasswordResetAPIMutation } from "~/hooks/useAccount";
import { ErrorMessages } from "~/utils/constants";
import ReCAPTCHA from "react-google-recaptcha";
import { useTenantFetchRecaptchaAPIQuery } from "~/hooks/useTenant";
import { FieldError } from "rc-field-form/lib/interface";
import styles from "./PasswordResetForm.scss";

const commonStyle: CSSProperties = {
    width: "100%",
};

type Props = {};

const PasswordResetForm = ({}: Props) => {
    const requiredFields = ["email", "recaptcha"];
    const [form] = Form.useForm<PasswordResetRequestFormModel>();
    const { mutate: passwordReset } = usePasswordResetAPIMutation();
    const { data, isLoading, isError } = useTenantFetchRecaptchaAPIQuery({});

    if (isLoading) {
        return (
            <Col span={24}>
                <Row justify="center" align="middle" style={{ height: "100%" }}>
                    <Spin spinning={isLoading} />
                </Row>
            </Col>
        );
    }

    if (!data?.sitekey || isError) {
        return (
            <Col span={24}>
                <Row justify="center">
                    <Col span={24}>
                        <Result
                            status="warning"
                            title="画面の表示に失敗しました。画面をリフレッシュしてください。"
                            extra={
                                <Button
                                    type="primary"
                                    onClick={() => window.location.reload()}>
                                    画面をリフレッシュ
                                </Button>
                            }
                        />
                    </Col>
                </Row>
            </Col>
        );
    }

    const handleSubmit = (values: PasswordResetRequestFormModel) => {
        passwordReset(values);
    };

    const hasErrors = (fieldErrors: FieldError[]) => {
        return !!fieldErrors.filter(({ errors }) => errors.length).length;
    };

    return (
        <Col span={24} style={{ margin: "32px auto" }}>
            <Row justify="center">
                <Col span={24}>
                    <Form
                        onFinish={handleSubmit}
                        form={form}
                        validateMessages={validateJapaneseMessages}>
                        <Form.Item
                            style={commonStyle}
                            name="email"
                            rules={[
                                {
                                    required: true,
                                    type: "email",
                                    message: ErrorMessages.validation.email,
                                },
                                {
                                    max: 100,
                                    message:
                                        ErrorMessages.validation.length.max100,
                                },
                            ]}>
                            <Input
                                prefix={
                                    <UserOutlined
                                        style={{ color: "rgba(0,0,0,.25)" }}
                                    />
                                }
                                placeholder="メールアドレス"
                            />
                        </Form.Item>
                        {!data.testMode && (
                            <Form.Item name="recaptcha" noStyle>
                                <ReCAPTCHA sitekey={data?.sitekey} />
                            </Form.Item>
                        )}
                        <Form.Item style={commonStyle} shouldUpdate>
                            {() => (
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    disabled={
                                        !form.isFieldsTouched(
                                            data.testMode
                                                ? requiredFields.filter(
                                                      (field) =>
                                                          field !== "recaptcha"
                                                  )
                                                : requiredFields,
                                            true
                                        ) || hasErrors(form.getFieldsError())
                                    }
                                    className={styles.button}>
                                    送信
                                </Button>
                            )}
                        </Form.Item>
                        <Form.Item>
                            <Link to={`${Paths.login}`}>
                                ログインはこちらから
                            </Link>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </Col>
    );
};

export default PasswordResetForm;
