import React from "react";
import PasswordResetForm from "~/components/Pages/PasswordResetPage/PasswordResetForm/PasswordResetForm";
import { Col, Row } from "antd";
import styles from "~/components/Pages/page.scss";

const PasswordResetPage = () => {
    return (
        <Col span={24} style={{ padding: "16px 0 0 0" }}>
            <Row justify="center">
                <Col style={{ width: "300px" }}>
                    <h1>パスワードの再設定手順をお送りいたします。</h1>
                    <PasswordResetForm />
                </Col>
            </Row>
        </Col>
    );
};

export default PasswordResetPage;
