import React from "react";
import { Button, Result } from "antd";
import { useHistory } from "react-router-dom";
import { useRole } from "~/hooks/useRole";
import { useGuardPlanSummary } from "~/hooks/usePlan";

const NotFoundPage = () => {
    const router = useHistory();
    const { isMasterRole } = useRole();
    const { isNotValidUser } = useGuardPlanSummary();

    return (
        <Result
            status="404"
            title="ページが見つかりません"
            subTitle="ご指定のURLに対応するページが見つかりませんでした。"
            extra={
                (isMasterRole || !isNotValidUser) && (
                    <Button type="primary" onClick={() => router.goBack()}>
                        戻る
                    </Button>
                )
            }
        />
    );
};

export default NotFoundPage;
