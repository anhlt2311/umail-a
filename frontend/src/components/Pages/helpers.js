export const setStateLoading = (component) => {
  component.setState({ loading: true });
};

export const setStateLoaded = (component) => {
  component.setState({ loading: false });
};

export const setStateLoadError = (component, error) => {
  component.setState({ loading: false, errorMessage: error.message });
};

export const setTableState = (component, data, currentPage, pageSize, totalCount) => {
  component.setState({
    data,
    currentPage,
    pageSize,
    totalCount,
  });
};

export const getDisplayName = WrappedComponent => (WrappedComponent.displayName || WrappedComponent.name || 'Component');

export const hasAnyError = (validationResult) => {
  for (const [key, value] of Object.entries(validationResult)) {
    if (value['errors'] && value['errors'].length > 0) { return true; }
  }
  return false;
};
