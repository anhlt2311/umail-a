import React from "react";
import { renderWithAllProviders, screen } from "~/test/utils";
import UserSearchPageContainer from "../../UserSearchPage";
import { configureStore } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import { userSearchPage, displaySettingPage } from "~/reducers/pages";
import { defaultInitialState as SearchPageInitialState } from "~/reducers/Factories/searchPage";
import { defaultInitialState as EditPageInitialState } from "~/reducers/Factories/editPage";
import {
    usersAPIResponseContent,
    usersModelList,
} from "~/test/mock/userAPIMock";
import _ from "lodash";

describe("UserSearchPage.jsx", () => {
    test("render test", async () => {
        renderWithAllProviders(<UserSearchPageContainer />, {
            store: configureStore({
                reducer: {
                    login,
                    userSearchPage,
                    displaySettingPage,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            users: {
                                _all: true,
                            },
                        },
                    },
                    userSearchPage: {
                        ...SearchPageInitialState,
                        data: [...usersModelList],
                    },
                    displaySettingPage: {
                        ...EditPageInitialState,
                        isDisplaySettingLoading: false,
                    },
                },
            }),
        });
        const pageTitleElement = screen.getByText(/^ユーザー設定$/);
        expect(pageTitleElement).toBeInTheDocument();
        const userInviteButton = screen.getByText(/ユーザー招待/);
        expect(userInviteButton).toBeInTheDocument();
        expect(userInviteButton).not.toBeDisabled();
        // NOTE(joshua-hashimoto): 簡易的なテーブルの存在確認
        const userNameElements = screen.getAllByText(/ユーザー名/);
        expect(userNameElements.length).toBe(2);
        const tableRowElements = screen.getAllByRole("row");
        expect(tableRowElements.length).toBe(usersAPIResponseContent.length);
        const tableDataElement = screen.getByText(
            new RegExp(
                usersAPIResponseContent[
                    _.random(usersAPIResponseContent.length - 1)
                ].display_name
            )
        );
        expect(tableDataElement).toBeInTheDocument();
        // NOTE(joshua-hashimoto): 簡易的な検索結果表示項目設定モーダルの存在確認
        const tableDisplaySettingModalCancelButtonElement =
            screen.getByText(/更 新/);
        expect(tableDisplaySettingModalCancelButtonElement).toBeInTheDocument();
        const paginationLabelElement = await screen.findByText(/合計/);
        expect(paginationLabelElement).toBeInTheDocument();
    });
});
