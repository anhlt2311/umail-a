import React from "react";
import { renderHook, renderWithAllProviders, screen } from "~/test/utils";
import { configureStore, PayloadAction } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import {
    contactSearchPage,
    scheduledEmailEditPageContactSearchForm,
} from "~/reducers/pages";
import { generateRandomToken } from "~/utils/utils";
import TagEditPage from "../TagEditPage";
import { useClient } from "~/hooks/useClient";
import { defaultInitialState as SearchPageInitialState } from "~/reducers/Factories/searchPage";
import { tagResponseModelList } from "~/test/mock/tagAPIMock";
import { convertTagResponseModelToTagDetailModel } from "~/hooks/useTag";

const randomToken = generateRandomToken();

describe("TagEditPage.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test", async () => {
        // NOTE(joshua-hashimoto): 将来のテストのためにメモとして残しておく
        // <MemoryRouter
        //         initialEntries={[
        //             `${Paths.tags}/${mockTagEditResponseData.id}`,
        //         ]}>
        //         <Route path={`${Paths.tags}/:id`}>
        //             <TagEditPage />
        //         </Route>
        //     </MemoryRouter>,
        renderWithAllProviders(<TagEditPage />, {
            store: configureStore({
                reducer: {
                    login,
                    contactSearchPage,
                    scheduledEmailEditPageContactSearchForm,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: true,
                            },
                        },
                    },
                    contactSearchPage: {
                        ...SearchPageInitialState,
                    },
                    scheduledEmailEditPageContactSearchForm: {
                        ...SearchPageInitialState,
                    },
                },
            }),
        });
        const pageTitleElement = screen.getByText(/タグ 編集/);
        expect(pageTitleElement).toBeInTheDocument();
        const mockData = convertTagResponseModelToTagDetailModel(
            tagResponseModelList[0]
        );
        const createdUserLabelElement = await screen.findByText(/作成者:/);
        expect(createdUserLabelElement).toBeInTheDocument();
        const createdUserElement = await screen.findByText(
            new RegExp(mockData.created_user, "g")
        );
        expect(createdUserElement).toBeInTheDocument();
        const modifiedTimeLabelElement = await screen.findByText(/更新日時:/);
        expect(modifiedTimeLabelElement).toBeInTheDocument();
        const modifiedTimeElement = await screen.findByText(
            new RegExp(mockData.modified_time, "g")
        );
        expect(modifiedTimeElement).toBeInTheDocument();
        const modifiedUserLabelElement = screen.getByText(/更新者:/);
        expect(modifiedUserLabelElement).toBeInTheDocument();
        const modifiedUserElement = await screen.findByText(
            new RegExp(mockData.modified_user, "g")
        );
        expect(modifiedUserElement).toBeInTheDocument();
        // NOTE(joshua-hashimoto): Formの簡易存在確認
        const tagNameLabelElement = screen.getByText(/タグ名/);
        expect(tagNameLabelElement).toBeInTheDocument();
        const editButtonElement = await screen.findByRole("button", {
            name: /更 新/,
        });
        expect(editButtonElement).toBeInTheDocument();
        expect(editButtonElement).not.toBeDisabled();
        const deleteButtonElement = screen.getByText(/^削 除$/);
        expect(deleteButtonElement).toBeInTheDocument();
        expect(deleteButtonElement).not.toBeDisabled();
    });

    test("authorized test", () => {
        renderWithAllProviders(<TagEditPage />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                    contactSearchPage,
                    scheduledEmailEditPageContactSearchForm,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: randomToken,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: true,
                            },
                        },
                    },
                    contactSearchPage: {
                        currentSearchConditions: {},
                    },
                    scheduledEmailEditPageContactSearchForm: {
                        currentSearchConditions: {},
                    },
                },
            }),
        });
        const notFoundTitleElement =
            screen.queryByText(/^ページが見つかりません$/);
        expect(notFoundTitleElement).not.toBeInTheDocument();
    });

    test("unauthorized test", () => {
        renderWithAllProviders(<TagEditPage />, {
            store: configureStore<any, PayloadAction<any>>({
                reducer: {
                    login,
                    contactSearchPage,
                    scheduledEmailEditPageContactSearchForm,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        token: randomToken,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            tags: {
                                _all: false,
                            },
                        },
                    },
                    contactSearchPage: {
                        currentSearchConditions: {},
                    },
                    scheduledEmailEditPageContactSearchForm: {
                        currentSearchConditions: {},
                    },
                },
            }),
        });
        const notFoundTitleElement =
            screen.getByText(/^ページが見つかりません$/);
        expect(notFoundTitleElement).toBeInTheDocument();
    });
});
