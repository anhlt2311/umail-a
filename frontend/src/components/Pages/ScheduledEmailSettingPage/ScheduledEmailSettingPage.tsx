import React from "react";
import { Col, Row, Spin } from "antd";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import CustomDetailPageContent from "~/components/Common/CustomDetailPageContent/CustomDetailPageContent";
import ScheduledEmailSettingForm from "./ScheduledEmailSettingForm/ScheduledEmailSettingForm";
import { useFetchScheduledEmailSettingAPIQuery } from "~/hooks/useScheduledEmailSetting";
import AboutThisData from "~/components/Common/AboutThisData/AboutThisData";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import NotFoundPage from "../NotFoundPage";
import styles from "./ScheduledEmailSettingPage.scss";

const ScheduledEmailSettingPage = () => {
    const { data, isLoading } = useFetchScheduledEmailSettingAPIQuery({});
    const { _all: scheduledEmailSettingsAuthorization } = useAuthorizedActions(
        "scheduled_email_settings"
    );

    if (!scheduledEmailSettingsAuthorization) {
        return <NotFoundPage />;
    }

    return (
        <Col span={24}>
            <Row>
                <Col span={24}>
                    <CustomPageHeader
                        title="配信メール設定"
                        extra={[
                            <AboutThisData
                                key="scheduledEmailSetting-aboutThisData"
                                data={{
                                    modified_time: data?.modified_time,
                                    modified_user: data?.modified_user,
                                }}
                            />,
                        ]}
                        style={{
                            padding: "16px 0px 0px 0px",
                        }}
                    />
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <Spin spinning={isLoading}>
                        <CustomDetailPageContent>
                            <ScheduledEmailSettingForm initialData={data} />
                        </CustomDetailPageContent>
                    </Spin>
                </Col>
            </Row>
        </Col>
    );
};

export default ScheduledEmailSettingPage;
