import React, { useEffect } from "react";
import { Form, Radio, Tooltip } from "antd";
import styles from "./ScheduledEmailSettingDetailSettingsForm.scss";
import { TooltipContentLink } from "~/components/Common/TooltipContentLink/TooltipContentLink";
import Paths from "~/components/Routes/Paths";
import { QuestionCircleFilled } from "@ant-design/icons";
import { iconCustomColor, Links, TooltipMessages } from "~/utils/constants";
import { ScheduledEmailSettingDetailSettingFormModel } from "~/models/scheduledEmailSettingModel";

const detailSettingFormLayoutLabelSpan = 7;
const detailSettingFormLayout = {
    labelCol: {
        span: detailSettingFormLayoutLabelSpan,
    },
    wrapperCol: {
        span: 24 - detailSettingFormLayoutLabelSpan,
    },
};

const detailSettingIndent2FormLayoutLabelSpan =
    detailSettingFormLayoutLabelSpan + 2;
const detailSettingIndent2FormLayout = {
    labelCol: {
        span: detailSettingIndent2FormLayoutLabelSpan,
    },
    wrapperCol: {
        span: 24 - detailSettingIndent2FormLayoutLabelSpan,
    },
};

export const formName = "detailSettingsForm";

type Props = {
    initialData?: ScheduledEmailSettingDetailSettingFormModel;
    isNotRegistered: boolean;
};

const ScheduledEmailSettingDetailSettingsForm = ({
    initialData,
    isNotRegistered,
}: Props) => {
    const [form] = Form.useForm<ScheduledEmailSettingDetailSettingFormModel>();

    useEffect(() => {
        if (initialData) {
            form.setFieldsValue(initialData);
        }
    }, [initialData]);

    return (
        <Form
            name={formName}
            form={form}
            style={{ textAlign: "left" }}
            labelAlign="right">
            <Form.Item
                {...detailSettingFormLayout}
                label={
                    <span>
                        ファイル形式&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    <TooltipContentLink
                                        to={`${Paths.scheduledMailsRegister}`}
                                        title="配信メール予約／編集"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    />
                                    <br />
                                    で添付するファイルの形式を選択します。
                                    <br />
                                    ダウンロードURLを選択した場合、挿入文の末尾に各メールごとのファイルダウンロードURLとパスワードが自動で挿入されます。
                                    <br />
                                    <a
                                        target="_blank"
                                        href="https://intercom.help/cmrb/ja/articles/6065171-%E6%B7%BB%E4%BB%98%E5%BD%A2%E5%BC%8F%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled
                                style={{
                                    color: iconCustomColor,
                                }}
                                className={styles.tooltip}
                            />
                        </Tooltip>
                    </span>
                }
                className={styles.field}
                name="file_type"
                initialValue="file">
                {isNotRegistered ? (
                    <Radio.Group disabled>
                        <Tooltip
                            title={
                                <span>
                                    基本情報にてSMTPサーバーの設定を行ってください。
                                    <br />
                                    SMTPサーバーの設定後、ファイル形式の設定を変更することが可能です。
                                </span>
                            }>
                            <Radio value={1}>ダウンロードURL</Radio>
                        </Tooltip>
                        <Tooltip
                            title={
                                <span>
                                    基本情報にてSMTPサーバーの設定を行ってください。
                                    <br />
                                    SMTPサーバーの設定後、ファイル形式の設定を変更することが可能です。
                                </span>
                            }>
                            <Radio value={2}>添付</Radio>
                        </Tooltip>
                    </Radio.Group>
                ) : (
                    <Radio.Group>
                        <Radio value={1}>ダウンロードURL</Radio>
                        <Radio value={2}>添付</Radio>
                    </Radio.Group>
                )}
            </Form.Item>
            <Form.Item
                {...detailSettingFormLayout}
                label={
                    <span>
                        開封情報の取得&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    <TooltipContentLink
                                        to={`${Paths.addons}`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        title="アドオン"
                                    />
                                    を追加することで開封情報の取得が可能です。
                                    <br />
                                    <a
                                        href={
                                            Links.helps.addon.scheduledEmails
                                                .htmlOpenInfo
                                        }
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled
                                style={{
                                    color: iconCustomColor,
                                }}
                                className={styles.tooltip}
                            />
                        </Tooltip>
                    </span>
                }
                className={styles.field}
                name="is_open_count_available">
                <Radio.Group disabled>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={false}>なし</Radio>
                    </Tooltip>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={true}>あり</Radio>
                    </Tooltip>
                </Radio.Group>
            </Form.Item>
            <Form.Item shouldUpdate noStyle>
                {() =>
                    form.getFieldsValue().is_open_count_available && (
                        <Form.Item
                            {...detailSettingIndent2FormLayout}
                            label={
                                <span>
                                    開封情報の取得期間&nbsp;
                                    <Tooltip
                                        title={
                                            <span>
                                                開封情報の取得期間が選択されています。
                                                <br />
                                                <TooltipContentLink
                                                    to={`${Paths.addons}`}
                                                    title="アドオン"
                                                />
                                                を追加することで取得期間を延長することが可能です。
                                                <br />
                                                <a
                                                    href={
                                                        Links.helps.addon
                                                            .scheduledEmails
                                                            .htmlOpenInfoReceivePeriod
                                                    }
                                                    target="_blank"
                                                    rel="noopener noreferrer">
                                                    詳細
                                                </a>
                                            </span>
                                        }>
                                        <QuestionCircleFilled
                                            style={{
                                                color: iconCustomColor,
                                            }}
                                            className={styles.tooltip}
                                        />
                                    </Tooltip>
                                </span>
                            }
                            className={styles.field}
                            name="is_open_count_extra_period_available">
                            <Radio.Group disabled>
                                <Tooltip
                                    title={
                                        TooltipMessages.scheduledEmailSetting
                                            .addons
                                    }>
                                    <Radio value={false}>72時間</Radio>
                                </Tooltip>
                                <Tooltip
                                    title={
                                        TooltipMessages.scheduledEmailSetting
                                            .addons
                                    }>
                                    <Radio value={true}>30日</Radio>
                                </Tooltip>
                            </Radio.Group>
                        </Form.Item>
                    )
                }
            </Form.Item>
            <Form.Item
                {...detailSettingFormLayout}
                label={
                    <span>
                        配信添付容量の上限&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    配信添付容量の上限が選択されています。
                                    <br />
                                    <TooltipContentLink
                                        to={`${Paths.addons}`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        title="アドオン"
                                    />
                                    を追加することで配信添付容量のアップグレードが可能です。
                                    <br />
                                    <a
                                        href={
                                            Links.helps.addon.scheduledEmails
                                                .attachmentSize
                                        }
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled
                                style={{
                                    color: iconCustomColor,
                                }}
                                className={styles.tooltip}
                            />
                        </Tooltip>
                    </span>
                }
                className={styles.field}
                name="is_use_attachment_max_size_available">
                <Radio.Group disabled>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={false}>2MB</Radio>
                    </Tooltip>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={true}>10MB</Radio>
                    </Tooltip>
                </Radio.Group>
            </Form.Item>
            <Form.Item
                {...detailSettingFormLayout}
                label={
                    <span>
                        HTML配信時の広告表示&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    <TooltipContentLink
                                        to={`${Paths.addons}`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        title="アドオン"
                                    />
                                    を追加することでHTML配信時の広告を非表示にすることが可能です。
                                    <br />
                                    <a
                                        href={
                                            Links.helps.addon.scheduledEmails
                                                .removeCmrbAdd
                                        }
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled
                                style={{
                                    color: iconCustomColor,
                                }}
                                className={styles.tooltip}
                            />
                        </Tooltip>
                    </span>
                }
                className={styles.field}
                name="is_use_remove_promotion_available">
                <Radio.Group disabled>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={false}>表示</Radio>
                    </Tooltip>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={true}>非表示</Radio>
                    </Tooltip>
                </Radio.Group>
            </Form.Item>
            <Form.Item
                {...detailSettingFormLayout}
                label={
                    <span>
                        配信件数の上限&nbsp;
                        <Tooltip
                            title={
                                <span>
                                    配信メール1通あたりの宛先の上限件数が選択されています。
                                    <br />
                                    <TooltipContentLink
                                        to={`${Paths.addons}`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        title="アドオン"
                                    />
                                    の追加回数に応じて宛先の上限件数を追加することが可能です。
                                    <br />
                                    <a
                                        href={
                                            Links.helps.addon.scheduledEmails
                                                .sendLimit
                                        }
                                        target="_blank"
                                        rel="noopener noreferrer">
                                        詳細
                                    </a>
                                </span>
                            }>
                            <QuestionCircleFilled
                                style={{
                                    color: iconCustomColor,
                                }}
                                className={styles.tooltip}
                            />
                        </Tooltip>
                    </span>
                }
                className={styles.field}
                name="target_count_addon_purchase_count">
                <Radio.Group disabled>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={0}>1000件</Radio>
                    </Tooltip>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={1}>2000件</Radio>
                    </Tooltip>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={2}>3000件</Radio>
                    </Tooltip>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={3}>4000件</Radio>
                    </Tooltip>
                    <Tooltip
                        title={TooltipMessages.scheduledEmailSetting.addons}>
                        <Radio value={4}>5000件</Radio>
                    </Tooltip>
                </Radio.Group>
            </Form.Item>
            <Form.Item shouldUpdate noStyle>
                {() => (
                    <Form.Item
                        {...detailSettingFormLayout}
                        label={
                            <span>
                                配信予約時間の間隔&nbsp;
                                <Tooltip
                                    title={
                                        <span>
                                            配信予約時間の間隔が選択されています。
                                            <br />
                                            <TooltipContentLink
                                                to={`${Paths.addons}`}
                                                target="_blank"
                                                rel="noopener noreferrer"
                                                title="アドオン"
                                            />
                                            を追加することで配信間隔の時短が可能です。
                                            <br />
                                            <a
                                                href={
                                                    Links.helps.addon
                                                        .scheduledEmails
                                                        .deliveryInterval
                                                }
                                                target="_blank"
                                                rel="noopener noreferrer">
                                                詳細
                                            </a>
                                        </span>
                                    }>
                                    <QuestionCircleFilled
                                        style={{
                                            color: iconCustomColor,
                                        }}
                                    />
                                </Tooltip>
                            </span>
                        }
                        className={styles.field}
                        name="is_use_delivery_interval_available">
                        <Radio.Group disabled>
                            <Tooltip
                                title={
                                    TooltipMessages.scheduledEmailSetting.addons
                                }>
                                <Radio value={false}>30分単位</Radio>
                            </Tooltip>
                            <Tooltip
                                title={
                                    TooltipMessages.scheduledEmailSetting.addons
                                }>
                                <Radio value={true}>10分単位</Radio>
                            </Tooltip>
                        </Radio.Group>
                    </Form.Item>
                )}
            </Form.Item>
        </Form>
    );
};

export default ScheduledEmailSettingDetailSettingsForm;
