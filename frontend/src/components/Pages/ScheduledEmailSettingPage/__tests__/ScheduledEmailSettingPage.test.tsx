import React from "react";
import { renderHook, renderWithAllProviders, screen } from "~/test/utils";
import ScheduledEmailSettingPage from "../ScheduledEmailSettingPage";
import { scheduledEmailSettingMockData } from "~/test/mock/scheduledEmailSettingMock";
import { useClient } from "~/hooks/useClient";
import { generateRandomToken } from "~/utils/utils";
import login, { LoginInitialState } from "~/reducers/login";
import { configureStore } from "@reduxjs/toolkit";

describe("ScheduledEmailSettingPage.tsx", () => {
    beforeAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() =>
            result.current.setTokenToClient(generateRandomToken())
        );
    });

    afterAll(async () => {
        const { result, waitFor } = renderHook(() => useClient());
        await waitFor(() => result.current.removeTokenFromClient());
    });

    test("render test", async () => {
        renderWithAllProviders(<ScheduledEmailSettingPage />, {
            store: configureStore({
                reducer: {
                    login,
                },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                        authorizedActions: {
                            ...LoginInitialState.authorizedActions,
                            scheduled_email_settings: {
                                _all: true,
                            },
                        },
                    },
                },
            }),
        });
        const pageTitleElement = screen.getByText(/配信メール設定/);
        expect(pageTitleElement).toBeInTheDocument();
        const modifiedTimeLabelElement = screen.getByText(/更新日時:/);
        expect(modifiedTimeLabelElement).toBeInTheDocument();
        const modifiedTimeElement = await screen.findByText(
            new RegExp(scheduledEmailSettingMockData.modified_time, "g")
        );
        expect(modifiedTimeElement).toBeInTheDocument();
        const modifiedUserLabelElement = screen.getByText(/更新者:/);
        expect(modifiedUserLabelElement).toBeInTheDocument();
        const modifiedUserElement = await screen.findByText(
            new RegExp(scheduledEmailSettingMockData.modified_user, "g")
        );
        expect(modifiedUserElement).toBeInTheDocument();
    });

    describe("authorization test", () => {
        test("authorized", () => {
            renderWithAllProviders(<ScheduledEmailSettingPage />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                scheduled_email_settings: {
                                    _all: true,
                                },
                            },
                        },
                    },
                }),
            });
            const notFoundTitleElement =
                screen.queryByText(/^ページが見つかりません$/);
            expect(notFoundTitleElement).not.toBeInTheDocument();
        });

        test("unauthorized", () => {
            renderWithAllProviders(<ScheduledEmailSettingPage />, {
                store: configureStore({
                    reducer: {
                        login,
                    },
                    preloadedState: {
                        login: {
                            ...LoginInitialState,
                            authorizedActions: {
                                ...LoginInitialState.authorizedActions,
                                scheduled_email_settings: {
                                    _all: false,
                                },
                            },
                        },
                    },
                }),
            });
            const notFoundTitleElement =
                screen.getByText(/^ページが見つかりません$/);
            expect(notFoundTitleElement).toBeInTheDocument();
        });
    });
});
