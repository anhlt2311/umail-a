import React, { ReactNode } from "react";
import {
    Alert,
    Button,
    Card,
    Col,
    ColProps,
    Row,
    Spin,
    Typography,
} from "antd";
import CustomPageHeader from "~/components/Common/CustomPageHeader/CustomPageHeader";
import AddonCard from "./AddonCard/AddonCard";
import { AddonMasterModel } from "~/models/addonModel";
import {
    useFetchAddonMasterAPIQuery,
    useFetchPurchasedAddonsAPIQuery,
} from "~/hooks/useAddon";
import _ from "lodash";
import { useFetchUserPlanInfoAPIQuery } from "~/hooks/usePlan";
import { useAuthorizedActions } from "~/hooks/useAuthorizedActions";
import { Links } from "~/utils/constants";

const { Title, Paragraph, Link } = Typography;

const cols: ColProps = {
    xs: 24,
    sm: 12,
    md: 12,
    xl: 12,
    xxl: 12,
};

const AddonsPage = () => {
    const messages: ReactNode[] = [
        <span>アドオンを追加するとその時点で決済の処理が行われます。</span>,
        <span>
            アドオンの利用期限は、アドオンの決済日に関わらずご利用中のプランの利用期限と同一となります。
            <Link target="_blank" rel="noopener noreferrer" href={Links.helps.payments.addonCalculation}>
                詳細
            </Link>
        </span>,
        <span>
            アドオンを削除すると利用期限に関わらず削除時点でご利用いただけなくなります。
        </span>,
    ];

    const { purchase: purchaseAuthorized } = useAuthorizedActions("addon");
    const { data: addonMasterList, isLoading: isAddonMasterListLoading } =
        useFetchAddonMasterAPIQuery({});
    const { data: purchasedAddonList, isLoading: isPurchasedAddonListLoading } =
        useFetchPurchasedAddonsAPIQuery({});
    const { data: userPlanInfo } = useFetchUserPlanInfoAPIQuery({});

    const isFreeTrial = userPlanInfo?.planMasterId === 0;

    const getNumberOfPossiblePurchase = (addon: AddonMasterModel) => {
        const addons = purchasedAddonList ?? [];
        const numberOfPossiblePurchase =
            addon.limit -
            addons.filter(
                (purchasedAddon) => purchasedAddon.addonMasterId === addon.id
            ).length;
        return numberOfPossiblePurchase;
    };

    const renderTosLink = () => {
        return (
            <Button type="link" href={Links.services.addonTos} key="addonTosBtn" target="_blank" rel="noopener noreferrer">
                アドオン利用規約
            </Button>
        );
    };

    const renderAddonCell = (
        addonMaster: AddonMasterModel,
        disableAllFunction?: boolean,
        tooltipMessage?: string
    ) => {
        const numberOfPossiblePurchase =
            getNumberOfPossiblePurchase(addonMaster);
        const purchasedAddonItem = _.find(
            purchasedAddonList,
            (item) => item.addonMasterId === addonMaster.id
        );

        return (
            <Col {...cols} key={addonMaster.id}>
                <Row justify="center" key="addonCellRow">
                    <Col span={18} key="addonCard">
                        <Row justify="center" key="addonCardRow">
                            <AddonCard
                                addon={purchasedAddonItem}
                                addonMaster={addonMaster}
                                numberOfPossiblePurchase={
                                    numberOfPossiblePurchase
                                }
                                purchaseAuthorized={purchaseAuthorized}
                                disableAllFunction={disableAllFunction}
                                tooltipMessage={tooltipMessage}
                            />
                        </Row>
                    </Col>
                </Row>
            </Col>
        );
    };

    const renderRecommendedAddons = () => {
        const addons = addonMasterList ?? [];
        const recommendedAddons = addons.filter((addon) => addon.isRecommended);
        return recommendedAddons.map((addon) => {
            if (isFreeTrial) {
                return renderAddonCell(
                    addon,
                    true,
                    "無料トライアル中にこの機能はご利用いただけません。"
                );
            }
            const parentAddon = addons.find((addon) => addon.id === 4);
            if (addon.id === 5 && parentAddon) {
                const numberOfPossiblePurchase =
                    getNumberOfPossiblePurchase(parentAddon);
                return renderAddonCell(
                    addon,
                    !!numberOfPossiblePurchase && addon.id === 5,
                    "アドオン「配信開封情報の取得」を追加してから購入できます。"
                );
            }
            const childAddon = addons.find((addon) => addon.id === 5);
            if (addon.id === 4 && childAddon) {
                const numberOfPossiblePurchase =
                    getNumberOfPossiblePurchase(childAddon);
                return renderAddonCell(
                    addon,
                    !numberOfPossiblePurchase && addon.id === 4,
                    "アドオン「配信開封情報の取得期間の延長」を削除してから削除できるようになります。"
                );
            }
            return renderAddonCell(addon);
        });
    };

    const renderOrganizationsAddons = () => {
        const addons = addonMasterList ?? [];
        return addons
            .filter((addon) => addon.isOrganizations)
            .map((addon) => {
                if (isFreeTrial) {
                    return renderAddonCell(
                        addon,
                        true,
                        "無料トライアル中にこの機能はご利用いただけません。"
                    );
                }
                return renderAddonCell(addon);
            });
    };

    const renderScheduledMailsAddons = () => {
        const addons = addonMasterList ?? [];
        const scheduledEmailAddons = addons.filter(
            (addon) => addon.isScheduledMails
        );
        return scheduledEmailAddons.map((addon) => {
            if (isFreeTrial) {
                return renderAddonCell(
                    addon,
                    true,
                    "無料トライアル中にこの機能はご利用いただけません。"
                );
            }
            const parentAddon = addons.find((addon) => addon.id === 4);
            if (addon.id === 5 && parentAddon) {
                const numberOfPossiblePurchase =
                    getNumberOfPossiblePurchase(parentAddon);
                return renderAddonCell(
                    addon,
                    !!numberOfPossiblePurchase && addon.id === 5,
                    "アドオン「配信開封情報の取得」を追加してから購入できます。"
                );
            }
            const childAddon = addons.find((addon) => addon.id === 5);
            if (addon.id === 4 && childAddon) {
                const numberOfPossiblePurchase =
                    getNumberOfPossiblePurchase(childAddon);
                return renderAddonCell(
                    addon,
                    !numberOfPossiblePurchase && addon.id === 4,
                    "アドオン「配信開封情報の取得期間の延長」を削除してから削除できるようになります。"
                );
            }

            return renderAddonCell(addon);
        });
    };

    const renderSharedMailsAddons = () => {
        const addons = addonMasterList ?? [];
        return addons
            .filter((addon) => addon.isSharedMails)
            .map((addon) => {
                if (isFreeTrial) {
                    return renderAddonCell(
                        addon,
                        true,
                        "無料トライアル中にこの機能はご利用いただけません。"
                    );
                }
                return renderAddonCell(addon);
            });
    };

    const renderMyCompanySettingAddons = () => {
        const addons = addonMasterList ?? [];
        return addons
            .filter((addon) => addon.isMyCompanySetting)
            .map((addon) => {
                if (isFreeTrial) {
                    return renderAddonCell(
                        addon,
                        true,
                        "無料トライアル中にこの機能はご利用いただけません。"
                    );
                }
                return renderAddonCell(addon);
            });
    };

    const renderAlertDescription = () => {
        return (
            <div>
                <Paragraph>
                    <ul
                        style={{
                            listStyleType: "disc",
                            textAlign: "left",
                        }}>
                        {messages.map((message, i) => (
                            <li key={i}
                                style={{
                                    marginBottom: "5px",
                                }}>
                                {message}
                            </li>
                        ))}
                    </ul>
                </Paragraph>
            </div>
        );
    };

    const renderAddonAlert = () => {
        return (
            <Col span={24} style={{ marginBottom: "3%" }} key="addonAlert">
                <Row justify="center" key="addonAlertRow">
                    <Col span={14} key="alert">
                        <Alert
                            style={{ textAlign: "left" }}
                            message="アドオンの決済と利用期間について"
                            description={renderAlertDescription()}
                            type="info"
                            showIcon
                        />
                    </Col>
                </Row>
            </Col>
        );
    };

    return (
        <Spin
            spinning={isAddonMasterListLoading || isPurchasedAddonListLoading}>
            <CustomPageHeader title="アドオン" extra={[renderTosLink()]} />
            {renderAddonAlert()}
            <Card>
                <Col span={24} key="card">
                    <Row justify="start" key="cardRow">
                        <Title level={4}>おすすめ</Title>
                    </Row>
                    <Row key="recommendedAddons">{renderRecommendedAddons()}</Row>
                    <div style={{ height: 50 }}></div>
                    <Row justify="start">
                        <Title level={4}>取引先管理</Title>
                    </Row>
                    <Row key="organizationsAddons">{renderOrganizationsAddons()}</Row>
                    <div style={{ height: 50 }}></div>
                    <Row justify="start">
                        <Title level={4}>配信メール管理</Title>
                    </Row>
                    <Row key="scheduledEmailAddons">{renderScheduledMailsAddons()}</Row>
                    {/* 対象になるアドオンが存在しないため、スタンダードプラン開始までは非表示
                    <div style={{ height: 50 }}></div>
                    <Row justify="start">
                        <Title level={4}>共有メール管理</Title>
                    </Row>
                    <Row>{renderSharedMailsAddons()}</Row>
                    <div style={{ height: 50 }}></div>
                    <Row justify="start">
                        <Title level={4}>自社設定</Title>
                    </Row>
                    <Row>{renderMyCompanySettingAddons()}</Row>
                    */}
                </Col>
            </Card>
        </Spin>
    );
};

export default AddonsPage;
