import {
    Badge, Button,
    Card, Checkbox, Col,
    Row,
    Tag, Tooltip, Typography
} from "antd";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import moment from "moment";
import React, { useState, useEffect } from "react";
import LinkIcon from "~/components/Common/LinkIcon/LinkIcon";
import { confirmModal } from "~/components/Modals/ConfirmModal";
import GenericModal from "~/components/Modals/GenericModal/GenericModal";
import {
    usePurchaseAddonAPIMutation,
    useRevokeAddonAPIMutation
} from "~/hooks/useAddon";
import { AddonMasterModel, AddonPurchaseItemModel } from "~/models/addonModel";
import { usePlanSummaryAPIQuery } from "~/hooks/usePlan";
const numberFormatter = new Intl.NumberFormat();

const { Paragraph, Title, Text, Link } = Typography;

type Props = {
    addon?: AddonPurchaseItemModel;
    addonMaster: AddonMasterModel;
    numberOfPossiblePurchase: number;
    purchaseAuthorized: boolean;
    disableAllFunction?: boolean;
    tooltipMessage?: string;
};

const AddonCard = ({
    addon,
    addonMaster: {
        id: masterId,
        price,
        title,
        targets,
        description,
        limit,
        helpUrl,
    },
    numberOfPossiblePurchase,
    purchaseAuthorized,
    disableAllFunction,
    tooltipMessage,
}: Props) => {
    const { mutate: purchaseAddon } = usePurchaseAddonAPIMutation();
    const { mutate: revokeAddon } = useRevokeAddonAPIMutation();

    const [isConfirmed, setIsConfirmed] = useState(false);
    const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
    const { data: planSummary } = usePlanSummaryAPIQuery({});
    const [isPlanNameEmpty, setIsPlanNameEmpty] = useState(false)

    useEffect(() => {
        setIsPlanNameEmpty(!planSummary?.planName)
    }, [planSummary])

    const onDeleteModalOk = () => {
        if (addon) {
            revokeAddon(addon.id);
        }
        onDeleteModalCancel();
    };

    const onDeleteModalCancel = () => {
        setIsConfirmed(false);
        setIsDeleteModalOpen(false);
    };

    const onCheck = (event: CheckboxChangeEvent) => {
        const checked = event.target.checked;
        setIsConfirmed(checked);
    };

    const onAdd = () => {
        confirmModal({
            title: "このアドオンを購入しますか？",
            content: (
                <>
                    <Title level={5} style={{ marginTop: 15 }}>
                        {title}
                    </Title>
                    <Paragraph
                        ellipsis={{
                            rows: 3,
                            expandable: true,
                        }}
                        style={{
                            textAlign: "left",
                        }}>
                        {description}
                    </Paragraph>
                </>
            ),
            onOk: () =>
                purchaseAddon({
                    addonMasterId: masterId,
                }),
        });
    };

    const getAdditionalMessage = () => {
        switch (masterId) {
            case 1:
            case 2:
                return "※削除後の上限数より多くテンプレートを登録しているユーザーがいると実行されません。";
            case 4:
                return "※削除と同時に配信開封情報の取得は停止されます。<br />※削除前に取得した配信開封情報は引き続き閲覧可能です。";
            case 5:
                return "※削除と同時に配信完了から72時間以上30日以下のメールの配信開封情報取得が停止されます。<br />※削除前に取得した配信開封情報は引き続き閲覧可能です。";
            case 6:
                return "※2MB以上（ヘッダー情報を含む）の添付容量、かつ配信ステータスが「下書き」「配信待ち」「配信中」のメールが配信メール予約に登録されていると実行されません。";
            case 7:
                return "※HTMLフォーマット、かつ配信ステータスが「下書き」「配信待ち」「配信中」のメールが配信メール予約に登録されていると実行されません。";
            case 8:
                return "※削除後の配信上限件数を超過、かつ配信ステータスが「下書き」「配信待ち」「配信中」のメールが配信メール予約に登録されていると実行されません。";
            case 9:
                return "※時短機能（配信時刻が10、20、40、50分）を指定、かつ配信ステータスが「下書き」「配信待ち」「配信中」のメールが配信メール予約に登録されていると実行されません。";
            default:
                return "";
        }
    };

    const renderDeleteModal = () => {
        const additional_message = getAdditionalMessage();

        return (
            <GenericModal
                visible={isDeleteModalOpen}
                title="このアドオンを削除しますか？"
                type="warning"
                onOk={onDeleteModalOk}
                okButtonProps={{ disabled: !isConfirmed }}
                onCancel={onDeleteModalCancel}>
                <>
                    <Paragraph
                        ellipsis={{
                            rows: 13,
                            expandable: true,
                        }}
                        style={{
                            textAlign: "left",
                        }}>
                        OKを押すと即時削除され、対象期間内でも利用できなくなります。
                        <br />
                        元には戻せません。
                        <br />
                        <br />
                        <span
                            dangerouslySetInnerHTML={{
                                __html: additional_message,
                            }}></span>
                    </Paragraph>
                    <Checkbox checked={isConfirmed} onChange={onCheck}>
                        即時削除に同意する
                    </Checkbox>
                </>
            </GenericModal>
        );
    };

    const onRevoke = () => {
        setIsDeleteModalOpen(true);
    };

    const renderAddonLimit = () => {
        return (
            <Col span={24}>
                <Row justify="center" gutter={5}>
                    <Col>購入可能回数:</Col>
                    <Col>
                        <Tag>
                            {numberOfPossiblePurchase} <span>/</span> {limit}
                        </Tag>
                    </Col>
                </Row>
            </Col>
        );
    };

    const renderTitleNode = () => {
        return (
            <Row justify="end" >
                {addon && addon.nextPaymentDate ? (
                    <Text type="secondary" style={{ fontSize: 10 }}>
                        次回決済日：
                        {moment(addon.nextPaymentDate).format("YYYY-MM-DD")}
                    </Text>
                ) : undefined}
            </Row>
        );
    };

    const renderAddButton = (disabled = false) => {
        let buttonRender;
        const button = (
            <Button
                onClick={disabled ? () => {} : onAdd}
                type="primary"
                size="small"
                disabled={disableAllFunction || disabled || isPlanNameEmpty}>
                追加
            </Button>
        );

        if (isPlanNameEmpty) {
            buttonRender = <Tooltip title="無料トライアル中はアドオンの購入はできません。">{button}</Tooltip>;
        }else if(disableAllFunction) {
            buttonRender = <Tooltip title={tooltipMessage}>{button}</Tooltip>;
        }else{
            buttonRender = button;
        }
        
        return (
            <Col>
                { buttonRender }
            </Col>
        );
    };

    const renderDeleteButton = (disabled = false) => {
        let buttonRender;
        const button = (
            <Button
                onClick={disabled ? () => {} : onRevoke}
                type="primary"
                size="small"
                danger
                disabled={disableAllFunction || disabled || isPlanNameEmpty}>
                削除
            </Button>
        );

        if (isPlanNameEmpty) {
            buttonRender = <Tooltip title="無料トライアル中はアドオンの購入はできません。">{button}</Tooltip>;
        }else if(disableAllFunction) {
            buttonRender = <Tooltip title={tooltipMessage}>{button}</Tooltip>;
        }else{
            buttonRender = button;
        }

        return (
            <Col>
                { buttonRender }
            </Col>
        );
    };

    const renderTargets = () => {
        return targets.map((target) => (
            <Tag key={target} style={{ marginTop: 3 }}>
                {target}
            </Tag>
        ));
    };

    const renderAddonControlButtons = () => {
        if (purchaseAuthorized) {
            return (
                <Row justify="center" gutter={7}>
                    {renderAddButton(!numberOfPossiblePurchase)}
                    {renderDeleteButton(numberOfPossiblePurchase === limit)}
                </Row>
            );
        } else {
            return (
                <Row justify="center" gutter={4}>
                    <Tooltip title={"特定の権限で操作できます"}>
                        {renderAddButton(true)}
                    </Tooltip>
                    <Tooltip title={"特定の権限で操作できます"}>
                        {renderDeleteButton(true)}
                    </Tooltip>
                </Row>
            );
        }
    };

    return (
        <>
            <Col span={24}>
                <Badge.Ribbon
                    text={`¥${numberFormatter.format(price)} (税抜) /月`}
                    placement="start"
                    color={ !purchaseAuthorized || disableAllFunction || !numberOfPossiblePurchase || isPlanNameEmpty ? "grey" : "green"}
                    style={{ top: 30 }}
                >
                    <Card
                        title={renderTitleNode()}
                        actions={[renderAddonLimit(), renderAddonControlButtons()]}
                        style={{
                            marginTop: 15,
                            marginBottom: 15,
                            width: "100%",
                        }}
                        headStyle={{
                            marginTop: 15
                        }}>
                        <Card.Meta
                            title={
                                <Row justify="space-between">
                                    <Col>
                                        <Title
                                            level={5}
                                            disabled={
                                                !purchaseAuthorized || 
                                                disableAllFunction ||
                                                !numberOfPossiblePurchase ||
                                                isPlanNameEmpty
                                            }>
                                            {title}
                                        </Title>
                                    </Col>
                                    <Col>
                                        <Link
                                            href={helpUrl}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                            style={{
                                                fontSize: 12,
                                                fontWeight: "normal",
                                            }}>
                                            <Row align="middle">
                                                <Col>
                                                    <LinkIcon />
                                                </Col>
                                                <Col>
                                                    <span>設定ガイド</span>
                                                </Col>
                                            </Row>
                                        </Link>
                                    </Col>
                                </Row>
                            }
                            style={{
                                textAlign: "left",
                                textOverflow: "ellipsis",
                                overflow: "hidden",
                                marginBottom: 10,
                            }}
                        />
                        <Row>
                            <Paragraph
                                ellipsis={{
                                    rows: 3,
                                    expandable: false,
                                }}
                                style={{
                                    height: 60,
                                    textAlign: "left",
                                }}
                                disabled={
                                    !purchaseAuthorized || disableAllFunction || !numberOfPossiblePurchase || isPlanNameEmpty
                                }>
                                {description}
                            </Paragraph>
                        </Row>
                        <Row style={{ marginTop: 2, marginBottom: 5 }}>
                            {renderTargets()}
                        </Row>
                    </Card>
                </Badge.Ribbon>
            </Col>
            {renderDeleteModal()}
        </>
    );
};

export default AddonCard;
