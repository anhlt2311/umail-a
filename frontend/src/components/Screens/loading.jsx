import React from 'react';
import { Spin } from 'antd';

const Loading = () => (
  <div className="loadingScreen">
    <Spin />
  </div>
);

export default Loading;
