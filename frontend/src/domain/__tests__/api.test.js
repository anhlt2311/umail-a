import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import {
    MyAPI,
    BadRequestError,
    ServerError,
    ClientError,
    ConnectionError,
} from "../api";

const token = "my_auth_token";
const mock = new MockAdapter(axios);

// Sample data for the mock.
const getResponseData = { name: "taro", email: "taro@example.com" };
const listTotalCount = 100;
const searchTotalCount = 1;

const listResponseData = {
    count: listTotalCount, // Total Count
    next: "http://example.com/resource?page=2&page_size=1",
    previous: null,
    results: [getResponseData],
};

const searchResponseData = {
    count: searchTotalCount, // Total Count
    next: null,
    previous: null,
    results: [getResponseData],
};

const PostData = { name: "taro", email: "taro@example.com" };
const UpdatedData = { name: "taro", email: "newtaro@example.com" };
const ValidationErrorResponse = {
    status: 400,
    data: {
        detail: "This field might be exists",
        mail_id: ["This field is required."],
        from_address: ["This field is required."],
    },
};

beforeEach(() => {
    mock.reset(); // Reset onXXXX operations.
});

afterAll(() => mock.restore()); // Completely removes mock.

// test starts from here
// TODO: Append a test that checks whether an API client is successfully configured.
test("GET should return data when a get success", () => {
    // Mock definition
    mock.onGet("").reply(200, getResponseData);

    // Instantiate API client.
    const api = new MyAPI("http://example.com/resource", token);

    // Execute a test.
    return api
        .get()
        .then((result) => expect(result.data).toEqual(getResponseData));
});

test("GET should return converted data when a responseConverter has given.", () => {
    // Mock definition
    mock.onGet("").reply(200, getResponseData);

    // Instantiate API client.
    const responseConverter = (data) => ({
        newName: data.name,
        newEmail: data.email,
    });
    const api = new MyAPI(
        "http://example.com/resource",
        token,
        responseConverter
    );

    // Execute a test.
    return api.get().then((result) =>
        expect(result.data).toEqual({
            newName: getResponseData.name,
            newEmail: getResponseData.email,
        })
    );
});

test("POST should return data when a post success", () => {
    // Mock definition
    mock.onPost("").reply(200, PostData);

    // Instantiate API client.
    const api = new MyAPI("http://example.com/resource", token);

    // Execute a test.
    return api
        .post(PostData)
        .then((result) => expect(result.data).toEqual(PostData));
});

test("POST should return converted data when a responseConverter has given.", () => {
    // Mock definition
    mock.onPost("").reply(200, PostData);

    // Instantiate API client.
    const responseConverter = (data) => ({
        newName: data.name,
        newEmail: data.email,
    });
    const api = new MyAPI(
        "http://example.com/resource",
        token,
        responseConverter
    );

    // Execute a test.
    return api.post(PostData).then((result) =>
        expect(result.data).toEqual({
            newName: PostData.name,
            newEmail: PostData.email,
        })
    );
});

test("PATCH should return data when a patch success", () => {
    // This is an error mock definition of axios.

    // Instantiate API client.
    const api = new MyAPI("http://example.com/resource", token);

    // Mock definition
    mock.onPatch("/resourceid").reply(200, UpdatedData);

    // Execute a test.
    return api
        .patch("resourceid", UpdatedData) // resource_id is optional.
        .then((result) => expect(result.data).toEqual(UpdatedData));
});

test("PATCH should return converted data when a responseConverter has given.", () => {
    // This is an error mock definition of axios.

    // Instantiate API client.
    const responseConverter = (data) => ({
        newName: data.name,
        newEmail: data.email,
    });
    const api = new MyAPI(
        "http://example.com/resource",
        token,
        responseConverter
    );

    // Mock definition
    mock.onPatch("/resourceid").reply(200, UpdatedData);

    // Execute a test.
    return api
        .patch("resourceid", UpdatedData) // resource_id is optional.
        .then((result) =>
            expect(result.data).toEqual({
                newName: UpdatedData.name,
                newEmail: UpdatedData.email,
            })
        );
});

test("LIST should return data and total count when a list success", () => {
    // Instantiate API client.
    const api = new MyAPI("http://example.com/resource", token);
    const pageNumber = 1;
    const pageSize = 1;

    // Mock definition
    mock.onGet(`?page=${pageNumber}&page_size=${pageSize}`).reply(
        200,
        listResponseData
    );

    // Execute a test.
    return api
        .list(pageNumber, pageSize) // resource_id is optional.
        .then((result) => {
            // Check types of result.
            expect(result).toBeInstanceOf(Object);
            expect(result.data).toBeInstanceOf(Array);

            // Check data of result.
            expect(result.data[0]).toEqual(getResponseData); // expect result.data is array.
            expect(result.total).toEqual(listTotalCount);
        });
});

test("SEARCH should return data and total count when a search success", () => {
    // Instantiate API client.
    const api = new MyAPI("http://example.com/resource", token);
    const searchParams = { name: "taro" };
    const keyConverter = (params) => params; // in this case, this does nothing.
    const pageNumber = 1;
    const pageSize = 1;

    // Mock definition
    mock.onGet(
        `?name=${searchParams.name}&page=${pageNumber}&page_size=${pageSize}`
    ).reply(200, searchResponseData);

    // Execute a test.
    return api
        .search(searchParams, keyConverter, pageNumber, pageSize) // resource_id is optional.
        .then((result) => {
            // Check types of result.
            expect(result).toBeInstanceOf(Object);
            expect(result.data).toBeInstanceOf(Array);

            // Check data of result.
            expect(result.data[0]).toEqual(getResponseData); // expect result.data is an array.
            expect(result.total).toEqual(searchTotalCount);
        });
});

test("POST should throw BadRequestError when a response status is 400", () => {
    const error = {
        // This is an error mock definition of axios.
        request: {},
        response: ValidationErrorResponse,
        message: "This is an error message generated by mock.",
    }; // TODO: Replace this by a custom class instance that implements an interface of AxiosError. (need Typescript.)

    const api = new MyAPI("http://example.com/resource", token);

    // Mock definition
    const fnMock = jest.fn();
    fnMock.mockRejectedValue(error);
    api.client.post = fnMock; // Patching axios.

    // Execute a test.
    return api.post("").catch((err) => {
        expect(err).toBeInstanceOf(BadRequestError);
        expect(typeof err.message).toBe("string");
        expect(typeof err.code).toBe("string");
        expect(err.code).toEqual("ERR_400");
        expect(typeof err.detail).toBe("string");
        expect(err.detail).toEqual(ValidationErrorResponse.data.detail);
        expect(err.field_errors).toBeInstanceOf(Object);
        expect(err.field_errors).toEqual(ValidationErrorResponse.data);
    });
});

// TODO: Remove duplications between this test and the above test.
test(" PATCH should throw BadRequestError when a response status is 400", () => {
    const error = {
        // This is an error mock definition of axios.
        request: {},
        response: ValidationErrorResponse,
        message: "This is an error message generated by mock.",
    }; // TODO: Replace this by a custom class instance that implements an interface of AxiosError. (need Typescript.)

    const api = new MyAPI("http://example.com/resource", token);

    // Mock definition
    const fnMock = jest.fn();
    fnMock.mockRejectedValue(error);
    api.client.patch = fnMock; // Patching axios.

    // Execute a test.
    return api.patch("").catch((err) => {
        expect(err).toBeInstanceOf(BadRequestError);
        expect(typeof err.message).toBe("string");
        expect(typeof err.code).toBe("string");
        expect(err.code).toEqual("ERR_400");
        expect(typeof err.detail).toBe("string");
        expect(err.detail).toEqual(ValidationErrorResponse.data.detail);
        expect(err.field_errors).toBeInstanceOf(Object);
        expect(err.field_errors).toEqual(ValidationErrorResponse.data);
    });
});

test("API should throw BadRequestError that has converted field errors when responseConverter is given", () => {
    const error = {
        // This is an error mock definition of axios.
        request: {},
        response: ValidationErrorResponse,
        message: "This is an error message generated by mock.",
    }; // TODO: Replace this by a custom class instance that implements an interface of AxiosError. (need Typescript.)

    const responseConverter = (data) => ({
        id: data.mail_id,
        email: data.from_address,
    });
    const api = new MyAPI(
        "http://example.com/resource",
        token,
        responseConverter
    );

    // Mock definition
    const fnMock = jest.fn();
    fnMock.mockRejectedValue(error);
    api.client.post = fnMock; // Patching axios.

    // Execute a test.
    return api.post("").catch((err) => {
        expect(err).toBeInstanceOf(BadRequestError);
        expect(typeof err.message).toBe("string");
        expect(typeof err.code).toBe("string");
        expect(err.code).toEqual("ERR_400");
        expect(typeof err.detail).toBe("string");
        expect(err.detail).toEqual(ValidationErrorResponse.data.detail);
        expect(err.field_errors).toBeInstanceOf(Object);
        expect(err.field_errors).toEqual({
            id: ValidationErrorResponse.data.mail_id,
            email: ValidationErrorResponse.data.from_address,
        });
    });
});

test("API should throw ServerError when a response status is 500", () => {
    const axiosError = {
        // This simulates an error of axios.
        request: {},
        response: { status: 500, data: {} },
        message: "This is an error message generated by mock.",
    }; // TODO: Replace this by a custom class instance that implements an interface of AxiosError. (need Typescript.)
    // Instantiate API client.
    const api = new MyAPI("http://example.com/resource", token, {});

    // Mock definition
    const fnMock = jest.fn();
    fnMock.mockRejectedValue(axiosError);
    api.client.get = fnMock; // Patching axios.

    // Execute a test.
    return api.get().catch((err) => {
        expect(err).toBeInstanceOf(ServerError);
        expect(typeof err.message).toBe("string");
        expect(typeof err.code).toBe("string");
        expect(err.code).toEqual("ERR_500");
    });
});

test("API should throw ConnectionError when a request failed", () => {
    const error = {
        // This is an error mock definition of axios.
        request: {},
        message: "This is an error message generated by mock.",
    }; // TODO: Replace this by a custom class instance that implements an interface of AxiosError. (need Typescript.)

    const api = new MyAPI("http://example.com/resource", token, {});

    // Mock definition
    const fnMock = jest.fn();
    fnMock.mockRejectedValue(error);
    api.client.get = fnMock; // Patching axios.

    // Execute a test.
    return api.get().catch((err) => {
        expect(err).toBeInstanceOf(ConnectionError);
        expect(typeof err.message).toBe("string");
        expect(typeof err.code).toBe("string");
        expect(err.code).toEqual("ERR_600");
    });
});

test("API should throw ClientError when a request creation failed", () => {
    const error = {
        // This is an error mock definition of axios.
        message: "This is an error message generated by mock.",
    }; // TODO: Replace this by a custom class instance that implements an interface of AxiosError. (need Typescript.)

    const api = new MyAPI("http://example.com/resource", token, {});

    // Mock definition
    const fnMock = jest.fn();
    fnMock.mockRejectedValue(error);
    api.client.get = fnMock; // Patching axios.

    // Execute a test.
    return api.get().catch((err) => {
        expect(err).toBeInstanceOf(ClientError);
        expect(typeof err.message).toBe("string");
        expect(typeof err.code).toBe("string");
        expect(err.code).toEqual("ERR_700");
    });
});
