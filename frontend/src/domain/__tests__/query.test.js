import objectToQueryString, { deleteEmptyValue } from '../query';

// test starts from here
test('Test deleting an entry that has an empty string or undefined from an object', () => {
  const originalObj = {
    ValidString: 'ThisIsValid', booleanFalse: false, nullValue: null, NumberValue: 1, emptyString: '', undefValue: undefined,
  };
  const queryStrings = deleteEmptyValue(originalObj);
  const expectedResult = {
    ValidString: 'ThisIsValid', booleanFalse: false, nullValue: null, NumberValue: 1,
  }; // Ascending order of key name.

  expect(queryStrings).toEqual(expectedResult);
});

test('Test Making Query from an object', () => {
  const originalObj = { name: 'MyName', description: 'This is a test data' };
  const queryStrings = objectToQueryString(originalObj);
  const expectedResult = 'description=This%20is%20a%20test%20data&name=MyName'; // Ascending order of key name.

  expect(queryStrings).toEqual(expectedResult);
});

test('Test Making a Query that has characters which must be escaped.', () => {
  const originalObj = { name: 'M&M', description: 'Key=Value' };
  const queryStrings = objectToQueryString(originalObj);
  const expectedResult = 'description=Key%3DValue&name=M%26M';

  expect(queryStrings).toEqual(expectedResult);
});
