import {
    UpdateAuthorizedActionsPayloadModel,
    LoginErrorReduxModel,
    LoginDispatchModel,
    UpdateAvatarPayloadModel
} from "~/models/authModel";
import { AUTHORIZED_ACTION_LOADED, LOGGING_OUT, UPDATE_AVATAR_AFTER_CHANGE_MYPROFILE } from "../actions/actionTypes";
import { LOGIN, LOGOUT, LOGIN_ERROR } from "~/actions/actionTypes";
import { PayloadAction } from "~/models/reduxModel";
import { Override } from "~/utils/types";

type AuthPayloadAction<T> = Override<PayloadAction<T>, { error: boolean }>;

export const AuthActions = {
    updateAuthorizedActions: (payload: UpdateAuthorizedActionsPayloadModel) => {
        return {
            type: AUTHORIZED_ACTION_LOADED,
            payload,
        };
    },
    loginAction: (
        payload: LoginDispatchModel
    ): AuthPayloadAction<LoginDispatchModel> => {
        return {
            type: LOGIN,
            payload,
            error: false,
        };
    },
    logoutAction: (): AuthPayloadAction<any> => {
        return {
            type: LOGOUT,
            payload: undefined,
            error: false,
        };
    },
    loggingOutAction: (): AuthPayloadAction<any> => {
        return {
            type: LOGGING_OUT,
            payload: undefined,
            error: false,
        };
    },
    loginErrorAction: (
        payload: LoginErrorReduxModel
    ): AuthPayloadAction<any> => {
        return {
            type: LOGIN_ERROR,
            payload,
            error: true,
        };
    },
    updateAvatarAfterChangeMyProfile: (
        payload: UpdateAvatarPayloadModel
    ) => {
        return {
            type: UPDATE_AVATAR_AFTER_CHANGE_MYPROFILE,
            payload,
            error: true,
        };
    },
};
