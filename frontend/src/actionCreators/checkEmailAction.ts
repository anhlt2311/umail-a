import { PayloadAction } from "@reduxjs/toolkit";
import {
    CHECKED_EMAIL_ADDRESS,
    CHECKING_EMAIL_ADDRESS,
    CHECK_CLEAR_EMAIL_ADDRESS,
    CHECK_ERROR_EMAIL_ADDRESS,
} from "~/actions/actionTypes";
import { CheckEmailInitialStateModel } from "~/reducers/checkEmailReducer";

export const CheckEmailActions = {
    checkingAction: (pageId: string) => {
        return {
            type: pageId + CHECKING_EMAIL_ADDRESS,
        };
    },
    checkedAction: (
        pageId: string,
        payload: {
            message: string;
        }
    ): PayloadAction<Partial<CheckEmailInitialStateModel>> => {
        return {
            type: pageId + CHECKED_EMAIL_ADDRESS,
            payload,
        };
    },
    errorAction: (
        pageId: string,
        payload: { errorMessage: string }
    ): PayloadAction<Partial<CheckEmailInitialStateModel>> => {
        return {
            type: pageId + CHECK_ERROR_EMAIL_ADDRESS,
            payload,
        };
    },
    clearAction: (pageId: string) => {
        return {
            type: pageId + CHECK_CLEAR_EMAIL_ADDRESS,
        };
    },
};