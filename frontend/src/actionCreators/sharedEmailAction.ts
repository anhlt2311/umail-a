import { WRITING_EDIT_COMMENTS, WRITING_NEW_COMMENTS } from "~/actions/actionTypes";
import { PayloadAction } from "~/models/reduxModel";

export const SharedEmailAction = {
    clearErrorMessageNewComment: (payload: any): PayloadAction<any>=> {
        return {
            type: WRITING_NEW_COMMENTS,
            payload
        };
    },
    clearErrorMessageEditComment: (payload: any): PayloadAction<any>=> {
        return {
            type: WRITING_EDIT_COMMENTS,
            payload
        };
    }
};
