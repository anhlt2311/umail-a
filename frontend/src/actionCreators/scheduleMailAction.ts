import { 
  SCHEDULE_TEMPLATE_CREATING,
  SCHEDULE_TEMPLATE_CREATED,
  SCHEDULE_TEMPLATE_COMMITTED, } from "~/actions/actionTypes";
import { PayloadAction } from "~/models/reduxModel";

export const ScheduleMailAction = {
    creatingTemplateAction: (pageId: string): PayloadAction<any> => {
        return {
            type: pageId + SCHEDULE_TEMPLATE_CREATING,
            payload: undefined
        };
    },

    createdTemplateAction: (pageId: string, payload: any): PayloadAction<any> => {
      return {
          type: pageId + SCHEDULE_TEMPLATE_CREATED,
          payload
      };
    },

    committedTemplateAction: (pageId: string): PayloadAction<any> => {
      return {
          type: pageId + SCHEDULE_TEMPLATE_COMMITTED,
          payload: undefined
      };
    },
};
