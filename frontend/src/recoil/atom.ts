import { atom } from "recoil";
import { AjaxItemModel } from "~/models/displayTextAndForeignKeyModel";
import { PersonnelBoardTypeAtom } from "~/models/personnelBoardModel";
import { ProjectBoardTypeAtom } from "~/models/projectBoardModel";
import { QueryKeys } from "~/utils/constants";

export const isCsvDownloading = atom<boolean>({
    key: QueryKeys.csvDownload.isLoading,
    default: false,
});

export const isAppLoading = atom<boolean>({
    key: QueryKeys.others.isAppLoading,
    default: false,
});

export const isCsvDownSuccess = atom<boolean>({
  key: QueryKeys.csvDownload.isSuccess,
  default: false,
});

export const ajaxItemSelect = atom<AjaxItemModel[]>({
  key: QueryKeys.ajaxSelect.items,
  default: [],
});

export const personnelBoard = atom<PersonnelBoardTypeAtom>({
    key: QueryKeys.personnel.personnel,
    default: {
        isCreateCheckItem: false,
        checkListId: '',
        isUpdatePersonnel: false,
        cardId: '',
        dataCreatedPersonnel: {
            isCreated: false,
            isDropDownPlus: false,
        }
    },
});

export const projectBoard = atom<ProjectBoardTypeAtom>({
  key: QueryKeys.project.project,
  default: {
      isCreateCheckItem: false,
      checkListId: '',
      isUpdateProject: false,
      projectId: '',
      dataCreatedProject: {
          isCreated: false,
          isDropDownPlus: false,
      }
  },
});


