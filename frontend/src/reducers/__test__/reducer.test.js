import createCommentListReducer, * as commentList from '../Factories/commentList.js'
import createEditPageReducer, * as editPage from '../Factories/editPage.js'
import createReadOnlyPageReducer, * as readOnlyPage from '../Factories/readOnlyPage.js'
import createRealtimeRegisterPageReducer, * as realtimeRegisterPage from '../Factories/realtimeRegisterPage.js'
import createRegisterPageReducer, * as registerPage from '../Factories/registerPage.js'
import createSearchPageReducer, * as searchPage from '../Factories/searchPage.js'
import * as scheduledEmailPreview from '../containers.js'
import * as sharedEmailDetail from '../pages.js'
import login, * as loginReducer from '../login'

import { LOGOUT } from '../../actions/actionTypes';

test('should return initial state when logout', () => {
    const page_id = "dummy_page_id"
    const action = { type: LOGOUT }

    const create_comment_list_reducer = createCommentListReducer(page_id)
    const create_comment_list_reducer_result = create_comment_list_reducer({}, action)
    expect(create_comment_list_reducer_result).toEqual(commentList.defaultInitialState)

    const create_edit_page_reducer = createEditPageReducer(page_id)
    const create_edit_page_reducer_result = create_edit_page_reducer({}, action )
    expect(create_edit_page_reducer_result).toEqual(editPage.defaultInitialState)

    const create_read_only_page_reducer = createReadOnlyPageReducer(page_id)
    const create_read_only_page_reducer_result = create_read_only_page_reducer({}, action )
    expect(create_read_only_page_reducer_result).toEqual(readOnlyPage.defaultInitialState)

    const create_realtime_register_page_reducer = createRealtimeRegisterPageReducer(page_id)
    const create_realtime_register_page_reducer_result = create_realtime_register_page_reducer({}, action )
    expect(create_realtime_register_page_reducer_result).toEqual(realtimeRegisterPage.defaultInitialState)

    const create_register_page_reducer = createRegisterPageReducer(page_id)
    const create_register_page_reducer_result = create_register_page_reducer({}, action )
    expect(create_register_page_reducer_result).toEqual(registerPage.defaultInitialState)

    const create_search_page_reducer = createSearchPageReducer(page_id)
    const create_search_page_reducer_result = create_search_page_reducer({}, action )
    expect(create_search_page_reducer_result).toEqual(searchPage.defaultInitialState)

    const scheduled_email_preview_container_result = scheduledEmailPreview.scheduledEmailPreviewContainer({}, action )
    expect(scheduled_email_preview_container_result).toEqual(scheduledEmailPreview.scheduledEmailPreviewInitialState)

    const shared_email_detail_page_result = sharedEmailDetail.sharedEmailDetailPage({}, action )
    expect(shared_email_detail_page_result).toEqual(sharedEmailDetail.sharedEmailDetailInitialState)

    const login_result = login({}, action )
    expect(login_result).toEqual(loginReducer.LoginInitialState)
});
