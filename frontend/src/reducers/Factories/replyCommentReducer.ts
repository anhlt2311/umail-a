import {
    WRITING_REPLY_COMMENTS,
    PARENT_CANNOT_SUBMIT_WHEN_REPLY_COMMENT_EXISTS,
    COMMENT_NEW_CLEAR,
    CREATED,
    VALIDATION_ERROR,
} from "../../actions/actionTypes";
import { ErrorMessages } from "~/utils/constants";
import { PayloadAction } from "~/models/reduxModel";

const commentActionSuffix = "__COMMENTS";

export type ReplyCommentInitialStateModel = {
  commentValue: string;
  commentError: string;
  error: any;
  method: string;
};

export const ReplyCommentInitialState: ReplyCommentInitialStateModel = {
    commentValue: "",
    commentError: "",
    error: undefined,
    method: "",
};

export const createReplyCommentReducer = (
    pageId: string,
    initialState = ReplyCommentInitialState
) => {
    const replyCommentReducer = (
        state = initialState,
        action: PayloadAction<ReplyCommentInitialStateModel>
    ): ReplyCommentInitialStateModel => {
        switch (action.type) {
            case COMMENT_NEW_CLEAR:
                return {
                    ...state,
                    commentValue: "",
                    commentError: "",
                    error: undefined,
                    method: "",
                };
            case WRITING_REPLY_COMMENTS:
                return {
                    ...state,
                    commentValue: action.payload.commentValue,
                    commentError: "",
                    error: undefined,
                    method: "",
                };
            case PARENT_CANNOT_SUBMIT_WHEN_REPLY_COMMENT_EXISTS:
                return {
                    ...state,
                    commentError: ErrorMessages.commentError,
                    error: undefined,
                };
            case pageId + commentActionSuffix + CREATED:
                return {
                    ...state,
                    commentValue: "",
                    commentError: "",
                    error: undefined,
                    method: "",
                };
            case pageId + commentActionSuffix + VALIDATION_ERROR:
                if (action.payload.method !== 'create') {
                    return {
                        ...state,
                    }
                }
                const error = action.payload.error;
                let errorMessage = error.message
                if (error.field_errors && error.field_errors.detail) {
                    errorMessage = error.field_errors.detail
                } else if(error.field_errors && error.field_errors.content) {
                    errorMessage = error.field_errors.content
                }
                return {
                    ...state,
                    commentError: errorMessage,
                    error: undefined,
                    method: "",
                }
            default:
                return { ...state };
        }
    };
    return replyCommentReducer;
};

export default createReplyCommentReducer;
