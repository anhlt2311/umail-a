import moment from "moment";
import {
    LOADED,
    LOADING,
    RESET_COUNT,
    SYSTEM_NOTIFICATIONS,
} from "~/actions/actionTypes";
import { SystemNotificationModel } from "~/models/notificationModel";
import { PayloadAction } from "~/models/reduxModel";
import { SYSTEM_NOTIFICATION_LAST_FETCHED_TIMESTAMP } from "~/utils/constants";

export type SystemNotificationInitialStateModel = {
    isLoading: boolean;
    systemNotifications: SystemNotificationModel[];
    previousUrl: string;
    nextUrl: string;
    doesNewSystemNotificationExists: boolean;
    newNotificationCount: number;
    userId: string
};

export const SystemNotificationInitialState: SystemNotificationInitialStateModel =
    {
        isLoading: false,
        systemNotifications: [],
        previousUrl: "",
        nextUrl: "",
        doesNewSystemNotificationExists: false,
        newNotificationCount: 0,
        userId: ""
    };

export type SystemNotificationPayloadActionModel = {
    systemNotifications: SystemNotificationModel[];
    previousUrl: string;
    nextUrl: string;
    userId: string
};

export const systemNotificationReducer = (
    state = SystemNotificationInitialState,
    action: PayloadAction<SystemNotificationPayloadActionModel>
) => {
    switch (action.type) {
        case SYSTEM_NOTIFICATIONS + LOADING:
            return {
                ...state,
                isLoading: true,
                systemNotifications: [...state.systemNotifications],
            };
        case SYSTEM_NOTIFICATIONS + LOADED:
            const lastFetchedTimestamp = moment(
              localStorage.getItem(
                `${SYSTEM_NOTIFICATION_LAST_FETCHED_TIMESTAMP}_${action.payload.userId || state.userId}`
              )
            );
            const newSystemNotifications =
                action.payload.systemNotifications.filter((item) => {
                    return lastFetchedTimestamp < moment(item.release_time);
                });
            return {
                isLoading: false,
                ...action.payload,
                doesNewSystemNotificationExists:
                    !!newSystemNotifications.length,
                newNotificationCount: newSystemNotifications.length,
            };
        case SYSTEM_NOTIFICATIONS + RESET_COUNT:
            return {
                ...state,
                doesNewSystemNotificationExists: false,
                newNotificationCount: 0,
            };
        default:
            return { ...state };
    }
};
