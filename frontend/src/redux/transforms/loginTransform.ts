import { createTransform } from "redux-persist";
import { RootState } from "~/models/store";
import { AUTHORIZED_ACTIONS_DEFAULT_VALUE } from "~/utils/constants";
import { isEmpty } from "~/utils/utils";

export const LoginTransform: any = createTransform(
    (inboundState: RootState["login"], key) => {
        const temp = { ...inboundState };
        const authorizedActions = temp.authorizedActions;
        if (
            !!temp.token &&
            (!authorizedActions || isEmpty(authorizedActions))
        ) {
            temp.authorizedActions = AUTHORIZED_ACTIONS_DEFAULT_VALUE;
        }
        return {
            ...temp,
        };
    },
    (outboundState: RootState["login"], key) => {
        // NOTE(joshua-hashimoto): redux-persistが値を復帰するとき、tokenは存在するが権限情報が不正な値であればauthorizedActionsにデフォルト値を入れる。
        const temp = { ...outboundState };
        const authorizedActions = temp.authorizedActions;
        if (
            !!temp.token &&
            (!authorizedActions || isEmpty(authorizedActions))
        ) {
            temp.authorizedActions = AUTHORIZED_ACTIONS_DEFAULT_VALUE;
        }
        return {
            ...temp,
        };
    },
    {
        whitelist: ["login"],
    }
);
