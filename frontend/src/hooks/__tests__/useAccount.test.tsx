import React from "react";
import { configureStore, EnhancedStore } from "@reduxjs/toolkit";
import {
    act,
    mockHistoryPush,
    QueryClientWrapper,
    renderHook,
    screen,
} from "~/test/utils";
import {
    useAccountDeleteAPIMutation,
    usePasswordResetAPIMutation,
} from "../useAccount";
import login, { LoginInitialState } from "~/reducers/login";
import { ReactNode } from "react";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import { ErrorMessages, SuccessMessages } from "~/utils/constants";
import { mockServer } from "~/test/setupTests";
import {
    mockDeleteAccountFailsWithBadRequest,
    mockPasswordResetFailureAPIRoute,
} from "~/test/mock/accountAPIMock";
import { PasswordResetRequestFormModel } from "~/models/authModel";
import Paths from "~/components/Routes/Paths";

const Wrapper = ({
    store,
    children,
}: {
    store: EnhancedStore;
    children: ReactNode;
}) => {
    return (
        <Provider store={store}>
            <QueryClientWrapper>
                <MemoryRouter>{children}</MemoryRouter>
            </QueryClientWrapper>
        </Provider>
    );
};

describe("useAccount", () => {
    describe("useAccountDeleteAPIMutation()", () => {
        test("success", async () => {
            const localStore = configureStore({
                reducer: { login },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                    },
                },
            });
            const { result, waitFor } = renderHook(
                () => useAccountDeleteAPIMutation(),
                {
                    wrapper: ({ children }) => (
                        <Wrapper store={localStore}>{children}</Wrapper>
                    ),
                }
            );
            act(() => {
                result.current.mutate();
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const successMessageRegex = new RegExp(
                SuccessMessages.account.disable
            );
            const successMessageElement = await screen.findByText(
                successMessageRegex
            );
            expect(successMessageElement).toBeInTheDocument();
            expect(mockHistoryPush).toHaveBeenCalledWith(Paths.login);
        });

        test("error", async () => {
            mockServer.use(mockDeleteAccountFailsWithBadRequest);
            const localStore = configureStore({
                reducer: { login },
                preloadedState: {
                    login: {
                        ...LoginInitialState,
                    },
                },
            });
            const { result, waitFor } = renderHook(
                () => useAccountDeleteAPIMutation(),
                {
                    wrapper: ({ children }) => (
                        <Wrapper store={localStore}>{children}</Wrapper>
                    ),
                }
            );
            act(() => {
                result.current.mutate();
            });
            await waitFor(() => result.current.isError);
            expect(result.current.status).toBe("error");
            const errorMessageRegex = new RegExp(ErrorMessages.account.disable);
            const errorMessageElement = await screen.findByText(
                errorMessageRegex
            );
            expect(errorMessageElement).toBeInTheDocument();
        });
    });

    describe("usePasswordResetAPIMutation()", () => {
        test("success", async () => {
            const { result, waitFor } = renderHook(
                () => usePasswordResetAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                const postData: PasswordResetRequestFormModel = {
                    email: "example@example.com",
                };
                result.current.mutate(postData);
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const successMessageRegex = new RegExp(
                SuccessMessages.account.passwordReset
            );
            const successMessageElement = await screen.findByText(
                successMessageRegex
            );
            expect(successMessageElement).toBeInTheDocument();
        });

        test("error", async () => {
            mockServer.use(mockPasswordResetFailureAPIRoute);
            const { result, waitFor } = renderHook(
                () => usePasswordResetAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                const postData: PasswordResetRequestFormModel = {
                    email: "example@example.com",
                };
                result.current.mutate(postData);
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const errorMessageRegex = new RegExp(
                ErrorMessages.account.accountNotFound
            );
            const errorMessageElement = await screen.findByText(
                errorMessageRegex
            );
            expect(errorMessageElement).toBeInTheDocument();
        });
    });
});
