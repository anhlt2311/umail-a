import {
    convertTagResponseModelToTagDetailModel,
    convertTagResponseModelToTagModel,
    useFetchTagsAPIQuery,
} from "~/hooks/useTag";
import { QueryClientWrapper, renderHook } from "~/test/utils";
import { tagResponseModelList } from "~/test/mock/tagAPIMock";
import getDateStr from "~/domain/date";

describe("useTag.ts", () => {
    describe("convertTagResponseModelToTagModel()", () => {
        test("test converted data", () => {
            const data = tagResponseModelList[0];
            const convertedData = convertTagResponseModelToTagModel(data);
            expect(convertedData.id).toBe(data.id);
            expect(convertedData.value).toBe(data.value);
            expect(convertedData.color).toBe(data.color);
            expect(convertedData.internalValue).toBe(data.internal_value);
            expect(convertedData.createdTime).toBe(data.created_time);
            expect(convertedData.createdUser).toBe(data.created_user__name);
            expect(convertedData.modifiedTime).toBe(data.modified_time);
            expect(convertedData.modifiedUser).toBe(data.modified_user__name);
        });
    });
    describe("convertTagResponseModelToTagDetailModel()", () => {
        test("test converted data", () => {
            const data = tagResponseModelList[1];
            const convertedData = convertTagResponseModelToTagDetailModel(data);
            expect(convertedData.id).toBe(data.id);
            expect(convertedData.value).toBe(data.value);
            expect(convertedData.color).toBe(data.color);
            expect(convertedData.internal_value).toBe(data.internal_value);
            expect(convertedData.created_time).toBe(
                getDateStr(data.created_time)
            );
            expect(convertedData.created_user).toBe(data.created_user__name);
            expect(convertedData.modified_time).toBe(
                getDateStr(data.modified_time)
            );
            expect(convertedData.modified_user).toBe(data.modified_user__name);
        });
    });
    describe("useFetchTagsAPIQuery()", () => {
        test("fetch success test", async () => {
            const { result, waitFor } = renderHook(
                () => useFetchTagsAPIQuery({ options: { enabled: true } }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toEqual("success");
            expect(result.current.data?.count).toEqual(2);
        });

        test("fetch success with queryParams test", async () => {
            const { result, waitFor } = renderHook(
                () =>
                    useFetchTagsAPIQuery({
                        deps: { value: "dj" },
                        options: { enabled: true },
                    }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toEqual("success");
            expect(result.current.data?.count).toEqual(1);
        });
    });
});
