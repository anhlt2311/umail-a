import {
    addonFetchAddonMasterResponseData,
    addonFetchPurchaseItemResponseData,
    mockAddonFetchAddonMasterFailureAPIRoute,
    mockAddonFetchAddonPurchasedAddonFailureAPIRoute,
    mockAddonPurchaseAddonFailureAPIRoute,
    mockAddonRevokeAddonFailureAPIRoute,
} from "~/test/mock/addonAPIMock";
import {
    convertAddonMasterResponseModelToAddonMasterModel,
    convertAddonPurchaseItemResponseModelToAddonPurchaseItemModel,
    useFetchAddonMasterAPIQuery,
    useFetchPurchasedAddonsAPIQuery,
    usePurchaseAddonAPIMutation,
    useRevokeAddonAPIMutation,
} from "../useAddon";
import { act, QueryClientWrapper, renderHook, screen } from "~/test/utils";
import { mockServer } from "~/test/setupTests";
import { ErrorMessages, SuccessMessages } from "~/utils/constants";

describe("useAddon.ts", () => {
    test("convertAddonMasterResponseModelToAddonMasterModel() converts data expectedly", () => {
        const convertedData = addonFetchAddonMasterResponseData.map(
            convertAddonMasterResponseModelToAddonMasterModel
        );
        convertedData.forEach((val, index) => {
            const originalData = addonFetchAddonMasterResponseData[index];
            expect(val.id).toBe(originalData.id);
            expect(val.description).toBe(originalData.description);
            expect(val.expirationTime).toBe(originalData.expiration_time);
            expect(val.isDashboard).toBe(originalData.is_dashboard);
            expect(val.isMyCompanySetting).toBe(
                originalData.is_my_company_setting
            );
            expect(val.isOrganizations).toBe(originalData.is_organizations);
            expect(val.isRecommended).toBe(originalData.is_recommended);
            expect(val.isScheduledMails).toBe(originalData.is_scheduled_mails);
            expect(val.isSharedMails).toBe(originalData.is_shared_mails);
            expect(val.limit).toBe(originalData.limit);
            expect(val.parents).toMatchObject(originalData.parents);
            expect(val.price).toBe(originalData.price);
            expect(val.targets).toMatchObject(originalData.targets);
            expect(val.title).toBe(originalData.title);
            expect(val.helpUrl).toBe(originalData.help_url);
        });
    });

    test("convertAddonPurchaseItemResponseModelToAddonPurchaseItemModel() converts data expectedly", () => {
        const convertedData = addonFetchPurchaseItemResponseData.map(
            convertAddonPurchaseItemResponseModelToAddonPurchaseItemModel
        );
        convertedData.forEach((val, index) => {
            const originalData = addonFetchPurchaseItemResponseData[index];
            expect(val.id).toBe(originalData.id);
            expect(val.addonMasterId).toBe(originalData.addon_master_id);
            expect(val.nextPaymentDate).toBe(originalData.next_payment_date);
        });
    });

    describe("useFetchAddonMasterAPIQuery()", () => {
        test("success", async () => {
            const { result, waitFor } = renderHook(
                () =>
                    useFetchAddonMasterAPIQuery({ options: { enabled: true } }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const convertedData = addonFetchAddonMasterResponseData.map(
                convertAddonMasterResponseModelToAddonMasterModel
            );
            const data = result.current.data;
            expect(data).toMatchObject(convertedData);
        });

        test("error", async () => {
            mockServer.use(mockAddonFetchAddonMasterFailureAPIRoute);
            const { result, waitFor } = renderHook(
                () =>
                    useFetchAddonMasterAPIQuery({ options: { enabled: true } }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isError);
            expect(result.current.status).toBe("error");
            const errorMessageText = ErrorMessages.addon.addonMaster;
            const errorMessageRegex = new RegExp(errorMessageText);
            const errorMessageElement = await screen.findByText(
                errorMessageRegex
            );
            expect(errorMessageElement).toBeInTheDocument();
        });
    });

    describe("useFetchPurchasedAddonsAPIQuery()", () => {
        test("success", async () => {
            const { result, waitFor } = renderHook(
                () =>
                    useFetchPurchasedAddonsAPIQuery({
                        options: { enabled: true },
                    }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const convertedData = addonFetchPurchaseItemResponseData.map(
                convertAddonPurchaseItemResponseModelToAddonPurchaseItemModel
            );
            const data = result.current.data;
            expect(data).toMatchObject(convertedData);
        });

        test("error", async () => {
            mockServer.use(mockAddonFetchAddonPurchasedAddonFailureAPIRoute);
            const { result, waitFor } = renderHook(
                () =>
                    useFetchPurchasedAddonsAPIQuery({
                        options: { enabled: true },
                    }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isError);
            expect(result.current.status).toBe("error");
            const errorMessageText = ErrorMessages.addon.purchasedList;
            const errorMessageRegex = new RegExp(errorMessageText);
            const errorMessageElement = await screen.findByText(
                errorMessageRegex
            );
            expect(errorMessageElement).toBeInTheDocument();
        });
    });

    describe("usePurchaseAddonAPIMutation()", () => {
        test("success", async () => {
            const { result, waitFor } = renderHook(
                () => usePurchaseAddonAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                result.current.mutate({ addonMasterId: 1 });
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const messageText = SuccessMessages.addon.purchase;
            const messageRegex = new RegExp(messageText);
            const messageElement = await screen.findByText(messageRegex);
            expect(messageElement).toBeInTheDocument();
        });

        test("error", async () => {
            mockServer.use(mockAddonPurchaseAddonFailureAPIRoute);
            const { result, waitFor } = renderHook(
                () => usePurchaseAddonAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                result.current.mutate({ addonMasterId: 1 });
            });
            await waitFor(() => result.current.isError);
            expect(result.current.status).toBe("error");
            const messageText = ErrorMessages.addon.purchase;
            const messageRegex = new RegExp(messageText);
            const messageElement = await screen.findByText(messageRegex);
            expect(messageElement).toBeInTheDocument();
        });
    });

    describe("useRevokeAddonAPIMutation()", () => {
        test("success", async () => {
            const { result, waitFor } = renderHook(
                () => useRevokeAddonAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                result.current.mutate(1);
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.status).toBe("success");
            const messageText = SuccessMessages.addon.revoke;
            const messageRegex = new RegExp(messageText);
            const messageElement = await screen.findByText(messageRegex);
            expect(messageElement).toBeInTheDocument();
        });

        test("error", async () => {
            mockServer.use(mockAddonRevokeAddonFailureAPIRoute);
            const { result, waitFor } = renderHook(
                () => useRevokeAddonAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            act(() => {
                result.current.mutate(1);
            });
            await waitFor(() => result.current.isError);
            expect(result.current.status).toBe("error");
            const messageText = ErrorMessages.addon.revoke;
            const messageRegex = new RegExp(messageText);
            const messageElement = await screen.findByText(messageRegex);
            expect(messageElement).toBeInTheDocument();
        });
    });
});
