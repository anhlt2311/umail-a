import { convertContactListResponseDataEntry } from "~/domain/data";
import getDateStr, { getDateStrByDateFormat } from "~/domain/date";
import { contactListResponseContent } from "~/test/mock/contactAPIMock";
import { convertContactListResponseModelToContactListModel } from "../useContact";

describe("useContact.ts", () => {
    describe("convertContactListResponseModelToContactListModel()", () => {
        test("test converter", () => {
            const data = contactListResponseContent[0];
            const convertedData =
                convertContactListResponseModelToContactListModel(data);
            expect(convertedData.id).toBe(data.id);
            expect(convertedData.display_name).toBe(data.display_name);
            expect(convertedData.email).toBe(data.email);
            expect(convertedData.cc_mails).toMatchObject(data.cc_mails);
            expect(convertedData.cc_addresses__email).toBe(
                data.cc_mails.join(",")
            );
            expect(convertedData.position).toBe(data.position);
            expect(convertedData.department).toBe(data.department);
            expect(convertedData.organization__name).toBe(
                data.organization__name
            );
            expect(convertedData.staff__name).toBe(data.staff__name);
            expect(convertedData.staff__last_name).toBe(data.staff__name);
            expect(convertedData.last_visit).toBe(
                getDateStrByDateFormat(data.last_visit)
            );
            expect(convertedData.created_time).toBe(
                getDateStr(data.created_time)
            );
            expect(convertedData.modified_time).toBe(
                getDateStr(data.modified_time)
            );
            expect(convertedData.comments).toMatchObject(data.comments);
            expect(convertedData.tel1).toBe(data.tel1);
            expect(convertedData.tel2).toBe(data.tel2);
            expect(convertedData.tel3).toBe(data.tel3);
            expect(convertedData.contactjobtypepreferences).toMatchObject(
                data.contactjobtypepreferences
            );
            expect(convertedData.contactpersonneltypepreferences).toMatchObject(
                data.contactpersonneltypepreferences
            );
            expect(convertedData.contactpreference).toMatchObject(
                data.contactpreference
            );
            expect(convertedData.tags).toMatchObject(data.tags);
            expect(convertedData.tag_objects).toMatchObject(data.tag_objects);
            expect(convertedData.category).toBe(data.category);
            expect(convertedData.is_ignored).toBe(data.is_ignored);
        });

        test("test against old converter", () => {
            const data = contactListResponseContent[0];
            const oldConverterData = convertContactListResponseDataEntry(data);
            delete oldConverterData.key;
            delete oldConverterData.first_name;
            delete oldConverterData.last_name;
            delete oldConverterData.score;
            delete oldConverterData.tel;
            delete oldConverterData.contactjobskillpreferences;
            delete oldConverterData.contactpersonnelskillpreferences;
            const convertedData =
                convertContactListResponseModelToContactListModel(data);
            expect(convertedData).toMatchObject(oldConverterData);
        });
    });
});
