import getDateStr, { DateStrToMoment } from "~/domain/date";
import { mockMyCompanyAPIMockData } from "~/test/mock/myCompanyAPIMock";
import { convertMyCompanyResponseModelToMyCompanyModel } from "../useMyCompany";

describe("useMyCompany.ts", () => {
    describe("convertMyCompanyResponseModelToMyCompanyModel", () => {
        test("convert test", () => {
            const convertedData = convertMyCompanyResponseModelToMyCompanyModel(
                mockMyCompanyAPIMockData
            );
            expect(convertedData.id).toBe(mockMyCompanyAPIMockData.id);
            expect(convertedData.name).toBe(mockMyCompanyAPIMockData.name);
            expect(convertedData.domain_name).toBe(
                mockMyCompanyAPIMockData.domain_name
            );
            expect(convertedData.address).toBe(
                mockMyCompanyAPIMockData.address
            );
            expect(convertedData.building).toBe(
                mockMyCompanyAPIMockData.building
            );
            expect(convertedData.capital_man_yen).toBe(
                mockMyCompanyAPIMockData.capital_man_yen
            );
            expect(convertedData.establishment_date?.format("YYYY-MM-DD")).toBe(
                mockMyCompanyAPIMockData.establishment_date
            );
            expect(convertedData.establishment_year).toBe(
                mockMyCompanyAPIMockData.establishment_year
            );
            expect(convertedData.has_p_mark_or_isms).toBe(
                mockMyCompanyAPIMockData.has_p_mark_or_isms
            );
            expect(convertedData.has_invoice_system).toBe(
                mockMyCompanyAPIMockData.has_invoice_system
            );
            expect(convertedData.has_haken).toBe(
                mockMyCompanyAPIMockData.has_haken
            );
            expect(convertedData.has_distribution).toBe(
                mockMyCompanyAPIMockData.has_distribution
            );
            expect(
                convertedData.capital_man_yen_required_for_transactions
            ).toBe(
                mockMyCompanyAPIMockData.capital_man_yen_required_for_transactions
            );
            expect(convertedData.p_mark_or_isms).toBe(
                mockMyCompanyAPIMockData.p_mark_or_isms
            );
            expect(convertedData.invoice_system).toBe(
                mockMyCompanyAPIMockData.invoice_system
            );
            expect(convertedData.haken).toBe(mockMyCompanyAPIMockData.haken);
            convertedData.exceptional_organization_names.forEach(
                (value, index) => {
                    expect(value).toBe(
                        mockMyCompanyAPIMockData.exceptional_organization_names[
                            index
                        ]
                    );
                }
            );
            convertedData.exceptional_organizations.forEach((value, index) => {
                expect(value).toBe(
                    mockMyCompanyAPIMockData.exceptional_organizations[index]
                );
            });
            expect(convertedData.modified_time).toBe(
                getDateStr(mockMyCompanyAPIMockData.modified_time)
            );
            expect(convertedData.modified_user).toBe(
                mockMyCompanyAPIMockData.modified_user__name
            );
        });
    });
});
