import { convertMyProfileDataEntry } from "~/domain/data";
import getDateStr from "~/domain/date";
import { MyProfileFormModel } from "~/models/myProfileModel";
import { myProfileResponseData } from "~/test/mock/myProfileAPIMock";
import {
    convertMyProfileFormModelToMyProfileRequestModel,
    convertMyProfileResponseModelToMyProfileModel,
} from "../useMyProfile";

describe("useMyProfile.ts", () => {
    describe("convertMyProfileResponseModelToMyProfileModel()", () => {
        test("converter can convert data expectedly", () => {
            const convertedData = convertMyProfileResponseModelToMyProfileModel(
                myProfileResponseData
            );
            expect(convertedData.avatar).toBe(myProfileResponseData.avatar);
            expect(convertedData.email).toBe(myProfileResponseData.email);
            expect(convertedData.emailSignature).toBe(
                myProfileResponseData.email_signature
            );
            expect(convertedData.firstName).toBe(
                myProfileResponseData.first_name
            );
            expect(convertedData.lastName).toBe(
                myProfileResponseData.last_name
            );
            expect(convertedData.userServiceId).toBe(
                myProfileResponseData.user_service_id
            );
            expect(convertedData.modifiedTime).toBe(
                getDateStr(myProfileResponseData.modified_time)
            );
            expect(convertedData.modifiedUser).toBe(
                myProfileResponseData.modified_user__name
            );
            expect(convertedData.password).toBe(myProfileResponseData.password);
            expect(convertedData.role).toBe(myProfileResponseData.role);
            expect(convertedData.tel1).toBe(myProfileResponseData.tel1);
            expect(convertedData.tel2).toBe(myProfileResponseData.tel2);
            expect(convertedData.tel3).toBe(myProfileResponseData.tel3);
        });
    });

    describe("convertMyProfileFormModelToMyProfileRequestModel()", () => {
        test("converter can convert data expectedly", () => {
            const myProfileFormData: MyProfileFormModel = {
                ...convertMyProfileResponseModelToMyProfileModel(
                    myProfileResponseData
                ),
                password: "Abcd1234$",
            };
            const convertedData =
                convertMyProfileFormModelToMyProfileRequestModel(
                    myProfileFormData
                );
            expect(convertedData.avatar).toBe(undefined);
            expect(convertedData.email).toBe(myProfileFormData.email);
            expect(convertedData.email_signature).toBe(
                myProfileFormData.emailSignature
            );
            expect(convertedData.first_name).toBe(myProfileFormData.firstName);
            expect(convertedData.last_name).toBe(myProfileFormData.lastName);
            expect(convertedData.user_service_id).toBe(
                myProfileFormData.userServiceId
            );
            expect(convertedData.role).toBe(myProfileFormData.role);
            expect(convertedData.tel1).toBe(myProfileFormData.tel1);
            expect(convertedData.tel2).toBe(myProfileFormData.tel2);
            expect(convertedData.tel3).toBe(myProfileFormData.tel3);
            expect(convertedData.password).toBe(myProfileFormData.password);
        });

        test("test converter when file is set", () => {
            const file = new File(["fasdfasdaf"], "random-image.png", {
                type: "image/png",
            });
            const myProfileFormData: MyProfileFormModel = {
                ...convertMyProfileResponseModelToMyProfileModel(
                    myProfileResponseData
                ),
                password: "Abcd1234$",
            };
            const convertedData =
                convertMyProfileFormModelToMyProfileRequestModel({
                    ...myProfileFormData,
                    avatar: file,
                });
            expect(convertedData.avatar).toBe(file);
            expect(convertedData.email).toBe(myProfileFormData.email);
            expect(convertedData.email_signature).toBe(
                myProfileFormData.emailSignature
            );
            expect(convertedData.first_name).toBe(myProfileFormData.firstName);
            expect(convertedData.last_name).toBe(myProfileFormData.lastName);
            expect(convertedData.role).toBe(myProfileFormData.role);
            expect(convertedData.tel1).toBe(myProfileFormData.tel1);
            expect(convertedData.tel2).toBe(myProfileFormData.tel2);
            expect(convertedData.tel3).toBe(myProfileFormData.tel3);
            expect(convertedData.password).toBe(myProfileFormData.password);
        });
    });
});
