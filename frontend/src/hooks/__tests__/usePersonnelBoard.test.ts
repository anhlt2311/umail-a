import { v4 as uuidv4 } from "uuid";
import { act } from "@testing-library/react";
import { renderHook } from "@testing-library/react-hooks";
import {
    cardCreateResponse,
    cardDetailResponse,
    cardsContent,
    listsResponse,
} from "~/test/mock/personnelBoardAPIMock";
import { QueryClientWrapper } from "~/test/utils";
import { randString } from "~/utils/utils";
import {
    usePersonnelBoardCreateCardAPIMutation,
    usePersonnelBoardFetchCardAPIQuery,
    usePersonnelBoardFetchListCardsAPIQuery,
    usePersonnelBoardFetchListsAPIQuery,
    usePersonnelBoardUpdateCardAPIMutation,
} from "../usePersonnelBoard";
import {
    PersonnelBoardFormModel,
    PersonnelBoardNewCardFormModel,
} from "~/models/personnelBoardModel";

describe("usePersonnelBoard.ts", () => {
    describe("usePersonnelBoardFetchListsAPIQuery()", () => {
        test("success pattern", async () => {
            const { result, waitFor } = renderHook(
                () =>
                    usePersonnelBoardFetchListsAPIQuery({
                        options: { enabled: true },
                    }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.data?.length).toEqual(2);
            result.current.data?.forEach((value, index) => {
                expect(value).toMatchObject(listsResponse[index]);
            });
        });
    });

    describe("usePersonnelBoardFetchListCardsAPIQuery()", () => {
        test("success pattern", async () => {
            const listId = randString();
            const { result, waitFor } = renderHook(
                () =>
                    usePersonnelBoardFetchListCardsAPIQuery({
                        deps: { listId },
                        options: { enabled: true },
                    }),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.data?.count).toEqual(2);
            result.current.data?.results.forEach((value, index) => {
                expect(value).toMatchObject(cardsContent[index]);
            });
        });
    });

    describe("usePersonnelBoardCreateCardAPIMutation()", () => {
        test("success pattern", async () => {
            const { result, waitFor } = renderHook(
                () => usePersonnelBoardCreateCardAPIMutation(),
                {
                    wrapper: QueryClientWrapper,
                }
            );
            const postData: PersonnelBoardNewCardFormModel = {
                list_id: randString(),
                first_name_initial: "J",
                last_name_initial: "D",
            };
            act(() => {
                result.current.mutate(postData);
            });
            await waitFor(() => result.current.isSuccess);
            expect(result.current.data?.data.id).toBe(cardCreateResponse.id);
        });
    });

    describe("usePersonnelBoardFetchCardAPIQuery()", () => {
        test("success pattern", async () => {
            const cardId = randString();
            const { result, waitFor } = renderHook(
                () =>
                    usePersonnelBoardFetchCardAPIQuery({
                        deps: { cardId },
                        options: {
                            enabled: true,
                        },
                    }),
                { wrapper: QueryClientWrapper }
            );
            await waitFor(() => result.current.isSuccess);
            expect(result.current.data).toMatchObject(cardDetailResponse);
        });
    });
});
