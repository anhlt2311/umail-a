import { convertDisplaySettingDataEntry } from "~/domain/data";
import getDateStr from "~/domain/date";
import { mockDisplaySettingAPIData } from "~/test/mock/displaySettingAPIMock";
import { convertDisplaySettingAPIModelToDisplaySetting } from "../useDisplaySetting";

describe("useDisplaySetting.ts", () => {
    describe("convertDisplaySettingAPIModelToDisplaySetting()", () => {
        test("test converter", () => {
            const convertedData = convertDisplaySettingAPIModelToDisplaySetting(
                mockDisplaySettingAPIData
            );
            expect(convertedData.company).toBe(
                mockDisplaySettingAPIData.company
            );
            expect(convertedData.content_hash).toMatchObject(
                mockDisplaySettingAPIData.content_hash
            );
            expect(convertedData.modified_time).toBe(
                getDateStr(mockDisplaySettingAPIData.modified_time)
            );
            expect(convertedData.modified_user).toBe(
                mockDisplaySettingAPIData.modified_user__name
            );
        });
        test("test against old converter", () => {
            const convertedData = convertDisplaySettingAPIModelToDisplaySetting(
                mockDisplaySettingAPIData
            );
            const oldConverterData = convertDisplaySettingDataEntry(
                mockDisplaySettingAPIData
            );
            expect(convertedData).toMatchObject(oldConverterData);
        });
    });
});
