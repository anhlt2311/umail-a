import React from "react";
import { render } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { createQueryString } from "~/utils/utils";
import { renderHook } from "@testing-library/react-hooks";
import { useQueryString } from "../useQueryString";

describe("useQueryString.ts", () => {
    test("can get query params", () => {
        const expectedParamsObj = {
            value: "text",
            pageSize: 100,
        };
        const createdQueryString = createQueryString(expectedParamsObj);
        const { result, waitFor } = renderHook(() => useQueryString(), {
            wrapper: ({ children }) => (
                <MemoryRouter
                    initialEntries={[
                        { pathname: "/", search: "?" + createdQueryString },
                    ]}>
                    {children}
                </MemoryRouter>
            ),
        });
        const { queryParams, rawQueryString } = result.current;
        const value = queryParams.value;
        expect(value).toBe("text");
        const pageSize = queryParams.page_size;
        expect(pageSize).toBe(100);
        const queryParamsString = rawQueryString.substring(1);
        expect(queryParamsString).toBe("page_size=100&value=text");
    });
});
