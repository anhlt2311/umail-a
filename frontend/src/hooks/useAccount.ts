import { accountAPI } from "~/networking/api";
import { ErrorMessages, SuccessMessages } from "~/utils/constants";
import { useLogoutAPIMutation } from "./useAuth";
import { useCustomMutation } from "./useCustomMutation";
import {
    customSuccessMessage,
    customErrorMessage,
} from "~/components/Common/AlertMessage/AlertMessage";

export const useAccountDeleteAPIMutation = () => {
    const { mutate: logout } = useLogoutAPIMutation();

    return useCustomMutation(accountAPI.deleteAccount, {
        onSuccess: () => {
            customSuccessMessage(SuccessMessages.account.disable);
            logout();
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.account.disable;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const usePasswordResetAPIMutation = () => {
    return useCustomMutation(accountAPI.passwordReset, {
        onSuccess: (response) => {
            // NOTE(joshua-hashimoto): エラーでもstatusは200で返ってくるので、onSuccessでエラーを処理
            const result = response.data.result;
            if (result === "AccountDoesNotExist") {
                customErrorMessage(ErrorMessages.account.accountNotFound);
            } else {
                customSuccessMessage(SuccessMessages.account.passwordReset);
            }
        },
    });
};
