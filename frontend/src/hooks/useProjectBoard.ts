import { useEffect, useState } from "react";
import { useQueries, UseQueryResult } from "react-query";
import {
    BoardChecklistItemCreateFormModel,
    BoardChecklistItemModel,
    BoardChecklistItemUpdateFormModel,
    BoardChecklistModel,
    BoardChecklistUpdateFormModel,
    BoardColumnModel,
    BoardCommentModel,
    BoardCommentUpdateFormModel,
    BoardDataModel,
    CardChangePositionRequestModel,
} from "~/models/boardCommonModel";
import {
    ProjectBoardContractModel,
    ProjectBoardContractUpdateFormModel,
    ProjectBoardDetailModel,
    ProjectBoardFormModel,
    ProjectBoardListCardModel,
} from "~/models/projectBoardModel";
import { customErrorMessage, customSuccessMessage } from "~/components/Common/AlertMessage/AlertMessage";
import { CustomUseQueryOptions } from "~/models/queryModel";
import { PaginationResponseModel } from "~/models/responseModel";
import { projectBoardAPI } from "~/networking/api";
import { formatDateReq, formatDateTime } from "~/utils/utils";
import { ErrorMessages, QueryKeys, SuccessMessages } from "~/utils/constants";
import { useCustomMutation } from "./useCustomMutation";
import { useCustomQuery } from "./useCustomQuery";
import { projectBoard } from "~/recoil/atom";
import { useRecoilState } from "recoil";
import { OrganizationListModel }  from "~/models/organizationModel";
import { ContactListModel }  from "~/models/contactModel";

const defaultDataCardDetail: ProjectBoardDetailModel = {
    id: '',
    order: 0,
    listId: '',
    assignees: [],	
    priority: {
        title: '中',
        value:'urgent'
    },
    period: {
        end: '',
        start: '',
        isFinished:false
    },
    detail: '',
    projectPeriod: {
        datetime: '',
        immediate: false,
        longTerm: false,
        remarks: ''
    },
    place: {
    id: '',
    remote: false,
    remarks: ''
    },
    workingHours: {
        start: '',
        end: '',
        remarks: '',
    },
    people: {
        amount: 0,
        isMultiple: false,
        isSetProposalWelcome: false,
        remarks: '',
    },
    description: '',	
    skills: {
        required: [],
        welcomed: [],
    },
    price: {
        amount: 0,
        isSkillAssessment: false,
        remarks: '',
    },
    settleWidth: {
        start: 0,
        end: 0,
        isFixed: false,
        remarks: '',
    },
    settleMethod: {
        method: '',
        remarks: ''
    },
    settleIncrement: {
        increment: 0,
        remarks: ''
    },
    payment: {
        day: 0,
        remarks: ''
    },
    interview: {
        amount: 0,
        isJoin: false,
        remarks: ''
    },
    distribution: {
        to: '',
        remarks: ''
    },
    dynamicRow: [],	
    projectAttachment: '',
    contracts: [],
    scheduledEmailTemplate: {
        subject: '',
        text: {
            upper: '',
            lower: ''
        },
        sendCopyToSender: false,
        sendCopyToShare: false,
        condition: undefined,
    },
    scheduledEmailHistory: [],
    checklist: [],
    comments: [],
    isArchived: false,
}

export const useProjectBoardFetchListsAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<BoardColumnModel[]>): UseQueryResult<
    BoardColumnModel[]
> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.lists,
        deps,
        options: {
            ...options,
        },
        apiRequest: projectBoardAPI.fetchLists,
    });
};

export const useProjectBoardCreator = () => {
    const { data: lists } = useProjectBoardFetchListsAPIQuery(
        {}
    );
        
    const cardsQueries = useQueries(
        (lists ?? []).map((list) => {
            return {
                queryKey: [
                    QueryKeys.projectBoard.cards,
                    list.id,
                    "queries",
                ],
                queryFn: () => projectBoardAPI.fetchListCards(list.id),
                enabled: !!lists,
            };
        })
    );

    const columns:
        | BoardDataModel<ProjectBoardListCardModel>[]
        | undefined = lists
        ?.map((list, index) => {
            const cards = cardsQueries[index].data?.data.results ?? [];
            return {
                ...list,
                cards,
            };
        })
            .filter((data) => typeof data !== "undefined");
    
    return {
        boardData: {
            columns: columns ?? [],
        },
    };
};

export const useProjectBoardFetchListCardsAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    PaginationResponseModel<ProjectBoardListCardModel[]>,
    PaginationResponseModel<ProjectBoardListCardModel[]>,
    { listId: string }
>): UseQueryResult<PaginationResponseModel<ProjectBoardListCardModel[]>> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.cards,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) => projectBoardAPI.fetchListCards(query?.listId!),
    });
};

export const useProjectBoardCreateCardAPIMutation = () => {
    return useCustomMutation(projectBoardAPI.createCard, {
        onSuccess: (response) => {},
        onError: (err) => {
          customErrorMessage("姓名合わせて3文字以内で入力してください。");
        },
    });
};

export const useProjectBoard = () => {
  const [idCard, setIdCard] = useState<string>('');
  const { mutate: projectBoardCreateCardMutate, isSuccess, isError, data } = useProjectBoardCreateCardAPIMutation();
  const { mutate: projectBoardChangeCardPositionMutate } = useProjectBoardChangeCardPositionAPIMutation();

  const projectBoardChangeCardPosition = (
    cardId: string,
    listId: string,
    position: number
  ) => {
    projectBoardChangeCardPositionMutate(
      {
        cardId,
        postData: {
          listId,
          position,
        },
      },
      {
        onSuccess: (res) => {},
        onError: (err) => {},
      }
    );
  };

  useEffect(() => {
      if (data?.data?.id) {
          setIdCard(data?.data?.id)
      }
  }, [data])

  useEffect(() => {
      if (isSuccess) {
          customSuccessMessage(SuccessMessages.generic.create);
      }
  }, [isSuccess])
  
  useEffect(() => {
      if (isError) {
          customErrorMessage(ErrorMessages.generic.create);
      }
  },[isError])

  return {
      idCard,
      projectBoardCreateCardMutate,
      projectBoardChangeCardPosition,
      isSuccess,
  }
}

export const useProjectBoardFetchCardAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    ProjectBoardDetailModel,
    ProjectBoardDetailModel,
    { projectId: string }
>): UseQueryResult<ProjectBoardDetailModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.card,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) => projectBoardAPI.fetchCard(query?.projectId!),
    });
};

export const useProjectBoardUpdateCardAPIMutation = () => {
    type RequestModel = {
        cardId: string;
        postData: ProjectBoardFormModel;
    };

    const apiRequest = ({ cardId, postData }: RequestModel) => {
        return projectBoardAPI.updateCard(cardId, postData);
    };

    return useCustomMutation(apiRequest, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardDeleteCardAPIMutation = () => {
    return useCustomMutation(projectBoardAPI.deleteCard, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardChangeCardPositionAPIMutation = () => {
    type RequestModel = {
        cardId: string;
        postData: CardChangePositionRequestModel;
    };

    const apiRequest = ({ cardId, postData }: RequestModel) => {
        return projectBoardAPI.changeCardPosition(cardId, postData);
    };

    return useCustomMutation(apiRequest, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardFetchChecklistsAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    BoardChecklistModel[],
    BoardChecklistModel[],
    { projectId: string }
>): UseQueryResult<BoardChecklistModel[]> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.checklists,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) => projectBoardAPI.fetchChecklists(query?.projectId!),
    });
};

export const useProjectBoardCreateChecklistAPIMutation = () => {
    return useCustomMutation(projectBoardAPI.createChecklist, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardFetchChecklistAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    BoardChecklistModel,
    BoardChecklistModel,
    { checklistId: string }
>): UseQueryResult<BoardChecklistModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.checklist,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) =>
            projectBoardAPI.fetchChecklist(query?.checklistId!),
    });
};

export const useProjectBoardUpdateChecklistAPIMutation = () => {
    type RequestModel = {
        checklistId: string;
        postData: BoardChecklistUpdateFormModel;
    };

    const apiRequest = ({ checklistId, postData }: RequestModel) => {
        return projectBoardAPI.updateChecklist(checklistId, postData);
    };

    return useCustomMutation(apiRequest, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardDeleteChecklistAPIMutation = () => {
    return useCustomMutation(projectBoardAPI.deleteChecklist, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardFetchChecklistItemsAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    BoardChecklistItemModel[],
    BoardChecklistItemModel[],
    { checklistId: string }
>): UseQueryResult<BoardChecklistItemModel[]> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.checklistItems,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) =>
            projectBoardAPI.fetchChecklistItems(query?.checklistId!),
    });
};

export const useProjectBoardCreateChecklistItemAPIMutation = () => {
    type RequestModel = {
        checklistId: string;
        postData: BoardChecklistItemCreateFormModel;
    };

    const apiRequest = ({ checklistId, postData }: RequestModel) => {
        return projectBoardAPI.createChecklistItem(checklistId, postData);
    };

    return useCustomMutation(apiRequest, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardFetchChecklistItemAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    BoardChecklistItemModel,
    BoardChecklistItemModel,
    { checklistId: string; itemId: string }
>): UseQueryResult<BoardChecklistItemModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.checklistItem,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) =>
            projectBoardAPI.fetchChecklistItem(
                query?.checklistId!,
                query?.itemId!
            ),
    });
};

export const useProjectBoardUpdateChecklistItemAPIMutation = () => {
    type RequestModel = {
        checklistId: string;
        itemId: string;
        postData: BoardChecklistItemUpdateFormModel;
    };

    const apiRequest = ({ checklistId, itemId, postData }: RequestModel) => {
        return projectBoardAPI.updateChecklistItem(
            checklistId,
            itemId,
            postData
        );
    };

    return useCustomMutation(apiRequest, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardDeleteChecklistItemAPIMutation = () => {
    type RequestModel = {
        checklistId: string;
        itemId: string;
    };

    const apiRequest = ({ checklistId, itemId }: RequestModel) => {
        return projectBoardAPI.deleteChecklistItem(checklistId, itemId);
    };

    return useCustomMutation(apiRequest, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardFetchCommentsAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    BoardCommentModel[],
    BoardCommentModel[],
    { projectId: string }
>): UseQueryResult<BoardCommentModel[]> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.comments,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) => projectBoardAPI.fetchComments(query?.projectId!),
    });
};

export const useProjectBoardCreateCommentAPIMutation = () => {
    return useCustomMutation(projectBoardAPI.createComment, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardFetchCommentAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    BoardCommentModel,
    BoardCommentModel,
    { commentId: string }
>): UseQueryResult<BoardCommentModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.comment,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) => projectBoardAPI.fetchComment(query?.commentId!),
    });
};

export const useProjectBoardUpdateCommentAPIMutation = () => {
    type RequestModel = {
        commentId: string;
        postData: BoardCommentUpdateFormModel;
    };

    const apiRequest = ({ commentId, postData }: RequestModel) => {
        return projectBoardAPI.updateComment(commentId, postData);
    };

    return useCustomMutation(apiRequest, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardDeleteCommentAPIMutation = () => {
    return useCustomMutation(projectBoardAPI.deleteComment, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardFetchSubCommentsAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    BoardCommentModel[],
    BoardCommentModel[],
    { commentId: string }
>): UseQueryResult<BoardCommentModel[]> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.subComments,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) =>
            projectBoardAPI.fetchSubComments(query?.commentId!),
    });
};

export const useProjectBoardFetchContractsAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    ProjectBoardContractModel[],
    ProjectBoardContractModel[],
    { cardId: string }
>): UseQueryResult<ProjectBoardContractModel[]> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.contracts,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) => projectBoardAPI.fetchContracts(query?.cardId!),
    });
};

export const useProjectBoardCreateContractAPIMutation = () => {
    return useCustomMutation(projectBoardAPI.createContract, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardFetchContractAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    ProjectBoardContractModel,
    ProjectBoardContractModel,
    { projectId: string }
>): UseQueryResult<ProjectBoardContractModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.projectBoard.contract,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) => projectBoardAPI.fetchContract(query?.projectId!),
    });
};

export const useProjectBoardUpdateContractAPIMutation = () => {
    type RequestModel = {
        contractId: string;
        postData: ProjectBoardContractUpdateFormModel;
    };

    const apiRequest = ({ contractId, postData }: RequestModel) => {
        return projectBoardAPI.updateContract(contractId, postData);
    };

    return useCustomMutation(apiRequest, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardDeleteContractAPIMutation = () => {
    return useCustomMutation(projectBoardAPI.deleteContract, {
        onSuccess: (response) => {},
        onError: (err) => {},
    });
};

export const useProjectBoardUpdateCardOrderAPIMutation = () => {
    type RequestModel = {
        cardId: string;
        postData: {
            listId: string,
            position:number  
        };
    };

    const apiRequest = ({ cardId, postData }: RequestModel) => {
        return projectBoardAPI.updateCardOrder(cardId, postData);
    };

    return useCustomMutation(apiRequest, {
        onSuccess: () => {},
        onError: () => {},
    });
};

export const useProjectBoardFetchOrganizationAPIQuery = ({
    deps,
    options,
  }: CustomUseQueryOptions<
    OrganizationListModel[],
    OrganizationListModel[]
  >): UseQueryResult<OrganizationListModel[]> => {
    return useCustomQuery({
        queryKey: QueryKeys.personnelBoard.organization,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) => projectBoardAPI.fetchOrganizations(),
    });
};

export const useProjectBoardFetchContactAPIQuery = ({
    deps,
    options,
  }: CustomUseQueryOptions<
    ContactListModel[],
    ContactListModel[]
  >): UseQueryResult<ContactListModel[]> => {
    return useCustomQuery({
        queryKey: QueryKeys.personnelBoard.contracts,
        deps,
        options: {
            ...options,
        },
        apiRequest: (query) => projectBoardAPI.fetchContacts(),
    });
};

export const useDetailProjectCardBoard = () => {
  const [deps, setDeps] = useState<{ projectId: string }>({
      projectId: ''
  });

  const title = "Title";

  const [checkLists, setCheckLists] = useState<BoardChecklistModel[]>([])
  const [projectBoardState, setProjectBoardState] = useRecoilState(projectBoard);

  const {
      isLoading,
      isSuccess,
      data: dataDetail,
      refetch: refetchFetchCard
  } = useProjectBoardFetchCardAPIQuery({
      deps,
      options: {
          enabled: !!deps.projectId
      }
  });

  const { data: dataCheckLists, refetch: refetchFetchCheckList } = useProjectBoardFetchChecklistsAPIQuery({
      deps,
      options: {
          enabled: !!deps.projectId
      }
  });

  const { data: dataComments, refetch: refetchFetchComments } = useProjectBoardFetchCommentsAPIQuery({
      deps,
      options: {
          enabled: !!deps.projectId
      }
  })

  const { mutate: projectBoardCreateChecklistItemMutate } = useProjectBoardCreateChecklistItemAPIMutation();

  const { mutate: projectBoardDeleteChecklistMutate } = useProjectBoardDeleteChecklistAPIMutation();

  const { mutate: projectBoardUpdateChecklistItemMutate } = useProjectBoardUpdateChecklistItemAPIMutation();
  
  const { mutate: projectBoardUpdateChecklistMutate } = useProjectBoardUpdateChecklistAPIMutation();

  const { mutate: projectBoardCreateChecklistMutate } = useProjectBoardCreateChecklistAPIMutation();

  const { mutate: projectBoardUpdateCardOrderMutate } = useProjectBoardUpdateCardOrderAPIMutation();

  const { data: dataOrganizations, refetch: refetchFetchOrganizations  } = useProjectBoardFetchOrganizationAPIQuery({
      options: {
          enabled: !!deps.projectId
      }
  });

  const { data: dataContacts, refetch: refetchFetchContacts } = useProjectBoardFetchContactAPIQuery({
      options: {
          enabled: !!deps.projectId
      }
  });

  
  
  useEffect(() => {
      if (deps && deps.projectId) {
          refetchFetchCard();
          refetchFetchComments();
          refetchFetchCheckList();
          refetchFetchOrganizations();
          refetchFetchContacts();
      }
  }, [deps, deps.projectId])

  useEffect(() => {
      if (dataCheckLists) {
          setCheckLists(dataCheckLists)
      }
  }, [dataCheckLists])
  
  useEffect(() => {
      if (projectBoardState && projectBoardState.projectId) {
          setDeps({ projectId: projectBoardState.projectId });
      }
  }, [projectBoardState, projectBoardState.projectId])

  const projectBoardUpdateChecklistItem = (postData: BoardChecklistItemUpdateFormModel, checklistId: string) => {
      projectBoardUpdateChecklistItemMutate({
          checklistId,
          itemId: postData.id,
          postData
      }, {
          onSuccess: (res) => {
              const lists = checkLists.map((m) => ({
                  ...m,
                  items: m.items.map((i) => ({
                      ...i,
                      isFinished: i.id === res.data.id ? res.data.isFinished : i.isFinished
                  }))
              }));
              setCheckLists(lists);
          },
          onError: () => {
              customErrorMessage(ErrorMessages.generic.update);
          }
      })
  }

  const projectBoardUpdateChecklist = (postData: BoardChecklistUpdateFormModel, checklistId: string) => {
      projectBoardUpdateChecklistMutate({
          checklistId,
          postData
      }, {
          onSuccess: (res) => {
              const lists = checkLists.map((m) => ({
                  ...m,
                  title:m.id === res.data.id ? res.data.title : m.title,
                  showFinished: m.id === res.data.id ? res.data.showFinished : m.showFinished,
              }));
              setCheckLists(lists);

          },
          onError: () => {
              customErrorMessage(ErrorMessages.generic.update);
          }
      })
  }
  
  const projectBoardDeleteChecklist = (checklistId: string) => {
      projectBoardDeleteChecklistMutate(checklistId, {
          onSuccess: () => {
              const data = checkLists?.filter((f) => f.id !== checklistId);
              setCheckLists(data || []);
              customSuccessMessage(SuccessMessages.generic.delete)
          },
          onError: () => {
              customErrorMessage(ErrorMessages.generic.delete);
          }
      })
  }

  const projectBoardCreateChecklistItem = (value: string, checklistId: string) => {
      projectBoardCreateChecklistItemMutate({
          checklistId,
          postData: {
              content:value
          }
      },
          {
              onSuccess: (res) => {
                  const lists = checkLists.map((item) => ({
                      ...item,
                      items: item.items.concat(res.data)
                  }));
                  setCheckLists(lists);
                  setProjectBoardState({
                      ...projectBoardState,
                      isCreateCheckItem: false
                  })
                  customSuccessMessage(SuccessMessages.generic.create)
              },
              onError: () => {
                  customErrorMessage(ErrorMessages.generic.create);
              }
          }
      )
  };

  const projectBoardCreateChecklist = (cardId: string) => {
      projectBoardCreateChecklistMutate({
          cardId,
          title
      },
          {
              onSuccess: (res) => {
                  const lists: BoardChecklistModel = {
                      id: res.data.id,
                      title,
                      items: [],
                      showFinished: false
                  }
                  setCheckLists(checkLists.concat(lists))
                  customSuccessMessage(SuccessMessages.generic.create)
              },
              onError: () => {
                  customErrorMessage(ErrorMessages.generic.create);
              }
          }
      )
  };

  const projectBoardUpdateCardOrder = (cardId: string,listId: string,position:number) => {
      projectBoardUpdateCardOrderMutate({
          cardId,
          postData: {
              listId,
              position
          }
      }, {
          onSuccess: (res) => {},
          onError:(err)=>{}
      })
  }
  
  return {
      checkLists,
      isLoading,
      isSuccess,
      dataDetail,
      defaultDataCardDetail,
      dataComments,
      dataContacts,
      dataOrganizations,
      refetchFetchCard,
      refetchFetchComments,
      projectBoardDeleteChecklist,
      projectBoardCreateChecklistItem,
      projectBoardUpdateChecklistItem,
      projectBoardUpdateChecklist,
      projectBoardCreateChecklist,
      projectBoardUpdateCardOrder,
      refetchFetchContacts,
      refetchFetchOrganizations,
  }
}

export const useUpdateProjectCardBoard = () => {
  const { mutate: updateCardMutate, data: dataUpdates } = useProjectBoardUpdateCardAPIMutation();
  const updateProjectCardBoard = (values: ProjectBoardDetailModel, cardId: string) => {
      updateCardMutate({
          cardId,
          postData: {
              assignees: values.assignees,
              id: cardId,
              isArchived: values.isArchived,
              listId: values.listId,
              order: values.order,
              period: {
                  ...values.period,
                  start: formatDateReq(values?.period?.start) || '',
                  end: formatDateReq(values.period?.end) || ''
              },
              priority: values?.priority,
              skills: values.skills,
              dynamicRow: values.dynamicRow,
              detail: values.detail,
              projectPeriod: {
                  datetime: formatDateReq(values?.projectPeriod?.datetime) || '',
                  immediate: values?.projectPeriod?.immediate ? true : false,
                  longTerm: values?.projectPeriod?.longTerm ? true : false,
                  remarks: formatDateReq(values?.projectPeriod?.remarks) ||'',
              },
              place: {
                  id: values?.place?.id || '',
                  remote: values?.place?.remote ? true : false,
                  remarks: values?.place?.remarks || ''
              },
              workingHours: {
                  start: formatDateReq(values?.workingHours?.start) || '',
                  end: formatDateReq(values?.workingHours?.end) || '',
                  remarks: values?.workingHours?.remarks || ''
              },
              people: {
                  amount: values?.people?.amount || 0,
                  isMultiple: values?.people?.isMultiple ? true : false,
                  isSetProposalWelcome: values?.people?.isSetProposalWelcome ? true : false,
                  remarks: values?.people?.remarks || '',
              },
              description: values?.description || '',
              price: {
                  amount: values?.price?.amount || 0,
                  isSkillAssessment: values?.price?.isSkillAssessment ? true : false,
                  remarks: values?.price?.remarks || ''
              },
              settleWidth: {
                  start: values?.settleWidth?.start || 0,
                  end: values?.settleWidth?.end || 0,
                  isFixed: values?.settleWidth?.isFixed ? true : false,
                  remarks: values?.settleWidth?.remarks || ''
              },
              settleMethod: {
                  method: values?.settleMethod?.method || '',
                  remarks: values?.settleMethod?.remarks || ''
              },
              settleIncrement: {
                  increment: values?.settleIncrement?.increment || 0,
                  remarks: values?.settleIncrement?.remarks || ''
              },
              payment: {
                  day: values?.payment?.day || 0,
                  remarks: values?.payment?.remarks || ''
              },
              interview: {
                  amount: values?.interview?.amount || 0,
                  isJoin: values?.interview?.isJoin ? true : false,
                  remarks: values?.interview?.remarks || ''
              },
              distribution: {
                  to: values?.distribution?.to || '',
                  remarks: values?.distribution?.remarks || ''
              },
              projectAttachment: values.projectAttachment || '',
          }
      })
  }
  const updateAssigneesCardBoard = (assignees: string[], cardId: string) => {
      updateCardMutate({
          cardId,
          postData: {
              assignees
          }
      })
  }
  return {
      updateProjectCardBoard,
      updateAssigneesCardBoard,
      dataUpdates
  }
}

