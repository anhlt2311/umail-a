import { useDebouncedCallback } from "use-debounce";
import { DebouncedState } from "use-debounce/lib/useDebouncedCallback";

export const useCustomDebouncedCallback = (
    callback: (values?: any) => void,
    wait = 600
): DebouncedState<(values?: any) => void> => {
    return useDebouncedCallback((values) => {
        callback(values);
    }, wait);
};
