import { message } from "antd";
import { userAPI } from "~/networking/api";
import { ErrorMessages, QueryKeys, SuccessMessages } from "~/utils/constants";
import { useCustomMutation } from "./useCustomMutation";
import {
    customErrorMessage,
    customSuccessMessage,
} from "~/components/Common/AlertMessage/AlertMessage";
import {
    UserFormFieldErrorResponseModel,
    UserFormModel,
    UserListItemAPIModel,
    UserListItemModel,
    UserRegisterFormModel,
    UserRegisterRequestDataModel,
    UserRegisterRequestModel,
    UserSpecificModel,
    UserUpdateRequestDataModel,
} from "~/models/userModel";
import getDateStr from "~/domain/date";
import { AxiosError } from "axios";
import { CustomUseQueryOptions } from "~/models/queryModel";
import { PaginationResponseModel } from "~/models/responseModel";
import { PaginationRequestModel } from "~/models/requestModel";
import { UseQueryResult } from "react-query";
import { useCustomQuery } from "./useCustomQuery";

export const convertUserListItemAPIModelToUserListItemModel = (
    data: UserListItemAPIModel
): UserListItemModel => {
    return {
        id: data.id,
        date_joined: "",
        display_name: data.display_name,
        email: data.email,
        last_login: getDateStr(data.last_login),
        is_active: data.is_active,
        first_name: data.first_name,
        last_name: data.last_name,
        role: data.role,
        tel: undefined,
        tel1: data.tel1,
        tel2: data.tel2,
        tel3: data.tel3,
        user_service_id: data.user_service_id,
    };
};

export const convertUserRegisterFormModelToUserRegisterRequestDataModel = (
    data: UserRegisterFormModel
): UserRegisterRequestDataModel => {
    const obj: UserRegisterRequestDataModel = {
        avatar: undefined,
        email: data.email,
        email_signature: data.email_signature,
        first_name: data.first_name,
        last_name: data.last_name,
        user_service_id: data.user_service_id,
        role: data.role,
        tel1: data.tel1,
        tel2: data.tel2,
        tel3: data.tel3,
        password: data.password,
        is_active: data.is_active,
    };
    if (typeof data.avatar !== "string" && typeof data.avatar !== undefined) {
        obj.avatar = data.avatar;
    }
    return obj;
};

export const convertUserFormModelToUserUpdateRequestDataModel = (
    data: UserFormModel
): UserUpdateRequestDataModel => {
    const obj: UserUpdateRequestDataModel = {
        avatar: undefined,
        email: data.email,
        email_signature: data.email_signature,
        first_name: data.first_name,
        last_name: data.last_name,
        user_service_id: data.user_service_id,
        role: data.role,
        tel1: data.tel1,
        tel2: data.tel2,
        tel3: data.tel3,
        password: data.password,
        is_active: data.is_active,
        pk: data.pk,
    };
    if (typeof data.avatar !== "string" && typeof data.avatar !== undefined) {
        obj.avatar = data.avatar;
    }
    return obj;
};

export const useFetchUsersAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    PaginationResponseModel<UserListItemAPIModel[]>,
    PaginationResponseModel<UserListItemAPIModel[]>,
    UserSpecificModel & PaginationRequestModel & { display_name?: string }
>): UseQueryResult<PaginationResponseModel<UserListItemAPIModel[]>> => {
    return useCustomQuery({
        queryKey: QueryKeys.users.users,
        deps,
        options: {
            ...options,
            keepPreviousData: true,
        },
        apiRequest: (query) => userAPI.fetchUsers(query),
    });
};

export const useUserRegisterAPIMutation = () => {
    return useCustomMutation(userAPI.register, {
        onSuccess: (result) => {
            customSuccessMessage(SuccessMessages.user.register);
        },
        onError: (err: AxiosError<UserFormFieldErrorResponseModel>) => {
            customErrorMessage(
                err.response?.data.detail || ErrorMessages.user.register
            );
        },
    });
};

export const useUserUpdateAPIMutation = () => {
    return useCustomMutation(userAPI.update, {
        onSuccess: (result) => {
            customSuccessMessage(SuccessMessages.user.update);
        },
        onError: (error: AxiosError<UserFormFieldErrorResponseModel>) => {
            customErrorMessage(
                error.response?.data.detail || ErrorMessages.user.update
            );
        },
    });
};
