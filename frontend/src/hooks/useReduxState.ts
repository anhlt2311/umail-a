import { useSelector } from "react-redux";
import { RootState } from "~/models/store";

type RootStateModel = {
    [key in keyof RootState]: RootState[key];
};

export const useReduxState = <TFnArg extends keyof RootState>(
    id: TFnArg
): RootStateModel[TFnArg] => {
    const state = useSelector((state: RootState) => state);

    return state[id];
};
