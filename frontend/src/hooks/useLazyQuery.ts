import { AxiosError, AxiosResponse } from "axios";
import { useCallback, useState } from "react";
import { useQueryClient, UseQueryResult } from "react-query";
import { UseQueryOptions } from "react-query/types";
import { ErrorDetailModel } from "~/models/responseModel";
import client from "~/networking/api";

type Props<
  TQueryFnData = unknown,
  TError = ErrorDetailModel
> = {
  queryKey?: string | any[];
  options?: UseQueryOptions<TQueryFnData, AxiosError<TError>, TQueryFnData>;
  apiRequest: (params: any) => Promise<AxiosResponse<TQueryFnData>>;
  isAutoData?: boolean;
};

export type LazyQueryType = (params: any) => Promise<any>;
export type LazyQueryDataType = UseQueryResult<unknown, AxiosError<ErrorDetailModel>> | {};

export const useLazyQuery = <
  TQueryFnData = unknown,
  TError = ErrorDetailModel
>({
  queryKey,
  options,
  apiRequest,
  isAutoData = true,
}: Props<TQueryFnData, TError>) => {
  const [data, setData] = useState<LazyQueryDataType>({});
  const queryClient = useQueryClient();

  const lazy = useCallback(
    (params: any) => {
      const k = params ? [queryKey, params] : [queryKey];

      return queryClient.fetchQuery<any, AxiosError<TError>, TQueryFnData>(
        k,
        async () => {
          try {
            const response = await apiRequest(params);

            if (response?.data && isAutoData) {
              setData(response);

              return;
            }

            return response?.data;
          } catch (err) {
            // NOTE(joshua-hashimoto): ここでエラーの共通処理を実行できる
            throw err;
          }
        },
        {
          refetchOnWindowFocus: false,
          enabled: !!client.defaults.headers.common["Authorization"], // NOTE(joshua-hashimoto): このメソッドを使える条件を設定できる。この条件では、共通axiosクライアントにAuthorizationヘッダーが設定されていない場合はAPIの呼び出しを行わない
          ...options,
        }
      );
    },
    [apiRequest, options, queryKey, isAutoData]
  );

  return {
    lazy,
    data: isAutoData ? data : {},
  };
};
