import getDateStr from "~/domain/date";
import {
    DisplaySettingChunkAPIModel,
    DisplaySettingChunkModel,
} from "~/models/displaySetting";

export const convertDisplaySettingAPIModelToDisplaySetting = (
    data: DisplaySettingChunkAPIModel
): DisplaySettingChunkModel => {
    return {
        company: data.company,
        content_hash: data.content_hash,
        modified_time: getDateStr(data.modified_time),
        modified_user: data.modified_user__name,
    };
};
