import { message } from "antd";
import { useQueryClient, UseQueryResult } from "react-query";
import getDateStr from "~/domain/date";
import { CustomUseQueryOptions } from "~/models/queryModel";
import { PaginationRequestModel } from "~/models/requestModel";
import { PaginationResponseModel } from "~/models/responseModel";
import { TagDetailModel, TagModel, TagResponseModel } from "~/models/tagModel";
import { tagAPI } from "~/networking/api";
import {
    DEFAULT_PAGE_SIZE,
    ErrorMessages,
    QueryKeys,
    SuccessMessages,
} from "~/utils/constants";
import { Override } from "~/utils/types";
import { useContactUtils } from "./useContact";
import { useCustomMutation } from "./useCustomMutation";
import { useCustomQuery } from "./useCustomQuery";
import { useScheduledEmailUtils } from "./useScheduledEmail";
import {
    customErrorMessage, customSuccessMessage,
} from "~/components/Common/AlertMessage/AlertMessage";

export const convertTagResponseModelToTagModel = (
    data: TagResponseModel
): TagModel => {
    return {
        id: data.id,
        value: data.value,
        color: data.color,
        internalValue: data.internal_value,
        createdTime: data.created_time,
        createdUser: data.created_user__name,
        modifiedTime: data.modified_time,
        modifiedUser: data.modified_user__name,
        is_skill:data.is_skill
    };
};

export const convertTagResponseModelToTagDetailModel = (
    data: TagResponseModel
): TagDetailModel => {
    return {
        id: data.id,
        value: data.value,
        color: data.color,
        internal_value: data.internal_value,
        created_user: data.created_user__name,
        created_time: getDateStr(data.created_time),
        modified_user: data.modified_user__name,
        modified_time: getDateStr(data.modified_time),
    };
};

export const useFetchTagsAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    PaginationResponseModel<TagResponseModel[]>,
    PaginationResponseModel<TagModel[]>,
    PaginationRequestModel
>): UseQueryResult<PaginationResponseModel<TagModel[]>> => {
    return useCustomQuery({
        queryKey: QueryKeys.tag.tags,
        deps,
        options: {
            onError: (err) => {
                if (err.response?.data.detail) {
                    const errorMessage = err.response.data.detail;
                    customErrorMessage(errorMessage);
                }
            },
            ...options,
            keepPreviousData: true,
            select: (result) => {
                return {
                    ...result,
                    results: result.results.map((item) =>
                        convertTagResponseModelToTagModel(item)
                    ),
                };
            },
        },
        apiRequest: (queryKey) =>
            tagAPI.fetchTags(
                queryKey ?? { page: 1, pageSize: DEFAULT_PAGE_SIZE, value: "" }
            ),
    });
};

export const useBulkDeleteTags = () => {
    const queryClient = useQueryClient();
    const {
        deleteCurrentSearchConditionTags:
            deleteContactCurrentSearchConditionTags,
    } = useContactUtils();
    const {
        deleteCurrentSearchConditionTags:
            deleteScheduledEmailCurrentSearchConditionTags,
    } = useScheduledEmailUtils();

    return useCustomMutation(tagAPI.bulkDelete, {
        onSuccess: (result) => {
            const deletedTagIds = result.data;
            deleteContactCurrentSearchConditionTags(deletedTagIds);
            deleteScheduledEmailCurrentSearchConditionTags(deletedTagIds);
            customSuccessMessage(SuccessMessages.generic.delete);
            queryClient.invalidateQueries(QueryKeys.tag.tags);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.generic.delete;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useCreateTagAPIMutation = () => {
    return useCustomMutation(tagAPI.create, {
        onSuccess: (response) => {
            customSuccessMessage(SuccessMessages.generic.create);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.generic.create;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useFetchTagAPIQuery = ({
    tagId,
    deps,
    options,
}: Override<
    CustomUseQueryOptions<TagResponseModel, TagDetailModel>,
    { tagId: string }
>): UseQueryResult<TagDetailModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.tag.tag,
        deps,
        options: {
            ...options,
            select: convertTagResponseModelToTagDetailModel,
        },
        apiRequest: () => tagAPI.fetchTag(tagId),
    });
};

export const useUpdateTagAPIMutation = () => {
    return useCustomMutation(tagAPI.update, {
        onSuccess: (response) => {
            customSuccessMessage(SuccessMessages.generic.update);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.generic.update;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useDeleteTagAPIMutation = () => {
    const {
        deleteCurrentSearchConditionTags:
            deleteContactCurrentSearchConditionTags,
    } = useContactUtils();
    const {
        deleteCurrentSearchConditionTags:
            deleteScheduledEmailCurrentSearchConditionTags,
    } = useScheduledEmailUtils();

    return useCustomMutation(tagAPI.delete, {
        onSuccess: (response) => {
            const deletedTagId = response.data.id;
            deleteContactCurrentSearchConditionTags([deletedTagId]);
            deleteScheduledEmailCurrentSearchConditionTags([deletedTagId]);
            customSuccessMessage(SuccessMessages.generic.delete);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.generic.delete;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useCreateTagFromContactAPIMutation = () => {
    const queryClient = useQueryClient();

    return useCustomMutation(tagAPI.create, {
        onSuccess: (response) => {
            queryClient.invalidateQueries(QueryKeys.tag.tags);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.generic.create;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};
