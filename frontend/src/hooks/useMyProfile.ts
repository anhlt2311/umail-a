import { AxiosError } from "axios";
import { UseQueryResult } from "react-query";
import { useDispatch } from "react-redux";
import { AuthActions } from "~/actionCreators/authActions";
import {
    customErrorMessage,
    customSuccessMessage,
} from "~/components/Common/AlertMessage/AlertMessage";
import getDateStr from "~/domain/date";
import {
    MyProfileFormFieldErrorResponseModel,
    MyProfileFormModel,
    MyProfileModel,
    MyProfileRequestModel,
    MyProfileResponseModel,
} from "~/models/myProfileModel";
import { CustomUseQueryOptions } from "~/models/queryModel";
import { myProfileAPI } from "~/networking/api";
import { ErrorMessages, QueryKeys, SuccessMessages } from "~/utils/constants";
import { useCustomMutation } from "./useCustomMutation";
import { useCustomQuery } from "./useCustomQuery";

export const convertMyProfileResponseModelToMyProfileModel = (
    data: MyProfileResponseModel
): MyProfileModel => {
    return {
        avatar: data.avatar,
        email: data.email,
        emailSignature: data.email_signature,
        firstName: data.first_name,
        lastName: data.last_name,
        userServiceId: data.user_service_id,
        modifiedTime: getDateStr(data.modified_time),
        modifiedUser: data.modified_user__name,
        role: data.role,
        tel1: data.tel1,
        tel2: data.tel2,
        tel3: data.tel3,
        password: data.password,
    };
};

export const convertMyProfileFormModelToMyProfileRequestModel = (
    data: MyProfileFormModel
): MyProfileRequestModel => {
    const obj: MyProfileRequestModel = {
        avatar: undefined,
        email: data.email,
        email_signature: data.emailSignature,
        first_name: data.firstName,
        last_name: data.lastName,
        user_service_id: data.userServiceId,
        role: data.role,
        tel1: data.tel1,
        tel2: data.tel2,
        tel3: data.tel3,
        password: data.password,
    };
    if (typeof data.avatar !== "string" && typeof data.avatar !== undefined) {
        obj.avatar = data.avatar;
    }
    return obj;
};

export const useFetchMyProfileAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    MyProfileResponseModel,
    MyProfileModel
>): UseQueryResult<MyProfileModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.myProfile.detail,
        deps,
        options: {
            ...options,
            select: convertMyProfileResponseModelToMyProfileModel,
        },
        apiRequest: myProfileAPI.fetchMyProfile,
    });
};

export const useUpdateMyProfileAPIMutation = () => {
    const dispatch = useDispatch();
    return useCustomMutation(myProfileAPI.update, {
        onSuccess: (response) => {
            customSuccessMessage(SuccessMessages.generic.update);
            dispatch(AuthActions.updateAvatarAfterChangeMyProfile(response.data));
        },
        onError: (err: AxiosError<MyProfileFormFieldErrorResponseModel>) => {
            let errorMessage = ErrorMessages.generic.update;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};
