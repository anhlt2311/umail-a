import { message } from "antd";
import { AxiosError, AxiosResponse } from "axios";
import moment from "moment";
import { useQueryClient, UseQueryResult } from "react-query";
import { PaymentFetchModel } from "~/models/paymentModel";
import { CustomUseQueryOptions } from "~/models/queryModel";
import {
    TenantCurrentStepModel,
    TenantMyCompanyFormModel,
    TenantMyCompanyRequestModel,
    TenantMyCompanyResponseModel,
    TenantMyProfileFormModel,
    TenantMyProfileModel,
    TenantMyProfileRequestDataModel,
    TenantMyProfileRequestModel,
    TenantMyProfileFormFieldErrorResponseModel,
    TenantRecaptchaModel,
    TenantRecaptchaResponseModel,
    TokenPostModel,
} from "~/models/tenantModel";
import { tenantAPI } from "~/networking/api";
import { ErrorMessages, QueryKeys, SuccessMessages } from "~/utils/constants";
import { Override } from "~/utils/types";
import { useCustomMutation } from "./useCustomMutation";
import { useCustomQuery } from "./useCustomQuery";
import {
    customErrorMessage,
    customSuccessMessage,
} from "~/components/Common/AlertMessage/AlertMessage";

export const convertTenantMyProfileFormModelToTenantMyProfileRequestDataModel =
    (data: TenantMyProfileFormModel): TenantMyProfileRequestDataModel => {
        const obj: TenantMyProfileRequestDataModel = {
            avatar: undefined,
            email: data.email,
            email_signature: data.email_signature,
            first_name: data.first_name,
            last_name: data.last_name,
            role: data.role,
            tel1: data.tel1,
            tel2: data.tel2,
            tel3: data.tel3,
            password: data.password,
            user_service_id: data.user_service_id,
        };
        if (
            typeof data.avatar !== "string" &&
            typeof data.avatar !== undefined
        ) {
            obj.avatar = data.avatar;
        }
        return obj;
    };

export const useTenantFetchRecaptchaAPIMutation = () => {
    const queryClient = useQueryClient();

    return useCustomMutation(tenantAPI.fetchRecaptcha, {
        onSuccess: (response) => {
            const data: TenantRecaptchaModel = {
                sitekey: response.data.sitekey,
                testMode: response.data.test_mode,
            };
            queryClient.setQueryData([QueryKeys.tenant.recaptcha], data);
        },
    });
};

export const useTenantFetchRecaptchaAPIQuery = ({
    deps,
    options,
}: CustomUseQueryOptions<
    TenantRecaptchaResponseModel,
    TenantRecaptchaModel
>): UseQueryResult<TenantRecaptchaModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.tenant.recaptcha,
        deps,
        options: {
            retry: false,
            enabled: true,
            ...options,
            select: (result) => {
                return {
                    ...result,
                    testMode: result.test_mode,
                };
            },
        },
        apiRequest: tenantAPI.fetchRecaptcha,
    });
};

export const useTenantRegisterBasicInfoAPIMutate = () => {
    const queryClient = useQueryClient();

    return useCustomMutation(tenantAPI.registerBasicInfo, {
        onSuccess: (response) => {
            const result = response.data;
            if (result.is_registered) {
                return;
            }
            customSuccessMessage(SuccessMessages.tenant.registerBaseInfo);
            // NOTE(joshua-hashimoto): Stepの操作用のAPIを呼び出す
            queryClient.invalidateQueries(QueryKeys.tenant.currentStep);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.tenant.registerBaseInfo;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useTenantFetchMyCompanyAPIQuery = ({
    authToken,
    deps,
    options,
}: TokenPostModel &
    CustomUseQueryOptions<
        TenantMyCompanyResponseModel,
        TenantMyCompanyFormModel
    >): UseQueryResult<TenantMyCompanyFormModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.tenant.myCompany,
        deps,
        options: {
            ...options,
            select: (result) => {
                return {
                    ...result,
                    establishment_date: moment(result.establishment_date),
                };
            },
            enabled: true,
        },
        apiRequest: () => tenantAPI.fetchMyCompany(authToken),
    });
};

export const useTenantMyCompanyAPIMutate = () => {
    const queryClient = useQueryClient();

    return useCustomMutation(tenantAPI.registerMyCompany, {
        onSuccess: (result) => {
            customSuccessMessage(SuccessMessages.tenant.registerMyCompany);
            // NOTE(joshua-hashimoto): Stepの操作用のAPIを呼び出す
            queryClient.invalidateQueries(QueryKeys.tenant.currentStep);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.tenant.registerMyCompany;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useTenantFetchMyProfileAPIQuery = ({
    authToken,
    deps,
    options,
}: TokenPostModel &
    CustomUseQueryOptions<TenantMyProfileModel>): UseQueryResult<TenantMyProfileModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.tenant.myProfile,
        deps,
        options: {
            ...options,
            enabled: true,
        },
        apiRequest: () => tenantAPI.fetchMyProfile(authToken),
    });
};

export const useTenantMyProfileAPIMutate = () => {
    const queryClient = useQueryClient();

    const onRequest = ({
        authToken,
        ...data
    }: TokenPostModel & TenantMyProfileFormModel): Promise<
        AxiosResponse<any>
    > => {
        const postData =
            convertTenantMyProfileFormModelToTenantMyProfileRequestDataModel(
                data
            );
        return tenantAPI.registerMyProfile({ authToken, ...postData });
    };

    return useCustomMutation(onRequest, {
        onSuccess: (result) => {
            successMessageCustom(SuccessMessages.tenant.registerMyProfile);
            // NOTE(joshua-hashimoto): Stepの操作用のAPIを呼び出す
            queryClient.invalidateQueries(QueryKeys.tenant.currentStep);
        },
        onError: (
          err: AxiosError<TenantMyProfileFormFieldErrorResponseModel>
        ) => {
            let errorMessage = ErrorMessages.tenant.registerMyProfile;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useTenantFetchPaymentAPIQuery = ({
    authToken,
    deps,
    options,
}: TokenPostModel &
    CustomUseQueryOptions<PaymentFetchModel>): UseQueryResult<PaymentFetchModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.tenant.payment,
        deps,
        options: {
            ...options,
            enabled: true,
            onError: (err) => {
                let errorMessage = ErrorMessages.tenant.fetchPayment;
                if (err.response?.data.detail) {
                    errorMessage = err.response.data.detail;
                }
                customErrorMessage(errorMessage);
            },
        },
        apiRequest: () => tenantAPI.fetchPayment(authToken),
    });
};

export const useTenantPaymentAPIMutate = () => {
    const queryClient = useQueryClient();

    return useCustomMutation(tenantAPI.registerPayment, {
        onSuccess: (result) => {
            customSuccessMessage(SuccessMessages.tenant.registerPayment);
            queryClient.invalidateQueries(QueryKeys.tenant.payment);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.tenant.registerPayment;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};

export const useTenantFetchCurrentStepAPIQuery = ({
    authToken,
    deps,
    options,
}: TokenPostModel &
    CustomUseQueryOptions<TenantCurrentStepModel>): UseQueryResult<TenantCurrentStepModel> => {
    return useCustomQuery({
        queryKey: QueryKeys.tenant.currentStep,
        deps,
        options: {
            ...options,
            enabled: true,
        },
        apiRequest: () => tenantAPI.fetchCurrentStep(authToken),
    });
};

export const useTenantFinishAPIMutate = () => {
    const queryClient = useQueryClient();

    return useCustomMutation(tenantAPI.finishRegister, {
        onSuccess: (result) => {
            // NOTE(joshua-hashimoto): Stepの操作用のAPIを呼び出す
            queryClient.invalidateQueries(QueryKeys.tenant.currentStep);
        },
        onError: (err) => {
            const detail = err.response?.data.detail;
            if (detail) {
                customErrorMessage(detail);
            }
        },
    });
};
