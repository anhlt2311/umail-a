import { customErrorMessage } from "~/components/Common/AlertMessage/AlertMessage";
import { thumbnailAPI } from "~/networking/api";
import { useCustomMutation } from "./useCustomMutation";

export const useDeleteThumbnailAPIMutation = () => {
    return useCustomMutation(thumbnailAPI.delete, {
        onSuccess: (response) => {},
        onError: (err) => {
            const errorMessage = err.response?.data.detail;
            if (errorMessage) {
                customErrorMessage(errorMessage);
            }
        },
    });
};
