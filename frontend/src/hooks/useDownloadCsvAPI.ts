import { useDispatch, useSelector } from "react-redux";
import iconv from "iconv-lite";
import { Endpoint, downloadCsv } from "~/domain/api";
import { ContactCsvDownloadType } from "~/models/csvControlModel";
import { RootState } from "~/models/store";
import { useCustomMutation } from "./useCustomMutation";
import { csvDownloadAPI } from "~/networking/api";
import FileDownload from "js-file-download";
import moment from "moment";
import { ErrorMessages, SuccessMessages } from "~/utils/constants";
import { useSetRecoilState } from "recoil";
import { isCsvDownloading as isCsvDownloadingAtom } from "~/recoil/atom";
import { customErrorMessage, customSuccessMessage } from "~/components/Common/AlertMessage/AlertMessage";

export const useDownloadCsvAPI = () => {
    const token = useSelector((state: RootState) => state.login.token);
    const dispatch = useDispatch();
    const baseURL = Endpoint.getBaseUrl();

    const downloadOrganizationCsv = (fileName: string) => {
        const url = `${baseURL}/${Endpoint.organizationsCsv}`;
        dispatch(downloadCsv(token, url, fileName));
    };

    const downloadContactCsv = (
        fileName: string,
        downloadType: ContactCsvDownloadType
    ) => {
        const url =
            downloadType == "all"
                ? `${baseURL}/${Endpoint.contactsFullCsv}`
                : `${baseURL}/${Endpoint.contactsCsv}`;
        dispatch(downloadCsv(token, url, fileName));
    };

    return {
        downloadOrganizationCsv,
        downloadContactCsv,
    };
};

export const useOrganizationCsvDownloadAPIMutation = () => {
    const setIsCsvDownloading = useSetRecoilState(isCsvDownloadingAtom);

    return useCustomMutation(csvDownloadAPI.organizationCsvDownload, {
        onMutate: () => {
            setIsCsvDownloading(true);
        },
        onSuccess: (result) => {
            const convertedText = iconv.encode(result.data, "SJIS");
            const newBlob = new Blob([convertedText], { type: "text/csv" });
            const fileName = `取引先一覧_${moment().format(
                "YYYY年MM月DD日"
            )}時点分.csv`;
            FileDownload(newBlob, fileName);
            customSuccessMessage(SuccessMessages.csv.download);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.csv.download;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
        onSettled: () => {
            setIsCsvDownloading(false);
        },
    });
};

export const useContactCsvDownloadAPIMutation = () => {
    const setIsCsvDownloading = useSetRecoilState(isCsvDownloadingAtom);

    return useCustomMutation(csvDownloadAPI.contactCsvDownload, {
        onMutate: () => {
            setIsCsvDownloading(true);
        },
        onSuccess: (result) => {
            const convertedText = iconv.encode(result.data, "SJIS");
            const newBlob = new Blob([convertedText], { type: "text/csv" });
            const fileName = `取引先担当者一覧_配信条件含まない_${moment().format(
                "YYYY年MM月DD日"
            )}時点分.csv`;
            FileDownload(newBlob, fileName);
            customSuccessMessage(SuccessMessages.csv.download);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.csv.download;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
        onSettled: () => {
            setIsCsvDownloading(false);
        },
    });
};

export const useContactCsvDownloadWithConditionAPIMutation = () => {
    const setIsCsvDownloading = useSetRecoilState(isCsvDownloadingAtom);

    return useCustomMutation(csvDownloadAPI.contactCsvDownloadWithCondition, {
        onMutate: () => {
            setIsCsvDownloading(true);
        },
        onSuccess: (result) => {
            const convertedText = iconv.encode(result.data, "SJIS");
            const newBlob = new Blob([convertedText], { type: "text/csv" });
            const fileName = `取引先担当者一覧_配信条件含む_${moment().format(
                "YYYY年MM月DD日"
            )}時点分.csv`;
            FileDownload(newBlob, fileName);
            customSuccessMessage(SuccessMessages.csv.download);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.csv.download;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
        onSettled: () => {
            setIsCsvDownloading(false);
        },
    });
};
