import { useMemo } from "react";
import { useLocation } from "react-router-dom";
import queryString from "query-string";
import { parseQueryString } from "~/utils/utils";

export const useQueryString = (options?: queryString.ParseOptions) => {
    const { search: rawQueryString } = useLocation();
    const queryParams = useMemo(
        () => parseQueryString(rawQueryString, options),
        [rawQueryString]
    );

    const queryParamsToObj = <T extends Object>() => {
        return queryParams as unknown as T;
    };

    return {
        queryString,
        rawQueryString,
        queryParams,
        queryParamsToObj,
    };
};
