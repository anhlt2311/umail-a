import {
    customErrorMessage,
    customInfoMessage,
    customSuccessMessage,
} from "~/components/Common/AlertMessage/AlertMessage";
import getDateStr from "~/domain/date";
import {
    CustomUseQueryOptions,
    CustomUseQueryResult,
} from "~/models/queryModel";
import {
    SharedEmailResponseModel,
    SharedEmailModel,
    SharedEmailFormModel,
    SharedEmailRequestModel,
} from "~/models/sharedEmailModel";
import { sharedEmailAPI } from "~/networking/api";
import {
    ErrorMessages,
    InfoMessages,
    QueryKeys,
    SuccessMessages,
} from "~/utils/constants";
import { useCustomMutation } from "./useCustomMutation";
import { useCustomQuery } from "./useCustomQuery";

export const convertSharedEmailResponseModelToSharedEmailModel = (
    data: SharedEmailResponseModel
): SharedEmailModel => {
    const obj: SharedEmailModel = {
        id: data.id,
        messageId: data.message_id,
        attachments: data.attachments,
        category: data.category,
        estimatedCategory: data.estimated_category,
        fromAddress: data.from_address,
        fromName: data.from_name,
        html: data.html,
        sentDate: getDateStr(data.sent_date),
        shared: data.shared,
        staffInChargeId: data.staff_in_charge__id,
        staffInChargeName: data.staff_in_charge__name,
        subject: data.subject,
        text: data.text,
        format: !!data.html ? "html" : "text",
    };
    return obj;
};

export const convertSharedEmailFormModelToSharedEmailRequestModel = (
    data: SharedEmailFormModel
): SharedEmailRequestModel => {
    const obj: SharedEmailRequestModel = {
        staff_in_charge_id: data.staffInChargeId,
    };
    return obj;
};

type UseSharedEmailFetchDetailProps = CustomUseQueryOptions<
    SharedEmailResponseModel,
    SharedEmailModel
> & {
    id: string;
};

export const useSharedEmailFetchDetailAPIQuery = ({
    id,
    deps,
    options,
}: UseSharedEmailFetchDetailProps): CustomUseQueryResult<SharedEmailModel> => {
    const queryDeps =
        deps && typeof deps === "object" ? { id, ...deps } : { id };

    return useCustomQuery({
        queryKey: QueryKeys.sharedEmail.detail,
        deps: queryDeps,
        options: {
            select: convertSharedEmailResponseModelToSharedEmailModel,
            ...options,
        },
        apiRequest: () => sharedEmailAPI.fetchDetail(id),
    });
};

export const useSharedEmailForwardAPIMutation = () => {
    type RequestModel = {
        id: string;
        postData: SharedEmailFormModel;
    };

    const apiRequest = ({ id, postData }: RequestModel) => {
        const data =
            convertSharedEmailFormModelToSharedEmailRequestModel(postData);
        return sharedEmailAPI.forward(id, data);
    };

    return useCustomMutation(apiRequest, {
        onMutate: (variables) => {
            const infoMessage = InfoMessages.sharedEmails.transfer;
            customInfoMessage(infoMessage);
        },
        onSuccess: (response) => {
            const successMessage = SuccessMessages.sharedEmails.transfer;
            customSuccessMessage(successMessage);
        },
        onError: (err) => {
            const errorMessage = ErrorMessages.sharedEmails.transfer;
            customErrorMessage(errorMessage);
        },
    });
};

export const useSharedEmailDeleteAPIMutation = () => {
    return useCustomMutation(sharedEmailAPI.delete, {
        onSuccess: (response) => {
            const successMessage = SuccessMessages.generic.delete;
            customSuccessMessage(successMessage);
        },
        onError: (err) => {
            let errorMessage = ErrorMessages.generic.delete;
            if (err.response?.data.detail) {
                errorMessage = err.response.data.detail;
            }
            customErrorMessage(errorMessage);
        },
    });
};
