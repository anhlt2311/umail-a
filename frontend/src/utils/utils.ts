import { FormInstance } from "antd";
import moment, { Moment } from "moment";
import queryString from "query-string";

export const stringBreak = (str: string, breakNumber = 10) => {
    let brokenWords = [];
    for (let i = 0; i < str.length / breakNumber; i++) {
        brokenWords.push(str.substr(i * breakNumber, breakNumber));
    }
    return brokenWords;
};

export const zeroLeftPadding = (num: number, length: number = 2) => {
    return num.toString().padStart(length, "0");
};

export const disabledFutureDates = (current: Moment) => {
    return current && current > moment().endOf("day");
};

export const disabledPastDates = (current: Moment) => {
    return current && current < moment().endOf("day");
};

export const isEmpty = (obj: object) => {
    return !Object.keys(obj).length;
};

export const randString = (): string => {
    return Math.random().toString(36).substring(2);
};

export const generateRandomToken = (): string => {
    return randString() + randString() + randString();
};

export const clearCacheData = async () => {
    try {
        const cacheNames = await caches.keys();
        cacheNames.forEach((name) => {
            caches.delete(name);
        });
    } catch (err) {
        console.error(err);
    }
};

export const convertObjectToFormData = (
    data: Object,
    convertAll = false
): FormData => {
    const formData = new FormData();
    for (const [key, value] of Object.entries(data)) {
        if (convertAll) {
            formData.append(key, value);
        } else {
            if (!!value) {
                formData.append(key, value);
            }
        }
    }
    return formData;
};

export const getBase64 = (
    file: File,
    callback: (value: FileReader["result"]) => void
) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        callback(reader.result);
    };
    reader.onerror = function (error) {
        console.log("Error: ", error);
    };
};

export const b64toBlob = (
    b64Data: string,
    contentType: string,
    sliceSize = 512
) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
};

export const isFormDisabled = (
    form: FormInstance,
    requiredFields: string[]
) => {
    return (
        !form.isFieldsTouched(requiredFields, true) ||
        !!form.getFieldsError().filter(({ errors }) => errors.length).length
    );
};

export const formatDate = (date?: string | Moment) => {
    return date ? moment(date).format('YYYY/MM/DD') : '';
}

export const formatDateReq = (date?: string | Moment) => {
    return date ? moment(date).format('YYYY-MM-DD') : undefined;
}

export const formatDateEndReq = (date?: string | Moment) => {
  return date ? moment(date).format('YYYY-MM-DD HH:mm:ss') : undefined;
}

export const calculateAge = (date:string) => {
    return moment().diff(date, 'years');   
}

export const formatDateTime = (date?: string | Moment) => {
    return date ? moment(date).format('YYYY-MM-DD HH:mm:00.000Z'):undefined
}
export const camelToSnakeCase = (value: string): string => {
    return value.replace(/[A-Z]/g, (letter, index) =>
        index === 0 ? letter.toLowerCase() : `_${letter.toLowerCase()}`
    );
};

export const parseQueryString = (
    values: string,
    options?: queryString.ParseOptions
) => {
    return queryString.parse(values, {
        arrayFormat: "comma",
        parseNumbers: true,
        parseBooleans: true,
        ...options,
    });
};

export const stringifyQueryString = <T>(
    values: T,
    options?: queryString.StringifyOptions
) => {
    return queryString.stringify(values, {
        arrayFormat: "comma",
        skipEmptyString: true,
        skipNull: true,
        ...options,
    });
};

export const createQueryString = (queryStringObj: {
    [key: string]: any;
}): string => {
    const obj = Object.assign(
        {},
        ...Object.keys(queryStringObj).map((key) => ({
            [camelToSnakeCase(key)]: queryStringObj[key],
        }))
    );
    return stringifyQueryString(obj);
};
