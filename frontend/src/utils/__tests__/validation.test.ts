import { EMAIL_REGEX } from "../constants";

describe("validation test", () => {
    describe("test EMAIL_REGEX", () => {
        test("matching pattern", () => {
            const targets = [
                "example@example.com",
                "example.example@example.com",
                "exam%ple@example.com",
                "example@example.jp",
                "exam_ple@example.co.jp",
                "example@example.co.jp.com",
            ];
            for (const target of targets) {
                expect(target.match(EMAIL_REGEX)).toBeTruthy();
            }
        });
        test("un-matching pattern", () => {
            const targets = [
                "aaaaaaaaaaaaaaa",
                "example$example.com",
                "example@example.c",
                "example@example.c.j",
            ];
            for (const target of targets) {
                expect(target.match(EMAIL_REGEX)).toBeFalsy();
            }
        });
    });
});
