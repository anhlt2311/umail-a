import {
  ContactSendTypeModel,
  ContactWantesLocationModel,
} from "~/models/contactModel";
import { DisplaySettingModel } from "~/models/displaySetting";
import { OrganizationStatusModel } from "~/models/organizationModel";
import { UserRoleModel } from "~/models/roleModel";
import {
  ScheduledEmailSendTypeModel,
  ScheduledEmailStatusModel,
  ScheduledEmailTextFormatModel,
} from "~/models/scheduledEmailModel";
import jaJP from "antd/lib/locale/ja_JP";
import { Locale } from "antd/lib/locale-provider";
import { AuthorizedActionsModel, RoleModel } from "~/models/authModel";
import { TagColorSelectModel } from "~/models/tagModel";
import {
  PersonnelBoardCardMenuOptionModel,
  PersonnelBoardAffiliationSelectModel,
  PersonnelBoardParallelSelectModel,
} from "~/models/personnelBoardModel";
import {
  BoardPriorityModel,
  BoardScheduledEmailTemplateCheckboxModel,
} from "~/models/boardCommonModel";

export const infoColor = "#1890ff";
export const warningColor = "#faad14";
export const infoTooltipLinkColor = "#A7CF69";
export const alertTooltipLinkColor = "#56840F";
export const themeColor = "#FFFFD2";
export const secondaryThemeColor = "#FFFFCC";
export const primaryColor = "#A7CF69";
export const secondaryColor = "#C3F477";
export const dangerColor = "#E1585A";
export const defaultBlackColor = "#262626"; // NOTE(joshua-hashimoto): Ant Design default black color
export const helpTextColor = "#8C8C8C";
export const iconPrimaryColor = "#82b435";
export const iconCustomColor = "#615748";

export const sharedEmailChartColor = [
  "rgba(212,56,12,1)",
  "rgba(212,108,11,1)",
  "rgba(12,212,100,1)",
  "rgba(30,56,195,1)",
  "rgba(123,141,236,1)",
  "rgba(126,206,238,1)",
];

export const proper = "proper";
export const individual = "individual";

export const quasiMandate = "quasi-mandate";
export const workerDispatch = "worker-dispatch";

export const intercomAppId = "j539oghr";

export const SYSTEM_NOTIFICATION_LAST_FETCHED_TIMESTAMP =
  "systemNotificationLastFetchedTimestamp";

export const USER_ROLES: RoleModel[] = [
  "master",
  "admin",
  "manager",
  "leader",
  "member",
  "guest",
];

export const ROLES: UserRoleModel[] = [
  {
    value: "master",
    title: "マスター",
    color: "red",
  },
  {
    value: "admin",
    title: "管理者",
    color: "volcano",
  },
  {
    value: "manager",
    title: "責任者",
    color: "cyan",
  },
  {
    value: "leader",
    title: "リーダー",
    color: "green",
  },
  {
    value: "member",
    title: "メンバー",
    color: "yellow",
  },
  {
    value: "guest",
    title: "ゲスト",
    color: "purple",
  },
];

export const NOT_ROLES: UserRoleModel[] = [
    {
        value: "not:master",
        title: "not:マスター",
        color: "red",
    },
    {
        value: "not:admin",
        title: "not:管理者",
        color: "volcano",
    },
    {
        value: "not:manager",
        title: "not:責任者",
        color: "cyan",
    },
    {
        value: "not:leader",
        title: "not:リーダー",
        color: "green",
    },
    {
        value: "not:member",
        title: "not:メンバー",
        color: "yellow",
    },
    {
        value: "not:guest",
        title: "not:ゲスト",
        color: "purple",
    },
];

export const ORGANIZATION_STATUS: OrganizationStatusModel[] = [
  {
    value: "prospective",
    title: "見込み客",
    color: "geekblue",
    search: true,
  },
  {
    value: "approached",
    title: "アプローチ済",
    color: "orange",
    search: true,
  },
  {
    value: "exchanged",
    title: "情報交換済",
    color: "green",
    search: true,
  },
  {
    value: "client",
    title: "契約実績有",
    color: "volcano",
    search: true,
  },
];

export const ORGANIZATION_NOT_STATUS: OrganizationStatusModel[] = [
  {
    value: "not:prospective",
    title: "not:見込み客",
    color: "geekblue",
    search: true,
  },
  {
    value: "not:approached",
    title: "not:アプローチ済",
    color: "orange",
    search: true,
  },
  {
    value: "not:exchanged",
    title: "not:情報交換済",
    color: "green",
    search: true,
  },
  {
    value: "not:client",
    title: "not:契約実績有",
    color: "volcano",
    search: true,
  },
];

export const CONTACT_SEND_TYPE: ContactSendTypeModel[] = [
  {
    value: "job",
    title: "案件",
    color: "magenta",
  },
  {
    value: "personnel",
    title: "要員",
    color: "cyan",
  },
];

export const WANTS_LOCATION: ContactWantesLocationModel[] = [
  {
    value: "wants_location_hokkaido_japan",
    title: "北海道",
    color: "geekblue",
  },
  {
    value: "wants_location_touhoku_japan",
    title: "東北",
    color: "orange",
  },
  {
    value: "wants_location_kanto_japan",
    title: "関東",
    color: "green",
  },
  {
    value: "wants_location_chubu_japan",
    title: "中部",
    color: "brown",
  },
  {
    value: "wants_location_toukai_japan",
    title: "東海",
    color: "purple",
  },
  {
    value: "wants_location_kansai_japan",
    title: "関西",
    color: "yellow",
  },
  {
    value: "wants_location_shikoku_japan",
    title: "四国",
    color: "volcano",
  },
  {
    value: "wants_location_chugoku_japan",
    title: "中国",
    color: "cyan",
  },
  {
    value: "wants_location_kyushu_japan",
    title: "九州",
    color: "pink",
  },
  {
    value: "wants_location_other_japan",
    title: "その他",
    color: "gray",
  },
];

export const NOT_WANTS_LOCATION: ContactWantesLocationModel[] = [
  {
    value: "not:wants_location_hokkaido_japan",
    title: "not:北海道",
    color: "geekblue",
  },
  {
    value: "not:wants_location_touhoku_japan",
    title: "not:東北",
    color: "orange",
  },
  {
    value: "not:wants_location_kanto_japan",
    title: "not:関東",
    color: "green",
  },
  {
    value: "not:wants_location_chubu_japan",
    title: "not:中部",
    color: "brown",
  },
  {
    value: "not:wants_location_toukai_japan",
    title: "not:東海",
    color: "purple",
  },
  {
    value: "not:wants_location_kansai_japan",
    title: "not:関西",
    color: "yellow",
  },
  {
    value: "not:wants_location_shikoku_japan",
    title: "not:四国",
    color: "volcano",
  },
  {
    value: "not:wants_location_chugoku_japan",
    title: "not:中国",
    color: "cyan",
  },
  {
    value: "not:wants_location_kyushu_japan",
    title: "not:九州",
    color: "pink",
  },
  {
    value: "not:wants_location_other_japan",
    title: "not:その他",
    color: "gray",
  },
];

export const SCHEDULED_EMAIL_STATUS: ScheduledEmailStatusModel[] = [
    {
        value: "draft",
        title: "下書き",
        color: "gray",
    },
    {
        value: "queued",
        title: "配信待ち",
        color: "yellow",
    },
    {
        value: "sending",
        title: "配信中",
        color: "orange",
    },
    {
        value: "sent",
        title: "配信済",
        color: "green",
    },
    {
        value: "error",
        title: "エラー",
        color: "volcano",
    },
];

export const SCHEDULED_EMAIL_NOT_STATUS: ScheduledEmailStatusModel[] = [
    {
        value: "not:draft",
        title: "not:下書き",
        color: "gray",
    },
    {
        value: "not:queued",
        title: "not:配信待ち",
        color: "yellow",
    },
    {
        value: "not:sending",
        title: "not:配信中",
        color: "orange",
    },
    {
        value: "not:sent",
        title: "not:配信済",
        color: "green",
    },
    {
        value: "not:error",
        title: "not:エラー",
        color: "volcano",
    },
];

export const SCHEDULED_EMAIL_SEND_TYPE: ScheduledEmailSendTypeModel[] = [
  {
    value: "job",
    title: "案件",
    color: "magenta",
  },
  {
    value: "personnel",
    title: "要員",
    color: "cyan",
  },
  {
    value: "other",
    title: "ご案内",
    color: "lime",
  },
  {
    value: "not:job",
    title: "not:案件",
    color: "magenta",
  },
  {
    value: "not:personnel",
    title: "not:要員",
    color: "cyan",
  },
  {
    value: "not:other",
    title: "not:ご案内",
    color: "lime",
  },
];

export const SCHEDULED_EMAIL_TEXT_FORMAT: ScheduledEmailTextFormatModel[] = [
  {
    value: "text",
    title: "テキスト",
    color: "blue",
  },
  {
    value: "html",
    title: "HTML",
    color: "red",
  },
];

export const DISPLAY_SETTING_ITEMS: DisplaySettingModel = {
  organizations: {
    page_size: 10,
    search: [
      { key: "corporate_number", title: "法人番号" },
      { key: "name", title: "取引先名" },
      { key: "category", title: "取引先ステータス" },
      { key: "score", title: "取引先評価" },
      { key: "country", title: "国籍" },
      { key: "establishment_date", title: "設立年月" },
      { key: "settlement_month", title: "決算期" },
      { key: "address", title: "住所" },
      { key: "tel", title: "TEL" },
      { key: "fax", title: "FAX" },
      { key: "domain_name", title: "URL" },
      { key: "employee_number", title: "社員数" },
      { key: "has_distribution", title: "商流" },
      { key: "contract", title: "請負" },
      { key: "capital", title: "資本金" },
      { key: "license", title: "保有資格" },
      { key: "branch_name", title: "取引先支店名" },
      { key: "branch_address", title: "取引先支店住所" },
      { key: "branch_tel", title: "取引先支店TEL" },
      { key: "branch_fax", title: "取引先支店FAX" },
      { key: "establishment_year", title: "取引に必要な設立年数" },
      {
        key: "capital_man_yen_required_for_transactions",
        title: "取引に必要な資本金",
      },
      {
        key: "license_required_for_transactions",
        title: "取引に必要な資格",
      },
      { key: "comment_user", title: "コメント投稿者" },
      { key: "created_user", title: "ページ作成者" },
      { key: "modified_user", title: "ページ編集者" },
    ],
    table: [
      { key: "corporate_number", title: "法人番号" },
      { key: "name", title: "取引先名" },
      { key: "organization_category__order", title: "取引先ステータス" },
      { key: "score", title: "取引先評価" },
      { key: "organization_country__order", title: "国籍" },
      { key: "establishment_date", title: "設立年月" },
      { key: "settlement_month", title: "決算期" },
      { key: "address", title: "住所" },
      { key: "tel1", title: "TEL" },
      { key: "fax1", title: "FAX" },
      { key: "domain_name", title: "URL" },
      { key: "organization_employee_number__order", title: "社員数" },
      { key: "has_distribution", title: "商流" },
      { key: "contract", title: "請負" },
      { key: "capital_man_yen", title: "資本金" },
      { key: "has_p_mark_or_isms", title: "保有資格" },
      { key: "branch_name", title: "取引先支店名" },
      { key: "branch_address", title: "取引先支店住所" },
      { key: "branch_tel1", title: "取引先支店TEL" },
      { key: "branch_fax1", title: "取引先支店FAX" },
      { key: "establishment_year", title: "取引に必要な設立年数" },
      {
        key: "capital_man_yen_required_for_transactions",
        title: "取引に必要な資本金",
      },
      { key: "p_mark_or_isms", title: "取引に必要な資格" },
      { key: "created_time", title: "作成日時" },
      { key: "modified_time", title: "更新日時" },
    ],
    require: [
      { key: "corporate_number", title: "法人番号" },
      { key: "score", title: "取引先評価" },
      { key: "country", title: "国籍" },
      { key: "establishment_date", title: "設立年月" },
      { key: "settlement_month", title: "決算期" },
      { key: "address", title: "住所" },
      { key: "building", title: "建物" },
      { key: "tel", title: "TEL" },
      { key: "fax", title: "FAX" },
      { key: "domain_name", title: "URL" },
      { key: "employee_number", title: "社員数" },
      { key: "has_distribution", title: "商流" },
      { key: "contract", title: "請負" },
      { key: "capital_man_yen", title: "資本金" },
      { key: "license", title: "保有資格" },
    ],
  },
  contacts: {
    page_size: 10,
    search: [
      { key: "name", title: "取引先担当者名" },
      { key: "organizations", title: "所属取引先" },
      { key: "email_to", title: "メールアドレス(TO)" },
      { key: "email_cc", title: "メールアドレス(CC)" },
      { key: "tel", title: "TEL" },
      { key: "position", title: "役職" },
      { key: "department", title: "部署" },
      { key: "staff", title: "自社担当者" },
      { key: "last_visit", title: "最終訪問日" },
      { key: "tag", title: "タグ" },
      { key: "category", title: "相性" },
      { key: "preference", title: "配信種別" },
      { key: "wants_location", title: "希望エリア" },
      { key: "comment_user", title: "コメント投稿者" },
      { key: "created_user", title: "ページ作成者" },
      { key: "modified_user", title: "ページ編集者" },
    ],
    table: [
      { key: "last_name", title: "取引先担当者名" },
      { key: "organization__name", title: "所属取引先" },
      { key: "email", title: "メールアドレス(TO)" },
      { key: "cc_addresses__email", title: "メールアドレス(CC)" },
      { key: "tel1", title: "TEL" },
      { key: "position", title: "役職" },
      { key: "department", title: "部署" },
      { key: "staff__last_name", title: "自社担当者" },
      { key: "last_visit", title: "最終訪問日" },
      { key: "tag_objects", title: "タグ" },
      { key: "category", title: "相性" },
      { key: "contactjobtypepreferences", title: "配信種別" },
      { key: "wants_location_hokkaido_japan", title: "希望エリア" },
      { key: "created_time", title: "作成日時" },
      { key: "modified_time", title: "更新日時" },
    ],
    require: [
      { key: "email_cc", title: "メールアドレス(CC)" },
      { key: "tel", title: "TEL" },
      { key: "position", title: "役職" },
      { key: "department", title: "部署" },
      { key: "staff", title: "自社担当者" },
      { key: "last_visit", title: "最終訪問日" },
      { key: "tag", title: "タグ" },
      { key: "category", title: "相性" },
    ],
  },
  shared_emails: {
    page_size: 10,
    search: [
      { key: "from_name", title: "差出人" },
      { key: "subject", title: "件名" },
      { key: "text", title: "本文" },
      { key: "has_attachments", title: "添付" },
      { key: "staff", title: "自社担当者" },
      { key: "date_range", title: "受信日" },
      { key: "comment_user", title: "コメント投稿者" },
    ],
    table: [
      { key: "sender", title: "差出人" },
      { key: "subject", title: "件名" },
      { key: "has_attachments", title: "添付" },
      { key: "staff_in_charge__last_name", title: "自社担当者" },
      { key: "sent_date", title: "受信日時" },
    ],
  },
  scheduled_mails: {
    page_size: 10,
    search: [
      { key: "send_type", title: "配信種別" },
      { key: "subject", title: "件名" },
      { key: "text", title: "本文" },
      { key: "attachments", title: "ファイル形式" },
      { key: "status", title: "配信ステータス" },
      { key: "sender", title: "配信者" },
      { key: "date_to_send", title: "配信予定日" },
      { key: "sent_date", title: "配信完了日" },
      { key: "text_format", title: "フォーマット" },
      { key: "send_total_count", title: "配信数" },
      { key: "open_count", title: "開封数" },
      { key: "created_user", title: "ページ作成者" },
      { key: "modified_user", title: "ページ編集者" },
    ],
    table: [
      { key: "scheduled_email_send_type__order", title: "配信種別" },
      { key: "subject", title: "件名" },
      { key: "attachments", title: "ファイル形式" },
      { key: "scheduled_email_status__order", title: "配信ステータス" },
      { key: "sender__last_name", title: "配信者" },
      { key: "date_to_send", title: "配信予定日時" },
      { key: "sent_date", title: "配信完了日時" },
      { key: "text_format", title: "フォーマット" },
      { key: "send_total_count", title: "配信数" },
      { key: "open_count_format", title: "開封数" },
      { key: "open_ratio", title: "開封率" },
      { key: "created_time", title: "作成日時" },
      { key: "modified_time", title: "更新日時" },
    ],
  },
  users: {
    page_size: 10,
    search: [
      { key: "name", title: "ユーザー名" },
      { key: "role", title: "権限" },
      { key: "email", title: "メールアドレス" },
      { key: "tel", title: "TEL" },
      { key: "last_login", title: "最終ログイン" },
    ],
    table: [
      { key: "name", title: "ユーザー名" },
      { key: "user_role__order", title: "権限" },
      { key: "email", title: "メールアドレス" },
      { key: "tel", title: "TEL" },
      { key: "last_login", title: "最終ログイン" },
    ],
  },
};

export const LIST_COMMENT_VIEW_LIMIT_COUNT = 5;

export const pickerLocaleConfig = {
  lang: {
    locale: "ja_JP",
    placeholder: "日付を選択",
    today: "今日",
    now: "現在",
    backToToday: "今日へ戻る",
    ok: "Ok",
    clear: "クリア",
    month: "月",
    year: "年",
    timeSelect: "時刻を選択",
    dateSelect: "日付を選択",
    monthSelect: "月を選択",
    yearSelect: "年を選択",
    decadeSelect: "10年ごとに選択",
    yearFormat: "YYYY年",
    dateFormat: "YYYY年MM月dd日",
    dayFormat: "dd",
    dateTimeFormat: "YYYY年MM月dd日 HH:mm:ss",
    monthFormat: "MMMM",
    monthBeforeYear: true,
    previousMonth: "前月",
    nextMonth: "次月",
    previousYear: "前年",
    nextYear: "次年",
    previousDecade: "前10年",
    nextDecade: "次10年",
    previousCentury: "前世紀",
    nextCentury: "次世紀",
  },
  timePickerLocale: {
    placeholder: "時刻を選択",
  },
  dateFormat: "YYYY-MM-DD",
  dateTimeFormat: "YYYY-MM-DD HH:mm:ss",
  weekFormat: "YYYY-wo",
  monthFormat: "YYYY-MM",
};

export const CommonGraphDataSet = {
  fill: false,
  lineTension: 0.1,
  borderCapStyle: "butt",
  borderDash: [],
  borderDashOffset: 0.0,
  borderJoinStyle: "miter",
  pointBorderWidth: 1,
  pointHoverRadius: 6,
  pointHoverBorderWidth: 2,
  pointRadius: 4,
  pointHitRadius: 16,
  pointBackgroundColor: "#fff",
  pointHoverBorderColor: "rgba(220,220,220,1)",
};

export const customLocale: Locale = {
  ...jaJP,
  Transfer: {
    ...jaJP.Transfer,
    selectAll: "全て選択",
    removeAll: "全て非選択",
    selectInvert: "選択／非選択を入れ替え",
  },
};

export const TABLE_OPERATION_SELECTABLE_LIMIT = 10;

export const HEADER_HEIGHT = 64;

export const PAYED_PLAN_TERMINATION_POSTPONEMENT_PERIOD = 10;

export const QueryKeys = {
  addon: {
    masterList: "addon.masterList",
    purchasedList: "addon.purchasedList",
    purchase: "addon.purchase",
    revoke: "addon.revoke",
  },
  ajaxSelect: {
    items: "ajaxSelect.items",
  },
  auth: {
    authorizedActions: "auth.authorizedActions",
  },
  csvDownload: {
    organization: "csvDownload.organization",
    contact: {
      withCondition: "csvDownload.contact.withCondition",
      withoutCondition: "csvDownload.contact.withoutCondition",
    },
    isLoading: "csvDownload.isLoading",
    isSuccess: "csvDownload.isSuccess",
  },
  myProfile: {
    detail: "myProfile.detail",
  },
  scheduledEmail: {
    attachmentSizeExtended: "scheduledEmail.attachmentSizeExtended",
    openerList: "scheduledEmail.openerList",
    search: "scheduledEmail.search",
  },
  scheduledEmailSetting: {
    detail: "scheduledEmailSetting.detail",
  },
  sharedEmail: {
    detail: "sharedEmail.detail",
  },
  personnelBoard: {
    lists: "personnelBoard.lists",
    list: "personnelBoard.list",
    cards: "personnelBoard.cards",
    card: "personnelBoard.card",
    checklists: "personnelBoard.checklists",
    checklist: "personnelBoard.checklist",
    checklistItems: "personnelBoard.checklistItems",
    checklistItem: "personnelBoard.checklistItem",
    comments: "personnelBoard.comments",
    comment: "personnelBoard.comment",
    subComments: "personnelBoard.subComments",
    contracts: "personnelBoard.contracts",
    contract: "personnelBoard.contract",
    organization: "personnelBoard.organization",
    trainStations: "personnelBoard.trainStations",
    skillSheet: "personnelBoard.skillSheet",
    sortCards: "personnelBoard.sortCards",
  },
  plan: {
    userPlan: "plan.userPlan",
    accountLimit: "plan.accountLimit",
    summary: "plan.summary",
  },
  projectBoard: {
    lists: "projectBoard.lists",
    list: "projectBoard.list",
    cards: "projectBoard.cards",
    card: "projectBoard.card",
    checklists: "projectBoard.checklists",
    checklist: "projectBoard.checklist",
    checklistItems: "projectBoard.checklistItems",
    checklistItem: "projectBoard.checklistItem",
    comments: "projectBoard.comments",
    comment: "projectBoard.comment",
    subComments: "projectBoard.subComments",
    contracts: "projectBoard.contracts",
    contract: "projectBoard.contract",
  },
  purchaseHistory: {
    purchaseHistories: "purchaseHistory.purchaseHistories",
  },
  tag: {
    tags: "tag.tags",
    tag: "tag.tag",
  },
  tenant: {
    recaptcha: "tenant.recaptcha",
    basicInfo: "tenant.basicInfo",
    myCompany: "tenant.myCompany",
    myProfile: "tenant.myProfile",
    payment: "tenant.payment",
    currentStep: "tenant.currentStep",
  },
  others: {
    isAppLoading: "isAppLoading",
  },
  users: {
    users: "users.users",
  },
  personnel: {
    personnel: "personnel",
  },
  project: {
    project: "project",
  },
};

// NOTE(joshua-hashimoto): 作成したが、reducerのtestでこのファイルからimportできなかったので、現在は仕様していない。問題が解決次第使用するので残しておく
export const AUTHORIZED_ACTIONS_DEFAULT_VALUE: AuthorizedActionsModel = {
  organizations: {
    create: false,
    update: false,
    delete: false,
    column_setting: false,
    csv_upload: false,
    csv_download: false,
    blacklist: false,
    search_template: false,
  },
  contacts: {
    create: false,
    update: false,
    delete: false,
    column_setting: false,
    csv_upload: false,
    csv_download: false,
    search_template: false,
  },
  scheduled_mails: {
    create: false,
    update: false,
    copy: false,
    delete: false,
    column_setting: false,
    select_sender: false,
    change_to_draft: false,
  },
  shared_emails: {
    delete: false,
    select_staff: false,
    select_all_staff: false,
  },
  users: {
    _all: false,
  },
  my_company: {
    _all: false,
  },
  tags: {
    _all: false,
  },
  display_settings: {
    _all: false,
  },
  column_setting: {
    organization: false,
    contact: false,
    scheduled_mail: false,
  },
  _editable_roles: [],
  shared_email_settings: {
    _all: false,
  },
  scheduled_email_settings: {
    _all: false,
  },
  addon: {
    _all: false,
    purchase: false,
  },
  notification_rules: {
    _all: false,
  },
  comment_template: {
    read: false,
    create: false,
    update: false,
    delete: false,
    insert: false,
  },
  comment: {
    read: false,
    create: false,
    update: false,
    delete: false,
    pin: false,
  },
  another_user_comment: {
    read: false,
    update: false,
    pin: false,
    delete: false,
  },
  billing: {
    _all: false,
  },
  purchase_history: {
    _all: false,
  },
  plan: {
    _all: false,
    purchase: false,
    user_add: false,
    user_delete: false,
  },
  account: {
    delete: false,
  },
};

export const TooltipMessages = {
  scheduledEmails: {
    undraftable:
      "「配信中」「配信済」「エラー」ステータスのメールを「下書き」ステータスに変更することはできません。",
    unupdateable:
      "「配信中」「配信済」「エラー」ステータスのメールを更新することはできません。",
    undeleteable: "「配信中」ステータスのメールを削除することはできません。",
    cancelUploadfileError:
      "「配信中」「配信済」「エラー」ステータスのメールに添付ファイルをアップロードすることはできません。",
  },
  scheduledEmailSetting: {
    addons: "アドオンを追加／削除することで自動で切り替わります。",
    warning:
      "受信側のメーラーの設定によってはHTMLが無効になることがあり、その場合は開封情報を取得することができません。",
  },
  plan: {
    removeUser:
      "利用ユーザー数が上限に達している場合、ユーザー上限を削除することができません。利用ユーザー数を減らしてから再度お試しください。",
  },
  table: {
    overLimit: "一括で選択可能な件数は最大で10件です。",
    notSelected: "1件以上の選択が必要です。",
    deleteSelected: "選択行を削除",
  },
  comment: {
    nonRemovableComment:
      "固定されている返信コメントは削除することができません。",
  },
};

export const SuccessMessages = {
  generic: {
    delete: "アイテムが削除されました。",
    create: "アイテムが作成されました。",
    copy: "アイテムがコピーされました。",
    update: "アイテムが更新されました。",
  },
  account: {
    disable: "退会に成功しました。",
    passwordReset: "パスワード再設定手順のメールを送信しました。",
  },
  addon: {
    purchase: "アドオンの購入に成功しました。",
    revoke: "アドオンの削除に成功しました。",
  },
  csv: {
    download: "CSVのダウンロードに成功しました。",
  },
  scheduledEmailSetting: {
    testConnect:
      "接続テストに成功しました。登録／更新ボタンをクリックして設定を保存してください。",
  },
  plan: {
    purchase: "プランを購入しました。",
    addUser: "ユーザーの追加に成功しました。",
    removeUser: "ユーザーの削除に成功しました。",
  },
  tenant: {
    registerBaseInfo:
      "会員情報の登録に成功しました。メールを確認してください。",
    registerMyCompany: "自社プロフィールの登録に成功しました。",
    registerMyProfile: "個人プロフィールの登録に成功しました。",
    registerPayment: "お支払い情報の登録に成功しました。",
    finish:
      "個人プロフィール登録およびテナント登録が完了しました。ログインをしてください。",
  },
  payment: {
    register: "お支払い情報の登録に成功しました。",
    delete: "お支払い情報の削除に成功しました。",
    update: "お支払い情報の更新に成功しました。",
  },
  purchaseHistory: {
    download: "領収書のダウンロードに成功しました。",
  },
  user: {
    enable: "ユーザーが有効化されました。",
    delete: "ユーザーが削除されました。",
    disable: "ユーザーが無効化されました。",
    register: "本登録を完了しました。",
    update: "ユーザーが更新されました。",
  },
  auth: {
    logout: "ログアウトしました。",
  },
  scheduledEmails: {
    saveTemplate: "テンプレートを作成しました。",
    deleteTemplate: "テンプレートを削除しました。",
  },
  sharedEmails: {
    transfer: "自社担当者への転送が完了しました。",
  },
};

export const ErrorMessages = {
  commentError: "コメント欄は空にしてください。",
  isNotAuthorized: "特定の権限で操作できます。",
  generic: {
    delete: "アイテムの削除に失敗しました。",
    create: "アイテムの作成に失敗しました。",
    copy: "アイテムのコピーに失敗しました。",
    update: "アイテムの更新に失敗しました。",
    selectRequired: "必ず選択してください。",
  },
  validation: {
    length: {
      max3: "3桁以内で入力してください。",
      max9: "9桁以内で入力してください。",
      max10: "10桁以内で入力してください。",
      max13: "13桁以内で入力してください。",
      max15: "15桁以内で入力してください。",
      maxUserId15: "15文字以内で入力してください。",
      max50: "50文字以内で入力してください。",
      max100: "100文字以内で入力してください。",
      max300: "300文字以内で入力してください。",
      max500: "500文字以内で入力してください。",
      max1000: "1000文字以内で入力してください。",
    },
    regex: {
      onlyHankakuNumber: "整数で入力してください。",
      onlyHankakuNumberAndUnderScore:
        "大小英数字とアンダースコア(_)のみ使用可能です。",
      onlyHankaku: "半角英数記号で入力してください。",
      onlyZenkaku: "全角文字で入力してください。",
      space: "スペースを入力することはできません。",
      password: "大小英数字記号混在で10-50桁で入力してください。",
      checkListMail: "正しい形式のメールアドレスを入力してください。",
    },
    password: "大小英数字記号混在で10-50桁で入力してください。",
    email: "メールアドレスを入力してください。",
  },
  account: {
    disable: "退会に失敗しました。",
    accountNotFound: "入力された情報に紐づくアカウントが見つかりませんでした。",
  },
  auth: {
    login: "ログインに失敗しました、ユーザー情報を確認してください。",
    authorizedActions:
      "権限情報の取得に失敗しました。お手数ですが、再度ログインをしてください。",
    invalidCredentials: "入力したIDもしくはパスワードが間違っています。",
  },
  commentTemplate: {
    titleEmpty: "テンプレートのタイトルを入力してください。",
    contentEmpty: "テンプレートの本文を入力してください。",
  },
  form: {
    required: "必須項目です。",
    telNotComplete: "TELの欄は全て入力してください。",
    faxNotComplete: "FAXの欄は全て入力してください。",
    numberOnly: "半角数字でご入力ください。",
    passwordMismatch: "パスワードが一致しません。",
  },
  addon: {
    addonMaster: "アドオンの一覧を取得するのに失敗しました。",
    purchasedList: "アドオンの購入履歴を取得するのに失敗しました。",
    purchase: "アドオンの購入に失敗しました。",
    revoke: "アドオンの削除に失敗しました。",
  },
  csv: {
    download: "CSVのダウンロードに失敗しました。",
    nothingToDownload: "検索結果が0件です。",
  },
  scheduledEmailSetting: {
    testConnect:
      "接続テストに失敗しました。入力情報に誤りがないかご確認ください。",
  },
  scheduledEmail: {
    fileIsDeleted: "添付ファイルは削除されました。",
    download: "ダウンロードに失敗しました。",
    maxsizeTextArea: "全角5000文字または半角10000文字以内で入力してください。",
    maxsize50Input: "テンプレート名は50文字以内で入力してください。",
    requiredInput: "テンプレート名を入力してください。",
  },
  payment: {
    fetchInfo: "お支払い情報の取得に失敗しました。",
    register: "お支払い情報の登録に失敗しました。",
    delete: "お支払い情報の削除に失敗しました。",
    update: "お支払い情報の更新に失敗しました。",
  },
  purchaseHistory: {
    download: "領収書のダウンロードに失敗しました。",
  },
  plan: {
    purchase: "プランの購入に失敗しました。",
    addUser: "ユーザーを追加することができませんでした。",
    removeUser: "ユーザーを削除することができませんでした。",
    planInfo: "お客様のご利用プラン情報を取得することができませんでした。",
  },
  tenant: {
    registerBaseInfoTOSConfirm:
      "同意いただけない場合は登録を完了することができません。",
    registerBaseInfoForm:
      "データの登録に失敗しました。入力情報に誤りがないかご確認ください。",
    registerBaseInfo: "会員情報の登録に失敗しました。",
    registerMyCompany: "自社プロフィール情報の登録に失敗しました。",
    registerMyProfile: "個人プロフィール情報の登録に失敗しました。",
    registerPayment: "お支払い情報の登録に失敗しました。",
    fetchPayment: "お支払い情報の取得に失敗しました。",
    isRegistered: "既にテナント作成は完了しています。ログインをしてください。",
  },
  user: {
    enable: "ユーザーの有効化に失敗しました。",
    delete: "ユーザーの削除に失敗しました。",
    disable: "ユーザーの無効化に失敗しました。",
    register: "ユーザーの登録に失敗しました。",
    update: "ユーザーの更新に失敗しました。",
    updateStatusFailed: "ユーザー編集より必須項目を入力してください。",
  },
  sharedEmails: {
    connectionTest:
      "接続テストに失敗しました。メールアドレスかパスワードに誤りがないかご確認ください。",
    transfer: "メールを転送できませんでした。",
  },
};

export const InfoMessages = {
  scheduledEmails: {
    messageTootipTemplateButtonSaved: "現在の基本情報を保存します。",
    messageTootipTemplateButtonDelete:
      "選択中の基本情報テンプレートを削除します。",
    messageTootipTemplateButtonSavedSendingAndSentAndErorrStatus:
      "「配信中」「配信済」「エラー」ステータスのメールでは基本情報テンプレートを選択することはできません。",
  },
  sharedEmails: {
    transfer: "自社担当者に転送中です。",
  },
};

export const Links = {
  services: {
    addonTos: "https://cmrb.jp/terms-of-options",
    management: "https://h-basis.co.jp",
    helpCenter: "https://intercom.help/cmrb/ja/",
    houjinBangou: "https://www.houjin-bangou.nta.go.jp/",
    privacyAgreement: "https://cmrb.jp/privacy-agreement",
    privacyPolicy: "https://h-basis.co.jp/privacy-policy",
    termsOfSales: "https://cmrb.jp/terms-of-sale",
    tos: "https://cmrb.jp/terms",
    payjp: "https://pay.jp",
    topics: "https://cmrb.jp/topics/",
  },
  helps: {
    addon: {
      scheduledEmails: {
        attachmentSize:
          "https://intercom.help/cmrb/ja/articles/5765624-%E9%85%8D%E4%BF%A1%E6%B7%BB%E4%BB%98%E5%AE%B9%E9%87%8F%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6",
        htmlOpenInfo:
          "https://intercom.help/cmrb/ja/articles/5765623-%E9%85%8D%E4%BF%A1%E9%96%8B%E5%B0%81%E6%83%85%E5%A0%B1%E3%81%AE%E5%8F%96%E5%BE%97%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6",
        htmlOpenInfoReceivePeriod:
          "https://intercom.help/cmrb/ja/articles/5765628-%E9%85%8D%E4%BF%A1%E9%96%8B%E5%B0%81%E6%83%85%E5%A0%B1%E3%81%AE%E5%8F%96%E5%BE%97%E6%9C%9F%E9%96%93%E3%81%AE%E5%BB%B6%E9%95%B7%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6",
        removeCmrbAdd:
          "https://intercom.help/cmrb/ja/articles/5777819-html%E9%85%8D%E4%BF%A1%E3%83%A1%E3%83%BC%E3%83%AB%E3%81%AE%E3%82%B3%E3%83%A2%E3%83%AC%E3%83%93%E5%BA%83%E5%91%8A%E3%82%92%E6%B6%88%E3%81%99%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6",
        sendLimit:
          "https://intercom.help/cmrb/ja/articles/5809436-1%E9%80%9A%E3%81%82%E3%81%9F%E3%82%8A%E3%81%AE%E9%80%81%E4%BF%A1%E5%8F%AF%E8%83%BD%E4%BB%B6%E6%95%B0%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6",
        deliveryInterval:
          "https://intercom.help/cmrb/ja/articles/5890838-%E9%85%8D%E4%BF%A1%E9%96%93%E9%9A%94%E3%81%AE%E6%99%82%E7%9F%AD%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6",
      },
    },
    contacts: {
      settingHasSendGuide:
        "https://intercom.help/cmrb/ja/articles/6168345-%E9%85%8D%E4%BF%A1%E3%83%A1%E3%83%BC%E3%83%AB%E3%81%AE-%E3%81%94%E6%A1%88%E5%86%85-%E3%81%AE%E7%94%A8%E9%80%94%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6%E8%A9%B3%E3%81%97%E3%81%8F%E7%9F%A5%E3%82%8A%E3%81%9F%E3%81%84",
      settingSteamingCondition:
        "https://intercom.help/cmrb/ja/articles/5570180-%E5%8F%96%E5%BC%95%E5%85%88%E6%8B%85%E5%BD%93%E8%80%85%E3%82%92%E7%99%BB%E9%8C%B2-%E7%B7%A8%E9%9B%86%E3%81%99%E3%82%8B#h_2e0756aaec",
    },
    csv: {
      upload:
        "https://intercom.help/cmrb/ja/articles/5759135-%E5%8F%96%E5%BC%95%E5%85%88-%E5%8F%96%E5%BC%95%E5%85%88%E6%8B%85%E5%BD%93%E8%80%85csv%E7%99%BB%E9%8C%B2",
    },
    filter: {
      partialMatch:
        "https://intercom.help/cmrb/ja/articles/5569959-絞り込み検索について#h_348219d4a0",
      or: "https://intercom.help/cmrb/ja/articles/5569959-%E7%B5%9E%E3%82%8A%E8%BE%BC%E3%81%BF%E6%A4%9C%E7%B4%A2%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_d153601c36",
      and: "https://intercom.help/cmrb/ja/articles/5569959-%E7%B5%9E%E3%82%8A%E8%BE%BC%E3%81%BF%E6%A4%9C%E7%B4%A2%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_3efbb0227f",
      includingBlockList:
        "https://intercom.help/cmrb/ja/articles/5569973-%E7%B5%9E%E3%82%8A%E8%BE%BC%E3%81%BF%E6%A4%9C%E7%B4%A2%E6%93%8D%E4%BD%9C%E3%83%9C%E3%82%BF%E3%83%B3%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_35b072baaf",
      includingOutOfTradingCondition:
        "https://intercom.help/cmrb/ja/articles/5569973-%E7%B5%9E%E3%82%8A%E8%BE%BC%E3%81%BF%E6%A4%9C%E7%B4%A2%E6%93%8D%E4%BD%9C%E3%83%9C%E3%82%BF%E3%83%B3%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_d857fa3370",
    },
    payments: {
      addonCalculation:
        "https://intercom.help/cmrb/ja/articles/5765609-%E3%82%A2%E3%83%89%E3%82%AA%E3%83%B3%E3%81%AE%E5%88%A9%E7%94%A8%E6%9C%9F%E9%99%90%E3%81%AF%E3%81%A9%E3%81%AE%E3%82%88%E3%81%86%E3%81%AB%E7%AE%97%E5%87%BA%E3%81%95%E3%82%8C%E3%81%BE%E3%81%99%E3%81%8B-%E3%81%BE%E3%81%9F-%E5%88%A9%E7%94%A8%E6%96%99%E9%87%91%E3%81%AF%E6%97%A5%E5%89%B2%E3%82%8A%E3%81%AB%E3%81%AA%E3%82%8A%E3%81%BE%E3%81%99%E3%81%8B",
    },
    tags: {
      settings:
        "https://intercom.help/cmrb/ja/articles/5570205-%E3%82%BF%E3%82%B0%E8%A8%AD%E5%AE%9A%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6",
      details:
        "https://intercom.help/cmrb/ja/articles/6231147-%E3%82%B9%E3%82%AD%E3%83%AB%E5%85%BC%E7%94%A8-%E3%81%A8%E3%81%AF%E4%BD%95%E3%81%A7%E3%81%99%E3%81%8B",
    },
    scheduledEmails: {
      deliveryCommercialDistribution:
        "https://intercom.help/cmrb/ja/articles/5570185-%E9%85%8D%E4%BF%A1%E3%83%A1%E3%83%BC%E3%83%AB%E4%BA%88%E7%B4%84%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_30191adb5f",
      autoInsert:
        "https://intercom.help/cmrb/ja/articles/5570185-%E9%85%8D%E4%BF%A1%E3%83%A1%E3%83%BC%E3%83%AB%E4%BA%88%E7%B4%84%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_1240c0ad48",
      numberOfDeliveries:
        "https://intercom.help/cmrb/ja/articles/5570185-%E9%85%8D%E4%BF%A1%E3%83%A1%E3%83%BC%E3%83%AB%E4%BA%88%E7%B4%84%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_a1e5eacd82",
      writableHTML:
        "https://intercom.help/cmrb/ja/articles/5856241-html%E3%83%A1%E3%83%BC%E3%83%AB%E4%BD%9C%E6%88%90%E6%99%82%E3%81%AB%E4%BD%BF%E7%94%A8%E3%81%A7%E3%81%8D%E3%81%AA%E3%81%84html%E3%82%BF%E3%82%B0%E3%81%AF%E3%81%82%E3%82%8A%E3%81%BE%E3%81%99%E3%81%8B",
      propertyDetail:
        "https://intercom.help/cmrb/ja/articles/5570185-%E9%85%8D%E4%BF%A1%E3%83%A1%E3%83%BC%E3%83%AB%E4%BA%88%E7%B4%84%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_9c7309cf47",
        undraftable:
            "「配信中」「配信済」「エラー」ステータスのメールを「下書き」ステータスに変更することはできません。",
        unupdateable:
            "「配信中」「配信済」「エラー」ステータスのメールを更新することはできません。",
        undeleteable:
            "「配信中」ステータスのメールを削除することはできません。",
        cancelUploadfileError:
            "「配信中」「配信済」「エラー」ステータスのメールに添付ファイルをアップロードすることはできません。",
        errorInput: "入力内容を確認してください。",
    },
    users: {
      activeToggle:
        "https://intercom.help/cmrb/ja/articles/5570203-%E3%83%A6%E3%83%BC%E3%82%B6%E3%83%BC%E3%81%AE%E6%9C%89%E5%8A%B9%E5%8C%96-%E7%84%A1%E5%8A%B9%E5%8C%96%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_8df5242d1f",
    },
    commercialDistribution: {
      about:
        "https://intercom.help/cmrb/ja/articles/5569988-%E5%95%86%E6%B5%81%E3%82%92%E6%8A%9C%E3%81%91%E3%82%8B-%E6%A9%9F%E8%83%BD%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_79c7225872",
      restriction:
        "https://intercom.help/cmrb/ja/articles/5569987-%E5%95%86%E6%B5%81%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_0ab3c61501",
      transactionTermsPatterns:
        "https://intercom.help/cmrb/ja/articles/5863511-%E5%8F%96%E5%BC%95%E6%9D%A1%E4%BB%B6%E3%81%A8%E5%95%86%E6%B5%81%E3%81%AE%E7%B5%84%E3%81%BF%E5%90%88%E3%82%8F%E3%81%9B%E3%81%A8%E5%90%84%E7%94%BB%E9%9D%A2%E3%81%A7%E3%81%AE%E8%A1%A8%E7%A4%BA%E3%83%91%E3%82%BF%E3%83%BC%E3%83%B3%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6-%E6%97%A9%E8%A6%8B%E8%A1%A8",
      exit: "https://intercom.help/cmrb/ja/articles/5569987-%E5%95%86%E6%B5%81%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_d1604518de",
    },
    myCompany: {
      exceptionalOrganizations:
        "https://intercom.help/cmrb/ja/articles/5570197-%E8%87%AA%E7%A4%BE%E3%83%97%E3%83%AD%E3%83%95%E3%82%A3%E3%83%BC%E3%83%AB%E3%82%92%E7%B7%A8%E9%9B%86%E3%81%99%E3%82%8B#h_872aeee32b",
    },
    plan: {
      trialExpired:
        "https://intercom.help/cmrb/ja/articles/5884891-%E7%84%A1%E6%96%99%E3%83%88%E3%83%A9%E3%82%A4%E3%82%A2%E3%83%AB%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6#h_6cc1c45cff",
    },
  },
    userEdit: {
        cannotDisabledUser: "ログイン中ユーザーを無効化することはできません。",
        cannotDeleteUser: "ログイン中ユーザーを削除することはできません。",
    }
};

export const HANKAKU_NUMBER_REGEX = new RegExp(/^[0-9]+$/);
export const SPECIAL_CHARACTER_USERID = ["comment", "here", "card", "board"];
export const HANKAKU_USERID_REGEX = new RegExp(/^[a-zA-z0-9_]+$/);
export const CSV_DOWNLOAD_LIMIT = 1000;
export const ONLY_HANKAKU_REGEX = new RegExp(/^[a-zA-Z0-9!-/:-@¥[-`{-~\s\-]*$/);
export const ONLY_ZENKAKU_REGEX = new RegExp(/^[^\x01-\x7E]+$/);
export const RESTRICT_SPACE_REGEX = new RegExp(/^(?!.*\s).*$/);
export const PASSWORD_REGEX = new RegExp(
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.`˜~!@#$%^&*()_+\-={}\[\]¥|:;"'<>,?])[a-zA-Z0-9.`˜~!@#$%^&*()_+\-={}\[\]¥|:;"'<>,?]{10,50}$/
);

export const DEFAULT_PAGE_SIZE = 10;
export const EMAIL_REGEX =
  /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9]){1,10}?$/;

export const PRODUCTION_DOMAIN = "app.cmrb.jp";
export const PAYJP_LIVE_PUBLIC_KEY = "pk_live_4a79b912e59ea01fd5cf6f16";
export const PAYJP_DEV_PUBLIC_KEY = "pk_test_7de0013318d7e72f5503502c";

export const DISABLED_COLOR = "#D9D9D9";

export const TAG_COLORS: TagColorSelectModel[] = [
  {
    title: "Default",
    value: "default",
  },
  {
    title: "Geek Blue",
    value: "geekblue",
  },
  {
    title: "Orange",
    value: "orange",
  },
  {
    title: "Brown",
    value: "brown",
  },
  {
    title: "Purple",
    value: "purple",
  },
  {
    title: "Yellow",
    value: "yellow",
  },
  {
    title: "Volcano",
    value: "volcano",
  },
  {
    title: "Cyan",
    value: "cyan",
  },
  {
    title: "Pink",
    value: "pink",
  },
  {
    title: "Gray",
    value: "gray",
  },
];

export const BOARD_PRIORITY_OPTIONS: BoardPriorityModel[] = [
  {
    title: "緊急",
    value: "urgent",
    color: "#EB5A46",
  },
  {
    title: "重大",
    value: "important",
    color: "#FFAB4A",
  },
  {
    title: "高",
    value: "high",
    color: "#F2D600",
  },
  {
    title: "中",
    value: "medium",
    color: "#61BD4F",
  },
  {
    title: "低",
    value: "low",
    color: "#0079BF",
  },
];

export const PERSONNEL_BOARD_PARALLEL_OPTIONS: PersonnelBoardParallelSelectModel[] =
  [
    {
      title: "なし",
      value: "none",
    },
    {
      title: "あり",
      value: "parallel",
    },
    {
      title: "1件",
      value: "parallel_one",
    },
    {
      title: "2件",
      value: "parallel_two",
    },
    {
      title: "3件以上",
      value: "parallel_three",
    },
  ];

export const PERSONNEL_BOARD_AFFILIATION_OPTIONS: PersonnelBoardAffiliationSelectModel[] =
  [
    {
      title: "弊社プロパー",
      value: "proper",
    },
    {
      title: "弊社フリーランス",
      value: "freelance",
    },
    {
      title: "1社先プロパー",
      value: "one_company_proper",
    },
    {
      title: "1社先フリーランス",
      value: "one_company_freelancer",
    },
    {
      title: "2社先プロパー",
      value: "two_company_proper",
    },
    {
      title: "2社先フリーランス",
      value: "two_company_freelancer",
    },
    {
      title: "3社先以上プロパー",
      value: "three_company_proper",
    },
    {
      title: "3社先以上フリーランス",
      value: "three_company_freelancer",
    },
  ];

export const dataDevelop: BoardScheduledEmailTemplateCheckboxModel = {
  selectedJobType: [
    {
      key: "Job_Type_1",
      name: "personneltype_dev_designer",
      title: "デザイナー",
      checked: false,
    },
    {
      key: "Job_Type_2",
      name: "personneltype_dev_front",
      title: "フロントエンド",
      checked: false,
    },
    {
      key: "Job_Type_3",
      name: "personneltype_dev_server",
      title: "バックエンド",
      checked: false,
    },
    {
      key: "Job_Type_4",
      name: "personneltype_dev_pm",
      title: "PM・ディレクター",
      checked: false,
    },
    {
      key: "Job_Type_5",
      name: "personneltype_dev_other",
      title: "その他",
      checked: false,
    },
  ],
  selectedJobSkill: [
    {
      key: "Job_Skill_1",
      name: "personnelskill_dev_youken",
      title: "要件定義",
      checked: false,
    },
    {
      key: "Job_Skill_2",
      name: "personnelskill_dev_kihon",
      title: "基本設計",
      checked: false,
    },
    {
      key: "Job_Skill_3",
      name: "personnelskill_dev_syousai",
      title: "詳細設計",
      checked: false,
    },
    {
      key: "Job_Skill_4",
      name: "personnelskill_dev_seizou",
      title: "製造",
      checked: false,
    },
    {
      key: "Job_Skill_5",
      name: "personnelskill_dev_test",
      title: "テスト・検証",
      checked: false,
    },
    {
      key: "Job_Skill_6",
      name: "personnelskill_dev_hosyu",
      title: "保守・運用",
      checked: false,
    },
    {
      key: "Job_Skill_7",
      name: "personnelskill_dev_beginner",
      title: "未経験",
      checked: false,
    },
  ],
};
export const dataInfrastructure: BoardScheduledEmailTemplateCheckboxModel = {
  selectedJobType: [
    {
      key: "Job_Type_6",
      name: "personneltype_infra_server",
      title: "サーバー",
      checked: false,
    },
    {
      key: "Job_Type_7",
      name: "personneltype_infra_network",
      title: "ネットワーク",
      checked: false,
    },
    {
      key: "Job_Type_8",
      name: "personneltype_infra_security",
      title: "セキュリティー",
      checked: false,
    },
    {
      key: "Job_Type_9",
      name: "personneltype_infra_database",
      title: "データベース",
      checked: false,
    },
    {
      key: "Job_Type_10",
      name: "personneltype_infra_sys",
      title: "情報システム",
      checked: false,
    },
    {
      key: "Job_Type_11",
      name: "personneltype_infra_other",
      title: "その他",
      checked: false,
    },
  ],
  selectedJobSkill: [
    {
      key: "Job_Skill_8",
      name: "jobskill_dev_youken",
      title: "要件定義",
      checked: false,
    },
    {
      key: "Job_Skill_8",
      name: "jobskill_dev_kihon",
      title: "基本設計",
      checked: false,
    },
    {
      key: "Job_Skill_9",
      name: "jobskill_dev_syousai",
      title: "詳細設計",
      checked: false,
    },
    {
      key: "Job_Skill_10",
      name: "jobskill_dev_seizou",
      title: "製造",
      checked: false,
    },
    {
      key: "Job_Skill_11",
      name: "jobskill_dev_test",
      title: "テスト・検証",
      checked: false,
    },
    {
      key: "Job_Skill_12",
      name: "jobskill_dev_hosyu",
      title: "保守・運用",
      checked: false,
    },
    {
      key: "Job_Skill_13",
      name: "jobskill_infra_kanshi",
      title: "監視",
      checked: false,
    },
    {
      key: "Job_Skill_14",
      name: "jobskill_dev_beginner",
      title: "未経験",
      checked: false,
    },
  ],
};
export const dataOther: BoardScheduledEmailTemplateCheckboxModel = {
  selectedJobType: [
    {
      key: "Job_Type_12",
      name: "jobtype_other_eigyo",
      title: "営業・事務",
      checked: false,
    },
    {
      key: "Job_Type_13",
      name: "jobtype_other_kichi",
      title: "基地局",
      checked: false,
    },
    {
      key: "Job_Type_14",
      name: "jobtype_other_support",
      title: "コールセンター・サポートデスク",
      checked: false,
    },
    {
      key: "Job_Type_15",
      name: "jobtype_other_other",
      title: "その他",
      checked: false,
    },
  ],
};
export const EMAIL_HOST_BLOCKED = [
  "gmail.com",
  "yahoo.co.jp",
  "ymail.ne.jp",
  "outlook.jp",
  "outlook.com",
  "hotmail.co.jp",
  "live.jp",
  "vivaldi.net",
  "protonmail.com",
  "mprotonmail.ch",
  "tutao.de",
  "tutanota.com",
  "zoho.com",
  "zoho.eu",
  "zoho.com.cn",
  "zoho.in",
  "zohomail.com",
  "inbox.lv",
  "aol.jp",
  "aol.com",
  "aol.co.uk",
  "aol.fr",
  "aol.de",
  "aim.com",
  "biglobe.ne.jp",
  "cx.117.cx",
  "momo-mail.com",
  "smoug.net",
  "mail.goo.ne.jp",
  "goo.jp",
  "yandex.com",
  "mail.ru",
  "inbox.ru",
  "list.ru",
  "bk.ru",
  "icloud.com",
];

export const HelpMessages = {
  cannotUsePrefix: "は使用できません。",
  planSummary: {
    freeTrial: {
      expiresToday: "無料トライアルは本日23:59まで",
      expiresIn: (remainingDays: number): string => {
        return `無料トライアルは残り${remainingDays}日まで`;
      },
    },
    payed: {
      expiresToday: "本日23:59にプランが停止します",
      expiresIn: (remainingDays: number): string => {
        return `プラン停止まで残り${remainingDays}日`;
      },
    },
  },
};

export const MAX_VISIBLE_AVATARS = 5;

export const PERSONNEL_CARD_SORT_MENUES: PersonnelBoardCardMenuOptionModel[] = [
  { label: "自社担当者：昇順", value: "staff_in_charge_name" },
  { label: "自社担当者：降順", value: "-staff_in_charge_name" },
  { label: "期限：近い順", value: "end" },
  { label: "期限：遠い順", value: "-end" },
  { label: "優先度：低い順", value: "priority" },
  { label: "優先度：高い順", value: "-priority" },
  { label: "年齢：若い順", value: "age" },
  { label: "年齢：高い順", value: "-age" },
  { label: "性別：男性順", value: "gender" },
  { label: "性別：女性順", value: "-gender" },
  { label: "稼働：近い順", value: "operate_period" },
  { label: "稼働：遠い順", value: "-operate_period" },
  { label: "所属：浅い順", value: "affiliation" },
  { label: "所属：深い順", value: "-affiliation" },
  { label: '単金：低い順', value: 'price'},
  { label: "単金：高い順", value: "-price" },
  { label: "並行：少ない順", value: "parallel" },
  { label: "並行：多い順", value: "-parallel" },
];

export const PERSONNEL_CARD_ARCHIVE_MENUES: PersonnelBoardCardMenuOptionModel[] = [
  { label: "実行する", value: "archive" },
];