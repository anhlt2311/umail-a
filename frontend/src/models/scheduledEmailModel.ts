import { number, string } from "prop-types";
import { Override, TAG_MODEL } from "~/utils/types";
import { PaginationResponseModel } from "./responseModel";

export type ScheduledMailTemplateResponseModel = {
    templates: any;
    total_available_count: number;
};

export type ScheduledMailTemplateModel = {
    templates: any;
    total_available_count: number;
};

export type ScheduledMailTemplateFormModel = {
    sender?: string; // Foreign key
    sender__name?: string;
    subject?: string;
    title?: string;
    text_format?: "text" | "html";
    text?: string;
    send_copy_to_sender?: boolean;
    send_copy_to_share?: boolean;
};
export type ScheduledEmailTextFormatValueModel = "text" | "html";

export type ScheduledEmailTextFormatTitleModel = "テキスト" | "HTML";

export type ScheduledEmailSendStatusModel =
    | "draft"
    | "queued"
    | "sending"
    | "sent"
    | "error"
    | "timeout";

export type ScheduledEmailStatusModel = Override<
    TAG_MODEL,
    {
        value:
            | "draft"
            | "queued"
            | "sending"
            | "sent"
            | "error"
            | "not:draft"
            | "not:queued"
            | "not:sending"
            | "not:sent"
            | "not:error";
    }
>;

export type ScheduledEmailSendTypeModel = Override<
    TAG_MODEL,
    {
        value:
            | "job"
            | "personnel"
            | "other"
            | "not:job"
            | "not:personnel"
            | "not:other";
    }
>;

export type ScheduledEmailTextFormatModel = Override<
    TAG_MODEL,
    {
        value: ScheduledEmailTextFormatValueModel;
    }
>;

// TODO(joshua-hashimoto): プロパティ名を変更する必要があるかもしれない
type OrganizationModel = {
    id: string;
    name: string;
};

// TODO(joshua-hashimoto): プロパティ名を変更する必要があるかもしれない
type ContactModel = {
    id: string;
    name: string;
};

export type ScheduledEmailOpenerListItemModel = {
    id: string;
    email: string;
    opened_time: string;
    organization: OrganizationModel;
    contact: ContactModel;
};

export type ScheduledEmailTooltipModalTableDataSourceModel = {
    organization: OrganizationModel;
    contact: ContactModel;
};

export type ScheduledEmailAttachmentSizeExtendedResponseModel = {
    is_attachment_size_extended: boolean;
};

export type ScheduledEmailAttachmentSizeExtendedModel = {
    isAttachmentSizeExtended: boolean;
};

export type ScheduledEmailOpenerListResponseModel = Override<
    PaginationResponseModel<ScheduledEmailOpenerListItemModel[]>,
    { is_open_count_extra_period_available: boolean }
>;

export type ScheduledEmailOpenerListModel = Override<
    PaginationResponseModel<ScheduledEmailOpenerListItemModel[]>,
    { isOpenCountExtraPeriodAvailable: boolean }
>;

export type ScheduledEmailSearchTemplateContentResponseModel = {
    send_type?: string[];
    date_range?: Date[];
    sent_date?: Date[];
    text_format?: string[];
    send_total_count?: any;
    open_count?: any;
    created_user?: string[];
    send_total_count_gt?: string;
    send_total_count_lt?: string;
    open_count_gt?: string;
    open_count_lt?: string;
    modified_user?: string[];
    sender_id?: string[];
    status?: string[];
    attachments?: string;
    text?: string;
    subject?: string;
};

export type ScheduledEmailSearchModel = {
    attachments: [],
    created_time: string;
    date_to_send: Date;
    id: string;
    modified_time: string;
    open_count: number;
    open_ratio: string
    send_total_count: number
    send_type: string
    sender: string
    sender__name: string
    sent_date?: string
    status: string;
    subject: string;
    text: string;
    text_format: string;
};

export type ScheduledEmailSearchResponseModel = {
    count: number;
    next: number;
    previous: number;
    results: ScheduledEmailSearchModel[];
}

export type ScheduledMailSearchType = {
    send_type?: string;
    status?: string;
    text_format?: string;
    page?: number;
    page_size?: number;
}

export type ScheduledEmailSearchTemplateContentRequestModel =
    ScheduledEmailSearchTemplateContentResponseModel;

export type ScheduledEmailContactPreferenceSearchTemplateContentResponseModel =
    {
        searchtype: "job" | "personnel" | "other";
        wants_location_hokkaido_japan?: boolean;
        wants_location_touhoku_japan?: boolean;
        wants_location_kanto_japan?: boolean;
        wants_location_chubu_japan?: boolean;
        wants_location_toukai_japan?: boolean;
        wants_location_kansai_japan?: boolean;
        wants_location_shikoku_japan?: boolean;
        wants_location_chugoku_japan?: boolean;
        wants_location_kyushu_japan?: boolean;
        wants_location_other_japan?: boolean;
        jobtype?: string;
        jobtype_dev?: any;
        jobskill_dev_youken?: boolean;
        jobskill_dev_kihon?: boolean;
        jobskill_dev_syousai?: boolean;
        jobskill_dev_seizou?: boolean;
        jobskill_dev_test?: boolean;
        jobskill_dev_hosyu?: boolean;
        jobskill_dev_beginner?: boolean;
        jobtype_infra?: any;
        jobskill_infra_youken?: boolean;
        jobskill_infra_kihon?: boolean;
        jobskill_infra_syousai?: boolean;
        jobskill_infra_kouchiku?: boolean;
        jobskill_infra_test?: boolean;
        jobskill_infra_hosyu?: boolean;
        jobskill_infra_kanshi?: boolean;
        jobskill_infra_beginner?: boolean;
        jobtype_other?: any;
        personneltype_dev?: boolean;
        personneltype_infra?: boolean;
        personneltype_other?: boolean;
        job_koyou?: string;
        category_inequality?: string;
        contact__tags__suffix?: string;
        contact__organization__category_prospective?: boolean;
        contact__organization__category_approached?: boolean;
        contact__organization__category_exchanged?: boolean;
        contact__organization__category_client?: boolean;
        contact__organization__organization_country_jp?: boolean;
        contact__organization__organization_country_kr?: boolean;
        contact__organization__organization_country_cn?: boolean;
        contact__organization__organization_country_other?: boolean;
        contact__organization__contract?: string;
        contact__staff?: string;
        contact__category?: string;
        contact__tags?: string[];
        job_syouryu?: number;
        personnel_syouryu?: number;
        personneltype_dev_designer?: boolean;
        personneltype_dev_front?: boolean;
        personneltype_dev_server?: boolean;
        personneltype_dev_pm?: boolean;
        personneltype_dev_other?: boolean;
        personnelskill_dev_youken?: boolean;
        personnelskill_dev_kihon?: boolean;
        personnelskill_dev_syousai?: boolean;
        personnelskill_dev_seizou?: boolean;
        personnelskill_dev_test?: boolean;
        personnelskill_dev_hosyu?: boolean;
        personnelskill_dev_beginner?: boolean;
        personneltype_infra_server?: boolean;
        personneltype_infra_network?: boolean;
        personneltype_infra_security?: boolean;
        personneltype_infra_database?: boolean;
        personneltype_infra_sys?: boolean;
        personneltype_infra_other?: boolean;
        personnelskill_infra_youken?: boolean;
        personnelskill_infra_kihon?: boolean;
        personnelskill_infra_syousai?: boolean;
        personnelskill_infra_kouchiku?: boolean;
        personnelskill_infra_test?: boolean;
        personnelskill_infra_hosyu?: boolean;
        personnelskill_infra_kanshi?: boolean;
        personnelskill_infra_beginner?: boolean;
        personneltype_other_eigyo?: boolean;
        personneltype_other_kichi?: boolean;
        personneltype_other_support?: boolean;
        personneltype_other_other?: boolean;
    };

export type ScheduledEmailContactPreferenceSearchTemplateContentRequestModel =
    ScheduledEmailContactPreferenceSearchTemplateContentResponseModel;

export type ScheduledEmailAttachmentModel = {
    id: string;
    gcpLink: string;
    name: string;
    size: number;
    status: number;
};

export type ScheduledEmailAttachmentDownloadModel = {
    attachments: ScheduledEmailAttachmentModel[];
    expiredDate: string;
    subject: string;
};

export type ScheduledEmailAttachmentsDownloadRequestModel = {
    attachment_ids: string[];
};

export type ScheduledEmailAttachmentAuthorizeFromModel = {
    password: string;
}
