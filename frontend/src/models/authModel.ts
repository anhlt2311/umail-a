import { DisplaySettingAPIModel } from "./displaySetting";

export type OrganizationsAuthorizedActionsModel = {
    create: boolean;
    update: boolean;
    delete: boolean;
    column_setting: boolean;
    csv_upload: boolean;
    csv_download: boolean;
    blacklist: boolean;
    search_template: boolean;
};

export type ContactsAuthorizedActionsModel = {
    create: boolean;
    update: boolean;
    delete: boolean;
    column_setting: boolean;
    csv_upload: boolean;
    csv_download: boolean;
    search_template: boolean;
};

export type ScheduledEmailsAuthorizedActionsModel = {
    create: boolean;
    update: boolean;
    copy: boolean;
    delete: boolean;
    column_setting: boolean;
    select_sender: boolean;
    change_to_draft: boolean;
};

export type SharedEmailsAuthorizedActionsModel = {
    delete: boolean;
    select_staff: boolean;
    select_all_staff: boolean;
};

export type UsersAuthorizedActionsModel = {
    _all: boolean;
};

export type MyCompanyAuthorizedActionsModel = {
    _all: boolean;
};

export type TagsAuthorizedActionsModel = {
    _all: boolean;
};

export type DisplaySettingsAuthorizedActionsModel = {
    _all: boolean;
};

export type ColumnSettingAuthorizedActionsModel = {
    organization: boolean;
    contact: boolean;
    scheduled_mail: boolean;
};

export type EditableRolesAuthorizedActionsModel = string[];

export type SharedEmailSettingsAuthorizedActionsModel = {
    _all: boolean;
};

export type ScheduledEmailSettingsAuthorizedActionsModel = {
    _all: boolean;
};

export type AddonAuthorizedActionsModel = {
    _all: boolean;
    purchase: boolean;
};

export type NotificationRulesAuthorizedActionsModel = {
    _all: boolean;
};

export type CommentTemplateAuthorizedActionsModel = {
    read: boolean;
    create: boolean;
    update: boolean;
    delete: boolean;
    insert: boolean;
};

export type CommentAuthorizedActionsModel = {
    read: boolean;
    create: boolean;
    update: boolean;
    delete: boolean;
    pin: boolean;
};

export type AuthorUserCommentAuthorizedActionsModel = {
    read: boolean;
    update: boolean;
    pin: boolean;
    delete: boolean;
};

export type BillingAuthorizedActionsModel = {
    _all: boolean;
};

export type PurchaseHistoryAuthorizedActionsModel = {
    _all: boolean;
};

export type PlanAuthorizedActionsModel = {
    _all: boolean;
    purchase: boolean;
    user_add: boolean;
    user_delete: boolean;
};

export type AccountAuthorizedActionsModel = {
    delete: boolean;
};

export type AuthorizedActionsModel = {
    organizations: OrganizationsAuthorizedActionsModel;
    contacts: ContactsAuthorizedActionsModel;
    scheduled_mails: ScheduledEmailsAuthorizedActionsModel;
    shared_emails: SharedEmailsAuthorizedActionsModel;
    users: UsersAuthorizedActionsModel;
    my_company: MyCompanyAuthorizedActionsModel;
    tags: TagsAuthorizedActionsModel;
    display_settings: DisplaySettingsAuthorizedActionsModel;
    column_setting: ColumnSettingAuthorizedActionsModel;
    _editable_roles: EditableRolesAuthorizedActionsModel;
    shared_email_settings: SharedEmailSettingsAuthorizedActionsModel;
    scheduled_email_settings: ScheduledEmailSettingsAuthorizedActionsModel;
    addon: AddonAuthorizedActionsModel;
    notification_rules: NotificationRulesAuthorizedActionsModel;
    comment_template: CommentTemplateAuthorizedActionsModel;
    comment: CommentAuthorizedActionsModel;
    another_user_comment: AuthorUserCommentAuthorizedActionsModel;
    billing: BillingAuthorizedActionsModel;
    purchase_history: PurchaseHistoryAuthorizedActionsModel;
    plan: PlanAuthorizedActionsModel;
    account: AccountAuthorizedActionsModel;
};

export type AuthorizedActionsResponseModel = {
    authorized_action: AuthorizedActionsModel;
};

export type UserDisplaySettingModel = {
    content_hash: DisplaySettingAPIModel;
};

export type RoleModel =
    | "master"
    | "admin"
    | "manager"
    | "leader"
    | "member"
    | "guest";

export type LoginAPIResponseModel = {
    token: string;
    user_id: string;
    display_name: string;
    is_user_admin: boolean;
    avatar: string;
    role: RoleModel;
    authorized_actions: AuthorizedActionsModel;
    intercom_user_hash: string;
    user_display_setting: UserDisplaySettingModel;
};

export type LoginResponseModel = {
    token: string;
    userId: string;
    displayName: string;
    isUserAdmin: boolean;
    avatar: string;
    role: RoleModel;
    authorizedActions: AuthorizedActionsModel;
    intercomUserHash: string;
    userDisplaySetting: UserDisplaySettingModel;
};

export type LoginResponseCheckModel = {
    token?: any;
    userId?: any;
    displayName?: any;
    isUserAdmin?: any;
    avatar?: any;
    role?: any;
    authorizedActions?: any;
    intercomUserHash?: any;
    userDisplaySetting?: any;
};

export type LoginDispatchModel = {
    token: string;
    userId: string;
    displayName: string;
    isUserAdmin: boolean;
    avatar: string;
    role: RoleModel;
    authorizedActions: AuthorizedActionsModel;
    intercomUserHash: string;
};

export type LoginFormModel = {
    username: string;
    password: string;
};

export type PasswordResetRequestFormModel = {
    email: string;
};

export type PasswordResetResponseModel = {
    result: string;
};

export type PasswordResetFormModel = {
    password: string;
    password_confirm: string;
};

export type LoginErrorReduxModel = {
    threeLogin?: number;
    error?: any;
};

export type UpdateAvatarPayloadModel = {
  avatar: string;
}

export type UpdateAuthorizedActionsPayloadModel = {
  authorizedActions: AuthorizedActionsModel;
};
