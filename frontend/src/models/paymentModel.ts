export type PaymentCardDataModel = {
    address_city?: string;
    address_line1?: string;
    address_line2?: string;
    address_state?: string;
    address_zip?: string;
    address_zip_check: string;
    brand: string;
    country?: string;
    created: number;
    customer?: string;
    cvc_check: string;
    exp_month: number;
    exp_year: number;
    fingerprint: string;
    id: string;
    last4: string;
    livemode: boolean;
    metadata: any;
    name?: string;
    object: string;
    isMainCard: boolean;
};

export type PaymentTokenGenerateModel = {
    card: PaymentCardDataModel;
    created: number;
    id: string;
    livemode: boolean;
    object: string;
    used: boolean;
};

export type PaymentFetchModel = {
    count: number;
    data: PaymentCardDataModel[];
    object: string;
    has_more: boolean;
    url: string;
};

export type PaymentCreateModel = {
    token: string;
    oldCardId: string;
    cardOrder: number;
};

export type PaymentDeleteModel = {
    cardId: string;
};

export type PaymentUpdateModel = {
    cardId: string;
    defaultCardId: string;
};
