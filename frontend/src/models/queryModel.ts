import { AxiosError } from "axios";
import { QueryKey, UseQueryOptions, UseQueryResult } from "react-query";
import { ErrorDetailModel } from "./responseModel";

export type CustomUseQueryOptions<
    TQueryFnData = unknown,
    TData = TQueryFnData,
    TDeps = QueryKey,
    TError = ErrorDetailModel
> = {
    deps?: TDeps;
    options?: UseQueryOptions<TQueryFnData, AxiosError<TError>, TData>;
};

export type CustomUseQueryResult<T, TError = ErrorDetailModel> = UseQueryResult<
    T,
    AxiosError<TError>
>;
