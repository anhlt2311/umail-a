import {
    BoardAssigneeModel,
    BoardChecklistModel,
    BoardCommentModel,
    BoardDynamicPropertyModel,
    BoardPeriodModel,
    BoardPriorityModel,
    BoardScheduledEmailHistoryModel,
    BoardScheduledEmailTemplateModel,
} from "./boardCommonModel";

export type ProjectBoardWorkingHoursCardModel = {
    start: string;
    end: string;
};

export type ProjectBoardSettleWidthCardModel = {
    start: number;
    end: number;
};

export type ProjectBoardListCardModel = {
    id: string;
    order: number;
    assignees: BoardAssigneeModel[];
    detail: string;
    isImmediate: boolean;
    place: string;
    workingHours: ProjectBoardWorkingHoursCardModel;
    peopleAmount: number;
    price: number;
    settleWidth: ProjectBoardSettleWidthCardModel;
    settleMethod: string;
    settleIncrement: number;
    paymentDays: number;
    interviewAmount: number;
    distribution: string;
    projectAttachment: string;
    checklist: BoardChecklistModel[];
    comments: BoardCommentModel[];
    isArchived: boolean;
};

export type ProjectBoardNewCardFormModel = {
    list_id: string;
    detail: string;
    
};

export type ProjectBoardCardCreateResponseModel = {
    id: string;
};

export type ProjectBoardPeriodModel = {
    datetime: string;
    immediate: boolean;
    longTerm: boolean;
    remarks: string;
};

export type ProjectBoardPlaceModel = {
    id: string;
    remote: boolean;
    remarks: string;
};

export type ProjectBoardWorkingHoursModel = {
    start: string;
    end: string;
    remarks: string;
};

export type ProjectBoardPeopleModel = {
    amount: number;
    isMultiple: boolean;
    isSetProposalWelcome: boolean;
    remarks: string;
};

export type ProjectBoardSkillsModel = {
    required: string[];
    welcomed: string[];
};

export type ProjectBoardPriceModel = {
    amount: number;
    isSkillAssessment: boolean;
    remarks: string;
};

export type ProjectBoardSettleWidthModel = {
    start: number;
    end: number;
    isFixed: boolean;
    remarks: string;
};

export type ProjectBoardSettleMethodModel = {
    method: string;
    remarks: string;
};

export type ProjectBoardSettleIncrementModel = {
    increment: number;
    remarks: string;
};

export type ProjectBoardPaymentModel = {
    day: number;
    remarks: string;
};

export type ProjectBoardInterviewModel = {
    amount: number;
    isJoin: boolean;
    remarks: string;
};

export type ProjectBoardDistributionModel = {
    to: string;
    remarks: string;
};

export type ProjectBoardContractModel = {
    id: string;
    higherOrganization: string;
    higherContact: string;
    lowerOrganization: string;
    lowerContact: string;
};

export type ProjectBoardDetailModel = {
    id?: string;
    order?: number;
    listId?: string;
    assignees?: string[];
    priority?: BoardPriorityModel;
    period?: BoardPeriodModel;
    detail?: string;
    projectPeriod?: ProjectBoardPeriodModel;
    place?: ProjectBoardPlaceModel;
    workingHours?: ProjectBoardWorkingHoursModel;
    people?: ProjectBoardPeopleModel;
    description?: string;
    skills?: ProjectBoardSkillsModel;
    price?: ProjectBoardPriceModel;
    settleWidth?: ProjectBoardSettleWidthModel;
    settleMethod?: ProjectBoardSettleMethodModel;
    settleIncrement?: ProjectBoardSettleIncrementModel;
    payment?: ProjectBoardPaymentModel;
    interview?: ProjectBoardInterviewModel;
    distribution?: ProjectBoardDistributionModel;
    dynamicRow?: BoardDynamicPropertyModel[];
    projectAttachment?: string;
    contracts?: ProjectBoardContractModel[];
    scheduledEmailTemplate?: BoardScheduledEmailTemplateModel;
    scheduledEmailHistory?: BoardScheduledEmailHistoryModel[];
    checklist?: BoardChecklistModel[];
    comments?: BoardCommentModel[];
    isArchived?: boolean;
};

export type ProjectBoardFormModel = {
    id?: string;
    order?: number;
    listId?: string;
    assignees?: string[]; // NOTE(joshua-hashimoto): 担当者のidリスト
    priority?: BoardPriorityModel;
    period?: BoardPeriodModel;
    detail?: string;
    projectPeriod?: ProjectBoardPeriodModel;
    place?: ProjectBoardPlaceModel;
    workingHours?: ProjectBoardWorkingHoursModel;
    people?: ProjectBoardPeopleModel;
    description?: string;
    skills?: ProjectBoardSkillsModel;
    price?: ProjectBoardPriceModel;
    settleWidth?: ProjectBoardSettleWidthModel;
    settleMethod?: ProjectBoardSettleMethodModel;
    settleIncrement?: ProjectBoardSettleIncrementModel;
    payment?: ProjectBoardPaymentModel;
    interview?: ProjectBoardInterviewModel;
    distribution?: ProjectBoardDistributionModel;
    dynamicRow?: BoardDynamicPropertyModel[];
    projectAttachment?: string;
    isArchived?: boolean;
};

export type ProjectBoardContractCreateFromModel = {
    personnelId: string;
    projectPeriod: string[]; // NOTE(joshua-hashimoto): Form側はmoment？
    higherOrganization: string;
    higherContact: string;
    lowerOrganization: string;
    lowerContact: string;
};

export type ProjectBoardContractUpdateFormModel = {
    personnelId: string;
    projectPeriod: string[]; // NOTE(joshua-hashimoto): Form側はmoment？
    higherOrganization: string;
    higherContact: string;
    lowerOrganization: string;
    lowerContact: string;
};

export type ProjectBoardTypeAtom = {
  isCreateCheckItem: boolean;
  checkListId: string;
  isUpdateProject: boolean;
  projectId: string;
  dataCreatedProject: {
      isCreated: boolean;
      isDropDownPlus: boolean;
  },
};
