export type PaginationResponseModel<T> = {
    count: number;
    next: string;
    previous: string;
    results: T;
};

export type ErrorDetailModel = {
    detail: string;
    code?: string;
};

export type SuccessDetailResponse = {
    detail: string;
};
