export type PaginationRequestModel = {
    page?: number;
    pageSize?: number;
    value?: string;
};
