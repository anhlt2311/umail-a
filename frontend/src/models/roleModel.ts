import { RoleModel } from "./authModel";

export type RoleNotModel =
    | "not:master"
    | "not:admin"
    | "not:manager"
    | "not:leader"
    | "not:member"
    | "not:guest";

export type UserRoleModel = {
    value: RoleModel | RoleNotModel;
    title: string;
    color: string;
};
