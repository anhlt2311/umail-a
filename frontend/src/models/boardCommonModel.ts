import { Moment } from "moment";
import {
    ScheduledEmailSendStatusModel,
    ScheduledEmailTextFormatValueModel,
} from "./scheduledEmailModel";

export type BoardChecklistItemModel = {
    id?: string;
    content: string;
    isFinished: boolean;
};

export type BoardChecklistModel = {
    id: string;
    title: string;
    items: BoardChecklistItemModel[];
    showFinished: boolean;
    isFinished: boolean;
};

export type BoardCommentModel = {
    content: string,
    createdTime: string,
    createdUser: string,
    createdUser_Name: string,
    deletedAt: string
    hasSubcomment: boolean,
    id: string,
    isImportant: boolean
    parentId: string,
    parentContent: string,
    parentCreatedTime: string,
    parentCreatedUserName: string,
    parentModifiedTime: string,
    parentModifiedUserName: string,
    totalSubComment: number,
    modifiedTime: string,
    modifiedUser: string,
};

export type BoardColumnModel = {
    id: string;
    title: string;
};

export type BoardAssigneeModel = {
    id: string;
    name: string;
    avatar?: string;
};

export type BoardPriorityTitleModel = "緊急" | "重大" | "高" | "中" | "低";

export type BoardPriorityColorModel = "#EB5A46" | "#FFAB4A" | "#F2D600" | "#61BD4F" | "#0079BF";

export type BoardPriorityValueModel =
    | "urgent"
    | "important"
    | "high"
    | "medium"
    | "low";

export type BoardPriorityModel = {
    title: BoardPriorityTitleModel;
    value: BoardPriorityValueModel;
    color: BoardPriorityColorModel;
};

export type BoardPeriodModel = {
    start?: Moment|string;
    end?: Moment|string;
    isFinished?: boolean;
};

export type BoardDynamicPropertyModel = {
    id?: string;
    title: string;
    content: string;
};

export type BoardScheduledEmailTemplateTextModel = {
    upper: string;
    lower: string;
};

export type BoardScheduledEmailTemplateModel = {
    subject: string;
    text: BoardScheduledEmailTemplateTextModel;
    sendCopyToSender: boolean;
    sendCopyToShare: boolean;
    condition: any; // NOTE(joshua-hashimoto): 現状、保存されている文字列をオブジェクトに変換して返されているので、型付ができない?
};

export type BoardScheduledEmailHistoryModel = {
    id: string;
    status: ScheduledEmailSendStatusModel;
    sendCompleteDate: string;
    format: ScheduledEmailTextFormatValueModel;
    sendCount: number;
    openCount: number;
    openRatio: number;
};

export type BoardChecklistCreateFormModel = {
    cardId: string;
    title: string;
};

export type BoardChecklistCreateResponseModel = {
    id: string;
};

export type BoardChecklistUpdateFormModel = {
    // id: string;
    title: string;
    // items: BoardChecklistItemUpdateFormModel[];
    showFinished: boolean;
};

export type BoardChecklistItemCreateFormModel = {
    content: string;
};

export type BoardChecklistItemUpdateFormModel = {
    id: string;
    content: string;
    isFinished: boolean;
};

export type BoardCommentCreateFormModel = {
    cardId: string;
    content: string;
    is_important: boolean;
};

export type BoardCommentUpdateFormModel = {
    id: string;
    content: string;
    is_important: boolean;
};

export type BoardDataModel<T> = {
    id: string;
    title: string;
    cards: T[];
};

export type BoardModel<T> = {
    columns: BoardDataModel<T>[];
};

export type CardChangePositionRequestModel = {
    listId: string;
    position: number;
};

export type BoardScheduledEmailTemplateCheckboxDataModel = {
    key: string;
    name: string;
    title: string;
    checked: boolean;
};

export type BoardScheduledEmailTemplateCheckboxModel = {
    selectedJobType?: BoardScheduledEmailTemplateCheckboxDataModel[];
    selectedJobSkill?: BoardScheduledEmailTemplateCheckboxDataModel[];
};
export type CardKanDragDrop = {
    fromPosition: number;
    fromColumnId: string;
};

export type SourceKanDragDrop = {
    toPosition: number;
    toColumnId: string;
};

export const defaultPersonnelBoard = {
    affiliation: "",
    age: null,
    assignees: [],
    checklist: [],
    comments: [],
    firstName: null,
    firstNameInitial: "",
    gender: null,
    id: "",
    isArchived: false,
    lastName: null,
    lastNameInitial: "",
    order: 0,
    parallel: null,
    period: { start: null, end: null, isFinished: false },
    price: null,
    skillSheet: null,
    skills: [],
    trainStation: null,
};

export const defaultProjectBoard = {
  affiliation: "",
  age: null,
  assignees: [],
  checklist: [],
  comments: [],
  firstName: null,
  firstNameInitial: "",
  gender: null,
  id: "",
  isArchived: false,
  lastName: null,
  lastNameInitial: "",
  order: 0,
  parallel: null,
  period: { start: null, end: null, isFinished: false },
  price: null,
  skillSheet: null,
  skills: [],
  trainStation: null,
};