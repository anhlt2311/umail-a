export type CsvDownloadFormModel = {
    downloadRangeIndex: number;
};

export type ContactCsvDownloadType = "table" | "all";

export type CsvDownloadRequestArgs = {
    index: number;
    queryString?: string;
};
