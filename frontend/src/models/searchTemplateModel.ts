export type SearchTemplateResponseContentModel<T> = {
    display_name: string;
    name: string;
    pageSize: number;
    selectedColumnKeys: string[];
    sortKey: string;
    sortOrder: string;
    start: boolean;
    template_name: string;
    values: T;
};

export type SearchTemplateContentModel<T> =
    SearchTemplateResponseContentModel<T>;

export type SearchTemplateResponseModel<T> = {
    templates: SearchTemplateResponseContentModel<T>[];
    total_available_count: number;
};

export type SearchTemplateModel<T> = SearchTemplateResponseModel<T>;
