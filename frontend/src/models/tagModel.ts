export type TagColorModel =
    | "default"
    | "geekblue"
    | "orange"
    | "green"
    | "brown"
    | "purple"
    | "yellow"
    | "volcano"
    | "cyan"
    | "pink"
    | "gray";

export type TagColorSelectModel = {
    value: TagColorModel;
    title:
        | "Default"
        | "Geek Blue"
        | "Orange"
        | "Green"
        | "Brown"
        | "Purple"
        | "Yellow"
        | "Volcano"
        | "Cyan"
        | "Pink"
        | "Gray";
};

export type TagFormModel = {
    id?: string;
    value: string;
    color: TagColorModel;
};

export type TagResponseModel = {
    id: string;
    value: string;
    color: TagColorModel;
    internal_value: string;
    created_time: string;
    created_user: string;
    created_user__name: string;
    modified_time: string;
    modified_user: string;
    modified_user__name: string;
    is_skill?: boolean
};

export type TagDetailModel = {
    id: string;
    value: string;
    color: TagColorModel;
    internal_value: string;
    created_time: string;
    created_user: string;
    modified_time: string;
    modified_user: string;
};

export type TagModel = {
    id: string;
    value: string;
    color: TagColorModel;
    internalValue: string;
    createdTime: string;
    createdUser: string;
    modifiedTime: string;
    modifiedUser: string;
    is_skill?: boolean;
};

export type TagContactSelectModel = {
    id: string;
    value: string;
};

export type TagSkillsSelectModel = {
    id: string;
    value: string;
}

export type TagBulkDeleteModel = {
    source: string[];
};

export type TagPostModel = {
    value: string;
    color: TagColorModel;
    is_skill?: boolean;
};

export type TagContactTableModel = {
    value: string;
    color: TagColorModel;
};

export type TagUpdateParamModel = {
    id: string;
    postData: TagPostModel;
};

export type TagDeleteResponseModel = {
    id: string;
};

export type NewTagFormModel = {
    newTag: string;
    newColor: TagColorModel;
    isSkill: boolean;
};

export type TagSearchFormModel = {
    value: string;
};
