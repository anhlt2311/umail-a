import { Moment } from "moment";
import {
    BoardAssigneeModel,
    BoardChecklistModel,
    BoardCommentModel,
    BoardDataModel,
    BoardDynamicPropertyModel,
    BoardPeriodModel,
    BoardPriorityModel,
    BoardPriorityValueModel,
    BoardScheduledEmailHistoryModel,
    BoardScheduledEmailTemplateModel,
} from "./boardCommonModel";
import { TagColorModel } from "./tagModel";

export type PersonnelBoardGenderModel = "male" | "female";

export type PersonnelBoardAffiliationTitleModel =
    | "弊社プロパー"
    | "弊社フリーランス"
    | "1社先プロパー"
    | "1社先フリーランス"
    | "2社先プロパー"
    | "2社先フリーランス"
    | "3社先以上プロパー"
    | "3社先以上フリーランス";

export type PersonnelBoardAffiliationModel =
    | "proper"
    | "freelance"
    | "one_company_proper"
    | "one_company_freelancer"
    | "two_company_proper"
    | "two_company_freelancer"
    | "three_company_proper"
    | "three_company_freelancer";

export type PersonnelBoardAffiliationSelectModel = {
    title: PersonnelBoardAffiliationTitleModel;
    value: PersonnelBoardAffiliationModel;
};

export type PersonnelBoardParallelTitleModel =
    | "なし"
    | "あり"
    | "1件"
    | "2件"
    | "3件以上";

export type PersonnelBoardParallelModel =
    | "none"
    | "parallel"
    | "parallel_one"
    | "parallel_two"
    | "parallel_three";

export type PersonnelBoardParallelSelectModel = {
    title: PersonnelBoardParallelTitleModel;
    value: PersonnelBoardParallelModel;
};

export type PersonnelBoardSkillModel = {
    id: string;
    name: string;
    color: TagColorModel;
};

export type SkillType = {
    id: string;
    name: string;
    color: string;
    isSkill: boolean;
};

export type skillSheetsType = {
    file: string;
    id: string;
    name: string;
}

export type PersonnelBoardListCardModel = {
    id: string;
    order: number;
    assignees: BoardAssigneeModel[];
    priority: BoardPriorityModel;
    lastName: string;
    firstName: string;
    lastNameInitial: string;
    firstNameInitial: string;
    image: string;
    age: number;
    gender: PersonnelBoardGenderModel;
    trainStation: string;
    affiliation: PersonnelBoardAffiliationModel;
    skills: SkillType[] | string[];
    period: BoardPeriodModel;
    parallel: PersonnelBoardParallelModel;
    price: number;
    skillSheets: skillSheetsType[] | string[];
    checklist: BoardChecklistModel[];
    comments: BoardCommentModel[];
    isArchived: boolean;
    operatePeriod: string;
};



export type PersonnelBoardNewCardFormModel = {
    list_id: string;
    last_name_initial: string;
    first_name_initial:string
};

export type PersonnelBoardCardCreateResponseModel = {
    id: string;
};

export type PersonnelBoardContractModel = {
    id?: string;
    detail: string;
    projectPeriod: string[];
    higherOrganization: string;
    higherContact: string;
    lowerOrganization: string;
    lowerContact: string;
};

export type PersonnelBoardDetailModel = {
    id?: string;
    order?: number;
    listId?: string;
    assignees?: string[]; // NOTE(joshua-hashimoto): 担当者のidリスト
    priority?: BoardPriorityModel;
    period?: BoardPeriodModel;
    lastName?: string;
    firstName?: string;
    lastNameInitial?: string;
    firstNameInitial?: string;
    age?: number;
    birthday?: Moment|string;
    gender?: PersonnelBoardGenderModel;
    trainStation?: string;
    affiliation?: PersonnelBoardAffiliationModel;
    skills?: string[];
    operatePeriod?: Moment|string;
    parallel?: PersonnelBoardParallelModel;
    price?: number;
    isNegotiable?: boolean;
    request?: string;
    dynamicRows?: BoardDynamicPropertyModel[];
    image?: string;
    skillSheet?: string;
    contracts?: PersonnelBoardContractModel[];
    scheduledEmailTemplate?: BoardScheduledEmailTemplateModel;
    scheduledEmailHistory?: BoardScheduledEmailHistoryModel[];
    checklist?: BoardChecklistModel[];
    comments?: BoardCommentModel[];
    isArchived?: boolean;
    createdTime?: string;
    createdUserName?: string;
    modifiedTime?: string;
    modifiedUserName?: string;
};

export type PersonnelBoardCopyCardFormModel = {
    cardId?: string;
    listId?: string;
    lastNameInitial?: string;
    firstNameInitial?: string;
}

export type PersonnelBoardFormModel = {
    id?: string;
    order?: number;
    listId?: string;
    assignees?: string[]; // NOTE(joshua-hashimoto): 担当者のidリスト
    priority?: string;
    period?: BoardPeriodModel;
    lastName?: string;
    firstName?: string;
    lastNameInitial?: string;
    firstNameInitial?: string;
    age?: number;
    birthday?: string | Moment;
    gender?: PersonnelBoardGenderModel;
    trainStation?: string;
    affiliation?: PersonnelBoardAffiliationModel;
    skills?: string[];
    operatePeriod?: string;
    parallel?: string;
    price?: number;
    isNegotiable?: boolean;
    request?: string;
    dynamicRows?: BoardDynamicPropertyModel[];
    image?: string;
    skillSheet?: string;
    projects?: string[];
    isArchived?: boolean;
};

export type PersonnelBoardContractCreateFormModel = {
    cardId: string;
    detail: string;
    projectPeriod: string[]; // NOTE(joshua-hashimoto): Form側はmoment？
    higherOrganization: string;
    higherContact: string;
    lowerOrganization: string;
    lowerContact: string;
};

export type PersonnelBoardContractUpdateFormModel = {
    id?: string;
    cardId: string;
    detail: string;
    projectPeriod: string[]; // NOTE(joshua-hashimoto): Form側はmoment？
    higherOrganization: string;
    higherContact: string;
    lowerOrganization: string;
    lowerContact: string;
    price: string;
};

export type PersonnelBoardTypeAtom = {
    isCreateCheckItem: boolean;
    checkListId: string;
    isUpdatePersonnel: boolean;
    cardId: string;
    dataCreatedPersonnel: {
        isCreated: boolean;
        isDropDownPlus: boolean;
    },
};

export type PersonnelScheduledMailTemplateForm = {
    text_format: string;
    subject: string;
    text: string;
    values_info: string;
    send_copy_to_sender: boolean;
    send_copy_to_share: boolean;

    personneltype_dev_designer: boolean;
    personneltype_dev_front: boolean;
    personneltype_dev_server: boolean;
    personneltype_dev_pm: boolean;
    personneltype_dev_other: boolean;
    personnelskill_dev_youken: boolean;
    personnelskill_dev_kihon: boolean;
    personnelskill_dev_syousai: boolean;
    personnelskill_dev_seizou: boolean;
    personnelskill_dev_test: boolean;
    personnelskill_dev_hosyu: boolean;
    personnelskill_dev_beginner: boolean;

    personneltype_infra_server: boolean;
    personneltype_infra_network: boolean;
    personneltype_infra_security: boolean;
    personneltype_infra_database: boolean;
    personneltype_infra_sys: boolean;
    personneltype_infra_other: boolean;
    jobskill_dev_youken: boolean;
    jobskill_dev_kihon: boolean;
    jobskill_dev_syousai: boolean;
    jobskill_dev_seizou: boolean;
    jobskill_dev_test: boolean;
    jobskill_dev_hosyu: boolean;
    jobskill_infra_kanshi: boolean;
    jobskill_dev_beginner: boolean;

    jobtype_other_eigyo: boolean;
    jobtype_other_kichi: boolean;
    jobtype_other_support: boolean;
    jobtype_other_other: boolean;


    personneltype_dev: boolean;
    personneltype_infra: boolean;
    personneltype_other: boolean;
};

export type PersonnelBoardTrainStationModel = {
    id: number
    name: string
    area: string
    location_key: string
}

export type PersonnelBoardSkillSheetModel = {
  id: string;
  name: string;
  file: string;
};

export type PersonnelBoardCardMenuOptionModel = {
  label: string
  value: string
}