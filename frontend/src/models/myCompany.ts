import { Moment } from "moment";

export type MyCompanyResponseModel = {
    id: string;
    name: string;
    domain_name: string;
    address: string;
    building?: string;
    capital_man_yen: number;
    establishment_date: string;
    establishment_year: number;
    has_p_mark_or_isms: boolean;
    has_invoice_system: boolean;
    has_haken: boolean;
    has_distribution: boolean;
    capital_man_yen_required_for_transactions?: number;
    p_mark_or_isms: boolean;
    invoice_system: boolean;
    haken: boolean;
    exceptional_organization_names: string[];
    exceptional_organizations: string[];
    modified_time: string;
    modified_user__name: string;
    settlement_month?: number;
};

export type MyCompanyModel = {
    id: string;
    name: string;
    domain_name: string;
    address: string;
    building?: string;
    capital_man_yen: number;
    establishment_date?: Moment;
    establishment_year?: number;
    has_p_mark_or_isms: boolean;
    has_invoice_system: boolean;
    has_haken: boolean;
    has_distribution: boolean;
    capital_man_yen_required_for_transactions?: number;
    p_mark_or_isms: boolean;
    invoice_system: boolean;
    haken: boolean;
    exceptional_organization_names: string[];
    exceptional_organizations: string[];
    modified_time: string;
    modified_user: string;
    settlement_month?: number;
};
