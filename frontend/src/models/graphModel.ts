export type GraphDataModel<T = number[]> = {
    labels: string[];
    values: T;
};
