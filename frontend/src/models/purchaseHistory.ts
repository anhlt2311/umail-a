export type PurchaseHistoryResponseModel = {
    period_start: string;
    period_end: string;
    plan_name: string;
    option_names: string[];
    card_last4: string;
    created_time: string;
    price: number;
    id: string;
    method_name: string;
};

export type PurchaseHistoryModel = {
    periodStart: string;
    periodEnd: string;
    planName: string;
    optionNames: string[];
    cardLast4: string;
    createdTime: string;
    price: number;
    id: string;
    methodName: string;
};
