export type AddonPurchaseItemResponseModel = {
    id: number;
    addon_master_id: number;
    next_payment_date?: string;
};
export type AddonPurchaseItemModel = {
    id: number;
    addonMasterId: number;
    nextPaymentDate?: string;
};

export type AddonPostModel = {
    addonMasterId: number;
};

export type AddonPostRequestModel = {
    addon_master_id: number;
};

export type AddonMasterResponseModel = {
    id: number;
    description: string;
    expiration_time?: string;
    is_dashboard: boolean;
    is_my_company_setting: boolean;
    is_organizations: boolean;
    is_recommended: boolean;
    is_scheduled_mails: boolean;
    is_shared_mails: boolean;
    limit: number;
    parents: string[];
    price: number;
    targets: string[];
    title: string;
    help_url: string;
};

export type AddonMasterModel = {
    id: number;
    description: string;
    expirationTime?: string;
    isDashboard: boolean;
    isMyCompanySetting: boolean;
    isOrganizations: boolean;
    isRecommended: boolean;
    isScheduledMails: boolean;
    isSharedMails: boolean;
    limit: number;
    parents: string[];
    price: number;
    targets: string[];
    title: string;
    helpUrl: string;
};
