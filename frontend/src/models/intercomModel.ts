export type IntercomModel = {
    userId: string;
    displayName: string;
    intercomUserHash: string;
};
