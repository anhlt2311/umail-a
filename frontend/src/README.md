# About
* This is a directory of sources of Single-page Application.
# Architecture
* This source architecture is based on React + Redux.
# Directories
* actions: A directory which contains redux actions.
* components: A directory which contains react components
* domain: A directory which contains domain-specific modules, like clients to backend APIs.
* reducers: A directory which contains redux reducers.
# Files
* constants.js: A file that defines constants.
* index.jsx: An entry point of this SPA.
* store.js: A file that defines a redux store.
