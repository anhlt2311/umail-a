import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import {
    ScheduledEmailAttachmentDownloadModel,
    ScheduledEmailAttachmentsDownloadRequestModel,
    ScheduledEmailAttachmentSizeExtendedResponseModel,
    ScheduledEmailOpenerListResponseModel,
    ScheduledEmailSearchResponseModel,
    ScheduledMailSearchType,
} from "~/models/scheduledEmailModel";

const scheduledEmail = (client: AxiosInstance) => {
    return {
        fetchOpenerList(
            scheduledEmailId: string
        ): Promise<AxiosResponse<ScheduledEmailOpenerListResponseModel>> {
            const url =
                Endpoint.scheduledEmailOpenerList + "/" + scheduledEmailId;
            return client.get(url);
        },
        fetchListScheduledMailSearch(
            params?: ScheduledMailSearchType
        ): Promise<AxiosResponse<ScheduledEmailSearchResponseModel>>{
            const url =
                Endpoint.scheduledEmails + `?send_type=${params?.send_type}
                &status=${params?.status}&text_format=${params?.text_format}&page=${params?.page}&page_size=${params?.page_size}`;
            return client.get(url)
        },
        scheduledEmailAttachmentSizeExtended(): Promise<
            AxiosResponse<ScheduledEmailAttachmentSizeExtendedResponseModel>
        > {
            return client.get(Endpoint.scheduledEmailAttachmentSizeLimit);
        },
        authorizeDownload(
            id: string,
            password: string
        ): Promise<AxiosResponse<ScheduledEmailAttachmentDownloadModel>> {
            const url = `${Endpoint.scheduledEmailFiles}/${id}?password=${password}`;
            return client.get(url);
        },
        downloadFiles: (postData: ScheduledEmailAttachmentsDownloadRequestModel) => {
            const url = Endpoint.scheduledEmailFilesDownload;
            return client.post(url, postData)
        }
    };
};

export default scheduledEmail;
