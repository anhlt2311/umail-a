import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import {
    PlanRegisterPostAPIModel,
    PlanSummaryResponseModel,
    PlanUserDataAPIModel,
} from "~/models/planModel";

const plan = (client: AxiosInstance) => {
    return {
        fetchUserPlanInfo(): Promise<AxiosResponse<PlanUserDataAPIModel>> {
            const url = Endpoint.plan;
            return client.get(url);
        },
        registerPlan(
            postData: PlanRegisterPostAPIModel
        ): Promise<AxiosResponse<null>> {
            const url = Endpoint.plan;
            return client.post(url, postData);
        },
        addAccountLimit(): Promise<AxiosResponse<null>> {
            const url = Endpoint.addUserCount;
            return client.post(url);
        },
        removeAccountLimit(): Promise<AxiosResponse<null>> {
            const url = Endpoint.deleteUserCount;
            return client.post(url);
        },
        fetchPlanSummary(): Promise<AxiosResponse<PlanSummaryResponseModel>> {
            const url = Endpoint.planSummary;
            return client.get(url);
        },
    };
};

export default plan;
