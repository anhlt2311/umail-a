import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import { ThumbnailDeleteRequestModel } from "~/models/thumbnailModel";

const thumbnail = (client: AxiosInstance) => {
    return {
        delete(
            postData: ThumbnailDeleteRequestModel
        ): Promise<AxiosResponse<any>> {
            const url = Endpoint.thumbnail;
            return client.delete(url, {
                data: postData,
            });
        },
    };
};

export default thumbnail;
