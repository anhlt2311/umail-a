import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import {
    SharedEmailRequestModel,
    SharedEmailResponseModel,
} from "~/models/sharedEmailModel";

const sharedEmail = (client: AxiosInstance) => {
    return {
        fetchDetail(
            id: string
        ): Promise<AxiosResponse<SharedEmailResponseModel>> {
            const url = Endpoint.sharedEmails + "/" + id;
            return client.get(url);
        },
        forward(
            id: string,
            postData: SharedEmailRequestModel
        ): Promise<AxiosResponse<any>> {
            const url = Endpoint.sharedEmails + "/" + id + "/forward";
            return client.post(url, postData);
        },
        delete(id: string): Promise<AxiosResponse<any>> {
            const url = Endpoint.sharedEmails + "/" + id;
            return client.delete(url);
        },
    };
};

export default sharedEmail;
