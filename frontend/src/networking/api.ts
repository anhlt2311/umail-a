import axios from "axios";
import { Endpoint } from "~/domain/api";
import account from "./account";
import addon from "./addon";
import auth from "./auth";
import csvDownload from "./csvDownload";
import myProfile from "./myProfile";
import personnelBoard from "./personnelBoard";
import plan from "./plan";
import projectBoard from "./projectBoard";
import purchaseHistory from "./purchaseHistory";
import scheduledEmail from "./scheduledEmail";
import scheduledEmailSetting from "./scheduledEmailSetting";
import sharedEmail from "./sharedEmail";
import tag from "./tag";
import thumbnail from "./thumbnail";
import user from "./user";
import tenant from "./tenant";

const baseURL = Endpoint.getBaseUrl();

const client = axios.create({
    baseURL: `${baseURL}/`,
    responseType: "json",
    headers: {
        "Content-Type": "application/json",
    },
    withCredentials: true,
});

const accountAPI = account(client);
const addonAPI = addon(client);
const authAPI = auth(client);
const csvDownloadAPI = csvDownload(client);
const myProfileAPI = myProfile(client);
const personnelBoardAPI = personnelBoard(client);
const planAPI = plan(client);
const projectBoardAPI = projectBoard(client);
const purchaseHistoryAPI = purchaseHistory(client);
const scheduledEmailAPI = scheduledEmail(client);
const scheduledEmailSettingAPI = scheduledEmailSetting(client);
const sharedEmailAPI = sharedEmail(client);
const tagAPI = tag(client);
const tenantAPI = tenant(client);
const thumbnailAPI = thumbnail(client);
const userAPI = user(client);

export {
    accountAPI,
    addonAPI,
    authAPI,
    csvDownloadAPI,
    myProfileAPI,
    personnelBoardAPI,
    planAPI,
    projectBoardAPI,
    purchaseHistoryAPI,
    scheduledEmailAPI,
    scheduledEmailSettingAPI,
    sharedEmailAPI,
    tagAPI,
    tenantAPI,
    thumbnailAPI,
    userAPI,
};
export default client;
