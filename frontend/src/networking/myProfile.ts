import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import { convertMyProfileFormModelToMyProfileRequestModel } from "~/hooks/useMyProfile";
import {
    MyProfileFormModel,
    MyProfileResponseModel,
} from "~/models/myProfileModel";
import { convertObjectToFormData } from "~/utils/utils";

export const myProfile = (client: AxiosInstance) => {
    return {
        fetchMyProfile(): Promise<AxiosResponse<MyProfileResponseModel>> {
            const url = Endpoint.myProfilePath;
            return client.get(url);
        },
        update(
            data: MyProfileFormModel
        ): Promise<AxiosResponse<MyProfileResponseModel>> {
            const url = Endpoint.myProfilePath;
            const obj = convertMyProfileFormModelToMyProfileRequestModel(data);
            const postData = convertObjectToFormData(obj);
            return client.patch(url, postData);
        },
    };
};

export default myProfile;
