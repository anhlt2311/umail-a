import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import {
    AddonMasterResponseModel,
    AddonPostModel,
    AddonPostRequestModel,
    AddonPurchaseItemResponseModel,
} from "~/models/addonModel";

const addon = (client: AxiosInstance) => {
    return {
        fetchAddonMaster(): Promise<AxiosResponse<AddonMasterResponseModel[]>> {
            const url = Endpoint.addonMaster;
            return client.get(url);
        },
        fetchPurchasedAddons(): Promise<AxiosResponse<AddonPurchaseItemResponseModel[]>> {
            const url = Endpoint.purchasedAddons;
            return client.get(url);
        },
        purchaseAddon(postData: AddonPostModel): Promise<AxiosResponse<any>> {
            const url = Endpoint.purchaseAddon;
            const convertedPostData: AddonPostRequestModel = {
                addon_master_id: postData.addonMasterId,
            };
            return client.post(url, convertedPostData);
        },
        revokeAddon(addonModelId: number): Promise<AxiosResponse<any>> {
            const url = Endpoint.revokePurchasedAddon + "/" + addonModelId;
            return client.delete(url);
        },
    };
};

export default addon;
