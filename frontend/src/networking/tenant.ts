import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import { PaymentFetchModel } from "~/models/paymentModel";
import {
    TenantCurrentStepModel,
    TenantMyCompanyRequestModel,
    TenantMyCompanyResponseModel,
    TenantMyProfileRequestDataModel,
    TenantMyProfileRequestModel,
    TenantPaymentRequestModel,
    TenantRecaptchaResponseModel,
    TenantRegisterBasicInfoModel,
} from "~/models/tenantModel";
import { convertObjectToFormData } from "~/utils/utils";

const tenant = (client: AxiosInstance) => {
    return {
        async fetchRecaptcha(): Promise<
            AxiosResponse<TenantRecaptchaResponseModel>
        > {
            return await client.get(Endpoint.tenantSitekey);
        },
        async registerBasicInfo(
            postData: TenantRegisterBasicInfoModel
        ): Promise<AxiosResponse<TenantCurrentStepModel>> {
            return await client.post(Endpoint.tenantRegister, postData);
        },
        async fetchMyCompany(
            authToken: string
        ): Promise<AxiosResponse<TenantMyCompanyResponseModel>> {
            return await client.get(Endpoint.tenantMyCompany, {
                params: {
                    auth_token: authToken,
                },
            });
        },
        async registerMyCompany(
            postData: TenantMyCompanyRequestModel
        ): Promise<AxiosResponse<any>> {
            return await client.post(Endpoint.tenantMyCompany, {
                ...postData,
                auth_token: postData.authToken,
            });
        },
        fetchMyProfile(authToken: string): Promise<AxiosResponse<any>> {
            const url = Endpoint.tenantMyProfile;
            return client.get(url, {
                params: {
                    auth_token: authToken,
                },
            });
        },
        registerMyProfile(
            data: TenantMyProfileRequestModel
        ): Promise<AxiosResponse<any>> {
            const url = Endpoint.tenantMyProfile;
            let postData:
                | (TenantMyProfileRequestDataModel & { auth_token: string })
                | FormData = {
                ...data,
                auth_token: data.authToken,
            };
            if (postData.avatar) {
                postData = convertObjectToFormData(postData);
            }
            return client.post(url, postData);
        },
        async fetchPayment(
            authToken: string
        ): Promise<AxiosResponse<PaymentFetchModel>> {
            return await client.get(Endpoint.tenantPayment, {
                params: {
                    auth_token: authToken,
                },
            });
        },
        async registerPayment(
            postData: TenantPaymentRequestModel
        ): Promise<AxiosResponse<PaymentFetchModel>> {
            return await client.post(Endpoint.tenantPayment, {
                ...postData,
                auth_token: postData.authToken,
            });
        },
        async fetchCurrentStep(
            authToken: string | null
        ): Promise<AxiosResponse<TenantCurrentStepModel>> {
            return await client.get(Endpoint.tenantCurrentStep, {
                params: {
                    auth_token: authToken,
                },
            });
        },
        async finishRegister(postData: {
            authToken: string;
        }): Promise<AxiosResponse<TenantCurrentStepModel>> {
            return await client.post(Endpoint.tenantFinish, {
                auth_token: postData.authToken,
            });
        },
    };
};

export default tenant;
