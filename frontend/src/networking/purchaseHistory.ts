import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import { PurchaseHistoryResponseModel } from "~/models/purchaseHistory";
import { PaginationRequestModel } from "~/models/requestModel";
import { PaginationResponseModel } from "~/models/responseModel";

const purchaseHistory = (client: AxiosInstance) => {
    return {
        fetchPurchaseHistory(
            query: PaginationRequestModel
        ): Promise<
            AxiosResponse<
                PaginationResponseModel<PurchaseHistoryResponseModel[]>
            >
        > {
            let url =
                Endpoint.purchaseHistory + "?" + `page_size=${query.pageSize}`;
            if (query.page) {
                url += "&page=" + query.page;
            }
            return client.get(url);
        },
    };
};

export default purchaseHistory;
