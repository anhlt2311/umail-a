import { AxiosInstance, AxiosResponse } from "axios";
import { Endpoint } from "~/domain/api";
import {
    convertUserRegisterFormModelToUserRegisterRequestDataModel,
    convertUserFormModelToUserUpdateRequestDataModel,
} from "~/hooks/useUser";
import { PaginationResponseModel } from "~/models/responseModel";
import {
    UserListItemAPIModel,
    UserRegisterRequestDataModel,
    UserRegisterRequestModel,
    UserUpdateRequestDataModel,
    UserUpdateRequestModel,
} from "~/models/userModel";
import { convertObjectToFormData, createQueryString } from "~/utils/utils";

const user = (client: AxiosInstance) => {
    return {
        fetchUsers(
            params?: Object
        ): Promise<
            AxiosResponse<PaginationResponseModel<UserListItemAPIModel[]>>
        > {
            let url = Endpoint.users;
            if (params) {
                const queryParams = createQueryString(params);
                url += "?" + queryParams;
            }
            return client.get(url);
        },
        register({
            registerToken,
            data,
        }: UserRegisterRequestModel): Promise<AxiosResponse<any>> {
            const url = Endpoint.usersRegister + "/" + registerToken;
            let postData: FormData | UserRegisterRequestDataModel =
                convertUserRegisterFormModelToUserRegisterRequestDataModel(
                    data
                );
            if (postData.avatar) {
                postData = convertObjectToFormData(postData);
            }
            return client.patch(url, postData);
        },
        update({
            userId,
            data,
        }: UserUpdateRequestModel): Promise<AxiosResponse<any>> {
            const url = Endpoint.users + "/" + userId;
            let postData: FormData | UserUpdateRequestDataModel =
                convertUserFormModelToUserUpdateRequestDataModel(data);
            if (postData.avatar) {
                postData = convertObjectToFormData(postData);
            }
            return client.patch(url, postData);
        },
    };
};

export default user;
