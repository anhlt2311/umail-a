import "@testing-library/jest-dom/extend-expect";
import { QueryCache, setLogger } from "react-query";
import { setupServer } from "msw/node";
import {
    mockHistoryGoBack,
    mockHistoryPush,
    testQueryClient,
} from "~/test/utils";
import { mockHandlers } from "./mock";
import { Modal } from "antd";

// NOTE(joshua-hashimoto): Ant Designの画面サイズなどに関連する設定
Object.defineProperty(window, "matchMedia", {
    writable: true,
    value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // deprecated
        removeListener: jest.fn(), // deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
    })),
});

Object.defineProperty(navigator, "clipboard", {
    value: {
        writeText: () => {},
    },
});

// NOTE(joshua-hashimoto): Intercomのtypeがテスト時は認識されないのでエラーを回避するために空文字列を設定
global.window.Intercom = () => {};

// NOTE(joshua-hashimoto): テスト時のconsoleの挙動を設定
global.console = {
    log: console.log,
    error: jest.fn(),
    warn: console.warn,
    info: console.info,
    debug: console.debug,
};

// NOTE(joshua-hashimoto): react-router-domのhistory.pushを記録するためにpushメソッドを専用メソッドに置き換え
jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useHistory: () => ({
        push: mockHistoryPush,
        goBack: mockHistoryGoBack,
    }),
}));

// NOTE(joshua-hashimoto): mswのデフォルトmock API一覧

const queryCache = new QueryCache();

// NOTE(joshua-hashimoto): テストで使えるMockサーバーを設定
export const mockServer = setupServer(...mockHandlers);

beforeAll(() => {
    // NOTE(joshua-hashimoto): テスト開始時(ファイル単位)にMockサーバーを起動
    mockServer.listen();
});

afterEach(() => {
    // NOTE(joshua-hashimoto): 各テスト(1テスト)毎にMockサーバーの状態を初期化
    mockServer.resetHandlers();
    mockServer.restoreHandlers();
    testQueryClient.clear();
    queryCache.clear();
    Modal.destroyAll();
});

afterAll(() => {
    // NOTE(joshua-hashimoto): テスト終了時(ファイル単位)にMockサーバーを閉じる
    mockServer.close();
});

// NOTE(joshua-hashimoto): テスト時のreact-queryのloggerを設定
setLogger({
    log: console.log,
    warn: console.warn,
    error: () => {},
});
