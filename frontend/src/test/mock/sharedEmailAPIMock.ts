import _ from "lodash";
import moment from "moment";
import { rest } from "msw";
import { v4 as uuidv4 } from "uuid";
import { Endpoint } from "~/domain/api";
import {
    SharedEmailAttachmentModel,
    SharedEmailResponseModel,
} from "~/models/sharedEmailModel";
import { randString } from "~/utils/utils";
import { mockServerPath } from "../utils";

export const createSharedEmailAttachment = (): SharedEmailAttachmentModel => {
    return {
        id: uuidv4(),
        name: randString(),
    };
};

export const sharedEmailResponseData: SharedEmailResponseModel = {
    id: uuidv4(),
    message_id: uuidv4(),
    attachments: _.range(2).map(createSharedEmailAttachment),
    category: undefined,
    estimated_category: undefined,
    from_address: randString() + "@example.com",
    from_name: randString(),
    html: "",
    sent_date: moment().toLocaleString(),
    shared: true,
    staff_in_charge__id: uuidv4(),
    staff_in_charge__name: randString(),
    subject: randString(),
    text: _.range(6).map(randString).join(""),
};

export const mockSharedEmailFetchDetailSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.sharedEmails + "/:id"),
    (req, res, ctx) => {
        const responseData = { ...sharedEmailResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockSharedEmailUpdateSuccessAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.sharedEmails + "/:id/forward"),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockSharedEmailDeleteSuccessAPIRoute = rest.delete(
    mockServerPath("/" + Endpoint.sharedEmails + "/:id"),
    (req, res, ctx) => {
        const responseData = { ...sharedEmailResponseData };
        return res(ctx.status(204), ctx.json(responseData));
    }
);

export const sharedEmailAPIMock = [
    mockSharedEmailFetchDetailSuccessAPIRoute,
    mockSharedEmailUpdateSuccessAPIRoute,
    mockSharedEmailDeleteSuccessAPIRoute,
];
