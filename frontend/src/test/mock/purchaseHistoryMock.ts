import { rest } from "msw";
import { Endpoint } from "~/domain/api";
import { convertPurchaseHistoryResponseModelToPurchaseHistoryModel } from "~/hooks/usePurchaseHistory";
import {
    PurchaseHistoryModel,
    PurchaseHistoryResponseModel,
} from "~/models/purchaseHistory";
import { PaginationResponseModel } from "~/models/responseModel";

export const mockPurchaseHistoryAPIResponseMockData: PurchaseHistoryResponseModel[] =
    [
        {
            card_last4: "4242",
            created_time: "2022-04-02T20:04:56.870164+09:00",
            id: "89ea359f-d711-4dd8-a713-6532211ebbb0",
            method_name: "クレジットカード",
            option_names: ["ユーザー追加+1"],
            period_end: "2022-04-30",
            period_start: "2022-04-01",
            plan_name: "ライト",
            price: 5500,
        },

        {
            card_last4: "4243",
            created_time: "2022-04-03T11:30:38.427027+09:00",
            id: "e7a33e93-5667-44e0-b183-be055630b823",
            method_name: "クレジットカード",
            option_names: ["配信開封情報の取得"],
            period_end: "2022-04-29",
            period_start: "2022-03-31",
            plan_name: "スタンダード",
            price: 4400,
        },

        {
            card_last4: "4244",
            created_time: "2022-04-04T11:30:35.242378+09:00",
            id: "545e4166-597a-4163-9e12-49f7b8cde9f6",
            method_name: "クレジットカード",
            option_names: ["配信件数の上限"],
            period_end: "2022-04-28",
            period_start: "2022-03-30",
            plan_name: "プロフェッショナル",
            price: 1100,
        },
    ];

export const mockPurchaseHistoryMockData: PurchaseHistoryModel[] =
    mockPurchaseHistoryAPIResponseMockData.map((item) =>
        convertPurchaseHistoryResponseModelToPurchaseHistoryModel(item)
    );

export const mockFetchPurchaseHistoriesSuccess = rest.get(
    `/app_staffing/${Endpoint.purchaseHistory}`,
    (req, res, ctx) => {
        const responseData: PaginationResponseModel<
            PurchaseHistoryResponseModel[]
        > = {
            count: mockPurchaseHistoryAPIResponseMockData.length,
            results: [...mockPurchaseHistoryAPIResponseMockData],
            next: "",
            previous: "",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const purchaseHistoryAPIMock = [mockFetchPurchaseHistoriesSuccess];
