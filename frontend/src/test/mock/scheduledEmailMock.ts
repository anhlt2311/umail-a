import _ from "lodash";
import moment from "moment";
import { rest } from "msw";
import { v4 as uuidv4 } from "uuid";
import { Endpoint } from "~/domain/api";
import {
    ScheduledEmailAttachmentDownloadModel,
    ScheduledEmailAttachmentModel,
} from "~/models/scheduledEmailModel";
import { randString } from "~/utils/utils";
import { mockServerPath } from "../utils";

export const createScheduledEmailAttachment =
    (): ScheduledEmailAttachmentModel => {
        return {
            id: uuidv4(),
            gcpLink: randString(),
            name: randString(),
            size: _.random(1, 99999),
            status: _.random(1, 5),
        };
    };

export const scheduledEmailAttachmentsAPIResponseContent: ScheduledEmailAttachmentModel[] =
    _.range(2).map(createScheduledEmailAttachment);

export const scheduledEmailAttachmentAuthorizeAPIResponseContent: ScheduledEmailAttachmentDownloadModel =
    {
        attachments: scheduledEmailAttachmentsAPIResponseContent,
        expiredDate: moment.toString(),
        subject: randString(),
    };

export const mockScheduledEmailAttachmentAuthorizeDownloadSuccessAPIRoute =
    rest.get(
        mockServerPath("/" + Endpoint.scheduledEmailFiles + ":id"),
        (req, res, ctx) => {
            const responseData = {
                ...scheduledEmailAttachmentAuthorizeAPIResponseContent,
            };
            return res(ctx.status(200), ctx.json(responseData));
        }
    );

export const mockScheduledEmailAttachmentsDownloadSuccessAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.scheduledEmailFilesDownload),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(201), ctx.json(responseData));
    }
);

export const scheduledEmailAPIMock = [
    mockScheduledEmailAttachmentAuthorizeDownloadSuccessAPIRoute,
    mockScheduledEmailAttachmentsDownloadSuccessAPIRoute,
];
