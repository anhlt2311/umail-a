import { rest } from "msw";
import { Endpoint } from "~/domain/api";
import {
    convertUserListItemAPIModelToUserListItemModel,
    useUserUpdateAPIMutation,
} from "~/hooks/useUser";
import { PaginationResponseModel } from "~/models/responseModel";
import { v4 as uuidv4 } from "uuid";
import moment from "moment";
import { generateRandomToken, randString } from "~/utils/utils";
import { USER_ROLES } from "~/utils/constants";
import _ from "lodash";
import {
    UserFormModel,
    UserListItemAPIModel,
    UserListItemModel,
} from "~/models/userModel";
import { mockServerPath } from "../utils";

export const generateMockUserListItem = (
    isIndexed?: boolean,
    index?: number
): UserListItemAPIModel => {
    const id = uuidv4();
    const now = moment().toString();
    const email = randString() + "@example.com";
    const telBase = _.random(0, 900);
    const tel1 = ("000000" + telBase).slice(-4);
    const tel2 = ("000000" + telBase + 1).slice(-4);
    const tel3 = ("000000" + telBase + 2).slice(-4);

    return {
        display_name: "example user " + (isIndexed ? index : id),
        editable: true,
        email,
        email_signature: `===\nuser ${id}\n===`,
        first_name: id,
        last_name: "example",
        id,
        is_active: true,
        is_user_admin: true,
        last_login: now,
        modified_time: now,
        modified_user__name: email,
        old_id: null,
        registed_at: null,
        role: USER_ROLES[_.random(USER_ROLES.length - 1)],
        tel1,
        tel2,
        tel3,
        username: email,
        user_service_id: randString(),
    };
};

export const usersAPIResponseContent: UserListItemAPIModel[] = _.range(10).map(
    (val, index) => {
        return generateMockUserListItem(true, index);
    }
);

export const fixedUsersAPIResponseContent: UserListItemAPIModel[] = [
    {
        display_name: "example user 2",
        editable: true,
        email: "admin@example.com",
        email_signature: "===========\nThe admin\n===========",
        first_name: "admin",
        id: "6dba67ff-4bb7-4210-afec-f6fbec1c5fcb",
        is_active: true,
        is_user_admin: true,
        last_login: "2022-04-07T11:45:01.108451+09:00",
        last_name: "example",
        modified_time: "2022-03-30T11:24:20.851136+09:00",
        modified_user__name: "master@example.com",
        old_id: null,
        registed_at: null,
        role: "admin",
        tel1: undefined,
        tel2: undefined,
        tel3: undefined,
        username: "admin@example.com",
        user_service_id: randString(),
    },
    {
        display_name: "example user 1",
        editable: true,
        email: "master@example.com",
        email_signature: undefined,
        first_name: "",
        id: "c9ed1cd2-f0d3-4075-8b4e-908b046e3c9d",
        is_active: true,
        is_user_admin: false,
        last_login: "2022-04-07T11:56:42.645887+09:00",
        last_name: "",
        modified_time: "2022-03-17T14:49:33.772293+09:00",
        modified_user__name: "example admin",
        old_id: null,
        registed_at: null,
        role: "master",
        tel1: undefined,
        tel2: undefined,
        tel3: undefined,
        username: "master@example.com",
        user_service_id: randString(),
    },
    {
        display_name: "TEST NORMAL USER",
        editable: true,
        email: "user@example.com",
        email_signature: "***********\n A User of example.com\n ***********\n",
        first_name: "NORMAL USER",
        id: "d31034e2-ca12-4a6f-b1dc-0be092d1ac5d",
        is_active: false,
        is_user_admin: false,
        last_login: "2018-01-01T09:00:00+09:00",
        last_name: "TEST",
        modified_time: "2022-03-17T14:49:33.772293+09:00",
        modified_user__name: "TEST",
        old_id: null,
        registed_at: null,
        role: "admin",
        tel1: undefined,
        tel2: undefined,
        tel3: undefined,
        username: "user@example.com",
        user_service_id: randString(),
    },
];

export const userFormData: UserFormModel = {
    avatar: "https://picsum.photos/200/300",
    email: "example@example.com",
    email_signature: " email\nsignature",
    first_name: "user",
    last_name: "example",
    user_service_id: "user_0000000001",
    role: "master",
    tel1: "001",
    tel2: "112",
    tel3: "223",
    password: "Abcd12345%",
    is_active: true,
    pk: generateRandomToken(),
};

export const usersModelList: UserListItemModel[] = usersAPIResponseContent.map(
    (data) => convertUserListItemAPIModelToUserListItemModel(data)
);

export const fixedUsersModelList: UserListItemModel[] =
    fixedUsersAPIResponseContent.map(
        convertUserListItemAPIModelToUserListItemModel
    );

export const usersPaginationResponseData: PaginationResponseModel<
    UserListItemAPIModel[]
> = {
    count: fixedUsersAPIResponseContent.length,
    results: fixedUsersAPIResponseContent,
    next: "",
    previous: "",
};

export const mockFetchUsersSuccessAPIRoute = rest.get(
    `/app_staffing/${Endpoint.users}`,
    (req, res, ctx) => {
        let responseData = { ...usersPaginationResponseData };
        const isActiveQueryParam = req.url.searchParams.get("is_active");
        if (isActiveQueryParam !== null) {
            const isActive = isActiveQueryParam === "true";
            const filteredResults = responseData.results.filter(
                (user) => user.is_active === isActive
            );
            responseData.results = filteredResults;
            responseData.count = filteredResults.length;
        }
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockFetchUserSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.users + "/:id"),
    (req, res, ctx) => {
        const responseData = { ...usersAPIResponseContent[0] };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockUpdateUserSuccessAPIRoute = rest.patch(
    mockServerPath("/" + Endpoint.users + "/:id"),
    (req, res, ctx) => {
        const responseData = { ...usersAPIResponseContent[0] };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const userAPIMock = [
    mockFetchUsersSuccessAPIRoute,
    mockFetchUserSuccessAPIRoute,
    mockUpdateUserSuccessAPIRoute,
];
