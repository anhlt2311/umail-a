import { rest } from "msw";
import {
    ScheduledEmailSettingModel,
    ScheduledEmailSettingResponseModel,
} from "~/models/scheduledEmailSettingModel";
import { v4 as uuidv4 } from "uuid";
import { convertScheduledEmailSettingResponseModelToScheduledEmailSettingModel } from "~/hooks/useScheduledEmailSetting";
import { Endpoint } from "~/domain/api";

const companyId = uuidv4();
const userId = uuidv4();

export const scheduledEmailSettingAPIMockData: ScheduledEmailSettingResponseModel =
    {
        file_type: 2,
        company: companyId,
        connection_type: 2,
        hostname: "example.com",
        is_open_count_available: true,
        is_open_count_extra_period_available: true,
        is_use_attachment_max_size_available: true,
        is_use_delivery_interval_available: true,
        is_use_remove_promotion_available: true,
        modified_time: "2022-03-02T17:32:52.348962+09:00",
        modified_user: userId,
        modified_user__name: "example master",
        password: "Abcd1234",
        port_number: 465,
        target_count_addon_purchase_count: 3,
        use_attachment_max_size: true,
        use_open_count: true,
        use_open_count_extra_period: true,
        use_remove_promotion: true,
        username: "example_user",
    };

export const scheduledEmailSettingMockData: ScheduledEmailSettingModel =
    convertScheduledEmailSettingResponseModelToScheduledEmailSettingModel(
        scheduledEmailSettingAPIMockData
    );

export const scheduledEmailSettingSuccess = rest.get(
    `/app_staffing/${Endpoint.scheduledEmailSetting}`,
    (req, res, ctx) => {
        const responseData = {
            ...scheduledEmailSettingAPIMockData,
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const scheduledEmailSettingAPIMock = [scheduledEmailSettingSuccess];
