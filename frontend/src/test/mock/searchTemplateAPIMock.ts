import { rest } from "msw";
import { ContactSearchTemplateContentResponseModel } from "~/models/contactModel";
import { OrganizationSearchTemplateContentResponseModel } from "~/models/organizationModel";
import {
    ScheduledEmailContactPreferenceSearchTemplateContentRequestModel,
    ScheduledEmailContactPreferenceSearchTemplateContentResponseModel,
    ScheduledEmailSearchTemplateContentResponseModel,
} from "~/models/scheduledEmailModel";
import {
    SearchTemplateResponseContentModel,
    SearchTemplateResponseModel,
} from "~/models/searchTemplateModel";
import { mockServerPath } from "../utils";

export const searchTemplateOrganizationContentResponseData: SearchTemplateResponseContentModel<OrganizationSearchTemplateContentResponseModel>[] =
    [];

export const searchTemplateOrganizationResponseData: SearchTemplateResponseModel<OrganizationSearchTemplateContentResponseModel> =
    {
        templates: searchTemplateOrganizationContentResponseData,
        total_available_count:
            searchTemplateOrganizationContentResponseData.length,
    };

export const mockFetchOrganizationSearchTemplateSuccessRoute = rest.get(
    mockServerPath("/search_template/organization"),
    (req, res, ctx) => {
        const responseData = { ...searchTemplateOrganizationResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const searchTemplateContactContentResponseData: SearchTemplateResponseContentModel<ContactSearchTemplateContentResponseModel>[] =
    [];

export const searchTemplateContactResponseData: SearchTemplateResponseModel<ContactSearchTemplateContentResponseModel> =
    {
        templates: searchTemplateContactContentResponseData,
        total_available_count: searchTemplateContactContentResponseData.length,
    };

export const mockFetchContactSearchTemplateSuccessRoute = rest.get(
    mockServerPath("/search_template/contact"),
    (req, res, ctx) => {
        const responseData = { ...searchTemplateContactResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const searchTemplateScheduledEmailContentResponseData: SearchTemplateResponseContentModel<ScheduledEmailSearchTemplateContentResponseModel>[] =
    [];

export const searchTemplateScheduledEmailResponseData: SearchTemplateResponseModel<ScheduledEmailSearchTemplateContentResponseModel> =
    {
        templates: searchTemplateScheduledEmailContentResponseData,
        total_available_count:
            searchTemplateScheduledEmailContentResponseData.length,
    };

export const mockFetchScheduledEmailSearchTemplateSuccessRoute = rest.get(
    mockServerPath("/search_template/scheduled_email"),
    (req, res, ctx) => {
        const responseData = { ...searchTemplateScheduledEmailResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const searchTemplateScheduledEmailContactPreferenceContentResponseData: SearchTemplateResponseContentModel<ScheduledEmailContactPreferenceSearchTemplateContentResponseModel>[] =
    [];

export const searchTemplateScheduledEmailContactPreferenceResponseData: SearchTemplateResponseModel<ScheduledEmailContactPreferenceSearchTemplateContentResponseModel> =
    {
        templates:
            searchTemplateScheduledEmailContactPreferenceContentResponseData,
        total_available_count:
            searchTemplateScheduledEmailContactPreferenceContentResponseData.length,
    };

export const mockFetchScheduledEmailContactPreferenceSearchTemplateSuccessRoute =
    rest.get(
        mockServerPath("/search_template/contact_mail_preference"),
        (req, res, ctx) => {
            const responseData = {
                ...searchTemplateScheduledEmailResponseData,
            };
            return res(ctx.status(200), ctx.json(responseData));
        }
    );

export const searchTemplateAPIMock = [
    mockFetchOrganizationSearchTemplateSuccessRoute,
    mockFetchContactSearchTemplateSuccessRoute,
    mockFetchScheduledEmailSearchTemplateSuccessRoute,
    mockFetchScheduledEmailContactPreferenceSearchTemplateSuccessRoute,
];
