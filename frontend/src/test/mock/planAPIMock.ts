import { rest } from "msw";
import { Endpoint } from "~/domain/api";
import {
    PlanSummaryResponseModel,
    PlanUserDataAPIModel,
} from "~/models/planModel";
import { mockServerPath } from "../utils";

export const mockPlanSummaryAPIResponseData: PlanSummaryResponseModel = {
    plan_id: 2,
    plan_name: "スタンダード",
    remaining_days: "2899",
    payment_error_exists: false,
    payment_error_card_ids: [],
};

export const mockPlanUserInfoAPIResponseData: PlanUserDataAPIModel = {
    current_user_count: 6,
    default_user_count: 10,
    expiration_date: "2022-04-28",
    id: "e8bdbca6-ad1e-486a-9509-5733181a1afc",
    plan_master_id: 2,
    user_registration_limit: 11,
};

export const mockPlanSummarySuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.planSummary),
    (req, res, ctx) => {
        const responseData = { ...mockPlanSummaryAPIResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanSummaryFreeTrialSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.planSummary),
    (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 0,
            plan_name: "無料トライアル",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanSummaryFreeTrialFailureWithRemainingDaysAPIRoute =
    rest.get(mockServerPath("/" + Endpoint.planSummary), (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 0,
            plan_name: "無料トライアル",
            remaining_days: "9",
        };
        return res(ctx.status(200), ctx.json(responseData));
    });

export const mockPlanSummaryFreeTrialFailureWithExpireTodayAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.planSummary),
    (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 0,
            plan_name: "無料トライアル",
            remaining_days: "0",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanSummaryFreeTrialFailureWithNoRemainingDaysAPIRoute =
    rest.get(mockServerPath("/" + Endpoint.planSummary), (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 0,
            plan_name: "無料トライアル",
            remaining_days: "",
        };
        return res(ctx.status(200), ctx.json(responseData));
    });

export const mockPlanSummaryLightPlanSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.planSummary),
    (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 1,
            plan_name: "ライト",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanSummaryLightPlanFailureWithRemainingDaysAPIRoute =
    rest.get(mockServerPath("/" + Endpoint.planSummary), (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 1,
            plan_name: "ライト",
            payment_error_exists: true,
            remaining_days: "9",
        };
        return res(ctx.status(200), ctx.json(responseData));
    });

export const mockPlanSummaryLightPlanFailureWithExpireTodayAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.planSummary),
    (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 1,
            plan_name: "ライト",
            payment_error_exists: true,
            remaining_days: "0",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanSummaryLightPlanFailureWithNoRemainingDaysAPIRoute =
    rest.get(mockServerPath("/" + Endpoint.planSummary), (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 1,
            plan_name: "ライト",
            payment_error_exists: true,
            remaining_days: "",
        };
        return res(ctx.status(200), ctx.json(responseData));
    });

export const mockPlanSummaryStandardPlanSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.planSummary),
    (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 2,
            plan_name: "スタンダード",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanSummaryStandardPlanFailureWithRemainingDaysAPIRoute =
    rest.get(mockServerPath("/" + Endpoint.planSummary), (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 2,
            plan_name: "スタンダード",
            payment_error_exists: true,
            remaining_days: "9",
        };
        return res(ctx.status(200), ctx.json(responseData));
    });

export const mockPlanSummaryStandardPlanFailureWithExpireTodayAPIRoute =
    rest.get(mockServerPath("/" + Endpoint.planSummary), (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 2,
            plan_name: "スタンダード",
            payment_error_exists: true,
            remaining_days: "0",
        };
        return res(ctx.status(200), ctx.json(responseData));
    });

export const mockPlanSummaryStandardPlanFailureWithNoRemainingDaysAPIRoute =
    rest.get(mockServerPath("/" + Endpoint.planSummary), (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 2,
            plan_name: "スタンダード",
            payment_error_exists: true,
            remaining_days: "",
        };
        return res(ctx.status(200), ctx.json(responseData));
    });

export const mockPlanSummaryProfessionalPlanSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.planSummary),
    (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 3,
            plan_name: "プロフェッショナル",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanSummaryProfessionalPlanFailureWithRemainingDaysAPIRoute =
    rest.get(mockServerPath("/" + Endpoint.planSummary), (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 3,
            plan_name: "プロフェッショナル",
            payment_error_exists: true,
            remaining_days: "9",
        };
        return res(ctx.status(200), ctx.json(responseData));
    });

export const mockPlanSummaryProfessionalPlanFailureWithExpireTodayAPIRoute =
    rest.get(mockServerPath("/" + Endpoint.planSummary), (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 3,
            plan_name: "プロフェッショナル",
            payment_error_exists: true,
            remaining_days: "0",
        };
        return res(ctx.status(200), ctx.json(responseData));
    });

export const mockPlanSummaryProfessionalPlanFailureWithNoRemainingDaysAPIRoute =
    rest.get(mockServerPath("/" + Endpoint.planSummary), (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 3,
            plan_name: "プロフェッショナル",
            payment_error_exists: true,
            remaining_days: "",
        };
        return res(ctx.status(200), ctx.json(responseData));
    });

export const mockPlanSummaryExpiredFreeTrialSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.planSummary),
    (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 0,
            plan_name: "無料トライアル",
            remaining_days: "",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);
export const mockPlanSummaryExpiredPayedPlanSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.planSummary),
    (req, res, ctx) => {
        const responseData: PlanSummaryResponseModel = {
            ...mockPlanSummaryAPIResponseData,
            plan_id: 1,
            plan_name: "ライト",
            remaining_days: "",
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanFetchUserInfoSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData = { ...mockPlanUserInfoAPIResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanFetchUserInfoWithFreeTrialSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData: PlanUserDataAPIModel = {
            ...mockPlanUserInfoAPIResponseData,
            plan_master_id: 0,
            user_registration_limit: 5,
            default_user_count: 5,
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanFetchUserInfoSameUserCountSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData: PlanUserDataAPIModel = {
            ...mockPlanUserInfoAPIResponseData,
            user_registration_limit: 5,
            default_user_count: 5,
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanFetchUserInfoFailureAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(400), ctx.json(responseData));
    }
);

export const mockPlanFetchFreeTrialUserInfoSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData: PlanUserDataAPIModel = {
            ...mockPlanUserInfoAPIResponseData,
            plan_master_id: 0,
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanFetchLightPlanUserInfoSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData: PlanUserDataAPIModel = {
            ...mockPlanUserInfoAPIResponseData,
            plan_master_id: 1,
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanFetchStandardPlanUserInfoSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData: PlanUserDataAPIModel = {
            ...mockPlanUserInfoAPIResponseData,
            plan_master_id: 2,
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanFetchProfessionalPlanUserInfoSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData: PlanUserDataAPIModel = {
            ...mockPlanUserInfoAPIResponseData,
            plan_master_id: 3,
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanFetchUserInfoWithDefaultLimitSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData: PlanUserDataAPIModel = {
            ...mockPlanUserInfoAPIResponseData,
            user_registration_limit:
                mockPlanUserInfoAPIResponseData.default_user_count,
        };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanRegisterSuccessAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanRegisterFailureAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.plan),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(400), ctx.json(responseData));
    }
);

export const mockPlanAddAccountLimitSuccessAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.addUserCount),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanAddAccountLimitFailureAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.addUserCount),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(400), ctx.json(responseData));
    }
);

export const mockPlanDeleteAccountLimitSuccessAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.deleteUserCount),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockPlanDeleteAccountLimitFailureAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.deleteUserCount),
    (req, res, ctx) => {
        const responseData = {};
        return res(ctx.status(400), ctx.json(responseData));
    }
);

export const planAPIMock = [
    mockPlanSummarySuccessAPIRoute,
    mockPlanFetchUserInfoSuccessAPIRoute,
    mockPlanRegisterSuccessAPIRoute,
    mockPlanAddAccountLimitSuccessAPIRoute,
    mockPlanDeleteAccountLimitSuccessAPIRoute,
];
