import { rest } from "msw";
import { Endpoint } from "~/domain/api";
import {
    TenantMyProfileModel,
    TenantRecaptchaResponseModel,
} from "~/models/tenantModel";
import { generateRandomToken } from "~/utils/utils";
import { mockServerPath } from "../utils";

export const mockSiteKeyResponseData: TenantRecaptchaResponseModel = {
    sitekey: generateRandomToken(), // NOTE(joshua-hashimoto): ReCAPTCHAコンポーネントを含むテストの実行のためには本物のキーが必要かもしれない
    test_mode: false,
};

export const tenantMyProfileResponseData: TenantMyProfileModel = {
    avatar: "https://picsum.photos/200/300",
    email: "example@example.com",
    email_signature: " email\nsignature",
    first_name: "user",
    last_name: "example",
    role: "master",
    tel1: "001",
    tel2: "112",
    tel3: "223",
};

export const mockFetchTenantReCAPTCHASiteKeySuccessAPIRoute = rest.get(
    mockServerPath("/tenant/sitekey"),
    (req, res, ctx) => {
        let responseData = { ...mockSiteKeyResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);
export const mockPostTenantReCAPTCHASiteKeySuccessAPIRoute = rest.post(
    mockServerPath("/tenant/sitekey"),
    (req, res, ctx) => {
        let responseData = { ...mockSiteKeyResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const mockFetchTenantMyProfileInfoSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.tenantMyProfile),
    (req, res, ctx) => {
        return res(ctx.status(200), ctx.json({}));
    }
);

export const mockPostTenantMyProfileInfoSuccessAPIRoute = rest.post(
    mockServerPath("/" + Endpoint.tenantMyProfile),
    (req, res, ctx) => {
        return res(ctx.status(201), ctx.json({}));
    }
);

export const tenantAPIMock = [
    mockFetchTenantReCAPTCHASiteKeySuccessAPIRoute,
    mockPostTenantReCAPTCHASiteKeySuccessAPIRoute,
    mockFetchTenantMyProfileInfoSuccessAPIRoute,
    mockPostTenantMyProfileInfoSuccessAPIRoute,
];
