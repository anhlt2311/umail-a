import { rest } from "msw";
import { Endpoint } from "~/domain/api";
import { convertContactListResponseModelToContactListModel } from "~/hooks/useContact";
import {
    ContactListModel,
    ContactListResponseModel,
} from "~/models/contactModel";
import { PaginationResponseModel } from "~/models/responseModel";
import { mockServerPath } from "../utils";

export const contactListResponseContent: ContactListResponseModel[] = [
    {
        id: "3cf02dc5-a1e3-4fd5-b71c-c3e0c8727aec",
        display_name: "t s",
        email: "dropscar041205@example.com",
        cc_mails: ["cc@example.com", "cc2@example.com"],
        position: "position 1",
        department: "department 1",
        organization__name: "Example Org 1",
        staff__name: "example staff name 1",
        last_visit: "2022-04-01T17:01:16.631362+09:00",
        created_time: "2022-04-02T17:01:16.631362+09:00",
        modified_time: "2022-04-03T11:57:31.523441+09:00",
        comments: [
            {
                content: "example comment",
                created_time: "2022-04-04T17:01:16.631362+09:00",
                created_user__name: "comment_user",
                modified_time: "",
                modified_user__name: "",
            },
        ],
        tel1: "112",
        tel2: "1113",
        tel3: "1114",
        tags: [],
        tag_objects: [
            {
                value: "Django",
                color: "default",
            },
        ],
        contactpreference: {
            wants_location_hokkaido_japan: false,
            wants_location_touhoku_japan: false,
            wants_location_kanto_japan: true,
            wants_location_chubu_japan: false,
            wants_location_toukai_japan: false,
            wants_location_kansai_japan: false,
            wants_location_shikoku_japan: false,
            wants_location_chugoku_japan: false,
            wants_location_kyushu_japan: false,
            wants_location_other_japan: false,
        },
        category: "heart",
        is_ignored: false,
        contactjobtypepreferences: [
            "dev_designer",
            "dev_front",
            "dev_server",
            "dev_pm",
            "dev_other",
            "infra_server",
            "infra_network",
            "infra_security",
            "infra_database",
            "infra_sys",
            "infra_other",
            "other_eigyo",
            "other_kichi",
            "other_support",
            "other_other",
        ],
        contactpersonneltypepreferences: [
            "dev_designer",
            "dev_front",
            "dev_server",
            "dev_pm",
            "dev_other",
            "infra_server",
            "infra_network",
            "infra_security",
            "infra_database",
            "infra_sys",
            "infra_other",
            "other_eigyo",
            "other_kichi",
            "other_support",
            "other_other",
        ],
    },
    {
        id: "da9e22e4-b695-4db5-bf5f-f1bcb28ba879",
        display_name: "テス ト",
        email: "admin@example.com",
        cc_mails: [],
        position: "position 2",
        department: "department 2",
        organization__name: "テスト",
        staff__name: "example staff name 2",
        last_visit: undefined,
        created_time: "2022-04-05T15:04:55.994108+09:00",
        modified_time: "2022-04-06T15:04:55.994156+09:00",
        comments: [],
        tel1: "223",
        tel2: "2224",
        tel3: "2225",
        tags: ["react"],
        tag_objects: [],
        contactpreference: {
            wants_location_hokkaido_japan: false,
            wants_location_touhoku_japan: false,
            wants_location_kanto_japan: false,
            wants_location_chubu_japan: false,
            wants_location_toukai_japan: false,
            wants_location_kansai_japan: true,
            wants_location_shikoku_japan: false,
            wants_location_chugoku_japan: false,
            wants_location_kyushu_japan: false,
            wants_location_other_japan: false,
        },
        category: "frown",
        is_ignored: true,
        contactjobtypepreferences: [],
        contactpersonneltypepreferences: [],
    },
];

export const contactListResponseData: PaginationResponseModel<
    ContactListResponseModel[]
> = {
    results: contactListResponseContent,
    count: contactListResponseContent.length,
    next: "",
    previous: "",
};

export const contactListContent: ContactListModel[] =
    contactListResponseContent.map(
        convertContactListResponseModelToContactListModel
    );

export const mockFetchContactsSuccessAPIRoute = rest.get(
    mockServerPath("/" + Endpoint.contacts),
    (req, res, ctx) => {
        let responseData = { ...contactListResponseData };
        return res(ctx.status(200), ctx.json(responseData));
    }
);

export const contactAPIMock = [mockFetchContactsSuccessAPIRoute];
