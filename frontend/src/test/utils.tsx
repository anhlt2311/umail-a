import React, { ReactElement, ReactNode } from "react";
import { render as rtlRender, RenderOptions } from "@testing-library/react";
import { BrowserRouter, MemoryRouter } from "react-router-dom";
import { ConfigProvider } from "antd";
import moment from "moment";
import { customLocale } from "~/utils/constants";
import { Provider } from "react-redux";
import { RecoilRoot } from "recoil";
import { QueryClient, QueryClientProvider } from "react-query";
import { AnyAction, configureStore, Store } from "@reduxjs/toolkit";
import login, { LoginInitialState } from "~/reducers/login";
import userEvent from "@testing-library/user-event";

moment.locale("ja"); // setting a timezone to JP.

type CustomRenderProps = RenderOptions & {};

type CustomRenderWithReduxProps = CustomRenderProps & {
    store: Store<any, AnyAction>;
};

type RenderWrapperModel = React.JSXElementConstructor<{
    children: React.ReactElement;
}>;

export const mockServerAuthPath = (path: string) => {
    const url = "/api-token-auth";
    if (!path) {
        return url;
    }
    return url + path;
};

export const mockServerPath = (path: string) => {
    const url = "/app_staffing";
    if (!path) {
        return url;
    }
    return url + path;
};

export const testQueryClient = new QueryClient({
    defaultOptions: {
        queries: {
            retry: false,
        },
    },
});

export const testStore = configureStore({
    reducer: {
        login,
    },
    preloadedState: {
        login: {
            ...LoginInitialState,
            threeLogin: 2,
        },
    },
});

export const renderWithAllProviders = (
    ui: ReactElement,
    { store, ...renderOptions }: CustomRenderWithReduxProps
) => {
    const passingStore = store || testStore;
    const wrapper: RenderWrapperModel = ({ children }) => {
        return (
            <QueryClientProvider client={testQueryClient}>
                <RecoilRoot>
                    <Provider store={passingStore}>
                        <BrowserRouter>
                            <ConfigProvider locale={customLocale}>
                                {children}
                            </ConfigProvider>
                        </BrowserRouter>
                    </Provider>
                </RecoilRoot>
            </QueryClientProvider>
        );
    };
    return rtlRender(ui, { wrapper, ...renderOptions });
};

export const renderWithRedux = (
    ui: ReactElement,
    { store, ...renderOptions }: CustomRenderWithReduxProps
) => {
    const passingStore = store || testStore;
    const wrapper: RenderWrapperModel = ({ children }) => {
        return (
            <Provider store={passingStore}>
                <BrowserRouter>
                    <ConfigProvider locale={customLocale}>
                        {children}
                    </ConfigProvider>
                </BrowserRouter>
            </Provider>
        );
    };
    return rtlRender(ui, { wrapper, ...renderOptions });
};

export const QueryClientWrapper = ({ children }: { children: ReactNode }) => {
    return (
        <QueryClientProvider client={testQueryClient}>
            <RecoilRoot>
                <BrowserRouter>
                    <ConfigProvider locale={customLocale}>
                        {children}
                    </ConfigProvider>
                </BrowserRouter>
            </RecoilRoot>
        </QueryClientProvider>
    );
};

export const renderWithQueryClient = (
    ui: ReactElement,
    { ...renderOptions }: CustomRenderProps = {}
) => {
    const wrapper: RenderWrapperModel = ({ children }) => {
        return <QueryClientWrapper>{children}</QueryClientWrapper>;
    };
    return rtlRender(ui, { wrapper, ...renderOptions });
};

export const renderWithRecoil = (
    ui: ReactElement,
    { ...renderOptions }: CustomRenderProps = {}
) => {
    const wrapper: RenderWrapperModel = ({ children }) => {
        return (
            <RecoilRoot>
                <BrowserRouter>
                    <ConfigProvider locale={customLocale}>
                        {children}
                    </ConfigProvider>
                </BrowserRouter>
            </RecoilRoot>
        );
    };
    return rtlRender(ui, { wrapper, ...renderOptions });
};

export const renderWithMemoryRouter = (
    ui: ReactElement,
    { ...renderOptions }: CustomRenderProps = {}
) => {
    const wrapper: RenderWrapperModel = ({ children }) => {
        return <MemoryRouter>{children}</MemoryRouter>;
    };
    return rtlRender(ui, { wrapper, ...renderOptions });
};

export const renderWithBrowserRouter = (
    ui: ReactElement,
    { ...renderOptions }: CustomRenderProps = {}
) => {
    const wrapper: RenderWrapperModel = ({ children }) => {
        return <BrowserRouter>{children}</BrowserRouter>;
    };
    return rtlRender(ui, { wrapper, ...renderOptions });
};

export const mockHistoryPush = jest.fn();
export const mockHistoryGoBack = jest.fn();

export * from "@testing-library/react";
export { renderHook } from "@testing-library/react-hooks";
export { userEvent };
