import React from "react";
import ReactDom from "react-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import App from "./components/App";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import store, { persistor } from "./store";
import LoadingScreen from "./components/Screens/loading";
import { Helmet } from "react-helmet";
import { RecoilRoot } from "recoil";
import "../stylesheets/index.scss";

const queryClient = new QueryClient();

if (process.env.NODE_ENV !== "development") {
    console.log = () => {};
    console.info = () => {};
    console.error = () => {};
}

ReactDom.render(
    <QueryClientProvider client={queryClient}>
        <RecoilRoot>
            <Provider store={store}>
                <PersistGate loading={<LoadingScreen />} persistor={persistor}>
                    <Helmet>
                        <link
                            rel="icon"
                            href="/static/app_staffing/favicon.svg"
                        />
                    </Helmet>
                    <App />
                </PersistGate>
            </Provider>
        </RecoilRoot>
        <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>,
    document.querySelector(".container")
);
