export const TEST_USER = 'useradmin@example.com';
export const TEST_PASSWORD = 'n2W6eKoa';

export const TEST_USER_FIRST_NAME = 'TEST';
export const TEST_USER_LAST_NAME = 'USER';
export const TEST_USER_TEL = '012-3456-7890';

export const TEST_USER_TENNANT_B = 'useradmin@tenant_B.com';
export const TEST_PASSWORD_TENNANT_B = 'n2W6eKoa';

export const TEST_USER_FIRST_NAME_TENNANT_B = 'USER ADMIN';
export const TEST_USER_LAST_NAME_TENNANT_B = 'TEST <TenantB>';
export const TEST_USER_TEL_TENNANT_B = '080-1234-5678';

export const TEST_COMPANY_NAME = 'Tenant A';
export const TEST_COMPANY_DOMAIN_NAME = 'tenant_A.co.jp';
export const TEST_COMPANY_CAPITAL_MAN_YEN = '2000';

export const TEST_COMPANY_NAME_TENNANT_B = 'Tenant B';
export const TEST_COMPANY_DOMAIN_NAME_TENNANT_B = 'tenant_B.co.jp';
export const TEST_COMPANY_CAPITAL_MAN_YEN_TENNANT_B = '3000';
