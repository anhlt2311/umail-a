import { Selector, t } from 'testcafe';

const resourcePath = 'sharedMailNotifications';
const registerPageSuffix = 'register';

const register = '登 録';
const add_condition = '条件を追加';
const attachment = '添付ファイル'

export async function accessSharedMailNotificationsListPage() {
  await t.navigateTo(`${resourcePath}`)
}

export async function accessSharedMailNotificationsRegisterPage() {
  await t.navigateTo(`${resourcePath}/${registerPageSuffix}`)
}

export async function selectSharedMailNotifications(name) {
  await t.click(Selector('.ant-table-row.ant-table-row-level-0').find('td').withText(`${name}`)) // Pick the search result.
}

export async function createSharedMailNotifications(name) {
  await t
    .typeText(Selector('#name'), `${name}`, { replace: true })
    .click(Selector('button[type="button"]').withText(`${add_condition}`))
    .typeText(Selector('#rules\\[0\\]\\.value'), `${name}`, { replace: true })
    .click(Selector('button[type="button"]').withText(`${add_condition}`))
    .click(Selector('#rules\\[1\\]\\.type'))
    .click(Selector('.ant-select-dropdown-menu-item').withText(`${attachment}`))
    .click(Selector('button[type="submit"]').withText(`${register}`));
}

export async function updateSharedMailNotifications(name) {
  await t
    .typeText(Selector('#name'), `${name}`, { replace: true })
    .click(Selector('button[type="submit"]'))// submit // execute update
}

export async function deleteSharedMailNotifications() {
  await t
    .click(Selector('.ant-btn-danger'))
    .pressKey('enter') // execute delete
}
