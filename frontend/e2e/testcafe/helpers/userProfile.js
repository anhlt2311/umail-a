import { Selector, t } from 'testcafe';

const myProfile_resourceUrl = 'http://127.0.0.1:8000/myProfile';

export async function accessMyProfile() {
  await t.navigateTo(`${myProfile_resourceUrl}`);
};

export async function updateMyProfile(lastname, firstname, password, tel) {
  await t
    .typeText(Selector('#last_name'), lastname, { replace: true })
    .typeText(Selector('#first_name'), firstname, { replace: true })
    .typeText(Selector('#password'), password)
    .typeText(Selector('#tel'), tel, { replace: true })
    .click(Selector('#tel'))// Focus on form, to submit by press enter.
    .click(Selector('button[type="submit"]')) // submit
};
