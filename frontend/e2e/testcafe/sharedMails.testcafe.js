import { Selector, t } from 'testcafe';

import testUser from './roles';
import { getPageUrl } from './helpers';
import { accessSharedMailsSearchPage, searchSharedMails, searchAllSharedMails, selectSharedMails, forwardSharedMails } from './helpers/sharedMails';

const errorAlert = Selector('.ant-message-error');
const resourcePath = 'sharedMails';

const sender = 'A Example Company'
const email = 'staff@example.com'
const subject = 'This is an example mail'
const text = 'A Search result must be empty. 3ajt94w9t...'

fixture`SharedEmails`
  .page`http://127.0.0.1:8000/${resourcePath}`;

export async function assertAlertNotExists() {
  await t.expect(errorAlert.exists).notOk('The page is not successfully initialized.') // Check whether login failed.
}

export async function assertListPage() {
  await t.expect(getPageUrl()).match(new RegExp(`${resourcePath}$`)) // move to list page
}

export async function assertDetailPage() {
  await t
    .expect(getPageUrl()).match(new RegExp(`${resourcePath}/[a-zA-Z0-9-]+$`))
}

export async function assertMailNotExist() {
  await t
    .expect(Selector('.ant-empty').exists).ok('Could not found an empty icon, Probably, data is not filtered correctly');
}

export async function assertForward() {
  await t.expect(Selector('.ant-message').textContent).contains('メールボックスに転送しています')
}


export async function testSearch(role) {
  await t.useRole(role)
  await accessSharedMailsSearchPage()
  await assertAlertNotExists()
  await assertListPage()
  await searchSharedMails(sender, email, subject, text)
  await assertMailNotExist()
}

export async function testForward(role) {
  await t.useRole(role)
  await accessSharedMailsSearchPage()
  await assertAlertNotExists()
  await assertListPage()
  await searchAllSharedMails()
  await selectSharedMails()
  await forwardSharedMails()
  await assertForward()
}

// TODO: Use ID instead of class name.
test('testSearch', async (t) => {
  await testSearch(testUser)
});

// TODO: Create a complex condition like Pagination + Search. It requires enough and sophisticated test data, so make your sufficient time to create.

test('testForward', async (t) => {
  await testForward(testUser)
});
