# About
* This is a directory of the frontend Application of the app.
# Architecture
* This frontend is based on React + Redux.
* When a user client access to this app (Url:/), we firstly return index.html to the client.
  * The index.html contains a javascript, which replaces HTML body to the SPA when the browser renders the HTML.
  * After that, SPA access to REST APIs of the backend(Url:/app_staffing/{resourceName}) via Ajax, to access, modify, create data.
  * So a basic request/response flow when a user access the app via a web browser is:
    1. Access to /.
    2. Get index.html.
    3. Initialize a SPA on the browser.
       * The entry point of the SPA is defined in webpack.config.js.
       * basically, it is index.jsx
    4. Render top page in the SPA.
    5. Load data from the backend API via Ajax request.
    6. Render (update) the page again with loaded data.

# Directory structure.
* Basically, this directory follows the structure of the official document of Django, but we have additional directories.
  * e2e : Test cases for end to end test that is runnable as npm scripts.
    * see details of package.json if you want to know more.
  * src: source code of the SPA.
  * stylesheets: A global style for the SPA.
