cd app/$1
env=${2-"development"}

if [ $env == "production" ]; then
  project=umail-254601
elif [ $env == "staging" ]; then
  project=stg-umail-64373
elif [ $env == "test" ]; then
  project=test-umail-412874
else
  project=dev-umail-303302
fi

echo "Deploying '$1' to '$project'...."
gcloud functions --project $project deploy $1 --env-vars-file .env.yaml --runtime=python39 --trigger-http --region asia-northeast1
