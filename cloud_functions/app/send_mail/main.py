import smtplib
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import traceback
import datetime
import asyncio
import os
from google.cloud import storage
import sqlalchemy
import time
import threading
import dkim
import re
import mimetypes

if os.environ.get('CLOUD_FUNCTIONS_ENV') != 'local':
    connection_name = os.environ.get('DB_CONNECTION_NAME', 'DB_CONNECTION_NAME is not set.')
    db_password = os.environ.get('DB_PASSWORD', 'DB_PASSWORD is not set.')
    db_name = os.environ.get('DB_NAME', 'DB_PASSWORD is not set.')
    db_user = "django"
    driver_name = 'postgres+pg8000'
    query_string = dict({"unix_sock": "/cloudsql/{}/.s.PGSQL.5432".format(connection_name)})
    engine = sqlalchemy.create_engine(
         sqlalchemy.engine.url.URL(
             drivername=driver_name,
             username=db_user,
             password=db_password,
             database=db_name,
             query=query_string,
         ),
         pool_size=5,
         max_overflow=2,
         pool_timeout=30,
         pool_recycle=1800
     )

    bucket_name = os.environ.get('GS_BUCKET_NAME', 'GS_BUCKET_NAME is not set.')
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)

else:
    sqllite_file = os.path.join(os.path.dirname(__file__), '../../db.sqlite3')
    engine = sqlalchemy.create_engine('sqlite:///%s' % sqllite_file, echo=True)

MAX_RETRY = 3
SLEEP = 1
SENDING_STATUS = 3
SENT_STATUS = 4
ERROR_STATUS = 5
LOCK = threading.Lock()

DKIM_SELECTOR = os.environ.get('DKIM_SELECTOR', 'DKIM_SELECTOR is not set.')
DKIM_DOMAIN = os.environ.get('DKIM_DOMAIN', 'DKIM_DOMAIN is not set.')
DKIM_PRIVATE_KEY = os.environ.get('DKIM_PRIVATE_KEY', 'DKIM_DOMAIN is not set.')

count = 0

def _get_attachment(filename):
    if os.environ.get('CLOUD_FUNCTIONS_ENV') != 'local':
        blob = bucket.blob(filename)
        content = blob.download_as_bytes()
        return {'filename': os.path.basename(filename), 'content': content}

    else:
        with open(filename) as f:
            content = f.read()
        return {'filename': os.path.basename(filename), 'content': content}

def _attachments(data):
    mb = []
    for e in data:
        attachment = _get_attachment(e['filename'])
        mb.append(attachment)

    return mb

def _send_mail(smtp, email_message):
    server = _server(smtp)

    if smtp['tls']:
        server.ehlo()
        server.starttls()
        server.ehlo()

    server.login(smtp['user'], smtp['password'])
    server.send_message(email_message)
    server.quit()
    return True

def _server(smtp):
    return smtplib.SMTP_SSL(smtp['host'], smtp['port']) if smtp['ssl'] else smtplib.SMTP(smtp['host'], smtp['port'])

async def _run_sending_task(smtp, email_message):
    global count
    with LOCK:
        count += 1

    print("[Info] Index: %d, To: %s" % (count, email_message['To']))
    try:
        loop = asyncio.get_event_loop()
        await loop.run_in_executor(None, _send_mail, smtp, email_message)
    except Exception as e:
        print("[Error] Index: %d, To: %s" % (count, email_message['To']))
        print(traceback.format_exc())

def _encode_address_name(addr):
    regex = re.findall('(.*)<(.*)>', addr)
    if not regex:
        return addr

    name, pure_addr = regex[0]
    encoded_addr =  '%s <%s>'%(Header(name.strip().encode('utf-8'), 'utf-8').encode(), pure_addr.strip())
    return encoded_addr

def _send_mails(data):
    print("[Info] EmailID: %s" % data['email_id'])
    print("[Info] Parameter: %s" % data)

    loop = asyncio.get_event_loop()

    # 性能をあげるために
    # 添付ファイルはメール毎に読まず、
    # 添付ファイルはメール塊(ScheduledEmailの５０件ずつ分割)毎に読むように修正
    attachments = _attachments(data['attachments'])

    # execute by parallel
    task_list = []
    for context in data['contexts']:
        email_message = MIMEMultipart()
        email_message['Subject'] = context['subject']
        email_message['From'] = _encode_address_name(context['from'])
        email_message['To'] = _encode_address_name(context['to'])

        encoded_cc = []
        for cc in context['cc']:
            encoded_cc.append(_encode_address_name(cc))
        email_message['Cc'] = ",".join(encoded_cc)

        format = 'plain'
        if context['format'] == 'html':
            format = 'html'

        email_message.attach(MIMEText(context['message'], format))
        for file_hash in attachments:
            format, enc = mimetypes.guess_type(file_hash['filename'])
            part = MIMEBase(*format.split('/'))
            part.set_payload(file_hash['content'])
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment', filename=file_hash['filename'])
            email_message.attach(part)

        signature = dkim.sign(email_message.as_bytes(),
                              DKIM_SELECTOR.encode('utf-8'),
                              DKIM_DOMAIN.encode('utf-8'),
                              DKIM_PRIVATE_KEY.encode('utf-8'))

        email_message["DKIM-Signature"] = signature[len("DKIM-Signature: ") :].decode()
        task_list.append(_run_sending_task(data['smtp'], email_message))

    loop.run_until_complete(asyncio.gather(*task_list))

def _create_results(data):
    if data['type'] != 'normal':
        return

    with engine.connect() as conn:
        number_of_sent_mails = len(data['contexts'])
        total = data['total']
        email_id = data['email_id']
        with conn.begin() as transaction:

            # Retryしながら頑張る
            for i in range(MAX_RETRY + 1):
                try:
                    if engine.driver == 'pysqlite':
                        send_total_count, status = conn.execute(
                            "SELECT send_total_count, scheduled_email_status_id FROM app_staffing_scheduledemail WHERE id = '{0}'".format(email_id)
                        ).fetchone()
                    else:
                        print("[Info] Before executing Lock: %s" % email_id)
                        send_total_count, status = conn.execute(
                            "SELECT send_total_count, scheduled_email_status_id FROM app_staffing_scheduledemail WHERE id = '{0}' FOR UPDATE".format(email_id)
                        ).fetchone()

                    print("[Info] send_total_count:%s number_of_sent_mails:%s status:%s" % (send_total_count, number_of_sent_mails, status))
                    send_total_count = send_total_count or 0
                    send_total_count += number_of_sent_mails
                    sent_date = 'NULL'

                    if total <= send_total_count:
                        status = SENT_STATUS
                        sent_date = "'%s'" % datetime.datetime.now()

                    else:
                        status = SENDING_STATUS

                    conn.execute(
                        "UPDATE app_staffing_scheduledemail SET send_total_count={0}, scheduled_email_status_id={1}, sent_date={2}  WHERE id = '{3}'".format(send_total_count, status, sent_date, email_id)
                    )
                    print("[Info] After executing UPDATE - send_total_count:%s sent_date:%s status:%s" % (send_total_count, sent_date, status))

                    transaction.commit()
                except Exception as e:
                    print('[Error] update execution error: {0}'.format(e))
                    transaction.rollback()
                    if i == MAX_RETRY:
                        print(traceback.format_exc())
                        break
                    print("sleep {0} sec".format(SLEEP))
                    time.sleep(SLEEP)
                    continue

                break


def send_mail(request):
    new_loop = asyncio.new_event_loop()
    asyncio.set_event_loop(new_loop)

    request_json = request.get_json()
    try:
        _send_mails(request_json)

    # 基本あり得ないが、パラメーターに問題があったりしてエラーになってもログだけ残し正常レスポンスを返す。
    # 理由：CloudTasksがリトライして大量のメールを送ることを防ぐために
    except Exception as e:
        print('send mail error: {0}'.format(e))
        print(traceback.format_exc())

    try:
        _create_results(request_json)

    # DBへの接続および送信結果を更新する処理が失敗してもログだけ残し正常レスポンスを返す
    # 理由：CloudTasksがリトライして大量のメールを送ることを防ぐために
    except Exception as e:
        print('create results error: {0}'.format(e))
        print(traceback.format_exc())

    return request_json
