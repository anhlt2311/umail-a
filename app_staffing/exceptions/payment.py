from rest_framework.exceptions import APIException


class PaymentCardNotRegisteredException(APIException):
    status_code = 400
    default_detail = '支払い処理に失敗しました。お支払い画面よりカード情報をご登録ください。'
    default_code = 'payment_card_not_registered'