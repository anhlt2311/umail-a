from rest_framework.exceptions import APIException

class ScheduledEmailPerameterException(APIException):
    status_code = 400
    default_detail = '必須項目の入力に不備があります。お手数ですが再度ご確認ください。'
    default_code = 'scheduled_email_parameter_failed'

class ScheduledEmailTargetNotSelectedException(APIException):
    status_code = 400
    default_detail = '宛先が選択されていません'
    default_code = 'scheduled_email_target_not_selected'

class ScheduledEmailAttachementSizeException(APIException):
    status_code = 400
    default_code = 'scheduled_email_attachement_size'

class ScheduledEmailAttachementFileLimitException(APIException):
    status_code = 400
    default_code = 'scheduled_email_attachment_file_limit'

class ScheduledEmailTargetCountException(APIException):
    status_code = 400
    default_code = 'scheduled_email_target_not_selected'

class ScheduledEmailReserveAllowedPeriodException(APIException):
    status_code = 400
    default_detail = '1ヶ月以上先の時刻を予約することはできません。配信時刻を変更してください。'
    default_code = 'scheduled_email_reserve_allowed_period'

class ScheduledEmailAlreadySentException(APIException):
    status_code = 400
    default_detail = '「配信中」「配信済」「エラー」ステータスのメールを更新することはできません。'
    default_code = 'scheduled_email_already_sent'

class ScheduledEmailExistsByTermException(APIException):
    status_code = 400
    default_code = 'scheduled_email_exists_by_term'

class ScheduledEmailNotWorkingTimeException(APIException):
    status_code = 400
    default_code = 'scheduled_email_not_working_time'

class ScheduledEmailHolidayException(APIException):
    status_code = 400
    default_code = 'scheduled_email_holiday'

class ScheduledEmailConnectionException(APIException):
    status_code = 400
    default_detail = '接続テストに失敗しました。入力情報に誤りがないかご確認ください。'
    default_code = 'scheduled_email_rconnection_exception'

class ScheduledEmailPastDateException(APIException):
    status_code = 400
    default_detail = '過去の日時で配信メールが予約されています。配信時刻を変更してください。'
    default_code = 'scheduled_email_past_date'

class ScheduledEmailDeliveryTimeIsNull(APIException):
    status_code = 400
    default_detail = '配信日時を選択してください。'
    default_code = 'scheduled_email_date_to_time_required'


class EmailDomainBlocked(APIException):
    status_code = 400
    default_code = 'email_domain_blocked'

    def __init__(self, hostname):
        self.default_detail = {
            'detail': f'{hostname}は使用できません。',
            'code': 'email_domain_blocked',
        }
        super(EmailDomainBlocked, self).__init__(code=self.default_code)


class ScheduledEmailIncorrectPassword(APIException):
    status_code = 400
    default_detail = '正しいパスワードを入力してください。'
    default_code = 'password_is_not_correct'


class ScheduledEmailAttachmentNotExist(APIException):
    status_code = 400
    default_detail = '添付ファイルは見つかりませんでした。'
    default_code = 'scheduled_email_attachment_is_not_exist'
