from rest_framework.exceptions import APIException

class CommentTemplateAddonRevokeException(APIException):
    status_code = 400
    default_code = 'comment_template_addon_revoke'

class SearchTemplateAddonRevokeException(APIException):
    status_code = 400
    default_code = 'search_template_addon_revoke'

class FreeTrialException(APIException):
    status_code = 400
    default_detail = '無料トライアル中のため購入できません'
    default_code = 'free_trial'

class PurchaseCountLimitException(APIException):
    status_code = 400
    default_detail = '購入回数制限を超えています'
    default_code = 'purchase_count_limit'

class ParentNotPurchasedException(APIException):
    status_code = 400
    default_detail = '親アドオンを購入していないため、購入できません'
    default_code = 'parent_not_purchased'

class AddonPurchasePaymentException(APIException):
    status_code = 400
    default_detail = '支払いに失敗しました。'
    default_code = 'addon_purchase_payment'

class AddonPurchasePaymentUnknownException(APIException):
    status_code = 500
    default_detail = '予期せぬエラーにより、支払いに失敗しました。'
    default_code = 'addon_purchase_payment_unknown'

class ChildNotDeletedException(APIException):
    status_code = 400
    default_detail = '子アドオンを削除していないため、削除できません'
    default_code = 'child_not_deleted'

class PlanNotFoundException(APIException):
    status_code = 500
    default_detail = '有効なプランが設定されていません'
    default_code = 'plan_not_found'

class MultiActivePlanFoundException(APIException):
    status_code = 500
    default_detail = '有効なプランが複数設定されています'
    default_code = 'multi_active_plan_found' 

class AddonPurchaseUnknownException(APIException):
    status_code = 500
    default_detail = 'アドオンの購入に失敗しました。ヘルプを参照の上、サポートまでお問い合わせください。'
    default_code = 'addon_purchase_unknown'
