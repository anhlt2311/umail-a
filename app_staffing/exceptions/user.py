from django.conf import settings
from rest_framework.exceptions import APIException


class UserAlreadyInvitedException(APIException):
    status_code = 400
    default_detail = '招待済みのメールアドレスです。'
    default_code = 'user_already_invited'


class UserDuplicateEmailException(APIException):
    status_code = 400
    default_detail = '登録済みのメールアドレスです。'
    default_code = 'user_duplicate_email'


class UserRegisterActivateLimitException(APIException):
    status_code = 400
    default_detail = 'ユーザー数の上限に達しているため、ユーザーの登録ができません。マスターユーザーにお問い合わせください。'
    default_code = 'user_activate_limit'


class UserUpdateActivateLimitException(APIException):
    status_code = 400
    default_detail = 'ユーザー数の上限に達しているため、ユーザーの有効化ができません。マスターユーザーにお問い合わせください。'
    default_code = 'user_activate_limit'


class UserInActivelException(APIException):
    status_code = 400
    default_detail = 'ユーザーが無効化されています。管理者以上のユーザーまでお問い合わせください。'
    default_code = 'user_inactive'


class UserTokenInvalidException(APIException):
    status_code = 401
    default_detail = '認証に失敗しました。お手数ですが、再度ログインをしてください。'
    default_code = 'user_token_invalid'


class UserTokenHasExpiredException(APIException):
    status_code = 401
    default_code = 'user_token_expired'
    default_detail = {"code": 'user_token_expired', "detail": "認証に失敗しました。お手数ですが、再度ログインをしてください。"}

class UserTokenDoesNotExistException(APIException):
    status_code = 401
    default_code = 'user_token_does_not_exist'
    default_detail = {"code": 'user_token_does_not_exist', "detail": "別ブラウザでのログインを検知したため自動でログアウトされました。"}


class FileExtensionValidatorException(APIException):
    status_code = 400
    default_detail = 'アップロードできる画像はJPEG、PNG、GIFのいずれかです。'
    default_code = 'file_extension_validator'


class MaxSizeValidatorException(APIException):
    status_code = 400
    default_detail = 'アップロードできる画像は1MB以下です。'
    default_code = 'file_max_size_validator'


class DeleteFileError(APIException):
    status_code = 400
    default_detail = 'サーバーエラーが発生しました。しばらく時間を置いてから再度お試しいただくか、サポートまでお問い合わせください。'
    default_code = 'delete_file_error'


class DeleteAvatarDefault(APIException):
    status_code = 400
    default_detail = 'アバターを削除する権限はありません。'
    default_code = 'delete_avatar_default'



class UserDoesNotExistException(APIException):
    status_code = 400
    default_code = 'user_does_not_exist'
    default_detail = '対象のユーザーが見つかりませんでした。'


class PermissionDeleteAvatarException(APIException):
    status_code = 400
    default_code = 'permission_to_delete_avatar'
    default_detail = 'アバターを削除する権限がありません。'


class UserServiceIdAlreadyExistsException(APIException):
    status_code = 400
    default_detail = settings.VALUE_VALIDATIONS['user']['user_service_id']['message']['unique_error']
    default_code = 'user_service_id_already_existed'


class UserLoginCanNotDeleteException(APIException):
    status_code = 400
    default_code = 'user_login_can_not_delete'
    default_detail = "ユーザーの削除に失敗しました。"


class UserLoginCanNotInActiveException(APIException):
    status_code = 400
    default_code = 'user_login_can_not_in_active'
    default_detail = 'ユーザーの無効化に失敗しました。'
