from rest_framework.exceptions import APIException

class SenderDoesNotExistException(APIException):
    status_code = 400
    default_detail = '対象の配信者が見つかりませんでした。'
    default_code = 'sender_does_not_exist'

class IsSenderBelongToAnotherCompany(APIException):
    status_code = 400
    default_detail = '正しい配信者を選択してください。'
    default_code = 'is_sender_belong_to_another_company'

class TemplateRegisterLimitException(APIException):
    status_code = 400
    default_detail = '登録可能な上限件数に達しています。'
    default_code = 'template_register_limit'

class TemplateNameBlankException(APIException):
    status_code = 400
    default_detail = 'テンプレート名が空です。テンプレート名を入力してください。'
    default_code = 'template_name_blank'

class TemplateNameRegisteredException(APIException):
    status_code = 400
    default_detail = '同一名称のテンプレートが既に存在します。別のテンプレート名を入力してください。'
    default_code = 'template_name_registered'

class TemplateIsRequiredException(APIException):
    status_code = 400
    default_detail = 'テンプレートが空です。テンプレートを入力してください。'
    default_code = 'templates_required'

class PermissionNotAllowedException(APIException):
    status_code = 403
    default_detail = '特定の権限で操作できます。'
    default_code = 'permission_not_allowed'

class BulkOperationLimitException(APIException):
    status_code = 400
    default_detail = '選択可能な件数は最大10件です。選択件数をご確認ください。'
    default_code = 'bulk_operation_limit'

class PasswordNotMatchException(APIException):
    status_code = 400
    default_detail = 'パスワードが一致していません。パスワードを正しく入力してください'
    default_code = 'password_not_match'

class CardNotRegisteredException(APIException):
    status_code = 400
    default_detail = 'クレジットカードが登録されていません'
    default_code = 'card_not_registered'

class InvalidateTelValueException(APIException):
    status_code = 400
    default_detail = 'TELの欄は全て半角数字で入力してください。'
    default_code = 'invalidate_tel_value'

class InvalidateFaxValueException(APIException):
    status_code = 400
    default_detail = 'FAXの欄は全て半角数字で入力してください。'
    default_code = 'invalidate_fax_value'


class TelValueIsTooLongException(APIException):
    status_code = 400
    default_detail = 'TELは15桁以内で入力してください。'
    default_code = 'tel_value_longer_than_15_characters'


class FaxValueIsTooLongException(APIException):
    status_code = 400
    default_detail = 'FAXは15桁以内で入力してください。'
    default_code = 'fax_value_longer_than_15_characters'


class RequiredFieldsException(APIException):
    status_code = 400
    default_code = 'require_fields'

    def __init__(self, field: str):
        self.default_detail = {field: '必須項目です。'}
        super().__init__(self.default_detail)


class RequestTimeoutException(APIException):
    status_code = 408
    default_code = 'request_timeout'
    default_detail = 'サーバーエラーが発生しました。しばらく時間を置いてから再度お試しいただくか、サポートまでお問い合わせください。'


class LoginIncorrectException(APIException):
    status_code = 403
    default_code = 'login_incorrect'
    default_detail = 'ログイン情報が間違っています。'


class ScheduledEmailNotExistException(APIException):
    status_code = 400
    default_detail = '関連する配信メールが存在しません。メールの送信元までお問い合わせください。'
    default_code = 'scheduled_email_is_not_exist'


class StartDateIsLessThanEndDateException(APIException):
    status_code = 400
    default_detail = '開始日には、終了日以前の日付を指定してください。'
    default_code = 'start_date_less_than_end_date'


class EndDateGreaterThanStartDateException(APIException):
    status_code = 400
    default_detail = '終了日には、開始日以降の日付を指定してください。'
    default_code = 'end_date_greater_than_start_date'
