from rest_framework.exceptions import APIException

class CommentImportantLimitException(APIException):
    status_code = 400
    default_detail = '固定できるコメントの上限は3件までです。'
    default_code = 'comment_important_limit'
