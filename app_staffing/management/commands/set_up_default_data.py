from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from django.core.exceptions import ObjectDoesNotExist

from app_staffing.models import Company, Tag

logger = getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('source_cid')

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        source_company = Company.objects.get(id=options['source_cid'])
        values = [
            '経理／財務',
            '人事／総務／広報',
            '法人営業',
            '一般事務／アシスタント',
            'カスタマーサポート／テクニカルサポート',
            'キッティング',
            'ヘルプデスク／サポートデスク',
            'ゲームプランナー',
            'マーケティング／プロモーション',
            'コンサルティング',
            'AWS',
            'Google Cloud Platform',
            'Azure',
            'Linux',
            'UNIX',
            'AIX',
            'Windows Server',
            'Oracle',
            'SQL Server',
            'Cisco',
            'JP1',
            'ネットワーク・監視',
            'Java',
            'PHP',
            'C/C++',
            'C#',
            'Objective-C',
            'Ruby',
            'C#.NET',
            'VB.NET',
            'VB',
            'VBA',
            'Python',
            'Perl',
            'Go',
            'Kotlin',
            'Swift',
            'Scala',
            'COBOL',
            'Unity',
            'R',
            'HTML/CSS',
            'JavaScript',
            'TypeScript',
            'Vue.js',
            'React.js',
            'iOS',
            'Android',
            'ActionScript',
            'Backbone.js',
            'CoffeeScript',
        ]

        tags = []
        for value in values:
            tags.append(
                Tag(
                    value=value,
                    internal_value=value.lower(),
                    company=source_company
                )
            )

        Tag.objects.bulk_create(tags)

        exit(0)
