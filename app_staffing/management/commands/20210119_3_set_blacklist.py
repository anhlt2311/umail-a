from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Organization

logger = getLogger(__name__)


class Command(BaseCommand):
    help = 'This command change organization category to client.'

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        organizations = Organization.objects.filter(is_blacklisted=True)
        logger.info(f'organization count : {len(organizations)}')
        for organization in organizations:
            logger.info(f'organization : {organization.id}')
            organization.category = 'blacklist'
            organization.save()
            logger.info(f'organization : {organization.id} saved')

        exit(0)
