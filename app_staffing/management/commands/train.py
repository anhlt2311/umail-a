from django.core.management.base import BaseCommand
from app_staffing.estimators.classifiers import EmailClassifier
from app_staffing.estimators.recommenders import JobEmailRecommender


class Command(BaseCommand):
    help = 'Train estimators with data.'

    def handle(self, *args, **options):
        estimators = [EmailClassifier(), JobEmailRecommender()]

        for estimator in estimators:
            estimator.batch_train()
            self.stdout.write(self.style.SUCCESS('Successfully trained an estimator "%s"' % estimator.__class__.__name__))

        self.stdout.write(self.style.SUCCESS('All estimator batch training has successful.'))
