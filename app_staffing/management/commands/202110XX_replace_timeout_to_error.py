from logging import getLogger
from django.core.management import BaseCommand

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import ScheduledEmail

logger = getLogger(__name__)


class Command(BaseCommand):
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        # 状況不明(scheduled_email_status_id=6)に分類されているレコードを全件取得する
        scheduled_emails = ScheduledEmail.objects.filter(scheduled_email_status_id=6)
        total = len(scheduled_emails)
        logger.info(f'total scheduled email count : {total}')

        count = 0
        for scheduled_email in scheduled_emails:
            logger.info(f'scheduled_email : {scheduled_email.id}')
            # ステータスをエラー(scheduled_email_status_id=5)に上書きする
            scheduled_email.scheduled_email_status_id = 5
            scheduled_email.save()
            logger.info(f'scheduled_email : {scheduled_email.id} saved')
            count = count + 1
            logger.info(f'count: {count} / {total}')

        exit(0)
