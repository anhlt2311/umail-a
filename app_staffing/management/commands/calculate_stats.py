from django.core.management import BaseCommand

from app_staffing.models import Company
from app_staffing.utils.timestamp import LogExecutionTime
from app_staffing.services.stats import (
    ContactStats,
    OrganizationStats,
    ScheduledEmailStats,
    SharedEmailStats,
    StaffInChargeStats,
)

from django.conf import settings

import logging
from app_staffing.utils.logger import use_cloud_logging
use_cloud_logging()

class Command(BaseCommand):
    help = 'This command execute heavy query and saving its result to the database.'

    @LogExecutionTime(logger=logging)
    def handle(self, *args, **options):
        # Execute methods that is being wrapped by a cache decorator.
        logging.info('Executing heavy calculations and saving its result to the database...')
        companies = Company.objects.only('id')
        for company in companies:
            try:
                ContactStats(company_id=company.id).calc_stats(None)
                OrganizationStats(company_id=company.id).calc_stats(None)
                ScheduledEmailStats(company_id=company.id).calc_stats(None)
                SharedEmailStats(company_id=company.id).calc_stats(None)
                StaffInChargeStats(company_id=company.id).calc_stats(None)
            except Exception as e:
                logging.error('calculate stats: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, e))
        exit(0)
