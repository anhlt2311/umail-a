from logging import getLogger
from django.core.management import BaseCommand

from app_staffing.models import Company
from app_staffing.utils.timestamp import LogExecutionTime
from app_staffing.services.stats import (
    EmailTrendStats,
    EmailRankingStats,
    EmailReplyTrendStats,
    ContactStats,
    OrganizationStats,
    ScheduledEmailStats,
    SharedEmailStats,
    ReceiveEmailStats,
)
from google.cloud.logging.handlers import ContainerEngineHandler

import sys

from django.conf import settings

logger = getLogger(__name__)
logger.propagate = False
logger.addHandler(ContainerEngineHandler(stream=sys.stdout))


class Command(BaseCommand):
    help = 'This command execute heavy query and cache its result to the database.'

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        exit(0)
