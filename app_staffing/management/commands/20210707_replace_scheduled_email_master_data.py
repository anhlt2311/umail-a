from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import ScheduledEmail, ScheduledEmailStatus, ScheduledEmailSendType

logger = getLogger(__name__)


class Command(BaseCommand):
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        scheduled_email_statuses = ScheduledEmailStatus.objects.all()
        scheduled_email_status_dict = {}
        for scheduled_email_status in scheduled_email_statuses:
            scheduled_email_status_dict[scheduled_email_status.name] = scheduled_email_status
        
        scheduled_email_send_types = ScheduledEmailSendType.objects.all()
        scheduled_email_send_type_dict = {}
        for scheduled_email_send_type in scheduled_email_send_types:
            scheduled_email_send_type_dict[scheduled_email_send_type.name] = scheduled_email_send_type

        scheduled_emails = ScheduledEmail.objects.all()
        total = len(scheduled_emails)
        logger.info(f'total scheduled email count : {total}')

        count = 0
        for scheduled_email in scheduled_emails:
            logger.info(f'scheduled_email : {scheduled_email.id}')
            logger.info(f'scheduled_email status : {scheduled_email.status}, send_type: {scheduled_email.send_type}')

            changed = False
            if scheduled_email.status in scheduled_email_status_dict:
                scheduled_email.scheduled_email_status = scheduled_email_status_dict[scheduled_email.status]
                logger.info(f'scheduled_email scheduled_email_status : {scheduled_email.scheduled_email_status}')
                changed = True

            if scheduled_email.send_type in scheduled_email_send_type_dict:
                scheduled_email.scheduled_email_send_type = scheduled_email_send_type_dict[scheduled_email.send_type]
                logger.info(f'scheduled_email scheduled_email_send_type : {scheduled_email.scheduled_email_send_type}')
                changed = True

            if changed:
                scheduled_email.save()
                logger.info(f'scheduled_email : {scheduled_email.id} saved')
                count = count + 1
                logger.info(f'count: {count} / {total}')

        exit(0)
