from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Organization

logger = getLogger(__name__)


class Command(BaseCommand):
    help = 'This command set contact last name.'

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        organizations = Organization.objects.filter(establishment_date__isnull=False)
        total = len(organizations)
        logger.info(f'total organization count : {total}')

        count = 0
        for organization in organizations:
            logger.info(f'organization : {organization.id}')
            logger.info(f'organization establishment date: {organization.establishment_date}')

            replaced_establishment_date = organization.establishment_date.replace(day=1)
            logger.info(f'organization replaced establishment date: {replaced_establishment_date}')

            organization.establishment_date = replaced_establishment_date
            organization.save()

            logger.info(f'organization : {organization.id} saved')
            count = count + 1
            logger.info(f'count: {count} / {total}')

        exit(0)
