from logging import getLogger
from django.core.management import BaseCommand

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Company, Addon, ScheduledEmailSetting
from django.conf import settings

logger = getLogger(__name__)


class Command(BaseCommand):
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        company_ids = ['0f78dc15-186e-4adb-87c5-6ba4187d8e66', '44a5720a-b3d4-4871-8efa-1e5ca7e7ffec']
        companies = Company.objects.filter(id__in=company_ids)
        for company in companies:
            purchased = Addon.objects.filter(company=company, addon_master_id=settings.REMOVE_PROMOTION_ADDON_MASTER_ID).exists()
            logger.info(f'company : {company.id} purchase {purchased}')
            if not purchased:
                Addon.objects.create(company=company, addon_master_id=settings.REMOVE_PROMOTION_ADDON_MASTER_ID)

        scheduled_email_settings = ScheduledEmailSetting.objects.filter(company_id__in=company_ids)
        for scheduled_email_setting in scheduled_email_settings:
            logger.info(f'company : {scheduled_email_setting.company_id} use_remove_promotion {scheduled_email_setting.use_remove_promotion}')
            scheduled_email_setting.use_remove_promotion = True
            scheduled_email_setting.save()

        exit(0)
