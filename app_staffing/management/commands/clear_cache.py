from logging import getLogger
from django.core.management import BaseCommand
from django.core.cache import cache

from app_staffing.utils.timestamp import LogExecutionTime

logger = getLogger(__name__)

CLEAR_CACHE_PREFIX = 'stats'
CLEAR_CACHE_VERSION = 1

class Command(BaseCommand):
    help = 'This command clear cached data from the database.'

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        exit(0)
