from logging import getLogger

from django.core.management import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from django.db.transaction import atomic

from app_staffing.models import Contact, Company, User

logger = getLogger(__name__)

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('source_cid')
        parser.add_argument('dest_cid')


    def handle(self, *args, **options):
        source_cid = options['source_cid']
        dest_cid = options['dest_cid']

        try:
            source_company = Company.objects.get(id=source_cid)
            destination_company = Company.objects.get(id=dest_cid)
        except ObjectDoesNotExist:
            logger.error(msg='The company you specified is not found.')
            exit(1)

        c1_contacts = Contact.objects.filter(company=source_company)
        c2_contacts = Contact.objects.filter(company=destination_company)
        c1_users = User.objects.filter(company=source_company)
        c2_users = User.objects.filter(company=destination_company)
        for c1_c in c1_contacts:
            if c1_c.staff is not None:
                for c2_c in c2_contacts:
                    if (c1_c.display_name == c2_c.display_name) and (c1_c.organization.name == c2_c.organization.name):
                        for c1_u in c1_users:
                            c1_name = c1_u.last_name + c1_u.first_name
                            c1_c_name = c1_c.staff.last_name + c1_c.staff.first_name
                            if c1_name == c1_c_name:
                                for c2_u in c2_users:
                                    c2_name = c2_u.last_name + c2_u.first_name
                                    if c1_name == c2_name:
                                        c2_c.staff = c2_u
                                        c2_c.save()
        exit(0)
