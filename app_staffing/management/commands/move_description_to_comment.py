from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Contact, Organization, ContactComment, OrganizationComment

logger = getLogger(__name__)


class Command(BaseCommand):
    help = 'This command moves description to comment.'

    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        contacts = Contact.objects.all()
        logger.info(f'contact count : {len(contacts)}')
        for contact in contacts:
            logger.info(f'contact : {contact.id}')
            if contact.description == '':
                logger.info(f'contact : {contact.id} skip')
                continue

            contact_comment = ContactComment(
                created_time=timezone.now(),
                modified_time=timezone.now(),
                content=contact.description,
                contact_id=contact.id,
                created_user_id=contact.created_user_id,
                modified_user_id=contact.modified_user_id,
                company_id=contact.company_id,
                is_important=False
            )
            contact_comment.save()
            logger.info(f'contact : {contact.id} saved')

        organizations = Organization.objects.all()
        logger.info(f'organization count : {len(organizations)}')
        for organization in organizations:
            logger.info(f'organization : {organization.id}')
            if organization.description == '':
                logger.info(f'organization : {organization.id} skip')
                continue

            organization_comment = OrganizationComment(
                created_time=timezone.now(),
                modified_time=timezone.now(),
                content=organization.description,
                organization_id=organization.id,
                created_user_id=organization.created_user_id,
                modified_user_id=organization.modified_user_id,
                company_id=organization.company_id,
                is_important=False
            )
            organization_comment.save()
            logger.info(f'organization : {organization.id} saved')

        exit(0)
