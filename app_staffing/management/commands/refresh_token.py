from logging import getLogger

from django.core.management.base import BaseCommand
from rest_framework.authtoken.models import Token
from app_staffing.utils.timestamp import LogExecutionTime
from google.cloud.logging.handlers import ContainerEngineHandler

import sys
logger = getLogger(__name__)
logger.propagate = False



class Command(BaseCommand):
    help = 'Refrensh tokens of non-staff users'

    logger.addHandler(ContainerEngineHandler(stream=sys.stdout))
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        logger.addHandler(ContainerEngineHandler(stream=sys.stdout))
        logger.info(f'Updating tokens of non-staff users...')
        tokens = Token.objects.filter(user__is_staff=False)
        for token in tokens:
            logger.addHandler(ContainerEngineHandler(stream=sys.stdout))
            logger.info(f'Updating authentication token of the user: {token.user.username}')
            user_id = token.user_id
            token.delete()
            Token.objects.create(user_id=user_id)
        logger.addHandler(ContainerEngineHandler(stream=sys.stdout))
        logger.info(f'Updated {len(tokens)} tokens.')

        exit(0)
