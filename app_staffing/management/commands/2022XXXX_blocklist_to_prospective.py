from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Organization

logger = getLogger(__name__)


class Command(BaseCommand):
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        # organization_category_id=5(ブロックリスト)の取引先を取得
        organizations = Organization.objects.filter(organization_category_id=5)
        logger.info(f'organization count : {len(organizations)}')
        for organization in organizations:
            logger.info(f'organization : {organization.id}')
            # organization_category_id=1(見込み客)で上書き
            organization.organization_category_id = 1
            # is_blacklisted(ブラックリストフラグ)を立てる
            organization.is_blacklisted = True
            organization.save()
            logger.info(f'organization : {organization.id} saved')

        exit(0)
