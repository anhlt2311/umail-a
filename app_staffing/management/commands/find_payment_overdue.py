from django.core.management import BaseCommand
from django.conf import settings
from django.db.models import Prefetch
from app_staffing.models import Company, Plan
from datetime import datetime, timedelta
from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.utils.logger import get_info_logger

import pytz
import logging
from app_staffing.utils.logger import use_cloud_logging
use_cloud_logging()

class Command(BaseCommand):
    @LogExecutionTime(logger=logging)
    def handle(self, *args, **options):
        today = datetime.now(pytz.timezone(settings.TIME_ZONE)).date()
        payment_limit_date_with_postponement = today - timedelta(days=settings.PLAN_EXPIRE_POSTPONEMENT_DAYS)
        # 退会済みのテナントは除外する
        plans = Plan.objects.filter(is_active=True, payment_date__lte=payment_limit_date_with_postponement, company__deactivated_time__isnull=True)
        
        if len(plans):
            logging.info('payment_overdue_info : Companies: {0}'.format(
                ",".join([str(p.company) for p in plans]),
            ))

        exit(0)
