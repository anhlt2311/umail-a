import pytz
import sys
from logging import getLogger
from google.cloud.logging.handlers import ContainerEngineHandler

from django.conf import settings
from datetime import timedelta, datetime

from app_staffing.models.organization import CompanyAttribute
from app_staffing.management.commands.delete_account_data import Command as RemoveAccountCommand


logger = getLogger(__name__)
logger.propagate = False
logger.addHandler(ContainerEngineHandler(stream=sys.stdout))


class Command(RemoveAccountCommand):
    def handle(self, *args, **options):
        from_date = datetime.now(pytz.timezone(settings.TIME_ZONE)) - timedelta(days=settings.TRIAL_ACCOUNT_DELETE_REMAINING_DAY)
        company_ids = CompanyAttribute.objects.filter(trial_expiration_date__lte=from_date).values_list('company_id', flat=True)
        self.remove_user_with_company_ids(company_ids, 'trial user')
        exit(0)
