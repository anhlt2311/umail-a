from logging import getLogger
from django.core.management import BaseCommand
from django.utils import timezone

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import User, UserRole

logger = getLogger(__name__)


class Command(BaseCommand):
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        user_roles = UserRole.objects.all()
        user_role_dict = {}
        for user_role in user_roles:
            user_role_dict[user_role.name] = user_role

        users = User.objects.filter(role__isnull=False)
        total = len(users)
        logger.info(f'total user count : {total}')

        count = 0
        for user in users:
            logger.info(f'user : {user.id}')
            logger.info(f'user role : {user.role}')

            if user.role in user_role_dict:
                user.user_role = user_role_dict[user.role]
                logger.info(f'user user_role : {user.user_role}')
                user.save()
                logger.info(f'user : {user.id} saved')
                count = count + 1
                logger.info(f'count: {count} / {total}')

        exit(0)
