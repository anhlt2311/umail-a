import json
import logging
import time
from datetime import date, datetime

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.management import BaseCommand
from django.db import connection, transaction
from django.db.models import Prefetch

from app_staffing.models.email import Email, EmailAttachment, EmailComment, SharedEmailTransferHistory
from app_staffing.models.organization import Company
from app_staffing.utils.email import get_due_date_of_shared_email
from app_staffing.utils.logger import use_cloud_logging

use_cloud_logging()


class Command(BaseCommand):
    help = 'This command upload old shared email to Storage and delete them'

    @staticmethod
    def generate_upload_path(path_key, company_id, index):
        """
        Generate file path
        :return: str - A relative file path from MEDIA_ROOT.
        """
        return f'{settings.SHARED_MAIL_DIR}/{str(date.today())}/{company_id}/{path_key}({index}).json'

    def handle(self, *args, **options):
        logging.info('Upload old shared email to Storage...')
        due_date = get_due_date_of_shared_email()
        path_key = int(time.time())

        # prepare queryset for prefetch related, for main query
        attachment_qs = EmailAttachment.objects.select_related('company', 'created_user', 'modified_user')
        email_comment_qs = EmailComment.objects.select_related('company', 'created_user', 'modified_user')
        shared_history_qs = SharedEmailTransferHistory.objects.select_related('sender', 'receiver')

        company_qs = Company.objects.all()
        for company in company_qs:
            start = time.time()
            logging.info(f'Start process upload for company {company.id}')

            # Queryset - shared email of current company
            shared_email_queryset = Email.objects.filter(
                shared=True,
                sent_date__lt=due_date,
                company=company,
            ).select_related(
                'company', 'created_user', 'modified_user', 'staff_in_charge',
            ).prefetch_related(
                Prefetch('attachments', queryset=attachment_qs),
                Prefetch('emailcomment_set', queryset=email_comment_qs),
                Prefetch('shared_history', queryset=shared_history_qs),
            )

            company_id = str(company.id)
            count_of_email = shared_email_queryset.count()

            # upload json to storage and delete Shared Email
            try:
                count, index = 0, 0
                offset, limit = 0, settings.SHARED_EMAIL_JSON_FILE_MAX_COUNT
                while count < count_of_email:
                    emails = [item.to_dictionary() for item in shared_email_queryset[offset:limit]]
                    count, index = count + len(emails), index + 1
                    json_string = json.dumps({
                        'upload_at': str(datetime.now()),
                        'count': len(emails),
                        'items': emails,
                    })

                    default_storage.save(
                        self.generate_upload_path(path_key, company_id, index),
                        ContentFile(json_string),
                    )

                    del emails, json_string
                    offset, limit = limit, limit + settings.SHARED_EMAIL_JSON_FILE_MAX_COUNT

                with transaction.atomic():
                    self.perform_query_to_delete_shared_email(due_date, company_id)
            except Exception as ex:
                logging.error(f'Upload Email - Exception occurred for company={company_id}, ErrorDetails: {ex}')

            del shared_email_queryset
            logging.info(f'It took {int(time.time() - start)} seconds, with total item is {count_of_email}')

        return str(path_key)

    @staticmethod
    def perform_query_to_delete_shared_email(due_date, company_id):
        format_id = 'id::text'
        format_boolean = 'TRUE'
        if 'sqlite3' in settings.DATABASES['default']['ENGINE']:
            company_id = company_id.replace('-', '')
            format_id = 'id'
            format_boolean = '1'
        shared_email_condition = f'shared = {format_boolean} AND sent_date < \'{due_date}\' AND company_id = \'{company_id}\''
        select_email_id = f'SELECT id FROM app_staffing_email WHERE {shared_email_condition}'
        select_message_id = f'SELECT message_id FROM app_staffing_email WHERE {shared_email_condition}'

        params = {
            'format_id': format_id,
            'shared_email_condition': shared_email_condition,
            'select_email_id': select_email_id,
            'select_message_id': select_message_id,
            'format_boolean': format_boolean,
            'due_date': due_date,
            'company_id': company_id,
        }
        with connection.cursor() as cursor:
            Command.delete_soft_delete_record(cursor, **params)
            Command.delete_django_mailbox(cursor, **params)
            Command.delete_email(cursor, **params)

    @staticmethod
    def delete_soft_delete_record(cursor, **kwargs):
        format_id = kwargs.get("format_id")
        shared_email_condition = kwargs.get("shared_email_condition")
        select_email_id = kwargs.get("select_email_id")

        select_content_type = 'SELECT id FROM django_content_type ' \
                              'WHERE app_label = \'app_staffing\' AND model = \'{model}\''
        base_sql_delete_soft_record = 'DELETE FROM softdelete_softdeleterecord ' \
                                      'WHERE content_type_id IN ({content_type}) ' \
                                      '      AND object_id IN ({object_id});'
        base_sql_delete_changeset = 'DELETE FROM softdelete_changeset ' \
                                    'WHERE content_type_id IN ({content_type}) ' \
                                    '      AND object_id IN ({object_id});'
        delete_soft_model = [
            ('email',), ('emailattachment', 'app_staffing_emailattachment',),
            ('emailcomment', 'app_staffing_emailcomment',),
            ('sharedemailtransferhistory', 'app_staffing_sharedemailtransferhistory',)
        ]

        for item in delete_soft_model:
            if len(item) == 1:
                object_id = f'SELECT {format_id} FROM app_staffing_email WHERE {shared_email_condition}'
            else:
                object_id = f'SELECT {format_id} FROM {item[1]} WHERE email_id IN ({select_email_id})'

            params = {'content_type': select_content_type.format(model=item[0]), 'object_id': object_id}
            cursor.execute(base_sql_delete_soft_record.format(**params))
            cursor.execute(base_sql_delete_changeset.format(**params))

    @staticmethod
    def delete_django_mailbox(cursor, **kwargs):
        select_message_id = kwargs.get('select_message_id')

        cursor.execute(f'DELETE FROM django_mailbox_messageattachment '
                       f'WHERE message_id IN (SELECT id FROM django_mailbox_message '
                       f'             WHERE message_id IN ({select_message_id}));')
        cursor.execute(f'DELETE FROM django_mailbox_message WHERE message_id IN ({select_message_id});')

    @staticmethod
    def delete_email(cursor, **kwargs):
        select_email_id = kwargs.get('select_email_id')
        format_boolean = kwargs.get('format_boolean')
        due_date = kwargs.get('due_date')
        company_id = kwargs.get('company_id')

        cursor.execute(f'DELETE FROM app_staffing_emailattachment WHERE email_id IN ({select_email_id});')
        cursor.execute(f'DELETE FROM app_staffing_emailcomment '
                       f'WHERE parent_id IS NOT NULL AND email_id IN ({select_email_id});')
        cursor.execute(f'DELETE FROM app_staffing_emailcomment '
                       f'WHERE parent_id IS NULL AND email_id IN ({select_email_id});')
        cursor.execute(f'DELETE FROM app_staffing_sharedemailtransferhistory '
                       f'WHERE email_id IN ({select_email_id});')
        cursor.execute(f'DELETE FROM app_staffing_email '
                       f'WHERE shared = {format_boolean} AND sent_date < \'{due_date}\' AND company_id = \'{company_id}\';')
