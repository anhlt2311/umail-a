from logging import getLogger
from django.core.management import BaseCommand

from app_staffing.utils.timestamp import LogExecutionTime

from app_staffing.models import Company, Addon
from django.conf import settings

logger = getLogger(__name__)


class Command(BaseCommand):
    @LogExecutionTime(logger=logger)
    def handle(self, *args, **options):
        companies = Company.objects.filter(id__in=['0f78dc15-186e-4adb-87c5-6ba4187d8e66', '44a5720a-b3d4-4871-8efa-1e5ca7e7ffec'])
        for company in companies:
            purchased_count = Addon.objects.filter(company=company, addon_master_id=settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID).count()
            purchase_count = 4 - purchased_count

            logger.info(f'company : {company.id} purchase {purchase_count}')

            for i in range(0, purchase_count):
                Addon.objects.create(company=company, addon_master_id=settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID)

        exit(0)
