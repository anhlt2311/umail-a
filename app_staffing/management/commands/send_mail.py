from django.utils import timezone
from datetime import timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from app_staffing.models import ScheduledEmail, SMTPServer, MailboxMapping
from app_staffing.models.email import QUEUED, SENDING, SENT, ERROR, ScheduledEmailStatus, ScheduledEmailSetting, ScheduledEmailSendError
from app_staffing.models.addon import Addon
from app_staffing.models.organization import Company
from app_staffing.services.email.send_mail import send_mails, put_mails
from app_staffing.utils.timestamp import LogExecutionTime
import asyncio
from django.core.mail import get_connection
import os

from app_staffing.utils.logger import use_cloud_logging
use_cloud_logging()

import logging

class Command(BaseCommand):
    help = 'This command sends scheduled emails which are queued and reached the scheduled time.'

    def _get_scheduled_email_status_dict(self):
        scheduled_email_statuses = ScheduledEmailStatus.objects.all()
        scheduled_email_status_dict = {}
        for scheduled_email_status in scheduled_email_statuses:
            scheduled_email_status_dict[scheduled_email_status.name] = scheduled_email_status
        return scheduled_email_status_dict

    async def _send_mail_task(self, company_id, instances, shared_email_address, scheduled_email_setting, is_open_count_available, is_use_remove_promotion_available):
        logging.info('Company: {0}, {1} scheduled mails to send.'.format(company_id, len(instances)))
        errors = []

        scheduled_email_status_dict = self._get_scheduled_email_status_dict()
        for instance in instances:
            try:
                instance.scheduled_email_status = scheduled_email_status_dict[SENDING]  # Change the status to SENDING first, to avoid infinite sending.
                instance.save()

                loop = asyncio.get_event_loop()

                result = await loop.run_in_executor(None, send_mails, instance, shared_email_address, scheduled_email_setting, is_open_count_available, is_use_remove_promotion_available)

                # After sending the email, change its status and timestamp.
                instance.sent_date = timezone.now()

                instance.send_total_count = result['send_total_count']

                instance.scheduled_email_status = scheduled_email_status_dict[SENT]

                instance.save()
            except Exception as e:
                logging.error('send mail: Company: {0}, ScheduledMail: {1}, Exception occurred. ErrorDetails:{2}'.format(company_id, instance.id, e))
                instance.scheduled_email_status = scheduled_email_status_dict[ERROR]  # Change the status to ERROR if an error happens, to avoid infinite sending.
                instance.save()
                errors.append(e)
                continue

        has_error = False
        if errors:
            logging.error('Company: {0} Some scheduled mail was not sent successfully. Details:{1}'.format(company_id, errors))
            has_error = True
        else:
            logging.info('Company: {0} Operation finished.'.format(company_id))

        return { 'has_error': has_error, 'company_id': company_id }

    # Cloud Tasksに予約しておく
    def _put_mail_task(self, company_id, instances, shared_email_address, scheduled_email_setting, is_open_count_available, is_use_remove_promotion_available):
        logging.info('PutMail: Company: {0}, {1} scheduled mails to send.'.format(company_id, len(instances)))
        errors = []

        scheduled_email_status_dict = self._get_scheduled_email_status_dict()
        for instance in instances:
            try:
                result = put_mails(instance, shared_email_address, scheduled_email_setting, is_open_count_available, is_use_remove_promotion_available)

                if result['has_error']:
                    instance.scheduled_email_status = scheduled_email_status_dict[ERROR]
                    errors.append("CreatingTaskError")
                instance.status = 'tasks'
                instance.save()

            except Exception as e:
                logging.error('put mail: Company: {0}, ScheduledMail: {1}, Exception occurred. ErrorDetails:{2}'.format(company_id, instance.id, e))
                instance.scheduled_email_status = scheduled_email_status_dict[ERROR]  # Change the status to ERROR if an error happens, to avoid infinite sending.
                instance.save()
                errors.append(e)
                continue

        has_error = False
        if errors:
            logging.error('Company: {0} Some scheduled mail was not sent successfully. Details:{1}'.format(company_id, errors))
            has_error = True
        else:
            logging.info('Company: {0} Operation finished.'.format(company_id))

        return { 'has_error': has_error, 'company_id': company_id }

    # ToDo: 今後実装
    # Cloud Functionまで疎通できるか事前にチェック
    def _health_check_for_cloud_task(self):
        return True

    def _sending_method(self):
        return settings.SENDING_MAIL_METHOD

    @LogExecutionTime(logger=logging)
    def handle(self, *args, **options):
        start_time = timezone.now()

        sending_method = self._sending_method()

        # Timeout scheduled emails that have a status SENDING and stuck on that status for enough time.
        stuck_mails = ScheduledEmail.objects.filter(
            scheduled_email_status__name=SENDING,
            date_to_send__lte=start_time - timedelta(minutes=settings.EMAIL_SENDING_STATUS_TIMEOUT_MINUTES)
        )

        scheduled_email_status_dict = self._get_scheduled_email_status_dict()
        for instance in stuck_mails:
            instance.scheduled_email_status = scheduled_email_status_dict[ERROR]
            instance.save()

        if sending_method == 'CloudTask':
            # 配信時刻２０分前のデーター(過去データ含め)全てをCloudTaskにあらかじめ入れておく
            target_scheduled_mails = ScheduledEmail.objects.filter(
                scheduled_email_status__name=QUEUED,
                date_to_send__lte=start_time + timedelta(minutes=20)
            ).exclude(status="tasks")

        else:
            target_scheduled_mails = ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED, date_to_send__lte=start_time).order_by('date_to_send')


        logging.info(f'Found {target_scheduled_mails.count()} scheduled mails to send.')

        scheduled_mails_group_by_company = {}
        for instance in target_scheduled_mails:
            if instance.company_id not in scheduled_mails_group_by_company:
                scheduled_mails_group_by_company[instance.company_id] = [instance]
            else:
                scheduled_mails_group_by_company[instance.company_id].append(instance)

        # companyに紐づくMailboxの情報を取得して、共有メールアドレスを取ってくる
        shared_email_address_group_by_company = {}
        company_ids = scheduled_mails_group_by_company.keys()
        for mailboxmapping in MailboxMapping.objects.select_related('mailbox').filter(company__in=company_ids):
            shared_email_address_group_by_company[mailboxmapping.company_id] = mailboxmapping.mailbox.from_email

        # companyに紐づくScheduledEmailSettingから開封率を使用できるかの判定結果を取得する
        scheduled_email_settings_group_by_company = {}
        for setting in ScheduledEmailSetting.objects.filter(company_id__in=company_ids):
            scheduled_email_settings_group_by_company[setting.company_id] = setting

        # companyに紐づくAddonから開封率のアドオンが開放されているかの判定結果を取得する
        open_count_addons_group_by_company = {}
        for addon in Addon.objects.filter(company_id__in=company_ids, addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID):
            open_count_addons_group_by_company[addon.company_id] = addon

        # companyに紐づくAddonから広告除去のアドオンが開放されているかの判定結果を取得する
        remove_promotion_addons_group_by_company = {}
        for addon in Addon.objects.filter(company_id__in=company_ids, addon_master_id=settings.REMOVE_PROMOTION_ADDON_MASTER_ID):
            remove_promotion_addons_group_by_company[addon.company_id] = addon

        # companyからテナントが解約されているかを判定する
        companies_group_by_id = {}
        for company in Company.objects.filter(id__in=company_ids):
            companies_group_by_id[company.id] = company

        if sending_method == 'SMTP':
            loop = asyncio.get_event_loop()

        task_list = []
        task_results = []
        for company_id, instances in scheduled_mails_group_by_company.items():
            # 解約されているテナントのメールは処理対象外
            company = companies_group_by_id.get(company_id)
            if company and company.deactivated_time:
                continue

            shared_email_address = shared_email_address_group_by_company.get(company_id)
            scheduled_email_setting = scheduled_email_settings_group_by_company.get(company_id)
            is_open_count_available = open_count_addons_group_by_company.get(company_id)
            is_use_remove_promotion_available = remove_promotion_addons_group_by_company.get(company_id)

            if sending_method == 'CloudTask':
                result = self._put_mail_task(company_id, instances, shared_email_address, scheduled_email_setting, is_open_count_available, is_use_remove_promotion_available)
                task_results.append(result)
            else:
                task_list.append(self._send_mail_task(company_id, instances, shared_email_address, scheduled_email_setting, is_open_count_available, is_use_remove_promotion_available))

        if sending_method == 'SMTP':
            task_results = loop.run_until_complete(asyncio.gather(*task_list))

        for task_result in task_results:
            if task_result['has_error']:
                logging.error('Company: {0} has error in task_results.'.format(task_result['company_id']))

        exit(0)
