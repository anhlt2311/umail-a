from logging import getLogger

from django.core.management import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from django.db.transaction import atomic

from app_staffing.models import Contact, Company, User

class Command(BaseCommand):

    def handle(self, *args, **options):

        company = Company.objects.all()
        for c in company:
            print (str(c.id) + '/' + c.name)

        exit(0)
