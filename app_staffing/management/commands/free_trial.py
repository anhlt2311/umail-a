from django.core.management import BaseCommand
from django.db.transaction import atomic

from app_staffing.models import Contact, CompanyAttribute, User

from datetime import timedelta, datetime
from django.core.exceptions import ObjectDoesNotExist
from dateutil.relativedelta import relativedelta
import pytz
from django.conf import settings

from app_staffing.models.user import MASTER
from django.template.loader import render_to_string

import logging
from app_staffing.utils.logger import use_cloud_logging
use_cloud_logging()


class Command(BaseCommand):

    def handle(self, *args, **options):
        from_date = datetime.now(pytz.timezone(settings.TIME_ZONE)) - timedelta(days=1)
        company_attributes = CompanyAttribute.objects.filter(trial_expiration_date=from_date)
        company_ids = [ca.company_id for ca in company_attributes]
        master_users = User.objects.filter(company_id__in=company_ids, user_role__name=MASTER)

        for master_user in master_users:
            try:
                context = {
                    'url': settings.HOST_URL,
                    'display_name': master_user.display_name
                }
                subject = render_to_string(settings.TEMPLATE_EMAIL_FREE_TRIAL_EXPIRED_SUBJECT, context)
                message = render_to_string(settings.TEMPLATE_EMAIL_FREE_TRIAL_EXPIRED_MESSAGE, context)
                master_user.email_user(subject, '', html_message=message)
            except Exception as e:
                logging.error('free trial expired notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(master_user.company_id, e))

        exit(0)
