from django.core.management.base import BaseCommand
from app_staffing.estimators.classifiers import EmailClassifier
from app_staffing.estimators.recommenders import JobEmailRecommender


class Command(BaseCommand):
    help = 'This command resets all data from remote estimators. Use this ONLY in a local / test environment.'

    def handle(self, *args, **options):
        estimators = [EmailClassifier(), JobEmailRecommender()]

        for estimator in estimators:
            estimator.reset()
            self.stdout.write(self.style.SUCCESS('Successfully reset data of estimator "%s"' % estimator.__class__.__name__))

        self.stdout.write(self.style.SUCCESS('All reset has successful.'))
