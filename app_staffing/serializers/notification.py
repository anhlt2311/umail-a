from rest_framework.serializers import ModelSerializer, SerializerMethodField

from app_staffing.models import EmailNotificationRule, EmailNotificationCondition, SystemNotification, UserSystemNotification
from app_staffing.serializers.base import AppResourceSerializer, MultiTenantFieldsMixin
from app_staffing.utils.custom_validation import custom_validation

def _create_related_conditions(rule, conditions_data):
    for i, data in enumerate(conditions_data):
        EmailNotificationCondition.objects.create(
            rule_id=rule.id,
            order=i,
            **data
        )


class EmailNotificationConditionSerializer(ModelSerializer):

    class Meta:
        model = EmailNotificationCondition
        fields = ('extraction_type', 'target_type', 'condition_type', 'value')


class EmailNotificationRuleSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    conditions = EmailNotificationConditionSerializer(many=True)

    def custom_validation(self, data):
        custom_validation(data, 'notification')

    class Meta:
        model = EmailNotificationRule
        fields = ('id', 'name', 'conditions', 'created_user', 'created_user__name', 'modified_user', 'modified_user__name',
                  'created_time', 'modified_time', 'company', 'master_rule', 'is_active')
        extra_kwargs = {'company': {'write_only': True}}

    def create(self, validated_data):
        conditions_data = validated_data.pop('conditions')
        instance = super().create(validated_data)
        _create_related_conditions(rule=instance, conditions_data=conditions_data)
        return instance

    def update(self, instance, validated_data):
        conditions_data = validated_data.pop('conditions')
        instance = super().update(instance, validated_data)

        # Clear all existing conditions first.
        if conditions_data is not None:  # Allow empty lists to clear all related conditions.
            EmailNotificationCondition.objects.filter(rule_id=instance.id).delete()
            # Then, replace them with new given conditions.
            _create_related_conditions(rule=instance, conditions_data=conditions_data)

        return instance


class SystemNotificationSerializer(ModelSerializer):
    is_checked = SerializerMethodField()

    def get_is_checked(self, obj):
        return (obj.id in obj.checkedusers.all().values_list('system_notification_id', flat=True))

    class Meta:
        model = SystemNotification
        fields = ('id', 'title', 'order', 'release_time', 'url', 'is_checked')
