import datetime
import json
import re

from attrdict import AttrDict
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.transaction import atomic
from django.forms.models import model_to_dict
from django.utils import dateformat
from localflavor.jp.forms import JPPostalCodeField
from rest_framework.exceptions import APIException
from rest_framework.serializers import (DateTimeField, DictField,
                                        ModelSerializer, SerializerMethodField,
                                        SlugRelatedField)

from app_staffing.exceptions.organization import CorporateNumberNotUniqueException
from app_staffing.models import (
    APPROACHED, CLIENT, CN, FROWN, HEART, HIGH, INFORMATION_EXCHANGED, JP, KR, LOW, MIDDLE,
    OTHER, PROSPECTIVE, SEMI_MIDDLE, VERY_HIGH, VERY_LOW, Company, Contact, ContactCC,
    ContactComment, DisplaySetting, ExceptionalOrganization, Location,
    Organization, OrganizationBranch, OrganizationCategory, OrganizationComment,
    OrganizationCountry, OrganizationEmployeeNumber, Tag, UserDisplaySetting)
from app_staffing.serializers.base import (AppResourceSerializer, BaseSubCommentSerializer,
                                           BaseCommentSerializer, MultiTenantFieldsMixin)
from app_staffing.serializers.factory.contact_category import edit_contact_category
from app_staffing.serializers.factory.contact_cc import assign_cc_addresses
from app_staffing.serializers.factory.contact_preference import (
    assign_jobskillpreference, assign_jobtypepreference,
    assign_personnelskillpreference, assign_personneltypepreference,
    assign_preference)
from app_staffing.serializers.factory.contact_score import edit_contact_score
from app_staffing.serializers.factory.contact_tags import assign_tags
from app_staffing.serializers.factory.organization_branch import assign_organization_branches
from app_staffing.serializers.helpers.representations import TIME_FORMAT_FOR_CSV
from app_staffing.serializers.preferences import (
    ContactPreferenceModelSerializer, ContactPreferenceSerializer,
    RequestContactJobSkillPreferenceSerializer,
    RequestContactJobTypePreferenceSerializer,
    RequestContactPersonnelSkillPreferenceSerializer,
    RequestContactPersonnelTypePreferenceSerializer,
    RequestContactPreferenceSerializer)
from app_staffing.utils.custom_validation import custom_validation
from app_staffing.utils.organization import get_fax_full, get_tel_full
from app_staffing.utils.utils import (deep_update, validate_fax_values, validate_tel_values)

CATEGORY_DISPLAY_NAME = {
    PROSPECTIVE: '見込み客',
    APPROACHED: 'アプローチ済',
    INFORMATION_EXCHANGED: '情報交換済',
    CLIENT: '契約実績有',
}

EMPLOYEE_NUMBER_DISPLAY_NAME = {
    VERY_LOW: '~10名',
    LOW: '11~30名',
    MIDDLE: '31~50名',
    SEMI_MIDDLE: '51~100名',
    HIGH: '101~300名',
    VERY_HIGH: '301名~',
}

COUNTRY_DISPLAY_NAME = {
    JP: '日本',
    CN: '中国',
    KR: '韓国',
    OTHER: 'その他',
}

CONTACT_CATEGORY_DISPLAY_NAME = {
    HEART: '良い',
    FROWN: '悪い',
}

TYPE_ITEMS = [
    'dev_designer', 'dev_front', 'dev_server', 'dev_pm', 'dev_other',
    'infra_server', 'infra_network', 'infra_security', 'infra_database', 'infra_sys', 'infra_other',
    'other_eigyo', 'other_kichi', 'other_support', 'other_other'
]
SKILL_ITEMS = [
    'dev_youken', 'dev_kihon', 'dev_syousai', 'dev_seizou', 'dev_test', 'dev_hosyu', 'dev_beginner',
    'infra_youken', 'infra_kihon', 'infra_syousai', 'infra_kouchiku', 'infra_test', 'infra_kanshi', 'infra_hosyu', 'infra_beginner'
]

JOB_SYOURYU_DISPLAY_NAME = {
    '1': 'なし',
    '2': '2次請まで',
    '3': '1次請まで',
    '4': 'エンド直・元請直まで',
}

PERSONNEL_SYOURYU_DISPLAY_NAME = {
    '1': 'なし',
    '2': '2社先所属まで',
    '3': '1社先所属まで',
    '4': '自社所属まで',
}


class LocationSerializer(ModelSerializer):
    name = JPPostalCodeField

    class Meta:
        model = Location
        fields = '__all__'


class CompanySerializer(ModelSerializer):
    id = SerializerMethodField()  # ReadOnly
    exceptional_organizations = SerializerMethodField()
    exceptional_organization_names = SerializerMethodField()
    modified_user__name = SerializerMethodField()

    def get_modified_user__name(self, obj):
        try:
            return obj.modified_user.display_name
        except AttributeError:
            return ''

    def custom_validation(self, data):
        custom_validation(data, 'company')

    def get_id(self, obj):
        return str(obj.id)

    def get_exceptional_organizations(self, obj):
        return list(ExceptionalOrganization.objects.filter(company=obj).order_by('organization__name').values_list('organization__id', flat=True))

    def get_exceptional_organization_names(self, obj):
        return list(ExceptionalOrganization.objects.filter(company=obj).order_by('organization__name').values_list('organization__name', flat=True))
    class Meta:
        model = Company
        fields = ('id', 'name', 'domain_name', 'address', 'building', 'capital_man_yen', 'establishment_date',
                  'settlement_month', 'has_haken', 'has_distribution', 'capital_man_yen_required_for_transactions',
                  'establishment_year', 'exceptional_organizations', 'exceptional_organization_names',
                  'has_p_mark_or_isms', 'has_invoice_system', 'p_mark_or_isms', 'invoice_system', 'haken', 'employee_number',
                  'modified_time', 'modified_user__name')


class OrganizationCommentSummarySerializer(AppResourceSerializer):
    created_time = SerializerMethodField()

    def get_created_time(self, obj):
        return dateformat.format(obj.created_time, settings.DATETIME_FORMAT_FOR_FRONT)

    class Meta:
        model = OrganizationComment
        fields = (
            'content',
            'created_time',
            'created_user__name',
            'modified_time',
            'modified_user__name',)


class OrganizationBranchSummarySerializer(AppResourceSerializer):
    class Meta:
        model = OrganizationBranch
        fields = ('name', 'address', 'building', 'tel1', 'tel2', 'tel3', 'fax1', 'fax2', 'fax3')


class OrganizationSummarySerializer(AppResourceSerializer):
    comments = OrganizationCommentSummarySerializer(many=True, read_only=True, source='organizationcomment_set')
    is_ignored = SerializerMethodField()
    country = SerializerMethodField()
    employee_number = SerializerMethodField()
    category = SerializerMethodField()
    branches = OrganizationBranchSummarySerializer(many=True, read_only=True,)

    def get_country(self, obj):
        return obj.organization_country_name

    def get_employee_number(self, obj):
        return obj.organization_employee_number_name

    def get_category(self, obj):
        return obj.organization_category_name

    def get_is_ignored(self, obj):
        company = obj.company

        # 「商流を抜ける」によって一覧に表示される場合はグレー表示にはしない
        is_ignored_by_has_distribution = obj.has_distribution != True

        if hasattr(obj, 'exceptional_organizations') and obj.id in [exceptional_organization.organization.id for exceptional_organization in obj.exceptional_organizations]:
            return False

        if company.capital_man_yen_required_for_transactions and obj.capital_man_yen and company.capital_man_yen_required_for_transactions > obj.capital_man_yen:
            return is_ignored_by_has_distribution

        if company.p_mark_or_isms == True and obj.has_p_mark_or_isms == False:
            return is_ignored_by_has_distribution

        if company.invoice_system == True and obj.has_invoice_system == False:
            return is_ignored_by_has_distribution

        if company.haken == True and obj.has_haken == False:
            return is_ignored_by_has_distribution

        if company.establishment_year and obj.establishment_date:
            base_date = datetime.datetime.now() - relativedelta(years=company.establishment_year)
            if obj.establishment_date > base_date.date():
                return is_ignored_by_has_distribution

        if obj.is_blacklisted:
            return True

        return False

    class Meta:
        model = Organization
        fields = ('id', 'corporate_number', 'name', 'category', 'employee_number', 'contract', 'country', 'address', 'building', 'capital_man_yen',
                  'capital_man_yen_required_for_transactions', 'establishment_year', 'score', 'domain_name',
                  'is_blacklisted', 'old_id', 'created_user', 'created_user__name', 'created_time', 'settlement_month',
                  'modified_user', 'modified_user__name', 'modified_time', 'comments', 'establishment_date', 'is_ignored',
                  'has_p_mark_or_isms', 'has_invoice_system', 'has_haken', 'has_distribution', 'p_mark_or_isms', 'invoice_system', 'haken',
                  'tel1', 'tel2', 'tel3', 'fax1', 'fax2', 'fax3', 'branches')


class OrganizationBranchNameSerializer(MultiTenantFieldsMixin, AppResourceSerializer):

    class Meta:
        model = Organization
        fields = ('id', 'name')


class OrganizationNameSummarySerializer(AppResourceSerializer):
    branches = OrganizationBranchNameSerializer(many=True, read_only=True)

    class Meta:
        model = Organization
        fields = ('id', 'name', 'branches', )


class OrganizationCsvSerializer(AppResourceSerializer):
    created_time = DateTimeField(format=TIME_FORMAT_FOR_CSV)
    modified_time = DateTimeField(format=TIME_FORMAT_FOR_CSV)
    tel_full = SerializerMethodField()
    fax_full = SerializerMethodField()
    organization_country__name = SerializerMethodField()
    organization_employee_number__name = SerializerMethodField()
    organization_category__name = SerializerMethodField()
    branch__name = SerializerMethodField()
    branch__address = SerializerMethodField()
    branch__building = SerializerMethodField()
    branch__tel_full = SerializerMethodField()
    branch__fax_full = SerializerMethodField()

    contract_int = SerializerMethodField()
    has_p_mark_or_isms_int = SerializerMethodField()
    has_invoice_system_int = SerializerMethodField()
    has_haken_int = SerializerMethodField()
    has_distribution_int = SerializerMethodField()
    p_mark_or_isms_int = SerializerMethodField()
    invoice_system_int = SerializerMethodField()
    haken_int = SerializerMethodField()
    is_blacklisted_int = SerializerMethodField()

    def get_tel_full(self, obj):
        return get_tel_full(obj)

    def get_fax_full(self, obj):
        return get_fax_full(obj)

    def get_organization_country__name(self, obj):
        return obj.organization_country_name

    def get_organization_employee_number__name(self, obj):
        return EMPLOYEE_NUMBER_DISPLAY_NAME.get(obj.organization_employee_number_name)

    def get_organization_category__name(self, obj):
        return CATEGORY_DISPLAY_NAME.get(obj.organization_category_name)

    def get_branch__name(self, obj):
        return ','.join([branch.name for branch in obj.branches.all()])

    def get_branch__address(self, obj):
        addresses = []
        for branch in obj.branches.all():
            if branch.address is not None and len(branch.address):
                addresses.append(branch.address)
        if len(addresses) >0:
            return ','.join(addresses)
        else:
            return ""

    def get_branch__building(self, obj):
        buildings = []
        for branch in obj.branches.all():
            if branch.building is not None and len(branch.building):
                buildings.append(branch.building)
        if len(buildings) >0:
            return ','.join(buildings)
        else:
            return ""

    def get_branch__tel_full(self, obj):
        tels = []
        for branch in obj.branches.all():
            if branch.tel1 is not None and len(branch.tel1) > 0 \
                    and branch.tel2 is not None and len(branch.tel2) > 0 \
                    and branch.tel3 is not None and len(branch.tel3) > 0:
                tels.append(get_tel_full(branch))
        if len(tels) > 0:
            return ','.join(tels)
        else:
            return ""

    def get_branch__fax_full(self, obj):
        faxes = []
        for branch in obj.branches.all():
            if branch.fax1 is not None and len(branch.fax1) > 0 \
                    and branch.fax2 is not None and len(branch.fax2) > 0 \
                    and branch.fax3 is not None and len(branch.fax3) > 0:
                faxes.append(get_fax_full(branch))
        if len(faxes) > 0:
            return ','.join(faxes)
        else:
            return ""

    def get_contract_int(self, obj):
        return None if obj.contract is None else int(obj.contract)

    def get_has_p_mark_or_isms_int(self, obj):
        return None if obj.has_p_mark_or_isms is None else int(obj.has_p_mark_or_isms)

    def get_has_invoice_system_int(self, obj):
        return None if obj.has_invoice_system is None else int(obj.has_invoice_system)

    def get_has_haken_int(self, obj):
        return None if obj.has_haken is None else int(obj.has_haken)

    def get_has_distribution_int(self, obj):
        return None if obj.has_distribution is None else int(obj.has_distribution)

    def get_p_mark_or_isms_int(self, obj):
        return None if obj.p_mark_or_isms is None else int(obj.p_mark_or_isms)

    def get_invoice_system_int(self, obj):
        return None if obj.invoice_system is None else int(obj.invoice_system)

    def get_haken_int(self, obj):
        return None if obj.haken is None else int(obj.haken)

    def get_is_blacklisted_int(self, obj):
        return None if obj.is_blacklisted is None else int(obj.is_blacklisted)

    class Meta:
        model = Organization
        fields = (
            'id', 'corporate_number', 'name', 'organization_category__name', 'score', 'organization_country__name',
            'establishment_date', 'settlement_month', 'address', 'building', 'tel_full', 'fax_full', 'domain_name',
            'organization_employee_number__name', 'contract', 'capital_man_yen', 'has_p_mark_or_isms', 'has_invoice_system',
            'has_haken', 'has_distribution', 'branch__name', 'branch__address', 'branch__building', 'branch__tel_full', 'branch__fax_full',
            'establishment_year', 'capital_man_yen_required_for_transactions',
            'p_mark_or_isms', 'invoice_system', 'haken', 'is_blacklisted', 'created_user__name', 'created_time',
            'modified_user__name', 'modified_time', 'contract_int', 'has_p_mark_or_isms_int', 'has_invoice_system_int', 'has_haken_int',
            'has_distribution_int', 'p_mark_or_isms_int', 'invoice_system_int', 'haken_int', 'is_blacklisted_int'
        )


class ContactSummarySerializer(ModelSerializer):
    class Meta:
        model = Contact
        fields = ('id', 'last_name', 'first_name', 'display_name', 'email', 'position', 'department', )


class OrganizationInitialCommentSerializer(AppResourceSerializer):
    class Meta:
        model = OrganizationComment
        fields = ('id', 'content', 'is_important')


class OrganizationBranchSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    class Meta:
        model = OrganizationBranch
        fields = ('id', 'organization', 'name', 'address', 'building', 'tel1', 'tel2', 'tel3', 'fax1', 'fax2', 'fax3')


class OrganizationSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    related_contacts = ContactSummarySerializer(many=True, read_only=True, source='contact_set')
    initial_comment = OrganizationInitialCommentSerializer(many=False, write_only=True, required=False)
    country = SerializerMethodField()
    employee_number = SerializerMethodField()
    category = SerializerMethodField()
    organization_branches = OrganizationBranchSerializer(many=True, read_only=True, source='branches')

    def get_country(self, obj):
        return obj.organization_country_name

    def get_employee_number(self, obj):
        return obj.organization_employee_number_name

    def get_category(self, obj):
        return obj.organization_category_name

    def corporate_number_exists(self, validated_data, instance=None):
        if not 'corporate_number' in validated_data or not validated_data['corporate_number']:
            return False

        if instance and instance.corporate_number == validated_data['corporate_number']:
            return False

        return Organization.objects.filter(
            corporate_number=validated_data['corporate_number'],
            company=self.context['request'].user.company
        ).exists()

    def create(self, validated_data):
        if 'country' in self.initial_data and self.initial_data['country']:
            selected_organization_country = OrganizationCountry.objects.get(name=self.initial_data['country'])
            validated_data['organization_country'] = selected_organization_country

        if 'employee_number' in self.initial_data and self.initial_data['employee_number']:
            selected_organization_employee_number = OrganizationEmployeeNumber.objects.get(name=self.initial_data['employee_number'])
            validated_data['organization_employee_number'] = selected_organization_employee_number

        if 'category' in self.initial_data and self.initial_data['category']:
            selected_organization_category = OrganizationCategory.objects.get(name=self.initial_data['category'])
            validated_data['organization_category'] = selected_organization_category

        if self.corporate_number_exists(validated_data):
            raise CorporateNumberNotUniqueException()

        validate_tel_values(validated_data, 'organization')

        validate_fax_values(validated_data, 'organization')

        comment = validated_data.pop('initial_comment', {})
        instance = super().create(validated_data)

        if comment and 'content' in comment and len(comment['content']) > 0:
            user = self.context['request'].user
            OrganizationComment.objects.create(
                organization = instance,
                company = user.company,
                content = comment.pop('content'),
                is_important = comment.pop('is_important', False),
                created_user = user,
                modified_user = user
            )

        if 'branches' in self.initial_data and self.initial_data['branches']:
            for d in self.initial_data['branches']:
                validate_tel_values(d, 'organization')
                validate_fax_values(d, 'organization')
            assign_organization_branches(instance, self.initial_data['branches'])

        return instance

    def update(self, instance, validated_data):
        if 'country' in self.initial_data and self.initial_data['country']:
            selected_organization_country = OrganizationCountry.objects.get(name=self.initial_data['country'])
            validated_data['organization_country'] = selected_organization_country

        if 'employee_number' in self.initial_data and self.initial_data['employee_number']:
            selected_organization_employee_number = OrganizationEmployeeNumber.objects.get(name=self.initial_data['employee_number'])
            validated_data['organization_employee_number'] = selected_organization_employee_number
        else:
            validated_data['organization_employee_number'] = None

        if 'category' in self.initial_data and self.initial_data['category']:
            selected_organization_category = OrganizationCategory.objects.get(name=self.initial_data['category'])
            validated_data['organization_category'] = selected_organization_category

        if self.corporate_number_exists(validated_data, instance):
            raise CorporateNumberNotUniqueException()

        validate_tel_values(validated_data, 'organization')

        validate_fax_values(validated_data, 'organization')

        if 'branches' in self.initial_data and self.initial_data['branches']:
            for d in self.initial_data['branches']:
                validate_tel_values(d, 'organization')
                validate_fax_values(d, 'organization')
            assign_organization_branches(instance, self.initial_data['branches'])

        updated_instance = super().update(instance, validated_data)
        return updated_instance

    def custom_validation(self, data):
        custom_validation(data, 'organization')

    class Meta:
        model = Organization
        fields = ('id', 'corporate_number', 'name', 'category', 'employee_number', 'contract', 'country', 'address', 'building', 'capital_man_yen',
                  'capital_man_yen_required_for_transactions', 'establishment_year', 'score', 'domain_name', 'settlement_month',
                  'is_blacklisted', 'old_id', 'created_user', 'created_user__name', 'created_time',
                  'modified_user', 'modified_user__name', 'modified_time', 'related_contacts', 'company', 'establishment_date', 'initial_comment',
                  'has_p_mark_or_isms', 'has_invoice_system', 'has_haken', 'has_distribution', 'p_mark_or_isms', 'invoice_system', 'haken', 'tel1', 'tel2', 'tel3', 'fax1', 'fax2', 'fax3', 'organization_branches')
        extra_kwargs = {'company': {'write_only': True}}

class OrganizationNameSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    class Meta:
        model = Organization
        fields = ('id', 'name')


class OrganizationCommentSerializer(BaseCommentSerializer):

    def custom_validation(self, data):
        custom_validation(data, 'comment')
    class Meta:
        model = OrganizationComment
        fields = ('id', 'created_user', 'created_user__name', 'created_user__avatar', 'created_time', 'modified_time', 'content', 'company',\
                'is_important', 'has_subcomment', 'deleted_at', 'total_sub_comment', 'parent', 'parent_content', \
                'parent_created_user_name', 'parent_modified_user_name', 'parent_created_time', 'parent_modified_time', "sub_comment_users_avatar")
        extra_kwargs = {'company': {'write_only': True}, 'sub_comment_users_avatar': {'read_only': True}, 'total_sub_comment': {'read_only': True}, 'parent': {'read_only': True}}


class OrganizationSubCommentSerializer(BaseSubCommentSerializer):
    class Meta:
        model = OrganizationComment
        fields = ('id', 'created_user', 'created_user__name', 'created_user__avatar', 'created_time', 'modified_time', 'content', 'company',\
                  'is_important', 'parent')
        extra_kwargs = {'company': {'write_only': True}, 'is_important': {'read_only': True}}



class ContactSerializer(AppResourceSerializer):
    staff__name = SerializerMethodField()
    organization__name = SerializerMethodField()
    organization__is_blacklisted = SerializerMethodField()

    def get_staff__name(self, obj):
        try:
            return obj.staff.display_name
        except AttributeError:
            return ' - '

    def get_organization__name(self, obj):
        return obj.organization.name

    def get_organization__is_blacklisted(self, obj):
        return obj.organization.is_blacklisted

    class Meta:
        model = Contact
        fields = (
            'id', 'last_name', 'first_name', 'display_name', 'email', 'position', 'department', 'organization', 'organization__name',
            'organization__is_blacklisted', 'staff', 'staff__name', 'last_visit',
            'created_user', 'created_user__name', 'created_time', 'modified_user', 'modified_user__name',
            'modified_time', 'old_id', 'tel1', 'tel2', 'tel3'
        )


class ContactFullCsvSerializer(AppResourceSerializer):
    created_time = DateTimeField(format=TIME_FORMAT_FOR_CSV)
    modified_time = DateTimeField(format=TIME_FORMAT_FOR_CSV)
    related_organization__name = SerializerMethodField()
    related_staff__email = SerializerMethodField()
    created_user__name = SerializerMethodField()
    modified_user__name = SerializerMethodField()
    tel_full = SerializerMethodField()
    cc = SerializerMethodField()
    tag = SerializerMethodField()
    category = SerializerMethodField()
    preference_types = SerializerMethodField()
    contactjobtypepreferences = SerializerMethodField()
    contactjobskillpreferences = SerializerMethodField()
    contactpersonneltypepreferences = SerializerMethodField()
    contactpersonnelskillpreferences = SerializerMethodField()
    contactpreference = ContactPreferenceSerializer(many=False, read_only=True)
    contactpreference_job_syouryu = SerializerMethodField()
    contactpreference_personnel_syouryu = SerializerMethodField()
    contactpreference_locations = SerializerMethodField()
    contactpreference_koyou = SerializerMethodField()

    def get_related_organization__name(self, obj):
        try:
            return obj.organization.name
        except AttributeError:
            return ''

    def get_related_staff__email(self, obj):
        try:
            return obj.staff.email
        except AttributeError:
            return ''

    def get_created_user__name(self, obj):
        try:
            return obj.created_user.display_name
        except AttributeError:
            return ''

    def get_modified_user__name(self, obj):
        try:
            return obj.modified_user.display_name
        except AttributeError:
            return ''

    def get_tel_full(self, obj):
        return get_tel_full(obj)

    def get_cc(self, obj):
        return ','.join([cc_address.email for cc_address in obj.cc_addresses.all()])

    def get_tag(self, obj):
        tag_values = []
        for tag_assignment in obj.tag_assignment.all():
            tag_values.append(tag_assignment.tag.value)
        return ','.join(tag_values)

    def get_category(self, obj):
        categories = []
        try:
            user_id = self.context['request'].user.id
        except Exception:
            user_id = None

        for contact_category in obj.categories.all():
            if user_id and contact_category and user_id == contact_category.user_id:
                categories.append(CONTACT_CATEGORY_DISPLAY_NAME.get(contact_category.category))
        return ','.join(categories)

    def get_preference_types(self, obj):
        preference_types = []
        if 0 < len(obj.contactjobtypepreferences.all()):
            preference_types.append('案件')
        if 0 < len(obj.contactpersonneltypepreferences.all()):
            preference_types.append('要員')
        return ','.join(preference_types)

    def get_contactjobtypepreferences(self, obj):
        contactjobtypepreferences = [c.type_preference.name for c in obj.contactjobtypepreferences.all()]
        type_dict = {}
        for type_item in TYPE_ITEMS:
            type_dict[type_item] = int(type_item in contactjobtypepreferences)
        return AttrDict(type_dict)

    def get_contactjobskillpreferences(self, obj):
        contactjobskillpreferences = [c.skill_preference.name for c in obj.contactjobskillpreferences.all()]
        skill_dict = {}
        for skill_item in SKILL_ITEMS:
            skill_dict[skill_item] = int(skill_item in contactjobskillpreferences)
        return AttrDict(skill_dict)

    def get_contactpersonneltypepreferences(self, obj):
        contactpersonneltypepreferences = [c.type_preference.name for c in obj.contactpersonneltypepreferences.all()]
        type_dict = {}
        for type_item in TYPE_ITEMS:
            type_dict[type_item] = int(type_item in contactpersonneltypepreferences)
        return AttrDict(type_dict)

    def get_contactpersonnelskillpreferences(self, obj):
        contactpersonnelskillpreferences = [c.skill_preference.name for c in obj.contactpersonnelskillpreferences.all()]
        skill_dict = {}
        for skill_item in SKILL_ITEMS:
            skill_dict[skill_item] = int(skill_item in contactpersonnelskillpreferences)
        return AttrDict(skill_dict)

    def get_contactpreference_locations(self, obj):
        location_dict = {}
        if hasattr(obj, 'contactpreference'):
            location_dict['wants_location_hokkaido_japan'] = int(obj.contactpreference.wants_location_hokkaido_japan)
            location_dict['wants_location_touhoku_japan'] = int(obj.contactpreference.wants_location_touhoku_japan)
            location_dict['wants_location_kanto_japan'] = int(obj.contactpreference.wants_location_kanto_japan)
            location_dict['wants_location_chubu_japan'] = int(obj.contactpreference.wants_location_chubu_japan)
            location_dict['wants_location_toukai_japan'] = int(obj.contactpreference.wants_location_toukai_japan)
            location_dict['wants_location_kansai_japan'] = int(obj.contactpreference.wants_location_kansai_japan)
            location_dict['wants_location_shikoku_japan'] = int(obj.contactpreference.wants_location_shikoku_japan)
            location_dict['wants_location_chugoku_japan'] = int(obj.contactpreference.wants_location_chugoku_japan)
            location_dict['wants_location_kyushu_japan'] = int(obj.contactpreference.wants_location_kyushu_japan)
            location_dict['wants_location_other_japan'] = int(obj.contactpreference.wants_location_other_japan)
        return AttrDict(location_dict)

    def get_contactpreference_koyou(self, obj):
        koyou_dict = {}
        if hasattr(obj, 'contactpreference'):
            koyou_dict['job_koyou_proper'] = int(obj.contactpreference.job_koyou_proper)
            koyou_dict['job_koyou_free'] = int(obj.contactpreference.job_koyou_free)
        return AttrDict(koyou_dict)

    def get_contactpreference_job_syouryu(self, obj):
        if hasattr(obj, 'contactpreference') and obj.contactpreference.job_syouryu:
            return JOB_SYOURYU_DISPLAY_NAME[str(obj.contactpreference.job_syouryu)]
        else:
            return ''

    def get_contactpreference_personnel_syouryu(self, obj):
        if hasattr(obj, 'contactpreference') and obj.contactpreference.personnel_syouryu:
            return PERSONNEL_SYOURYU_DISPLAY_NAME[str(obj.contactpreference.personnel_syouryu)]
        else:
            return ''

    class Meta:
        model = Contact
        fields = (
            'id', 'last_name', 'first_name', 'display_name', 'email', 'cc', 'tel_full', 'position', 'department', 'related_staff__email', 'last_visit',
            'created_user__name', 'created_time', 'modified_user__name', 'modified_time', 'related_organization__name', 'tag', 'category', 'preference_types', 'contactpreference',
            'contactjobtypepreferences', 'contactjobskillpreferences', 'contactpersonneltypepreferences', 'contactpersonnelskillpreferences',
            'contactpreference_job_syouryu', 'contactpreference_personnel_syouryu', 'contactpreference_locations', 'contactpreference_koyou'
        )


class ContactCsvSerializer(AppResourceSerializer):
    created_time = DateTimeField(format=TIME_FORMAT_FOR_CSV)
    modified_time = DateTimeField(format=TIME_FORMAT_FOR_CSV)
    related_organization__name = SerializerMethodField()
    related_staff__email = SerializerMethodField()
    created_user__name = SerializerMethodField()
    modified_user__name = SerializerMethodField()
    tel_full = SerializerMethodField()
    cc = SerializerMethodField()
    tag = SerializerMethodField()
    category = SerializerMethodField()
    preference_types = SerializerMethodField()

    def get_related_organization__name(self, obj):
        try:
            return obj.organization.name
        except AttributeError:
            return ''

    def get_related_staff__email(self, obj):
        try:
            return obj.staff.email
        except AttributeError:
            return ''

    def get_created_user__name(self, obj):
        try:
            return obj.created_user.display_name
        except AttributeError:
            return ''

    def get_modified_user__name(self, obj):
        try:
            return obj.modified_user.display_name
        except AttributeError:
            return ''

    def get_tel_full(self, obj):
        return get_tel_full(obj)

    def get_cc(self, obj):
        return ','.join([cc_address.email for cc_address in obj.cc_addresses.all()])

    def get_tag(self, obj):
        tag_values = []
        for tag_assignment in obj.tag_assignment.all():
            tag_values.append(tag_assignment.tag.value)
        return ','.join(tag_values)

    def get_category(self, obj):
        categories = []
        try:
            user_id = self.context['request'].user.id
        except Exception:
            user_id = None

        for contact_category in obj.categories.all():
            if user_id and contact_category and user_id == contact_category.user_id:
                categories.append(CONTACT_CATEGORY_DISPLAY_NAME.get(contact_category.category))
        return ','.join(categories)

    def get_preference_types(self, obj):
        preference_types = []
        if 0 < len(obj.contactjobtypepreferences.all()):
            preference_types.append('案件')
        if 0 < len(obj.contactpersonneltypepreferences.all()):
            preference_types.append('要員')
        return ','.join(preference_types)

    class Meta:
        model = Contact
        fields = (
            'id', 'last_name', 'first_name', 'display_name', 'email', 'cc', 'tel_full', 'position', 'department', 'related_staff__email', 'last_visit',
            'created_user__name', 'created_time', 'modified_user__name', 'modified_time', 'related_organization__name', 'tag', 'category', 'preference_types',
        )


class ContactCcSerializer(ModelSerializer):
    class Meta:
        model = ContactCC
        fields = ('email',)

class ContactCommentSerializer(MultiTenantFieldsMixin, BaseCommentSerializer):

    def custom_validation(self, data):
        custom_validation(data, 'comment')

    class Meta:
        model = ContactComment
        fields = ('id', 'created_user', 'created_user__name', 'created_user__avatar', 'created_time', 'content', 'company', 'is_important',
                  'has_subcomment', 'deleted_at', 'total_sub_comment', 'sub_comment_users_avatar', 'parent', 'parent_content', \
                  'parent_created_user_name', 'parent_modified_user_name', 'parent_created_time', 'parent_modified_time')
        extra_kwargs = {'company': {'write_only': True}, 'sub_comment_users_avatar': {'read_only': True}, 'total_sub_comment': {'read_only': True}, 'parent': {'read_only': True}}


class ContactSubCommentSerializer(BaseSubCommentSerializer):

    class Meta:
        model = ContactComment
        fields = ('id', 'created_user', 'created_user__name', 'created_user__avatar', 'created_time', 'content', 'company', 'is_important',
                  'parent')
        extra_kwargs = {'company': {'write_only': True}, 'is_important': {'read_only': True}}

class ContactCommentSummarySerializer(AppResourceSerializer):
    created_time = SerializerMethodField()

    def get_created_time(self, obj):
        return dateformat.format(obj.created_time, settings.DATETIME_FORMAT_FOR_FRONT)

    class Meta:
        model = ContactComment
        fields = (
            'content',
            'created_time',
            'created_user__name',
            'modified_time',
            'modified_user__name',)


class ContactInitialCommentSerializer(AppResourceSerializer):
    class Meta:
        model = ContactComment
        fields = ('id', 'content', 'is_important')


class NestedContactSummarySerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    contactpreference = ContactPreferenceModelSerializer(many=False, read_only=True)
    cc_mails = SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='email',
        source='cc_addresses',
        required=False,
    )

    category = SerializerMethodField()
    tags = SerializerMethodField()
    tag_objects = SerializerMethodField()
    staff__name = SerializerMethodField()
    organization__name = SerializerMethodField()

    comments = ContactCommentSummarySerializer(many=True, read_only=True, source='contactcomment_set')

    contactjobtypepreferences = SerializerMethodField()
    contactpersonneltypepreferences = SerializerMethodField()

    is_ignored = SerializerMethodField()

    def custom_validation(self, data):
        custom_validation(data, 'contact')

    def get_contactjobtypepreferences(self, obj):
        return [x.type_preference.name for x in obj.contactjobtypepreferences.all() if x.type_preference]

    def get_contactpersonneltypepreferences(self, obj):
        return [x.type_preference.name for x in obj.contactpersonneltypepreferences.all() if x.type_preference]

    def get_score(self, obj):
        user = self.context['request'].user
        if user:
            try:
                for score in obj.scores.all():
                    if score.user_id == user.id:
                        return score.score
            except (AttributeError, ObjectDoesNotExist):
                return None
        else:
            return None

    def get_category(self, obj):
        user = self.context['request'].user
        if user:
            try:
                for category in obj.categories.all():
                    if category.user_id == user.id:
                        return category.category
                return None
            except (AttributeError, ObjectDoesNotExist):
                return None
        else:
            return None

    def get_tags(self, obj):
        return [tag_assignment.tag.id for tag_assignment in obj.tag_assignment.all() if tag_assignment]

    def get_staff__name(self, obj):
        try:
            return obj.staff.display_name
        except AttributeError:
            return ' - '

    def get_organization__name(self, obj):
        return obj.organization.name

    def get_tag_objects(self, obj):
        return [
            {
                'value': tag_assignment.tag.value,
                'color': tag_assignment.tag.color
            } for tag_assignment in obj.tag_assignment.all() if tag_assignment
        ]

    def get_is_ignored(self, obj):
        company = obj.company

        # 「商流を抜ける」によって一覧に表示される場合はグレー表示にはしない
        is_ignored_by_has_distribution = obj.organization.has_distribution != True

        if hasattr(obj, 'exceptional_organizations') and obj.organization.id in [
            exceptional_organization.organization_id for exceptional_organization in obj.exceptional_organizations]:
            return False

        if company.capital_man_yen_required_for_transactions and obj.organization.capital_man_yen and company.capital_man_yen_required_for_transactions > obj.organization.capital_man_yen:
            return is_ignored_by_has_distribution

        if company.p_mark_or_isms == True and obj.organization.has_p_mark_or_isms == False:
            return is_ignored_by_has_distribution

        if company.invoice_system == True and obj.organization.has_invoice_system == False:
            return is_ignored_by_has_distribution

        if company.haken == True and obj.organization.has_haken == False:
            return is_ignored_by_has_distribution

        if company.establishment_year and obj.organization.establishment_date:
            base_date = datetime.datetime.now() - relativedelta(years=company.establishment_year)
            if obj.organization.establishment_date > base_date.date():
                return is_ignored_by_has_distribution

        if obj.organization.is_blacklisted:
            return True

        return False

    class Meta:
        model = Contact
        fields = (
            'id',
            'display_name',
            'email',
            'cc_mails',
            'position',
            'department',
            'organization__name',
            'staff__name',
            'last_visit',
            'created_time',
            'modified_time',
            'comments',
            'tel1',
            'tel2',
            'tel3',
            'tags',
            'tag_objects',
            'contactpreference',
            'category',
            'is_ignored',
            'contactjobtypepreferences',
            'contactpersonneltypepreferences',
            'organization',
        )


class NestedContactSerializer(NestedContactSummarySerializer):
    """
    A nested serializer to register Contact with multiple relationships on create/update forms.
    """
    organization_branch__name = SerializerMethodField()
    organization__is_blacklisted = SerializerMethodField()

    score = SerializerMethodField()
    initial_comment = ContactInitialCommentSerializer(many=False, write_only=True, required=False)
    jobtypepreference = RequestContactJobTypePreferenceSerializer(many=False, write_only=True)
    jobskillpreference = RequestContactJobSkillPreferenceSerializer(many=False, write_only=True)
    personneltypepreference = RequestContactPersonnelTypePreferenceSerializer(many=False, write_only=True)
    personnelskillpreference = RequestContactPersonnelSkillPreferenceSerializer(many=False, write_only=True)
    preference = RequestContactPreferenceSerializer(many=False, write_only=True)

    contactjobskillpreferences = SerializerMethodField()
    contactpersonnelskillpreferences = SerializerMethodField()
    contactpreference = ContactPreferenceSerializer(many=False, read_only=True)

    score_dict = None
    category_dict = None
    tag_dict = None

    def get_contactjobskillpreferences(self, obj):
        return [x.skill_preference.name for x in obj.contactjobskillpreferences.all() if x.skill_preference]

    def get_contactpersonnelskillpreferences(self, obj):
        return [x.skill_preference.name for x in obj.contactpersonnelskillpreferences.all() if x.skill_preference]

    def get_score(self, obj):
        user = self.context['request'].user
        if user:
            try:
                for score in obj.scores.all():
                    if score.user_id == user.id:
                        return score.score
            except (AttributeError, ObjectDoesNotExist):
                return None
        else:
            return None


    def get_organization__name(self, obj):
        return obj.organization.name

    def get_organization_branch__name(self, obj):
        if obj.organization_branch:
            return obj.organization_branch.name
        else:
            return ''

    def get_organization__is_blacklisted(self, obj):
        return obj.organization.is_blacklisted

    def validate_tel_values(self, validated_data):
        tel1 = validated_data.get('tel1')
        tel2 = validated_data.get('tel2')
        tel3 = validated_data.get('tel3')
        # tel123の少なくとも１つは入力されている場合は値を確認
        if tel1 or tel2 or tel3:
            # tel123が全てが１桁以上の数字であることの確認
            if not re.compile(r'(^\d+)-(\d+)-(\d+$)').search(f'{tel1}-{tel2}-{tel3}'):
                return False

        return True

    def create(self, validated_data):
        pop_cc_mails = self.initial_data.pop('cc_mails', None)
        tmp_cc_mails = []
        if pop_cc_mails and pop_cc_mails != []:
            tmp_cc_mails = pop_cc_mails[0].replace(' ','').split(',')

        cc_mails = [mail for mail in tmp_cc_mails if mail != '']
        if len(cc_mails) > 5:
            raise OverLimitCCMailsException()

        score = self.initial_data.pop('score', 'undefined')
        category = self.initial_data.pop('category', 'undefined')
        tags = self.initial_data.pop('tags', None)
        if tags and len(tags) > 10:
            raise OverLimitTagsException()
        initial_comment = validated_data.pop('initial_comment', {})

        jobtypepreference = validated_data.pop('jobtypepreference', {})
        jobskillpreference = validated_data.pop('jobskillpreference', {})
        personneltypepreference = validated_data.pop('personneltypepreference', {})
        personnelskillpreference = validated_data.pop('personnelskillpreference', {})
        preference = validated_data.pop('preference', {})

        if len(validated_data['email']) != 0 and \
                Contact.objects.filter(email=validated_data['email'], company=self.context['request'].user.company).exists():
            raise AlreadyRegisteredEmailException()

        validate_tel_values(validated_data, 'contact')

        instance = super().create(validated_data)

        user = self.context['request'].user

        if user and score != 'undefined':
            edit_contact_score(contact=instance, user=user, value=score)

        if user and category != 'undefined':
            edit_contact_category(contact=instance, user=user, category=category)

        assign_cc_addresses(contact=instance, addresses=cc_mails)
        assign_tags(contact=instance, tags=tags)
        assign_jobtypepreference(contact=instance, jobtypepreference=jobtypepreference)
        assign_jobskillpreference(contact=instance, jobskillpreference=jobskillpreference)
        assign_personneltypepreference(contact=instance, personneltypepreference=personneltypepreference)
        assign_personnelskillpreference(contact=instance, personnelskillpreference=personnelskillpreference)
        assign_preference(contact=instance, preference=preference)

        if initial_comment and 'content' in initial_comment and len(initial_comment['content']) > 0:
            user = self.context['request'].user
            ContactComment.objects.create(
                contact = instance,
                company = user.company,
                content = initial_comment.pop('content'),
                is_important = initial_comment.pop('is_important', False),
                created_user = user,
                modified_user = user
            )

        return instance

    def update(self, instance, validated_data):
        pop_cc_mails = self.initial_data.pop('cc_mails', None)
        tmp_cc_mails = []
        if pop_cc_mails and pop_cc_mails != []:
            tmp_cc_mails = pop_cc_mails[0].replace(' ','').split(',')

        cc_mails = [mail for mail in tmp_cc_mails if mail != '']
        if len(cc_mails) > 5:
            raise OverLimitCCMailsException()

        new_email = validated_data.get('email')
        if new_email and instance.email != new_email:
            if Contact.objects.filter(email=new_email, company=self.context['request'].user.company).exists():
                raise AlreadyRegisteredEmailException()

        score = self.initial_data.pop('score', 'undefined')
        category = self.initial_data.pop('category', 'undefined')
        tags = self.initial_data.pop('tags', None)
        if tags and len(tags) > 10:
            raise OverLimitTagsException()
        jobtypepreference = validated_data.pop('jobtypepreference', {})
        jobskillpreference = validated_data.pop('jobskillpreference', {})
        personneltypepreference = validated_data.pop('personneltypepreference', {})
        personnelskillpreference = validated_data.pop('personnelskillpreference', {})
        preference = validated_data.pop('preference', {})

        if not 'organization_branch' in self.initial_data:
            validated_data['organization_branch'] = None

        validate_tel_values(validated_data, 'contact')

        instance = super().update(instance, validated_data)

        user = self.context['request'].user

        if user and score != 'undefined':
            edit_contact_score(contact=instance, user=user, value=score)

        if user and category != 'undefined':
            edit_contact_category(contact=instance, user=user, category=category)

        assign_cc_addresses(contact=instance, addresses=cc_mails)
        assign_tags(contact=instance, tags=tags)
        assign_jobtypepreference(contact=instance, jobtypepreference=jobtypepreference)
        assign_jobskillpreference(contact=instance, jobskillpreference=jobskillpreference)
        assign_personneltypepreference(contact=instance, personneltypepreference=personneltypepreference)
        assign_personnelskillpreference(contact=instance, personnelskillpreference=personnelskillpreference)
        assign_preference(contact=instance, preference=preference)

        return instance

    @atomic
    def save(self, **kwargs):
        instance = super().save(**kwargs)
        return instance

    class Meta:
        model = Contact
        fields = (
            'id', 'last_name', 'first_name', 'display_name', 'email', 'position', 'department', 'organization', 'organization__name',
            'organization__is_blacklisted', 'tel1', 'tel2', 'tel3',
            'cc_mails', 'score', 'tags', 'staff', 'staff__name', 'last_visit',
            'created_time', 'created_user', 'created_user__name',
            'modified_time', 'modified_user', 'modified_user__name', 'company', 'old_id', 'comments', 'initial_comment',
            'jobtypepreference', 'jobskillpreference', 'personneltypepreference', 'personnelskillpreference', 'preference',
            'contactjobtypepreferences', 'contactjobskillpreferences', 'contactpersonneltypepreferences', 'contactpersonnelskillpreferences', 'contactpreference',
            'tag_objects', 'category', 'organization_branch', 'is_ignored', 'organization_branch__name', 'organization',
        )
        extra_kwargs = {'company': {'write_only': True}}


class OverLimitCCMailsException(APIException):
    status_code = 400
    default_detail = 'CCに指定できるアドレスは5件までです。'
    default_code = 'over_limit_cc_mails'


class OverLimitTagsException(APIException):
    status_code = 400
    default_detail = '選択できるタグは10件までです。'
    default_code = 'over_limit_tags'


class AlreadyRegisteredEmailException(APIException):
    status_code = 400
    default_detail = 'TOに入力したメールアドレスは既に登録されています。'
    default_code = 'already_registered_email'


class NestedContactEmailSerializer(MultiTenantFieldsMixin, AppResourceSerializer):

    class Meta:
        model = Contact
        fields = (
            'id', 'email'
        )


class TagSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    def custom_validation(self, data):
        custom_validation(data, 'tag')

    class Meta:
        model = Tag
        fields = (
            'id', 'value', 'internal_value', 'color', 'created_user', 'created_user__name',
            'created_time', 'company', 'modified_time', 'modified_user', 'modified_user__name', 'is_skill'
        )
        extra_kwargs = {'company': {'write_only': True}}


class DisplaySettingSerializer(ModelSerializer):

    content_hash = SerializerMethodField()
    content = DictField(write_only=True, required=False)
    modified_user__name = SerializerMethodField()

    def get_content_hash(self, obj):
        return json.loads(obj.content) if obj.content else {}

    def get_modified_user__name(self, obj):
        try:
            return obj.modified_user.display_name
        except AttributeError:
            return ''

    def check_must_require(self, content):
        for key, v in settings.DISPLAY_SETTING_DEFAULT.items():
            default_require_items = v.get('require')
            if len(default_require_items) > 0 and content.get(key):
                l = set(default_require_items) & set(content[key].get('require'))
                if len(default_require_items) != len(l):
                    raise MustRequireItemException()

    def create(self, validated_data):
        content = validated_data.pop('content', {})
        self.check_must_require(content)
        content_json = json.dumps(content)
        validated_data['content'] = content_json
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        content = validated_data.pop('content', {})
        self.check_must_require(content)
        content_json = json.dumps(content)
        validated_data['content'] = content_json
        updated_instance = super().update(instance, validated_data)
        return updated_instance

    class Meta:
        model = DisplaySetting
        fields = ('company', 'content', 'content_hash', 'modified_time', 'modified_user__name')


class MustRequireItemException(APIException):
    status_code = 400
    default_detail = '固定の必須項目を任意にすることはできません。'
    default_code = 'must_require_item'


class UserDisplaySettingSerializer(ModelSerializer):

    content_hash = SerializerMethodField()
    content = DictField(write_only=True, required=False)

    def get_content_hash(self, obj):
        return json.loads(obj.content) if obj.content else {}

    def create(self, validated_data):
        content = validated_data.pop('content', {})
        content_json = json.dumps(content)
        validated_data['content'] = content_json
        instance = super().create(validated_data)
        return instance

    def update(self, instance, validated_data):
        user = self.context["request"].user
        user_display_setting = user.userdisplaysetting
        user_display_setting_dict = model_to_dict(user_display_setting)
        user_display_setting_content = json.loads(user_display_setting_dict.pop("content"))
        content = validated_data.pop('content', {})
        updated_content = deep_update(user_display_setting_content, content)
        content_json = json.dumps(updated_content)
        validated_data['content'] = content_json
        updated_instance = super().update(instance, validated_data)
        return updated_instance

    class Meta:
        model = UserDisplaySetting
        fields = ('user', 'content', 'content_hash')
