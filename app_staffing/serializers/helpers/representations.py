TIME_FORMAT_FOR_CSV = '%Y/%m/%d %X'


def bool_to_symbol(boolean):
    if boolean is True:
        return '◯'
    elif boolean is False:
        return '×'
    elif boolean is None:
        return '不明'
    else:
        raise ValueError('The input is not a boolean value.')
