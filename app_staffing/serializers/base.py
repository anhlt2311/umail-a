from uuid import UUID

from django.db.models import ObjectDoesNotExist
from django.db.transaction import atomic
from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    SlugRelatedField,
    PrimaryKeyRelatedField,
)

from app_staffing.board.constants import PRIORITY_MAP
from app_staffing.models import Contact, Company
from app_staffing.utils.custom_validation import custom_validation


class ValidationErrorCodes(object):
    """Constants that represents Django rest framework serializer's Error codes."""
    UNIQUE = 'unique'
    SIZEOVER = 'size_over'


class MultiTenantFieldsMixin(object):
    """A mixin that adds a tenant field to your writeable serializer to separate data between tenants (Companies).
    Note: If Do not forget to specify following fields on your concrete serializer, or you fail to create resources.
    """
    company = PrimaryKeyRelatedField(queryset=Company.objects.all(), required=True, write_only=True)

    def update(self, instance, validated_data):
        validated_data.pop('company', None)  # validated_data no longer has company for update.
        return super().update(instance, validated_data)


class AppResourceSerializer(ModelSerializer):
    """A mixin that replaces user_ids to display name for App resource representation."""
    created_user__name = SerializerMethodField()
    modified_user__name = SerializerMethodField()
    created_user__avatar = SerializerMethodField()

    def get_created_user__name(self, obj):
        if obj.created_user:
            return obj.created_user.display_name
        else:
            return ''

    def get_modified_user__name(self, obj):
        if obj.modified_user:
            return obj.modified_user.display_name
        else:
            return ''

    def get_created_user__avatar(self, obj):
        if obj.created_user and obj.created_user.avatar:
            return obj.created_user.avatar.url
        else:
            return None


class CreatableSlugRelatedField(SlugRelatedField):

    def to_internal_value(self, data):
        try:
            return self.get_queryset().get_or_create(**{self.slug_field: data})[0]
        except ObjectDoesNotExist:
            self.fail('does_not_exist', slug_name=self.slug_field, value=data)
        except (TypeError, ValueError):
            self.fail('invalid')


class CreatableTargetContactSlugRelatedField(SlugRelatedField):

    def to_internal_value(self, data):
        try:
            UUID(data)
        except (ValueError, AttributeError):
            self.fail('invalid')
        try:
            Contact.objects.get(pk=data)  # Check if the target contact exists.
            return data  # Just pass original data. (we need to get object ID to create actual data
            # so actual data will be created in serializer's create method.)
        except ObjectDoesNotExist:
            self.fail('does_not_exist', slug_name=self.slug_field, value=data)
        except (TypeError, ValueError):
            self.fail('invalid')

    def to_representation(self, obj):
        return obj.contact.id


class BaseCommentSerializer(AppResourceSerializer):
    total_sub_comment = SerializerMethodField()
    sub_comment_users_avatar = SerializerMethodField()
    parent_content = SerializerMethodField()
    parent_modified_user_name = SerializerMethodField()
    parent_created_user_name = SerializerMethodField()
    parent_created_time = SerializerMethodField()
    parent_modified_time = SerializerMethodField()

    def get_parent_content(self, data):
        if data.parent:
            return data.parent.content
        return None

    def get_parent_created_user_name(self, data):
        if data.parent:
            return data.parent.created_user.display_name
        return None

    def get_parent_modified_user_name(self, data):
        if data.parent:
            return data.parent.modified_user.display_name
        return None

    def get_parent_created_time(self, data):
        if data.parent:
            return data.parent.created_time
        return None

    def get_parent_modified_time(self, data):
        if data.parent:
            return data.parent.modified_time
        return None

    def get_total_sub_comment(self, data):
        return data.sub_comments.count()

    def get_sub_comment_users_avatar(self, data):
        if not data.has_subcomment:
            return []
        result_dict: dict = {}
        queryset = data.sub_comments.all().select_related("created_user")
        for item in queryset:
            user_avatar = getattr(item.created_user, "avatar", None)
            url = user_avatar.url if user_avatar else ""
            result_dict.update({item.created_user_id: url})
        return result_dict.values()


class BaseSubCommentSerializer(AppResourceSerializer):
    def custom_validation(self, data):
        custom_validation(data, 'comment')

    def create(self, validated_data):
        with atomic():
            instance = super().create(validated_data)
            if instance.parent.has_subcomment is False:
                instance.parent.has_subcomment = True
                instance.parent.save()
            return instance


class BaseCardSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    list_id = SerializerMethodField()
    modified_user_name = SerializerMethodField()
    created_user_name = SerializerMethodField()
    scheduled_email_template = SerializerMethodField()

    def get_scheduled_email_template(self, obj):
        template = obj.templates.all().first()
        return template.to_dictionary() if template else None

    def get_list_id(self, obj):
        return obj.card_list_id

    def get_modified_user_name(self, obj):
        return obj.modified_user.display_name if obj.modified_user else None

    def get_created_user_name(self, obj):
        return obj.created_user.display_name if obj.created_user else None

    def to_internal_value(self, data):
        period = data.pop('period', None)
        if period:
            if period.get('start'):
                data.update(start=period.get('start'))
            if period.get('end', None):
                data.update(end=period.get('end'))
            if period.get('is_finished'):
                data.update(is_finished=period.get('is_finished'))
        list_id = data.pop('list_id', None)
        if list_id:
            data.update(card_list=list_id)
        return super().to_internal_value(data)

    def to_representation(self, data):
        data = super().to_representation(data)
        priority = data.get('priority')
        if priority:
            data['priority'] = dict(title=PRIORITY_MAP.get(priority), value=priority)
        return data
