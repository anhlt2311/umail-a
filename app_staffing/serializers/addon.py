from rest_framework.serializers import ModelSerializer, SerializerMethodField

from app_staffing.models.addon import Addon, AddonMaster


class AddonSerializer(ModelSerializer):

    class Meta:
        model = Addon
        fields = (
            "id",
            "expiration_time",
            "addon_master_id",
        )
        read_only_fields = (
            'id',
        )

class AddonMasterSerializer(ModelSerializer):
    targets = SerializerMethodField()
    is_dashboard = SerializerMethodField()
    is_organizations = SerializerMethodField()
    is_scheduled_mails = SerializerMethodField()
    is_shared_mails = SerializerMethodField()
    is_my_company_setting = SerializerMethodField()
    parents = SerializerMethodField()

    def get_targets(self, obj):
        return obj.target_assignment.all().values_list('addon_target__name', flat=True)
    
    def get_is_dashboard(self, obj):
        return False

    def get_is_organizations(self, obj):
        return (1 in list(obj.target_assignment.all().values_list('addon_target_id', flat=True)))
    
    def get_is_scheduled_mails(self, obj):
        ids = list(obj.target_assignment.all().values_list('addon_target_id', flat=True))
        return (3 in ids or 4 in ids or 5 in ids)
    
    def get_is_shared_mails(self, obj):
        return False
    
    def get_is_my_company_setting(self, obj):
        return False

    def get_parents(self, obj):
        return obj.parent_assignment.all().values_list('parent_addon_master_id', flat=True)

    class Meta:
        model = AddonMaster
        fields = (
            "id",
            "title",
            "description",
            "price",
            "is_recommended",
            "expiration_time",
            "limit",
            "targets",
            "is_dashboard",
            "is_organizations",
            "is_scheduled_mails",
            "is_shared_mails",
            "is_my_company_setting",
            "parents",
            "help_url",
        )
