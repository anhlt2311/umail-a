from app_staffing.models import TypePreference, SkillPreference, ContactJobTypePreference, ContactJobSkillPreference, \
    ContactPersonnelSkillPreference, ContactPersonnelTypePreference, ContactPreference


def assign_jobtypepreference(contact, jobtypepreference):
    jobtype_register_items = []
    jobtype_delete_items = []
    typepreferences = TypePreference.objects.all()
    for typepreference in typepreferences:
        if jobtypepreference.get(typepreference.name, False):
            jobtype_register_items.append(typepreference)
        else:
            jobtype_delete_items.append(typepreference)

    for register_item in jobtype_register_items:
        cjtp, _ = ContactJobTypePreference.objects.get_or_create(contact=contact, type_preference=register_item, defaults={"company": contact.company})
        cjtp.save()

    ContactJobTypePreference.objects.filter(contact=contact, type_preference__in=jobtype_delete_items).delete()

    return

def assign_jobskillpreference(contact, jobskillpreference):
    jobskill_register_items = []
    jobskill_delete_items = []
    skillpreferences = SkillPreference.objects.all()
    for skillpreference in skillpreferences:
        if jobskillpreference.get(skillpreference.name, False):
            jobskill_register_items.append(skillpreference)
        else:
            jobskill_delete_items.append(skillpreference)

    for register_item in jobskill_register_items:
        cjtp, _ = ContactJobSkillPreference.objects.get_or_create(contact=contact, skill_preference=register_item, defaults={"company": contact.company})
        cjtp.save()

    ContactJobSkillPreference.objects.filter(contact=contact, skill_preference__in=jobskill_delete_items).delete()

    return

def assign_personneltypepreference(contact, personneltypepreference):
    personneltype_register_items = []
    personneltype_delete_items = []
    typepreferences = TypePreference.objects.all()
    for typepreference in typepreferences:
        if personneltypepreference.get(typepreference.name, False):
            personneltype_register_items.append(typepreference)
        else:
            personneltype_delete_items.append(typepreference)

    for register_item in personneltype_register_items:
        cjtp, _ = ContactPersonnelTypePreference.objects.get_or_create(contact=contact, type_preference=register_item, defaults={"company": contact.company})
        cjtp.save()

    ContactPersonnelTypePreference.objects.filter(contact=contact, type_preference__in=personneltype_delete_items).delete()

    return

def assign_personnelskillpreference(contact, personnelskillpreference):
    personnelskill_register_items = []
    personnelskill_delete_items = []
    skillpreferences = SkillPreference.objects.all()
    for skillpreference in skillpreferences:
        if personnelskillpreference.get(skillpreference.name, False):
            personnelskill_register_items.append(skillpreference)
        else:
            personnelskill_delete_items.append(skillpreference)

    for register_item in personnelskill_register_items:
        cjtp, _ = ContactPersonnelSkillPreference.objects.get_or_create(contact=contact, skill_preference=register_item, defaults={"company": contact.company})
        cjtp.save()

    ContactPersonnelSkillPreference.objects.filter(contact=contact, skill_preference__in=personnelskill_delete_items).delete()

    return

def assign_preference(contact, preference):
    cp, _ = ContactPreference.objects.get_or_create(contact=contact, defaults={"company": contact.company})
    cp.wants_location_kanto_japan = preference.get('wants_location_kanto_japan', False)
    cp.wants_location_kansai_japan = preference.get('wants_location_kansai_japan', False)
    cp.wants_location_hokkaido_japan = preference.get('wants_location_hokkaido_japan', False)
    cp.wants_location_touhoku_japan = preference.get('wants_location_touhoku_japan', False)
    cp.wants_location_chubu_japan = preference.get('wants_location_chubu_japan', False)
    cp.wants_location_kyushu_japan = preference.get('wants_location_kyushu_japan', False)
    cp.wants_location_other_japan = preference.get('wants_location_other_japan', False)
    cp.wants_location_chugoku_japan = preference.get('wants_location_chugoku_japan', False)
    cp.wants_location_shikoku_japan = preference.get('wants_location_shikoku_japan', False)
    cp.wants_location_toukai_japan = preference.get('wants_location_toukai_japan', False)
    cp.job_koyou_proper = preference.get('job_koyou_proper', False)
    cp.job_koyou_free = preference.get('job_koyou_free', False)
    cp.job_syouryu = preference.get('job_syouryu')
    cp.personnel_syouryu = preference.get('personnel_syouryu')
    cp.personnel_country_japan = preference.get('personnel_country_japan', False)
    cp.personnel_country_other = preference.get('personnel_country_other', False)
    cp.has_send_guide = preference.get('has_send_guide', True)
    cp.save()

    return
