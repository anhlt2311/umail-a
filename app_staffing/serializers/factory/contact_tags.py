from rest_framework.exceptions import ValidationError

from app_staffing.models import Tag, ContactTagAssignment
from app_staffing.models.organization import TAG_VALUE_MAX_LENGTH


def assign_tags(contact, tags):
    if tags is not None:  # Distinguish None (undefined) and an empty list.
        contact.tags.clear()
        tags = Tag.objects.filter(id__in=tags)
        for tag in tags:
            contact.tags.add(tag, through_defaults={'company': contact.company})

        ContactTagAssignment.objects.filter(contact__isnull=True).delete()
    return
