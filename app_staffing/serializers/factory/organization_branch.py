from app_staffing.models import OrganizationBranch

KEYS = ['name','address','tel1','tel2','tel3','fax1','fax2','fax3']

def assign_organization_branches(organization, branches):
    if not branches:
        return

    insert_items = []
    update_items = []
    for branch in branches:
        if 'id' in branch:
            update_items.append(branch)
        else:
            insert_items.append(branch)

    if insert_items:
        insert_objects = []
        for insert_item in insert_items:
            insert_objects.append(OrganizationBranch(organization=organization, company=organization.company, **insert_item))
        OrganizationBranch.objects.bulk_create(insert_objects)

    if update_items:
        update_item_dict = {}
        for update_item in update_items:
            update_item_dict[update_item['id']] = update_item

        organization_branches = OrganizationBranch.objects.filter(id__in=update_item_dict.keys())

        for organization_branch in organization_branches:
            for key in KEYS:
                if str(organization_branch.id) in update_item_dict and key in update_item_dict[str(organization_branch.id)]:
                    setattr(organization_branch, key, update_item_dict[str(organization_branch.id)][key])

        OrganizationBranch.objects.bulk_update(organization_branches, KEYS)
