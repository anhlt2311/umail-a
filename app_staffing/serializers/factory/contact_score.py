from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import status
from rest_framework.exceptions import ValidationError

from app_staffing.models import ContactScore


def edit_contact_score(contact, user, value):
    """Create, Update or Delete ContactScore instance depends on the given arguments.
    :param contact: A contact you want to score.
    :type contact: An instance of app_staffing.models.ContactScore
    :param user: A user who want to score the contact.
    :type user: An instance of app_staffing.models.User (settings.AUTH_USER_MODEL)
    :param value: A Score value you want to set.
    :param value: None or int (1 ~ 5)
    :return: score_instance or None
    :raises: rest_framework.exceptions.ValidationError
    """

    if value is None:  # back to unrated.
        try:
            score_obj = ContactScore.objects.get(contact=contact, user=user)
            score_obj.delete()
            return None
        except ObjectDoesNotExist:
            return None
    elif isinstance(value, int) and value in range(1, 6):  # score 1 ~ 5. watch out for definition of range().
        try:
            score_obj = ContactScore.objects.get(contact=contact, user=user)
            score_obj.score = value
            score_obj.save()
            return score_obj
        except ObjectDoesNotExist:
            score_obj = ContactScore.objects.create(contact=contact, user=user, score=value, company=user.company)
            return score_obj
    else:
        raise ValidationError(code=status.HTTP_400_BAD_REQUEST, detail='Scoreには1~5の数字をいれてください。')
