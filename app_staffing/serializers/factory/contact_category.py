from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import status
from rest_framework.exceptions import ValidationError

from app_staffing.models import ContactCategory


def edit_contact_category(contact, user, category):
    if category is None:  # back to categorized.
        try:
            category_obj = ContactCategory.objects.get(contact=contact, user=user)
            category_obj.delete()
            return None
        except ObjectDoesNotExist:
            return None
    else:
        try:
            category_obj = ContactCategory.objects.get(contact=contact, user=user)
            category_obj.category = category
            category_obj.save()
            return category_obj
        except ObjectDoesNotExist:
            category_obj = ContactCategory.objects.create(contact=contact, user=user, category=category, company=user.company)
            return category_obj
