from app_staffing.models import ContactCC


def assign_cc_addresses(contact, addresses):
    if addresses is not None:  # Distinguish None (undefined) and an empty list.
        contact.cc_addresses.clear()  # Clear relationships between existing ContactCC Records.
        for email in addresses:
            ContactCC.objects.get_or_create(
                email=email, contact=contact, defaults={"company": contact.company}
            )
        # Remove dangling Records.
        ContactCC.objects.filter(contact__isnull=True).delete()
    return
