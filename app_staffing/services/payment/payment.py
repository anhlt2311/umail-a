from django.db import transaction
from app_staffing.models.plan import PlanPaymentError, CardPaymentError
from app_staffing.utils.payment import get_taxed_price, validate_active_plans, extract_addon_total_price_and_master_ids,\
    validate_company_attributes, extract_user_add_price_and_additional_options, create_new_plan,\
    create_purchase_history, exec_charge, get_main_card_id, get_backup_card_id

import logging


def exec_company_payment(company):
    payment_error_plan = None
    # 成功でも失敗でもない場合があるため、戻り値として以下のタプルを返す
    succeeded = succeeded_with_backup = failed = None
    err_with_main = err_with_backup = None
    try:
        # テナントが退会済みの場合は処理対象から外す
        if company.deactivated_time:
            raise RuntimeError('company deactivated')
        # プラン情報を取得
        current_active_plan = validate_active_plans(company.plan_set.all())
        # プランに設定されている金額を取得
        plan_price = current_active_plan.plan_master.price
        # アドオン情報とアドオンの合計金額を取得する
        addon_total_price, addon_master_ids = extract_addon_total_price_and_master_ids(company.addon_set.all())
        
        # テナント設定情報を取得
        company_attribute = validate_company_attributes(company.companyattribute_set.all())
        
        # ユーザー追加料金を取得
        user_add_price, additional_options = extract_user_add_price_and_additional_options(company_attribute, current_active_plan)
        # プラン料金とアドオン料金とユーザー追加料金を加算した値が請求額になる
        total_price = plan_price + addon_total_price + user_add_price
        # 税率を乗算する
        total_taxed_price = get_taxed_price(total_price)
        # トランザクション開始
        with transaction.atomic():
            # 現在のプランを非アクティブにする
            current_active_plan.is_active = False
            current_active_plan.save()
            # 新しいプランを作成
            new_plan = create_new_plan(company, current_active_plan.plan_master_id, current_active_plan.payment_date)
            # 支払い履歴を作成
            create_purchase_history(company, current_active_plan, new_plan, addon_master_ids, additional_options, total_taxed_price)

            # エラーによりデータはロールバックされて、請求だけが走ってしまうのを防ぐため、請求処理は一番最後に回す
            is_success, err_with_main, err_with_backup = exec_charge(
                total_taxed_price,
                company,
                f'charge plan master : {current_active_plan.plan_master_id}, addon master : {",".join(addon_master_ids)}'
            )
            if is_success:
                # 決済成功
                succeeded = company
                if err_with_main is not None:
                    # メインカードが失敗しているので、エラーをログに出力しておく
                    logging.error('payment with main card: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, err_with_main))
                    CardPaymentError.objects.get_or_create(company_id=company.id, plan_id=new_plan.id, card_id=get_main_card_id(company), card_type='main')
                    succeeded_with_backup = company
            else:
                # 決済失敗
                payment_error_plan = current_active_plan
                failed = company
                raise err_with_main

    except Exception as e:
        if payment_error_plan:
            PlanPaymentError.objects.get_or_create(company_id=company.id, plan_id=payment_error_plan.id)

        if err_with_main:
            logging.error('payment with main card: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, err_with_main))
            CardPaymentError.objects.get_or_create(company_id=company.id, plan_id=payment_error_plan.id, card_id=get_main_card_id(company), card_type='main')

        if err_with_backup:
            logging.error('payment with backup card: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, err_with_backup))
            CardPaymentError.objects.get_or_create(company_id=company.id, plan_id=payment_error_plan.id, card_id=get_backup_card_id(company), card_type='sub')

        if err_with_main is None and err_with_backup is None:
            # 支払い以外のエラーログの出力
            logging.error('payment: Company: {0} Exception occurred. ErrorDetails:{1}'.format(company.id, e))

    return succeeded, succeeded_with_backup, failed
