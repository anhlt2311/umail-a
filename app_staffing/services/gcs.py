from google.oauth2 import service_account
from google.cloud import storage
from django.conf import settings

class GcsService:
    def __init__(self, credential_info=None, default_bucket_name=None) -> None:
        _credentials = service_account.Credentials.from_service_account_info(credential_info)
        self.client = storage.Client(
            project=credential_info['project_id'], credentials=_credentials)
        self.default_bucket = self.client.bucket(default_bucket_name)

    def get_download_url(self, file_name):
        return self.default_bucket.get_blob(file_name)._get_download_url()



gcs_service = None
if hasattr(settings, 'GS_CREDENTIAL_INFO'):
    gcs_service = GcsService(credential_info=settings.GS_CREDENTIAL_INFO,
                         default_bucket_name=settings.GS_BUCKET_NAME)
