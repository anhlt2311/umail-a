
from dateutil.relativedelta import relativedelta

from django.db.models import Count, Q

from app_staffing.models import Organization, OrganizationStat
from app_staffing.services.stats.base import Statistics

from django.conf import settings

class OrganizationStats(Statistics):
    def calc_stats(self, target_date):
        company_id = self.get_company_id()
        from_date, to_date = self.get_target_from_to(target_date)
        result = Organization.objects.filter(
            company_id=company_id,
            created_time__lt=to_date,
        ).aggregate(
            prospective=Count('id', filter=Q(organization_category_id=1)), # prospective
            approached=Count('id', filter=Q(organization_category_id=2)), # approached
            exchanged=Count('id', filter=Q(organization_category_id=3)), # exchanged
            client=Count('id', filter=Q(organization_category_id=4)), # client
        )
        organization_stat = OrganizationStat.objects.filter(
            company_id=company_id,
            target_date=from_date
        )

        if organization_stat:
            organization_stat[0].prospective = result['prospective']
            organization_stat[0].approached = result['approached']
            organization_stat[0].exchanged = result['exchanged']
            organization_stat[0].client = result['client']
            organization_stat[0].save()
        else:
            OrganizationStat.objects.create(
                company_id=company_id,
                target_date=from_date,
                prospective=result['prospective'],
                approached=result['approached'],
                exchanged=result['exchanged'],
                client=result['client'],
            )

    def get_stats(self):
        company_id = self.get_company_id()
        base_datetime = self.get_base_datetime()
        from_datetime = base_datetime - relativedelta(months=self.get_month_range())
        labels = []
        values = {
            'prospective': [],
            'approached': [],
            'exchanged': [],
            'client': [],
        }

        results =OrganizationStat.objects.filter(
            company_id=company_id,
            target_date__lt=base_datetime,
            target_date__gte=from_datetime
        ).order_by('target_date')[:settings.DASHBOARD_DISPLAY_MAX_COUNT]

        for result in results:
            labels.append(f'{result.target_date.year}/{result.target_date.month}')
            values['prospective'].append(result.prospective)
            values['approached'].append(result.approached)
            values['exchanged'].append(result.exchanged)
            values['client'].append(result.client)

        return {
            'labels': labels,
            'values': values,
        }
