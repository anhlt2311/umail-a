from dateutil.relativedelta import relativedelta
from django.db.models import Count

from app_staffing.models.email import Email
from app_staffing.services.stats.base import Statistics


class EmailRankingStats(Statistics):
    _STATS_ID = 'RxEmail_Count_Ranking'

    def _calc_stats(self, company_id, base_datetime):

        # Get top 5 senders and num of emails of the last month.
        emails = Email.objects.filter(
            sent_date__year=(base_datetime - relativedelta(months=1)).year,
            sent_date__month=(base_datetime - relativedelta(months=1)).month,
            company_id=company_id
        ).values('from_header').annotate(num_mails=Count('from_header')).order_by('-num_mails')[:5]

        labels = [x['from_header'] for x in emails]
        values = [x['num_mails'] for x in emails]

        return {
            'labels': labels,
            'values': values,
        }
