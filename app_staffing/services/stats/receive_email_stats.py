
from dateutil.relativedelta import relativedelta

from app_staffing.models.email import Email
from app_staffing.services.stats.base import Statistics


class ReceiveEmailStats(Statistics):
    _STATS_ID = 'Receive_Email_Stats'

    def _calc_stats(self, company_id, base_datetime):

        labels = [
            '{year}/{month}'.format(
                year=(base_datetime - relativedelta(months=x)).year,
                month=(base_datetime - relativedelta(months=x)).month,
                company_id=company_id
            ) for x in reversed(range(1, self.get_month_range() + 1))
        ]

        return {
            'labels': labels,
            'values': [
                10,
                2,
                9,
                7,
                5,
                1,
            ],
        }
