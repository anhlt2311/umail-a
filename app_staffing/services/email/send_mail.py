import os

from django.core.mail import EmailMessage, get_connection
from django.core.files.storage import default_storage as storage
from django.template.loader import get_template
from django.conf import settings

from app_staffing.constants.email import ScheduledEmailSettingFileStatus, SCHEDULED_EMAIL_CONTENT_WITH_URL_SETTING
from app_staffing.models import SMTPServer, Addon, ScheduledEmailSetting, ContactPreference, ExceptionalOrganization
from app_staffing.models.organization import ContactCategory
from app_staffing.services.email.helper.smtp_connection import get_smtp_connection

import bleach
from datetime import datetime, timedelta, timezone
from app_staffing.utils.gcp import CloudTasks

import json
from django.db.models.query_utils import Q
from app_staffing.utils.query_builder import generate_or_query
from dateutil.relativedelta import relativedelta

import time

import logging


EMAIL_TEMPLATE_NAME = 'mail.txt'
HTML_EMAIL_TEMPLATE_NAME = 'mail.html'

EMAIL_TEMPLATE_HEADER_NAME = 'mail_header.txt'
HTML_EMAIL_TEMPLATE_HEADER_NAME = 'mail_header.html'
EMAIL_TEMPLATE_BODY_NAME = 'mail_body.txt'
HTML_EMAIL_TEMPLATE_BODY_NAME = 'mail_body.html'
EMAIL_TEMPLATE_FOOTER_NAME = 'mail_footer.txt'
HTML_EMAIL_TEMPLATE_FOOTER_NAME = 'mail_footer.html'
EMAIL_TEMPLATE_SIG_NAME = 'mail_sig.txt'
HTML_EMAIL_TEMPLATE_SIG_NAME = 'mail_sig.html'
EMAIL_TEMPLATE_URL_ATTACHMENT = 'mail_url_attachment.txt'
HTML_EMAIL_TEMPLATE_URL_ATTACHMENT = 'mail_url_attachment.html'

DUMMY_CONTACT_LAST_NAME = '取引先担当者'
DUMMY_CONTACT_FIRST_NAME = '取引先担当者'
DUMMY_CONTACT_DISPLAY_NAME = '取引先担当者'
DUMMY_ORGANIZATION_NAME = '所属取引先'
MAX_CHUNK_SIZE_FOR_MAILS = 50
MAX_RETRY = 3

def render_body(content, text_format, scheduled_email_setting=None, is_use_remove_promotion_available=None):
    context = {
        'content': get_breached_content(content, text_format) or '',  # For empty text.
        'promotion': get_promotion_message(text_format, scheduled_email_setting, is_use_remove_promotion_available) or '',
    }

    template_name = HTML_EMAIL_TEMPLATE_BODY_NAME if text_format == 'html' else EMAIL_TEMPLATE_BODY_NAME

    mail_template = get_template(template_name)
    return mail_template.render(context)


def get_header_template(text_format):
    template_name = HTML_EMAIL_TEMPLATE_HEADER_NAME if text_format == 'html' else EMAIL_TEMPLATE_HEADER_NAME
    return get_template(template_name)


def render_header(contact_display_name, contact_organization_name, text_format, header_template=None):
    context = {
        'contact_display_name': get_breached_content(contact_display_name, text_format) or '',
        'contact_organization_name': get_breached_content(contact_organization_name, text_format) or '',
    }

    if not header_template:
        header_template = get_header_template(text_format)

    return header_template.render(context)


def render_footer(text_format,contact_id=None, email_id=None, scheduled_email_setting=None, is_open_count_available=None):
    if text_format == 'html' and contact_id and email_id and is_open_count_available and scheduled_email_setting and scheduled_email_setting.use_open_count:
        return append_open_tag('', contact_id, email_id)
    return ''


def render_sig(sender_signature, text_format):
    context = {
        'sender_signature': get_breached_content(sender_signature, text_format) or '',
    }

    template_name = HTML_EMAIL_TEMPLATE_SIG_NAME if text_format == 'html' else EMAIL_TEMPLATE_SIG_NAME
    return get_template(template_name).render(context)


def render_url_attachment_template(url, password, text_format):
    context = {
        'url': get_breached_content(url, text_format) or '',
        'password': get_breached_content(password, text_format) or ''
    }

    template_name = HTML_EMAIL_TEMPLATE_URL_ATTACHMENT if text_format == 'html' else EMAIL_TEMPLATE_URL_ATTACHMENT
    return get_template(template_name).render(context)


def get_breached_content(content, text_format):
    if content and text_format == 'html':
        return bleach.clean(content, tags=settings.HTML_ALLOWED_TAGS, attributes=settings.HTML_ALLOWED_TAG_ATTRIBUTES)
    else:
        return content


def get_promotion_message(text_format, scheduled_email_setting, is_use_remove_promotion_available):
    if not text_format == 'html':
        return ''

    if is_use_remove_promotion_available and scheduled_email_setting and scheduled_email_setting.use_remove_promotion:
        return ''

    return settings.SCHEDULED_EMAIL_PROMOTION_MESSAGE


def append_open_tag(rendered_body, contact_id, email_id):
    tag = '<img src=\'{0}?cid={1}&eid={2}\' />'.format(settings.MAIL_OPEN_COUNT_ACCESS_POINT, contact_id, email_id)
    return rendered_body + tag


def _create_email_instance(
        from_address, sender_signature, contact_last_name, contact_first_name, contact_display_name, contact_organization_name,
        to_addresses, cc_addresses, subject, content, attachments, connection, text_format, contact_id=None, email_id=None,
        scheduled_email_setting=None, is_open_count_available=None, is_use_remove_promotion_available=None):

    body = render_body(
        content=content,
        text_format=text_format,
        scheduled_email_setting=scheduled_email_setting,
        is_use_remove_promotion_available=is_use_remove_promotion_available,
    )

    header = render_header(
        contact_display_name=contact_display_name,
        contact_organization_name=contact_organization_name,
        text_format=text_format,
    )

    footer = render_footer(
        text_format=text_format,
        contact_id=contact_id,
        email_id=email_id,
        scheduled_email_setting=scheduled_email_setting,
        is_open_count_available=is_open_count_available,
    )

    sig = render_sig(
        sender_signature=sender_signature,
        text_format=text_format,
    )

    merged_content = header + body + sig + footer

    email = EmailMessage(
        subject=subject, body=merged_content, from_email=from_address, to=to_addresses, cc=cc_addresses,
        connection=connection
    )

    if text_format == 'html':
        email.content_subtype = 'html'

    for attachment in attachments:
        email.attach(filename=attachment['filename'], content=attachment['content'])

    return email

def _create_email_dict(
        from_address, sender_signature, contact_last_name, contact_first_name, contact_display_name, contact_organization_name,
        to_addresses, cc_addresses, subject, content, text_format, contact_id=None, email_id=None,
        scheduled_email_setting=None, is_open_count_available=None, is_use_remove_promotion_available=None, header_template=None):

    header = render_header(
        contact_display_name=contact_display_name,
        contact_organization_name=contact_organization_name,
        text_format=text_format,
        header_template=header_template
    )

    footer = render_footer(
        text_format=text_format,
        contact_id=contact_id,
        email_id=email_id,
        scheduled_email_setting=scheduled_email_setting,
        is_open_count_available=is_open_count_available,
    )

    attachment_data = []

    email = {
            'subject': subject,
            'header': header,
            'footer': footer,
            'to': to_addresses[0],
            'cc': cc_addresses,
            'from': from_address,
            'format': text_format,
            'attachments': attachment_data,
        }

    return email


def _create_copy_for_sender(sender, subject, content, attachments, connection, text_format, data_type, scheduled_email_setting, is_use_remove_promotion_available):

    from_address = settings.DEFAULT_FROM_EMAIL
    subject = '配信メール控え: {}'.format(subject)
    header_template = get_header_template(text_format)

    if data_type == 'EmailMessage':
        email = _create_email_instance(
            from_address=from_address, sender_signature=sender.email_signature,
            contact_last_name=DUMMY_CONTACT_LAST_NAME, contact_first_name=DUMMY_CONTACT_FIRST_NAME, contact_display_name=DUMMY_CONTACT_DISPLAY_NAME, contact_organization_name=DUMMY_ORGANIZATION_NAME,
            to_addresses=[sender.email], cc_addresses=[], subject=subject,
            content=content, attachments=attachments, connection=connection, text_format=text_format,
            scheduled_email_setting=scheduled_email_setting, is_use_remove_promotion_available=is_use_remove_promotion_available
        )

    else:
        email = _create_email_dict(
            from_address=from_address, sender_signature=sender.email_signature,
            contact_last_name=DUMMY_CONTACT_LAST_NAME, contact_first_name=DUMMY_CONTACT_FIRST_NAME, contact_display_name=DUMMY_CONTACT_DISPLAY_NAME, contact_organization_name=DUMMY_ORGANIZATION_NAME,
            to_addresses=[sender.email], cc_addresses=[], subject=subject,
            content=content, text_format=text_format,
            scheduled_email_setting=scheduled_email_setting, is_use_remove_promotion_available=is_use_remove_promotion_available,
            header_template=header_template
        )
    return email


def _create_copy_for_share(sender, subject, content, attachments, connection, to_addresses, text_format, data_type, scheduled_email_setting, is_use_remove_promotion_available):
    subject = '配信メール控え: {}'.format(subject)
    header_template = get_header_template(text_format)

    if data_type == 'EmailMessage':
        email = _create_email_instance(
            from_address=sender.email, sender_signature=sender.email_signature,
            contact_last_name=DUMMY_CONTACT_LAST_NAME, contact_first_name=DUMMY_CONTACT_FIRST_NAME, contact_display_name=DUMMY_CONTACT_DISPLAY_NAME, contact_organization_name=DUMMY_ORGANIZATION_NAME,
            to_addresses=to_addresses, cc_addresses=[], subject=subject,
            content=content, attachments=attachments, connection=connection, text_format=text_format,
            scheduled_email_setting=scheduled_email_setting, is_use_remove_promotion_available=is_use_remove_promotion_available
        )
    else:
        email = _create_email_dict(
            from_address=sender.email, sender_signature=sender.email_signature,
            contact_last_name=DUMMY_CONTACT_LAST_NAME, contact_first_name=DUMMY_CONTACT_FIRST_NAME, contact_display_name=DUMMY_CONTACT_DISPLAY_NAME, contact_organization_name=DUMMY_ORGANIZATION_NAME,
            to_addresses=to_addresses, cc_addresses=[], subject=subject,
            content=content, text_format=text_format,
            scheduled_email_setting=scheduled_email_setting, is_use_remove_promotion_available=is_use_remove_promotion_available,
            header_template=header_template
        )


    return email


def _generate_emails(scheduled_email_instance, connection, shared_email_address, scheduled_email_setting=None, is_open_count_available=None, data_type='EmailMessage', is_use_remove_promotion_available=None):

    scheduled_email = scheduled_email_instance

    from_address = scheduled_email.sender.email
    sender_signature = scheduled_email.sender.email_signature
    subject = scheduled_email.subject
    content = scheduled_email.text
    text_format = scheduled_email.text_format

    has_url_setting = (
        scheduled_email_setting.file_type == ScheduledEmailSettingFileStatus.URL if scheduled_email_setting
        else ScheduledEmailSetting.objects.filter(company=scheduled_email.company, file_type=ScheduledEmailSettingFileStatus.URL).exists())

    if text_format == 'html':
        content = content.replace('\n' , '<br>')

    # Note: This attachment model is ScheduledEmailAttachment, which is different from EmailAttachment.
    attachments = []
    if data_type == 'EmailMessage':
        for attachment in scheduled_email.attachments.all():
            if has_url_setting:
                forward_url = SCHEDULED_EMAIL_CONTENT_WITH_URL_SETTING.format(settings.HOST_NAME, scheduled_email.id, scheduled_email.password)
                content += forward_url
            else:
                with storage.open(attachment.file.name, "rb") as f:
                    attachments.append({'filename': os.path.basename(attachment.file.name), 'content': f.read()})


    mail_data = []  # Define a list that stores email instances to send.
    copy_data = []

    # Create a copy of the email to the sender first.
    if scheduled_email_instance.send_copy_to_sender:
        copied_email = _create_copy_for_sender(
            sender=scheduled_email.sender, subject=subject, content=content, attachments=attachments,
            connection=connection, text_format=text_format, data_type=data_type,
            scheduled_email_setting=scheduled_email_setting, is_use_remove_promotion_available=is_use_remove_promotion_available
        )
        copy_data.append(copied_email)

    if scheduled_email_instance.send_copy_to_share and shared_email_address:
        copied_email_for_share = _create_copy_for_share(
            sender=scheduled_email.sender, subject=subject, content=content, attachments=attachments,
            connection=connection, to_addresses=[shared_email_address], text_format=text_format, data_type=data_type,
            scheduled_email_setting=scheduled_email_setting, is_use_remove_promotion_available=is_use_remove_promotion_available
        )
        copy_data.append(copied_email_for_share)

    # Then, create email instances for the targets.
    targets = scheduled_email.targets.select_related("contact", "contact__organization").filter(
        contact__organization__is_blacklisted=False,
    )

    # 保存されている検索条件でcontactsを再度検索して、targetsと付き合わせる
    contactpreference_queryset = ContactPreference.objects.all()
    search_condition = {}
    if hasattr(scheduled_email, 'searchcondition') and scheduled_email.searchcondition and scheduled_email.searchcondition.search_condition:
        search_condition = json.loads(scheduled_email.searchcondition.search_condition)

    # 配信エリアによる検索
    location_items = [
        'wants_location_hokkaido_japan', 'wants_location_touhoku_japan', 'wants_location_kanto_japan', 'wants_location_kansai_japan',
        'wants_location_chubu_japan', 'wants_location_kyushu_japan', 'wants_location_other_japan',
        'wants_location_chugoku_japan', 'wants_location_shikoku_japan', 'wants_location_toukai_japan'
    ]
    location_query_params = []
    for location in location_items:
        if search_condition.get(location):
            location_query_params.append({f'{location}': True})
    if location_query_params:
        contactpreference_queryset = generate_or_query(contactpreference_queryset, location_query_params)

    # 取引先ステータスによる検索
    category_items = ['prospective', 'approached', 'exchanged', 'client']
    organization_category_query_params = []
    for category in category_items:
        if search_condition.get(f'contact__organization__category_{category}'):
            organization_category_query_params.append({'contact__organization__organization_category__name': category})
    if organization_category_query_params:
        contactpreference_queryset = generate_or_query(contactpreference_queryset, organization_category_query_params)

    # 自社担当者による検索
    if search_condition.get('contact__staff'):
        contactpreference_queryset = contactpreference_queryset.filter(contact__staff_id=search_condition.get('contact__staff'))

    # 相性による検索
    if search_condition.get('contact__category') and search_condition.get('category_inequality'):
        if search_condition.get('category_inequality') == 'eq':
            contactpreference_queryset = contactpreference_queryset.filter(
                contact__categories__category=search_condition.get('contact__category'),
                contact__categories__user_id=scheduled_email.sender_id
            )
        elif search_condition.get('category_inequality') == 'not_eq':
            contactpreference_queryset = contactpreference_queryset.exclude(
                contact__categories__category=search_condition.get('contact__category'),
                contact__categories__user_id=scheduled_email.sender_id
            )

    # タグによる検索
    if search_condition.get('contact__tags') and search_condition.get('contact__tags__suffix'):
        if search_condition.get('contact__tags__suffix') == 'and':
            contactpreference_queryset = contactpreference_queryset.filter(contact__tags__id__in=search_condition.get('contact__tags'))
        elif search_condition.get('contact__tags__suffix') == 'or':
            tags_query_params = []
            for tag_id in search_condition.get('contact__tags'):
                tags_query_params.append({'contact__tags__id': tag_id})
            if tags_query_params:
                contactpreference_queryset = contactpreference_queryset = generate_or_query(contactpreference_queryset, tags_query_params)

    type_dev_items = ['dev_designer', 'dev_front', 'dev_server', 'dev_pm', 'dev_other']
    skill_dev_items = ['dev_youken', 'dev_kihon', 'dev_syousai', 'dev_seizou', 'dev_test', 'dev_hosyu', 'dev_beginner']
    type_infra_items = ['infra_server', 'infra_network', 'infra_security', 'infra_database', 'infra_sys', 'infra_other']
    skill_infra_items = ['infra_youken', 'infra_kihon', 'infra_syousai', 'infra_kouchiku', 'infra_test', 'infra_kanshi', 'infra_hosyu', 'infra_beginner']
    type_other_items = ['other_eigyo', 'other_kichi', 'other_support', 'other_other']

    # 配信種別による検索
    # 検索種別はラジオボタンなので、どれか一つ(job, personnel, otherのどれか)が選択される
    if search_condition.get("searchtype") == 'job':
        # 配信職種(案件)はラジオボタンなので、どれか一つが選択される
        if search_condition.get("jobtype") == 'dev':
            # 配信職種詳細(案件)はラジオボタンなので、どれか一つが選択される
            for item in type_dev_items:
                if search_condition.get(f'jobtype_{item}'):
                    contactpreference_queryset = contactpreference_queryset.filter(contact__contactjobtypepreferences__type_preference__name=item)

            # 配信スキル詳細(案件)はチェックボックスなので複数選択される(検索はOR検索)
            jobskill_dev_search_params = []
            for item in skill_dev_items:
                if search_condition.get(f'jobskill_{item}'):
                    jobskill_dev_search_params.append(item)
            if jobskill_dev_search_params:
                contactpreference_queryset = contactpreference_queryset.filter(contact__contactjobskillpreferences__skill_preference__name__in=jobskill_dev_search_params)
        elif search_condition.get("jobtype") == 'infra':
            # 配信職種詳細(案件)はラジオボタンなので、どれか一つが選択される
            for item in type_infra_items:
                if search_condition.get(f'jobtype_{item}'):
                    contactpreference_queryset = contactpreference_queryset.filter(contact__contactjobtypepreferences__type_preference__name=item)

            # 配信スキル詳細(案件)はチェックボックスなので複数選択される(検索はOR検索)
            jobskill_infra_search_params = []
            for item in skill_infra_items:
                if search_condition.get(f'jobskill_{item}'):
                    jobskill_infra_search_params.append(item)
            if jobskill_infra_search_params:
                contactpreference_queryset = contactpreference_queryset.filter(contact__contactjobskillpreferences__skill_preference__name__in=jobskill_infra_search_params)
        elif search_condition.get("jobtype") == 'other':
            # 配信職種詳細(案件)はラジオボタンなので、どれか一つが選択される
            for item in type_other_items:
                if search_condition.get(f'jobtype_{item}'):
                    contactpreference_queryset = contactpreference_queryset.filter(contact__contactjobtypepreferences__type_preference__name=item)
    elif search_condition.get("searchtype") == 'personnel':
        # 配信職種(要員)はチェックボックスなので複数選択される(検索はOR検索)
        personnel_search_params = []
        if search_condition.get("personneltype_dev"):
            # 配信職種詳細(要員)はチェックボックスなので複数選択される(検索はOR検索)
            personneltype_dev_search_query = []
            personneltype_dev_search_params = []
            for item in type_dev_items:
                if search_condition.get(f'personneltype_{item}'):
                    personneltype_dev_search_params.append(item)
            if personneltype_dev_search_params:
                personneltype_dev_search_query.append(
                    Q(contact__contactpersonneltypepreferences__type_preference__name__in=personneltype_dev_search_params)
                )
            # 配信スキル詳細(要員)はチェックボックスなので複数選択される(検索はOR検索)
            personnelskill_dev_search_params = []
            for item in skill_dev_items:
                if search_condition.get(f'personnelskill_{item}'):
                    personnelskill_dev_search_params.append(item)
            if personnelskill_dev_search_params:
                personneltype_dev_search_query.append(
                    Q(contact__contactpersonnelskillpreferences__skill_preference__name__in=personnelskill_dev_search_params)
                )
            personnel_search_params.append(
                # 職種詳細とスキル詳細はANDでつなげる
                Q(*personneltype_dev_search_query)
            )
        if search_condition.get("personneltype_infra"):
            # 配信職種詳細(要員)はチェックボックスなので複数選択される(検索はOR検索)
            personneltype_infra_search_query = []
            personneltype_infra_search_params = []
            for item in type_infra_items:
                if search_condition.get(f'personneltype_{item}'):
                    personneltype_infra_search_params.append(item)
            if personneltype_infra_search_params:
                personneltype_infra_search_query.append(
                    Q(contact__contactpersonneltypepreferences__type_preference__name__in=personneltype_infra_search_params)
                )

            # 配信スキル詳細(案件)はチェックボックスなので複数選択される(検索はOR検索)
            personnelskill_infra_search_params = []
            for item in skill_infra_items:
                if search_condition.get(f'personnelskill_{item}'):
                    personnelskill_infra_search_params.append(item)
            if personnelskill_infra_search_params:
                personneltype_infra_search_query.append(
                    Q(contact__contactpersonnelskillpreferences__skill_preference__name__in=personnelskill_infra_search_params)
                )
            personnel_search_params.append(
                # 職種詳細とスキル詳細はANDでつなげる
                Q(*personneltype_infra_search_query)
            )
        if search_condition.get("personneltype_other"):
            # 配信職種詳細(要員)はチェックボックスなので複数選択される(検索はOR検索)
            personneltype_other_search_query = []
            personneltype_other_search_params = []
            for item in type_other_items:
                if search_condition.get(f'personneltype_{item}'):
                    personneltype_other_search_params.append(item)
            if personneltype_other_search_params:
                personneltype_other_search_query.append(
                    Q(contact__contactpersonneltypepreferences__type_preference__name__in=personneltype_other_search_params)
                )
            personnel_search_params.append(
                # 職種詳細とスキル詳細はANDでつなげる
                Q(*personneltype_other_search_query)
            )
        if personnel_search_params:
            personnel_search_query = personnel_search_params.pop()
            for item in personnel_search_params:
                personnel_search_query |= item
            contactpreference_queryset = contactpreference_queryset.filter(personnel_search_query)
    elif search_condition.get("searchtype") == "other":
        contactpreference_queryset = contactpreference_queryset.filter(has_send_guide=True)


    # 取引先取引条件で取引先を絞る
    # 取引先の取引条件が設定されていない場合は、この制御は無効になる
    # 取引先の取引条件「設立年数」「取引に必要な資格」「取引に必要な資本金」と自社の自社情報設定「設立年月」「保有資格」「資本金」を比較
    # ただしこのフィルタは、自社情報設定の「商流を抜ける」がFalseになっている場合のみ適用される
    filter_params = []
    if not scheduled_email.company.has_distribution:
        filter_params.append(
            Q(
                Q(contact__organization__capital_man_yen_required_for_transactions__lte=scheduled_email.company.capital_man_yen) |
                Q(contact__organization__capital_man_yen_required_for_transactions=None)
            ),
        )
        filter_params.append(
            Q(
                Q(contact__organization__p_mark_or_isms=scheduled_email.company.has_p_mark_or_isms) |
                Q(contact__organization__p_mark_or_isms=False)
            ),
        )
        filter_params.append(
            Q(
                Q(contact__organization__invoice_system=scheduled_email.company.has_invoice_system) |
                Q(contact__organization__invoice_system=False)
            ),
        )
        filter_params.append(
            Q(
                Q(contact__organization__haken=scheduled_email.company.has_haken) |
                Q(contact__organization__haken=False)
            ),
        )
        base_year = relativedelta(datetime.now(), scheduled_email.company.establishment_date).years
        filter_params.append(
            Q(
                Q(contact__organization__establishment_year__lte=base_year) |
                Q(contact__organization__establishment_year=None)
            ),
        )
    # 自社取引条件で取引先を絞る
    # 自社の取引条件が設定されていない場合は、この制御は無効になる
    # 自社の取引条件「設立年数」「取引に必要な資格」「取引に必要な資本金」と取引先の取引先情報「設立年月」「保有資格」「資本金」を比較
    if scheduled_email.company.capital_man_yen_required_for_transactions:
        filter_params.append(
            Q(contact__organization__capital_man_yen__gte=scheduled_email.company.capital_man_yen_required_for_transactions) |
            Q(contact__organization__capital_man_yen__isnull=True) |
            Q(contact__organization__has_distribution=True)
        )
    if scheduled_email.company.p_mark_or_isms:
        filter_params.append(
            Q(
                Q(contact__organization__has_p_mark_or_isms=scheduled_email.company.p_mark_or_isms) |
                Q(contact__organization__has_p_mark_or_isms__isnull=True) |
                Q(contact__organization__has_distribution=scheduled_email.company.p_mark_or_isms)
            )
        )
    if scheduled_email.company.invoice_system:
        filter_params.append(
            Q(
                Q(contact__organization__has_invoice_system=scheduled_email.company.invoice_system) |
                Q(contact__organization__has_invoice_system__isnull=True) |
                Q(contact__organization__has_distribution=scheduled_email.company.invoice_system)
            )
        )
    if scheduled_email.company.haken:
        filter_params.append(
            Q(
                Q(contact__organization__has_haken=scheduled_email.company.haken) |
                Q(contact__organization__has_haken__isnull=True) |
                Q(contact__organization__has_distribution=scheduled_email.company.haken)
            ),
        )
    if scheduled_email.company.establishment_year:
        base_date = datetime.now() - relativedelta(years=scheduled_email.company.establishment_year)
        filter_params.append(
            Q(contact__organization__establishment_date__lte=base_date) |
            Q(contact__organization__establishment_date__isnull=True) |
            Q(contact__organization__has_distribution=True)
        )
    # 除外取引先が登録されている場合は、取引条件の対象から除く。つまり、検索対象になるようにする
    exceptional_organizations = ExceptionalOrganization.objects.filter(company=scheduled_email.company)
    if exceptional_organizations:
        exceptional_organization_ids = [exceptional_organization.organization.id for exceptional_organization in exceptional_organizations]
        contactpreference_queryset = contactpreference_queryset.filter(Q(*filter_params) | Q(contact__organization__id__in=exceptional_organization_ids))
    else:
        contactpreference_queryset = contactpreference_queryset.filter(*filter_params)

    latest_target_contact_id_dict = {}
    for contact_id in contactpreference_queryset.values_list('contact_id', flat=True):
        latest_target_contact_id_dict[contact_id] = True

    header_template = get_header_template(text_format)

    for target in targets:  # recipient -> an instance of model scheduledEmailTarget.
        # 最新の検索結果に存在する、かつ、targetsに存在する場合のみ、送信対象にする
        # 例外1: 最新の検索結果には存在するが、targetsには存在しない => 選択されていないので送信対象にはならない
        # 例外2: 最新の検索結果には存在しないが、targetsには存在する => 配信対象から外れているので送信対象にならない
        if target.contact_id not in latest_target_contact_id_dict:
            continue

        to_addresses = [target.contact.email]
        contact_last_name = target.contact.last_name
        contact_first_name = target.contact.first_name
        contact_display_name = target.contact.display_name
        contact_organization_name = target.contact.organization.name
        cc_addresses = list(target.contact.cc_addresses.values_list('email', flat=True))

        if data_type == 'EmailMessage':
            mail = _create_email_instance(
                from_address=from_address, sender_signature=sender_signature,
                contact_last_name=contact_last_name, contact_first_name=contact_first_name, contact_display_name=contact_display_name, contact_organization_name=contact_organization_name, to_addresses=to_addresses,
                cc_addresses=cc_addresses, subject=subject, content=content, attachments=attachments, connection=connection, text_format=text_format,
                contact_id=target.contact.id, email_id=scheduled_email.id, scheduled_email_setting=scheduled_email_setting, is_open_count_available=is_open_count_available,
                is_use_remove_promotion_available=is_use_remove_promotion_available
            )
        else:
            mail = _create_email_dict(
                from_address=from_address, sender_signature=sender_signature,
                contact_last_name=contact_last_name, contact_first_name=contact_first_name, contact_display_name=contact_display_name, contact_organization_name=contact_organization_name, to_addresses=to_addresses,
                cc_addresses=cc_addresses, subject=subject, content=content, text_format=text_format,
                contact_id=target.contact.id, email_id=scheduled_email.id, scheduled_email_setting=scheduled_email_setting, is_open_count_available=is_open_count_available,
                is_use_remove_promotion_available=is_use_remove_promotion_available,
                header_template=header_template
            )

        mail_data.append(mail)

    return copy_data, mail_data



def _split_mails(mails, n):
    for idx in range(0, len(mails), n):
        yield mails[idx:idx + n]
    return True

def put_mails(scheduled_email_instance, shared_email_address, scheduled_email_setting=None, is_open_count_available=None, is_use_remove_promotion_available=None):
    logging.info(msg='Trying to put Scheduled Email into Cloud Tasks: ID:{0}, Subject:{1}'.format(
        scheduled_email_instance.id, scheduled_email_instance.subject)
    )

    smtp_server = SMTPServer.objects.get(company=scheduled_email_instance.sender.company)

    smtp_server_json = {
        'host': smtp_server.hostname,
        'port': smtp_server.port_number,
        'user': smtp_server.username,
        'password': smtp_server.password,
        'tls': smtp_server.use_tls,
        'ssl': smtp_server.use_ssl,
    }

    copies, target_mails = _generate_emails(
        scheduled_email_instance=scheduled_email_instance,
        connection=None,
        shared_email_address=shared_email_address,
        scheduled_email_setting=scheduled_email_setting,
        is_open_count_available=is_open_count_available,
        data_type='Dict',
        is_use_remove_promotion_available=is_use_remove_promotion_available,
    )

    cloud_tasks = CloudTasks(settings.CLOUD_TASKS_FOR_MAIL, settings.WORKER_NAME_FOR_MAIL)

    attachments = [
        {'filename': attachment.file.name} for attachment in scheduled_email_instance.attachments.all()
    ]

    raw_content = scheduled_email_instance.text
    if scheduled_email_instance.text_format == 'html':
        raw_content = raw_content.replace('\n' , '<br>')

    content = render_body(
        content=raw_content,
        text_format=scheduled_email_instance.text_format,
        scheduled_email_setting=scheduled_email_setting,
        is_use_remove_promotion_available=is_use_remove_promotion_available,
    )

    sig = render_sig(
        sender_signature=scheduled_email_instance.sender.email_signature,
        text_format=scheduled_email_instance.text_format,
    )

    if len(copies) >= 0:
        payload = {
            'type': 'copy',
            'email_id': str(scheduled_email_instance.id),
            'attachments': attachments,
            'smtp': smtp_server_json,
            'contexts': copies,
            'content': content,
            'sig': sig,
        }

        logging.info(msg=f"Put Email Task(Copies). Payload: {copies}")

        try:
            cloud_tasks.create_task(payload, scheduled_email_instance.date_to_send, payload['email_id'])
        except Exception as e:
            # 控えメール送信でエラーになってエラー終了になるのはもったいないので、ここはエラーログ出して継続
            logging.error('put mail(target mails): Exception occurred. Id:{0} Payload:{1} ErrorDetails:{2}'.format(
                scheduled_email_instance.id, payload, e
            ))


    mails = list(_split_mails(target_mails, smtp_server.send_count_per_task))
    total = len(target_mails)

    for index, mail in enumerate(mails):
        payload = {
            'type': 'normal',
            'email_id': str(scheduled_email_instance.id),
            'attachments': attachments,
            'total': total,
            'smtp': smtp_server_json,
            'contexts': mail,
            'content': content,
            'sig': sig,
        }
        logging.info(msg=f"Put Email Task(Target Mails). Payload: {payload} Index: {index}")

        for i in range(MAX_RETRY + 1):
            try:
                cloud_tasks.create_task(payload, scheduled_email_instance.date_to_send, payload['email_id'])
                break
            except Exception as e:
                logging.warning('create task error: {0} Retry:{1}'.format(e, i))
                if i >= MAX_RETRY:
                    logging.error('put mail(target mails): Exception occurred. Id:{0} Payload:{1} ErrorDetails:{2}'.format(
                        scheduled_email_instance.id, payload, e
                    ))
                    # 一つのメールタスクでもエラーになったら、CloudTasks中のジョブを全部初期化
                    # この後ステータスがエラーになり、オペレーターさんが再度配信することを想定
                    # 削除が失敗したら？==> エラーのエラーまで想定するときりがないので、一旦諦める (ステータスがエラーなのに一部のメールは送信される)
                    cloud_tasks.delete_task(str(scheduled_email_instance.id))
                    return { 'has_error': True }

                time.sleep(3)

    return { 'has_error': False }

# Note: make sure that EMAIL_BACKEND is defined in settings.py
def send_mails(scheduled_email_instance, shared_email_address, scheduled_email_setting=None, is_open_count_available=None, is_use_remove_promotion_available=None):
    logging.info(msg='Trying to send Scheduled Email: ID:{0}, Subject:{1}'.format(
        scheduled_email_instance.id, scheduled_email_instance.subject)
    )

    connection = get_smtp_connection(company=scheduled_email_instance.sender.company)

    copies, target_mails, = _generate_emails(
        scheduled_email_instance=scheduled_email_instance,
        connection=connection,
        shared_email_address=shared_email_address,
        scheduled_email_setting=scheduled_email_setting,
        is_open_count_available=is_open_count_available,
        is_use_remove_promotion_available=is_use_remove_promotion_available,
    )

    for mails in [copies, target_mails]:
        for mail in mails:
            logging.info(msg=f'Sending Raw Email. From:{mail.from_email} Subject:{mail.subject} To:{mail.to} CC:{mail.cc}')
            try:
                mail.send()
            except Exception as e:
                logging.error('send mail: Exception occurred. Id: {0} From:{1} Subject:{2} To:{3} CC:{4} ErrorDetails:{5}'.format(
                    scheduled_email_instance.id, mail.from_email, mail.subject, mail.to, mail.cc, e
                ))
                continue

    send_total_count = len(target_mails)
    return {
        'send_total_count': send_total_count,
    }
