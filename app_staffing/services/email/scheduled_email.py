from app_staffing.constants.email import ScheduledEmailSettingFileStatus
from app_staffing.models import ScheduledEmail, ScheduledEmailAttachment
from app_staffing.exceptions.base import ScheduledEmailNotExistException
from google.cloud import storage

from django.conf import settings
import base64


class ScheduledEmailService:
    @classmethod
    def download_file_from_gcp(cls, gcp_link):
        from google.oauth2 import service_account
        data = dict()
        credentials = service_account.Credentials.from_service_account_info(settings.GS_CREDENTIAL_INFO)
        client = storage.Client(project=settings.GS_CREDENTIAL_INFO['project_id'], credentials=credentials)
        bucket = client.bucket(settings.GS_BUCKET_NAME)
        blob = bucket.get_blob(gcp_link)
        if blob:
            data.update(content_type=blob.content_type, file_name=blob.name.split('/')[-1], content=base64.b64encode(blob.download_as_string()))
        return data

    @classmethod
    def get_stream_file_data_from_gcp(cls, attachment_ids: list):
        files_data = dict()
        attachments = list(ScheduledEmailAttachment.objects.filter(id__in=attachment_ids))
        for index, attachment in enumerate(attachments):
            content = cls.download_file_from_gcp(attachment.gcp_link)
            files_data[index] = content
        return files_data

    @classmethod
    def get_list_attachments(cls, scheduled_email_id):
        try:
            scheduled_email = ScheduledEmail.objects.get(id=scheduled_email_id)
            data = dict(subject=scheduled_email.subject, expired_date=scheduled_email.expired_date)
            attachments = (
                ScheduledEmailAttachment.objects.filter(
                    email_id=scheduled_email_id, file_type=ScheduledEmailSettingFileStatus.URL
                ).values("id", "name", "gcp_link", "size", "status")
            )
            data.update(attachments=attachments)
            return data
        except ScheduledEmail.DoesNotExist:
            raise ScheduledEmailNotExistException()

    @classmethod
    def delete_expired_attachments(cls, today):
        # Should delete all files when email is expired
        expired_mails = ScheduledEmail.objects.filter(expired_date__lt=today, file_type=ScheduledEmailSettingFileStatus.URL)
        expired_mail_ids = expired_mails.values_list("id", flat=True)
        expired_attachments = ScheduledEmailAttachment.objects.filter(email_id__in=expired_mail_ids)
        for attachment in expired_attachments:
            attachment.file.storage.delete(name=attachment.file.name)
        expired_mails.delete()
