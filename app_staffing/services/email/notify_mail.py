from copy import deepcopy

from datetime import timedelta
from functools import reduce
from operator import and_, or_
# Modules for type hinting.
from typing import Any, List, Callable

from django.conf import settings
from django.core.mail import EmailMessage
from django.db.models import QuerySet
from django.template.loader import get_template
from django.utils import timezone

from app_staffing.models import Email, EmailNotificationRule, EmailNotificationCondition
from app_staffing.services.email.helper.smtp_connection import get_smtp_connection

import logging


EMAIL_TEMPLATE_NAME = 'email_notification.txt'


def get_unprocessed_emails(company) -> QuerySet:
    """ Return a queryset of unprocessed emails.
    :param company: A Condition to filter email Queryset.
    :type company: app_staffing.models.Company
    :rtype: QuerySet of Email
    """
    now = timezone.now()
    oldest_time = now - timedelta(minutes=settings.EMAIL_SEARCH_RANGE_MINUTES_FOR_NOTIFICATIONS)
    return Email.objects.filter(
        created_time__gte=oldest_time,
        notifications_have_done=False,
        company=company,
    )


def get_notify_rules(company) -> QuerySet:
    """ Return a queryset of notification Rules.
    :param company: A Condition to filter email Queryset.
    :type company: app_staffing.models.Company
    :rtype: QuerySet of EmailNotificationRule
    """
    return EmailNotificationRule.objects\
        .select_related('created_user__company')\
        .prefetch_related('conditions')\
        .filter(created_user__company=company, is_active=True)


def __includes(field_value: str, condition_value: str) -> bool:
    return condition_value in field_value


def __excludes(field_value: str, condition_value: str) -> bool:
    return condition_value not in field_value


def __equal(field_value: str, condition_value: str) -> bool:
    return condition_value == field_value


def __not_equal(field_value: str, condition_value: str) -> bool:
    return condition_value != field_value


def __start_with(field_value: str, condition_value: str) -> bool:
    return field_value.startswith(condition_value)


def __end_with(field_value: str, condition_value: str) -> bool:
    return field_value.endswith(condition_value)


def __exists(field_value: bool, condition_value: str) -> bool:
    return field_value == True


def __not_exists(field_value: bool, condition_value: str) -> bool:
    return field_value == False


def __get_search_field_values(condition: EmailNotificationCondition, email: Email) -> List[str]:
    """ Return field values of the email based on the rule condition.
    :type condition: EmailNotificationCondition
    :type email: Email
    :return: evaluated field values.
    :rtype: list of str
    """

    if condition.extraction_type == 'keyword':
        lookup_table = {
            'subject': [email.subject],
            'content': [email.text],
            'from_address': [email.from_address],
            'attachment': [email.has_attachments]
        }
        return lookup_table[condition.target_type]  # Return Field values
    if condition.extraction_type == 'attachment':
        return [email.has_attachments]
    raise RuntimeError('Invalid extraction type has specified.')


def __get_judgement_func(condition_type: str) -> Callable[[str, str], bool]:
    """ Return A Judgement function from the value of the condition_type.
    :param condition_type: A string that specifies the way to judge the field value.
    :type condition_type: str
    :return: A judgement function which judges whether a field value satisfies the condition.
    :rtype: callable(field_value: str , condition_value: str) -> bool
    """

    if condition_type == 'include':
        return __includes
    if condition_type == 'exclude':
        return __excludes
    if condition_type == 'equal':
        return __equal
    if condition_type == 'not_equal':
        return __not_equal
    if condition_type == 'start_with':
        return __start_with
    if condition_type == 'end_with':
        return __end_with
    if condition_type == 'exists':
        return __exists
    if condition_type == 'not_exists':
        return __not_exists
    raise RuntimeError('Invalid condition type has specified.')


def __get_master_rule_operator(master_rule: str) -> Callable[[bool, bool], bool]:
    if master_rule == 'all':
        return and_
    if master_rule == 'any':
        return or_
    raise RuntimeError('Invalid master rule has specified.')


def __create_filter_func(condition: EmailNotificationCondition) -> Callable[[Email], bool]:
    """Create a filter function that judges whether the given email satisfies the notification condition.
    :param condition: A condition model instance used for creating filter function.
    :type condition: EmailNotificationCondition
    :return: A judgement function which distinguish whether the given email satisfies the condition.
    :rtype: callable(email: Email) -> bool
    """
    def func(email: Email) -> bool:
        search_field_values = __get_search_field_values(condition=condition, email=email)
        judgement_func = __get_judgement_func(condition_type=condition.condition_type)
        per_field_results = [judgement_func(field_value, condition.value) for field_value in search_field_values]
        result = reduce(or_, per_field_results)
        return result
    return func


def _render_notification_email_body(rules_and_emails: dict) -> str:
    context = {
        'rules_and_emails': rules_and_emails,
        'domain': settings.HOST_NAME,
    }
    template = get_template(EMAIL_TEMPLATE_NAME)
    return template.render(context)


def _create_notification_email_instance(to: str, rules_and_emails: dict, connection: Any) -> EmailMessage:
    subject = '共有メール通知'
    body = _render_notification_email_body(
        rules_and_emails=rules_and_emails,
    )
    email = EmailMessage(subject=subject, body=body, to=[to], connection=connection)
    return email


def _send_notify_email(to: str, rules_and_emails: dict, connection: Any) -> None:
    notification_email = _create_notification_email_instance(to=to, rules_and_emails=rules_and_emails, connection=connection)
    logging.info(msg='Trying to send Notification Email: To:{0}, Subject:{1}'.format(
        notification_email.to, notification_email.subject)
    )
    notification_email.send()


def _mark_as_notified(email: Email) -> None:
    email.notifications_have_done = True
    email.save(update_fields=['notifications_have_done'])


def _get_master_rule_name(key: str) -> str:
    return settings.SHARED_EMAILS_MASTER_RULE_NAMES[key]


def _get_condition_names(values_list: List[tuple]) -> List[List]:
    conditions = []
    for values in values_list:
        messages = []
        listed_values = list(values)
        messages.append(settings.SHARED_EMAILS_TARGET_DISPLAY_NAMES[listed_values[0]])
        messages.append(settings.SHARED_EMAILS_CONDITION_DISPLAY_NAMES[listed_values[1]])
        messages.append(listed_values[2])
        conditions.append(messages)
    return conditions


def match(email: Email, rule: EmailNotificationRule) -> bool:
    """A judgement function which judges whether the given email satisfies all conditions of the given rule.
    :param email: An Email instance.
    :type email: Email
    :param rule: An EmailNotificationRule.
    :type rule: EmailNotificationRule
    :return: Whether the Email matches the Rule.
    :rtype: bool
    """
    filter_funcs = [__create_filter_func(condition) for condition in rule.conditions.all()]
    evaluation_results = [filter_func(email) for filter_func in filter_funcs]
    _operator = __get_master_rule_operator(master_rule=rule.master_rule)
    matched = reduce(_operator, evaluation_results)
    return matched


def send_notification_emails(company) -> int:
    """The main logic of this module.
    :param company: A company ID to execute notification.
    :type company: An instance of app_staffing.models.Company
    """
    emails = get_unprocessed_emails(company)
    rules = get_notify_rules(company)
    matched_items = {}

    sent_count = 0
    for email in emails:
        for rule in rules:
            try:
                if match(email=email, rule=rule):
                    if rule.created_user.email in matched_items:
                        if rule.id in matched_items[rule.created_user.email]:
                            matched_items[rule.created_user.email][rule.id]['email_ids'].append(email.id)
                        else:
                            matched_items[rule.created_user.email][rule.id] = {
                                'rule_name': rule.name,
                                'master_rule': _get_master_rule_name(rule.master_rule),
                                'email_ids': [email.id],
                                'conditions': _get_condition_names(list(rule.conditions.values_list('target_type', 'condition_type', 'value'))),
                            }
                    else:
                        matched_items[rule.created_user.email] = {}
                        matched_items[rule.created_user.email][rule.id] = {
                            'rule_name': rule.name,
                            'master_rule': _get_master_rule_name(rule.master_rule),
                            'email_ids': [email.id],
                            'conditions': _get_condition_names(list(rule.conditions.values_list('target_type', 'condition_type', 'value'))),
                        }
            except Exception as e:
                logging.error('notification mail: Rule: {0} Exception occurred. ErrorDetails:{1}'.format(rule.id, e))
        _mark_as_notified(email)

    connection = get_smtp_connection(company)
    for to, rules_and_emails in matched_items.items():
        _send_notify_email(to, rules_and_emails, connection)
        sent_count += 1

    return sent_count  # Return the number of processed emails.
