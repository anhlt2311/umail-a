from django.core.mail import get_connection
from app_staffing.models import SMTPServer

def get_smtp_connection(company):
    smtp_server = SMTPServer.objects.get(company=company)
    connection = get_connection(
        host=smtp_server.hostname,
        port=smtp_server.port_number,
        username=smtp_server.username,
        password=smtp_server.password,
        use_tls=smtp_server.use_tls,
        use_ssl=smtp_server.use_ssl
    )
    return connection
