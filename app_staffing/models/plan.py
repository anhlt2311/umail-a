
from datetime import timedelta
from django.conf import settings
from django.db import models

from app_staffing.models.base import PerCompanyModel

from softdelete.models import SoftDeleteObject

class PlanMaster(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=128)
    description = models.CharField(max_length=512)
    price = models.IntegerField()
    user_add_price = models.IntegerField()
    default_user_count = models.IntegerField()
    order = models.IntegerField(unique=True)

    def __str__(self):
        return f"{self.title}"

    class Meta:
        indexes = [
            models.Index(fields=['order'], name='plan_master_order_idx'),
        ]

class Plan(PerCompanyModel, SoftDeleteObject):
    plan_master = models.ForeignKey(PlanMaster, on_delete=models.PROTECT, null=True)
    expiration_time = models.DateTimeField(editable=True, null=True, blank=True)
    is_active = models.BooleanField(default=False)
    payment_date = models.DateField(editable=True, null=True, blank=True)

    def __str__(self):
        return f"{self.company.name} : {self.plan_master.title}"
    
    @property
    def expiration_date(self):
        if self.payment_date:
            # 決済日の前日を有効期限日として扱う
            return self.payment_date - timedelta(days=1)
        else:
            return None

    class Meta:
        indexes = [
            models.Index(fields=['is_active', 'payment_date'], name='plan_pay_active_idx'),
        ]

class PlanPaymentError(PerCompanyModel, SoftDeleteObject):
    plan = models.ForeignKey(Plan, on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.company.name} : {self.plan.plan_master.title} : 支払い失敗"

class CardPaymentError(PerCompanyModel):
    plan = models.ForeignKey(Plan, on_delete=models.PROTECT)
    card_id = models.CharField(max_length=35, null=True, blank=True)
    card_type = models.CharField(max_length=4, null=True, blank=True)

    def __str__(self):
        return f"{self.company.name} : {self.plan.plan_master.title} : 支払い失敗({self.card_id})"
