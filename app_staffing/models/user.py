import time
from uuid import uuid4

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from app_staffing.models.base import AutoUserFieldsMixin
from app_staffing.utils import generate_image_filename

ADMIN = 'admin'
MANAGER = 'manager'
LEADER = 'leader'
MEMBER = 'member'
GUEST = 'guest'
MASTER = 'master'

USER_ROLES = (
    (MASTER, 'master'),  # 管理者
    (ADMIN, 'admin'),  # 管理者
    (MANAGER, 'manager'),  # 責任者
    (LEADER, 'leader'),  # リーダー
    (MEMBER, 'member'),  # メンバー
    (GUEST, 'guest')  # ゲスト
)


class UserRole(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128)
    order = models.IntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        indexes = [
            models.Index(fields=['name'], name='user_role_name_idx'),
            models.Index(fields=['order'], name='type_preference_order_idx'),
        ]


class User(AutoUserFieldsMixin, AbstractUser):
    # Using UUID as instead of default AutoInteger ID to avoid blute force attacking.
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    user_service_id = models.CharField(max_length=15)
    is_user_admin = models.BooleanField(
        default=False,
        help_text='ユーザがサービスでの管理者権限を持っているかどうかを示します。(システム管理者ではない)',
        verbose_name=_("サービスでの管理者権限")
    )
    email_signature = models.TextField(max_length=8192, blank=True, null=True)
    company = models.ForeignKey(
        'Company',
        blank=True,  # Note, System Admin (superuser) does not belongs to any company.
        null=True,  # Note, System Admin (superuser) does not belongs to any company.
        help_text='A company that the user belongs to.',
        on_delete=models.CASCADE
    )
    # Note: Remove this column after the data migration from the old system.
    old_id = models.IntegerField(null=True, unique=True, help_text='An unique ID on Old System.')
    user_role = models.ForeignKey(UserRole, on_delete=models.PROTECT, null=True)
    tel1 = models.CharField(max_length=15, blank=True, null=True)
    tel2 = models.CharField(max_length=15, blank=True, null=True)
    tel3 = models.CharField(max_length=15, blank=True, null=True)
    registed_at = models.DateTimeField(blank=True, null=True, help_text='本登録した日付')
    is_hidden = models.BooleanField(default=False, help_text='非表示ユーザフラグ')

    created_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, editable=False,
                                     on_delete=models.PROTECT, related_name='created_parent_%(class)s')
    created_time = models.DateTimeField(auto_now_add=True, editable=False, null=True)
    modified_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, editable=False,
                                     on_delete=models.PROTECT, related_name='modified_parent_%(class)s')
    modified_time = models.DateTimeField(auto_now=True, editable=False, null=True)
    avatar = models.ImageField(upload_to=generate_image_filename, blank=True, null=True)

    @property
    def display_name(self):
        display_name = self.username
        if self.last_name or self.first_name:
            display_name = self.last_name + ' ' + self.first_name

        if self.is_hidden:
            display_name = display_name + '(削除済ユーザー)'
        elif not self.is_active:
            display_name = display_name + '(無効ユーザー)'

        return display_name

    @property
    def user_role_name(self):
        if self.user_role:
            return self.user_role.name
        else:
            return ''

    @staticmethod
    def is_user_service_id_exist(pk, company, user_service_id):
        """
        Check user_service_id already exist in the company?
        return: bool - True if exist else False
        """
        return User.objects.filter(user_service_id=user_service_id, company_id=company).exclude(id=pk).exists()

    @staticmethod
    def generate_random_user_service_id():
        head, tail = str(time.time()).split('.')
        return f'user_{head[1:] + tail[:1]}'

    class Meta:
        unique_together = ['user_service_id', 'company']
