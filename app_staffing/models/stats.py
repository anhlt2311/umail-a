from uuid import uuid4

from django.db import models
from django.db.models.query_utils import Q

from softdelete.models import SoftDeleteObject

class OrganizationStat(SoftDeleteObject, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    target_date = models.DateField(null=False)
    prospective = models.PositiveIntegerField(default=0)
    approached = models.PositiveIntegerField(default=0)
    exchanged = models.PositiveIntegerField(default=0)
    client = models.PositiveIntegerField(default=0)
    blocklist = models.PositiveIntegerField(default=0)
    created_time = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return f"{self.target_date}の取引先ステータス集計"

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['company', 'target_date'], condition=Q(deleted_at=None), name='uniq_company_target_date'),
        ]

class ContactStat(SoftDeleteObject, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    target_date = models.DateField(null=False)
    jobtype_dev = models.PositiveIntegerField(default=0)
    jobtype_infra = models.PositiveIntegerField(default=0)
    jobtype_other = models.PositiveIntegerField(default=0)
    personneltype_dev = models.PositiveIntegerField(default=0)
    personneltype_infra = models.PositiveIntegerField(default=0)
    personneltype_other = models.PositiveIntegerField(default=0)
    other = models.PositiveIntegerField(default=0)
    created_time = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return f"{self.target_date}の配信種別詳細集計"

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['company', 'target_date'], condition=Q(deleted_at=None), name='u_con_stat_company_target_date'),
        ]

class ScheduledEmailStat(SoftDeleteObject, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    target_date = models.DateField(null=False)
    job = models.PositiveIntegerField(default=0)
    personnel = models.PositiveIntegerField(default=0)
    other = models.PositiveIntegerField(default=0)
    created_time = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return f"{self.target_date}のメール配信種別集計"

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['company', 'target_date'], condition=Q(deleted_at=None), name='u_se_stat_company_target_date'),
        ]


class StaffInChargeStat(SoftDeleteObject, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    target_date = models.DateField(null=False)
    top_values = models.CharField(max_length=255, blank=True)
    staff_in_charge = models.CharField(max_length=255, blank=True)
    created_time = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        unique_together = ('company', 'target_date', )


class SharedEmailStat(SoftDeleteObject, models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    target_date = models.DateField(null=False)
    top_values = models.CharField(max_length=255, blank=True)
    from_address = models.CharField(max_length=255, blank=True)
    from_name = models.CharField(max_length=255, blank=True)
    created_time = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        unique_together = ('company', 'target_date', )
