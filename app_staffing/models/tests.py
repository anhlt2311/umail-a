from django.db import models
from app_staffing.models.base import PerCompanyModel
from softdelete.models import SoftDeleteObject


class DummyModel(PerCompanyModel, SoftDeleteObject):
    """A dummy model for unit_test
    We can check a behavior when a target function got a wrong model, using this class.
    """
    name = models.CharField(max_length=64, default='')
