from uuid import uuid4, UUID

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models

from crum import get_current_user

from softdelete.models import SoftDeleteObject

DJANGO_MAX_USER_NAME_LENGTH = 150


def check_fields_consistency(fields):
    # Check Input Data
    if not fields:
        raise ValueError('we need a list of fields that contains at least 1 entry.')

    # Distinguish type of fields and cast if necessary.
    if isinstance(fields[0], str) or isinstance(fields[0], UUID):  # Mandatory for foreign key fields.
        consistent = all((str(field) == str(fields[0]) for field in fields))
    else:
        consistent = all((field == fields[0] for field in fields))

    # Execute Validation
    if not consistent:
        raise ValidationError(message=f'There is a inconsistency between company_id of given fields {fields}')


class ModelValidationMixin(object):
    """A model mixin that kicks model fields validation when saving a instance."""
    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)


class AutoUserFieldsMixin(object):  # Apply this mixin to your model.
    """An auto recording mixin that stores user info to a Django model."""

    def set_user_info(self):
        from app_staffing.models.user import User
        from django.utils.functional import SimpleLazyObject
        """This Automatically set username of logged in user to user fields of the model."""
        user = get_current_user()  # get user info from request context (thread-local).

        if user and (type(user) == User or type(user) == SimpleLazyObject) and not user.is_anonymous:
            if self._state.adding:  # for model creation.
                self.created_user = user
                self.modified_user = user
            else:  # for model updating.
                self.modified_user = user

    # Enabling Automatic Username logging to records.
    def save(self, *args, **kwargs):
        self.set_user_info()
        super().save(*args, **kwargs)


class PerCompanyModel(AutoUserFieldsMixin, ModelValidationMixin, models.Model):
    """A super class of models which belongs to specific Company (Tenant)"""

    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    company = models.ForeignKey(
        'Company',
        on_delete=models.CASCADE,
        help_text='A company (tenant) which this resource belongs to',
    )
    created_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, editable=False,
                                     on_delete=models.PROTECT, related_name='created_%(class)s')
    created_time = models.DateTimeField(auto_now_add=True, editable=False)
    modified_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, editable=False,
                                      related_name='modified_%(class)s', on_delete=models.PROTECT)
    modified_time = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class PerCompanyOneToOneModel(AutoUserFieldsMixin, SoftDeleteObject, models.Model):
    company = models.ForeignKey(
        'Company',
        on_delete=models.CASCADE,
        help_text='A company (tenant) which this resource belongs to',
    )
    created_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, editable=False,
                                     on_delete=models.PROTECT, related_name='created_%(class)s')
    created_time = models.DateTimeField(auto_now_add=True, editable=False)
    modified_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, editable=False,
                                      related_name='modified_%(class)s', on_delete=models.PROTECT)
    modified_time = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class OneToOneModel(AutoUserFieldsMixin, models.Model):
    created_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, editable=False,
                                     on_delete=models.PROTECT, related_name='created_%(class)s')
    created_time = models.DateTimeField(auto_now_add=True, editable=False)
    modified_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, editable=False,
                                     on_delete=models.PROTECT, related_name='modified_%(class)s')
    modified_time = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True

class OrderIdMixin(object):
    @classmethod
    def get_ordered_ids(self, order_key, id_key):
        return list(dict.fromkeys(list(self.objects.order_by(order_key).values_list(id_key, flat=True))))

class CommentBaseModel(PerCompanyModel):  # Apply this mixin to your model.
    """a base model for all comment models."""

    content = models.TextField(blank=True, null=False, max_length=1024, help_text='コメント内容')
    is_important = models.BooleanField(default=False, null=False)
    parent = models.ForeignKey('self', on_delete=models.PROTECT, null=True, related_name="sub_comments")
    has_subcomment = models.BooleanField(default=False, null=False)

    def _do_delete(self, changeset, related):
        if related.one_to_many and related.related_name == 'sub_comments':
            return
        super()._do_delete(changeset, related)


    class Meta:
        abstract = True
