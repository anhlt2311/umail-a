from jubakit.base import BaseLoader


def model_to_dict(instance):
    return {field.name: field.value_from_object(instance) for field in instance._meta.fields}


class QuerysetLoader(BaseLoader):
    """A Custom Jubatus Data Loader for Django queryset object."""
    def __init__(self, queryset, *args, **kwargs):
        self.queryset = queryset
        super().__init__(*args, **kwargs)

    def rows(self):
        for obj in self.queryset:
            yield model_to_dict(obj)


class ModelLoader(BaseLoader):
    """A Custom Jubatus Data Loader for Django model object."""
    def __init__(self, instance, *args, **kwargs):
        self.instance = [instance]
        super().__init__(*args, **kwargs)

    def rows(self):
        for obj in self.instance:
            yield model_to_dict(obj)
