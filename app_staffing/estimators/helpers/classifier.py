
def is_not_successfully_predicted(label_score_sorted):
    """helper for Jubatus classifier.
    Jubatus classifier returns same score between all labels if classifier is not trained.

    :param label_score_sorted: one of the return value of jubakit.Classifier.classify
     ex: <class 'list'>: [('person', 0.5684308029544834), ('job', -0.07227155162309501), ('other', -0.49664429530201293)]
    :type label_score_sorted: list of tuple
    :rtype bool
    """
    first_score = label_score_sorted[0][1]
    label_scores = [x[1] for x in label_score_sorted]
    return all(value == first_score for value in label_scores)
