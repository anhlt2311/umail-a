"""This is a batch training job for machine learning models.
Call this module via admin shell with cron-like scheduler.
"""
from django.db.transaction import atomic

from jubakit.classifier import Classifier
from jubakit.classifier import Dataset

from app_staffing.models.email import Email
from app_staffing.estimators.helpers.classifier import is_not_successfully_predicted
from app_staffing.estimators.loaders.models import QuerysetLoader, ModelLoader
from app_staffing.estimators.schemas.classifier import EmailSchema

from django.conf import settings


# TODO: Create Interface or abstract class before increasing the number of classes.
class EmailClassifier(object):
    _model_class = Email
    _schema = EmailSchema

    def __init__(self):
        self.service = Classifier(settings.MAIL_CLASSIFIER_HOST, int(settings.MAIL_CLASSIFIER_PORT))

    def _represent_prediction_result(self):
        """Convert the raw result to another one based on the Interface or abstract class."""
        raise NotImplementedError

    @atomic
    def batch_train(self):
        """This method trains remote classifier with human labeled data.
        Transaction between RDBMS and Jubatus is not supported,
        So it is highly recommended to use a classification algorithm which is robust to data duplication
        """

        # Train mail classifier with all data with human labeled data.
        training_data = self._model_class.objects.filter(category__isnull=False)

        loader = QuerysetLoader(queryset=training_data)
        dataset = Dataset(loader=loader, schema=self._schema).shuffle()
        # Train the classifier with every data in the dataset.
        for (idx, label) in self.service.train(dataset):
            pass
            # You can peek the datum being trained.
            # TODO: Use Logger instead of writing to stdout.
            # print("Labeled {0} to: {1}".format(dataset[idx][0], dataset.get(idx)))
        return

    def train(self, instance):
        loader = ModelLoader(instance=instance)
        dataset = Dataset(loader=loader, schema=self._schema)
        for (idx, label) in self.service.train(dataset):
            pass
            # You can peek the datum being trained.
            # TODO: Use Logger instead of writing to stdout.
            # print("Labeled {0} to: {1}".format(dataset[idx][0], dataset.get(idx)))
        return

    def batch_predict(self):
        # Predict mail data with all data with human labeled data.
        target_data = self._model_class.objects.filter(estimated_category__isnull=True)
        for instance in target_data:
            label, result = self.predict(instance)
            # TODO: Use a custom exception class.
            # Save estimated label to the Database record.
            instance.estimated_category = label
            instance.save()
        return

    def predict(self, instance):
        """
        :param instance: Django model object instance.
        :return: result: An list of tuple, that contains label and confidence.
          e.g [('person', 0.47727449762682617), ('job', 0.270650032913715), ('other', 0.2520754694594588)]
        :rtype: list of tuple
        """
        # Connect to an existing Jubatus service.
        loader = ModelLoader(instance=instance)
        dataset = Dataset(loader=loader, schema=self._schema)

        for (idx, label, result) in self.service.classify(dataset, softmax=True):
            # TODO: Use Logger instead of writing to stdout.
            # TODO: Use a custom exception class.
            estimated_label = result[0][0]
            label_score_sorted = result

            assert not is_not_successfully_predicted(label_score_sorted), \
                "All scores are exactly same. Maybe there is an inconsistency between given data and model"

            print('classify[{0}]: prediction: {1})'.format(idx, estimated_label))
            return estimated_label, label_score_sorted  # Currently, It only returns 1 item result.

    def reset(self):
        """Warning, this method delete all learned data from remote estimator."""
        self.service.clear()

    def __del__(self):
        if self.service:
            self.service.stop()
