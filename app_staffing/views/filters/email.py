from django.db.models import Q
import django_filters.rest_framework as django_filters
from app_staffing.views.filters.base import multiple_word_filter, datetime_range_lt_filter, datetime_range_gte_filter

def staff_filter(queryset, name, value):
    return _get_query(name, queryset, value)


def dummy_filter(queryset, name, value):
    return queryset

def email_comment_user_filter(queryset, name, value):
    return _get_query('emailcomment__created_user', queryset, value).distinct()

def _get_query(key, queryset, value):
    splited_values = value.split(',')
    queries = []
    for splited_value in splited_values:
        if 'not:' in splited_value :
            exclude_value = splited_value.split('not:')[1]
            queries.append(~Q(**{key: exclude_value}))
        else:
            queries.append(Q(**{key: splited_value}))

    query = queries.pop()
    for item in queries:
        query |= item

    return queryset.filter(query)

class EmailFilter(django_filters.FilterSet):
    from_name = django_filters.CharFilter(field_name='from_name', lookup_expr='icontains')
    from_address = django_filters.CharFilter(field_name='from_address', lookup_expr='icontains')
    subject = django_filters.CharFilter(field_name='subject', lookup_expr='icontains')
    subject_AND = django_filters.CharFilter(field_name='subject', method=multiple_word_filter(filter_type='AND'))
    text = django_filters.CharFilter(field_name='text', lookup_expr='icontains')
    text_AND = django_filters.CharFilter(field_name='text', method=multiple_word_filter(filter_type='AND'))
    has_attachments = django_filters.BooleanFilter(field_name='has_attachments')
    estimated_category = django_filters.CharFilter(field_name='estimated_category', lookup_expr='exact')
    sent_date_gte = django_filters.DateFilter(field_name='sent_date', method=datetime_range_gte_filter)
    sent_date_lte = django_filters.DateFilter(field_name='sent_date', method=datetime_range_lt_filter)
    staff = django_filters.CharFilter(field_name='staff_in_charge_id', method=staff_filter)
    ignore_filter = django_filters.CharFilter(method=dummy_filter)
    comment_user = django_filters.CharFilter(field_name='emailcomment__created_user_id', method=email_comment_user_filter)
