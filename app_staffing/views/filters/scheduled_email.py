import django_filters.rest_framework as django_filters
from django.db.models import Q

from app_staffing.models import ScheduledEmail, ScheduledEmailAttachment

from app_staffing.views.filters.base import multiple_word_filter, datetime_range_lt_filter, datetime_range_gte_filter

def status_filter(queryset, name, value):
    return _get_in_clause_query('scheduled_email_status__name', queryset, value)

def send_type_filter(queryset, name, value):
    return _get_in_clause_query('scheduled_email_send_type__name', queryset, value)

def text_format_filter(queryset, name, value):
    return _get_query('text_format', queryset, value)

def sender_filter(queryset, name, value):
    return _get_query('sender', queryset, value)

def created_user_filter(queryset, name, value):
    return _get_query('created_user', queryset, value)

def modified_user_filter(queryset, name, value):
    return _get_query('modified_user', queryset, value)

def _get_query(key, queryset, value):
    splited_values = value.split(',')
    queries = []
    for splited_value in splited_values:
        if 'not:' in splited_value :
            exclude_value = splited_value.split('not:')[1]
            queries.append(~Q(**{key: exclude_value}))
        else:
            queries.append(Q(**{key: splited_value}))

    query = queries.pop()
    for item in queries:
        query |= item

    return queryset.filter(query)

def _get_in_clause_query(key, queryset, value):
    splited_values = value.split(',')
    include_values = []
    exclude_values = []
    for splited_value in splited_values:
        if 'not:' in splited_value :
            exclude_value = splited_value.split('not:')[1]
            exclude_values.append(exclude_value)
        else:
            include_values.append(splited_value)

    expr = key + '__in'
    if include_values and exclude_values:
        # 非not と not を同時に指定した場合は OR で結合する
        return queryset.filter(Q(**{expr: include_values}) | ~Q(**{expr: exclude_values}))
    elif exclude_values:
        return queryset.exclude(**{expr: exclude_values})
    return queryset.filter(Q(**{expr: include_values}))

def attachments_filter(queryset, name, value):
    sea = ScheduledEmailAttachment

    if value == '1':
        sea_email_id = sea.objects.filter(email_id__in=[str(e.pk) for e in queryset], gcp_link__isnull=False).values("email_id").distinct()
    elif value == '2':
        sea_email_id = sea.objects.filter(email_id__in=[str(e.pk) for e in queryset], gcp_link__isnull=True).values("email_id").distinct()
    else:
        sea_email_id = sea.objects.filter(email_id__in=[str(e.pk) for e in queryset]).values("email_id").distinct()

    sea_email_id_str = [str(o['email_id']) for o in sea_email_id]

    return queryset.filter(id__in=sea_email_id_str)

def send_total_count_gte_filter(queryset, name, value):
    ret_queryset = queryset.filter(send_total_count__gte=value)
    return ret_queryset

def send_total_count_lte_filter(queryset, name, value):
    ret_queryset = queryset.filter(send_total_count__lte=value)
    return ret_queryset

def open_count_gte_filter(queryset, name, value):
    ret_queryset = queryset.filter(open_count__gte=value)
    return ret_queryset

def open_count_lte_filter(queryset, name, value):
    ret_queryset = queryset.filter(open_count__lte=value)
    return ret_queryset

class ScheduledEmailFilter(django_filters.FilterSet):
    sender_id = django_filters.CharFilter(field_name='sender', method=sender_filter)
    subject = django_filters.CharFilter(field_name='subject', method=multiple_word_filter(filter_type='AND'))
    status = django_filters.CharFilter(field_name='scheduled_email_status__name', method=status_filter)
    create_user_id = django_filters.UUIDFilter(field_name='created_user', lookup_expr='exact')
    date_to_send_gte = django_filters.DateFilter(field_name='date_to_send', method=datetime_range_gte_filter)
    date_to_send_lte = django_filters.DateFilter(field_name='date_to_send', method=datetime_range_lt_filter)
    send_type = django_filters.CharFilter(field_name='scheduled_email_send_type__name', method=send_type_filter)
    attachments = django_filters.CharFilter(field_name='attachments', method=attachments_filter)
    sent_date_gte = django_filters.DateFilter(field_name='sent_date', method=datetime_range_gte_filter)
    sent_date_lte = django_filters.DateFilter(field_name='sent_date', method=datetime_range_lt_filter)
    text_format = django_filters.CharFilter(field_name='text_format', method=text_format_filter)
    text = django_filters.CharFilter(field_name='text', method=multiple_word_filter(filter_type='AND'))
    send_total_count_gt = django_filters.NumberFilter(field_name='send_total_count_gt', method=send_total_count_gte_filter)
    send_total_count_lt = django_filters.NumberFilter(field_name='send_total_count_lt', method=send_total_count_lte_filter)
    open_count_gt = django_filters.NumberFilter(field_name='open_count_gt', method=open_count_gte_filter)
    open_count_lt = django_filters.NumberFilter(field_name='open_count_lt', method=open_count_lte_filter)
    created_user = django_filters.CharFilter(field_name='created_user_id', method=created_user_filter)
    modified_user = django_filters.CharFilter(field_name='modified_user_id', method=modified_user_filter)
    project_id = django_filters.UUIDFilter(field_name='project_histories__card', lookup_expr='exact')
    personnel_id = django_filters.UUIDFilter(field_name='personnel_histories__card', lookup_expr='exact')
