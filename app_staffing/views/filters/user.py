from django.db.models import Q
import django_filters.rest_framework as django_filters

from app_staffing.views.filters.base import datetime_range_lt_filter, datetime_range_gte_filter

def inactive_filter(queryset, name, value):
    if value == 'ignore_filter':
        return queryset
    return queryset.filter(is_active=True)

def role_filter(queryset, name, value):
    return _get_in_clause_query('user_role__name', queryset, value)

def _get_query(key, queryset, value):
    splited_values = value.split(',')
    queries = []
    for splited_value in splited_values:
        if 'not:' in splited_value :
            exclude_value = splited_value.split('not:')[1]
            queries.append(~Q(**{key: exclude_value}))
        else:
            queries.append(Q(**{key: splited_value}))

    query = queries.pop()
    for item in queries:
        query |= item

    return queryset.filter(query)

def _get_in_clause_query(key, queryset, value):
    splited_values = value.split(',')
    include_values = []
    exclude_values = []
    for splited_value in splited_values:
        if 'not:' in splited_value :
            exclude_value = splited_value.split('not:')[1]
            exclude_values.append(exclude_value)
        else:
            include_values.append(splited_value)

    expr = key + '__in'
    if include_values and exclude_values:
        # 非not と not を同時に指定した場合は OR で結合する
        return queryset.filter(Q(**{expr: include_values}) | ~Q(**{expr: exclude_values}))
    elif exclude_values:
        return queryset.exclude(**{expr: exclude_values})
    return queryset.filter(Q(**{expr: include_values}))

def full_name_filter(queryset, name, value):
    return queryset.filter(Q(first_name__icontains=value) | Q(last_name__icontains=value)).distinct()

class UserFilter(django_filters.FilterSet):
    id = django_filters.UUIDFilter(field_name='id', lookup_expr='exact')
    first_name = django_filters.CharFilter(field_name='first_name', lookup_expr='icontains')
    last_name = django_filters.CharFilter(field_name='last_name', lookup_expr='icontains')
    email = django_filters.CharFilter(field_name='email', lookup_expr='icontains')
    is_user_admin = django_filters.BooleanFilter(field_name='is_user_admin')
    is_active = django_filters.BooleanFilter(field_name='is_active')
    old_id = django_filters.NumberFilter(field_name='old_id', lookup_expr='exact')  # For data migration.
    role = django_filters.CharFilter(field_name='user_role__name', method=role_filter)
    inactive_filter = django_filters.CharFilter(method=inactive_filter)
    tel1 = django_filters.CharFilter(field_name='tel1', lookup_expr='icontains')
    tel2 = django_filters.CharFilter(field_name='tel2', lookup_expr='icontains')
    tel3 = django_filters.CharFilter(field_name='tel3', lookup_expr='icontains')
    last_login_gte = django_filters.DateFilter(field_name='last_login', method=datetime_range_gte_filter)
    last_login_lte = django_filters.DateFilter(field_name='last_login', method=datetime_range_lt_filter)
    full_name = django_filters.CharFilter(field_name='first_name', method=full_name_filter)
