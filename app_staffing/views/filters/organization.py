from django.db.models import Q
import django_filters.rest_framework as django_filters
from crum import get_current_user

from app_staffing.models import ContactScore, Tag, Company, ContactCategory

from app_staffing.utils.query_builder import generate_or_query

from app_staffing.views.filters.base import date_range_lte_filter

from logging import getLogger
logger = getLogger(__name__)

def _get_marketing_target_qs(queryset):
    base_lookup = {
        'contact__organization__is_blacklisted': False,
    }
    filtered_qs = queryset.filter(**base_lookup)

    return filtered_qs


def is_marketing_target(queryset, name, value):
    marketing_target_qs = _get_marketing_target_qs(queryset)

    if value is True:
        return marketing_target_qs
    elif value is False:
        return queryset.difference(marketing_target_qs)
    # else
    return queryset


def tag_and_filter(queryset, name, value):

    tags = value.split(',')

    for tag in tags:
        lookup_kwargs = {f'{name}__tag_id': tag, f'{name}__deleted_at__isnull': True,}
        queryset = queryset.filter(**lookup_kwargs)
    return queryset.distinct()

def tag_or_filter(queryset, name, value):

    tags = value.split(',')

    query_params = []
    for tag in tags:
        query_params.append({f'{name}__tag_id': tag, f'{name}__deleted_at__isnull': True,})

    return generate_or_query(queryset, query_params).distinct()


def user_score_filter(lookup_expr):
    def filter(queryset, name, value):
        """ This enables filtering on contact_score attribute that exists per Contact x User.

        :param queryset: An original queryset, that does not have filtered with this field name.
        :type queryset: django.db.models.QuerySet
        :param value: An Input value for filtering given from a view.
        :return: filtered_queryset: A new queryset that has filter statements.
        :rtype: django.db.models.QuerySet
        """
        expr = 'score__' + lookup_expr
        arguments = {expr: value, 'user': get_current_user()}
        inner_query_set = ContactScore.objects.filter(**arguments)

        expr = name + '__in'
        arguments = {expr: inner_query_set}
        queryset = queryset.filter(**arguments)

        return queryset

    return filter

def _get_user_category_filter_arguments(lookup_expr, queryset, name, value):
    expr = 'category__' + lookup_expr
    arguments = {expr: value, 'user': get_current_user()}
    inner_query_set = ContactCategory.objects.filter(**arguments)

    expr = name + '__in'
    return {expr: inner_query_set}


def user_category_include_filter(lookup_expr):
    def filter(queryset, name, value):
        arguments = _get_user_category_filter_arguments(lookup_expr, queryset, name, value)
        queryset = queryset.filter(**arguments)

        return queryset

    return filter

def user_category_exclude_filter(lookup_expr):
    def filter(queryset, name, value):
        arguments = _get_user_category_filter_arguments(lookup_expr, queryset, name, value)
        queryset = queryset.exclude(**arguments)

        return queryset

    return filter

def country_filter(queryset, name, value):
    return _get_in_clause_query('organization_country__name', queryset, value)

def category_filter(queryset, name, value):
    return _get_in_clause_query('organization_category__name', queryset, value)

def employee_number_filter(queryset, name, value):
    return _get_query('organization_employee_number__name', queryset, value)

def settlement_month_filter(queryset, name, value):
    return _get_query('settlement_month', queryset, value)

def staff_filter(queryset, name, value):
    return _get_query(name, queryset, value)

def created_user_filter(queryset, name, value):
    return _get_query('created_user', queryset, value)

def modified_user_filter(queryset, name, value):
    return _get_query('modified_user', queryset, value)

def organization_comment_user_filter(queryset, name, value):
    return _get_query('organizationcomment__created_user', queryset, value).distinct()

def contact_comment_user_filter(queryset, name, value):
    return _get_query('contactcomment__created_user', queryset, value).distinct()

def wants_location_filter(queryset, name, value):
    key_names = value.split(',')
    include_q_list = []
    exclude_q_list = []
    for key_name in key_names:
        if 'not:' in key_name :
            exclude_key_name = key_name.split('not:')[1]
            exclude_q_list.append(Q(**{f'contactpreference__{exclude_key_name}': False}))
        else:
            include_q_list.append(Q(**{f'contactpreference__{key_name}': True}))

    # in句と同じ条件になるので、論理和をとる
    include_query = None
    if include_q_list:
        include_query = include_q_list.pop()
        for q in include_q_list:
            include_query |= q

    # not in句と同じ条件になるので、否定の論理積(=論理和の否定)をとる
    exclude_query = None
    if exclude_q_list:
        exclude_query = exclude_q_list.pop()
        for q in exclude_q_list:
            exclude_query &= q

    if include_query is not None and exclude_query is not None:
        # 非not と not を同時に指定した場合は OR で結合する
        return queryset.filter(include_query | exclude_query)
    elif exclude_query is not None:
        return queryset.filter(exclude_query)
    return queryset.filter(include_query)

def _get_query(key, queryset, value):
    splited_values = value.split(',')
    queries = []
    for splited_value in splited_values:
        if 'not:' in splited_value :
            exclude_value = splited_value.split('not:')[1]
            queries.append(~Q(**{key: exclude_value}))
        else:
            queries.append(Q(**{key: splited_value}))

    query = queries.pop()
    for item in queries:
        query |= item

    return queryset.filter(query)

def _get_in_clause_query(key, queryset, value):
    splited_values = value.split(',')
    include_values = []
    exclude_values = []
    for splited_value in splited_values:
        if 'not:' in splited_value :
            exclude_value = splited_value.split('not:')[1]
            exclude_values.append(exclude_value)
        else:
            include_values.append(splited_value)

    expr = key + '__in'
    if include_values and exclude_values:
        # 非not と not を同時に指定した場合は OR で結合する
        return queryset.filter(Q(**{expr: include_values}) | ~Q(**{expr: exclude_values}))
    elif exclude_values:
        return queryset.exclude(**{expr: exclude_values})
    return queryset.filter(Q(**{expr: include_values}))

def capital_man_yen_gte_filter(queryset, name, value):
    ret_queryset = queryset.filter(capital_man_yen__gte=value)
    return ret_queryset

def capital_man_yen_lte_filter(queryset, name, value):
    ret_queryset = queryset.filter(capital_man_yen__lte=value)
    return ret_queryset

def capital_man_yen_required_for_transactions_gte_filter(queryset, name, value):
    ret_queryset = queryset.filter(capital_man_yen_required_for_transactions__gte=value)
    return ret_queryset

def capital_man_yen_required_for_transactions_lte_filter(queryset, name, value):
    ret_queryset = queryset.filter(capital_man_yen_required_for_transactions__lte=value)
    return ret_queryset

def establishment_year_gte_filter(queryset, name, value):
    ret_queryset = queryset.filter(establishment_year__gte=value)
    return ret_queryset

def establishment_year_lte_filter(queryset, name, value):
    ret_queryset = queryset.filter(establishment_year__lte=value)
    return ret_queryset

def dummy_filter(queryset, name, value):
    return queryset

def jobtypepreference_filter(queryset, name, value):
    ret_queryset = queryset.filter(contactjobtypepreferences__isnull=False)
    return ret_queryset.distinct()

def personneltypepreference_filter(queryset, name, value):
    ret_queryset = queryset.filter(contactpersonneltypepreferences__isnull=False)
    return ret_queryset.distinct()

def license_filter(queryset, name, value):
    splited_key_names = value.split(',')
    queries = []
    for splited_key_name in splited_key_names:
        queries.append(Q(**{splited_key_name: True}))

    query = queries.pop()
    for item in queries:
        query |= item

    return queryset.filter(query)

def license_required_for_transactions_filter(queryset, name, value):
    splited_key_names = value.split(',')
    queries = []
    for splited_key_name in splited_key_names:
        queries.append(Q(**{splited_key_name: True}))

    query = queries.pop()
    for item in queries:
        query |= item

    return queryset.filter(query)

def preference_filter(queryset, name, value):
    splited_key_names = value.split(',')
    queries = []
    for splited_key_name in splited_key_names:
        if splited_key_name == 'job':
            queries.append(Q(**{'contactjobtypepreferences__isnull': False}))
        elif splited_key_name == 'personnel':
            queries.append(Q(**{'contactpersonneltypepreferences__isnull': False}))

    query = queries.pop()
    for item in queries:
        query |= item

    return queryset.filter(query).distinct()

def with_organization_branch_filter(queryset, name, value):
    return queryset.filter(Q(**{f'{name}__icontains': value}) | Q(**{f'branches__{name}__icontains': value})).distinct()

class OrganizationFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')
    address = django_filters.CharFilter(field_name='address', lookup_expr='icontains')
    branch_address = django_filters.CharFilter(field_name='branches__address', lookup_expr='icontains')
    building = django_filters.CharFilter(field_name='building', lookup_expr='icontains')
    domain_name = django_filters.CharFilter(field_name='domain_name', lookup_expr='icontains')
    settlement_month = django_filters.CharFilter(field_name='settlement_month', method=settlement_month_filter)
    corporate_number = django_filters.CharFilter(field_name='corporate_number', lookup_expr='icontains')
    country = django_filters.CharFilter(field_name='organization_country__name', method=country_filter)
    category = django_filters.CharFilter(field_name='organization_category__name', method=category_filter)
    employee_number = django_filters.CharFilter(field_name='organization_employee_number__name', method=employee_number_filter)
    contract = django_filters.BooleanFilter(field_name='contract')
    establishment_year = django_filters.NumberFilter(field_name='establishment_year')
    has_distribution = django_filters.BooleanFilter(field_name='has_distribution')
    is_blacklisted = django_filters.BooleanFilter(field_name='is_blacklisted')
    old_id = django_filters.NumberFilter(field_name='old_id')
    capital_man_yen_gt = django_filters.NumberFilter(field_name='capital_man_yen_gt', method=capital_man_yen_gte_filter)
    capital_man_yen_lt = django_filters.NumberFilter(field_name='capital_man_yen_lt', method=capital_man_yen_lte_filter)
    ignore_filter = django_filters.CharFilter(method=dummy_filter)
    ignore_blocklist_filter = django_filters.CharFilter(method=dummy_filter)
    content = django_filters.CharFilter(method=dummy_filter)
    is_important = django_filters.BooleanFilter(method=dummy_filter)
    establishment_date_gte = django_filters.DateFilter(field_name='establishment_date', lookup_expr='gte')
    establishment_date_lte = django_filters.DateFilter(field_name='establishment_date', method=date_range_lte_filter)
    license = django_filters.CharFilter(method=license_filter)
    license_required_for_transactions = django_filters.CharFilter(method=license_required_for_transactions_filter)
    tel1 = django_filters.CharFilter(field_name='tel1', lookup_expr='icontains')
    tel2 = django_filters.CharFilter(field_name='tel2', lookup_expr='icontains')
    tel3 = django_filters.CharFilter(field_name='tel3', lookup_expr='icontains')
    fax1 = django_filters.CharFilter(field_name='fax1', lookup_expr='icontains')
    fax2 = django_filters.CharFilter(field_name='fax2', lookup_expr='icontains')
    fax3 = django_filters.CharFilter(field_name='fax3', lookup_expr='icontains')
    branch_tel1 = django_filters.CharFilter(field_name='branches__tel1', lookup_expr='icontains')
    branch_tel2 = django_filters.CharFilter(field_name='branches__tel2', lookup_expr='icontains')
    branch_tel3 = django_filters.CharFilter(field_name='branches__tel3', lookup_expr='icontains')
    branch_fax1 = django_filters.CharFilter(field_name='branches__fax1', lookup_expr='icontains')
    branch_fax2 = django_filters.CharFilter(field_name='branches__fax2', lookup_expr='icontains')
    branch_fax3 = django_filters.CharFilter(field_name='branches__fax3', lookup_expr='icontains')
    score_gte = django_filters.NumberFilter(field_name='score', lookup_expr='gte')
    score_eq = django_filters.NumberFilter(field_name='score', lookup_expr='exact')
    branch_name = django_filters.CharFilter(field_name='branches__name', lookup_expr='icontains', distinct=True)
    capital_man_yen_required_for_transactions_gt = django_filters.NumberFilter(field_name='capital_man_yen_required_for_transactions_gt', method=capital_man_yen_required_for_transactions_gte_filter)
    capital_man_yen_required_for_transactions_lt = django_filters.NumberFilter(field_name='capital_man_yen_required_for_transactions_lt', method=capital_man_yen_required_for_transactions_lte_filter)
    establishment_year_gt = django_filters.NumberFilter(field_name='establishment_year_gt', method=establishment_year_gte_filter)
    establishment_year_lt = django_filters.NumberFilter(field_name='establishment_year_lt', method=establishment_year_lte_filter)
    comment_user = django_filters.CharFilter(field_name='organizationcomment__created_user_id', method=organization_comment_user_filter)
    created_user = django_filters.CharFilter(field_name='created_user_id', method=created_user_filter)
    modified_user = django_filters.CharFilter(field_name='modified_user_id', method=modified_user_filter)
    index = django_filters.NumberFilter(method=dummy_filter)

class ContactFilter(django_filters.FilterSet):
    # Note: Case-sensitive filters won't work correctly in a sqlite3 environment.
    last_name = django_filters.CharFilter(field_name='last_name', lookup_expr='icontains')
    first_name = django_filters.CharFilter(field_name='first_name', lookup_expr='icontains')
    email = django_filters.CharFilter(field_name='email', lookup_expr='icontains')
    position = django_filters.CharFilter(field_name='position', lookup_expr='icontains')
    department = django_filters.CharFilter(field_name='department', lookup_expr='icontains')
    organization__name = django_filters.CharFilter(field_name='organization__name', lookup_expr='icontains')
    cc_addresses__email = django_filters.CharFilter(field_name='cc_addresses__email', lookup_expr='icontains', distinct=True)
    staff = django_filters.CharFilter(field_name='staff_id', method=staff_filter)
    old_id = django_filters.NumberFilter(field_name='old_id')
    last_visit_gte = django_filters.DateFilter(field_name='last_visit', lookup_expr='gte')
    last_visit_lte = django_filters.DateFilter(field_name='last_visit', method=date_range_lte_filter)
    score_gte = django_filters.NumberFilter(field_name='scores', method=user_score_filter(lookup_expr='gte'))
    score_eq = django_filters.NumberFilter(field_name='scores', method=user_score_filter(lookup_expr='exact'))
    tel1 = django_filters.CharFilter(field_name='tel1', lookup_expr='icontains')
    tel2 = django_filters.CharFilter(field_name='tel2', lookup_expr='icontains')
    tel3 = django_filters.CharFilter(field_name='tel3', lookup_expr='icontains')
    tags_and = django_filters.CharFilter(field_name='tag_assignment', method=tag_and_filter, distinct=True)
    tags_or = django_filters.CharFilter(field_name='tag_assignment', method=tag_or_filter, distinct=True)
    preference = django_filters.CharFilter(method=preference_filter)
    category_eq = django_filters.CharFilter(field_name='categories', method=user_category_include_filter(lookup_expr='exact'))
    category_not_eq = django_filters.CharFilter(field_name='categories', method=user_category_exclude_filter(lookup_expr='exact'))
    wants_location = django_filters.CharFilter(field_name='wants_location', method=wants_location_filter)
    ignore_filter = django_filters.CharFilter(method=dummy_filter)
    ignore_blocklist_filter = django_filters.CharFilter(method=dummy_filter)
    comment_user = django_filters.CharFilter(field_name='contactcomment__created_user_id', method=contact_comment_user_filter)
    created_user = django_filters.CharFilter(field_name='created_user_id', method=created_user_filter)
    modified_user = django_filters.CharFilter(field_name='modified_user_id', method=modified_user_filter)
    index = django_filters.NumberFilter(method=dummy_filter)
    organization = django_filters.CharFilter(field_name='organization', lookup_expr='exact')


class ContactPreferenceFilter(django_filters.FilterSet):

    def __init__(self, data=None, *args, **kwargs):
        # Setting default value on some filter fields.
        if data is not None:
            # get a mutable copy of the QueryDict
            data = data.copy()
            data['is_marketing_target'] = data.get('is_marketing_target', True)  # Setting default value: True.
        super().__init__(data=data, *args, **kwargs)

    searchtype = django_filters.BooleanFilter(method=dummy_filter)
    jobtype = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_dev = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_infra = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_other = django_filters.BooleanFilter(method=dummy_filter)

    wants_location_hokkaido_japan = django_filters.BooleanFilter(method=dummy_filter)
    wants_location_touhoku_japan = django_filters.BooleanFilter(method=dummy_filter)
    wants_location_kanto_japan = django_filters.BooleanFilter(method=dummy_filter)
    wants_location_kansai_japan = django_filters.BooleanFilter(method=dummy_filter)
    wants_location_chubu_japan = django_filters.BooleanFilter(method=dummy_filter)
    wants_location_kyushu_japan = django_filters.BooleanFilter(method=dummy_filter)
    wants_location_other_japan = django_filters.BooleanFilter(method=dummy_filter)
    wants_location_chugoku_japan = django_filters.BooleanFilter(method=dummy_filter)
    wants_location_shikoku_japan = django_filters.BooleanFilter(method=dummy_filter)
    wants_location_toukai_japan = django_filters.BooleanFilter(method=dummy_filter)

    job_koyou_proper = django_filters.BooleanFilter(field_name='job_koyou_proper', lookup_expr='exact')
    job_koyou_free = django_filters.BooleanFilter(field_name='job_koyou_free', lookup_expr='exact')
    job_syouryu = django_filters.NumberFilter(field_name='job_syouryu', lookup_expr='lte')
    personnel_syouryu = django_filters.NumberFilter(field_name='personnel_syouryu', lookup_expr='lte')
    personnel_country_japan = django_filters.BooleanFilter(field_name='personnel_country_japan', lookup_expr='exact')
    personnel_country_other = django_filters.BooleanFilter(field_name='personnel_country_other', lookup_expr='exact')
    contact__score_gte = django_filters.NumberFilter(field_name='contact__scores', method=user_score_filter(lookup_expr='gte'))
    contact__category_eq = django_filters.CharFilter(field_name='contact__categories', method=user_category_include_filter(lookup_expr='exact'))
    contact__category_not_eq = django_filters.CharFilter(field_name='contact__categories', method=user_category_exclude_filter(lookup_expr='exact'))
    contact__tags_and = django_filters.CharFilter(field_name='contact__tag_assignment', method=tag_and_filter, distinct=True)
    contact__tags_or = django_filters.CharFilter(field_name='contact__tag_assignment', method=tag_or_filter, distinct=True)
    contact__staff = django_filters.UUIDFilter(field_name='contact__staff__id', lookup_expr='exact')
    contact__organization__category_prospective = django_filters.BooleanFilter(method=dummy_filter)
    contact__organization__category_approached = django_filters.BooleanFilter(method=dummy_filter)
    contact__organization__category_exchanged = django_filters.BooleanFilter(method=dummy_filter)
    contact__organization__category_client = django_filters.BooleanFilter(method=dummy_filter)
    contact__organization__organization_country_jp = django_filters.BooleanFilter(method=dummy_filter)
    contact__organization__organization_country_kr = django_filters.BooleanFilter(method=dummy_filter)
    contact__organization__organization_country_cn = django_filters.BooleanFilter(method=dummy_filter)
    contact__organization__organization_country_other = django_filters.BooleanFilter(method=dummy_filter)
    contact__organization__contract = django_filters.BooleanFilter(field_name='contact__organization__contract', lookup_expr='exact')

    jobtype_dev_designer = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_dev_front = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_dev_server = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_dev_pm = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_dev_other = django_filters.BooleanFilter(method=dummy_filter)

    jobtype_infra_server = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_infra_network = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_infra_security = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_infra_database = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_infra_sys = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_infra_other = django_filters.BooleanFilter(method=dummy_filter)

    jobtype_other_eigyo = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_other_kichi = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_other_support = django_filters.BooleanFilter(method=dummy_filter)
    jobtype_other_other = django_filters.BooleanFilter(method=dummy_filter)

    jobskill_dev_youken = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_dev_kihon = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_dev_syousai = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_dev_seizou = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_dev_test = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_dev_hosyu = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_dev_beginner = django_filters.BooleanFilter(method=dummy_filter)

    jobskill_infra_youken = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_infra_kihon = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_infra_syousai = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_infra_kouchiku = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_infra_test = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_infra_hosyu = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_infra_kanshi = django_filters.BooleanFilter(method=dummy_filter)
    jobskill_infra_beginner = django_filters.BooleanFilter(method=dummy_filter)

    personneltype_dev_designer = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_dev_front = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_dev_server = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_dev_pm = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_dev_other = django_filters.BooleanFilter(method=dummy_filter)

    personneltype_infra_server = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_infra_network = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_infra_security = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_infra_database = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_infra_sys = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_infra_other = django_filters.BooleanFilter(method=dummy_filter)

    personneltype_other_eigyo = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_other_kichi = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_other_support = django_filters.BooleanFilter(method=dummy_filter)
    personneltype_other_other = django_filters.BooleanFilter(method=dummy_filter)

    personnelskill_dev_youken = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_dev_kihon = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_dev_syousai = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_dev_seizou = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_dev_test = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_dev_hosyu = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_dev_beginner = django_filters.BooleanFilter(method=dummy_filter)

    personnelskill_infra_youken = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_infra_kihon = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_infra_syousai = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_infra_kouchiku = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_infra_test = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_infra_hosyu = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_infra_kanshi = django_filters.BooleanFilter(method=dummy_filter)
    personnelskill_infra_beginner = django_filters.BooleanFilter(method=dummy_filter)

    contact__old_id = django_filters.NumberFilter(field_name='contact__old_id')
    is_marketing_target = django_filters.BooleanFilter(method=is_marketing_target)

class TagFilter(django_filters.FilterSet):
    id = django_filters.CharFilter(field_name='id', lookup_expr='exact')
    value = django_filters.CharFilter(field_name='value', lookup_expr='icontains')
    internal_value = django_filters.CharFilter(field_name='internal_value', lookup_expr='icontains')

class OrganizationNameFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', method=with_organization_branch_filter)
