import django_filters.rest_framework as django_filters
import json


def inactive_filter(queryset, name, value):
    if value == 'ignore_filter':
        return queryset
    return queryset.filter(is_active=True)

def rules_filter(queryset, name, value):
    for rule in json.loads(value):
        if not rule['target_type'] or not rule['condition_type'] or not rule['value']:
            continue

        queryset = queryset.filter(
            conditions__target_type=rule['target_type'],
            conditions__condition_type=rule['condition_type'],
            conditions__value__icontains=rule['value'],
            conditions__deleted_at__isnull=True
        )
    return queryset.distinct()

def dummy_filter(queryset, name, value):
    return queryset

class EmailNotificationRuleFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')
    master_rule = django_filters.CharFilter(field_name='master_rule', lookup_expr='icontains')
    inactive_filter = django_filters.CharFilter(method=inactive_filter)
    rules = django_filters.CharFilter(method=rules_filter)

class SystemNotificationFilter(django_filters.FilterSet):
    unread = django_filters.BooleanFilter(method=dummy_filter)
