import traceback

from rest_framework.response import Response
from rest_framework.status import HTTP_500_INTERNAL_SERVER_ERROR
from rest_framework.views import exception_handler

from django.conf import settings

from app_staffing.utils.logger import get_info_logger, get_error_logger
logger_stderr = get_error_logger(__name__)
logger_stdout = get_info_logger(__name__)


def parsable_as_integer(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def recast(data):
    """A workaround for poor implementation of the default exception handler.
    Since Django rest framework's error handler forces all error details to str,
    We have to cast back again if we want to include an integer data to the error response.
    Note: Currently, nested error detail is not supported.
    """
    for key, value in data.items():
        if isinstance(value, str) and parsable_as_integer(value):
            data[key] = int(value)
    return data


def custom_exception_handler(exc, context):
    # ステータスコードが取れない(想定外のエラー等でAPIExceptionを継承していないエラー)またはステータスコードが500(API側で明示的に500でスローしたエラー)
    status_code = getattr(exc, 'status_code', None)
    if not status_code or 500 <= exc.status_code:
        msg = ''
        if context['request']:
            # リクエストURLのパスをログに追加
            msg = msg + 'path = {0} : '.format(context['request'].get_full_path())

            # アクセスユーザのIDをログに追加
            user = getattr(context['request'], 'user', None)
            user_id = getattr(user, 'id', None)
            if user and user_id:
                msg = msg + 'user_id = {0} : '.format(str(user_id))

            # アクセスユーザのテナントIDをログに追加
            company = getattr(user, 'company', None)
            if company:
                msg = msg + 'company_id = {0} : '.format(str(company.id))

            # リクエストパラメータをログに追加
            data = getattr(context['request'], 'data', None)
            if data:
                msg = msg + 'request_data = {0} : '.format(str(data))

        # スタックトレースをログに追加
        msg = msg + traceback.format_exc()

        # エラーログ出力
        logger_stderr.error(msg)
    else:
        logger_stdout.info(traceback.format_exc())

    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)
    if settings.SHOW_STACK:
        traceback.print_exc()

    if response is not None:
        response.data = recast(response.data)
        return response
    else:  # By default, Django returns HTML response, but it is not suitable for API,
        # so create custom JSON response here.
        return Response({'detail': settings.DEFAULT_ERROR_MESSAGE}, status=HTTP_500_INTERNAL_SERVER_ERROR)
