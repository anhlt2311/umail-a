from rest_framework.exceptions import APIException

class AddonRevokeException(APIException):
    status_code = 400
    default_detail = 'アドオンの解約に失敗しました。'
    default_code = 'addon_revoke_failed'
