import json

from django.conf import settings
from django.contrib.auth import get_user_model
from django.templatetags.static import static
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from app_staffing.models.organization import CompanyAttribute
from rest_framework.exceptions import APIException
from rest_framework.generics import ListAPIView, CreateAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from app_staffing.models.addon import Addon, AddonMaster
from app_staffing.serializers.addon import AddonSerializer, AddonMasterSerializer
from app_staffing.views.base import CustomTokenAuthentication, SessionAuthentication, IsAuthorizedAction, DeleteErrorHandlerMixin, MultiTenantMixin
from app_staffing.models.user import User

from app_staffing.models.email import ScheduledEmailSetting, ScheduledEmail
from django.db.transaction import atomic

from datetime import timedelta, datetime
from app_staffing.models.plan import Plan
from app_staffing.models.purchase_history import PurchaseHistory

from django.utils import timezone

import payjp
import pytz

from app_staffing.exceptions.addon import AddonPurchaseUnknownException, CommentTemplateAddonRevokeException, SearchTemplateAddonRevokeException, FreeTrialException,\
    PurchaseCountLimitException, ParentNotPurchasedException, AddonPurchasePaymentException, AddonPurchasePaymentUnknownException,\
    ChildNotDeletedException, PlanNotFoundException, MultiActivePlanFoundException
from app_staffing.exceptions.base import CardNotRegisteredException
from app_staffing.views.exceptions.addon import AddonRevokeException
from app_staffing.utils.payment import exec_charge, get_taxed_price
from app_staffing.utils.cache import get_search_template_cache_key
from app_staffing.utils.system_mail import send_system_mail

from django.db.models import Count
from app_staffing.utils.addon import scheduled_email_limit_target_count

from app_staffing.utils.logger import get_info_logger, get_error_logger
logger_stderr = get_error_logger(__name__)
logger_stdout = get_info_logger(__name__)


class AddonMasterListAPIView(ListAPIView):
    serializer_class = AddonMasterSerializer
    queryset = AddonMaster.objects.all().order_by('order')


class AddonPurchasedListAPIView(MultiTenantMixin, ListAPIView):
    serializer_class = AddonSerializer
    queryset = Addon.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            active_plan = Plan.objects.get(company=self.request.user.company, is_active=True)
        except ObjectDoesNotExist:
            active_plan = None

        addons = Addon.objects.filter(company=self.request.user.company)
        ret = []
        for addon in addons:
            ret.append({
                "id": addon.id,
                "next_payment_date": active_plan.payment_date if active_plan else None,
                "addon_master_id": addon.addon_master_id,
            })

        return Response(status=status.HTTP_200_OK, data=ret)


class AddonOperationHelper(object):
    def _update_use_open_count(self, request, addon_master_id, value):
        if addon_master_id == settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID:
            try:
                scheduled_email_setting = ScheduledEmailSetting.objects.get(company=request.user.company)
            except ObjectDoesNotExist:
                return False

            scheduled_email_setting.use_open_count = value
            scheduled_email_setting.save()
    
    def _update_use_open_count_extra_period(self, request, addon_master_id, value):
        if addon_master_id == settings.IS_OPEN_COUNT_EXTRA_PERIOD_AVAILABLE_ADDON_MASTER_ID:
            try:
                scheduled_email_setting = ScheduledEmailSetting.objects.get(company=request.user.company)
            except ObjectDoesNotExist:
                return False

            scheduled_email_setting.use_open_count_extra_period = value
            scheduled_email_setting.save()

    def _update_use_attachment_max_size(self, request, addon_master_id, value):
        if addon_master_id == settings.SCHEDULED_EMAIL_BYTE_SIZE_ADDON_MASTER_ID:
            try:
                scheduled_email_setting = ScheduledEmailSetting.objects.get(company=request.user.company)
            except ObjectDoesNotExist:
                return False

            scheduled_email_setting.use_attachment_max_size = value
            scheduled_email_setting.save()

    def _update_use_remove_promotion(self, request, addon_master_id, value):
        if addon_master_id == settings.REMOVE_PROMOTION_ADDON_MASTER_ID:
            try:
                scheduled_email_setting = ScheduledEmailSetting.objects.get(company=request.user.company)
            except ObjectDoesNotExist:
                return False

            scheduled_email_setting.use_remove_promotion = value
            scheduled_email_setting.save()


class AddonPurchaseAPIView(MultiTenantMixin, CreateAPIView, AddonOperationHelper):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, IsAuthorizedAction)
    serializer_class = AddonSerializer
    queryset = Addon.objects.all()

    def _exec_charge(self, addon_master):
        # card番号取得する
        payjp.api_key = settings.PAYJP_API_KEY
        customer = payjp.Customer.retrieve(str(self.request.user.company_id))
        if customer['cards']['data'] and 0 < len(customer['cards']['data']):
            last4 = customer['cards']['data'][0]['last4']
        else:
            raise CardNotRegisteredException()

        # プラン情報を取得する。見つからなかったり、複数見つかったりした場合はエラー
        active_plans = Plan.objects.filter(company_id=self.request.user.company_id, is_active=True)
        if not active_plans or 0 == len(active_plans):
            raise PlanNotFoundException()
        elif 1 < len(active_plans):
            raise MultiActivePlanFoundException()

        # プランは必ず一つなので先頭を取得
        current_active_plan = active_plans[0]

        # 税率を乗算する
        taxed_price = get_taxed_price(addon_master.price)

        # PurchaseHistoryをCreateする
        PurchaseHistory.objects.create(
            company_id=self.request.user.company_id,
            period_start=timezone.now(),
            period_end=current_active_plan.expiration_date,
            addon_ids=addon_master.id,
            card_last4=last4,
            price=taxed_price,
            method=settings.PURCHASE_HISTORY_METHOD_CREDIT_CARD,
        )

        return exec_charge(
            taxed_price,
            self.request.user.company,
            f'purchase addon master : {addon_master.id}'
        )

    @atomic
    def perform_create(self, serializer):
        try:
            # トライアル期間中ならばエラーにする
            if CompanyAttribute.objects.filter(company_id=self.request.user.company_id, trial_expiration_date__gte=datetime.now()).exists():
                raise FreeTrialException()

            addon_master = AddonMaster.objects.get(id=self.request.data.get("addon_master_id"))

            # 購入可能回数を超えていないか確認
            purcheased_count = Addon.objects.filter(company_id=self.request.user.company_id, addon_master_id=addon_master.id).count()
            if addon_master.limit <= purcheased_count:
                raise PurchaseCountLimitException()
            
            # 購入対象アドオンに親が紐づいていた場合、親アドオンを購入していない場合は購入できない
            if addon_master.parent_assignment.exists():
                for parent_assinment in addon_master.parent_assignment.all():
                    if not Addon.objects.filter(company_id=self.request.user.company_id, addon_master_id=parent_assinment.parent_addon_master.id).exists():
                        raise ParentNotPurchasedException()

            # 開封数取得のアドオンを購入した場合、配信メール設定側もONにする
            self._update_use_open_count(self.request, addon_master.id, True)

            # 配信容量のアドオンを購入した場合、配信メール設定側もONにする
            self._update_use_attachment_max_size(self.request, addon_master.id, True)

            # 広告除去のアドオンを購入した場合、配信メール設定側もONにする
            self._update_use_remove_promotion(self.request, addon_master.id, True)

            # 開封タグ有効期間のアドオンを購入した場合、配信メール設定側もONにする
            self._update_use_open_count_extra_period(self.request, addon_master.id, True)

            # アドオン購入情報の保存
            created_item = serializer.save(company=self.request.user.company, addon_master_id=addon_master.id)

            # 有効化処理が全て完了したら支払い処理を実行する(PayjpのAPIを実行)
            is_success, err_with_main, err_with_backup = self._exec_charge(addon_master)

            if not is_success:
                try:
                    if err_with_backup is None:
                        raise err_with_main
                    else:
                        logger_stderr.error('payment with main card: Company: {0} Exception occurred. ErrorDetails:{1}'.format(self.request.user.company_id, err_with_main))
                        raise err_with_backup
                except payjp.error.CardError as err:
                    error_code = err.json_body['error']['code']
                    if error_code in settings.PAYJP_CHARGE_ERROR_MESSAGES:
                        raise AddonPurchasePaymentException(detail=settings.PAYJP_CHARGE_ERROR_MESSAGES[error_code])
                    else:
                        raise AddonPurchasePaymentException()
                except Exception as err:
                    raise AddonPurchasePaymentUnknownException()
        
            try:
                context = {
                    'addon_name': addon_master.title,
                    'url': settings.HOST_URL,
                }
                if is_success:
                    # 決済が成功した場合
                    send_system_mail(self.request.user.company_id, settings.TEMPLATE_EMAIL_ADDON_PURCHASE_SUBJECT, settings.TEMPLATE_EMAIL_ADDON_PURCHASE_MESSAGE, context)
                    if err_with_main is not None:
                        # 予備カードで決済した場合
                        send_system_mail(self.request.user.company_id, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_SUBJECT, settings.TEMPLATE_EMAIL_PAYMENT_SUCCEEDED_WITH_BACKUP_MESSAGE, context)
            except Exception as e:
                logger_stderr.error('addon purchased notification: Company: {0} Exception occurred. ErrorDetails:{1}'.format(self.request.user.company_id, e))
        
            # 全て成功した場合はsaveしたアドオン購入情報を返す
            return created_item
        except (
            FreeTrialException, PurchaseCountLimitException, ParentNotPurchasedException, AddonPurchasePaymentException, AddonPurchasePaymentUnknownException
        ) as e:
            # 既知のエラーに関してはそのままRaiseする
            raise e
        except Exception as e:
            # 未知のエラーについては共通の500エラーではなく、専用のエラーをraiseする
            raise AddonPurchaseUnknownException()


class AddonRevokeAPIView(DeleteErrorHandlerMixin, DestroyAPIView, AddonOperationHelper):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, IsAuthorizedAction)
    queryset = Addon.objects.all()

    def _check_comment_template_register_count(self, addon):
        if addon.addon_master_id != settings.COMMENT_TEMPLATE_ADDON_ID:
            return False

        purchase_count = Addon.objects.filter(company=self.request.user.company, addon_master_id=settings.COMMENT_TEMPLATE_ADDON_ID).count()

        if purchase_count <= 0:
            return False

        after_count = purchase_count - 1
        after_additional_count = settings.COMMENT_TEMPLATE_ADDON_ADDITIONAL_COUNT * after_count
        after_total_available_count = settings.COMMENT_TEMPLATE_DEFAULT_LIMIT + after_additional_count
        error_resource_names = []
        for resource_name, name_ja in settings.COMMENT_TEMPLATE_RESOURCE_NAMES.items():
            cache_key = f'{settings.CACHE_COMMENT_TEMPLATE_KEY_BASE}_{self.request.user.company.id}_{resource_name}'
            template_registered_count = len(cache.get(cache_key, settings.CACHE_COMMENT_TEMPLATE_DEFAULT_VALUE[resource_name]))
            if after_total_available_count < template_registered_count:
                error_resource_names.append(name_ja)

        if 0 < len(error_resource_names):
            raise AddonRevokeException()
        else:
            return False
    
    def _check_search_template_register_count(self, addon):
        if addon.addon_master_id != settings.SEARCH_TEMPLATE_ADDON_ID:
            return False

        purchase_count = Addon.objects.filter(company=self.request.user.company, addon_master_id=settings.SEARCH_TEMPLATE_ADDON_ID).count()

        if purchase_count <= 0:
            return False

        after_count = purchase_count - 1
        after_additional_count = settings.SEARCH_TEMPLATE_ADDON_ADDITIONAL_COUNT * after_count
        after_total_available_count = settings.SEARCH_TEMPLATE_DEFAULT_LIMIT + after_additional_count
        error_user_and_resource_names = []

        for user in User.objects.filter(company=self.request.user.company):
            error_resource_names = []
            for resource_name, name_ja in settings.SEARCH_TEMPLATE_RESOURCE_NAMES.items():
                cache_key = get_search_template_cache_key(self.request.user.company.id, user.id, resource_name)
                template_registered_count = len(cache.get(cache_key, []))
                if after_total_available_count < template_registered_count:
                    error_resource_names.append(name_ja)
            if 0 < len(error_resource_names):
                error_user_and_resource_names.append({'user_name': user.display_name, 'resource_names': error_resource_names})

        if 0 < len(error_user_and_resource_names):
            error_messages = []
            for error_user_and_resource_name in error_user_and_resource_names:
                error_messages.append(
                    settings.SEARCH_TEMPLATE_ADDON_REVOKE_ERROR_MESSAGE.format(
                        error_user_and_resource_name['user_name'],
                        ','.join(error_user_and_resource_name['resource_names']), 
                        str(after_total_available_count)
                    )
                )
            raise AddonRevokeException()
        else:
            return False

    def _check_delivery_interval_addon_deletable(self, addon_master_id):
        if addon_master_id != settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_ADDON_MASTER_ID:
            return True

        scheduled_emails = ScheduledEmail.objects.filter(
            company=self.request.user.company,
            scheduled_email_status__name__in=[
                'draft', # 下書き
                'queued', # 配信待ち
                'sending', # 送信中
            ]
        )
        not_allowed_miutes = settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_NOT_ALLOWED_MINUTES
        for scheduled_email in scheduled_emails:
            if not scheduled_email.date_to_send:
                continue
            selected_minute = scheduled_email.date_to_send.astimezone(pytz.timezone(settings.TIME_ZONE)).minute
            if selected_minute in not_allowed_miutes:
                raise AddonRevokeException()

        return True
    
    def _check_remove_promotion_addon_deletable(self, addon_master_id):
        if addon_master_id != settings.REMOVE_PROMOTION_ADDON_MASTER_ID:
            return True

        scheduled_email_exists =  ScheduledEmail.objects.filter(
            company=self.request.user.company,
            scheduled_email_status__name__in=[
                'draft', # 下書き
                'queued', # 配信待ち
                'sending', # 送信中
            ],
            text_format='html',
        ).exists()

        if scheduled_email_exists:
            raise AddonRevokeException()

        return True
    
    def _check_target_count_addon_deletable(self, addon_master_id):
        if addon_master_id != settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID:
            return True

        scheduled_emails = ScheduledEmail.objects.filter(
            company=self.request.user.company,
            scheduled_email_status__name__in=[
                'draft', # 下書き
                'queued', # 配信待ち
                'sending', # 送信中
            ]
        ).annotate(target_count=Count('targets'))

        target_count_after = scheduled_email_limit_target_count(self.request.user.company) - settings.SCHEDULED_EMAIL_DEFAULT_LIMIT_TARGET_COUNT

        for scheduled_email in scheduled_emails:
            if target_count_after < scheduled_email.target_count:
                raise AddonRevokeException()

        return True
    
    def _check_attachment_size_addon_deletable(self, addon_master_id):
        if addon_master_id != settings.SCHEDULED_EMAIL_BYTE_SIZE_ADDON_MASTER_ID:
            return True

        scheduled_emails = ScheduledEmail.objects.prefetch_related('attachments').filter(
            company=self.request.user.company,
            scheduled_email_status__name__in=[
                'draft', # 下書き
                'queued', # 配信待ち
                'sending', # 送信中
            ]
        )

        for scheduled_email in scheduled_emails:
            total_size = 0
            for attachment in scheduled_email.attachments.all():
                total_size += attachment.size
            if settings.SCHEDULED_EMAIL_MAX_BYTE_SIZE < total_size:
                raise AddonRevokeException()

        return True

    @atomic
    def delete(self, request, *args, **kwargs):
        try:
            addon = Addon.objects.get(id=kwargs["pk"])
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND, data={})
        
        # 削除対象アドオンに子が紐づいていた場合、子アドオンを削除していない場合は削除できない
        if addon.addon_master.child_assignment.exists():
            for child_assinment in addon.addon_master.child_assignment.all():
                if Addon.objects.filter(company_id=self.request.user.company_id, addon_master_id=child_assinment.addon_master.id).exists():
                    raise ChildNotDeletedException()

        # 広告除去のアドオンを削除した場合、配信メール設定側もOFFにする
        self._update_use_remove_promotion(request, addon.addon_master_id, False)

        # 配信容量のアドオンを削除した場合、配信メール設定側もOFFにする
        self._update_use_attachment_max_size(request, addon.addon_master_id, False)

        # コメントテンプレート登録数チェック
        self._check_comment_template_register_count(addon)

        # 開封数取得のアドオンを削除した場合、配信メール設定側もOFFにする
        self._update_use_open_count(request, addon.addon_master_id, False)

        # 検索条件テンプレート登録数チェック
        self._check_search_template_register_count(addon)

        # 開封タグ有効期間のアドオンを削除した場合、配信メール設定側もOFFにする
        self._update_use_open_count_extra_period(request, addon.addon_master_id, False)

        # 配信間隔のアドオン削除可能かのチェック
        self._check_delivery_interval_addon_deletable(addon.addon_master_id)

        # 広告削除のアドオンが削除可能かチェック
        self._check_remove_promotion_addon_deletable(addon.addon_master_id)

        # 送信可能件数のアドオンが削除可能かチェック
        self._check_target_count_addon_deletable(addon.addon_master_id)

        # 添付容量のアドオンが削除可能かチェック
        self._check_attachment_size_addon_deletable(addon.addon_master_id)

        context = {
            'addon_name': addon.addon_master.title,
        }
        send_system_mail(self.request.user.company_id, settings.TEMPLATE_EMAIL_ADDON_REVOKE_SUBJECT, settings.TEMPLATE_EMAIL_ADDON_REVOKE_MESSAGE, context)

        return super().delete(request, *args, **kwargs)
