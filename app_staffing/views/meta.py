from django.conf import settings

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny


class VersionCheckView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        data = {'revision': settings.REVISION_NUMBER}
        return Response(status=status.HTTP_200_OK, data=data)
