from rest_framework.pagination import PageNumberPagination
from rest_framework.exceptions import NotFound


class StandardPagenation(PageNumberPagination):
    """A customized page number pagination class
        Note: This customized class returns 404 Error if the provided page_number is larger than the last page,
        also, includes the last page number to an error response so that the frontend app can handle this error.
    """

    def paginate_queryset(self, queryset, request, view=None):
        try:
            return super().paginate_queryset(queryset, request, view=view)
        except NotFound as e:  # Intercept NotFound exception
            page_size = self.get_page_size(request)
            last_page_number = self.django_paginator_class(queryset, page_size).num_pages

            raise NotFound(detail={'detail': e.detail, 'lastPageNumber': last_page_number })

    page_size = 10000
    page_size_query_param = 'page_size'
    max_page_size = 1000000000


class TrainStationPagination(StandardPagenation):
    page_size = 30
    page_size_query_param = 'page_size'
    max_page_size = 30
