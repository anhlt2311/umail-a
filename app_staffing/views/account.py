from django.conf import settings
from app_staffing.models import User
from app_staffing.models.user import MASTER

from rest_framework import status
from rest_framework.exceptions import MethodNotAllowed, APIException
from rest_framework.response import Response

from datetime import datetime
from app_staffing.models import CompanyAttribute
from app_staffing.views.base import MultiTenantMixin, GenericListAPIView, UserHelper

import pytz

from logging import getLogger
from google.cloud.logging.handlers import ContainerEngineHandler
import sys
from app_staffing.utils.system_mail import send_system_mail

logger = getLogger(__name__)
logger.propagate = False
logger.addHandler(ContainerEngineHandler(stream=sys.stdout))

class AccountView(MultiTenantMixin, GenericListAPIView, UserHelper):

    def get(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='GET')

    def post(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PUT')

    def put(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PUT')

    def patch(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PATCH')
    
    def delete(self, request, *args, **kwargs):
        try:
            user = self.request.user
            company = user.company
            company.deactivated_time = datetime.now(pytz.timezone(settings.TIME_ZONE))
            company.save()

            # マスター以外のユーザーを削除
            not_master_users = User.objects.filter(company=company).exclude(user_role__name=MASTER)
            self.hide(not_master_users)

            # 退会メール送信
            send_system_mail(company.id, settings.TEMPLATE_EMAIL_ACCOUNT_DELETE_SUBJECT, settings.TEMPLATE_EMAIL_ACCOUNT_DELETE_MESSAGE)
        except Exception:
            raise AccountDeleteException
        return Response(status=status.HTTP_204_NO_CONTENT, data={})

class AccountDeleteException(APIException):
    status_code = 400
    default_detail = 'アカウントの削除に失敗しました。'
    default_code = 'account_delete_failed'
