from django.http import FileResponse
from django.db.transaction import atomic
from django.shortcuts import get_object_or_404
from django.db.models.deletion import ProtectedError
from app_staffing.board.exceptions.contract import NumberPersonnelContractSetOrganizationContactException
from djangorestframework_camel_case.render import CamelCaseJSONRenderer, CamelCaseBrowsableAPIRenderer
from djangorestframework_camel_case.parser import CamelCaseJSONParser, CamelCaseMultiPartParser

from django.db.models.query_utils import Q
from app_staffing.models.organization import ContactCategory, DisplaySetting, ExceptionalOrganization, OrganizationComment, ContactComment, CompanyAttribute, Tag
from app_staffing.models.email import EmailComment
from app_staffing.models.plan import Plan, PlanPaymentError
from app_staffing.models.user import MASTER, MEMBER, GUEST, User
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import status, serializers
from rest_framework.exceptions import MethodNotAllowed, APIException
from rest_framework.views import APIView
from rest_framework.generics import RetrieveDestroyAPIView, CreateAPIView, ListCreateAPIView,\
    RetrieveUpdateAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView, mixins, GenericAPIView
from rest_framework.permissions import IsAuthenticated, BasePermission, AllowAny
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.parsers import MultiPartParser, FileUploadParser
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework_csv.renderers import CSVRenderer

from app_staffing.board.models import PersonnelComment, ProjectComment, PersonnelContract
from app_staffing.views.classes.renderers import BinaryFileRenderer
from app_staffing.views.classes.permissions import CommentPinablePermission

from django.core.cache import cache
from django.conf import settings

import jpholiday
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta
from app_staffing.models.email import ScheduledEmail, QUEUED
from django.utils import dateformat

from django.db.models.query_utils import Q
from django.db.models import Case, When, Value
import datetime

from rest_framework.authtoken.models import Token
import pytz

from app_staffing.utils.custom_validation import custom_validation
from app_staffing.utils.addon import comment_template_total_available_count, search_template_total_available_count, \
    scheduled_email_template_total_available_count
from app_staffing.models.addon import Addon
from app_staffing.models.organization import Contact

import payjp

from app_staffing.exceptions.base import TemplateRegisterLimitException, TemplateNameRegisteredException, \
    TemplateNameBlankException, TemplateIsRequiredException, PermissionNotAllowedException, \
    IsSenderBelongToAnotherCompany, SenderDoesNotExistException, \
    StartDateIsLessThanEndDateException, EndDateGreaterThanStartDateException
from app_staffing.utils.cache import get_search_template_cache_key
import json

import secrets
from app_staffing.models.organization import UserDisplaySetting
from app_staffing.exceptions.user import (
    UserTokenInvalidException,
    UserTokenHasExpiredException,
    UserTokenDoesNotExistException,
)
from app_staffing.board.exceptions import PersonnelContractSetOrganizationAndContactException, \
    NumberPersonnelContractSetOrganizationContactException, \
    ProjectSettleWidthStartGreaterThanSettleWidthEnd
from app_staffing.board.constants import MAX_PERSONNEL_CONTRACT

from app_staffing.utils.logger import get_info_logger, get_error_logger
logger_stderr = get_error_logger(__name__)
logger_stdout = get_info_logger(__name__)


ID = 'id'


class ResourceProtectedException(APIException):
    status_code = 409
    default_detail = '他のリソースから参照されているため、削除できません。'
    default_code = 'conflict'


class InvalidQueryParameterException(APIException):
    status_code = 400
    default_detail = '無効なクエリーパラメータがあるため、処理できません。'
    default_code = 'bad parameter'

    def __init__(self, detail=None, code=None, params=()):
        if params:
            detail = self.default_detail + '(' + ','.join(params) + ')'
        super().__init__(detail, code)


class LoneWolfException(APIException):
    status_code = 500
    default_detail = 'どの組織にも属していないため、サービスをご利用できません。'
    default_code = 'error'


class DeleteErrorHandlerMixin(object):
    """A Mixin for REST framework views"""

    def perform_destroy(self, instance):
        try:
            super().perform_destroy(instance)
        except ProtectedError:
            raise ResourceProtectedException


class SummaryForListMixin(object):
    serializer_class_for_list = None  # Override in a child class.!

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return self.serializer_class_for_list
        else:
            return self.serializer_class


class MultiTenantMixin(object):
    """A mixin that Isolates resources between tenants by app_staffing.models.Company.id .
    Note: This mixin must be written on the most left position, except PerUserMixin, in a mixin order.
    Note: Target model must contain column 'company_id'
    """

    def get_queryset(self):
        company = self.request.user.company
        if company:
            qs = super().get_queryset()
            return qs.filter(company=company)
        else:
            raise LoneWolfException

    def create(self, request, *args, **kwargs):
        company = self.request.user.company
        if company:
            data = request.data.copy()
            data['company'] = company.id
            serializer = self.get_serializer(data=data)
            custom_validation = getattr(serializer, "custom_validation", None)
            if callable(custom_validation):
                serializer.custom_validation(data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            raise LoneWolfException


class PerUserMixin(object):
    """A mixin that Isolates resources between users by app_staffing.models.User.id .
    Note: This mixin must be written on the most left position in a mixin order.
    Note: Target model must contain column 'created_user'
    """

    def get_queryset(self):
        user = self.request.user
        qs = super().get_queryset()
        return qs.filter(created_user=user)


class IsAuthorizedAction(BasePermission):
    def is_illegal_format(self, splitted_path_items):
        return len(splitted_path_items) < 3 or splitted_path_items[1] != 'app_staffing'

    def get_sub_resource_name(self, request, resource_name, splitted_path_items):
        if resource_name in ['users', 'tags', 'my_company'] and request.method != 'GET':
            return '_all'
        elif request.method == 'DELETE':
            return 'delete'
        elif len(splitted_path_items) > 3:
            return splitted_path_items[3]

    def has_permission(self, request, view):
        if request.user:
            # request.path は '/app_staffing/organizations' のようなパスが入ることを想定
            # split が実行されると ['', 'app_staffing', 'organizations'] のような配列になる
            splitted_path_items = request.path.split('/')

            if self.is_illegal_format(splitted_path_items):
                return False

            resource_name = splitted_path_items[2]
            authorized_actions = settings.ROLE_AUTHORIZED_ACTIONS.get(request.user.user_role_name, {})

            if resource_name not in authorized_actions:
                return True

            sub_resource_name = self.get_sub_resource_name(request, resource_name, splitted_path_items)
            return authorized_actions.get(resource_name, {}).get(sub_resource_name, True)

        return False


class CustomTokenAuthentication(TokenAuthentication):
    def is_expired(self, token):
        jst_created = token.created.astimezone(pytz.timezone(settings.TIME_ZONE))
        delta_hours = 24 - settings.TOKEN_REGENERATION_HOUR
        base_datetime = jst_created + timedelta(hours=delta_hours)
        expired_at = base_datetime.replace(hour=settings.TOKEN_REGENERATION_HOUR, minute=0, second=0, microsecond=0)
        now = datetime.datetime.now().astimezone(pytz.timezone(settings.TIME_ZONE))

        return expired_at < now

    def authenticate_credentials(self, key):
        try:
            token = Token.objects.select_related('user', 'user__company').get(key=key)
        except Token.DoesNotExist:
            # トークンがない場合は認証失敗
            logger_stdout.info(f'Invalid Token, key: {key}')
            raise UserTokenDoesNotExistException()

        # ユーザが無効になっている場合は認証失敗
        if not token.user.is_active:
            logger_stdout.info(f'User is not active, user_id: {token.user.id}, key: {key}')
            raise UserTokenInvalidException()

        # トークンが期限切れの場合は認証失敗
        if self.is_expired(token):
            logger_stdout.info(f'The Token is expired, user_id: {token.user.id}, key: {key}, token_created: {token.created}')
            raise UserTokenHasExpiredException()

        # 所属テナントが退会状態の場合は認証失敗
        if token.user.company and token.user.company.deactivated_time:
            raise UserTokenInvalidException()

        token_created_date = token.created.astimezone(pytz.timezone(settings.TIME_ZONE)).date()
        now_date = datetime.datetime.now(pytz.timezone(settings.TIME_ZONE)).date()

        try:
            company_attribute = CompanyAttribute.objects.get(company_id=token.user.company_id)
        except Exception:
            company_attribute = None
            logger_stdout.info('failed to get company attribute, company_id: {0}'.format(token.user.company_id))

        # 無料トライアルが期限切れ、かつ、トークンが期限日よりも前に作成されている(期限が切れてから再ログインしていない)場合
        if company_attribute\
                and company_attribute.trial_expiration_date\
                and company_attribute.trial_expiration_date < now_date\
                and token_created_date <= company_attribute.trial_expiration_date:
            raise UserTokenInvalidException()

        try:
            active_plan = Plan.objects.get(company_id=token.user.company_id, is_active=True)
            plan_expiration_date = active_plan.expiration_date
            plan_expiration_date_with_postponement = plan_expiration_date + timedelta(days=settings.PLAN_EXPIRE_POSTPONEMENT_DAYS)
            plan_payment_error_exists = PlanPaymentError.objects.filter(company_id=token.user.company_id, plan_id=active_plan.id).exists()
        except Exception:
            plan_expiration_date_with_postponement = None
            logger_stdout.info('failed to check plan postponement expiration, company_id: {0}'.format(token.user.company_id))

        # 有料プランの支払い猶予期限切れ、かつ、トークンが期限日よりも前に作成されている(期限が切れてから再ログインしていない)場合
        if plan_expiration_date_with_postponement\
                and plan_payment_error_exists\
                and plan_expiration_date_with_postponement < now_date\
                and token_created_date <= plan_expiration_date_with_postponement:
            raise UserTokenInvalidException()

        return (token.user, token)


class GenericListAPIView(ListCreateAPIView):

    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, IsAuthorizedAction)

    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)

    @atomic
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def get_queryset(self):
        invalid_param_keys = self.request.query_params.keys() - \
            self.get_pagination_query_param_names() - \
            self.get_search_query_param_names() - \
            self.get_ordering_query_param_names()

        if invalid_param_keys:
            raise InvalidQueryParameterException(params=invalid_param_keys)

        return super().get_queryset()

    def get_pagination_query_param_names(self):
        """Valid query parameter names used by pagination of the view."""
        query_params = set()
        pagination_kls = self.pagination_class

        # Different pagination class has different `*_query_param` names.
        for attr_name in dir(pagination_kls):
            if not attr_name.endswith('_query_param'):
                continue
            attr_val = getattr(pagination_kls, attr_name)
            if callable(attr_val):
                continue
            query_params.add(attr_val)

        return query_params

    def get_search_query_param_names(self):
        """Valid query parameter names used by search filter of the view."""
        search_query_params = set()
        for filter_kls in self.filter_backends:
            if issubclass(filter_kls, DjangoFilterBackend):
                if hasattr(self, 'filterset_fields'):
                    search_query_params |= set(self.filterset_fields)
                elif hasattr(self, 'filterset_class'):
                    search_query_params |= set(self.filterset_class.base_filters.keys())
            elif issubclass(filter_kls, SearchFilter):
                search_query_params |= set({'search', 'fields'})

        return search_query_params

    def get_ordering_query_param_names(self):
        """Valid query parameter names used by ordering filter of the view."""
        return {OrderingFilter.ordering_param}

    def trade_condition_company(
        self, company, capital_man_yen_key='capital_man_yen', has_p_mark_or_isms_key='has_p_mark_or_isms', has_invoice_system_key='has_invoice_system',
        has_haken_key='has_haken', has_distribution_key='has_distribution', establishment_date_key='establishment_date'
    ):
        filter_params = []
        # 取引先取引条件で取引先を絞る
        # 取引先の取引条件が設定されていない場合は、この制御は無効になる
        # 取引先の取引条件「設立年数」「取引に必要な資格」「取引に必要な資本金」と自社の自社情報設定「設立年月」「保有資格」「資本金」を比較
        # filter_params.append(
        #     Q(
        #         Q(capital_man_yen_required_for_transactions__lte=company.capital_man_yen) |
        #         Q(capital_man_yen_required_for_transactions=None)
        #     ),
        # )
        # filter_params.append(
        #     Q(
        #         Q(p_mark_or_isms=company.has_p_mark_or_isms) |
        #         Q(p_mark_or_isms=False)
        #     ),
        # )
        # filter_params.append(
        #     Q(
        #         Q(haken=company.has_haken) |
        #         Q(haken=False)
        #     ),
        # )
        # base_year = relativedelta(datetime.datetime.now(), company.establishment_date).years
        # filter_params.append(
        #     Q(
        #         Q(establishment_year__lte=base_year) |
        #         Q(establishment_year=None)
        #     ),
        # )

        # 自社取引条件で取引先を絞る
        # 自社の取引条件が設定されていない場合は、この制御は無効になる
        # 自社の取引条件「設立年数」「取引に必要な資格」「取引に必要な資本金」と取引先の取引先情報「設立年月」「保有資格」「資本金」を比較
        gte_key = '__gte'
        lte_key = '__lte'
        isnull_key = '__isnull'
        if company.capital_man_yen_required_for_transactions:
            filter_params.append(
                Q(
                    Q(**{capital_man_yen_key + gte_key: company.capital_man_yen_required_for_transactions}) |
                    Q(**{capital_man_yen_key + isnull_key: True}) |
                    Q(**{has_distribution_key: True})
                ),
            )

        if company.p_mark_or_isms:
            filter_params.append(
                Q(
                    Q(**{has_p_mark_or_isms_key: company.p_mark_or_isms}) |
                    Q(**{has_p_mark_or_isms_key + isnull_key: True}) |
                    Q(**{has_distribution_key: company.p_mark_or_isms})
                )
            )

        if company.invoice_system:
            filter_params.append(
                Q(
                    Q(**{has_invoice_system_key: company.invoice_system}) |
                    Q(**{has_invoice_system_key + isnull_key: True}) |
                    Q(**{has_distribution_key: company.invoice_system})
                )
            )

        if company.haken:
            filter_params.append(
                Q(
                    Q(**{has_haken_key: company.haken}) |
                    Q(**{has_haken_key + isnull_key: True}) |
                    Q(**{has_distribution_key: company.haken})
                )
            )

        if company.establishment_year:
            base_date = datetime.datetime.now() - relativedelta(years=company.establishment_year)
            filter_params.append(
                Q(
                    Q(**{establishment_date_key + lte_key: base_date}) |
                    Q(**{establishment_date_key + isnull_key: True}) |
                    Q(**{has_distribution_key: True})
                ),
            )

        return filter_params


class GenericReadOnlyView(RetrieveAPIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)


class GenericRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, IsAuthorizedAction)

    @atomic
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PUT')


class GenericDetailAPIView(DeleteErrorHandlerMixin, RetrieveUpdateDestroyAPIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, IsAuthorizedAction)

    @atomic
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        raise MethodNotAllowed(method='PUT')


class GenericSubCommentListView(GenericListAPIView):
    related_model_class = None  # Override in child class
    related_field_name = None  # Override in child class
    related_model_pk_representation_in_url = 'parent_pk'

    parent_model_class = None  # Override in child class
    parent_field_name = None  # Override in child class
    parent_model_pk_representation_in_url = 'pk'

    def __init__(self):
        super().__init__()
        if not self.related_model_class:
            raise NotImplementedError('Specify related model which comments attaches to.')
        if not self.related_field_name:
            raise NotImplementedError('Please specify a related field name,'
                                      ' which is the field name of related object of the comment model.')
        if not self.parent_model_class:
            raise NotImplementedError('Specify parent model which comments attaches to.')
        if not self.related_field_name:
            raise NotImplementedError('Please specify a parent field name,'
                                      ' which is the field name of parent object of the comment model.')

    @property
    def related_model_id(self):
        return self.kwargs[self.related_model_pk_representation_in_url]

    @property
    def parent_model_id(self):
        return self.kwargs[self.parent_model_pk_representation_in_url]

    @property
    def _lookup_expr(self):
        lookup_key = self.related_field_name + '__id'
        parent_object_id = self.related_model_id
        parent_lookup_key = self.parent_field_name + '__id'
        parent_comment_object_id = self.parent_model_id
        expr = {
            lookup_key: parent_object_id,
            parent_lookup_key: parent_comment_object_id,
        }
        return expr

    def dispatch(self, request, *args, **kwargs):
        if kwargs.get(self.related_model_pk_representation_in_url) is None:
            raise RuntimeError(f'The Lookup name of the related model must be {self.related_model_pk_representation_in_url} in urls.py,'
                               ' or change following class attribute to match url s one'
                               ': related_model_pk_representation_in_url')
        if kwargs.get(self.parent_model_pk_representation_in_url) is None:
            raise RuntimeError(f'The Lookup name of the parent model must be {self.parent_model_pk_representation_in_url} in urls.py,'
                               ' or change following class attribute to match url s one'
                               ': parent_model_pk_representation_in_url')

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        get_object_or_404(self.related_model_class, pk=self.related_model_id, company=self.request.user.company)
        get_object_or_404(self.parent_model_class.objects.all_with_deleted(), pk=self.parent_model_id, company=self.request.user.company)
        qs = super().get_queryset()
        return qs.filter(**self._lookup_expr)

    def get_related_object(self):
        obj = get_object_or_404(self.related_model_class, pk=self.related_model_id, company=self.request.user.company)
        return obj

    def get_parent_object(self):
        obj = get_object_or_404(self.parent_model_class, pk=self.parent_model_id, company=self.request.user.company)
        return obj

    def perform_create(self, serializer):
        instance = self.get_related_object()
        parent_instance = self.get_parent_object()
        serializer.save(**{self.related_field_name: instance, self.parent_field_name: parent_instance})  # Append additional data.


class GenericCommentListView(GenericListAPIView):
    related_model_class = None  # Override in child class
    related_field_name = None  # Override in child class
    related_model_pk_representation_in_url = 'parent_pk'

    def __init__(self):
        super().__init__()
        if not self.related_model_class:
            raise NotImplementedError('Specify related model which comments attaches to.')
        if not self.related_field_name:
            raise NotImplementedError('Please specify a related field name,'
                                      ' which is the field name of related object of the comment model.')

    @property
    def related_model_id(self):
        return self.kwargs[self.related_model_pk_representation_in_url]

    @property
    def _lookup_expr(self):
        lookup_key = self.related_field_name + '__id'
        parent_object_id = self.related_model_id
        expr = {lookup_key: parent_object_id}
        return expr

    def dispatch(self, request, *args, **kwargs):
        if kwargs.get(self.related_model_pk_representation_in_url) is None:
            raise RuntimeError(f'The Lookup name of the related model must be {self.related_model_pk_representation_in_url} in urls.py,'
                               ' or change following class attribute to match url s one'
                               ': related_model_pk_representation_in_url')

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        get_object_or_404(self.related_model_class, pk=self.related_model_id, company=self.request.user.company)
        qs = super().get_queryset()
        return qs.filter(**self._lookup_expr)

    def get_related_object(self):
        obj = get_object_or_404(self.related_model_class, pk=self.related_model_id, company=self.request.user.company)
        return obj

    def perform_create(self, serializer):
        instance = self.get_related_object()
        serializer.save(**{self.related_field_name: instance})  # Append additional data.


class GenericCommentDetailView(GenericDetailAPIView):
    related_field_name = None  # Override in child class

    pk_field_representation_in_url = 'pk'
    related_model_pk_representation_in_url = 'parent_pk'

    comment_model = None  # Override in child class

    permission_classes = (IsAuthenticated, CommentPinablePermission)

    def __init__(self):
        super().__init__()
        if not self.related_field_name:
            raise NotImplementedError('Please specify a related field name,'
                                      ' which is the field name of related object of the comment model.')
        if not self.comment_model:
            raise NotImplementedError('Specify comment model which comments attaches to.')

    @property
    def instance_id(self):
        return self.kwargs[self.pk_field_representation_in_url]

    @property
    def related_model_id(self):
        return self.kwargs[self.related_model_pk_representation_in_url]

    @property
    def _lookup_expr(self):
        expr = {
            'pk': self.instance_id,
            self.related_field_name: self.related_model_id,
        }
        return expr

    def dispatch(self, request, *args, **kwargs):
        if kwargs.get(self.pk_field_representation_in_url) is None:
            raise RuntimeError(f'The Lookup name of the comment model must be {self.pk_field_representation_in_url} in urls.py,'
                               ' or change following class attribute to match url s one'
                               ': pk_field_representation_in_url')
        if kwargs.get(self.related_model_pk_representation_in_url) is None:
            raise RuntimeError(f'The Lookup name of the related model must be {self.related_model_pk_representation_in_url} in urls.py,'
                               ' or change following class attribute to match url s one'
                               ': related_model_pk_representation_in_url')

        return super().dispatch(request, *args, **kwargs)

    def get_object(self):
        qs = self.get_queryset()
        instance = get_object_or_404(qs, **self._lookup_expr)
        self.check_object_permissions(self.request, instance)
        return instance

    def check_parent_sub_comment(self, instance):
        total_sub_comments = instance.sub_comments.count()
        return total_sub_comments != 0

    def get_parent(self):
        sub_comment = get_object_or_404(self.comment_model.objects.all_with_deleted()\
                                        .prefetch_related('parent')\
                                        .prefetch_related('parent__sub_comments'),\
                                        **self._lookup_expr)
        return sub_comment.parent

    @atomic
    def delete(self, request, *args, **kwargs):
        response = super().delete(request, *args, **kwargs)
        parent = self.get_parent()
        if parent and self.check_parent_sub_comment(parent) is False:
            parent.has_subcomment = False
            parent.save()
        return response


class GenericActionView(CreateAPIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    lookup_field = 'id'
    lookup_url_kwarg = 'pk'

    def create(self, request, *args, **kwargs):
        raise NotImplementedError  # Override in child class.


class FileListView(ListCreateAPIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FileUploadParser,)


class FileListCreateUsingPatchView(FileListView):
    def patch(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class CsvDownloadView(GenericListAPIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    renderer_classes = (CSVRenderer, )


class FileDetailView(DeleteErrorHandlerMixin, RetrieveDestroyAPIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer, BinaryFileRenderer,)

    def get(self, request, *args, **kwargs):
        if request.accepted_renderer.render_style == 'binary':
            # Return binary data if requests have a header 'Accept' and requests binary data.
            instance = self.get_object()
            # WARNING: Make sure that the original object has an attribute "file"
            file = instance.file.open()
            response = FileResponse(file)
            response['Content-Length'] = instance.file.size
            response['Content-Disposition'] = 'attachment; filename="%s"' % instance.file.name
            return response
        else:
            # Else, Return a normal JSON response.
            return super().get(request, *args, **kwargs)


class GenericRecommendAPIView(RetrieveAPIView):

    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    recommender_class = None  # Override by child class

    def get(self, request, *args, **kwargs):
        # TODO: catch and handle recommender related errors.
        instance = self.get_object()
        recommender = self.recommender_class()

        related_items = recommender.predict(instance=instance)
        response = super().get(request, *args, **kwargs)

        related_item_ids = [item[ID] for item in related_items]
        response.data = {'items': related_item_ids}

        return response


class StatisticsAPIView(APIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)


class ColumnSettingView(APIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, IsAuthorizedAction)

    def get(self, request, *args, **kwargs):
        resource_name = kwargs.get('resource_name')
        cache_key = f'{settings.CACHE_COLUMN_SETTING_KEY_BASE}_{self.request.user.company.id}_{resource_name}'
        setting = cache.get(cache_key, [])
        return Response(status=status.HTTP_200_OK, data=setting)

    def post(self, request, *args, **kwargs):
        resource_name = kwargs.get('resource_name')
        cache_key = f'{settings.CACHE_COLUMN_SETTING_KEY_BASE}_{self.request.user.company.id}_{resource_name}'
        cache.set(cache_key, request.data['selectedColumnKeys'], settings.CACHE_COLUMN_SETTING_TIMEOUT)
        return Response(status=status.HTTP_201_CREATED, data=[])


class CsvUploadView(APIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, IsAuthorizedAction)

    def _get_common_illegal_error_message(self, column_name, value):
        return f'{value}は{column_name}として登録できません。'

    def _get_unkwon_enum_item_error_message(self, index, column_name, value, enum_items):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}「{"」「".join(enum_items)}」のいずれかを入力してください。'

    def _get_out_of_range_error_message(self, index, column_name, value, min, max):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}「{min}」から「{max}」 (半角数字)のいずれかを入力してください。'

    def _get_input_format_error_message(self, index, column_name, value, input_format_description):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}{input_format_description}入力してください。'

    def _get_boolean_item_error_message(self, index, column_name, value):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}「0」と「1」(半角数字)または空欄のみ有効です。'

    def _get_cannot_input_space_error_message(self, index, column_name, value):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}スペースの入力はできません。'

    def _get_hyphen_format_error_message(self, index, column_name, value):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}半角ハイフンを含め適切なフォーマットで入力してください。'

    def _get_future_date_error_message(self, index, column_name, value,):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}未来日付の登録は不可です。'

    def _get_illegal_error_message(self, index, value):
        return f'{index+1}行目: {value}は想定された入力項目ではありません。'

    def _get_required_error_message(self, index, column_name):
        return f'{index+1}行目: {column_name}は必須項目です。'

    def _get_count_error_message(self, index, column_name):
        return f'{index+1}行目: {column_name}の入力可能数上限を超えています。'

    def _get_already_registered_error_message(self, index, column_name, value):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}既に登録されています。'

    def _get_duplicate_error_message(self, index, column_name, value):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}値がファイル内で重複しています。'

    def _get_unmatch_branch_value_length_error_message(self, index, column_name, value):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}取引先支店名と{column_name}の値の数が一致しません。'

    def _get_unregistered_organization_error_message(self, index, value):
        return f'{index+1}行目: {value}は未登録の取引先です。先に取引先の登録を行ってください。'

    def _get_unregistered_staff_error_message(self, index, value):
        return f'{index+1}行目: {value}に紐づくユーザーが見当たりません。再度ユーザーのメールアドレスをご確認ください。'

    def _get_over_registration_limit_error_message(self, index, column_name, limit):
        return f'{index+1}行目: {column_name}の登録可能数上限{limit}件を超えています。'

    def _get_illegal_email_address_error_message(self, index, column_name, value):
        common = self._get_common_illegal_error_message(column_name, value)
        return f'{index+1}行目: {common}有効なメールアドレスを入力してください。'

    def _get_email_cc_over_length_error_message(self, index):
        return f'{index+1}行目: メールアドレス > CCはカンマを含めて500文字以内で入力してください。'

    def _get_required_items(self, resource_name):
        try:
            display_setting = DisplaySetting.objects.get(company_id=self.request.user.company)
            content = json.loads(display_setting.content)
            return content[resource_name]['require']
        except Exception:
            return []

    def _get_column_length_error_message(self, index):
        return f'{index+1}行目: カラム数が正しくありません。'

class SearchTemplateView(APIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        resource_name = kwargs.get('resource_name')
        assigned_company = self.request.user.company
        cache_key = get_search_template_cache_key(assigned_company.id, self.request.user.id, resource_name)
        templates = cache.get(cache_key, [])
        copied_templates = templates.copy()
        for index, template in enumerate(copied_templates):
            values = template.get("values", None)
            if not values:
                continue
            tag_field_name = "contact__tags" if resource_name == "contact_mail_preference" else "tags"
            tag_ids = values.get(tag_field_name, None)
            if not tag_ids:
                continue
            available_tag_ids = [str(x) for x in Tag.objects.filter(id__in=tag_ids).values_list('id', flat=True)]
            filtered_tags = []
            for tag_id in tag_ids:
                if tag_id in available_tag_ids:
                    filtered_tags.append(tag_id)
            values[tag_field_name] = filtered_tags
            template["values"] = values
            copied_templates[index] = template
        total_available_count = search_template_total_available_count(assigned_company)
        return Response(status=status.HTTP_200_OK, data={'templates': copied_templates, 'total_available_count': total_available_count})

    def post(self, request, *args, **kwargs):
        custom_validation(request.data, 'search_template')
        resource_name = kwargs.get('resource_name')
        # メモリ上に
        cache_key = get_search_template_cache_key(self.request.user.company.id, self.request.user.id, resource_name)
        templates = cache.get(cache_key, [])

        # 同一ユーザで同じ名称のテンプレートが登録済みかを確認
        for template in templates:
            newTemplateName = request.data['templateName']
            if template['template_name'] == newTemplateName:
                msg = settings.TEMPLATEALREADY_EXISTS_ERROR_MESSAGE.format(newTemplateName=newTemplateName)
                return Response({'non_field_errors': [msg]}, status=status.HTTP_400_BAD_REQUEST)

        total_available_count = search_template_total_available_count(self.request.user.company)
        if total_available_count <= len(templates):
            raise TemplateRegisterLimitException()

        template_name = request.data['templateName']
        if not template_name:
            raise TemplateNameBlankException()

        for registered_template in templates:
            if registered_template['template_name'] == template_name:
                raise TemplateNameRegisteredException()

        default_name = f'{template_name}'
        template = {
            "name": default_name,
            "values": request.data['formValues'],
            "star": False,
            "display_name": default_name,
            "template_name": template_name,
            "pageSize": request.data['pageSize'],
            "sortKey": request.data['sortKey'],
            "sortOrder": request.data['sortOrder'],
            "selectedColumnKeys": request.data['selectedColumnKeys'],
        }
        templates.append(template)
        cache.set(cache_key, templates, settings.CACHE_SEARCH_TEMPLATE_TIMEOUT)
        return Response(status=status.HTTP_201_CREATED, data={'created_data': template, 'index': len(templates) - 1, 'total_available_count': total_available_count})

    def patch(self, request, *args, **kwargs):
        resource_name = kwargs.get('resource_name')
        cache_key = get_search_template_cache_key(self.request.user.company.id, self.request.user.id, resource_name)
        templates = cache.get(cache_key, [])
        index = int(kwargs.get('index'))
        new_templates = []
        for i, template in enumerate(templates):
            if i == index and not template['star']:
                template['star'] = True
                name = template['name']
                template['display_name'] = f'☆ : {name}'
            else:
                template['display_name'] = template['name']
                template['star'] = False
            new_templates.append(template)
        cache.set(cache_key, new_templates, settings.CACHE_SEARCH_TEMPLATE_TIMEOUT)
        return Response(status=status.HTTP_200_OK, data=[])

    def delete(self, request, *args, **kwargs):
        resource_name = kwargs.get('resource_name')
        cache_key = get_search_template_cache_key(self.request.user.company.id, self.request.user.id, resource_name)
        templates = cache.get(cache_key, [])
        index = int(kwargs.get('index'))
        new_templates = []
        for i, template in enumerate(templates):
            if i != index:
                new_templates.append(template)
        cache.set(cache_key, new_templates, settings.CACHE_SEARCH_TEMPLATE_TIMEOUT)
        return Response(status=status.HTTP_204_NO_CONTENT, data=[])


class CommentTemplateView(APIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        resource_name = kwargs.get('resource_name')
        cache_key = f'{settings.CACHE_COMMENT_TEMPLATE_KEY_BASE}_{self.request.user.company.id}_{resource_name}'
        templates = cache.get(cache_key, settings.CACHE_COMMENT_TEMPLATE_DEFAULT_VALUE[resource_name])
        total_available_count = comment_template_total_available_count(self.request.user.company)
        return Response(status=status.HTTP_200_OK, data={'templates': templates, 'total_available_count': total_available_count})

    def post(self, request, *args, **kwargs):
        template_names = []
        for template in request.data['templates']:
            if not 'title' in template or not template['title']:
                raise TemplateNameBlankException()

            if template['title'] in template_names:
                raise TemplateNameRegisteredException()

            template_names.append(template['title'])
            custom_validation(template, 'comment_template')

        total_available_count = comment_template_total_available_count(self.request.user.company)
        if total_available_count < len(request.data['templates']):
            raise TemplateRegisterLimitException()

        resource_name = kwargs.get('resource_name')
        cache_key = f'{settings.CACHE_COMMENT_TEMPLATE_KEY_BASE}_{self.request.user.company.id}_{resource_name}'
        cache.set(cache_key, request.data['templates'], settings.CACHE_COMMENT_TEMPLATE_TIMEOUT)
        return Response(status=status.HTTP_201_CREATED, data={'templates': request.data['templates'], 'total_available_count': total_available_count})


class ReservedDateView(APIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        date_from = date.today()
        date_to = date_from + relativedelta(months=3)
        holidays = jpholiday.between(date_from, date_to)

        company = self.request.user.company
        if not company:
            raise LoneWolfException

        scheduled_emails = ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).filter(date_to_send__range=(date_from, date_to), company=company).values('date_to_send')
        return Response(status=status.HTTP_200_OK, data={
            'around_minutes': settings.SCHEDULED_MAIL_REGISTER_RESTRICTION_AROUND_MINUTES,
            'holidays': [
                day for day, name in holidays
            ],
            'date_to_sends': [
                dateformat.format(scheduled_email['date_to_send'].astimezone(pytz.timezone(settings.TIME_ZONE)), 'Y-m-d H:i') for scheduled_email in scheduled_emails
            ],
            'is_use_delivery_term_available': Addon.objects.filter(company=company, addon_master_id=settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_ADDON_MASTER_ID).exists()
        })


class AuthorizedActionView(APIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return Response(
            status=status.HTTP_200_OK,
            data={
                'authorized_action': settings.ROLE_AUTHORIZED_ACTIONS.get(self.request.user.user_role_name, {})
            }
        )


class UnauthorizedActionView(APIView):
    permission_classes = [AllowAny]
    authentication_classes = []


class OrderIdHelper(object):
    def get_queryset_order_by_ids(self, queryset, ids, direction):
        if not ids or len(ids) == 0:
            return queryset

        if direction == 'asc':
            order = Case(*[When(id=id, then=pos) for pos, id in enumerate(ids)])
            queryset = queryset.order_by(order)
        elif direction == 'desc':
            order = Case(*[When(id=id, then=(pos + 1)) for pos, id in enumerate(ids)], default=Value(1))
            queryset = queryset.order_by(order)
        return queryset


class CustomValidationHelper(object):
    def exec_custom_validation(self, request):
        data = request.data.copy()
        serializer = self.get_serializer(data=data)
        custom_validation = getattr(serializer, "custom_validation", None)
        if callable(custom_validation):
            serializer.custom_validation(data)


class CommentValidationHelper(object):
    def update_organization_important_comment_or_pass(self, request, comment_pk):
        comments = OrganizationComment.objects.filter(
            id=comment_pk,
            is_important=True,
        )
        return not comments.count() == 1

    def is_organization_comment_over_limit(self, request, related_model_id):
        if 'is_important' in request.data and request.data['is_important']:
            count = OrganizationComment.objects.filter(
                company=request.user.company,
                is_important=True,
                organization_id=related_model_id
            ).count()
            return settings.COMMENT_IMPORTANT_LIMIT <= count
        else:
            return False

    def update_contact_important_comment_or_pass(self, request, comment_pk):
        comments = ContactComment.objects.filter(
            id=comment_pk,
            is_important=True,
        )
        return not comments.count() == 1

    def is_contact_comment_over_limit(self, request, related_model_id):
        if 'is_important' in request.data and request.data['is_important']:
            count = ContactComment.objects.filter(
                company=request.user.company,
                is_important=True,
                contact_id=related_model_id
            ).count()
            return settings.COMMENT_IMPORTANT_LIMIT <= count
        else:
            return False

    def update_shared_email_important_comment_or_pass(self, comment_pk):
        return not EmailComment.objects.filter(id=comment_pk, is_important=True).exists()

    def is_shared_email_comment_over_limit(self, request, related_model_id):
        if 'is_important' in request.data and request.data['is_important']:
            count = EmailComment.objects.filter(
                company=request.user.company,
                is_important=True,
                email_id=related_model_id
            ).count()
            return settings.COMMENT_IMPORTANT_LIMIT <= count
        else:
            return False

    def is_personnel_comment_over_limit(self, request, related_model_id):
        if 'is_important' in request.data and request.data['is_important']:
            count = PersonnelComment.objects.filter(
                company=request.user.company,
                is_important=True,
                personnel_id=related_model_id
            ).count()
            return settings.COMMENT_IMPORTANT_LIMIT <= count
        else:
            return False

    def update_personnel_important_comment_or_pass(self, comment_pk):
        return not PersonnelComment.objects.filter(id=comment_pk, is_important=True).exists()

    def is_project_comment_over_limit(self, request, related_model_id):
        if 'is_important' in request.data and request.data['is_important']:
            count = ProjectComment.objects.filter(
                company=request.user.company,
                is_important=True,
                project_id=related_model_id
            ).count()
            return settings.COMMENT_IMPORTANT_LIMIT <= count
        else:
            return False

    def update_project_important_comment_or_pass(self, comment_pk):
        return not ProjectComment.objects.filter(id=comment_pk, is_important=True).exists()


class BulkOperationHelper(object):
    def get_unique_items(self, items):
        uniq_items = list(set(items))
        uniq_items_none_and_blank_removed = list(filter(None, uniq_items))
        return uniq_items_none_and_blank_removed

    def is_over_bulk_operation_limit(self, items):
        return settings.BULK_OPERATION_LIMIT < len(items)


class PayjpHelper(object):
    def _get_authorized_payjp_object(self):
        payjp.api_key = settings.PAYJP_API_KEY
        return payjp

    def _get_customer(self, company_id):
        try:
            return self._get_authorized_payjp_object().Customer.retrieve(str(company_id))
        except payjp.error.InvalidRequestError as e:
            return self._get_authorized_payjp_object().Customer.create(id=str(company_id))

    def _get_cards(self, company_id):
        return self._get_customer(company_id).cards.all()

    def _get_default_card_id(self, company_id):
        return self._get_customer(company_id).get('default_card')

class CsvDownloadHelper(object):
    def get_queryset(self):
        qs = self.filterset_class(
            self.request.query_params,
            queryset=super().get_queryset()
        ).qs

        order = self.request.GET.get("ordering", '-created_time')
        if order in ['created_time', '-created_time']:
            qs = qs.order_by(order)
        elif order in self.ordering_fields:
            # GETパラメータの値を第1ソート、データの作成日時を第2ソートにする
            qs = qs.order_by(order, '-created_time')
        else:
            qs = qs.order_by('-created_time')

        if self.request.GET.get("ignore_blocklist_filter") == 'use_blocklist_filter':
            qs = self.get_is_blacklisted_query(qs)

        if self.request.GET.get("ignore_filter") == 'ignore_filter':
            return self.set_limit(qs)

        company = self.request.user.company

        filter_params = self.get_filter_params(company)
        if not filter_params:
            return self.set_limit(qs)

        # 除外取引先が登録されている場合は、取引条件の対象から除く。つまり、検索対象になるようにする
        exceptional_organizations = ExceptionalOrganization.objects.filter(company=company)
        if exceptional_organizations:
            exceptional_organization_ids = [exceptional_organization.organization.id for exceptional_organization in exceptional_organizations]
            return self.set_limit(
                qs.filter(Q(*filter_params) | self.get_exceptional_organization_q_obj(exceptional_organization_ids))
            )
        else:
            return self.set_limit(qs.filter(*filter_params))

    def set_limit(self, qs):
        index = self.request.GET.get("index", 0)
        start = int(index) * settings.CSV_DOWNLOAD_LIMIT
        end = start + settings.CSV_DOWNLOAD_LIMIT
        return qs[start:end]

class UserHelper(object):
    def get_relation_error_messages(self, deactive_users):
        error_messages = []

        if not deactive_users or len(deactive_users) == 0:
            return error_messages

        user_id_array = [user.id for user in deactive_users]
        scheduled_emails_user_id_array = ScheduledEmail.objects.filter(
            sender_id__in=user_id_array,
            scheduled_email_status__name__in=['draft', 'queued', 'sending'],
            company=self.request.user.company
        ).values_list('sender_id', flat=True)

        has_scheduled_email_error = False
        for user in deactive_users:
            if not has_scheduled_email_error and user.id in scheduled_emails_user_id_array:
                error_messages.append(settings.USER_DEACTIVATE_SCHEDULED_EMAIL_ERROR_MESSAGE)
                has_scheduled_email_error = True

        if 0 < len(error_messages):
            error_messages.insert(0, settings.USER_DEACTIVATE_DEFAULT_ERROR_MESSAGE)

        return error_messages

    def hide(self, hide_users):
        for user in hide_users:
            if user.user_role.name == MASTER:
                continue
            user.is_hidden = True
            user.is_active = False
            replace_token = secrets.token_urlsafe(16)
            user.username = replace_token
            user.email = replace_token
            user.save()
            try:
                file = user.avatar
                if file.name != '' and file.name is not None:
                    file.storage.delete(name=file.name)
                user.avatar = None
                user.save()
            except Exception as e:
                logger_stdout.info(e)
        self.delete_user_settings(hide_users)

    def delete_user_settings(self, users):
        for user in users:
            # 検索条件テンプレートを削除
            for resource_name in list(settings.SEARCH_TEMPLATE_RESOURCE_NAMES.keys()):
                cache_key = get_search_template_cache_key(user.company.id, user.id, resource_name)
                cache.delete(cache_key)

            # 表示項目設定を削除
            UserDisplaySetting.objects.filter(user__in=users).delete()
            # OneToOneのリレーションアイテムは論理削除されていても値が取得できてしまうため、物理削除で対応する
            UserDisplaySetting.objects.deleted_set().filter(user__in=users).delete()

            # 相性の設定を削除
            ContactCategory.objects.filter(user__in=users).delete()


class PersonnelBoardBaseView(object):
    renderer_classes = (CamelCaseJSONRenderer, CamelCaseBrowsableAPIRenderer)
    parser_classes = [CamelCaseJSONParser, CamelCaseMultiPartParser]


class CheckListItemCreateBaseViewMixin(GenericListAPIView):
    related_model_class = None  # Override in child class
    related_field_name = None  # Override in child class
    related_model_pk_representation_in_url = 'pk'

    def __init__(self):
        super().__init__()
        if not self.related_model_class:
            raise NotImplementedError('Specify related model which comments attaches to.')
        if not self.related_field_name:
            raise NotImplementedError('Please specify a related field name,'
                                      ' which is the field name of related object of the comment model.')

    @property
    def related_model_id(self):
        return self.kwargs[self.related_model_pk_representation_in_url]

    @property
    def _lookup_expr(self):
        lookup_key = self.related_field_name + '__id'
        parent_object_id = self.related_model_id
        expr = {lookup_key: parent_object_id}
        return expr

    def dispatch(self, request, *args, **kwargs):
        if kwargs.get(self.related_model_pk_representation_in_url) is None:
            raise RuntimeError(f'The Lookup name of the related model must be {self.related_model_pk_representation_in_url} in urls.py,'
                               ' or change following class attribute to match url s one'
                               ': related_model_pk_representation_in_url')

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        get_object_or_404(self.related_model_class, pk=self.related_model_id, company=self.request.user.company)
        qs = super().get_queryset()
        return qs.filter(**self._lookup_expr)

    def get_related_object(self):
        obj = get_object_or_404(self.related_model_class, pk=self.related_model_id, company=self.request.user.company)
        return obj

    def perform_create(self, serializer):
        instance = self.get_related_object()
        serializer.save(**{self.related_field_name: instance})  # Append additional data.


class ScheduledEmailTemplateView(APIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        cache_key = f'{settings.CACHE_SCHEDULED_EMAIL_TEMPLATE_KEY_BASE}_{self.request.user.company.id}_{self.request.user.id}'
        templates = cache.get(cache_key, [])
        total_available_count = scheduled_email_template_total_available_count(self.request.user.company)
        return Response(status=status.HTTP_200_OK, data={'templates': templates, 'total_available_count': total_available_count})

    def post(self, request, *args, **kwargs):
        template_names = []
        if request.user.user_role_name == GUEST:
            raise PermissionNotAllowedException()
        if not 'templates' in request.data:
            raise TemplateIsRequiredException()
        total_available_count = scheduled_email_template_total_available_count(self.request.user.company)

        if total_available_count < len(request.data['templates']):
            raise TemplateRegisterLimitException()

        sender_id = [template.get('sender') for template in request.data['templates']]
        is_sender_exists = User.objects.filter(id__in=sender_id).count()

        if is_sender_exists != len(set(sender_id)):
            raise SenderDoesNotExistException()

        is_sender_id_belong_to_another_company = User.objects.filter(Q(id__in=sender_id), ~Q(company=request.user.company)).exists()

        if is_sender_id_belong_to_another_company is True:
            raise IsSenderBelongToAnotherCompany()

        for template in request.data['templates']:
            if not 'title' in template or not template['title']:
                raise TemplateNameBlankException()

            if template['title'] in template_names:
                raise TemplateNameRegisteredException()

            if request.user.user_role_name == MEMBER:
                template['sender'] = request.user.id

            template_names.append(template['title'])
            custom_validation(template, 'scheduled_email_template')

        cache_key = f'{settings.CACHE_SCHEDULED_EMAIL_TEMPLATE_KEY_BASE}_{self.request.user.company.id}_{self.request.user.id}'
        cache.set(cache_key, request.data['templates'], settings.CACHE_SCHEDULED_EMAIL_TEMPLATE_TIMEOUT)
        return Response(status=status.HTTP_201_CREATED, data={'templates': request.data['templates'], 'total_available_count': total_available_count})


class CardListMixin(object):

    def get_queryset(self):
        card_list_id = self.request.GET.get("list_id")
        if card_list_id:
            qs = super().get_queryset()
            return qs.filter(card_list_id=card_list_id)
        else:
            raise LoneWolfException

    def create(self, request, *args, **kwargs):
        card_list_id = request.data.get("list_id")
        if card_list_id:
            request.data.update(card_list=card_list_id)
            return super().create(request, *args, **kwargs)
        else:
            raise LoneWolfException


class SummaryForDetailMixin(object):
    serializer_class_for_detail = None  # Override in a child class.!

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return self.serializer_class_for_detail
        else:
            return self.serializer_class


class ContractValidationHelper(object):
    def check_set_organization_contact(self, request):
        """
            check set organization contact
        """
        errors = {}
        data = request.data.copy()
        higher_organization = data.get('higher_organization')
        lower_organization = data.get('lower_organization')
        higher_contact = data.get('higher_contact')
        lower_contact = data.get('lower_contact')

        record_higher_contact = Contact.objects.filter(
            id=higher_contact, organization=higher_organization).exists()

        record_lower_contact = Contact.objects.filter(
            id=lower_contact, organization=lower_organization).exists()

        if not record_higher_contact and not record_lower_contact:
            errors['contact'] = PersonnelContractSetOrganizationAndContactException().default_detail
            raise serializers.ValidationError(errors)
        pass

    def check_size_set_organization_contact(self, request):
        """
            check the number of set organization contact more than 50
        """
        errors = {}
        data = request.data.copy()
        if 'card_id' in data.keys():
            personnel_id = data.get('card_id')

            number_record_personnel_contract = PersonnelContract.objects.filter(
                personnel_id=personnel_id).count()

            if number_record_personnel_contract and number_record_personnel_contract > MAX_PERSONNEL_CONTRACT:
                errors['contract'] = NumberPersonnelContractSetOrganizationContactException().default_detail
                raise serializers.ValidationError(errors)
        pass


class ProjectValidationHelper(object):
    def check_settle_width(self, request):
        """
            check settle width start and settle width end
        """
        errors = {}
        data = request.data.copy()
        if 'settle_width' in data.keys():
            settle_width = data.get('settle_width')
            settle_width_start = settle_width.get('start')
            settle_width_end = settle_width.get('end')

            if settle_width_start > settle_width_end:
                errors['settle_width'] = ProjectSettleWidthStartGreaterThanSettleWidthEnd().default_detail
                raise serializers.ValidationError(errors)
        pass
