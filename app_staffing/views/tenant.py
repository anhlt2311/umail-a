from datetime import datetime, timedelta

import pytz
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.core.cache import cache
from django.core.signing import dumps, loads
from django.db.transaction import atomic
from django.template.loader import render_to_string
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers

from app_staffing.exceptions.tenant import (
    TenantCannotLoginInactiveUserOrWrongPasswordException,
    TenantFormerMasterUserReRegisteringException,
    TenantRegisterPasswordException)
from app_staffing.models import User
from app_staffing.models.organization import Company, CompanyAttribute
from app_staffing.models.user import MASTER
from app_staffing.serializers import UserSerializer
from app_staffing.serializers.organization import CompanySerializer
from app_staffing.utils.custom_validation import (file_validation, password_validator)
from app_staffing.utils.system_mail import send_system_mail
from app_staffing.utils.utils import validate_tel_values
from app_staffing.utils.user import validate_user_service_id
from app_staffing.utils.logger import get_info_logger, get_error_logger
from app_staffing.views.base import (CustomValidationHelper, GenericDetailAPIView,
                                     MultiTenantMixin, PayjpHelper)

logger_stderr = get_error_logger(__name__)
logger_stdout = get_info_logger(__name__)


class TenantHelper(object):
    def get_user(self, auth_token):
        user_pk = loads(auth_token, max_age=settings.TENANT_REGISTER_EMAIL_EXPIRE_SECONDS)
        return User.objects.get(pk=user_pk)

    def set_current_step(self, company_id, step):
        company_attribute = CompanyAttribute.objects.get(company_id=company_id)
        company_attribute.tenant_register_current_step = step
        company_attribute.save()


class TenantSiteKeyAPIView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        data = {
            "sitekey": settings.RE_CAPTCHA_SITE_KEY,
            "test_mode": bool(settings.RE_CAPTCHA_TEST_MODE),
        }
        return Response(status=status.HTTP_200_OK, data=data)


class TenantRegisterView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper, TenantHelper):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []

    def send_mail(self, user):
        current_site = get_current_site(self.request)
        domain = current_site.domain
        context = {
            'protocol': self.request.scheme,
            'domain': domain,
            'auth_token': dumps(str(user.pk)),
        }

        cache_key = f'{settings.CACHE_TENANT_REGISTER_KEY_BASE}_{user.pk}'
        cache.set(cache_key, context, settings.TENANT_REGISTER_EMAIL_EXPIRE_SECONDS)
        subject = render_to_string(settings.TEMPLATE_EMAIL_TENANT_REGISTER_SUBJECT, context)
        message = render_to_string(settings.TEMPLATE_EMAIL_TENANT_REGISTER_MESSAGE, context)
        user.email_user(subject, '', html_message=message)

    @atomic
    def post(self, request, *args, **kwargs):
        email = self.request.data.get('email')
        password = self.request.data.get('password')

        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            can_authenticate = authenticate(username=email, password=password)

            # NOTE(joshua-hashimoto): 退会したマスターユーザーのメールアドレスだった場合
            is_deactivated = user.company.deactivated_time is not None
            if user.user_role.name == MASTER and is_deactivated:
                raise TenantFormerMasterUserReRegisteringException()

            # NOTE(joshua-hashimoto): 無効ユーザーもしくは有効ユーザーのパスワードが間違っている場合
            if not can_authenticate or not user.is_active:
                raise TenantCannotLoginInactiveUserOrWrongPasswordException()

            if CompanyAttribute.objects.filter(company=user.company, tenant_register_current_step=settings.TENANT_REGISTER_STEP_COMPLETED).exists():
                return Response(status=status.HTTP_200_OK, data={'is_registered': True})
            else:
                self.send_mail(user)
        else:
            password_confirm = self.request.data.get('password_confirm')
            if password != password_confirm:
                raise TenantRegisterPasswordException()
            if password_validator(password, settings.LENGTH_VALIDATIONS['user']['password']):
                return Response(status=status.HTTP_400_BAD_REQUEST, data={
                    'password': settings.LENGTH_VALIDATIONS['user']['password']['message']
                })
            new_company = Company.objects.create()
            CompanyAttribute.objects.create(
                company=new_company,
                user_registration_limit=5,
            )
            new_master_user = User.objects.create(
                username=email,
                email=email,
                user_role_id=settings.USER_ROLE_MASTER_ID,
                company=new_company,
                user_service_id=User.generate_random_user_service_id(),
            )
            new_master_user.set_password(password)
            new_master_user.save()
            self.set_current_step(new_company.id, settings.TENANT_REGISTER_STEP_MY_COMPANY)
            self.send_mail(new_master_user)

        return Response(status=status.HTTP_201_CREATED, data={
            'current_step': settings.TENANT_REGISTER_STEP_MY_COMPANY,
            'is_completed': False,
        })


class TenantMyCompanyView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper, TenantHelper):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []

    def get(self, request, *args, **kwargs):
        return Response(
            status=status.HTTP_200_OK,
            data=CompanySerializer(self.get_user(request.GET.get('auth_token')).company).data
        )

    @atomic
    def post(self, request, *args, **kwargs):
        company = self.get_user(request.data.get('auth_token')).company
        available_columns = [
            'address',
            'building',
            'capital_man_yen',
            'domain_name',
            'establishment_date',
            'settlement_month',
            'has_distribution',
            'has_haken',
            'has_invoice_system',
            'has_p_mark_or_isms',
            'name'
        ]

        for column in available_columns:
            if self.request.data.get(column):
                setattr(company, column, self.request.data.get(column))

        company.save()

        self.set_current_step(company.id, settings.TENANT_REGISTER_STEP_MY_PROFILE)

        return Response(status=status.HTTP_201_CREATED, data={
            'current_step': settings.TENANT_REGISTER_STEP_MY_PROFILE,
            'is_completed': False,
        })


class TenantMyProfileView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper, TenantHelper):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []

    def get(self, request, *args, **kwargs):
        return Response(
            status=status.HTTP_200_OK,
            data=UserSerializer(self.get_user(request.GET.get('auth_token'))).data
        )

    @atomic
    def post(self, request, *args, **kwargs):
        user = self.get_user(request.data.get('auth_token'))
        company_attribute = CompanyAttribute.objects.get(company=user.company)
        if company_attribute.tenant_register_current_step != settings.TENANT_REGISTER_STEP_MY_PROFILE:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'detail': settings.TENANT_REGISTER_FINISH_ERROR_MESSAGE
            })
        available_columns = [
            'email_signature',
            'first_name',
            'last_name',
            'tel1',
            'tel2',
            'tel3',
            'avatar',
            'user_service_id',
        ]

        validate_tel_values(request.data, 'user')
        is_valid, msg = validate_user_service_id(user.id, user.company.id, self.request.data.get('user_service_id'))
        if not is_valid:
            raise serializers.ValidationError({'user_service_id': msg})

        for column in available_columns:
            if self.request.data.get(column):
                if column == 'avatar':
                    file = self.request.data.get(column)
                    file_validation(file)
                setattr(user, column, self.request.data.get(column))

        if self.request.data.get('password'):
            if password_validator(self.request.data.get('password'), settings.LENGTH_VALIDATIONS['user']['password']):
                return Response(status=status.HTTP_400_BAD_REQUEST, data={
                    'password': settings.LENGTH_VALIDATIONS['user']['password']['message']
                })
            user.set_password(self.request.data.get('password'))

        user.save()

        company_attribute.tenant_register_current_step = settings.TENANT_REGISTER_STEP_COMPLETED
        trial_expiration_datetime = (
            datetime.now(pytz.timezone(settings.TIME_ZONE)) + timedelta(days=settings.TRIAL_EXPIRATION_DAYS)
        )
        company_attribute.trial_expiration_date = trial_expiration_datetime.date()
        company_attribute.save()

        context = {
            'expiration_time': trial_expiration_datetime.replace(hour=23, minute=59),
        }
        send_system_mail(
            user.company_id,
            settings.TEMPLATE_EMAIL_TENANT_REGISTER_FINISH_SUBJECT,
            settings.TEMPLATE_EMAIL_TENANT_REGISTER_FINISH_MESSAGE,
            context
        )

        logger_stdout.info('tenant_register_finish_info : Company: {0}, User: {1}, Email: {2}'.format(
            user.company.name,
            user.display_name,
            user.email
        ))

        return Response(status=status.HTTP_200_OK, data={
            'current_step': settings.TENANT_REGISTER_STEP_COMPLETED,
            'is_completed': True
        })


class TenantCurrentStepView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper, TenantHelper):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []

    def get(self, request, *args, **kwargs):
        try:
            user = self.get_user(request.GET.get('auth_token'))
            company_attribute = CompanyAttribute.objects.get(company=user.company)
            return Response(status=status.HTTP_200_OK, data={
                'current_step': company_attribute.tenant_register_current_step,
                'is_completed': company_attribute.tenant_register_current_step == settings.TENANT_REGISTER_STEP_COMPLETED
            })
        except Exception:
            return Response(status=status.HTTP_200_OK, data={
                'current_step': settings.TENANT_REGISTER_STEP_FIRST,
                'is_completed': False,
            })
