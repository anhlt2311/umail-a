import re
import secrets

from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.core.cache import cache
from django.core.signing import SignatureExpired, dumps, loads
from django.db.models import ObjectDoesNotExist
from django.db.transaction import atomic
from django.template.loader import render_to_string
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import permissions, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.filters import OrderingFilter
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from app_staffing.exceptions.addon import (MultiActivePlanFoundException, PlanNotFoundException)
from app_staffing.exceptions.base import BulkOperationLimitException, RequiredFieldsException
from app_staffing.exceptions.user import (UserLoginCanNotInActiveException, UserRegisterActivateLimitException, UserAlreadyInvitedException,
                                          UserDuplicateEmailException, DeleteFileError,
                                          UserDoesNotExistException,
                                          PermissionDeleteAvatarException, DeleteAvatarDefault,
                                          UserUpdateActivateLimitException, UserLoginCanNotDeleteException)
from app_staffing.exceptions.validation import (CannotActivateException, CannotDeactivateException,
                                                CannotDeleteException, CannotUpdateException,
                                                CannotDeleteUserException)
from app_staffing.models import (CompanyAttribute, Plan, User)
from app_staffing.models.user import ADMIN, MASTER
from app_staffing.serializers import (MyProfileSerializer, UserSerializer)
from app_staffing.utils.utils import validate_tel_values
from app_staffing.views.base import (
    BulkOperationHelper, CustomTokenAuthentication, CustomValidationHelper, GenericDetailAPIView,
    GenericListAPIView, GenericRetrieveUpdateAPIView, IsAuthorizedAction, MultiTenantMixin,
    UnauthorizedActionView, UserHelper)
from app_staffing.views.classes.pagenation import StandardPagenation
from app_staffing.views.classes.permissions import IsSelfOrUserAdmin
from app_staffing.views.filters import UserFilter


class UsersView(MultiTenantMixin, GenericListAPIView, UserHelper, CustomValidationHelper, BulkOperationHelper):
    queryset = User.objects.select_related('modified_user', 'user_role').filter(is_hidden=False)
    serializer_class = UserSerializer
    pagination_class = StandardPagenation
    permission_classes = (IsSelfOrUserAdmin, IsAuthorizedAction)

    filterset_class = UserFilter
    filter_backends = (DjangoFilterBackend, OrderingFilter)

    ordering_fields = ('first_name', 'email', 'last_login', 'date_joined', 'role', 'user_role__order')
    ordering = ('-date_joined',)

    # ユーザー招待
    def create(self, request, *args, **kwargs):
        # Sync email and username. (username is used for login.)
        email = request.data.get('email')
        if email:
            # 招待済みのユーザーに対して実行した and 再送リンクではない場合はエラー
            if User.objects.filter(email=email).exists() and request.data.get('sendAgain') is None:
                raise UserAlreadyInvitedException()
            # 招待メールは送信済み かつ 招待されたユーザーがまだ本登録をしてない状態で、再度メール招待を送った場合を想定
            if User.objects.filter(email=email, registed_at__isnull=True).exists():
                user = User.objects.get(email=email)
                self.send_mail(user)
                return Response(status=status.HTTP_204_NO_CONTENT, data=[])
            else:
                # Warning: If request.data is a form media type, request.data become immutable and cause a crash.
                # TODO: Fix this behavior if you want to use this API as a form media type.
                # Detail: https://stackoverflow.com/questions/52367379/why-is-django-rest-frameworks-request-data-sometimes-immutable
                # 仮登録
                request.data['username'] = email
                request.data['is_active'] = False
                request.data['last_name'] = ''
                request.data['first_name'] = ''
                request.data['user_service_id'] = User.generate_random_user_service_id()

                password = secrets.token_urlsafe(10)
                while True:
                    if bool(re.search(r"[a-zA-Z]", password)) and bool(re.search(r"[0-9]", password)) \
                            and bool(re.search(r"([`~!@#$%^&*()_+\-={}[\]¥|:;\"'<>,.?/])", password)):
                        break
                    password = secrets.token_urlsafe(10)

                request.data['password'] = password

        return super().create(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        resource_ids = self.get_unique_items(request.data['source'])
        if self.is_over_bulk_operation_limit(resource_ids):
            raise BulkOperationLimitException()
        value = request.data['value']
        column = request.data['column']
        if column and column == 'is_active':
            if str(request.user.id) in resource_ids:
                raise UserLoginCanNotInActiveException()
            users = User.objects.filter(id__in=resource_ids)
            if value == False:
                error_messages = self.get_relation_error_messages(users)
                if 0 < len(error_messages):
                    raise CannotDeactivateException()
                else:
                    self.delete_user_settings(users)
            elif value == True:
                active_user_count = User.objects.filter(company=self.request.user.company, is_active=True).count()
                attribute = CompanyAttribute.objects.get(company=self.request.user.company)
                if active_user_count + len(users) > attribute.user_registration_limit:
                    raise UserUpdateActivateLimitException()

            users.update(is_active=value)
        return Response(status=status.HTTP_200_OK, data=resource_ids)

    def perform_create(self, serializer):
        user = serializer.save()
        self.send_mail(user)

    def send_mail(self, user):
        current_site = get_current_site(self.request)
        domain = current_site.domain
        sender = User.objects.get(pk=self.request.user.id)
        context = {
            'protocol': self.request.scheme,
            'domain': domain,
            'token': dumps(str(user.pk)),
            'user': user,
            'sender': sender,
        }

        cache_key = f'{settings.CACHE_INVITE_KEY_BASE}_{user.pk}'
        cache.set(cache_key, context, settings.INVITE_EMAIL_EXPIRE_SECONDS)
        subject = render_to_string(settings.TEMPLATE_EMAIL_INVITATION_SUBJECT, context)
        message = render_to_string(settings.TEMPLATE_EMAIL_INVITATION_MESSAGE, context)
        user.email_user(subject, '', html_message=message)

    def delete(self, request, *args, **kwargs):
        resource_ids = self.get_unique_items(request.data['source'])
        if str(request.user.id) in resource_ids:
            raise UserLoginCanNotDeleteException()
        if self.is_over_bulk_operation_limit(resource_ids):
            raise BulkOperationLimitException()
        users = User.objects.filter(id__in=resource_ids, company=self.request.user.company)
        error_messages = self.get_relation_error_messages(users)
        if 0 < len(error_messages):
            raise CannotDeleteUserException()
        self.hide(users)
        return Response(status=status.HTTP_204_NO_CONTENT, data=resource_ids)

class UsersDetailView(MultiTenantMixin, GenericDetailAPIView, UserHelper, CustomValidationHelper):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsSelfOrUserAdmin, IsAuthorizedAction)

    def update(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        validate_tel_values(request.data, 'user')
        if 'is_active' in request.data and request.user.id == self.get_object().id:
            raise UserLoginCanNotInActiveException()
        if self.get_object().is_active == True and request.data.get('is_active') == False:
            error_messages = self.get_relation_error_messages([self.get_object()])
            if 0 < len(error_messages):
                raise CannotDeactivateException()
            else:
                self.delete_user_settings([self.get_object()])
        elif self.get_object().is_active == False and request.data.get('is_active') == True:
            active_user_count = User.objects.filter(company=self.request.user.company, is_active=True).count()
            attribute = CompanyAttribute.objects.get(company=self.get_object().company)
            if active_user_count + 1 > attribute.user_registration_limit:
                raise UserUpdateActivateLimitException()
        # Sync email and username. (username is used for login.)
        if request.data.get('email'):
            # usernameが重複する場合はエラーにする
            if User.objects.filter(email=request.data.get('email')).exclude(id=str(kwargs.get('pk'))).exists():
                raise UserDuplicateEmailException()
            # Note: # Note: If request.data is a form media type, request.data become immutable and cause a crash.
            # TODO: Fix this behavior if you want to use this API as a form media type.
            # Detail: https://stackoverflow.com/questions/52367379/why-is-django-rest-frameworks-request-data-sometimes-immutable
            request.data['username'] = request.data.get('email')
        return super().update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        user = self.get_object()
        if request.user.id == user.id:
            raise UserLoginCanNotDeleteException()
        error_messages = self.get_relation_error_messages([user])
        if 0 < len(error_messages):
            raise CannotDeleteUserException()
        self.hide([user])
        return Response(status=status.HTTP_204_NO_CONTENT, data=[])


class UsersRegisterView(MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]
    authentication_classes = []

    def get_object(self):
        token = self.kwargs.get('token')
        user_pk = loads(token, max_age=settings.INVITE_EMAIL_EXPIRE_SECONDS)
        return User.objects.get(pk=user_pk)

    @atomic
    def update(self, request, *args, **kwargs):
        try:
            attribute = CompanyAttribute.objects.get(company=self.get_object().company)
        except:
            active_plans = Plan.objects.filter(is_active=True, company=self.get_object().company)

            if not active_plans or 0 == len(active_plans):
                raise PlanNotFoundException()
            elif 1 < len(active_plans):
                raise MultiActivePlanFoundException()
            current_active_plan = active_plans[0]
            attribute = CompanyAttribute.objects.create(
                company=self.get_object().company,
                user_registration_limit=current_active_plan.plan_master.default_user_count
            )

        user = self.get_object()
        current_user_count = User.objects.filter(company=user.company, is_active=True).count()

        if current_user_count >= attribute.user_registration_limit:
            raise UserRegisterActivateLimitException()

        request.user = user
        self.exec_custom_validation(request)
        request.data['is_active'] = True
        request.data['registed_at'] = timezone.now()
        return super().update(request, *args, **kwargs)


class MyProfileView(GenericRetrieveUpdateAPIView, CustomValidationHelper):
    """A to see / edit own profile.
    Note: This is a wrapper of model User, but it only allows to edit specific fields.
    """
    serializer_class = MyProfileSerializer

    def get_object(self):
        return User.objects.get(pk=self.request.user.id)

    def update(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        validate_tel_values(request.data, 'user')

        return super().update(request, *args, **kwargs)


class ResendInvitationEmailView(UnauthorizedActionView, CreateAPIView):
    def create(self, request, *args, **kwargs):
        try:
            user = User.objects.get(email=request.data.get('email'))
            if user.registed_at is None:
                self.send_mail(user)
                return Response(status=status.HTTP_200_OK, data={ 'result': 'OK' })
            else:
                return Response(status=status.HTTP_200_OK, data={ 'result': 'AlreadyRegistered' })

        except SignatureExpired:
            return Response(status=status.HTTP_200_OK, data={ 'result': 'SignatureExpired' })

        except ObjectDoesNotExist:
            return Response(status=status.HTTP_200_OK, data={ 'result': 'AccountDoesNotExist' })

    def send_mail(self, user):
        cache_key = f'{settings.CACHE_INVITE_KEY_BASE}_{user.pk}'
        context = cache.get(cache_key)

        if context is None:
            raise SignatureExpired
        subject = render_to_string(settings.TEMPLATE_EMAIL_INVITATION_SUBJECT, context)
        message = render_to_string(settings.TEMPLATE_EMAIL_INVITATION_MESSAGE, context)
        user.email_user(subject, '', html_message=message)


class UserAvatarView(APIView):
    authentication_classes = (CustomTokenAuthentication, SessionAuthentication)
    permission_classes = (permissions.IsAuthenticated,)

    def delete(self, request, format=None):
        if 'email' not in request.data:
            raise RequiredFieldsException('email')

        try:
            request_user = request.user
            email = request.data['email']
            user = User.objects.get(email=email, is_hidden=False, is_active=True)
            if user == request_user or request_user.user_role.name in [ADMIN, MASTER]:
                file = user.avatar
                if file.name == '' or file.name is None:
                    raise DeleteAvatarDefault
                file.storage.delete(name=file.name)
                user.avatar = None
                user.save()
            else:
                raise CannotDeleteException
        except User.DoesNotExist:
            raise UserDoesNotExistException
        except CannotDeleteException:
            raise PermissionDeleteAvatarException
        except DeleteAvatarDefault:
            raise DeleteAvatarDefault
        except Exception:
            raise DeleteFileError
        return Response(status=status.HTTP_204_NO_CONTENT,
                        data={'detail': settings.USER_DELETE_AVATAR_SUCCESS})
