from django.conf import settings
from django.core.cache import cache
from django.core.signing import SignatureExpired

from rest_framework import status
from rest_framework.generics import CreateAPIView, UpdateAPIView

from app_staffing.utils.custom_validation import password_validator
from app_staffing.views.base import UnauthorizedActionView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from django.core.signing import loads, dumps
from app_staffing.models import User
from rest_framework.authtoken.models import Token
from django.db.models import ObjectDoesNotExist
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from app_staffing.serializers import UserSerializer

import secrets

from app_staffing.exceptions.base import PasswordNotMatchException


class PasswordResetEmailView(UnauthorizedActionView, CreateAPIView):

    def create(self, request, *args, **kwargs):
        email = request.data.get('email')
        if email is None:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'email': '必須項目です。'})

        if len(str(email)) > settings.LENGTH_VALIDATIONS['user']['email']['max']:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={
                'email': settings.LENGTH_VALIDATIONS['user']['email']['message']
            })
        try:
            user = User.objects.get(email=email)
            self.send_mail(user)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_200_OK, data={ 'result': 'AccountDoesNotExist' })

        return Response(
            status=status.HTTP_200_OK,
            data={
                'result': 'ok'
            }
        )

    def send_mail(self, user):
        current_site = get_current_site(self.request)
        domain = current_site.domain

        salt = secrets.token_hex(16)
        token = dumps(str(user.pk), salt=salt)

        context = {
            'protocol': self.request.scheme,
            'domain': domain,
            'token': token,
            'display_name': user.display_name,
        }

        cache_key = f'{settings.CACHE_PASSWORD_RESET_KEY_BASE}_{token}'
        cache.set(cache_key, salt, settings.PASSWORD_RESET_EMAIL_EXPIRE_SECONDS)

        subject = render_to_string(settings.TEMPLATE_EMAIL_PASSWORD_RESET_SUBJECT, context)
        message = render_to_string(settings.TEMPLATE_EMAIL_PASSWORD_RESET_MESSAGE, context)
        user.email_user(subject, '', html_message=message)

class PasswordChangeView(UnauthorizedActionView, UpdateAPIView):
    serializer_class = UserSerializer

    def update(self, request, *args, **kwargs):
        try:
            if request.data.get('password') is None or request.data.get('password_confirm') is None:
                data = {}
                if request.data.get('password') is None:
                    data['password'] = '必須項目です。'
                if request.data.get('password_confirm') is None:
                    data['password_confirm'] = '必須項目です。'
                return Response(status=status.HTTP_400_BAD_REQUEST, data=data)

            if request.data['password'] != request.data['password_confirm']:
                raise PasswordNotMatchException()

            if password_validator(request.data['password'], settings.LENGTH_VALIDATIONS['user']['password']):
                return Response(status=status.HTTP_400_BAD_REQUEST, data={
                    'password': settings.LENGTH_VALIDATIONS['user']['password']['message']
                })
            super().update(request, *args, **kwargs)

            # ログインしているセッションを無効化
            Token.objects.filter(user=self.get_object()).delete()

            token = self.kwargs.get('token')
            cache_key = f'{settings.CACHE_PASSWORD_RESET_KEY_BASE}_{token}'
            cache.delete(cache_key)
            return Response(status=status.HTTP_204_NO_CONTENT)
        except SignatureExpired:
            return Response(status=status.HTTP_410_GONE)

    def get(self, request, *args, **kwargs):
        try:
            self.get_object()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except SignatureExpired:
            return Response(status=status.HTTP_410_GONE)

    def get_object(self):
        if hasattr(self, 'object'):
            return self.object
        token = self.kwargs.get('token')
        cache_key = f'{settings.CACHE_PASSWORD_RESET_KEY_BASE}_{token}'
        salt = cache.get(cache_key)
        if salt is None:
            raise SignatureExpired

        user_pk = loads(token, salt=salt, max_age=settings.PASSWORD_RESET_EMAIL_EXPIRE_SECONDS)
        self.object = User.objects.get(pk=user_pk)
        return self.object
