from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.board.models.card import Personnel, Project
from app_staffing.models.base import OneToOneModel
from app_staffing.models import ScheduledEmail


class PersonnelScheduledEmailHistory(OneToOneModel, SoftDeleteObject):
    card = models.ForeignKey(Personnel, on_delete=models.CASCADE, related_name="personnel_emails")
    email = models.ForeignKey(ScheduledEmail, on_delete=models.CASCADE, related_name='personnel_histories')


class ProjectScheduledEmailHistory(OneToOneModel, SoftDeleteObject):
    card = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="project_emails")
    email = models.ForeignKey(ScheduledEmail, on_delete=models.CASCADE, related_name='project_histories')
