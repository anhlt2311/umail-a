from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.board.models.card import Personnel, Project
from app_staffing.models.base import PerCompanyModel


class BaseDynamicRow(PerCompanyModel, SoftDeleteObject):
    title = models.CharField(max_length=50, blank=True, null=True)
    content = models.TextField(max_length=300, blank=True, null=True)

    class Meta:
        abstract = True


class PersonnelDynamicRow(BaseDynamicRow):
    personnel = models.ForeignKey(Personnel, on_delete=models.CASCADE, related_name="personnel_dynamic_row", null=True,
                                  blank=True)


class ProjectDynamicRow(BaseDynamicRow):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="project_dynamic_row", null=True,
                                blank=True)
