from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.board.models.card import Personnel, Project
from app_staffing.models.base import PerCompanyModel


class BaseCheckListBoardModel(PerCompanyModel, SoftDeleteObject):
    title = models.CharField(max_length=255, blank=False, null=False)
    show_finished = models.BooleanField(default=False)

    class Meta:
        abstract = True


class PersonnelCheckList(BaseCheckListBoardModel):
    personnel = models.ForeignKey(Personnel, on_delete=models.CASCADE, related_name="checklist_personnel", null=True,
                                  blank=True)


class ProjectCheckList(BaseCheckListBoardModel):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="checklist_project", null=True,
                                blank=True)
