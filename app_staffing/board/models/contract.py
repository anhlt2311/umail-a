from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.board.models.card import Personnel, Project
from app_staffing.models import Organization, Contact
from app_staffing.models.base import PerCompanyModel


class BaseContract(PerCompanyModel, SoftDeleteObject):
    higher_organization = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True, blank=True,
                                            related_name="higher_organization_%(class)s")
    lower_organization = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True, blank=True,
                                           related_name="lower_organization_%(class)s")
    higher_contact = models.ForeignKey(Contact, on_delete=models.CASCADE, null=True, blank=True,
                                       related_name="higher_contact_%(class)s")
    lower_contact = models.ForeignKey(Contact, on_delete=models.CASCADE, null=True, blank=True,
                                      related_name="lower_contact_%(class)s")

    class Meta:
        abstract = True


class PersonnelContract(BaseContract):
    personnel = models.ForeignKey(Personnel, on_delete=models.CASCADE, related_name="personnel_contracts", null=True,
                                  blank=True)

    detail = models.CharField(max_length=50, blank=True, null=True)
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)

    @property
    def project_period(self):
        return [self.start, self.end]


class ProjectContract(BaseContract):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="project_contracts", null=True,
                                blank=True)

    personnel = models.ForeignKey(Personnel, on_delete=models.CASCADE, related_name="personnel_targets", null=True,
                                  blank=True)
