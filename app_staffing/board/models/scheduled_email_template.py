from django.db import models
from softdelete.models import SoftDeleteObject

from app_staffing.board.models.card import Personnel, Project
from app_staffing.models.base import OneToOneModel
from app_staffing.models.email import TEXT_FORMAT, TEXT


class BaseScheduledEmailTemplate(OneToOneModel, SoftDeleteObject):
    text_format = models.CharField(blank=True, null=True, max_length=8, choices=TEXT_FORMAT, default=TEXT)
    subject = models.TextField(max_length=255, help_text='Subject of the email.', blank=True, null=True,db_index=True)
    upper = models.TextField(help_text='Message upper text of the email.', blank=True, null=True)
    lower = models.TextField(help_text='Message lower text of the email.', blank=True, null=True)
    send_copy_to_share = models.BooleanField(default=False)
    send_copy_to_sender = models.BooleanField(
        default=False, help_text='If True, this app sends a copy of the email to the sender.'
    )

    # 配信職種に関する条件
    personnelskill_dev_beginner = models.BooleanField(default=False)
    personnelskill_dev_hosyu = models.BooleanField(default=False)
    personnelskill_dev_kihon = models.BooleanField(default=False)
    personnelskill_dev_seizou = models.BooleanField(default=False)
    personnelskill_dev_syousai = models.BooleanField(default=False)
    personnelskill_dev_test = models.BooleanField(default=False)
    personnelskill_dev_youken = models.BooleanField(default=False)
    personnelskill_infra_beginner = models.BooleanField(default=False)
    personnelskill_infra_hosyu = models.BooleanField(default=False)
    personnelskill_infra_kanshi = models.BooleanField(default=False)
    personnelskill_infra_kihon = models.BooleanField(default=False)
    personnelskill_infra_kouchiku = models.BooleanField(default=False)
    personnelskill_infra_syousai = models.BooleanField(default=False)
    personnelskill_infra_test = models.BooleanField(default=False)
    personnelskill_infra_youken = models.BooleanField(default=False)
    personneltype_dev = models.BooleanField(default=False)
    personneltype_dev_designer = models.BooleanField(default=False)
    personneltype_dev_front = models.BooleanField(default=False)
    personneltype_dev_other = models.BooleanField(default=False)
    personneltype_dev_pm = models.BooleanField(default=False)
    personneltype_dev_server = models.BooleanField(default=False)
    personneltype_infra = models.BooleanField(default=False)
    personneltype_infra_database = models.BooleanField(default=False)
    personneltype_infra_network = models.BooleanField(default=False)
    personneltype_infra_other = models.BooleanField(default=False)
    personneltype_infra_security = models.BooleanField(default=False)
    personneltype_infra_server = models.BooleanField(default=False)
    personneltype_infra_sys = models.BooleanField(default=False)
    personneltype_other = models.BooleanField(default=False)
    personneltype_other_eigyo = models.BooleanField(default=False)
    personneltype_other_kichi = models.BooleanField(default=False)
    personneltype_other_other = models.BooleanField(default=False)
    personneltype_other_support = models.BooleanField(default=False)

    @property
    def body(self):
        return dict(upper=self.upper, lower=self.lower)

    @property
    def condition(self):
        return dict(
            personnelskill_dev_beginner = self.personnelskill_dev_beginner,
            personnelskill_dev_hosyu = self.personnelskill_dev_hosyu,
            personnelskill_dev_kihon = self.personnelskill_dev_kihon,
            personnelskill_dev_seizou = self.personnelskill_dev_seizou,
            personnelskill_dev_syousai = self.personnelskill_dev_syousai,
            personnelskill_dev_test = self.personnelskill_dev_test,
            personnelskill_dev_youken = self.personnelskill_dev_youken,
            personnelskill_infra_beginner = self.personnelskill_infra_beginner,
            personnelskill_infra_hosyu = self.personnelskill_infra_hosyu,
            personnelskill_infra_kanshi = self.personnelskill_infra_kanshi,
            personnelskill_infra_kihon = self.personnelskill_infra_kihon,
            personnelskill_infra_kouchiku = self.personnelskill_infra_kouchiku,
            personnelskill_infra_syousai = self.personnelskill_infra_syousai,
            personnelskill_infra_test = self.personnelskill_infra_test,
            personnelskill_infra_youken = self.personnelskill_infra_youken,
            personneltype_dev = self.personneltype_dev,
            personneltype_dev_designer = self.personneltype_dev_designer,
            personneltype_dev_front = self.personneltype_dev_front,
            personneltype_dev_other = self.personneltype_dev_other,
            personneltype_dev_pm = self.personneltype_dev_pm,
            personneltype_dev_server = self.personneltype_dev_server,
            personneltype_infra = self.personneltype_infra,
            personneltype_infra_database = self.personneltype_infra_database,
            personneltype_infra_network = self.personneltype_infra_network,
            personneltype_infra_other = self.personneltype_infra_other,
            personneltype_infra_security = self.personneltype_infra_security,
            personneltype_infra_server = self.personneltype_infra_server,
            personneltype_infra_sys = self.personneltype_infra_sys,
            personneltype_other = self.personneltype_other,
            personneltype_other_eigyo = self.personneltype_other_eigyo,
            personneltype_other_kichi = self.personneltype_other_kichi,
            personneltype_other_other = self.personneltype_other_other,
            personneltype_other_support = self.personneltype_other_support,
        )

    def to_dictionary(self):
        return {
            "id": self.id,
            "subject": self.subject,
            "body": self.body,
            "text_format": self.text_format,
            "send_copy_to_share": self.send_copy_to_share,
            "send_copy_to_sender": self.send_copy_to_sender,
            "condition": self.condition,
        }

    class Meta:
        abstract = True


class PersonnelScheduledEmailTemplate(BaseScheduledEmailTemplate):
    personnel = models.ForeignKey(Personnel, on_delete=models.CASCADE, related_name="templates", null=True,
                                  blank=True)


class ProjectScheduledEmailTemplate(BaseScheduledEmailTemplate):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="templates", null=True,
                                blank=True)
