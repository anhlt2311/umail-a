from django.db.models import Prefetch
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_http_methods
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter

from app_staffing.board.exceptions import ActionCardListNotAllowedException
from app_staffing.board.exceptions import PersonnelCannotDeleteDueToContractException
from app_staffing.board.models import PersonnelCardList, Personnel, PersonnelAssignee, PersonnelCheckList, \
    PersonnelCheckListItem, PersonnelSkillAssignment, PersonnelComment, PersonnelDynamicRow, PersonnelSkillSheet, \
    ProjectContract, PersonnelScheduledEmailTemplate
from app_staffing.board.serializers import CardListSerializer, PersonnelBoardCardListCreateSerializer, \
    PersonnelBoardCardSummarySerializer, PersonnelDetailUpdateDestroySerializer, PersonnelOrderUpdateSerializer, \
    PersonnelSkillSheetSerializer, PersonnelCopyCardSerializer, ActionPersonnelCardListDetailSerializer, \
    UpdatePersonnelScheduledEmailTemplateSerializer
from app_staffing.board.services import PersonnelService
from app_staffing.board.views.base import BaseOrderUpdateAPIView
from app_staffing.board.views.filters.personnel import PersonnelFilter
from app_staffing.views.base import FileDetailView, MultiTenantMixin, GenericDetailAPIView, GenericListAPIView, \
    CardListMixin, PersonnelBoardBaseView, CustomValidationHelper, FileListCreateUsingPatchView
from app_staffing.views.classes.pagenation import StandardPagenation


class PersonnelCardListAPIView(MultiTenantMixin, GenericListAPIView):
    queryset = PersonnelCardList.objects.order_by('created_time')
    serializer_class = CardListSerializer

    def create(self, request, *args, **kwargs):
        company = request.user.company
        PersonnelService.create_card_list(company.id)
        return Response(status=status.HTTP_201_CREATED)


class GetPersonnelCardListAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericListAPIView):
    serializer_class = PersonnelBoardCardSummarySerializer
    queryset = Personnel.objects.filter(deleted_at__isnull=True).prefetch_related(
        Prefetch(
            'checklist_personnel',
            queryset=PersonnelCheckList.objects.order_by('created_time').prefetch_related(
                Prefetch(
                    'items',
                    queryset=PersonnelCheckListItem.objects.order_by('created_time')
                )
            ),
            to_attr="checklist"
        )
    ).prefetch_related(
        Prefetch(
            'personnel_assignees',
            queryset=PersonnelAssignee.objects.order_by('created_time').select_related('user'),
            to_attr='assignees'
        )
    ).prefetch_related(
        Prefetch(
            'skill_assignment',
            queryset=PersonnelSkillAssignment.objects.select_related('skill').filter(skill__is_skill=True).order_by(
                'created_time'),
        )
    ).prefetch_related(
        Prefetch(
            'comments',
            queryset=PersonnelComment.objects.select_related('created_user', 'modified_user').filter(is_important=True).
            order_by('-is_important', '-modified_time'),
        )
    ).prefetch_related('personnel_contracts')

    filterset_class = PersonnelFilter

    filter_backends = (DjangoFilterBackend, OrderingFilter)
    pagination_class = StandardPagenation
    ordering_fields = (
        'order', 'first_name', 'priority', 'end', 'age', 'gender', 'operate_period', 'affiliation', 'parallel', 'price')
    ordering = ('order',)

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(card_list=self.kwargs['pk'])


class ActionPersonnelCardListDetailAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericDetailAPIView):
    serializer_class = ActionPersonnelCardListDetailSerializer
    queryset = PersonnelCardList.objects

    def patch(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        action = self.kwargs['action']
        company = request.user.company
        if action == 'move':
            list_id = self.request.data['list_id']
            if list_id == str(pk):
                return Response(status=status.HTTP_200_OK)
            try:
                cardlist = PersonnelCardList.objects.get(pk=list_id, company=company)
            except PersonnelCardList.DoesNotExist:
                raise ActionCardListNotAllowedException()
            max_order = cardlist.personnel_items.order_by('order').last()
            if max_order:
                max_order = max_order.order
            else:
                max_order = -1
            # # Get all personnel of card list
            items = PersonnelCardList.objects.get(pk=pk, company=company).personnel_items.order_by('order')
            # Move Personnel reference new PersonnelCardList.
            for item in items:
                max_order += 1
                item.order = max_order
                item.card_list = cardlist
                item.save()
        elif action == 'archive':
            PersonnelCardList.objects.get(pk=pk, company=company).personnel_items.update(is_archived=True)
        else:
            raise ActionCardListNotAllowedException()
        return Response(status=status.HTTP_200_OK)


@method_decorator(require_http_methods(["POST"]), name='dispatch')
class PersonnelBoardCardListCreateAPIView(PersonnelBoardBaseView, CardListMixin, MultiTenantMixin,
                                          GenericListAPIView, CustomValidationHelper):
    serializer_class = PersonnelBoardCardListCreateSerializer

    def post(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().post(request, *args, **kwargs)


@method_decorator(require_http_methods(["PATCH"]), name='dispatch')
class PersonnelOrderUpdateAPIView(BaseOrderUpdateAPIView):
    queryset = Personnel.objects.filter(deleted_at__isnull=True)
    serializer_class = PersonnelOrderUpdateSerializer


class PersonnelDetailUpdateDestroyAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericDetailAPIView):
    serializer_class = PersonnelDetailUpdateDestroySerializer
    queryset = Personnel.objects.filter(deleted_at__isnull=True).prefetch_related(
        Prefetch(
            'personnel_dynamic_row',
            queryset=PersonnelDynamicRow.objects.order_by('created_time'),
            to_attr="dynamic_row"
        )
    ).prefetch_related(
        Prefetch(
            'personnel_assignees',
            queryset=PersonnelAssignee.objects.order_by('created_time').select_related('user'),
            to_attr='assignees'
        )
    ).prefetch_related(
        Prefetch(
            'skill_assignment',
            queryset=PersonnelSkillAssignment.objects.select_related('skill').order_by('created_time'),
            to_attr='skills'
        )
    ).prefetch_related(
        Prefetch(
            'comments',
            queryset=PersonnelComment.objects.select_related('created_user', 'modified_user').filter(is_important=True).
                order_by('created_time'),
        )
    ).prefetch_related('templates').select_related('created_user', 'modified_user')

    def perform_destroy(self, instance):
        if ProjectContract.objects.filter(personnel=instance).exists():
            raise PersonnelCannotDeleteDueToContractException()

        super().perform_destroy(instance)


class PersonnelSkillSheetAPIView(MultiTenantMixin, FileListCreateUsingPatchView):
    serializer_class = PersonnelSkillSheetSerializer
    queryset = PersonnelSkillSheet.objects.all()

    def get_queryset(self):
        return super().get_queryset().filter(personnel=self.kwargs['parent_pk'])


class PersonnelSkillSheetDetailAPIView(MultiTenantMixin, FileDetailView):
    queryset = PersonnelSkillSheet.objects.all()
    serializer_class = PersonnelSkillSheetSerializer


@method_decorator(require_http_methods(["POST"]), name='dispatch')
class CopyCardPersonnelAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericListAPIView, CustomValidationHelper):
    queryset = Personnel.objects
    serializer_class = PersonnelCopyCardSerializer

    def post(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().post(request, *args, **kwargs)


@method_decorator(require_http_methods(["PATCH"]), name='dispatch')
class PersonnelUpdateTemplateAPIView(PersonnelBoardBaseView, GenericDetailAPIView, CustomValidationHelper):
    queryset = PersonnelScheduledEmailTemplate.objects
    serializer_class = UpdatePersonnelScheduledEmailTemplateSerializer

    def get_queryset(self):
        return super().get_queryset().filter(personnel_id=self.kwargs.get('card_id'))

    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().patch(request, *args, **kwargs)
