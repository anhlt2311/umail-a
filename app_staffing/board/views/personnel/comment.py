from django.db.models import Q

from app_staffing.board.models import PersonnelComment, Personnel
from app_staffing.board.serializers import PersonnelCommentSerializer, PersonnelSubCommentSerializer
from app_staffing.exceptions.comment import CommentImportantLimitException
from app_staffing.views.base import MultiTenantMixin, GenericCommentListView, CommentValidationHelper, \
    GenericCommentDetailView, CustomValidationHelper, GenericSubCommentListView, PersonnelBoardBaseView


class PersonnelCommentListView(PersonnelBoardBaseView, MultiTenantMixin, GenericCommentListView,
                               CommentValidationHelper):
    queryset = PersonnelComment.objects.all_with_deleted().filter(parent__isnull=True). \
        filter(Q(deleted_at__isnull=True) | Q(deleted_at__isnull=False, has_subcomment=True)). \
        select_related('created_user').prefetch_related('sub_comments')
    related_model_class = Personnel
    serializer_class = PersonnelCommentSerializer
    related_field_name = 'personnel'

    ordering_fields = ('is_important', 'created_time',)
    ordering = ('-is_important', '-created_time',)

    def post(self, request, *args, **kwargs):
        if self.is_personnel_comment_over_limit(request, self.related_model_id):
            raise CommentImportantLimitException()
        return super().post(request, *args, **kwargs)


class PersonnelSubCommentListView(PersonnelBoardBaseView, MultiTenantMixin, GenericSubCommentListView,
                                  CommentValidationHelper):
    queryset = PersonnelComment.objects.filter(parent__isnull=False, is_important=False).select_related('created_user')
    related_model_class = Personnel
    serializer_class = PersonnelSubCommentSerializer
    related_field_name = 'personnel'

    parent_model_class = PersonnelComment
    parent_field_name = 'parent'

    ordering_fields = ('created_time',)
    ordering = ('created_time',)


class PersonnelCommentDetailView(PersonnelBoardBaseView, MultiTenantMixin, GenericCommentDetailView,
                                 CustomValidationHelper,
                                 CommentValidationHelper):
    queryset = PersonnelComment.objects.select_related('created_user')
    serializer_class = PersonnelCommentSerializer
    related_field_name = 'personnel'
    comment_model = PersonnelComment

    def patch(self, request, pk, *args, **kwargs):
        self.exec_custom_validation(request)
        if self.update_personnel_important_comment_or_pass(pk) and \
                self.is_personnel_comment_over_limit(request, self.related_model_id):
            raise CommentImportantLimitException()
        return super().patch(request, *args, **kwargs)
