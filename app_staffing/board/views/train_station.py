from django.utils.decorators import method_decorator
from django.views.decorators.http import require_http_methods
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter

from app_staffing.board.models import TrainStation
from app_staffing.board.serializers import TrainStationSerializer
from app_staffing.board.views.filters.train_station import TrainStationFilter
from app_staffing.views.base import GenericListAPIView
from app_staffing.views.classes.pagenation import TrainStationPagination


@method_decorator(require_http_methods(["GET"]), name='dispatch')
class TrainstationListAPIView(GenericListAPIView):
    serializer_class = TrainStationSerializer
    queryset = TrainStation.objects

    filterset_class = TrainStationFilter

    filter_backends = (DjangoFilterBackend, OrderingFilter)

    ordering = ('name',)

    pagination_class = TrainStationPagination
