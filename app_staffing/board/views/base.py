from rest_framework import status
from rest_framework.response import Response

from app_staffing.views.base import CustomValidationHelper, GenericDetailAPIView, MultiTenantMixin, \
    PersonnelBoardBaseView


class BaseOrderUpdateAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericDetailAPIView, CustomValidationHelper):

    def patch(self, request, pk, *args, **kwargs):
        self.exec_custom_validation(request)
        data = request.data.copy()
        position = int(data.get("position", None))
        super().patch(request, pk, *args, **kwargs)
        card = self.get_object()
        card.to(position)
        return Response(status=status.HTTP_200_OK)
