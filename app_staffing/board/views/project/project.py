from django.db.models import Prefetch
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_http_methods
from rest_framework import status
from rest_framework.response import Response

from app_staffing.board.models import ProjectCheckList, ProjectCheckListItem, ProjectSkillAssignment, ProjectAssignee, Project, \
    ProjectComment, ProjectCardList, ProjectDynamicRow, ProjectScheduledEmailTemplate
from app_staffing.board.serializers import ProjectBoardCreateSerializer, \
    ProjectBoardCardSummarySerializer, ProjectDetailSerializer, ProjectOrderUpdateSerializer, \
    ProjectCardListSerializer, ProjectUpdateDestroySerializer, ProjectCopyCardSerializer, \
    UpdateProjectScheduledEmailTemplateSerializer, ActionProjectCardListDetailSerializer
from app_staffing.board.services import ProjectBoardService
from app_staffing.board.views.base import BaseOrderUpdateAPIView
from app_staffing.views.base import SummaryForDetailMixin, PersonnelBoardBaseView, MultiTenantMixin, GenericListAPIView, \
    CardListMixin, GenericDetailAPIView, CustomValidationHelper, \
    ProjectValidationHelper
from app_staffing.views.classes.pagenation import StandardPagenation
from app_staffing.board.exceptions import ActionCardListNotAllowedException


class ProjectCardListAPIView(MultiTenantMixin, GenericListAPIView):
    queryset = ProjectCardList.objects.order_by('created_time')
    serializer_class = ProjectCardListSerializer

    def create(self, request, *args, **kwargs):
        company = request.user.company
        ProjectBoardService.create_card_list(company.id)
        return Response(status=status.HTTP_201_CREATED)


@method_decorator(require_http_methods(["POST"]), name='dispatch')
class ProjectBoardCardListCreateAPIView(CardListMixin, PersonnelBoardBaseView, MultiTenantMixin,
                                        GenericListAPIView, CustomValidationHelper, ProjectValidationHelper):
    serializer_class = ProjectBoardCreateSerializer

    def post(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        self.check_settle_width(request)
        return super().post(request, *args, **kwargs)


class GetProjectCardListAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericListAPIView):
    serializer_class = ProjectBoardCardSummarySerializer
    queryset = Project.objects.filter(deleted_at__isnull=True).order_by('order').prefetch_related(
        Prefetch(
            'checklist_project',
            queryset=ProjectCheckList.objects.order_by('created_time').prefetch_related(
                Prefetch(
                    'items',
                    queryset=ProjectCheckListItem.objects.order_by('created_time')
                )
            ),
            to_attr="checklist"
        )
    ).prefetch_related(
        Prefetch(
            'project_assignees',
            queryset=ProjectAssignee.objects.order_by('created_time').select_related('user'),
            to_attr='assignees'
        )
    ).prefetch_related(
        Prefetch(
            'comments',
            queryset=ProjectComment.objects.select_related('created_user', 'modified_user').filter(is_important=True).
                order_by('-is_important', '-modified_time'),
        ),
    ).select_related('place')

    pagination_class = StandardPagenation

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(card_list=self.kwargs['pk'])


class ProjectDetailUpdateDestroyAPIView(PersonnelBoardBaseView, MultiTenantMixin, SummaryForDetailMixin,
                                        GenericDetailAPIView,
                                        CustomValidationHelper):
    serializer_class = ProjectUpdateDestroySerializer
    serializer_class_for_detail = ProjectDetailSerializer
    queryset = Project.objects.filter(deleted_at__isnull=True).prefetch_related(
        Prefetch(
            'project_dynamic_row',
            queryset=ProjectDynamicRow.objects.order_by('created_time'),
            to_attr="dynamic_row"
        )
    ).prefetch_related(
        Prefetch(
            'project_assignees',
            queryset=ProjectAssignee.objects.order_by('created_time').select_related('user'),
            to_attr='assignees'
        )
    ).prefetch_related(
        Prefetch(
            'skill_assignment',
            queryset=ProjectSkillAssignment.objects.select_related('skill').order_by('created_time'),
            to_attr='skills'
        )
    ).prefetch_related(
        Prefetch(
            'comments',
            queryset=ProjectComment.objects.select_related('created_user', 'modified_user').filter(is_important=True).
                order_by('created_time'),
        )
    ).prefetch_related('templates').select_related('created_user', 'modified_user')

    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().patch(request, *args, **kwargs)


@method_decorator(require_http_methods(["PATCH"]), name='dispatch')
class ProjectOrderUpdateAPIView(BaseOrderUpdateAPIView):
    queryset = Project.objects.filter(deleted_at__isnull=True)
    serializer_class = ProjectOrderUpdateSerializer


@method_decorator(require_http_methods(["POST"]), name='dispatch')
class CopyCardProjectAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericListAPIView):
    queryset = Project.objects
    serializer_class = ProjectCopyCardSerializer


@method_decorator(require_http_methods(["PATCH"]), name='dispatch')
class ProjectUpdateTemplateAPIView(PersonnelBoardBaseView, GenericDetailAPIView, CustomValidationHelper):
    queryset = ProjectScheduledEmailTemplate.objects
    serializer_class = UpdateProjectScheduledEmailTemplateSerializer

    def get_queryset(self):
        return super().get_queryset().filter(project=self.kwargs.get('card_id'))

    def patch(self, request, *args, **kwargs):
        self.exec_custom_validation(request)
        return super().patch(request, *args, **kwargs)


class ActionProjectCardListDetailAPIView(PersonnelBoardBaseView, MultiTenantMixin, GenericDetailAPIView):
    serializer_class = ActionProjectCardListDetailSerializer
    queryset = ProjectCardList.objects

    def patch(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        action = self.kwargs['action']
        company = request.user.company
        if action == 'move':
            list_id = self.request.data['list_id']
            if list_id == str(pk):
                return Response(status=status.HTTP_200_OK)
            try:
                cardlist = ProjectCardList.objects.get(pk=list_id, company=company)
            except ProjectCardList.DoesNotExist:
                raise ActionCardListNotAllowedException()
            max_order = cardlist.project_items.order_by('order').last()
            if max_order:
                max_order = max_order.order
            else:
                max_order = -1
            # # Get all project of card list
            items = ProjectCardList.objects.get(pk=pk, company=company).project_items.order_by('order')
            # Move Project reference new ProjectCardList.
            for item in items:
                max_order += 1
                item.order = max_order
                item.card_list = cardlist
                item.save()
        elif action == 'archive':
            ProjectCardList.objects.get(pk=pk, company=company).project_items.update(is_archived=True)
        else:
            raise ActionCardListNotAllowedException()
        return Response(status=status.HTTP_200_OK)
