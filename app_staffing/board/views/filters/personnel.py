from xmlrpc.client import boolean
from django.db.models import Q
import django_filters.rest_framework as django_filters

from app_staffing.views.filters.base import datetime_range_lt_filter, datetime_range_gte_filter


def gender_filter(queryset, name, value):
    ret_queryset = queryset.filter(gender=value)
    return ret_queryset


def parallel_filter(queryset, name, value):
    ret_queryset = queryset.filter(parallel=value)
    return ret_queryset


def image_filter(queryset, name, value):
    ret_queryset = queryset.exclude(image_name__isnull=boolean(value))
    return ret_queryset


def skill_sheet_filter(queryset, name, value):
    ret_queryset = queryset.exclude(skill_sheets__name__isnull=boolean(value))
    return ret_queryset


def _get_in_clause_query(key, queryset, value):
    splited_values = value.split(',')
    include_values = []
    for splited_value in splited_values:
        include_values.append(splited_value)
    expr = key + '__in'
    return queryset.filter(Q(**{expr: include_values}))


def skill_filter(queryset, name, value):
    return _get_in_clause_query('skill_assignment__skill__internal_value', queryset, value)


def assignee_filter(queryset, name, value):
    return queryset.filter(personnel_assignees__user__id=value, personnel_assignees__deleted_at__isnull=True)


class PersonnelFilter(django_filters.FilterSet):
    assigneeId = django_filters.UUIDFilter(field_name='personnel_assignees__user__id', method=assignee_filter)
    priority = django_filters.CharFilter(field_name='priority', lookup_expr='icontains')
    endGte = django_filters.DateFilter(field_name='end', method=datetime_range_gte_filter)
    endLte = django_filters.DateFilter(field_name='end', method=datetime_range_lt_filter)
    lastName = django_filters.CharFilter(field_name='last_name', lookup_expr='icontains')
    firstName = django_filters.CharFilter(field_name='first_name', lookup_expr='icontains')
    lastNameInitial = django_filters.CharFilter(field_name='last_name_initial', lookup_expr='icontains')
    firstNameInitial = django_filters.CharFilter(field_name='first_name_initial', lookup_expr='icontains')
    operatePeriodGte = django_filters.DateFilter(field_name='operate_period', method=datetime_range_gte_filter)
    operatePeriodLte = django_filters.DateFilter(field_name='operate_period', method=datetime_range_lt_filter)
    affiliation = django_filters.CharFilter(field_name='affiliation', lookup_expr='icontains')
    skill = django_filters.CharFilter(field_name='skill_assignment__skill__internal_value', method=skill_filter)
    cardListId = django_filters.UUIDFilter(field_name='card_list')
    ageLte = django_filters.NumberFilter(field_name='age', lookup_expr='lte')
    ageGte = django_filters.NumberFilter(field_name='age', lookup_expr='gte')
    gender = django_filters.CharFilter(field_name='gender', method=gender_filter)
    trainStation = django_filters.CharFilter(field_name='train_station', lookup_expr='icontains')
    priceLte = django_filters.NumberFilter(field_name='price', lookup_expr='lte')
    priceGte = django_filters.NumberFilter(field_name='price', lookup_expr='gte')
    parallel = django_filters.CharFilter(field_name='parallel', method=parallel_filter)
    request = django_filters.CharFilter(field_name='request', lookup_expr='icontains')
    image = django_filters.BooleanFilter(field_name='image_name', method=image_filter)
    skillSheet = django_filters.BooleanFilter(field_name='skill_sheets__name', method=skill_sheet_filter)
    description = django_filters.CharFilter(field_name='personnel_contracts__detail', lookup_expr='icontains')
    contractStart = django_filters.CharFilter(field_name='personnel_contracts__start')
    contractEnd = django_filters.CharFilter(field_name='personnel_contracts__end')
    contractPriceLte = django_filters.NumberFilter(field_name='personnel_contracts__price', lookup_expr='lte')
    contractPriceGte = django_filters.NumberFilter(field_name='personnel_contracts__price', lookup_expr='gte')
    organizationHigher = django_filters.CharFilter(field_name='personnel_contracts__higher_organization__name', lookup_expr='icontains')
    organizationLower = django_filters.CharFilter(field_name='personnel_contracts__lower_organization__name', lookup_expr='icontains')
    contactHigherId = django_filters.UUIDFilter(field_name='personnel_contracts__higher_contact__id')
    contactLowerId = django_filters.UUIDFilter(field_name='personnel_contracts__lower_contact__id')
    isArchived = django_filters.BooleanFilter(field_name='is_archived')
