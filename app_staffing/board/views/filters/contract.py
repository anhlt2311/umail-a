import django_filters.rest_framework as django_filters


class PersonnelContractFilter(django_filters.FilterSet):
    cardId = django_filters.UUIDFilter(field_name='personnel', lookup_expr='exact')


class ProjectContractFilter(django_filters.FilterSet):
    cardId = django_filters.UUIDFilter(field_name='project', lookup_expr='exact')
