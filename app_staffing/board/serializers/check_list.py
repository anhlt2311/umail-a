from rest_framework import serializers

from app_staffing.board.models import PersonnelCheckList, ProjectCheckList, PersonnelCheckListItem, ProjectCheckListItem
from app_staffing.board.serializers.base import BaseCreateWithPersonnelSerializer, BaseCreateWithProjectSerializer
from app_staffing.serializers.base import MultiTenantFieldsMixin, AppResourceSerializer
from app_staffing.utils.custom_validation import custom_validation


class PersonnelChecklistItemSummarySerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    class Meta:
        model = PersonnelCheckListItem
        fields = ('id', 'content', 'is_finished')


class ProjectChecklistItemSummarySerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    class Meta:
        model = ProjectCheckListItem
        fields = ('id', 'content', 'is_finished')


class ProjectCheckListSerializer(BaseCreateWithProjectSerializer):
    class Meta:
        model = ProjectCheckList
        fields = ('id', 'company', 'title', 'project')

        extra_kwargs = {
            'company': {'write_only': True},
            'title': {'write_only': True},
            'project': {'write_only': True},
        }


class PersonnelCheckListSerializer(BaseCreateWithPersonnelSerializer):
    def custom_validation(self, data):
        custom_validation(data, 'check_list')

    class Meta:
        model = PersonnelCheckList
        fields = ('id', 'company', 'title', 'personnel')

        extra_kwargs = {
            'company': {'write_only': True},
            'title': {'write_only': True},
            'personnel': {'write_only': True},
        }


class BaseCheckListSummarySerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    is_finished = serializers.SerializerMethodField()

    def get_is_finished(self, obj):
        return obj.items.filter(is_finished=False).exists() is False


class PersonnelCheckListSummarySerializer(BaseCheckListSummarySerializer):
    items = PersonnelChecklistItemSummarySerializer(many=True, read_only=True)

    class Meta:
        model = PersonnelCheckList
        fields = ('id', 'title', 'show_finished', 'items', 'is_finished')


class ProjectCheckListSummarySerializer(BaseCheckListSummarySerializer):
    items = ProjectChecklistItemSummarySerializer(many=True, read_only=True)

    class Meta:
        model = ProjectCheckList
        fields = ('id', 'title', 'show_finished', 'items', 'is_finished')


class PersonnelCheckListItemSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    class Meta:
        model = PersonnelCheckListItem
        fields = ('id', 'content', 'is_finished', 'company')
        extra_kwargs = {
            'company': {'write_only': True},
        }


class ProjectCheckListItemSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    class Meta:
        model = ProjectCheckListItem
        fields = ('id', 'content', 'is_finished', 'company')
        extra_kwargs = {
            'company': {'write_only': True},
        }
