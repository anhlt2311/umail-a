from django.conf import settings
from django.db import transaction
from app_staffing.board.exceptions.card import OverloadDynamicRow
from rest_framework import serializers
from rest_framework.exceptions import NotFound

from app_staffing.board.models import Project, ProjectCardList, ProjectScheduledEmailTemplate
from app_staffing.board.serializers.assignee import ProjectAssigneeSummarySerializer
from app_staffing.board.serializers.base import BaseOrderUpdateSerializer, BaseBoardCardListCreateSerializer
from app_staffing.board.serializers.check_list import ProjectCheckListSummarySerializer
from app_staffing.board.serializers.comment import PersonnelCommentSummarySerializer, ProjectCommentSummarySerializer
from app_staffing.board.services import ProjectBoardService
from app_staffing.serializers.base import BaseCardSerializer, MultiTenantFieldsMixin, AppResourceSerializer
from app_staffing.utils.custom_validation import custom_validation
from app_staffing.board.constants import PRIORITY_MAP


class ProjectBoardCardSummarySerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    assignees = ProjectAssigneeSummarySerializer(many=True, read_only=True)
    checklist = ProjectCheckListSummarySerializer(many=True, read_only=True)
    comments = ProjectCommentSummarySerializer(many=True, read_only=True)
    place = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()
    settle_method = serializers.SerializerMethodField()
    settle_increment = serializers.SerializerMethodField()
    distribution = serializers.SerializerMethodField()

    def get_place(self, obj):
        if obj.place:
            return obj.place.name
        return None

    def get_price(self, obj):
        return obj.price_amount

    def get_settle_method(self, obj):
        return obj.method

    def get_settle_increment(self, obj):
        return obj.increment

    def get_distribution(self, obj):
        return obj.distribution_to

    def to_representation(self, data):
        data = super().to_representation(data)
        priority = data.get('priority')
        if priority:
            data['priority'] = dict(title=PRIORITY_MAP.get(priority), value=priority)
        return data

    class Meta:
        model = Project
        fields = ('id', 'order', 'assignees', 'detail', 'is_immediate', 'place',
                  'working_hours', 'people_amount', 'price', 'settle_width', 'settle_method', 'settle_increment',
                  'payment_days',
                  'interview_amount', 'distribution', 'project_attachment',
                  'checklist', 'comments', 'is_archived', 'priority')


class ProjectBoardCreateSerializer(BaseBoardCardListCreateSerializer):
    def custom_validation(self, data):
        custom_validation(data, 'project')

    def create(self, validated_data):
        new_instance = super().create(validated_data)
        ProjectScheduledEmailTemplate.objects.create(project=new_instance)
        return new_instance

    class Meta:
        model = Project
        fields = ('id', 'detail', 'card_list', 'company')
        extra_kwargs = {
            'company': {'write_only': True},
            'card_list': {'write_only': True},
            'detail': {'write_only': True},
        }


class ProjectDetailSerializer(BaseCardSerializer):
    assignees = serializers.SerializerMethodField()
    dynamic_rows = serializers.SerializerMethodField()
    skills = serializers.SerializerMethodField()
    place = serializers.SerializerMethodField()
    comments = PersonnelCommentSummarySerializer(many=True, read_only=True)

    def get_skills(self, obj):
        return [skill.skill_id for skill in obj.skill_assignment.all() if skill]

    def get_dynamic_rows(self, obj):
        return [dict(id=dynamic_row.id, title=dynamic_row.title, content=dynamic_row.content) for dynamic_row \
                in obj.dynamic_row if dynamic_row]

    def get_assignees(self, obj):
        return [assignee.user_id for assignee in obj.assignees if assignee]

    def get_place(self, obj):
        return obj.train_station

    class Meta:
        model = Project
        fields = (
            'id', 'order', 'list_id', 'assignees', 'priority', 'period', 'detail', 'project_period', 'place',
            'working_hours', 'people', 'description', 'skills', 'price', 'settle_width', 'settle_method',
            'settle_increment', 'payment', 'interview', 'distribution', 'dynamic_rows', 'project_attachment', 'comments',
            'created_time', 'created_user', 'created_user_name', 'modified_time', 'modified_user', 'modified_user_name',
            'scheduled_email_template',
        )


class ProjectUpdateDestroySerializer(BaseCardSerializer):
    assignees = serializers.SerializerMethodField()
    dynamic_rows = serializers.SerializerMethodField()
    skills = serializers.SerializerMethodField()
    comments = PersonnelCommentSummarySerializer(many=True, read_only=True)
    place = serializers.SerializerMethodField()

    def get_place(self, obj):
        return obj.train_station

    def get_skills(self, obj):
        return [skill.skill_id for skill in obj.skill_assignment.all() if skill]

    def get_dynamic_rows(self, obj):
        return [dict(id=dynamic_row.id, title=dynamic_row.title, content=dynamic_row.content) for dynamic_row \
                in obj.dynamic_row if dynamic_row]

    def get_assignees(self, obj):
        return [assignee.user_id for assignee in obj.assignees if assignee]

    def custom_validation(self, data):
        custom_validation(data, 'project')

    def to_internal_value(self, data):

        project_period = data.pop('project_period', None)
        if project_period:
            if project_period.get('datetime'):
                data.update(project_period_datetime=project_period.get('datetime'))
            if project_period.get('immediate'):
                data.update(project_period_immediate=project_period.get('immediate'))
            if project_period.get('long_term'):
                data.update(project_period_long_term=project_period.get('long_term'))
            if project_period.get('remarks'):
                data.update(project_period_remarks=project_period.get('remarks'))
        place = data.pop('place', None)

        if place:
            if place.get('id'):
                data.update(train_station_id=place.get('id'))
            if place.get('remote', ):
                data.update(train_station_remote=place.get('remote'))
            if place.get('remarks'):
                data.update(train_station_remarks=place.get('remarks'))

        working_hours = data.pop('working_hours', None)
        if working_hours:
            if working_hours.get('start'):
                data.update(working_hours_start=working_hours.get('start'))
            if working_hours.get('end'):
                data.update(working_hours_end=working_hours.get('end'))
            if working_hours.get('remarks'):
                data.update(working_hours_remarks=working_hours.get('remarks'))

        people = data.pop('people', None)

        if people:
            if people.get('amount'):
                data.update(people_amount=people.get('amount'))
            if people.get('is_multiple'):
                data.update(people_is_multiple=people.get('is_multiple'))
            if people.get('is_set_proposal_welcome'):
                data.update(people_is_set_proposal_welcome=people.get('is_set_proposal_welcome'))
            if people.get('remarks'):
                data.update(people_remarks=people.get('remarks'))

        price = data.pop('price', None)
        if price:
            if price.get('amount'):
                data.update(price_amount=price.get('amount'))
            if price.get('is_skill_assessment'):
                data.update(price_is_skill_assessment=price.get('is_skill_assessment'))
            if price.get('remarks'):
                data.update(price_remarks=price.get('remarks'))

        settle_width = data.pop('settle_width', None)

        if settle_width:
            if settle_width.get('start'):
                data.update(settle_width_start=settle_width.get('start'))
            if settle_width.get('end'):
                data.update(settle_width_end=settle_width.get('end'))
            if settle_width.get('is_fixed'):
                data.update(settle_width_is_fixed=settle_width.get('is_fixed'))
            if settle_width.get('remarks'):
                data.update(settle_width_remarks=settle_width.get('remarks'))

        settle_method = data.pop('settle_method', None)

        if settle_method:
            if settle_method.get('method'):
                data.update(method=settle_method.get('method'))
            if settle_method.get('remarks'):
                data.update(settle_method_remarks=settle_method.get('remarks'))

        settle_increment = data.pop('settle_increment', None)

        if settle_increment:
            if settle_increment.get('increment'):
                data.update(increment=settle_increment.get('increment'))
            if settle_increment.get('remarks'):
                data.update(settle_increment_remarks=settle_increment.get('remarks'))

        payment = data.pop('payment', None)

        if payment:
            if payment.get('day'):
                data.update(payment_days=payment.get('day'))
            if payment.get('remarks'):
                data.update(payment_remarks=payment.get('remarks'))

        interview = data.pop('interview', None)

        if interview:
            if interview.get('amount'):
                data.update(interview_amount=interview.get('amount'))
            if interview.get('is_join'):
                data.update(interview_is_join=interview.get('is_join'))
            if interview.get('remarks'):
                data.update(interview_remarks=interview.get('remarks'))

        distribution = data.pop('distribution', None)

        if distribution:
            if distribution.get('to'):
                data.update(distribution_to=distribution.get('to'))
            if distribution.get('remarks'):
                data.update(distribution_remarks=distribution.get('remarks'))
        return super().to_internal_value(data)

    def update(self, instance, validated_data):
        with transaction.atomic():
            # Handle assignees
            assignees = self.initial_data.get("assignees", None)
            if 'assignees' in self.initial_data.keys():
                instance = ProjectBoardService.update_assignees(instance, assignees)

            # Handle skills
            skills = self.initial_data.get("skills", None)
            if 'skills' in self.initial_data.keys():
                ProjectBoardService.update_skills(instance, skills)

            # Update dynamic_rows
            dynamic_rows = self.initial_data.get("dynamic_rows", None)
            if 'dynamic_rows' in self.initial_data.keys():
                if len(dynamic_rows) > settings.MAX_DYNAMIC_ROW:
                    raise OverloadDynamicRow()
                instance = ProjectBoardService.update_dynamic_row(instance, dynamic_rows)

            # Update place
            train_station_id = self.initial_data.get("train_station_id", None)
            if train_station_id:
                validated_data['place_id'] = train_station_id

            current_card_list_id = instance.card_list_id
            if 'card_list' in validated_data.keys() and validated_data.get('card_list').id == current_card_list_id:
                validated_data.pop('card_list')

            update_instance = super().update(instance, validated_data)

            if 'card_list' in validated_data.keys() and validated_data.get('card_list').id != current_card_list_id:
                update_instance.bottom()

            return super().update(instance, validated_data)

    class Meta:
        model = Project
        # TODO: add fields 'scheduled_email_template'
        fields = (
            'id', 'order', 'list_id', 'assignees', 'priority', 'period', 'detail', 'project_period', 'place',
            'working_hours', 'people', 'description', 'skills', 'price', 'settle_width', 'settle_method', 'is_archived',
            'settle_increment', 'payment', 'interview', 'distribution', 'dynamic_rows', 'project_attachment', 'comments',
            'start', 'end', 'is_finished', 'distribution_to', 'distribution_remarks', 'interview_remarks',
            'project_period_datetime', 'project_period_immediate', 'project_period_long_term', 'project_period_remarks',
            'train_station_remote', 'train_station_remarks', 'working_hours_start', 'working_hours_end',
            'people_remarks', 'working_hours_remarks', 'people_amount', 'people_is_multiple',
            'people_is_set_proposal_welcome', 'price_amount', 'price_is_skill_assessment', 'price_remarks',
            'settle_width_start', 'settle_width_end', 'settle_width_is_fixed', 'settle_width_remarks',
            'increment', 'settle_increment_remarks', 'method', 'settle_method_remarks', 'payment_days',
            'payment_remarks', 'interview_amount', 'interview_is_join', 'card_list'

        )

        extra_kwargs = {
            'order': {'read_only': True},
            'id': {'read_only': True},
            'start': {'write_only': True},
            'card_list': {'write_only': True},
            'list_id': {'read_only': True},
            'end': {'write_only': True},
            'is_finished': {'write_only': True},
            'is_archived': {'write_only': True},
            'project_period_datetime': {'write_only': True},
            'project_period_immediate': {'write_only': True},
            'project_period_long_term': {'write_only': True},
            'project_period_remarks': {'write_only': True},
            'train_station_remote': {'write_only': True},
            'train_station_remarks': {'write_only': True},
            'working_hours_start': {'write_only': True},
            'working_hours_end': {'write_only': True},
            'working_hours_remarks': {'write_only': True},
            'people_amount': {'write_only': True},
            'people_is_multiple': {'write_only': True},
            'people_is_set_proposal_welcome': {'write_only': True},
            'people_remarks': {'write_only': True},
            'price_amount': {'write_only': True},
            'price_is_skill_assessment': {'write_only': True},
            'price_remarks': {'write_only': True},
            'settle_width_start': {'write_only': True},
            'settle_width_end': {'write_only': True},
            'settle_width_is_fixed': {'write_only': True},
            'settle_width_remarks': {'write_only': True},
            'method': {'write_only': True},
            'settle_method_remarks': {'write_only': True},
            'increment': {'write_only': True},
            'settle_increment_remarks': {'write_only': True},
            'payment_days': {'write_only': True},
            'payment_remarks': {'write_only': True},
            'distribution_to': {'write_only': True},
            'distribution_remarks': {'write_only': True},
            'interview_amount': {'write_only': True},
            'interview_is_join': {'write_only': True},
            'interview_remarks': {'write_only': True},
        }


class ProjectOrderUpdateSerializer(BaseOrderUpdateSerializer):
    model = Project

    class Meta:
        model = Project
        fields = ('list_id',)
        extra_kwargs = {'list_id': {'write_only': True}}


class ProjectCardListSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    class Meta:
        model = ProjectCardList
        fields = ("id", "title")


class ProjectCopyCardSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    model = None
    action_service = None
    card_id = serializers.SerializerMethodField()

    def to_internal_value(self, data):
        action_id = data.get('card_id', None)
        try:
            action = self.model.objects.get(pk=action_id)
            instance_copy = action.__dict__
            instance_copy.pop('id')
            instance_copy.pop('_state')
            instance_copy.pop('order')
            instance_copy['card_list'] = instance_copy.pop('card_list_id')
            data.update(instance_copy)
        except self.model.DoesNotExist:
            raise NotFound()
        return super().to_internal_value(data)

    def get_card_id(self, obj):
        return None

    def create(self, validated_data):
        new_instance = super().create(validated_data)
        copy_action = self.model.objects.get(pk=self.initial_data.get('card_id'))
        # clone related objects
        self.action_service.clone_related_objects(copy_action, new_instance)
        return


class ActionProjectCardListDetailSerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    list_id = serializers.CharField(allow_blank=True)

    class Meta:
        model = ProjectCardList
        fields = ('list_id',)
