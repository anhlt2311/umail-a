from app_staffing.board.models import PersonnelSkillSheet
from app_staffing.serializers.base import MultiTenantFieldsMixin, AppResourceSerializer
from app_staffing.services.gcs import gcs_service


class SkillSheetSummarySerializer(MultiTenantFieldsMixin, AppResourceSerializer):
    def to_representation(self, instance):
        data = super().to_representation(instance)
        if gcs_service:
            data['file'] = gcs_service.get_download_url(instance.file.name)
        return data

    class Meta:
        model = PersonnelSkillSheet
        fields = ('id', 'name', 'file')
