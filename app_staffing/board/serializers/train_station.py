from app_staffing.board.models import TrainStation
from rest_framework import serializers
class TrainStationSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrainStation
        fields = ('id', 'name', 'location_key', 'area')
