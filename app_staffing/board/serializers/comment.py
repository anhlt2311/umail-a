from rest_framework.fields import SerializerMethodField

from app_staffing.board.models import PersonnelComment, ProjectComment
from app_staffing.serializers.base import BaseSubCommentSerializer, AppResourceSerializer, MultiTenantFieldsMixin, \
    BaseCommentSerializer
from app_staffing.utils.custom_validation import custom_validation


class PersonnelCommentSerializer(MultiTenantFieldsMixin, BaseCommentSerializer):

    def custom_validation(self, data):
        custom_validation(data, 'comment')

    class Meta:
        model = PersonnelComment
        fields = ('id', 'created_user', 'created_user__name', 'created_time', 'content', 'company', 'is_important',
                  'has_subcomment', 'deleted_at', 'total_sub_comment', 'parent', 'parent_content',
                  'parent_created_user_name', 'parent_modified_user_name', 'parent_created_time',
                  'parent_modified_time', 'parent_id')
        extra_kwargs = {'company': {'write_only': True}, 'total_sub_comment': {'read_only': True},
                        'parent': {'read_only': True}}


class PersonnelSubCommentSerializer(BaseSubCommentSerializer):
    class Meta:
        model = PersonnelComment
        fields = ('id', 'created_user', 'created_user__name', 'created_time', 'content', 'company', 'is_important',
                  'parent', 'parent_id')
        extra_kwargs = {'company': {'write_only': True}, 'is_important': {'read_only': True}}


class PersonnelCommentSummarySerializer(AppResourceSerializer):
    parent_id = SerializerMethodField()

    created_user = SerializerMethodField()

    modified_user = SerializerMethodField()

    def get_parent_id(self, obj):
        return obj.parent_id

    def get_created_user(self, obj):
        if obj.created_user:
            return obj.created_user.display_name
        return None

    def get_modified_user(self, obj):
        if obj.modified_user:
            return obj.modified_user.display_name
        return None

    class Meta:
        model = PersonnelComment
        fields = (
            'id',
            'parent_id',
            'content',
            'created_time',
            'created_user',
            'modified_time',
            'modified_user',)


class ProjectCommentSerializer(MultiTenantFieldsMixin, BaseCommentSerializer):

    def custom_validation(self, data):
        custom_validation(data, 'comment')

    class Meta:
        model = ProjectComment
        fields = ('id', 'created_user', 'created_user__name', 'created_time', 'content', 'company', 'is_important',
                  'has_subcomment', 'deleted_at', 'total_sub_comment', 'parent', 'parent_content', \
                  'parent_created_user_name', 'parent_modified_user_name', 'parent_created_time',
                  'parent_modified_time', 'parent_id')
        extra_kwargs = {'company': {'write_only': True}, 'total_sub_comment': {'read_only': True},
                        'parent': {'read_only': True}}


class ProjectSubCommentSerializer(BaseSubCommentSerializer):
    class Meta:
        model = ProjectComment
        fields = ('id', 'created_user', 'created_user__name', 'created_time', 'content', 'company', 'is_important',
                  'parent', 'parent_id')
        extra_kwargs = {'company': {'write_only': True}, 'is_important': {'read_only': True}}


class ProjectCommentSummarySerializer(AppResourceSerializer):
    class Meta:
        model = ProjectComment
        fields = (
            'id',
            'parent',
            'content',
            'created_time',
            'created_user',
            'modified_time',
            'modified_user',
            'parent_id',)
