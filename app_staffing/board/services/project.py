from app_staffing.board.constants import CARD_LIST_TITLES
from app_staffing.board.models import ProjectCheckList, ProjectCheckListItem, ProjectAssignee, ProjectCardList, ProjectSkillAssignment, \
    ProjectDynamicRow, ProjectComment
from app_staffing.board.services.base import BaseCardService


class ProjectBoardService(BaseCardService):
    model_card_list = ProjectCardList
    model_assignee = ProjectAssignee
    model_dynamic_row = ProjectDynamicRow
    model_card_tag = ProjectSkillAssignment
    model_comment = ProjectComment
    model_checklist = ProjectCheckList
    model_checklist_item = ProjectCheckListItem
    model_name = 'project'
