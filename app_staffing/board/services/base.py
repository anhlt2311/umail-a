from app_staffing.board.constants import CARD_LIST_TITLES
from app_staffing.models import User, Tag


class BaseCardService:
    model_card_list = None
    model_assignee = None
    model_dynamic_row = None
    model_card_tag = None
    model_comment = None
    model_checklist = None
    model_checklist_item = None
    model_name = None

    @classmethod
    def create_card_list(cls, company_id):
        new_cards_list = []
        old_titles = cls.model_card_list.objects.filter(company_id=company_id).exists()
        if not old_titles:
            for _, title in CARD_LIST_TITLES:
                card_list = cls.model_card_list(title=title, company_id=company_id)
                new_cards_list.append(card_list)

            return cls.model_card_list.objects.bulk_create(new_cards_list)

    @classmethod
    def update_assignees(cls, instance, assignees_input):
        cls.model_assignee.objects.filter(card_id=instance.id).delete()
        if assignees_input is not None:
            list_assignees = []
            users = User.objects.filter(id__in=assignees_input, company=instance.company).values_list('id', flat=True)
            for user in users:
                assignee = cls.model_assignee(card_id=instance.id, company_id=instance.company_id, user_id=user)
                list_assignees.append(assignee)
            if list_assignees:
                cls.model_assignee.objects.bulk_create(list_assignees)
                instance.assignees = list_assignees
            else:
                instance.assignees = []
        return instance

    @classmethod
    def update_checklist(cls, instance, checklist_input):
        checklists = cls.model_checklist.objects.filter(**{f'{cls.model_name}_id': instance.id})
        checklists_ids = checklists.values_list('id', flat=True)
        checklists.delete()
        for checklist in checklist_input:
            title = checklist.get("title")
            show_finished = checklist.get("show_finished")
            _prep_checklist = dict(title=title, show_finished=show_finished, company_id=instance.company.id,)
            _prep_checklist[f'{cls.model_name}_id'] = instance.id
            new_checklist = cls.model_checklist.objects.create(**_prep_checklist)
            items = checklist.get("items")
            if items:
                cls.model_checklist_item.objects.filter(check_list_id__in=checklists_ids).delete()
                for item in items:
                    content = item.get("content")
                    is_finished = item.get("is_finished")
                    _prep_item = dict(content=content, is_finished=is_finished, check_list_id=new_checklist.id,
                                      company_id=instance.company.id)
                    cls.model_checklist_item.objects.create(**_prep_item)

    @classmethod
    def update_skills(cls, instance, skill_inputs):
        if skill_inputs is not None:
            cls.model_card_tag.objects.filter(**{f'{cls.model_name}_id':instance.id}).delete()
            list_skills = []
            skills = Tag.objects.filter(id__in=skill_inputs, is_skill=True, company=instance.company).values_list(
                'id',
                flat=True)
            for skill_id in skills:
                skill = cls.model_card_tag(**{
                    f'{cls.model_name}_id': instance.id,
                    'company_id': instance.company_id,
                    'skill_id': skill_id,
                })
                list_skills.append(skill)
            if list_skills:
                cls.model_card_tag.objects.bulk_create(list_skills)
        return

    @classmethod
    def clone_related_objects(cls, copy_instance, new_instance):
        # clone Assignee
        assignees = cls.model_assignee.objects.filter(card_id=copy_instance.id)
        list_assignees = []
        for assignee in assignees:
            dict_assignee = assignee.__dict__
            dict_assignee.pop('id')
            dict_assignee.pop('_state')
            dict_assignee.pop('_SoftDeleteObject__dirty')
            dict_assignee['card_id'] = new_instance.id
            list_assignees.append(cls.model_assignee(**dict_assignee))
        if list_assignees:
            cls.model_assignee.objects.bulk_create(list_assignees)

        # clone tags
        tags = cls.model_card_tag.objects.filter(**{f'{cls.model_name}_id':copy_instance.id})
        list_tags = []
        for tag in tags:
            dict_tag = tag.__dict__
            dict_tag.pop('id')
            dict_tag.pop('_state')
            dict_tag.pop('_SoftDeleteObject__dirty')
            dict_tag[f'{cls.model_name}_id'] = new_instance.id
            list_tags.append(cls.model_card_tag(**dict_tag))
        if list_tags:
            cls.model_card_tag.objects.bulk_create(list_tags)

        # clone comments
        comments = cls.model_comment.objects.filter(**{f'{cls.model_name}_id':copy_instance.id})
        list_comments = []
        for comment in comments:
            dict_comment = comment.__dict__
            dict_comment.pop('id')
            dict_comment.pop('_state')
            dict_comment.pop('_SoftDeleteObject__dirty')
            dict_comment[f'{cls.model_name}_id'] = new_instance.id
            list_comments.append(cls.model_comment(**dict_comment))
        if list_comments:
            cls.model_comment.objects.bulk_create(list_comments)

        # clone checklists
        checklists = cls.model_checklist.objects.prefetch_related('items').filter(
            **{f'{cls.model_name}_id': copy_instance.id})
        list_checklists = []
        list_checklist_items = []
        for checklist in checklists:
            dict_checklist = checklist.__dict__.copy()
            dict_checklist.pop('id')
            dict_checklist.pop('_state')
            dict_checklist.pop('_SoftDeleteObject__dirty')
            dict_checklist.pop('_prefetched_objects_cache')
            dict_checklist[f'{cls.model_name}_id'] = new_instance.id
            new_checklist = cls.model_checklist(**dict_checklist)
            for checklist_item in checklist.items.all():
                dict_checklist_item = checklist_item.__dict__
                dict_checklist_item.pop('id')
                dict_checklist_item.pop('_state')
                dict_checklist_item.pop('_SoftDeleteObject__dirty')
                dict_checklist_item['check_list_id'] = new_checklist.id
                list_checklist_items.append(cls.model_checklist_item(**dict_checklist_item))
            list_checklists.append(new_checklist)
        if list_checklists:
            cls.model_checklist.objects.bulk_create(list_checklists)
        if list_checklist_items:
            cls.model_checklist_item.objects.bulk_create(list_checklist_items)

        # clone dynamic_row
        dynamic_rows = cls.model_dynamic_row.objects.filter(**{f'{cls.model_name}_id':copy_instance.id})
        list_dynamic_rows = []
        for dynamic_row in dynamic_rows:
            dict_dynamic_row = dynamic_row.__dict__
            dict_dynamic_row.pop('id')
            dict_dynamic_row.pop('_state')
            dict_dynamic_row.pop('_SoftDeleteObject__dirty')
            dict_dynamic_row[f'{cls.model_name}_id'] = new_instance.id
            list_dynamic_rows.append(cls.model_dynamic_row(**dict_dynamic_row))
        if list_dynamic_rows:
            cls.model_dynamic_row.objects.bulk_create(list_dynamic_rows)

        return

    @classmethod
    def update_dynamic_row(cls, instance, dynamic_rows):
        if dynamic_rows:
            cls.model_dynamic_row.objects.filter(**{f'{cls.model_name}':instance}).delete()
            dynamic_row_list = [cls.model_dynamic_row(**{
                f'{cls.model_name}_id': instance.id,
                'title': item.get("title"),
                'content': item.get("content"),
                'company_id': instance.company_id,
                }
            ) for item in dynamic_rows]
            cls.model_dynamic_row.objects.bulk_create(dynamic_row_list)
            instance.dynamic_rows = dynamic_row_list
        else:
            instance.dynamic_rows = []
        return instance
