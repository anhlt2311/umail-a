from django.urls import path

from app_staffing.board.views import ProjectCardListAPIView, GetProjectCardListAPIView, \
    ProjectBoardCardListCreateAPIView, ProjectSubCommentListView, ProjectDetailUpdateDestroyAPIView, \
    ProjectCommentDetailView, ProjectCommentListView, ProjectCheckListItemActionAPIView, \
    ProjectCheckListItemCreateAPIView, CopyCardProjectAPIView, ProjectOrderUpdateAPIView, \
    ProjectCheckListActionAPIView, ProjectCheckListCreateAPIView, ProjectContractActionAPIView, \
    ProjectContractCreateAPIView, ProjectUpdateTemplateAPIView, ActionProjectCardListDetailAPIView

urlpatterns = [
    path('lists', ProjectCardListAPIView.as_view(), name='project_card_list__list_create_view'),
    path('lists/<uuid:pk>', GetProjectCardListAPIView.as_view(), name='project_card_list_get_view'),
    path('lists/<uuid:pk>/<str:action>', ActionProjectCardListDetailAPIView.as_view(),
         name='project_card_list_detail__action_view'),
    path('cards', ProjectBoardCardListCreateAPIView.as_view(), name='project_list_create_view'),
    path('cards/copy', CopyCardProjectAPIView.as_view(), name='project_copy_card_view'),
    path('cards/<uuid:pk>', ProjectDetailUpdateDestroyAPIView.as_view(), name='project_action_view'),
    path('cards/<uuid:card_id>/templates/<int:pk>', ProjectUpdateTemplateAPIView.as_view(),
         name='project_update_template_view'),
    path('cards/<uuid:pk>/position', ProjectOrderUpdateAPIView.as_view(), name='project_order_update_view'),
    path('checklists', ProjectCheckListCreateAPIView.as_view(), name='project_check_list_view'),
    path('checklists/<uuid:pk>', ProjectCheckListActionAPIView.as_view(), name='project_check_list_action_view'),
    path('checklists/<uuid:pk>/items', ProjectCheckListItemCreateAPIView.as_view(),
         name='project_check_list_item_view'),
    path('checklists/<uuid:parent>/items/<uuid:pk>', ProjectCheckListItemActionAPIView.as_view(),
         name='project_check_list_item_action_view'),
    path('cards/<uuid:parent_pk>/comments', ProjectCommentListView.as_view(),
         name='project_comments'),
    path('cards/<uuid:parent_pk>/comments/<uuid:pk>', ProjectCommentDetailView.as_view(),
         name='project_comment'),
    path('cards/<uuid:parent_pk>/comments/<uuid:pk>/sub-comments', ProjectSubCommentListView.as_view(),
         name='project_sub_comment'),
    path('contracts', ProjectContractCreateAPIView.as_view(), name='project_contract_view'),
    path('contracts/<uuid:pk>', ProjectContractActionAPIView.as_view(), name='project_contract_action_view'),
]
