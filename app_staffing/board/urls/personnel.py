from django.urls import path

from app_staffing.board.views import PersonnelCardListAPIView, GetPersonnelCardListAPIView, \
    PersonnelBoardCardListCreateAPIView, \
    PersonnelDetailUpdateDestroyAPIView, PersonnelOrderUpdateAPIView, PersonnelContractCreateAPIView, \
    PersonnelContractActionAPIView, \
    PersonnelCheckListCreateAPIView, PersonnelCheckListActionAPIView, PersonnelCheckListItemCreateAPIView, \
    PersonnelCheckListItemActionAPIView, \
    PersonnelCommentListView, PersonnelCommentDetailView, PersonnelSubCommentListView, TrainstationListAPIView, \
    PersonnelSkillSheetAPIView, PersonnelSkillSheetDetailAPIView, CopyCardPersonnelAPIView, \
    ActionPersonnelCardListDetailAPIView, PersonnelUpdateTemplateAPIView

urlpatterns = [
    path('lists', PersonnelCardListAPIView.as_view(), name='personnel_card_list__list_create_view'),
    path('lists/<uuid:pk>', GetPersonnelCardListAPIView.as_view(), name='personnel_card_list__get_view'),
    path('lists/<uuid:pk>/<str:action>', ActionPersonnelCardListDetailAPIView.as_view(),
         name='personnel_card_list_detail__action_view'),
    path('cards', PersonnelBoardCardListCreateAPIView.as_view(), name='personnel_list_create_view'),
    path('cards/copy', CopyCardPersonnelAPIView.as_view(), name='personnel_copy_card_view'),
    path('cards/<uuid:pk>', PersonnelDetailUpdateDestroyAPIView.as_view(), name='personnel_action_view'),
    path('cards/<uuid:card_id>/templates/<int:pk>', PersonnelUpdateTemplateAPIView.as_view(),
         name='personnel_update_template_view'),
    path('cards/<uuid:pk>/position', PersonnelOrderUpdateAPIView.as_view(), name='personnel_order_update_view'),
    path('checklists', PersonnelCheckListCreateAPIView.as_view(), name='personnel_check_list_view'),
    path('checklists/<uuid:pk>', PersonnelCheckListActionAPIView.as_view(), name='personnel_check_list_action_view'),
    path('checklists/<uuid:pk>/items', PersonnelCheckListItemCreateAPIView.as_view(),
         name='personnel_check_list_item_view'),
    path('checklists/<uuid:parent>/items/<uuid:pk>', PersonnelCheckListItemActionAPIView.as_view(),
         name='personnel_check_list_item_action_view'),
    path('cards/<uuid:parent_pk>/comments', PersonnelCommentListView.as_view(),
         name='personnel_comments'),
    path('cards/<uuid:parent_pk>/comments/<uuid:pk>', PersonnelCommentDetailView.as_view(),
         name='personnel_comment'),
    path('cards/<uuid:parent_pk>/comments/<uuid:pk>/sub-comments', PersonnelSubCommentListView.as_view(),
         name='personnel_sub_comment'),
    path('contracts', PersonnelContractCreateAPIView.as_view(), name='personnel_contract_view'),
    path('contracts/<uuid:pk>', PersonnelContractActionAPIView.as_view(), name='personnel_contract_action_view'),
    path('train_stations', TrainstationListAPIView.as_view(), name='trainstation_card_list_get_view'),
    path('cards/<uuid:parent_pk>/skill-sheets', PersonnelSkillSheetAPIView.as_view(),
         name='personnel_skill_sheets'),
    path('cards/skill-sheets/<uuid:pk>', PersonnelSkillSheetDetailAPIView.as_view(),
         name='personnel_skill_sheet_detail')
]
