from rest_framework.exceptions import APIException


class PersonnelFileSizeException(APIException):
    status_code = 400
    default_code = 'personnel_file_size'


class PersonnelNumberOfFileSizeException(APIException):
    status_code = 400
    default_code = 'personnel_number_of_file'


class PersonnelCannotDeleteDueToContractException(APIException):
    status_code = 400
    default_code = 'can_not_delete_due_to_contract'
    default_detail = "アイテムの削除に失敗しました"


class ProjectSettleWidthStartGreaterThanSettleWidthEnd(APIException):
    status_code = 400
    default_detail = '上限には、下限以上の値を入力してください。'


class OverloadDynamicRow(APIException):
    status_code = 400
    default_detail = '登録可能な上限件数に達しています。'
