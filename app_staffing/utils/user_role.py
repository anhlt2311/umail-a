from app_staffing.models.user import MASTER, ADMIN

def is_admin_or_master(user):
    return user.user_role_name == ADMIN or user.user_role_name == MASTER
