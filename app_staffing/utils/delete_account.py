from django.core.paginator import Paginator
from django.conf import settings

def delete_with_chunk(queryset):
    paginator = Paginator(queryset.order_by("pk"), settings.DELETE_ACCOUNT_DATA_CHUNK_SIZE)
    page = 1
    while page < (paginator.num_pages + 1):
        # データが削除されて減っていくため、常に1ページ目を参照する必要がある
        pk_list = paginator.page(1).object_list.values_list('pk')
        queryset.filter(pk__in=pk_list).delete()
        page += 1
