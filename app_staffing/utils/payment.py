from django.conf import settings

from app_staffing.models import Plan, PurchaseHistory
from app_staffing.utils.expiration_time import get_payment_date

import pytz
import payjp

def validate_active_plans(active_plans):
    # プラン情報を取得する。見つからなかったり、複数見つかったりした場合はエラー
    if not active_plans or 0 == len(active_plans):
        raise RuntimeError('no active plan')
    elif 1 < len(active_plans):
        raise RuntimeError('multiple active plans exists')

    # プランは必ず一つなので先頭を取得し、プランに設定されている金額を取得する
    return active_plans[0]

def extract_addon_total_price_and_master_ids(addons):
    addon_total_price = 0
    addon_master_ids = []
    for addon in addons:
        addon_total_price += addon.addon_master.price
        addon_master_ids.append(str(addon.addon_master_id))

    return addon_total_price, addon_master_ids

def validate_company_attributes(company_attributes):
    # テナント設定情報を取得
    if not company_attributes or 0 == len(company_attributes):
        raise RuntimeError('no company attribute')
    elif 1 < len(company_attributes):
        raise RuntimeError('multiple company attributes exists')

    # テナント設定情報は必ず一つなので先頭を取得
    return company_attributes[0]

def extract_user_add_price_and_additional_options(company_attribute, current_active_plan):
    user_add_price = 0
    additional_options = ''
    user_registration_limit = company_attribute.user_registration_limit
    default_user_count = current_active_plan.plan_master.default_user_count
    if default_user_count < user_registration_limit:
        user_add_count = user_registration_limit - default_user_count
        user_add_price += (user_add_count * current_active_plan.plan_master.user_add_price)
        additional_options = settings.PURCHASE_HISTORY_USER_ADD_MESSAGE.format(user_add_count)

    return user_add_price, additional_options

def create_new_plan(company, plan_master_id, base_date):
    # 新しいプランレコード作成
    return Plan.objects.create(
        company_id=company.id,
        plan_master_id=plan_master_id,
        is_active=True,
        payment_date=get_payment_date(base_date),
    )

def get_card_last4(company):
    # card番号取得する
    payjp.api_key = settings.PAYJP_API_KEY
    customer = payjp.Customer.retrieve(str(company.id))
    if customer['cards']['data'] and 0 < len(customer['cards']['data']):
        return customer['cards']['data'][0]['last4']
    else:
        raise RuntimeError('credit card is not registered')

def create_purchase_history(company, current_active_plan, new_plan, addon_master_ids, additional_options, total_price):
    # PurchaseHistoryをCreateする
    PurchaseHistory.objects.create(
        company_id=company.id,
        period_start=current_active_plan.payment_date,
        period_end=new_plan.expiration_date,
        plan=new_plan,
        addon_ids=",".join(addon_master_ids),
        card_last4=get_card_last4(company),
        additional_options=additional_options,
        price=total_price,
        method=settings.PURCHASE_HISTORY_METHOD_CREDIT_CARD,
    )

# 決済処理を実行する
# 戻り値として 決済成功判定, メインカードのエラー, 予備カードのエラー を返す
# 成功時は true, None, None を返す
def exec_charge(total_price, company, description):
    is_success = True
    err_main = err_sub = None
    payjp.api_key = settings.PAYJP_API_KEY
    try:
        payjp.Charge.create(
            amount=total_price,
            customer=str(company.id),
            card=get_main_card_id(company),
            currency='jpy',
            description=description,
        )
    except Exception as e:
        err_main = e
        backup_card_id = get_backup_card_id(company)
        if backup_card_id is not None:
            try:
                # 予備カードでの決済処理
                payjp.Charge.create(
                    amount=total_price,
                    customer=str(company.id),
                    card=backup_card_id,
                    currency='jpy',
                    description=description,
                )
            except Exception as e2:
                # 予備も失敗したので決済失敗
                is_success = False
                err_sub = e2
        else:
            # メインが失敗して、予備の登録が無いので失敗
            is_success = False
    # 決済成功判定, メインカードのエラー, 予備カードのエラー
    return is_success, err_main, err_sub

# メインカードのIDを取得する
# 未登録の場合はNoneを返す
def get_main_card_id(company):
    payjp.api_key = settings.PAYJP_API_KEY
    try:
        customer = payjp.Customer.retrieve(str(company.id))
        return customer.get('default_card')
    except Exception:
        return None

# 予備扱いのカードのIDを取得する
# 未登録の場合はNoneを返す
def get_backup_card_id(company):
    payjp.api_key = settings.PAYJP_API_KEY
    try:
        customer = payjp.Customer.retrieve(str(company.id))
        cards = customer.cards.all()
        default_card_id = customer.get('default_card')
        if cards.count > 1:
            for card in cards.data:
                if card.id != default_card_id:
                    return card.id
        return None
    except Exception:
        return None

def get_taxed_price(price):
    return int(price*settings.TAX_RATE)
