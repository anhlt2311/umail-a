from schematics.types import BaseType
from schematics.exceptions import ValidationError


class NoneType(BaseType):
    def validate_none(self, value):
        if value is not None:
            raise ValidationError('value must be none, but given: {0}'.format(value))
