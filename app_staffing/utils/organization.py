def join_str(connector, item1, item2, item3):
    items = []
    if item1:
        items.append(item1)
    if item2:
        items.append(item2)
    if item3:
        items.append(item3)
    return connector.join(items)

def get_tel_full(obj):
    return join_str('-', obj.tel1, obj.tel2, obj.tel3)

def get_fax_full(obj):
    return join_str('-', obj.fax1, obj.fax2, obj.fax3)

def is_valid_corporate_number_format(corporate_number):
    return corporate_number.isdecimal() and len(corporate_number) == 13
