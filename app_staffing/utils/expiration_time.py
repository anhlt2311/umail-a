from dateutil.relativedelta import relativedelta
from datetime import timedelta

def get_payment_date(base_date):
    charge_day = base_date.day
    try:
        # 翌月の同じ日を決済日にする
        return (base_date + relativedelta(months=1)).replace(day=charge_day)
    except Exception:
        # 翌月に同じ日が存在しない場合(今の決済日が31日で、翌月が30日までしかない場合)、翌月の末日を決済日とする
        return (base_date.replace(day=1) + relativedelta(months=2) - timedelta(days=1))
