from django.contrib import admin
from app_staffing.admins.base import ModelAdminBase

from app_staffing.models.organization import (
    Contact,
    ContactCC,
    ContactScore,
    ContactComment,
    Company,
    Organization,
    OrganizationComment,
    Tag, 
    ContactTagAssignment, 
    ExceptionalOrganization,
    DisplaySetting,
    UserDisplaySetting,
    ContactCategory,
    OrganizationCategory,
    OrganizationCountry,
    OrganizationEmployeeNumber,
    OrganizationBranch, 
    CompanyAttribute,
)


class ContactAdmin(ModelAdminBase):
    search_fields = ("last_name", "first_name", "email", )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.list_display.append('contact_name')
        self.list_display.append('_tag')

    def _tag(self, row):
        return ','.join([t.value for t in row.tags.all()])
    _tag.short_description = "タグ"

    def contact_name(self, obj):
        if obj.first_name is not None:
            return obj.last_name + obj.first_name
        return obj.last_name
    contact_name.short_description = "連絡先名"


admin.site.register(Contact, ContactAdmin)


class ContactCCAdmin(ModelAdminBase):
    pass

admin.site.register(ContactCC, ContactCCAdmin)


class ContactScoreAdmin(ModelAdminBase):
    pass

admin.site.register(ContactScore, ContactScoreAdmin)


class ContactCommentAdmin(ModelAdminBase):
    pass


admin.site.register(ContactComment, ContactCommentAdmin)


class CompanyAdmin(ModelAdminBase):
    list_filter = ("has_invoice_system", "has_haken", "has_distribution", "employee_number", )
    search_fields = ("name", "domain_name", "address", "building", "capital_man_yen", )


admin.site.register(Company, CompanyAdmin)


class OrganizationAdmin(ModelAdminBase):
    list_filter = ("category", "organization_country", "is_blacklisted", "invoice_system", )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.list_display.append('organization_country_name')

    def organization_country_name(self, obj):
        message = "設定されていません"
        organization_country = obj.organization_country
        if not organization_country:
            return message
        name = organization_country.name
        if not name:
            return message
        if name == "JP":
            return "🇯🇵"
        elif name == "CN":
            return "🇨🇳"
        elif name == "KR":
            return "🇰🇷"
        elif name == "OTHER":
            return "🏳️"
        return name
    organization_country_name.short_description = "国籍"


admin.site.register(Organization, OrganizationAdmin)


class OrganizationCommentAdmin(ModelAdminBase):
    pass


admin.site.register(OrganizationComment, OrganizationCommentAdmin)


class TagAdmin(ModelAdminBase):
    list_filter = ("company", "color",)
    search_fields = ("value", )


admin.site.register(Tag, TagAdmin)


class ContactTagAssignmentAdmin(ModelAdminBase):
    pass

admin.site.register(ContactTagAssignment, ContactTagAssignmentAdmin)


class ExceptionalOrganizationAdmin(ModelAdminBase):
    pass

admin.site.register(ExceptionalOrganization, ExceptionalOrganizationAdmin)


class DisplaySettingAdmin(ModelAdminBase):
    pass

admin.site.register(DisplaySetting, DisplaySettingAdmin)


class UserDisplaySettingAdmin(ModelAdminBase):
    pass

admin.site.register(UserDisplaySetting, UserDisplaySettingAdmin)


class ContactCategoryAdmin(ModelAdminBase):
    pass


admin.site.register(ContactCategory, ContactCategoryAdmin)


class OrganizationCategoryAdmin(ModelAdminBase):
    pass


admin.site.register(OrganizationCategory, OrganizationCategoryAdmin)


class OrganizationCountryAdmin(ModelAdminBase):
    pass


admin.site.register(OrganizationCountry, OrganizationCountryAdmin)


class OrganizationEmployeeNumberAdmin(ModelAdminBase):
    pass


admin.site.register(OrganizationEmployeeNumber, OrganizationEmployeeNumberAdmin)


class OrganizationBranchAdmin(ModelAdminBase):
    pass

admin.site.register(OrganizationBranch, OrganizationBranchAdmin)


class CompanyAttributeAdmin(ModelAdminBase):
    pass

admin.site.register(CompanyAttribute, CompanyAttributeAdmin)

