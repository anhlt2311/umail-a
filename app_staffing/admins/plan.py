from django.contrib import admin
from app_staffing.admins.base import ModelAdminBase

from app_staffing.models.plan import (
    PlanMaster,
    Plan,
    PlanPaymentError,
    CardPaymentError,
)

class PlanMasterAdmin(ModelAdminBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.list_display.append('price_text')
        self.list_display.append('user_add_price_text')

    def price_text(self, obj):
        return f"¥{obj.price}"
    price_text.short_description = "金額"

    def user_add_price_text(self, obj):
        return f"¥{obj.user_add_price}"
    user_add_price_text.short_description = "ユーザー追加料金"


admin.site.register(PlanMaster, PlanMasterAdmin)


class PlanAdmin(ModelAdminBase):
    list_filter = ("plan_master", )
    search_fields = ("company__name", )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.list_display.append('plan_name')
        self.list_display.append('company_name')

    def plan_name(self, obj):
        return obj.plan_master.title
    plan_name.short_description = "プラン名"

    def company_name(self, obj):
        return obj.company.name
    company_name.short_description = "会社名"


admin.site.register(Plan, PlanAdmin)


class PlanPaymentErrorAdmin(ModelAdminBase):
    list_filter = ("plan", )
    search_fields = ("company__name", )


admin.site.register(PlanPaymentError, PlanPaymentErrorAdmin)

class CardPaymentErrorAdmin(ModelAdminBase):
    list_filter = ("plan", )
    search_fields = ("company__name", )


admin.site.register(CardPaymentError, CardPaymentErrorAdmin)
