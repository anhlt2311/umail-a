from django.contrib import admin
from app_staffing.admins.base import ModelAdminBase

from app_staffing.models.stats import (
    OrganizationStat,
    ContactStat,
    ScheduledEmailStat,
)

class OrganizationStatAdmin(ModelAdminBase):
    pass


admin.site.register(OrganizationStat, OrganizationStatAdmin)

class ContactStatAdmin(ModelAdminBase):
    pass


admin.site.register(ContactStat, ContactStatAdmin)

class ScheduledEmailStatAdmin(ModelAdminBase):
    pass


admin.site.register(ScheduledEmailStat, ScheduledEmailStatAdmin)
