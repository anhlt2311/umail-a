from django.contrib import admin

class ModelAdminBase(admin.ModelAdmin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # 生成者、生成時刻、変更者、変更時刻 を詳細画面に読取専用で表示する
        common_readonly_items = ["created_user", "created_time", "modified_user", "modified_time"]
        self.readonly_fields = [f.name for f in self.model._meta.get_fields() if f.name in common_readonly_items]

        # 新規作成(コピー起票のような機能)の有効化
        self.save_as = True

        # 一覧表示に全ての項目を表示させる
        self.list_display = [f.name for f in self.model._meta.get_fields() if f.concrete and not f.many_to_many]

        # 主キーを詳細外面へのリンクにする
        self.list_display_links = [f.name for f in self.model._meta.get_fields() if getattr(f, 'primary_key', False)]
