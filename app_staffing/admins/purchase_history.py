from django.contrib import admin
from app_staffing.admins.base import ModelAdminBase

from app_staffing.models.purchase_history import PurchaseHistory


class PurchaseHistoryAdmin(ModelAdminBase):
    list_filter = ("plan__plan_master", )
    search_fields = ("company__name", )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.list_display.append('price_text')
        self.list_display.append('company_name')

    def price_text(self, obj):
        return f"¥{obj.price}"
    price_text.short_description = "金額"

    def company_name(self, obj):
        return obj.company.name
    company_name.short_description = "会社名"


admin.site.register(PurchaseHistory, PurchaseHistoryAdmin)
