# Generated by Django 2.2.27 on 2022-03-08 04:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0137_remove_pmark_isms_unused_columns'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contact',
            name='description',
        ),
        migrations.RemoveField(
            model_name='contact',
            name='name',
        ),
        migrations.RemoveField(
            model_name='contact',
            name='tel',
        ),
        migrations.RemoveField(
            model_name='organization',
            name='description',
        ),
        migrations.RemoveField(
            model_name='organization',
            name='ever_had_a_contract_with_my_company',
        ),
        migrations.RemoveField(
            model_name='user',
            name='tel',
        ),
    ]
