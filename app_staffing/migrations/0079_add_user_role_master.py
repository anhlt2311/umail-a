# Generated by Django 2.2.24 on 2021-06-29 05:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0078_add_index_to_organization'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserRole',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=128)),
                ('order', models.IntegerField(unique=True)),
            ],
        ),
        migrations.AddIndex(
            model_name='userrole',
            index=models.Index(fields=['name'], name='user_role_name_idx'),
        ),
        migrations.AddIndex(
            model_name='userrole',
            index=models.Index(fields=['order'], name='type_preference_order_idx'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_role',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='app_staffing.UserRole'),
        ),
    ]
