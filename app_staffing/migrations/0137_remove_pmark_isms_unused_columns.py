# Generated by Django 2.2.27 on 2022-03-08 02:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0136_add_db_index'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='has_isms',
        ),
        migrations.RemoveField(
            model_name='company',
            name='has_p_mark',
        ),
        migrations.RemoveField(
            model_name='company',
            name='isms',
        ),
        migrations.RemoveField(
            model_name='company',
            name='p_mark',
        ),
        migrations.RemoveField(
            model_name='organization',
            name='has_isms',
        ),
        migrations.RemoveField(
            model_name='organization',
            name='has_p_mark',
        ),
        migrations.RemoveField(
            model_name='organization',
            name='isms',
        ),
        migrations.RemoveField(
            model_name='organization',
            name='p_mark',
        ),
    ]
