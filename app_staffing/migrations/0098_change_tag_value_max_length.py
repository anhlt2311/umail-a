# Generated by Django 2.2.24 on 2021-09-10 02:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_staffing', '0097_change_organization_max_length'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tag',
            name='internal_value',
            field=models.CharField(editable=False, help_text='タグの内部値(検索用, 小文字統一)', max_length=50),
        ),
        migrations.AlterField(
            model_name='tag',
            name='value',
            field=models.CharField(help_text='タグの内容', max_length=50),
        ),
    ]
