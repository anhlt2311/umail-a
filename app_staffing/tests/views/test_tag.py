from django.conf import settings
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse

from app_staffing.models.organization import Tag
from app_staffing.tests.factories.organization import TagFactory
from app_staffing.tests.settings import (TEST_USER_NAME, TEST_USER_PASSWORD,
                                         NORMAL_USER_NAME, NORMAL_USER_PASSWORD)
from app_staffing.tests.views.base import APICRUDTestMixin
from app_staffing.tests.views.validators.organizations import IsTag


class TagViewTestCase(APICRUDTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/tags.json',
    ]
    base_url = '/app_staffing/tags'
    resource_id = '1018d509-d56b-4e19-b2ca-201bbc5f4218'
    resource_validator_class = IsTag

    post_test_cases = [
        ('unregistered value in whole', {
            'value': 'hoge',
            'color': 'geekblue'
        }),
        ('registered value by another company', {
            'value': 'Rust',
        }),
    ]
    patch_test_cases = [
        ('unregistered value in whole', {
            'value': 'fuga',
        }),
        ('registered value by another company', {
            'value': 'Rust',
        }),
    ]

    skip_tests = ('put',)

    def test_over_max_length(self):
        data = {
            'value': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit nam.',
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['value']), settings.LENGTH_VALIDATIONS['tag']['value']['message'])

        response = self.client.patch(path=f'{self.base_url}/{self.resource_id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['value']), settings.LENGTH_VALIDATIONS['tag']['value']['message'])

    def test_set_null_value(self):
        data = {
            'value': None,
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

        response = self.client.patch(path=f'{self.base_url}/{self.resource_id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def test_unauth_user_cannot_registe(self):
        self.client.logout()
        data = {
            'value': 'hoge',
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(path=f'{self.base_url}/{self.resource_id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_normal_user_cannot_registe(self):
        self.client.logout()
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        data = {
            'value': 'hoge',
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response = self.client.patch(path=f'{self.base_url}/{self.resource_id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_tags(self):
        # 10件までには一括で削除可能
        data = {
            'source': [
                '1018d509-d56b-4e19-b2ca-201bbc5f4218',
                '2018d509-d56b-4e19-b2ca-201bbc5f4218',
                '3018d509-d56b-4e19-b2ca-201bbc5f4218',
                '4018d509-d56b-4e19-b2ca-201bbc5f4218',
                '5018d509-d56b-4e19-b2ca-201bbc5f4218',
                '6018d509-d56b-4e19-b2ca-201bbc5f4218',
                '7018d509-d56b-4e19-b2ca-201bbc5f4218',
                '8018d509-d56b-4e19-b2ca-201bbc5f4218',
                '9018d509-d56b-4e19-b2ca-201bbc5f4218',
                '1118d509-d56b-4e19-b2ca-201bbc5f4218',
            ],
        }
        tags = Tag.objects.filter(id__in=data['source'])
        self.assertEqual(len(tags), 10)
        response = self.client.delete(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tags = Tag.objects.filter(id__in=data['source'])
        self.assertEqual(len(tags), 0)

    def test_delete_tags_over_bulk_operation_limit(self):
        # 11件以上を削除しようとするとエラーになる
        data = {
            'source': [
                '1018d509-d56b-4e19-b2ca-201bbc5f4218',
                '2018d509-d56b-4e19-b2ca-201bbc5f4218',
                '3018d509-d56b-4e19-b2ca-201bbc5f4218',
                '4018d509-d56b-4e19-b2ca-201bbc5f4218',
                '5018d509-d56b-4e19-b2ca-201bbc5f4218',
                '6018d509-d56b-4e19-b2ca-201bbc5f4218',
                '7018d509-d56b-4e19-b2ca-201bbc5f4218',
                '8018d509-d56b-4e19-b2ca-201bbc5f4218',
                '9018d509-d56b-4e19-b2ca-201bbc5f4218',
                '1118d509-d56b-4e19-b2ca-201bbc5f4218',
                '2218d509-d56b-4e19-b2ca-201bbc5f4218',
            ],
        }
        response = self.client.delete(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_unauth_user_cannot_delete(self):
        self.client.logout()
        data = {
            'source': ['1018d509-d56b-4e19-b2ca-201bbc5f4218'],
        }
        response = self.client.delete(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_normal_user_cannot_delete(self):
        self.client.logout()
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        data = {
            'source': ['1018d509-d56b-4e19-b2ca-201bbc5f4218'],
        }
        response = self.client.delete(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_api(self):
        self.client.logout()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        url = api_reverse('tag_detail', kwargs={'pk': self.resource_id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_data = response.data
        self.assertEqual(str(response_data['id']), self.resource_id)

    def test_create_tag_with_is_skill_field(self):
        data = {
            'value': 'test',
            'is_skill': True
        }
        response = self.client.post(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('is_skill'), data.get('is_skill'))
        self.assertEqual(response.data.get('value'), data.get('value'))

    def test_update_tag_with_is_skill_field(self):
        data = {
            'is_skill': True,
            'value': 'new'
        }
        tag = TagFactory()
        response = self.client.patch(path=f'/app_staffing/tags/{tag.id}', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('value'), data.get('value'))
        self.assertEqual(response.data.get('is_skill'), data.get('is_skill'))


class TagCannotDeleteViewTestCase(APICRUDTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/contact_tags.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
    ]
    base_url = '/app_staffing/tags'
    resource_id = '6018d509-d56b-4e19-b2ca-201bbc5f4218'
    resource_validator_class = IsTag

    post_test_cases = [
        ('unregistered value in whole', {
            'value': 'hoge',
        }),
        ('registered value by another company', {
            'value': 'Rust',
        }),
    ]
    patch_test_cases = [
        ('unregistered value in whole', {
            'value': 'fuga',
        }),
        ('registered value by another company', {
            'value': 'Rust',
        }),
    ]

    skip_tests = ('put', 'get', 'patch', 'delete')

    def test_tag_cannot_delete(self):
        self.client.logout()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        url = api_reverse('tag_detail', kwargs={'pk': self.resource_id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_tags_cannot_delete(self):
        self.client.logout()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

        data = {
            'source': [
                '1018d509-d56b-4e19-b2ca-201bbc5f4218',
                '2018d509-d56b-4e19-b2ca-201bbc5f4218',
                '3018d509-d56b-4e19-b2ca-201bbc5f4218',
                '4018d509-d56b-4e19-b2ca-201bbc5f4218',
                '5018d509-d56b-4e19-b2ca-201bbc5f4218',
                '6018d509-d56b-4e19-b2ca-201bbc5f4218',
                '7018d509-d56b-4e19-b2ca-201bbc5f4218',
                '8018d509-d56b-4e19-b2ca-201bbc5f4218',
                '9018d509-d56b-4e19-b2ca-201bbc5f4218',
                '1118d509-d56b-4e19-b2ca-201bbc5f4218',
            ],
        }
        response = self.client.delete(path=self.base_url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TagCannotRegisterViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/tags.json',
    ]
    base_url = '/app_staffing/tags'
    resource_id = '1018d509-d56b-4e19-b2ca-201bbc5f4218'

    data = { 'value': 'Android' }

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_cannot_register(self):
        response = self.client.post(path=self.base_url, data=self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_update(self):
        response = self.client.patch(path=f'{self.base_url}/{self.resource_id}', data=self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TagViewPermissionTestCaseWithNormalUser(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/tags.json',
    ]
    base_url = '/app_staffing/tags'
    resource_id = '1018d509-d56b-4e19-b2ca-201bbc5f4218'

    def setUp(self):
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)

    def test_can_not_create(self):
        response = self.client.post(path=self.base_url, data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_get(self):
        response = self.client.get(path=self.base_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_not_update(self):
        response = self.client.patch(path=f'{self.base_url}/{self.resource_id}', data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_can_not_delete(self):
        response = self.client.delete(path=f'{self.base_url}/{self.resource_id}', data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
