from rest_framework import status
from rest_framework.test import APITestCase

from app_staffing.models import ContactPreference
from app_staffing.tests.factories.organization import ApiUser, AnotherUser, ContactFactory, BlacklistedProfile, RefuseAdsProfile, \
    AllOkProfile, AllNgProfile, JobOnlyProfile, PersonnelOnlyProfile, InfraProjectOnlyProfile, SalesProjectOnlyProfile, \
    DevelopmentProjectOnlyProfile, ProperOnlyProfile, IndividualOnlyProfile, SkilledPersonOnlyProfile, JobReEntrustProhibitedProfile, \
    PersonnelReEntrustProhibitedProfile, WorkerDispatchPersonOnlyProfile, QuasiMandatePersonOnlyProfile, KantoOnlyProfile, \
    KansaiOnlyProfile, RichManProfile, NotRichManProfile, ScoreFactory, TagFactory, ContactTagAssignmentFactory, Tag, ContactTagAssignment

from app_staffing.tests.views.base import APICRUDTestMixin, FilterTestMixin, MultiTenancyTestMixin, ReferenceViewMultiTenancyMixin
from app_staffing.tests.views.validators.preferences import IsContactPreference, IsContactPreferenceSummary

from app_staffing.models.organization import Company, Contact, Organization
from app_staffing.tests.settings import DEFAULT_COMPANY_ID

from datetime import datetime
from dateutil.relativedelta import relativedelta

class ContactPreferenceListViewTestCase(APICRUDTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/contact_scores.json',
        'app_staffing/tests/fixtures/contact_preferences.json',
    ]  # Note: Preference will be automatically created when accessed a /preference view first.

    base_url = '/app_staffing/contact_preference'
    resource_validator_class = IsContactPreferenceSummary

    skip_tests = ('post', 'get', 'patch', 'put', 'delete')  # Test List only.

    organization_id = '749303ca-7701-463e-83a8-8a5a35df1646'
    data_searchtype_other = {"searchtype": "other"}

    def test_search_restriction(self):
        path = self.base_url + '?wants_location_hokkaido_japan=true'
        response = self.client.get(path=path, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_search_with_company_restriction(self):
        # 自社の取引条件を設定し、Fixtureに記載されている取引先が全て条件対象外になるようにする
        company = Company.objects.get(id=DEFAULT_COMPANY_ID)
        company.capital_man_yen_required_for_transactions = 5000
        company.establishment_year = 10
        company.p_mark_or_isms = True
        company.invoice_system = True
        company.haken = True
        company.save()

        # 取引条件に使用されるパラメータが全てNullなのでこの取引先のみ検索対象になる
        organization = Organization.objects.create(
            name="test organization",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=False,
            company=company,
            organization_category_id=4 # client
        )

        Contact.objects.create(
            last_name='test',
            email='test_search_with_company_restriction@example.com',
            organization=organization,
            company=company,
        )

        org1 = Organization.objects.create(
            name="has_p_mark_or_isms_false_has_distribution_true",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=False,
            has_distribution=True,
        )
        Contact.objects.create(
            last_name='org01',
            email='org01@example.com',
            organization=org1,
            company=company,
        )
        org2 = Organization.objects.create(
            name="has_p_mark_or_isms_false_has_distribution_false",
            category='client',
            score=3,
            company=company,
            has_p_mark_or_isms=False,
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org02',
            email='org02@example.com',
            organization=org2,
            company=company,
        )
        org3 = Organization.objects.create(
            name="has_invoice_system_false_has_distribution_true",
            category='client',
            score=3,
            company=company,
            has_invoice_system=False,
            has_distribution=True,
        )
        Contact.objects.create(
            last_name='org03',
            email='org03@example.com',
            organization=org3,
            company=company,
        )
        org4 = Organization.objects.create(
            name="has_invoice_system_false_has_distribution_false",
            category='client',
            score=3,
            company=company,
            has_invoice_system=False,
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org04',
            email='org04@example.com',
            organization=org4,
            company=company,
        )

        org5 = Organization.objects.create(
            name="has_haken_false_has_distribution_true",
            category='client',
            score=3,
            company=company,
            has_haken=False,
            has_distribution=True,
        )
        Contact.objects.create(
            last_name='org05',
            email='org05@example.com',
            organization=org5,
            company=company,
        )
        org6 = Organization.objects.create(
            name="has_haken_false_has_distribution_false",
            category='client',
            score=3,
            company=company,
            has_haken=False,
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org06',
            email='org06@example.com',
            organization=org6,
            company=company,
        )
        org7 = Organization.objects.create(
            name="capital_man_yen_under_has_distribution_true",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            has_distribution=True,
        )
        Contact.objects.create(
            last_name='org07',
            email='org07@example.com',
            organization=org7,
            company=company,
        )
        org8 = Organization.objects.create(
            name="capital_man_yen_under_has_distribution_false",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org08',
            email='org08@example.com',
            organization=org8,
            company=company,
        )
        org9 = Organization.objects.create(
            name="establishment_date_has_distribution_true",
            category='client',
            score=3,
            company=company,
            establishment_date=datetime.now().strftime("%Y-%m-%d"),
            has_distribution=True,
        )
        Contact.objects.create(
            last_name='org09',
            email='org09@example.com',
            organization=org9,
            company=company,
        )
        org10 = Organization.objects.create(
            name="establishment_date_has_distribution_false",
            category='client',
            score=3,
            company=company,
            capital_man_yen=1,
            establishment_date=datetime.now().strftime("%Y-%m-%d"),
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org10',
            email='org10@example.com',
            organization=org10,
            company=company,
        )
        org11 = Organization.objects.create(
            name="satisfy_all_conditions",
            category='client',
            score=3,
            company=company,
            capital_man_yen=5001,
            establishment_date="1900-01-01",
            has_p_mark_or_isms=True,
            has_invoice_system=True,
            has_haken=True,
            has_distribution=False,
        )
        Contact.objects.create(
            last_name='org11',
            email='org11@example.com',
            organization=org11,
            company=company,
        )
        org12 = Organization.objects.create(
            name="is_blacklisted_true",
            category='client',
            score=3,
            company=company,
            is_blacklisted=True,
        )
        Contact.objects.create(
            last_name='org12',
            email='org12@example.com',
            organization=org12,
            company=company,
        )

        path = self.base_url + '?searchtype=other'
        response = self.client.get(path=path, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        actualEmailList = [response.data['results'][i]['email'] for i in range(len(response.data['results']))]
        actualEmailList.sort()
        expectedEmailList = [
            'org01@example.com',
            'org03@example.com',
            'org05@example.com',
            'org07@example.com',
            'org09@example.com',
            'org11@example.com',
            'test_search_with_company_restriction@example.com',
        ]
        self.assertEqual(actualEmailList, expectedEmailList)
        self.assertEqual(len(response.data['results']), 7)

    def test_removed_tag_not_match(self):
        tag = Tag.objects.create(company_id=DEFAULT_COMPANY_ID)
        contact_a = Contact.objects.get(id='78630567-72ce-49a3-84e6-8947b5b057f3')
        contact_b = Contact.objects.get(id='aa8f0a2b-acc5-4908-b4d0-918078a9373a')
        assign_a = ContactTagAssignment.objects.create(company_id=DEFAULT_COMPANY_ID, tag=tag, contact=contact_a)
        assign_b = ContactTagAssignment.objects.create(company_id=DEFAULT_COMPANY_ID, tag=tag, contact=contact_b)

        data_and = {
            "searchtype": "other",
            "contact__tags_and": str(tag.id)
        }
        response = self.client.get(path=self.base_url, data=data_and, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

        data_or = {
            "searchtype": "other",
            "contact__tags_or": str(tag.id)
        }
        response = self.client.get(path=self.base_url, data=data_or, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

        assign_a.delete()

        response = self.client.get(path=self.base_url, data=data_and, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

        response = self.client.get(path=self.base_url, data=data_or, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

        assign_b.delete()

        response = self.client.get(path=self.base_url, data=data_and, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0)

        response = self.client.get(path=self.base_url, data=data_or, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0)

    def test_blacklist(self):
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4)

        blacklisted_organization_id = '5468c29f-7b35-4540-b9d0-c1807d945734'
        blacklisted_organization = Organization.objects.get(id=blacklisted_organization_id)
        blacklisted_organization.is_blacklisted = False
        blacklisted_organization.save()

        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 5)

    def test_filter_establishment(self):
        # テストパターンについては以下を参照
        # https://docs.google.com/spreadsheets/d/1mFvw73D_zZs-ZQHuL73RQiRMuvwS0Tl96xMdejiuXUM/edit#gid=349468277&range=A9:E32

        company = Company.objects.get(id=DEFAULT_COMPANY_ID)
        organization = Organization.objects.get(id=self.organization_id)

        now_date = datetime.now().date()
        ten_years_ago = (datetime.now() - relativedelta(years=10)).date()

        # 自社 -> 取引先 = NG, 取引先 -> 自社 = NG
        company.establishment_date = now_date
        company.establishment_year = 10
        organization.establishment_date = now_date
        organization.establishment_year = 10

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = OK, 取引先 -> 自社 = NG
        company.establishment_date = now_date
        company.establishment_year = 10
        organization.establishment_date = ten_years_ago
        organization.establishment_year = 10

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = NG, 取引先 -> 自社 = OK
        company.establishment_date = ten_years_ago
        company.establishment_year = 10
        organization.establishment_date = now_date
        organization.establishment_year = 10

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = OK, 取引先 -> 自社 = OK
        company.establishment_date = ten_years_ago
        company.establishment_year = 10
        organization.establishment_date = ten_years_ago
        organization.establishment_year = 10

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

    def test_filter_capital_man_yen(self):
        # テストパターンについては以下を参照
        # https://docs.google.com/spreadsheets/d/1mFvw73D_zZs-ZQHuL73RQiRMuvwS0Tl96xMdejiuXUM/edit#gid=349468277&range=A35:E57

        company = Company.objects.get(id=DEFAULT_COMPANY_ID)
        organization = Organization.objects.get(id=self.organization_id)

        # 自社 -> 取引先 = NG, 取引先 -> 自社 = NG
        company.capital_man_yen = 1
        company.capital_man_yen_required_for_transactions = 100
        organization.capital_man_yen = 1
        organization.capital_man_yen_required_for_transactions = 100

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = OK, 取引先 -> 自社 = NG
        company.capital_man_yen = 1
        company.capital_man_yen_required_for_transactions = 100
        organization.capital_man_yen = 101
        organization.capital_man_yen_required_for_transactions = 100

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = NG, 取引先 -> 自社 = OK
        company.capital_man_yen = 101
        company.capital_man_yen_required_for_transactions = 100
        organization.capital_man_yen = 1
        organization.capital_man_yen_required_for_transactions = 100

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = OK, 取引先 -> 自社 = OK
        company.capital_man_yen = 101
        company.capital_man_yen_required_for_transactions = 100
        organization.capital_man_yen = 101
        organization.capital_man_yen_required_for_transactions = 100

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

    def test_filter_p_mark_or_isms(self):
        # テストパターンについては以下を参照
        # https://docs.google.com/spreadsheets/d/1mFvw73D_zZs-ZQHuL73RQiRMuvwS0Tl96xMdejiuXUM/edit#gid=349468277&range=A60:E82

        company = Company.objects.get(id=DEFAULT_COMPANY_ID)
        organization = Organization.objects.get(id=self.organization_id)

        # 自社 -> 取引先 = NG, 取引先 -> 自社 = NG
        company.has_p_mark_or_isms = False
        company.p_mark_or_isms = True
        organization.has_p_mark_or_isms = False
        organization.p_mark_or_isms = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = OK, 取引先 -> 自社 = NG
        company.has_p_mark_or_isms = False
        company.p_mark_or_isms = True
        organization.has_p_mark_or_isms = True
        organization.p_mark_or_isms = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = NG, 取引先 -> 自社 = OK
        company.has_p_mark_or_isms = True
        company.p_mark_or_isms = True
        organization.has_p_mark_or_isms = False
        organization.p_mark_or_isms = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = OK, 取引先 -> 自社 = OK
        company.has_p_mark_or_isms = True
        company.p_mark_or_isms = True
        organization.has_p_mark_or_isms = True
        organization.p_mark_or_isms = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

    def test_filter_invoice_system(self):
        # テストパターンについては以下を参照
        # https://docs.google.com/spreadsheets/d/1mFvw73D_zZs-ZQHuL73RQiRMuvwS0Tl96xMdejiuXUM/edit#gid=349468277&range=A60:E82

        company = Company.objects.get(id=DEFAULT_COMPANY_ID)
        organization = Organization.objects.get(id=self.organization_id)

        # 自社 -> 取引先 = NG, 取引先 -> 自社 = NG
        company.has_invoice_system = False
        company.invoice_system = True
        organization.has_invoice_system = False
        organization.invoice_system = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = OK, 取引先 -> 自社 = NG
        company.has_invoice_system = False
        company.invoice_system = True
        organization.has_invoice_system = True
        organization.invoice_system = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = NG, 取引先 -> 自社 = OK
        company.has_invoice_system = True
        company.invoice_system = True
        organization.has_invoice_system = False
        organization.invoice_system = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = OK, 取引先 -> 自社 = OK
        company.has_invoice_system = True
        company.invoice_system = True
        organization.has_invoice_system = True
        organization.invoice_system = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

    def test_filter_haken(self):
        # テストパターンについては以下を参照
        # https://docs.google.com/spreadsheets/d/1mFvw73D_zZs-ZQHuL73RQiRMuvwS0Tl96xMdejiuXUM/edit#gid=349468277&range=A60:E82

        company = Company.objects.get(id=DEFAULT_COMPANY_ID)
        organization = Organization.objects.get(id=self.organization_id)

        # 自社 -> 取引先 = NG, 取引先 -> 自社 = NG
        company.has_haken = False
        company.haken = True
        organization.has_haken = False
        organization.haken = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = OK, 取引先 -> 自社 = NG
        company.has_haken = False
        company.haken = True
        organization.has_haken = True
        organization.haken = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = NG, 取引先 -> 自社 = OK
        company.has_haken = True
        company.haken = True
        organization.has_haken = False
        organization.haken = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 0) # 表示されない

        # 自社 -> 取引先 = OK, 取引先 -> 自社 = OK
        company.has_haken = True
        company.haken = True
        organization.has_haken = True
        organization.haken = True

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜けない
        company.has_distribution = True
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜ける
        company.has_distribution = False
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜ける, 取引先商流 -> 抜ける
        company.has_distribution = True
        company.save()
        organization.has_distribution = True
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

        ## 自社商流 -> 抜けない, 取引先商流 -> 抜けない
        company.has_distribution = False
        company.save()
        organization.has_distribution = False
        organization.save()
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 4) # 表示される

    def test_filter_with_has_send_guide_setting(self):
        contact_pref_ids = ["78630567-72ce-49a3-84e6-8947b5b057f3", "aa8f0a2b-acc5-4908-b4d0-918078a9373a"]
        ContactPreference.objects.filter(contact_id__in=contact_pref_ids).update(has_send_guide=False)
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)
        self.assertEqual(len(response.data['results']), 2)

    def test_filter_with_has_send_guide_setting_for_all_contacts(self):
        ContactPreference.objects.filter().update(has_send_guide=False)
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 0)
        self.assertEqual(len(response.data['results']), 0)

    def test_filter_without_has_send_guide_setting_for_all_contacts(self):
        response = self.client.get(path=self.base_url, data=self.data_searchtype_other, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 4)
        self.assertEqual(len(response.data['results']), 4)




class ContactPreferenceListViewMultiTeantTestCase(MultiTenancyTestMixin, APITestCase):
    base_url = '/app_staffing/contact_preference'
    test_patterns = ('list',)

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        # Warning, contact preference is automatically created when creating a new contact.
        contact1 = ContactFactory(company=cls.company_1, created_user=cls.user1)
        contact2 = ContactFactory(company=cls.company_2, created_user=cls.user2)

        # Note: make sure that contact willing to receive ads, or that preference will not listed in List API.
        preference1 = contact1.contactpreference
        preference2 = contact2.contactpreference
        preference1.save()
        preference2.save()

        cls.resource_id_for_tenant_1 = contact1.id
        cls.resource_id_for_tenant_1 = contact2.id


class ContactPreferenceViewTestCase(APICRUDTestMixin, APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/contact_preferences.json'
    ]

    contact_id = '78630567-72ce-49a3-84e6-8947b5b057f3'

    base_url = f'/app_staffing/contacts/{contact_id}/preference'
    id_suffix = ''
    resource_validator_class = IsContactPreference

    patch_test_cases = [
        ('minimum', {"wants_location_kanto_japan": False}),
        ('full', {
            "wants_location_kanto_japan": True,
            "wants_location_kansai_japan": True
        })
    ]

    skip_tests = ('list', 'post', 'put', 'delete')

    def test_if_profile_can_not_delete(self):
        """Make sure that you can not delete contact profiles from API."""
        response = self.client.delete(path=self.base_url + self.id_suffix.format(self.resource_id), format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class ContactPreferenceViewMultiTenantTestCase(ReferenceViewMultiTenancyMixin, APITestCase):
    base_url = '/app_staffing/contacts/'
    suffix = 'preference'
    test_patterns = ('patch', 'delete')

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        # Warning, contact preference is automatically created when creating a new contact.
        contact1 = ContactFactory(company=cls.company_1, created_user=cls.user1)
        contact2 = ContactFactory(company=cls.company_2, created_user=cls.user2)

        # Note: make sure that contact willing to receive ads, or that preference will not listed in List API.
        preference1 = contact1.contactpreference
        preference2 = contact2.contactpreference
        preference1.save()
        preference2.save()

        cls.resource_id_for_tenant_1 = contact1.id
        cls.resource_id_for_tenant_2 = contact2.id


class ContactPreferenceViewFilterTestCase(FilterTestMixin, APITestCase):
    """A test case for contact filtering based on its profile.
    This test is critical because this filtered result will be used for targeting emails.
    """

    # テストの粒度について
    # https://docs.google.com/spreadsheets/d/1PkzpqYDvImnS9WJH5UBasO6mgje4ZNXqebelXcdMMuA/edit#gid=0
    # このシートを参考にしています。
    #
    # 配信情報
    # ・案件検索条件(エリア、職種詳細、スキル詳細、商流)
    # ・要員検索条件(エリア、職種詳細、スキル詳細、雇用形態、国籍、商流)
    # こだわり
    # ・個人評価
    # ・タグ
    # ・取引先ステータス
    # ・自社担当者
    # 取引条件
    # ・自社取引条件
    # ・取引先取引条件
    #
    # が、テスト対象の項目です。
    # 取引条件に関しては検索条件に関わらず常にバリデーションされます。
    #
    # ・各項目単品で検索
    # ・全項目入力して検索
    # ・配信ステータス * こだわり
    #
    # 最低でも上記のパターンを網羅するようテストを記述する

    fixtures = [
        'app_staffing/tests/fixtures/contactPreferenceFilter/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/contactPreferenceFilter/organizations.json',
        'app_staffing/tests/fixtures/contactPreferenceFilter/contacts.json',
        'app_staffing/tests/fixtures/contactPreferenceFilter/contact_preferences.json',
        'app_staffing/tests/fixtures/contact_scores.json',
        'app_staffing/tests/fixtures/contact_tags.json',
        'app_staffing/tests/fixtures/type_preferences.json',
        'app_staffing/tests/fixtures/skill_preferences.json',
        'app_staffing/tests/fixtures/contact_jobtypepreferences.json',
        'app_staffing/tests/fixtures/contact_jobskillpreferences.json',
        'app_staffing/tests/fixtures/contact_personneltypepreferences.json',
        'app_staffing/tests/fixtures/contact_personnelskillpreferences.json',
    ]

    contact_a_id = 'aa8f0a2b-acc5-4908-b4d0-918078a9373a'
    contact_b_id = '291dc792-d5a0-43fa-a500-e9e2273beaec'
    contact_c_id = '4acfeb11-e071-4901-ba90-4aeed52895d1'
    contact_d_id = '78630567-72ce-49a3-84e6-8947b5b057f3'
    contact_e_id = '78630567-72ce-49a3-84e6-8947b5b057a1'
    contact_f_id = '78630567-72ce-49a3-84e6-8947b5b057a2'
    contact_g_id = '78630567-72ce-49a3-84e6-8947b5b057a3'
    contact_h_id = '78630567-72ce-49a3-84e6-8947b5b057a4'
    contact_i_id = '78630567-72ce-49a3-84e6-8947b5b057a5'
    contact_j_id = '78630567-72ce-49a3-84e6-8947b5b057a6'
    contact_k_id = '78630567-72ce-49a3-84e6-8947b5b057a7'
    contact_l_id = '78630567-72ce-49a3-84e6-8947b5b057a8'
    contact_m_id = '78630567-72ce-49a3-84e6-8947b5b057a9'
    contact_n_id = '78630567-72ce-49a3-84e6-8947b5b057b1'
    contact_o_id = '78630567-72ce-49a3-84e6-8947b5b057b2'
    contact_p_id = '78630567-72ce-49a3-84e6-8947b5b057b3'
    contact_q_id = '78630567-72ce-49a3-84e6-8947b5b058b9'

    def test_filtering(self):

        test_patterns = [
            ('No filter', {}, [
                self.contact_a_id, self.contact_b_id, self.contact_c_id, self.contact_d_id, self.contact_e_id, self.contact_f_id, self.contact_g_id,
                self.contact_h_id, self.contact_i_id, self.contact_j_id, self.contact_k_id, self.contact_l_id, self.contact_m_id,
                self.contact_n_id, self.contact_o_id, self.contact_p_id, self.contact_q_id,
            ]),

            # 単品で検索
            ('filter jobtype dev',
                {
                    'searchtype': 'job', 'wants_location_hokkaido_japan': True, 'jobtype': 'dev', 'jobtype_dev_designer': True,
                    'jobskill_dev_youken': True, 'job_syouryu': 1
                },
                [self.contact_d_id]
            ),
            ('filter jobtype infra',
                {
                    'searchtype': 'job', 'wants_location_hokkaido_japan': True, 'jobtype': 'infra', 'jobtype_infra_server': True,
                    'jobskill_infra_youken': True, 'job_syouryu': 1
                },
                [self.contact_c_id]
            ),
            ('filter jobtype other',
                {
                    'searchtype': 'job', 'wants_location_hokkaido_japan': True, 'jobtype': 'other', 'jobtype_other_eigyo': True, 'job_syouryu': 1
                },
                [self.contact_b_id]
            ),
            ('filter personneltype dev',
                {
                    'searchtype': 'personnel', 'wants_location_hokkaido_japan': True, 'personneltype_dev': True,
                    'personneltype_dev_designer': True, 'personnelskill_dev_youken': True, 'personnel_country_japan': True, 'personnel_syouryu': 1
                },
                [self.contact_d_id]
            ),
            ('filter personneltype infra',
                {
                    'searchtype': 'personnel', 'wants_location_hokkaido_japan': True, 'personneltype_infra': True, 'personneltype_infra_server': True,
                    'personnelskill_infra_youken': True,'personnel_country_japan': True, 'personnel_syouryu': 1
                },
                [self.contact_c_id]
            ),
            ('filter personneltype other',
                {
                    'searchtype': 'personnel', 'wants_location_hokkaido_japan': True, 'personneltype_other': True, 'personneltype_other_eigyo': True,
                    'personnel_country_japan': True, 'personnel_syouryu': 1
                },
                [self.contact_b_id]
            ),
            ('filter score',
                {
                    'contact__score_gte': 4
                },
                [self.contact_a_id]
            ),
            ('filter tag_and',
                {
                    'contact__tags_and': '6018d509-d56b-4e19-b2ca-201bbc5f4218'
                },
                [self.contact_d_id]
            ),
            ('filter tag_or',
                {
                    'contact__tags_or': '6018d509-d56b-4e19-b2ca-201bbc5f4218,6018d509-d56b-4e19-b2ca-201bbc5f4217'
                },
                [self.contact_c_id, self.contact_d_id]
            ),
            ('filter organization category',
                {
                    'contact__organization__category_client': True
                },
                [
                    self.contact_a_id, self.contact_b_id, self.contact_c_id, self.contact_d_id, self.contact_e_id, self.contact_f_id, self.contact_g_id, self.contact_h_id,
                    self.contact_i_id, self.contact_j_id, self.contact_k_id, self.contact_l_id, self.contact_m_id,
                    self.contact_n_id, self.contact_o_id, self.contact_p_id, self.contact_q_id,
                ]
            ),
            ('filter staff',
                {
                    'contact__staff': 'd31034e2-ca12-4a6f-b1dc-0be092d1ac5d'
                },
                [
                    self.contact_d_id, self.contact_e_id, self.contact_f_id, self.contact_g_id, self.contact_h_id,
                    self.contact_i_id, self.contact_j_id, self.contact_k_id, self.contact_l_id, self.contact_m_id,
                    self.contact_n_id, self.contact_o_id, self.contact_p_id, self.contact_q_id,
                ]
            ),

            # 案件開発全網羅
            ('filter jobtype dev designer all skill',
                {
                    'searchtype': 'job', 'jobtype': 'dev', 'jobtype_dev_designer': True,
                    'jobskill_dev_youken': True, 'jobskill_dev_kihon': True, 'jobskill_dev_syousai': True, 'jobskill_dev_seizou': True,
                    'jobskill_dev_test': True, 'jobskill_dev_hosyu': True, 'jobskill_dev_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_d_id]
            ),
            ('filter jobtype dev front all skill',
                {
                    'searchtype': 'job', 'jobtype': 'dev', 'jobtype_dev_front': True,
                    'jobskill_dev_youken': True, 'jobskill_dev_kihon': True, 'jobskill_dev_syousai': True, 'jobskill_dev_seizou': True,
                    'jobskill_dev_test': True, 'jobskill_dev_hosyu': True, 'jobskill_dev_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_e_id]
            ),
            ('filter jobtype dev server all skill',
                {
                    'searchtype': 'job', 'jobtype': 'dev', 'jobtype_dev_server': True,
                    'jobskill_dev_youken': True, 'jobskill_dev_kihon': True, 'jobskill_dev_syousai': True, 'jobskill_dev_seizou': True,
                    'jobskill_dev_test': True, 'jobskill_dev_hosyu': True, 'jobskill_dev_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_f_id]
            ),
            ('filter jobtype dev pm all skill',
                {
                    'searchtype': 'job', 'jobtype': 'dev', 'jobtype_dev_pm': True,
                    'jobskill_dev_youken': True, 'jobskill_dev_kihon': True, 'jobskill_dev_syousai': True, 'jobskill_dev_seizou': True,
                    'jobskill_dev_test': True, 'jobskill_dev_hosyu': True, 'jobskill_dev_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_g_id]
            ),
            ('filter jobtype dev other all skill',
                {
                    'searchtype': 'job', 'jobtype': 'dev', 'jobtype_dev_other': True,
                    'jobskill_dev_youken': True, 'jobskill_dev_kihon': True, 'jobskill_dev_syousai': True, 'jobskill_dev_seizou': True,
                    'jobskill_dev_test': True, 'jobskill_dev_hosyu': True, 'jobskill_dev_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_h_id]
            ),

            # 案件インフラ全網羅
            ('filter jobtype infra all skill',
                {
                    'searchtype': 'job', 'jobtype': 'infra', 'jobtype_infra_server': True,
                    'jobskill_infra_youken': True, 'jobskill_infra_kihon': True, 'jobskill_infra_syousai': True, 'jobskill_infra_kouchiku': True,
                    'jobskill_infra_test': True, 'jobskill_infra_hosyu': True, 'jobskill_infra_kanshi': True, 'jobskill_infra_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_c_id]
            ),
            ('filter jobtype infra network all skill',
                {
                    'searchtype': 'job', 'jobtype': 'infra', 'jobtype_infra_network': True,
                    'jobskill_infra_youken': True, 'jobskill_infra_kihon': True, 'jobskill_infra_syousai': True, 'jobskill_infra_kouchiku': True,
                    'jobskill_infra_test': True, 'jobskill_infra_hosyu': True, 'jobskill_infra_kanshi': True, 'jobskill_infra_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_i_id]
            ),
            ('filter jobtype infra database all skill',
                {
                    'searchtype': 'job', 'jobtype': 'infra', 'jobtype_infra_database': True,
                    'jobskill_infra_youken': True, 'jobskill_infra_kihon': True, 'jobskill_infra_syousai': True, 'jobskill_infra_kouchiku': True,
                    'jobskill_infra_test': True, 'jobskill_infra_hosyu': True, 'jobskill_infra_kanshi': True, 'jobskill_infra_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_j_id]
            ),
            ('filter jobtype infra security all skill',
                {
                    'searchtype': 'job', 'jobtype': 'infra', 'jobtype_infra_security': True,
                    'jobskill_infra_youken': True, 'jobskill_infra_kihon': True, 'jobskill_infra_syousai': True, 'jobskill_infra_kouchiku': True,
                    'jobskill_infra_test': True, 'jobskill_infra_hosyu': True, 'jobskill_infra_kanshi': True, 'jobskill_infra_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_k_id]
            ),
            ('filter jobtype infra sys all skill',
                {
                    'searchtype': 'job', 'jobtype': 'infra', 'jobtype_infra_sys': True,
                    'jobskill_infra_youken': True, 'jobskill_infra_kihon': True, 'jobskill_infra_syousai': True, 'jobskill_infra_kouchiku': True,
                    'jobskill_infra_test': True, 'jobskill_infra_hosyu': True, 'jobskill_infra_kanshi': True, 'jobskill_infra_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_l_id]
            ),
            ('filter jobtype infra other all skill',
                {
                    'searchtype': 'job', 'jobtype': 'infra', 'jobtype_infra_other': True,
                    'jobskill_infra_youken': True, 'jobskill_infra_kihon': True, 'jobskill_infra_syousai': True, 'jobskill_infra_kouchiku': True,
                    'jobskill_infra_test': True, 'jobskill_infra_hosyu': True, 'jobskill_infra_kanshi': True, 'jobskill_infra_beginner': True,
                    'job_syouryu': 1
                },
                [self.contact_m_id]
            ),

            # 案件その他全網羅
            ('filter jobtype other kichi all skill',
                {
                    'searchtype': 'job', 'jobtype': 'other', 'jobtype_other_kichi': True,
                    'job_syouryu': 1
                },
                [self.contact_n_id]
            ),
            ('filter jobtype other support all skill',
                {
                    'searchtype': 'job', 'jobtype': 'other', 'jobtype_other_support': True,
                    'job_syouryu': 1
                },
                [self.contact_o_id]
            ),
            ('filter jobtype other other all skill',
                {
                    'searchtype': 'job', 'jobtype': 'other', 'jobtype_other_other': True,
                    'job_syouryu': 1
                },
                [self.contact_p_id]
            ),

            # 要員開発全網羅
            ('filter personneltype dev all type and skill',
                {
                    'searchtype': 'personnel', 'personneltype_dev': True,
                    'personneltype_dev_designer': True, 'personneltype_dev_server': True, 'personneltype_dev_front': True,
                    'personneltype_dev_pm': True, 'personneltype_dev_other': True,
                    'personnelskill_dev_youken': True, 'personnelskill_dev_kihon': True, 'personnelskill_dev_syousai': True, 'personnelskill_dev_seizou': True,
                    'personnelskill_dev_test': True, 'personnelskill_dev_hosyu': True, 'personnelskill_dev_beginner': True,
                    'personnel_country_japan': True, 'personnel_syouryu': 1
                },
                [self.contact_d_id]
            ),

            # 要員インフラ全網羅
            ('filter personneltype infra all type and skill',
                {
                    'searchtype': 'personnel', 'personneltype_infra': True,
                    'personneltype_infra_server': True, 'personneltype_infra_network': True, 'personneltype_infra_security': True,
                    'personneltype_infra_database': True, 'personneltype_infra_sys': True, 'personneltype_infra_other': True,
                    'personnelskill_infra_youken': True, 'personnelskill_infra_kihon': True, 'personnelskill_infra_syousai': True, 'personnelskill_infra_kouchiku': True,
                    'personnelskill_infra_test': True, 'personnelskill_infra_hosyu': True, 'personnelskill_infra_kanshi': True, 'personnelskill_infra_beginner': True,
                    'personnel_country_japan': True, 'personnel_syouryu': 1
                },
                [self.contact_c_id]
            ),

            # 要員その他全網羅
            ('filter personneltype other all type',
                {
                    'searchtype': 'personnel', 'personneltype_other': True,
                    'personneltype_other_eigyo': True, 'personneltype_other_kichi': True, 'personneltype_other_support': True, 'personneltype_other_other': True,
                    'personnel_country_japan': True, 'personnel_syouryu': 1
                },
                [self.contact_b_id]
            ),

            # 要員項目全網羅
            ('filter personneltype all param',
                {
                    'searchtype': 'personnel', 'personneltype_dev': True,
                    # dev
                    'personneltype_dev_designer': True, 'personneltype_dev_server': True, 'personneltype_dev_front': True,
                    'personneltype_dev_pm': True, 'personneltype_dev_other': True,
                    'personnelskill_dev_youken': True, 'personnelskill_dev_kihon': True, 'personnelskill_dev_syousai': True, 'personnelskill_dev_seizou': True,
                    'personnelskill_dev_test': True, 'personnelskill_dev_hosyu': True, 'personnelskill_dev_beginner': True,
                    # infra
                    'personneltype_infra_server': True, 'personneltype_infra_network': True, 'personneltype_infra_security': True,
                    'personneltype_infra_database': True, 'personneltype_infra_sys': True, 'personneltype_infra_other': True,
                    'personnelskill_infra_youken': True, 'personnelskill_infra_kihon': True, 'personnelskill_infra_syousai': True, 'personnelskill_infra_kouchiku': True,
                    'personnelskill_infra_test': True, 'personnelskill_infra_hosyu': True, 'personnelskill_infra_kanshi': True, 'personnelskill_infra_beginner': True,
                    # other
                    'personneltype_other_eigyo': True, 'personneltype_other_kichi': True, 'personneltype_other_support': True, 'personneltype_other_other': True,
                    'personnel_country_japan': True, 'personnel_syouryu': 1
                },
                [self.contact_d_id]
            ),

            # こだわり条件網羅
            ('filter tag organization staff',
                {
                    'contact__tags_or': '6018d509-d56b-4e19-b2ca-201bbc5f4218,6018d509-d56b-4e19-b2ca-201bbc5f4217',
                    'contact__organization__category_client': True,
                    'contact__staff': 'd31034e2-ca12-4a6f-b1dc-0be092d1ac5d',
                },
                [self.contact_d_id]
            ),

            # 全部検索
            ('filter job tag organization staff',
                {
                    'searchtype': 'job', 'wants_location_hokkaido_japan': True, 'jobtype': 'dev', 'jobtype_dev_designer': True,
                    'jobskill_dev_youken': True, 'job_syouryu': 1,
                    'contact__tags_or': '6018d509-d56b-4e19-b2ca-201bbc5f4218,6018d509-d56b-4e19-b2ca-201bbc5f4217',
                    'contact__organization__category_client': True,
                    'contact__staff': 'd31034e2-ca12-4a6f-b1dc-0be092d1ac5d',
                },
                [self.contact_d_id]
            ),
            ('filter personnel tag organization staff',
                {
                    'searchtype': 'personnel', 'personneltype_dev': True,
                    # dev
                    'personneltype_dev_designer': True, 'personneltype_dev_server': True, 'personneltype_dev_front': True,
                    'personneltype_dev_pm': True, 'personneltype_dev_other': True,
                    'personnelskill_dev_youken': True, 'personnelskill_dev_kihon': True, 'personnelskill_dev_syousai': True, 'personnelskill_dev_seizou': True,
                    'personnelskill_dev_test': True, 'personnelskill_dev_hosyu': True, 'personnelskill_dev_beginner': True,
                    # infra
                    'personneltype_infra_server': True, 'personneltype_infra_network': True, 'personneltype_infra_security': True,
                    'personneltype_infra_database': True, 'personneltype_infra_sys': True, 'personneltype_infra_other': True,
                    'personnelskill_infra_youken': True, 'personnelskill_infra_kihon': True, 'personnelskill_infra_syousai': True, 'personnelskill_infra_kouchiku': True,
                    'personnelskill_infra_test': True, 'personnelskill_infra_hosyu': True, 'personnelskill_infra_kanshi': True, 'personnelskill_infra_beginner': True,
                    # other
                    'personneltype_other_eigyo': True, 'personneltype_other_kichi': True, 'personneltype_other_support': True, 'personneltype_other_other': True,
                    'personnel_country_japan': True, 'personnel_syouryu': 1,
                    'contact__tags_or': '6018d509-d56b-4e19-b2ca-201bbc5f4218,6018d509-d56b-4e19-b2ca-201bbc5f4217',
                    'contact__organization__category_client': True,
                    'contact__staff': 'd31034e2-ca12-4a6f-b1dc-0be092d1ac5d',
                },
                [self.contact_d_id]
            ),
            ('filter contact organization country name',
                {
                    'searchtype': 'job', 'jobtype': 'other', 'jobtype_other_support': True, 'job_syouryu': 1,
                    # other
                    'contact__organization__organization_country_jp': True,
                },
                [self.contact_o_id]
            ),
            ('filter contact organization country name JP + KR',
                {
                    'searchtype': 'other',
                    # other
                    'contact__organization__organization_country_kr': True,
                    'contact__organization__organization_country_jp': True,
                },
                [self.contact_a_id, self.contact_b_id, self.contact_c_id, self.contact_d_id, self.contact_e_id, self.contact_f_id, self.contact_g_id,
                self.contact_h_id, self.contact_i_id, self.contact_j_id, self.contact_k_id, self.contact_l_id, self.contact_m_id,
                self.contact_n_id, self.contact_o_id, self.contact_p_id, self.contact_q_id]
            ),
            ('filter contact organization contract',
                {
                    'searchtype': 'other',
                    # other
                    'contact__organization__contract': True,
                },
                [self.contact_q_id]
            ),
            ('filter contact organization no contract',
                {
                    'searchtype': 'job', 'jobtype': 'other', 'jobtype_other_support': True, 'job_syouryu': 1,
                    # other
                    'contact__organization__contract': False,
                },
                [self.contact_o_id]
            ),
        ]

        self.execute_sub_test(
            base_url='/app_staffing/contact_preference',
            test_patterns=test_patterns,
            id_key_name='id'
        )
