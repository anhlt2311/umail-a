from django.conf import settings
from django.core.cache import cache
from rest_framework import status
from rest_framework.reverse import reverse as api_reverse
from rest_framework.test import APITestCase

from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, NORMAL_USER_NAME, NORMAL_USER_PASSWORD, ANOTHER_TENANT_NORMAL_USER_NAME, ANOTHER_TENANT_NORMAL_USER_PASSWORD
from app_staffing.models import Addon, Tag
from app_staffing.utils.cache import get_search_template_cache_key
from app_staffing.exceptions.base import TemplateRegisterLimitException


class SearchTemplateViewOrganizationTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/addon_datas.json',
    ]

    cache_key = get_search_template_cache_key("3efd25d0-989a-4fba-aa8d-b91708760eb1", "1957ab0f-b47c-455a-a7b7-4453cfffd05e", "organization")
    default = [
        {
            "name": "testname",
            "star": False,
            "display_name": "testname",
            "template_name": "testname",
            "values": {
                "address": "",
                "capital": 0,
                "category": [],
                "country": [],
                "employee_number": [],
                "contract": False,
                "fax1": "",
                "fax2": "",
                "fax3": "",
                "ignore_filter": False,
                "license": [],
                "name": "test",
                "score": 0,
                "tel1": "",
                "tel2": "",
                "tel3": ""
            },
            "pageSize": 10,
            "sortKey": "",
            "sortOrder": "",
            "selectedColumnKeys": []
        }
    ]

    default_with_total_count = {
        'templates': default,
        'total_available_count': 3,
    }

    stared = [
        {
            "name": "testname",
            "star": True,
            "display_name": "☆ : testname",
            "template_name": "testname",
            "values": {
                "address": "",
                "capital": 0,
                "category": [],
                "country": [],
                "employee_number": [],
                "contract": False,
                "fax1": "",
                "fax2": "",
                "fax3": "",
                "ignore_filter": False,
                "license": [],
                "name": "test",
                "score": 0,
                "tel1": "",
                "tel2": "",
                "tel3": ""
            },
            "pageSize": 10,
            "sortKey": "",
            "sortOrder": "",
            "selectedColumnKeys": []
        }
    ]

    stared_with_total_count = {
        'templates': stared,
        'total_available_count': 3,
    }

    post_data = {
        "templateName": "posttestname",
        "formValues": {
            "address": "",
            "capital": 0,
            "category": [],
            "country": [],
            "employee_number": [],
            "contract": True,
            "fax1": "",
            "fax2": "",
            "fax3": "",
            "ignore_filter": False,
            "license": [],
            "name": "posttest",
            "score": 0,
            "tel1": "",
            "tel2": "",
            "tel3": ""
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    post_data_2 = {
        "templateName": "posttestname_2",
        "formValues": {
            "address": "",
            "capital": 0,
            "category": [],
            "country": [],
            "employee_number": [],
            "contract": True,
            "fax1": "",
            "fax2": "",
            "fax3": "",
            "ignore_filter": False,
            "license": [],
            "name": "posttest",
            "score": 0,
            "tel1": "",
            "tel2": "",
            "tel3": ""
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    post_data_3 = {
        "templateName": "posttestname_3",
        "formValues": {
            "address": "",
            "capital": 0,
            "category": [],
            "country": [],
            "employee_number": [],
            "contract": True,
            "fax1": "",
            "fax2": "",
            "fax3": "",
            "ignore_filter": False,
            "license": [],
            "name": "posttest",
            "score": 0,
            "tel1": "",
            "tel2": "",
            "tel3": ""
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    invalid_data = {
        "templateName": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc",
        "formValues": {
            "address": "",
            "capital": 0,
            "category": [],
            "country": [],
            "employee_number": [],
            "contract": False,
            "fax1": "",
            "fax2": "",
            "fax3": "",
            "ignore_filter": False,
            "license": [],
            "name": "posttest",
            "score": 0,
            "tel1": "",
            "tel2": "",
            "tel3": ""
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        cache.set(self.cache_key, self.default)

    def test_get_search_template(self):
        response = self.client.get(path='/app_staffing/search_template/organization', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.default_with_total_count)

        # デフォルトに設定
        response = self.client.patch(path='/app_staffing/search_template/organization/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(path='/app_staffing/search_template/organization', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.stared_with_total_count)

        # デフォルトを解除
        response = self.client.patch(path='/app_staffing/search_template/organization/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(path='/app_staffing/search_template/organization', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.default_with_total_count)

    def test_post_search_template(self):
        response = self.client.post(path='/app_staffing/search_template/organization', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        response = self.client.delete(path='/app_staffing/search_template/organization/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(cache.get(self.cache_key)), 1)

        response = self.client.post(path='/app_staffing/search_template/organization', data=self.invalid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['templateName']), settings.LENGTH_VALIDATIONS['search_template']['templateName']['message'])

    def test_post_search_template_over_limit(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = "posttestname1"
        response = self.client.post(path='/app_staffing/search_template/organization', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname2"
        response = self.client.post(path='/app_staffing/search_template/organization', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/organization', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TemplateRegisterLimitException().default_detail)

        post_data["templateName"] = "posttestname2"
        response = self.client.delete(path='/app_staffing/search_template/organization/1', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/organization', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

    def test_post_search_template_with_addon(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = "posttestname1"
        response = self.client.post(path='/app_staffing/search_template/organization', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname2"
        response = self.client.post(path='/app_staffing/search_template/organization', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/organization', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TemplateRegisterLimitException().default_detail)

        Addon.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', addon_master_id=1)

        post_data["templateName"] = "posttestname4"
        response = self.client.post(path='/app_staffing/search_template/organization', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 4)

    def test_post_delete_search_template_same_name(self):
        response = self.client.post(path='/app_staffing/search_template/organization', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        # 同一ユーザで同一名称のテンプレートは登録できない
        response = self.client.post(path='/app_staffing/search_template/organization', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.TEMPLATEALREADY_EXISTS_ERROR_MESSAGE.format(newTemplateName=self.post_data["templateName"])
        self.assertTrue(msg in response.data['non_field_errors'][0])

        # 別ユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/organization', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete(path='/app_staffing/search_template/organization/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # あるユーザのテンプレート削除が別のユーザに影響することはない
        self.assertEqual(len(cache.get(self.cache_key)), 2)

    def test_post_delete_search_template_same_name_multi_tenancy(self):
        self.client.logout()
        cache_key = get_search_template_cache_key("3efd25d0-989a-4fba-aa8d-b91708760eb1", "d31034e2-ca12-4a6f-b1dc-0be092d1ac5d", "organization")
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/organization', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(cache_key)), 1)

        # 別テナントのユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=ANOTHER_TENANT_NORMAL_USER_NAME, password=ANOTHER_TENANT_NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/organization', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete(path='/app_staffing/search_template/organization/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # あるユーザのテンプレート削除が別のユーザに影響することはない
        self.assertEqual(len(cache.get(cache_key)), 1)

    def test_post_null_name(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = None
        response = self.client.post(path='/app_staffing/search_template/organization', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['templateName']), settings.NOT_NULL_VALIDATIONS['search_template']['message'])

    def test_unauth_get_post_patch_delete(self):
        response = self.client.post(path='/app_staffing/search_template/organization', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        self.client.logout()

        response = self.client.get(path='/app_staffing/search_template/organization', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(path='/app_staffing/search_template/organization', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(path='/app_staffing/search_template/organization/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.delete(path='/app_staffing/search_template/organization/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class SearchTemplateViewContactTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/tags.json',
    ]

    cache_key = get_search_template_cache_key("3efd25d0-989a-4fba-aa8d-b91708760eb1", "1957ab0f-b47c-455a-a7b7-4453cfffd05e", "contact")

    deleting_tag_id = "2018d509-d56b-4e19-b2ca-201bbc5f4218"
    default = [
        {
            "name": "testname",
            "star": False,
            "display_name": "testname",
            "template_name": "testname",
            "values": {
                "first_name": "test",
                "last_name": "",
                "organization__name": "",
                "email": "",
                "tel1": "",
                "tel2": "",
                "tel3": "",
                "position": "",
                "department": "",
                "staff": [],
                "date_range": [],
                "cc_mails": "",
                "tags": [
                    "1018d509-d56b-4e19-b2ca-201bbc5f4218",
                    deleting_tag_id,
                    "3018d509-d56b-4e19-b2ca-201bbc5f4218",
                ],
                "category": "",
                "contact_preference": [],
                "wants_location": [],
                "created_user": [],
                "modified_user": [],
                "comment_user": [],
                "ignore_blocklist_filter": False,
                "ignore_filter": False,
            },
            "pageSize": 10,
            "sortKey": "",
            "sortOrder": "",
            "selectedColumnKeys": []
        }
    ]

    default_with_total_count = {
        'templates': default,
        'total_available_count': 3,
    }

    contact_mail_preference_resource_name = "contact_mail_preference"
    contact_mail_preference_cache_key = get_search_template_cache_key("3efd25d0-989a-4fba-aa8d-b91708760eb1", "1957ab0f-b47c-455a-a7b7-4453cfffd05e", contact_mail_preference_resource_name)

    contact_mail_preference_default = [
        {
            "name": "testname",
            "star": False,
            "display_name": "testname",
            "template_name": "testname",
            "values": {
                "searchtype": "other",
                "contact__tags": [
                    "1018d509-d56b-4e19-b2ca-201bbc5f4218",
                    deleting_tag_id,
                    "3018d509-d56b-4e19-b2ca-201bbc5f4218",
                ],
            },
            "pageSize": 10,
            "sortKey": "",
            "sortOrder": "",
            "selectedColumnKeys": []
        }
    ]

    contact_mail_preference_default_with_total_count = {
        "templates": contact_mail_preference_default,
        "total_available_count": 1,
    }

    stared = [
        {
            "name": "testname",
            "star": True,
            "display_name": "☆ : testname",
            "template_name": "testname",
            "values": {
                "first_name": "test",
                "last_name": "",
                "organization__name": "",
                "email": "",
                "tel1": "",
                "tel2": "",
                "tel3": "",
                "position": "",
                "department": "",
                "staff": [],
                "date_range": [],
                "cc_mails": "",
                "tags": [
                    "1018d509-d56b-4e19-b2ca-201bbc5f4218",
                    deleting_tag_id,
                    "3018d509-d56b-4e19-b2ca-201bbc5f4218",
                ],
                "category": "",
                "contact_preference": [],
                "wants_location": [],
                "created_user": [],
                "modified_user": [],
                "comment_user": [],
                "ignore_blocklist_filter": False,
                "ignore_filter": False,
            },
            "pageSize": 10,
            "sortKey": "",
            "sortOrder": "",
            "selectedColumnKeys": []
        }
    ]

    stared_with_total_count = {
        'templates': stared,
        'total_available_count': 3,
    }

    post_data = {
        "templateName": "posttestname",
        "formValues": {
            "first_name": "posttest",
            "last_name": "",
            "organization__name": "",
            "email": "",
            "tel1": "",
            "tel2": "",
            "tel3": "",
            "position": "",
            "department": "",
            "staff": [],
            "date_range": [],
            "cc_mails": "",
            "tags": [],
            "category": "",
            "contact_preference": [],
            "wants_location": [],
            "created_user": [],
            "modified_user": [],
            "comment_user": [],
            "ignore_blocklist_filter": False,
            "ignore_filter": False,
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    post_data_2 = {
        "templateName": "posttestname_2",
        "formValues": {
            "first_name": "posttest",
            "last_name": "",
            "organization__name": "",
            "email": "",
            "tel1": "",
            "tel2": "",
            "tel3": "",
            "position": "",
            "department": "",
            "staff": [],
            "date_range": [],
            "cc_mails": "",
            "tags": [],
            "category": "",
            "contact_preference": [],
            "wants_location": [],
            "created_user": [],
            "modified_user": [],
            "comment_user": [],
            "ignore_blocklist_filter": False,
            "ignore_filter": False,
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    post_data_3 = {
        "templateName": "posttestname_3",
        "formValues": {
            "first_name": "posttest",
            "last_name": "",
            "organization__name": "",
            "email": "",
            "tel1": "",
            "tel2": "",
            "tel3": "",
            "position": "",
            "department": "",
            "staff": [],
            "date_range": [],
            "cc_mails": "",
            "tags": [],
            "category": "",
            "contact_preference": [],
            "wants_location": [],
            "created_user": [],
            "modified_user": [],
            "comment_user": [],
            "ignore_blocklist_filter": False,
            "ignore_filter": False,
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    invalid_data = {
        "templateName": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc",
        "formValues": {
            "first_name": "posttest",
            "last_name": "",
            "organization__name": "",
            "email": "",
            "tel1": "",
            "tel2": "",
            "tel3": "",
            "position": "",
            "department": "",
            "staff": [],
            "date_range": [],
            "cc_mails": "",
            "tags": [],
            "category": "",
            "contact_preference": [],
            "wants_location": [],
            "created_user": [],
            "modified_user": [],
            "comment_user": [],
            "ignore_blocklist_filter": False,
            "ignore_filter": False,
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        cache.set(self.cache_key, self.default)
        cache.set(self.contact_mail_preference_cache_key, self.contact_mail_preference_default)

    def test_get_search_template(self):
        response = self.client.get(path='/app_staffing/search_template/contact', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.default_with_total_count)

        # デフォルトに設定
        response = self.client.patch(path='/app_staffing/search_template/contact/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(path='/app_staffing/search_template/contact', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.stared_with_total_count)

        # デフォルトを解除
        response = self.client.patch(path='/app_staffing/search_template/contact/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(path='/app_staffing/search_template/contact', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.default_with_total_count)

    def test_post_search_template(self):
        response = self.client.post(path='/app_staffing/search_template/contact', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        response = self.client.delete(path='/app_staffing/search_template/contact/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(cache.get(self.cache_key)), 1)

        response = self.client.post(path='/app_staffing/search_template/contact', data=self.invalid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['templateName']), settings.LENGTH_VALIDATIONS['search_template']['templateName']['message'])

    def test_post_search_template_over_limit(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = "posttestname1"
        response = self.client.post(path='/app_staffing/search_template/contact', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname2"
        response = self.client.post(path='/app_staffing/search_template/contact', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/contact', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TemplateRegisterLimitException().default_detail)

        post_data["templateName"] = "posttestname2"
        response = self.client.delete(path='/app_staffing/search_template/contact/1', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/contact', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

    def test_post_search_template_with_addon(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = "posttestname1"
        response = self.client.post(path='/app_staffing/search_template/contact', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname2"
        response = self.client.post(path='/app_staffing/search_template/contact', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/contact', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TemplateRegisterLimitException().default_detail)

        Addon.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', addon_master_id=1)

        post_data["templateName"] = "posttestname4"
        response = self.client.post(path='/app_staffing/search_template/contact', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 4)

    def test_post_delete_search_template_same_name(self):
        response = self.client.post(path='/app_staffing/search_template/contact', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        # 同一ユーザで同一名称のテンプレートは登録できない
        response = self.client.post(path='/app_staffing/search_template/contact', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.TEMPLATEALREADY_EXISTS_ERROR_MESSAGE.format(newTemplateName=self.post_data["templateName"])
        self.assertTrue(msg in response.data['non_field_errors'][0])

        # 別ユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/contact', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete(path='/app_staffing/search_template/contact/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # あるユーザのテンプレート削除が別のユーザに影響することはない
        self.assertEqual(len(cache.get(self.cache_key)), 2)

    def test_post_delete_search_template_same_name_multi_tenancy(self):
        self.client.logout()
        cache_key = get_search_template_cache_key("3efd25d0-989a-4fba-aa8d-b91708760eb1", "d31034e2-ca12-4a6f-b1dc-0be092d1ac5d", "contact")
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/contact', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(cache_key)), 1)

        # 別テナントのユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=ANOTHER_TENANT_NORMAL_USER_NAME, password=ANOTHER_TENANT_NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/contact', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete(path='/app_staffing/search_template/contact/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # あるユーザのテンプレート削除が別のユーザに影響することはない
        self.assertEqual(len(cache.get(cache_key)), 1)

    def test_post_null_name(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = None
        response = self.client.post(path='/app_staffing/search_template/contact', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['templateName']), settings.NOT_NULL_VALIDATIONS['search_template']['message'])

    def test_unauth_get_post_patch_delete(self):
        response = self.client.post(path='/app_staffing/search_template/contact', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        self.client.logout()

        response = self.client.get(path='/app_staffing/search_template/contact', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(path='/app_staffing/search_template/contact', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(path='/app_staffing/search_template/contact/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.delete(path='/app_staffing/search_template/contact/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_deleted_tag_does_not_appear_in_response(self):
        tag = Tag.objects.get(id=self.deleting_tag_id)
        tag.delete()
        response = self.client.get(path='/app_staffing/search_template/contact', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        templates = data["templates"]
        self.assertFalse(self.deleting_tag_id in templates[0]["values"]["tags"])

    def test_search_condition_deleted_tag_does_not_appear_in_response(self):
        tag = Tag.objects.get(id=self.deleting_tag_id)
        tag.delete()
        url = api_reverse("search_template", kwargs={"resource_name": self.contact_mail_preference_resource_name})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        templates = data["templates"]
        self.assertFalse(self.deleting_tag_id in templates[0]["values"]["contact__tags"])

class SearchTemplateViewScheduledEmailTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/addon_datas.json',
    ]

    cache_key = get_search_template_cache_key("3efd25d0-989a-4fba-aa8d-b91708760eb1", "1957ab0f-b47c-455a-a7b7-4453cfffd05e", "scheduled_email")
    default = [
        {
            "name": "testname",
            "star": False,
            "display_name": "testname",
            "template_name": "testname",
            "values": {
                "attachments": True,
                "created_user": [],
                "date_range": [],
                "modified_user": [],
                "open_count_gt": "",
                "open_count_lt": "",
                "send_total_count_gt": "",
                "send_total_count_lt": "",
                "send_type": [],
                "sender_id": [],
                "sent_date": [],
                "status": [],
                "subject": "",
                "text": "",
                "text_format": [],
            },
            "pageSize": 10,
            "sortKey": "",
            "sortOrder": "",
            "selectedColumnKeys": []
        }
    ]

    default_with_total_count = {
        'templates': default,
        'total_available_count': 3,
    }

    stared = [
        {
            "name": "testname",
            "star": True,
            "display_name": "☆ : testname",
            "template_name": "testname",
            "values": {
                "attachments": True,
                "created_user": [],
                "date_range": [],
                "modified_user": [],
                "open_count_gt": "",
                "open_count_lt": "",
                "send_total_count_gt": "",
                "send_total_count_lt": "",
                "send_type": [],
                "sender_id": [],
                "sent_date": [],
                "status": [],
                "subject": "",
                "text": "",
                "text_format": [],
            },
            "pageSize": 10,
            "sortKey": "",
            "sortOrder": "",
            "selectedColumnKeys": []
        }
    ]

    stared_with_total_count = {
        'templates': stared,
        'total_available_count': 3,
    }

    post_data = {
        "templateName": "posttestname",
        "formValues": {
            "attachments": True,
            "created_user": [],
            "date_range": [],
            "modified_user": [],
            "open_count_gt": "",
            "open_count_lt": "",
            "send_total_count_gt": "",
            "send_total_count_lt": "",
            "send_type": [],
            "sender_id": [],
            "sent_date": [],
            "status": [],
            "subject": "",
            "text": "",
            "text_format": [],
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    post_data_2 = {
        "templateName": "posttestname_2",
        "formValues": {
            "attachments": True,
            "created_user": [],
            "date_range": [],
            "modified_user": [],
            "open_count_gt": "",
            "open_count_lt": "",
            "send_total_count_gt": "",
            "send_total_count_lt": "",
            "send_type": [],
            "sender_id": [],
            "sent_date": [],
            "status": [],
            "subject": "",
            "text": "",
            "text_format": [],
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    post_data_3 = {
        "templateName": "posttestname_3",
        "formValues": {
            "attachments": True,
            "created_user": [],
            "date_range": [],
            "modified_user": [],
            "open_count_gt": "",
            "open_count_lt": "",
            "send_total_count_gt": "",
            "send_total_count_lt": "",
            "send_type": [],
            "sender_id": [],
            "sent_date": [],
            "status": [],
            "subject": "",
            "text": "",
            "text_format": [],
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    invalid_data = {
        "templateName": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc",
        "formValues": {
            "attachments": True,
            "created_user": [],
            "date_range": [],
            "modified_user": [],
            "open_count_gt": "",
            "open_count_lt": "",
            "send_total_count_gt": "",
            "send_total_count_lt": "",
            "send_type": [],
            "sender_id": [],
            "sent_date": [],
            "status": [],
            "subject": "",
            "text": "",
            "text_format": [],
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        cache.set(self.cache_key, self.default)

    def test_get_search_template(self):
        response = self.client.get(path='/app_staffing/search_template/scheduled_email', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.default_with_total_count)

        # デフォルトに設定
        response = self.client.patch(path='/app_staffing/search_template/scheduled_email/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(path='/app_staffing/search_template/scheduled_email', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.stared_with_total_count)

        # デフォルトを解除
        response = self.client.patch(path='/app_staffing/search_template/scheduled_email/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(path='/app_staffing/search_template/scheduled_email', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.default_with_total_count)

    def test_post_search_template(self):
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        response = self.client.delete(path='/app_staffing/search_template/scheduled_email/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(cache.get(self.cache_key)), 1)

        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=self.invalid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['templateName']), settings.LENGTH_VALIDATIONS['search_template']['templateName']['message'])

    def test_post_search_template_over_limit(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = "posttestname1"
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname2"
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TemplateRegisterLimitException().default_detail)

        post_data["templateName"] = "posttestname2"
        response = self.client.delete(path='/app_staffing/search_template/scheduled_email/1', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

    def test_post_search_template_with_addon(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = "posttestname1"
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname2"
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TemplateRegisterLimitException().default_detail)

        Addon.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', addon_master_id=1)

        post_data["templateName"] = "posttestname4"
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 4)

    def test_post_delete_search_template_same_name(self):
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        # 同一ユーザで同一名称のテンプレートは登録できない
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.TEMPLATEALREADY_EXISTS_ERROR_MESSAGE.format(newTemplateName=self.post_data["templateName"])
        self.assertTrue(msg in response.data['non_field_errors'][0])

        # 別ユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete(path='/app_staffing/search_template/scheduled_email/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # あるユーザのテンプレート削除が別のユーザに影響することはない
        self.assertEqual(len(cache.get(self.cache_key)), 2)

    def test_post_delete_search_template_same_name_multi_tenancy(self):
        self.client.logout()
        cache_key = get_search_template_cache_key("3efd25d0-989a-4fba-aa8d-b91708760eb1", "d31034e2-ca12-4a6f-b1dc-0be092d1ac5d", "scheduled_email")
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(cache_key)), 1)

        # 別テナントのユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=ANOTHER_TENANT_NORMAL_USER_NAME, password=ANOTHER_TENANT_NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete(path='/app_staffing/search_template/scheduled_email/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # あるユーザのテンプレート削除が別のユーザに影響することはない
        self.assertEqual(len(cache.get(cache_key)), 1)

    def test_post_null_name(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = None
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['templateName']), settings.NOT_NULL_VALIDATIONS['search_template']['message'])

    def test_unauth_get_post_patch_delete(self):
        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        self.client.logout()

        response = self.client.get(path='/app_staffing/search_template/scheduled_email', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(path='/app_staffing/search_template/scheduled_email', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(path='/app_staffing/search_template/scheduled_email/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.delete(path='/app_staffing/search_template/scheduled_email/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class SearchTemplateViewContactMailPreferenceTestCase(APITestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/addon_datas.json',
    ]

    cache_key = get_search_template_cache_key("3efd25d0-989a-4fba-aa8d-b91708760eb1", "1957ab0f-b47c-455a-a7b7-4453cfffd05e", "contact_mail_preference")
    default = [
        {
            "name": "testname",
            "star": False,
            "display_name": "testname",
            "template_name": "testname",
            "values": {
                "contact__category": "heart",
                "contact__organization__category_approached": True,
                "contact__organization__category_client": True,
                "contact__organization__category_exchanged": True,
                "contact__organization__category_prospective": True,
                "job_syouryu": 4,
                "jobskill_dev_beginner": True,
                "jobskill_dev_hosyu": True,
                "jobskill_dev_kihon": True,
                "jobskill_dev_seizou": True,
                "jobskill_dev_syousai": True,
                "jobskill_dev_test": True,
                "jobskill_dev_youken": True,
                "jobtype": "dev",
                "jobtype_dev": "jobtype_dev_server",
                "searchtype": "job",
                "wants_location_hokkaido_japan": True,
            },
            "pageSize": 10,
            "sortKey": "",
            "sortOrder": "",
            "selectedColumnKeys": []
        }
    ]

    default_with_total_count = {
        'templates': default,
        'total_available_count': 3,
    }

    stared = [
        {
            "name": "testname",
            "star": True,
            "display_name": "☆ : testname",
            "template_name": "testname",
            "values": {
                "contact__category": "heart",
                "contact__organization__category_approached": True,
                "contact__organization__category_client": True,
                "contact__organization__category_exchanged": True,
                "contact__organization__category_prospective": True,
                "job_syouryu": 4,
                "jobskill_dev_beginner": True,
                "jobskill_dev_hosyu": True,
                "jobskill_dev_kihon": True,
                "jobskill_dev_seizou": True,
                "jobskill_dev_syousai": True,
                "jobskill_dev_test": True,
                "jobskill_dev_youken": True,
                "jobtype": "dev",
                "jobtype_dev": "jobtype_dev_server",
                "searchtype": "job",
                "wants_location_hokkaido_japan": True,
            },
            "pageSize": 10,
            "sortKey": "",
            "sortOrder": "",
            "selectedColumnKeys": []
        }
    ]

    stared_with_total_count = {
        'templates': stared,
        'total_available_count': 3,
    }

    post_data = {
        "templateName": "posttestname",
        "formValues": {
            "contact__category": "heart",
            "contact__organization__category_approached": True,
            "contact__organization__category_client": True,
            "contact__organization__category_exchanged": True,
            "contact__organization__category_prospective": True,
            "job_syouryu": 4,
            "jobskill_dev_beginner": True,
            "jobskill_dev_hosyu": True,
            "jobskill_dev_kihon": True,
            "jobskill_dev_seizou": True,
            "jobskill_dev_syousai": True,
            "jobskill_dev_test": True,
            "jobskill_dev_youken": True,
            "jobtype": "dev",
            "jobtype_dev": "jobtype_dev_server",
            "searchtype": "job",
            "wants_location_hokkaido_japan": True,
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    post_data_2 = {
        "templateName": "posttestname_2",
        "formValues": {
            "contact__category": "heart",
            "contact__organization__category_approached": True,
            "contact__organization__category_client": True,
            "contact__organization__category_exchanged": True,
            "contact__organization__category_prospective": True,
            "job_syouryu": 4,
            "jobskill_dev_beginner": True,
            "jobskill_dev_hosyu": True,
            "jobskill_dev_kihon": True,
            "jobskill_dev_seizou": True,
            "jobskill_dev_syousai": True,
            "jobskill_dev_test": True,
            "jobskill_dev_youken": True,
            "jobtype": "dev",
            "jobtype_dev": "jobtype_dev_server",
            "searchtype": "job",
            "wants_location_hokkaido_japan": True,
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    post_data_3 = {
        "templateName": "posttestname_3",
        "formValues": {
            "contact__category": "heart",
            "contact__organization__category_approached": True,
            "contact__organization__category_client": True,
            "contact__organization__category_exchanged": True,
            "contact__organization__category_prospective": True,
            "job_syouryu": 4,
            "jobskill_dev_beginner": True,
            "jobskill_dev_hosyu": True,
            "jobskill_dev_kihon": True,
            "jobskill_dev_seizou": True,
            "jobskill_dev_syousai": True,
            "jobskill_dev_test": True,
            "jobskill_dev_youken": True,
            "jobtype": "dev",
            "jobtype_dev": "jobtype_dev_server",
            "searchtype": "job",
            "wants_location_hokkaido_japan": True,
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    invalid_data = {
        "templateName": "aaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbaaaaabbbbbccc",
        "formValues": {
            "contact__category": "heart",
            "contact__organization__category_approached": True,
            "contact__organization__category_client": True,
            "contact__organization__category_exchanged": True,
            "contact__organization__category_prospective": True,
            "job_syouryu": 4,
            "jobskill_dev_beginner": True,
            "jobskill_dev_hosyu": True,
            "jobskill_dev_kihon": True,
            "jobskill_dev_seizou": True,
            "jobskill_dev_syousai": True,
            "jobskill_dev_test": True,
            "jobskill_dev_youken": True,
            "jobtype": "dev",
            "jobtype_dev": "jobtype_dev_server",
            "searchtype": "job",
            "wants_location_hokkaido_japan": True,
        },
        "pageSize": 10,
        "sortKey": "",
        "sortOrder": "",
        "selectedColumnKeys": []
    }

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        cache.set(self.cache_key, self.default)

    def test_get_search_template(self):
        response = self.client.get(path='/app_staffing/search_template/contact_mail_preference', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.default_with_total_count)

        # デフォルトに設定
        response = self.client.patch(path='/app_staffing/search_template/contact_mail_preference/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(path='/app_staffing/search_template/contact_mail_preference', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.stared_with_total_count)

        # デフォルトを解除
        response = self.client.patch(path='/app_staffing/search_template/contact_mail_preference/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(path='/app_staffing/search_template/contact_mail_preference', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.default_with_total_count)

    def test_post_search_template(self):
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        response = self.client.delete(path='/app_staffing/search_template/contact_mail_preference/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(cache.get(self.cache_key)), 1)

        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=self.invalid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['templateName']), settings.LENGTH_VALIDATIONS['search_template']['templateName']['message'])

    def test_post_search_template_over_limit(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = "posttestname1"
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname2"
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TemplateRegisterLimitException().default_detail)

        post_data["templateName"] = "posttestname2"
        response = self.client.delete(path='/app_staffing/search_template/contact_mail_preference/1', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

    def test_post_search_template_with_addon(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = "posttestname1"
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        post_data["templateName"] = "posttestname2"
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 3)

        post_data["templateName"] = "posttestname3"
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], TemplateRegisterLimitException().default_detail)

        Addon.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', addon_master_id=1)

        post_data["templateName"] = "posttestname4"
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 4)

    def test_post_delete_search_template_same_name(self):
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        # 同一ユーザで同一名称のテンプレートは登録できない
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = settings.TEMPLATEALREADY_EXISTS_ERROR_MESSAGE.format(newTemplateName=self.post_data["templateName"])
        self.assertTrue(msg in response.data['non_field_errors'][0])

        # 別ユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete(path='/app_staffing/search_template/contact_mail_preference/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # あるユーザのテンプレート削除が別のユーザに影響することはない
        self.assertEqual(len(cache.get(self.cache_key)), 2)

    def test_post_delete_search_template_same_name_multi_tenancy(self):
        self.client.logout()
        cache_key = get_search_template_cache_key("3efd25d0-989a-4fba-aa8d-b91708760eb1", "d31034e2-ca12-4a6f-b1dc-0be092d1ac5d", "contact_mail_preference")
        self.client.login(username=NORMAL_USER_NAME, password=NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(cache_key)), 1)

        # 別テナントのユーザであれば同一名称でも登録できる
        self.client.logout()
        self.client.login(username=ANOTHER_TENANT_NORMAL_USER_NAME, password=ANOTHER_TENANT_NORMAL_USER_PASSWORD)
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.delete(path='/app_staffing/search_template/contact_mail_preference/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # あるユーザのテンプレート削除が別のユーザに影響することはない
        self.assertEqual(len(cache.get(cache_key)), 1)

    def test_post_null_name(self):
        post_data = self.post_data.copy()
        post_data["templateName"] = None
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['templateName']), settings.NOT_NULL_VALIDATIONS['search_template']['message'])

    def test_unauth_get_post_patch_delete(self):
        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(cache.get(self.cache_key)), 2)

        self.client.logout()

        response = self.client.get(path='/app_staffing/search_template/contact_mail_preference', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.post(path='/app_staffing/search_template/contact_mail_preference', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.patch(path='/app_staffing/search_template/contact_mail_preference/0', format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = self.client.delete(path='/app_staffing/search_template/contact_mail_preference/1', data=self.post_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
