from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse

from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD


class PurchaseHistoryViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/plan.json',
        'app_staffing/tests/fixtures/purchase_history.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get(self):
        url = api_reverse('purchase_history')
        response_get = self.client.get(url)
        self.assertEqual(response_get.status_code, status.HTTP_200_OK)

        expected_data = {
            'e57a0a67-a005-4cf7-9f26-62d089bfdd33': {
                'id': 'e57a0a67-a005-4cf7-9f26-62d089bfdd33',
                'period_start': '2021-12-01',
                'period_end': '2021-12-30',
                'plan_name': 'ライト',
                'method_name': 'クレジットカード',
                'card_last4': '4242',
                'price': 41000,
                'option_names': ['検索条件テンプレート登録数', 'コメントテンプレート登録数', '配信開封情報の取得', '配信開封情報の取得期間の延長', '配信添付容量', 'HTML配信メールのコモレビ広告を消す', '1通あたりの送信可能件数', 'ユーザー追加+1'],
                'created_time': '2021-12-01T14:59:08.878000+09:00',
            },
            'e57a0a67-a005-4cf7-9f26-62d089bfdd32': {
                'id': 'e57a0a67-a005-4cf7-9f26-62d089bfdd32',
                'period_start': '2021-11-03',
                'period_end': '2021-11-30',
                'plan_name': '',
                'method_name': 'クレジットカード',
                'card_last4': '4242',
                'price': 3000,
                'option_names': ['ユーザー追加+1'],
                'created_time': '2021-11-03T14:59:08.878000+09:00',
            },
            'e57a0a67-a005-4cf7-9f26-62d089bfdd31': {
                'id': 'e57a0a67-a005-4cf7-9f26-62d089bfdd31',
                'period_start': '2021-11-02',
                'period_end': '2021-11-30',
                'plan_name': '',
                'method_name': 'クレジットカード',
                'card_last4': '4242',
                'price': 500,
                'option_names': ['検索条件テンプレート登録数'],
                'created_time': '2021-11-02T14:59:08.878000+09:00',
            },
            'e57a0a67-a005-4cf7-9f26-62d089bfdd30': {
                'id': 'e57a0a67-a005-4cf7-9f26-62d089bfdd30',
                'period_start': '2021-11-01',
                'period_end': '2021-11-30',
                'plan_name': 'ライト',
                'method_name': 'クレジットカード',
                'card_last4': '4242',
                'price': 30000,
                'option_names': [],
                'created_time': '2021-11-01T14:59:08.878000+09:00',
            },
        }

        self.assertEqual(response_get.data.get('count'), len(expected_data))
        for dic in response_get.data.get('results'):
            id = dic.get('id')
            for k, v in dic.items():
                self.assertEqual(v, expected_data[id][k])

    def test_unauth_user_cannot_get(self):
        self.client.logout()
        url = api_reverse('purchase_history')
        response_get = self.client.get(url)
        self.assertEqual(response_get.status_code, status.HTTP_401_UNAUTHORIZED)
