from uuid import UUID
from urllib.parse import urlencode
from dateutil.parser import parse as parse_date

from django.contrib.auth import get_user_model
from django.http import QueryDict, HttpResponse, StreamingHttpResponse
from rest_framework import status
from rest_framework.test import APIClient

from app_staffing.tests.factories.company import CompanyFactory
from app_staffing.tests.factories.user import AppUserFactory, UserRoleFactory
from app_staffing.tests.views.validators.generals import IsPagenatedResponse, IsPageOutOfRangeResponse
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD

COMPANY_1_ID_FOR_MULTI_TENANCY_TEST = 'c5bb8bb6-9a46-4021-b805-99ae5306cfae'
COMPANY_2_ID_FOR_MULTI_TENANCY_TEST = '6bc095d0-04c1-428d-a451-332dd3922357'


def is_date_string(string):
    try:
        parse_date(string)
        return True
    except ValueError:
        return False


class HttpMethod(object):
    list = 'list'
    get = 'get'
    post = 'post'
    put = 'put'
    patch = 'patch'
    delete = 'delete'


class SubTestMixin(object):
    def execute_sub_test(self, test_patterns):
        """A helper function that helps SubTest of filters.
        Note: The TestCase which has this function must have a class attribute 'base_url'.
        :param test_patterns: A list of tuples that stands for test pattens.
        :type test_patterns: list of tuple, ('description of the test pattern', 'query_string', 'expected num of entries')[]
        :return: None
        """
        for pattern, query_string, result in test_patterns:
            with self.subTest(pattern):
                response = self.client.get(path='{0}{1}'.format(self.base_url, query_string))
                self.assertEqual(response.status_code, 200)
                self.assertEqual(len(response.data['results']), result)


class TestPostErrorMixin(object):
    fixtures = None  # Override by child class.
    base_url = '/your_app_name/resource_url'  # Override by child class.

    post_test_cases = []  # Define test patterns for a post test. (List of a tuple that includes pattern name, test data and keys which should be detected as an error.)
    # e.g: [('wrong_name', {'name': 'foo', 'description': 'bar'}, ['name']), ('wrong_all', {'name': 1, 'description': 2}, ['name', 'description'])]

    def test_post_error_cases(self):
        for test_pattern, test_data, wrong_keys in self.post_test_cases:
            assert isinstance(test_data, dict), "Given test_data is not a dict"

            with self.subTest(pattern=test_pattern):
                response = self.client.post(path=self.base_url, data=test_data, format='json')
                self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

                for key in wrong_keys:
                    self.assertTrue(key in response.data.keys())

class TestQueryParamErrorMixin(object):
    # Override by child class.
    fixtures = None
    base_url = None

    filter_params = {}
    pagination_params = { 'page': 1, 'page_size': 1 }
    ordering_params = { 'ordering': 'dummy' }
    invalid_params = { 'notexist_field': 'dummy' }

    def all_valid_params(self):
        return dict(self.filter_params, **self.pagination_params, **self.ordering_params)

    def test_query_param_error_cases_with_valid_query_params(self):
        q = QueryDict(mutable=True)
        q.update(self.all_valid_params())
        query_string = q.urlencode()

        response = self.client.get(path=self.base_url + '?{}'.format(query_string))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_query_param_error_cases_with_invalid_query_params_only(self):
        q = QueryDict(mutable=True)
        q.update(self.invalid_params)
        query_string = q.urlencode()

        response = self.client.get(path=self.base_url + '?{}'.format(query_string))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_query_param_error_cases_with_valid_query_params_additionally(self):
        q = QueryDict(mutable=True)
        q.update(self.all_valid_params())
        q.update(self.invalid_params)
        query_string = q.urlencode()

        response = self.client.get(path=self.base_url + '?{}'.format(query_string))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class APICRUDTestMixin(object):
    """A Mixin for rest_framework APITestCase"""

    fixtures = None  # Override by child class.
    base_url = '/your_app_name/resource_url'  # Override by child class.
    resource_id = ''  # Override by child class, a id of a fixture.
    id_suffix = '/{0}'  # Override by child class if necessary.
    list_response_validator_class = IsPagenatedResponse  # It must be an instance of schematics.Model that represents a whole API response.
    resource_validator_class = None  # It must be an instance of schematics.Model that represents a single resource data.
    resource_validator_class_for_list = None  # Specify this if the API has alternative serializer for list API.

    post_test_cases = []  # Define test patterns for a post test. (List of a tuple that includes pattern name and test data.)
    # e.g: [('fully_qualified', {'name': 'foo', 'description': 'bar'}), ('minimum', {'name': 'foo'})]
    patch_test_cases = []  # Define test patterns for a patch test. (List of a tuple that includes pattern name and test data.)
    # e.g: [('fully_qualified', {'name': 'foo', 'description': 'bar'}), ('minimum', {'name': 'foo'})]

    get_after_post_test_cases = []

    skip_tests = ('put',)  # Specify a tuple of SkipChoices.

    @staticmethod
    def _auto_cast(entry):
        if isinstance(entry, UUID):
            return str(entry)
        else:
            return entry

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def _check_values(self, test_data, response_data):
        for attribute_name, expected_value in test_data.items():

            received_element = response_data.get(attribute_name)
            if not received_element:
                continue

            if isinstance(received_element, list):
                cast_list = map(self._auto_cast, received_element)
                self.assertCountEqual(cast_list, expected_value)
            elif isinstance(received_element, dict):
                cast_dict = {k: self._auto_cast(v) for k, v in received_element}
                self.assertDictEqual(cast_dict, expected_value)
            elif isinstance(received_element, str):
                if is_date_string(received_element):
                    self.assertEqual(parse_date(received_element), parse_date(expected_value))
                else:
                    self.assertEqual(self._auto_cast(received_element), expected_value)
            elif isinstance(received_element, UUID):
                self.assertEqual(str(received_element), expected_value)
            else:
                self.assertEqual(received_element, expected_value)

    def test_list_api(self):
        if HttpMethod.list in self.skip_tests:
            self.skipTest('list test is marked as Skip.')

        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(len(response.data['results']) > 0)

        validator = self.list_response_validator_class(response.data)
        validator.validate()

        for entry in response.data['results']:
            if self.resource_validator_class_for_list:
                validator = self.resource_validator_class_for_list(entry)
            else:
                validator = self.resource_validator_class(entry)
            validator.validate()

    def test_list_api_out_of_range(self):
        if HttpMethod.list in self.skip_tests:
            self.skipTest('list test is marked as Skip.')

        response = self.client.get(path=self.base_url + '?page=100000000000', format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        validator = IsPageOutOfRangeResponse(response.data)
        validator.validate()

    def test_post_get_api(self):
        if HttpMethod.post in self.skip_tests:
            self.skipTest('post test is marked as Skip.')

        self.assertTrue(expr=self.post_test_cases, msg='A test data for post is not defined in a class.')

        for test_pattern, test_data in self.post_test_cases:
            assert isinstance(test_data, dict), "Given test_data is not a dict"
            with self.subTest(pattern=test_pattern):
                response = self.client.post(path=self.base_url, data=test_data, format='json')
                self.assertEqual(response.status_code, status.HTTP_201_CREATED)

                validator = self.resource_validator_class(response.data)
                validator.validate()

                # Check if data is successfully updated.
                self._check_values(test_data, response_data=response.data)

        for test_pattern, test_data in self.get_after_post_test_cases:
            assert isinstance(test_data, dict), "Given test_data is not a dict"
            with self.subTest(pattern=test_pattern):
                q = QueryDict(mutable=True)
                q.update(test_data)
                query_string = q.urlencode()
                response = self.client.get(path=self.base_url + '?{}'.format(query_string))
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                self.assertEqual(len(response.data['results']), test_data['page_size'])

    def test_get_api(self):
        if HttpMethod.get in self.skip_tests:
            self.skipTest('get test is marked as Skip.')

        response = self.client.get(path=self.base_url + self.id_suffix.format(self.resource_id), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        validator = self.resource_validator_class(response.data)
        validator.validate()

    def test_patch_api(self):
        if HttpMethod.patch in self.skip_tests:
            self.skipTest('post test is marked as Skip.')

        self.assertTrue(expr=self.patch_test_cases, msg='A test data for patch is not defined in a class.')

        for test_pattern, test_data in self.patch_test_cases:
            assert isinstance(test_data, dict), "Given test_data is not a dict"
            with self.subTest(pattern=test_pattern):

                response = self.client.patch(path=self.base_url + self.id_suffix.format(self.resource_id),
                                             data=test_data, format='json')
                self.assertEqual(response.status_code, status.HTTP_200_OK)

                validator = self.resource_validator_class(response.data)
                validator.validate()

                # Check if data is successfully updated.
                self._check_values(test_data, response_data=response.data)

    def test_delete_api(self):
        if HttpMethod.delete in self.skip_tests:
            self.skipTest('delete test is marked as Skip.')

        response = self.client.delete(path=self.base_url + self.id_suffix.format(self.resource_id), format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class FilterTestMixin(object):
    """A Mixin for rest_framework APITestCase
    Note: you need to create test data via self.setupTestData in a TestCase.
    Note: Currently, this Filter test is only available for paginated list API.
    """

    # test_patterns = [
    #     (Define a List of tuple that includes pattern name, filter condition, list of the id of resources
    #     which are expected to be in a result.)
    #     Example: [('JobItemOnly', {'job_item_only': True}, ['68acf370-9e4f-494b-b8f4-7100c0801c80'])
    # ]
    maxDiff = None

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def execute_sub_test(self, base_url, test_patterns, id_key_name):

        for test_pattern_name, filter_condition, expected_ids in test_patterns:
            with self.subTest(filter_condition=filter_condition):
                query_params = urlencode(filter_condition)
                response = self.client.get(path=base_url + '?' + query_params, format='json')
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                hit_resource_ids = [data[id_key_name] for data in response.data['results']]
                self.assertCountEqual(expected_ids, hit_resource_ids)


class PerUserViewAccessControlTestMixin(object):
    base_url = None
    data_for_post = None
    user_model = get_user_model()

    @classmethod
    def setUpTestData(cls):
        company = CompanyFactory.create()
        AppUserFactory.create(username='user1', user_service_id='user1_347328', email='user1@example.com', company=company)
        AppUserFactory.create(username='user2', user_service_id='user2_347328', email='user2@example.com', company=company)
        # Make sure that the factory actually created the test users.
        assert cls.user_model.objects.get(username='user1') and cls.user_model.objects.get(username='user2')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert self.base_url, "Please override the class attribute 'base_url' in a test case which uses this mixin."
        assert self.data_for_post, "Please override the class attribute 'data_for_post' in a test case which uses this mixin."

    def setUp(self) -> None:
        self.client_user_1 = APIClient()
        self.client_user_1.force_authenticate(user=self.user_model.objects.get(username='user1'))
        self.client_user_2 = APIClient()
        self.client_user_2.force_authenticate(user=self.user_model.objects.get(username='user2'))

    def test_list_access_control(self):
        # Post 1 data via user_1 first.
        response = self.client_user_1.post(path=self.base_url, data=self.data_for_post, format='json')
        assert response.status_code == status.HTTP_201_CREATED

        # Then, check if the only user_1 can see posted data in the list view.
        response_1 = self.client_user_1.get(path=self.base_url, format='json')
        response_2 = self.client_user_2.get(path=self.base_url, format='json')
        self.assertEqual(status.HTTP_200_OK, response_1.status_code)
        self.assertEqual(status.HTTP_200_OK, response_2.status_code)
        self.assertEqual(1, len(response_1.data['results']))
        self.assertEqual(0, len(response_2.data['results']))

    def test_detail_access_control(self):
        # Post 1 data via user_1 first.
        response = self.client_user_1.post(path=self.base_url, data=self.data_for_post, format='json')
        assert response.status_code == status.HTTP_201_CREATED

        # Set the resource ID for this test.
        resource_id = response.data['id']

        # Check if the only user_1 can access the created resource.
        response_1 = self.client_user_1.get(path=f'{self.base_url}/{resource_id}', format='json')
        response_2 = self.client_user_2.get(path=f'{self.base_url}/{resource_id}', format='json')
        self.assertEqual(status.HTTP_200_OK, response_1.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response_2.status_code)


class CommentApiTestMixin(object):

    fixtures = None  # Specify fixtures of users, all related resources, and comments.

    comment_id = None
    related_resource_id = None  # Correspond to foreign key value of the comment.
    other_users_comment_id = None  # Specify the comment which created_user is not the test user.

    base_url = None  # Base path for the comment API.
    resource_validator_class = None  # Specify Schematics validator class.

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_comment_list(self):
        response = self.client.get(path=self.base_url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        for data in response.data:
            self.resource_validator_class(data).validate()

        self.assertEqual(True, response.data[0].get('is_important'))
        self.assertEqual(False, response.data[-1].get('is_important'))

    def test_post_comment(self):
        new_content = "A new comment"
        is_important = True

        response = self.client.post(path=self.base_url, data={'content': new_content, 'is_important': is_important})
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.resource_validator_class(response.data).validate()
        self.assertEqual(response.data.get('content'), new_content)
        self.assertEqual(response.data.get('is_important'), is_important)
        self.assertEqual(response.data.get('created_user__name'), TEST_USER_NAME)

    def test_update_comment(self):
        new_content = "Updated."
        is_important = False
        response = self.client.patch(path=f'{self.base_url}/{self.comment_id}', data={'content': new_content, 'is_important': is_important})
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.resource_validator_class(response.data).validate()
        self.assertEqual(response.data.get('content'), new_content)
        self.assertEqual(response.data.get('is_important'), is_important)

    def test_delete_comment(self):
        response = self.client.delete(path=f'{self.base_url}/{self.comment_id}')
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)

    def test_update_another_user_comment(self):
        response = self.client.patch(path=f'{self.base_url}/{self.other_users_comment_id}', data={})
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_delete_another_user_comment(self):
        response = self.client.delete(path=f'{self.base_url}/{self.other_users_comment_id}')
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)


class CsvApiTestMixin(object):
    _url = None
    _EXPECTED_HEADER = None
    _EXPECTED_FIRST_FIELDS = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not (self._url and self._EXPECTED_HEADER):
            raise ValueError('Please override all class attributes of this mixin on a concrete TestCase.')

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_csv(self):
        response = self.client.get(path=f'{self._url}')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_type = type(response)
        if response_type is HttpResponse:
            csv_body = response.content.decode(encoding='utf-8')
            rows = csv_body.splitlines()
            header = rows[0].split(',')
            self.assertEqual(header, self._EXPECTED_HEADER)

            entries = rows[1:]
            for index, entry in enumerate(entries):
                fields = entry.split(',')
                self.assertEqual(len(fields), len(self._EXPECTED_HEADER))
                if index == 0 and self._EXPECTED_FIRST_FIELDS is not None:
                    self.assertEqual(fields, self._EXPECTED_FIRST_FIELDS)

        elif StreamingHttpResponse:
            rows = list(response.streaming_content)
            header = rows[0].decode('utf-8').replace('\r\n', '').split(',')
            self.assertEqual(header, self._EXPECTED_HEADER)

            entries = rows[1:]
            for index, entry in enumerate(entries):
                fields = entry.decode('utf-8').replace('\r\n', '').split(',')
                self.assertEqual(len(fields), len(self._EXPECTED_HEADER))
                if index == 0 and self._EXPECTED_FIRST_FIELDS is not None:
                    self.assertEqual(fields, self._EXPECTED_FIRST_FIELDS)

class MultiTenancySetupMixin(object):

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company_1 = CompanyFactory(id=COMPANY_1_ID_FOR_MULTI_TENANCY_TEST, name='Tenant A')
        cls.company_2 = CompanyFactory(id=COMPANY_2_ID_FOR_MULTI_TENANCY_TEST, name='Tenant B')
        cls.user_role_admin = UserRoleFactory(id=1, name='admin', order=1)
        cls.user1 = AppUserFactory(company=cls.company_1, email='user@tenantA.com', username='user@tenantA.com', user_role=cls.user_role_admin)
        cls.user2 = AppUserFactory(company=cls.company_2, email='user@tenantB.com', username='user@tenantB.com', user_role=cls.user_role_admin)
        cls.client_1 = APIClient()
        cls.client_1.force_authenticate(user=cls.user1)
        cls.client_2 = APIClient()
        cls.client_2.force_authenticate(user=cls.user2)

        # Note: Create test data related to test target using above companies and users.
        # Warning: You must define test data on following class attributes.
        # Mandatory.
        cls.resource_id_for_tenant_1 = None  # Override this in a child class.
        cls.resource_id_for_tenant_2 = None  # Override this in a child class.
        # Optional.
        cls.post_data_for_tenant_1 = None  # Override this in a child class.
        cls.post_data_for_tenant_2 = None  # Override this in a child class.
        cls.patch_data_for_tenant_1 = None  # Override this in a child class.
        cls.patch_data_for_tenant_2 = None  # Override this in a child class.


class MultiTenancyTestMixin(MultiTenancySetupMixin):
    """This test case extend the test case to check data separation between tenants (Companies) """

    base_url = None  # Override by child class. ex: '/your_app_name/resource_url'
    test_patterns = ('list', 'post', 'get', 'patch', 'delete')

    def __init__(self, *args, **kwargs):
        assert self.base_url, "Please define the base path of target resources."
        super().__init__(*args, **kwargs)

    def test_list_data_separation(self):
        """This test checks data separation between different company's resource.
        Note: We need to pass basic CRUD operations to execute this test correctly.
        """
        if 'list' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

        # Check if a user can only see data that belong to the same tenant.
        response1 = self.client_1.get(path=self.base_url, format='json')
        response2 = self.client_2.get(path=self.base_url, format='json')
        self.assertEqual(status.HTTP_200_OK, response1.status_code)
        self.assertEqual(status.HTTP_200_OK, response2.status_code)
        self.assertEqual(1, len(response1.data['results']))
        self.assertEqual(1, len(response2.data['results']))

    def test_get_data_separation(self):
        if 'get' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

        # Check all test data is defined correctly.
        if not (self.resource_id_for_tenant_1 and self.resource_id_for_tenant_2):
            raise NotImplementedError(
                'You need to define 2 ids of resources to class attribute to execute get test.')

        # Check if a user can only see data that belong to the same tenant.
        response1 = self.client_1.get(path=f'{self.base_url}/{self.resource_id_for_tenant_1}', format='json')
        response2 = self.client_2.get(path=f'{self.base_url}/{self.resource_id_for_tenant_2}', format='json')
        self.assertEqual(status.HTTP_200_OK, response1.status_code)
        self.assertEqual(status.HTTP_200_OK, response2.status_code)

        # Check if a user can NOT see data that belongs to other tenants.
        response3 = self.client_1.get(path=f'{self.base_url}/{self.resource_id_for_tenant_2}', format='json')
        response4 = self.client_2.get(path=f'{self.base_url}/{self.resource_id_for_tenant_1}', format='json')
        self.assertEqual(status.HTTP_404_NOT_FOUND, response3.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response4.status_code)

    def test_post_data_separation(self):
        if 'post' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

        # Check if all test data is defined correctly.
        if not (self.post_data_for_tenant_1 and self.post_data_for_tenant_2):
            raise NotImplementedError(
                'You need to define 2 test data of resources to class attribute to execute a post test.')

        # Check if two similar data do not conflict with each other because those 2 are separated by the tenant barrier.
        response1 = self.client_1.post(path=self.base_url, data=self.post_data_for_tenant_1, format='json')
        response2 = self.client_2.post(path=self.base_url, data=self.post_data_for_tenant_2, format='json')
        self.assertEqual(status.HTTP_201_CREATED, response1.status_code)
        self.assertEqual(status.HTTP_201_CREATED, response2.status_code)

        # Check if a user can NOT see the created data that belongs to other tenants.
        # We need to do this to make sure that 2 created resources are separated by the tenant barrier.
        created_resource_id_1 = response1.data["id"]
        created_resource_id_2 = response2.data["id"]

        response3 = self.client_1.get(path=f'{self.base_url}/{created_resource_id_2}', format='json')
        response4 = self.client_2.get(path=f'{self.base_url}/{created_resource_id_1}', format='json')
        self.assertEqual(status.HTTP_404_NOT_FOUND, response3.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response4.status_code)

    def test_patch_data_separation(self):
        if 'patch' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

        # Check if all test data is defined correctly.
        if not (self.resource_id_for_tenant_1 and self.resource_id_for_tenant_2):
            raise NotImplementedError(
                'You need to define 2 ids of resources to class attribute to execute a patch test.')
        if not (self.patch_data_for_tenant_1 and self.patch_data_for_tenant_2):
            raise NotImplementedError(
                'You need to define 2 test data of resources to class attribute to execute a patch test.')

        # Check if a user can NOT patch data that belongs to other tenants.
        response1 = self.client_1.patch(path=f'{self.base_url}/{self.resource_id_for_tenant_2}', data=self.patch_data_for_tenant_1, format='json')
        response2 = self.client_2.patch(path=f'{self.base_url}/{self.resource_id_for_tenant_1}', data=self.patch_data_for_tenant_2, format='json')
        self.assertEqual(status.HTTP_404_NOT_FOUND, response1.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response2.status_code)

        # Check if two similar data do not conflict with each other because those 2 are separated by the tenant barrier.
        response3 = self.client_1.patch(path=f'{self.base_url}/{self.resource_id_for_tenant_1}', data=self.patch_data_for_tenant_1, format='json')
        response4 = self.client_2.patch(path=f'{self.base_url}/{self.resource_id_for_tenant_2}', data=self.patch_data_for_tenant_2, format='json')
        self.assertEqual(status.HTTP_200_OK, response3.status_code)
        self.assertEqual(status.HTTP_200_OK, response4.status_code)

    def test_delete_data_separation(self):
        if 'delete' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

        if not (self.resource_id_for_tenant_1 and self.resource_id_for_tenant_2):
            raise NotImplementedError(
                'You need to define 2 ids of resources to class attribute to execute a delete test.')

        # Check if a user can NOT delete data that belongs to other tenants.
        response1 = self.client_1.delete(path=f'{self.base_url}/{self.resource_id_for_tenant_2}', format='json')
        response2 = self.client_2.delete(path=f'{self.base_url}/{self.resource_id_for_tenant_1}', format='json')
        self.assertEqual(status.HTTP_404_NOT_FOUND, response1.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response2.status_code)

        # Check if a user can only delete data that belongs to same tenant.
        response3 = self.client_1.delete(path=f'{self.base_url}/{self.resource_id_for_tenant_1}', format='json')
        response4 = self.client_2.delete(path=f'{self.base_url}/{self.resource_id_for_tenant_2}', format='json')
        self.assertEqual(status.HTTP_204_NO_CONTENT, response3.status_code)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response4.status_code)


class ActionViewMTMixin(MultiTenancySetupMixin):

    base_url = None
    action_name = ''
    action_types = ('get', 'post')
    success_code_for_post = status.HTTP_200_OK  # Some actions returns 201, so override if necessary.

    url_format = '{0}/{1}/{2}'  # {0} will be replaced by a base_url, {1}: resource id, {2}: action_name

    def _generate_url(self, resource_id):
        url = self.url_format.format(self.base_url, resource_id, self.action_name)
        return url

    def test_post_separation(self):
        if 'post' not in self.action_types:
            self.skipTest(reason='Not defined in cls.action_types')

        # Check if the user can execute action to a resource that belongs to the same tenant of the user.
        response1 = self.client_1.post(
            path=self._generate_url(resource_id=self.resource_id_for_tenant_1),
            format='json'
        )
        response2 = self.client_2.post(
            path=self._generate_url(resource_id=self.resource_id_for_tenant_2),
            format='json'
        )

        self.assertEqual(self.success_code_for_post, response1.status_code)
        self.assertEqual(self.success_code_for_post, response2.status_code)

        # Check if a user can NOT do action toward the resource that belongs to other tenants.
        response3 = self.client_2.post(
            path=self._generate_url(resource_id=self.resource_id_for_tenant_1),
            format='json'
        )
        response4 = self.client_1.post(
            path=self._generate_url(resource_id=self.resource_id_for_tenant_2),
            format='json'
        )

        self.assertEqual(status.HTTP_404_NOT_FOUND, response3.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response4.status_code)

    def test_get_separation(self):
        if 'get' not in self.action_types:
            self.skipTest(reason='Not defined in cls.action_types')

        # Check if the user can execute action to a resource that belongs to the same tenant of the user.
        response1 = self.client_1.get(
            path=self._generate_url(resource_id=self.resource_id_for_tenant_1),
            format='json'
        )
        response2 = self.client_2.get(
            path=self._generate_url(resource_id=self.resource_id_for_tenant_2),
            format='json'
        )

        self.assertEqual(status.HTTP_200_OK, response1.status_code)
        self.assertEqual(status.HTTP_200_OK, response2.status_code)

        response3 = self.client_1.get(
            path=self._generate_url(resource_id=self.resource_id_for_tenant_2),
            format='json'
        )
        response4 = self.client_2.get(
            path=self._generate_url(resource_id=self.resource_id_for_tenant_1),
            format='json'
        )
        self.assertEqual(status.HTTP_404_NOT_FOUND, response3.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response4.status_code)


class ReferenceViewMultiTenancyMixin(MultiTenancySetupMixin):
    """A mixin for Multi-tenancy tests of APIs
    Note: This mixin supports API endpoints which have rotated URLs like: /app_staffing/contacts/{uuid}/preference
    """

    base_url = None
    suffix = ''
    url_format = '{0}/{1}/{2}'  # {0} will be replaced by a base_url, {1}: resource id, {2}: resource suffix
    test_patterns = ('list', 'get', 'post', 'patch', 'delete')
    is_paginated = True  # Override this to False if your list API does not support pagination.
    # Basically, test_patterns will be either of (list, post) or (get, patch, delete)

    def _generate_url(self, resource_id):
        url = self.url_format.format(self.base_url, resource_id, self.suffix)
        return url

    def test_list_data_separation(self):
        if 'list' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

        # Check if a user can only see data that belong to the same tenant.
        response1 = self.client_1.get(path=self._generate_url(resource_id=self.resource_id_for_tenant_1), format='json')
        response2 = self.client_2.get(path=self._generate_url(resource_id=self.resource_id_for_tenant_2), format='json')
        self.assertEqual(status.HTTP_200_OK, response1.status_code)
        self.assertEqual(status.HTTP_200_OK, response2.status_code)
        if self.is_paginated:
            self.assertEqual(1, len(response1.data['results']))
            self.assertEqual(1, len(response2.data['results']))
        else:
            self.assertEqual(1, len(response1.data))
            self.assertEqual(1, len(response2.data))

    def test_post_data_separation(self):
        if 'post' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

        # Check if the user can NOT create related resources of the resource that belongs to other tenants.
        response1 = self.client_1.post(
            path=self._generate_url(resource_id=self.resource_id_for_tenant_2),
            format='json',
            data={}
        )
        response2 = self.client_2.post(
            path=self._generate_url(resource_id=self.resource_id_for_tenant_1),
            format='json',
            data={}
        )
        self.assertEqual(status.HTTP_404_NOT_FOUND, response1.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response2.status_code)

    def test_get_data_separation(self):
        if 'get' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

        # Check if a user can only see data that belong to the same tenant.
        response1 = self.client_1.get(path=self._generate_url(resource_id=self.resource_id_for_tenant_1), format='json')
        response2 = self.client_2.get(path=self._generate_url(resource_id=self.resource_id_for_tenant_2), format='json')
        self.assertEqual(status.HTTP_200_OK, response1.status_code)
        self.assertEqual(status.HTTP_200_OK, response2.status_code)

        # Check if a user can NOT see data that belongs to other tenants.
        response3 = self.client_1.get(path=self._generate_url(resource_id=self.resource_id_for_tenant_2), format='json')
        response4 = self.client_2.get(path=self._generate_url(resource_id=self.resource_id_for_tenant_1), format='json')
        self.assertEqual(status.HTTP_404_NOT_FOUND, response3.status_code)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response4.status_code)

    def test_patch_data_separation(self):
        if 'patch' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

            # Check if the user can NOT patch related resources of the resource that belongs to other tenants.
            response1 = self.client_1.patch(
                path=self._generate_url(resource_id=self.resource_id_for_tenant_2),
                format='json',
                data={}
            )
            response2 = self.client_2.patch(
                path=self._generate_url(resource_id=self.resource_id_for_tenant_1),
                format='json',
                data={}
            )
            self.assertEqual(status.HTTP_404_NOT_FOUND, response1.status_code)
            self.assertEqual(status.HTTP_404_NOT_FOUND, response2.status_code)

    def test_delete_data_separation(self):
        if 'delete' not in self.test_patterns:
            self.skipTest(reason='Not defined in cls.test_patterns')

            # Check if the user can NOT delete related resources of the resource that belongs to other tenants.
            response1 = self.client_1.delete(
                path=self._generate_url(resource_id=self.resource_id_for_tenant_2),
            )
            response2 = self.client_2.delete(
                path=self._generate_url(resource_id=self.resource_id_for_tenant_1),
            )
            self.assertEqual(status.HTTP_404_NOT_FOUND, response1.status_code)
            self.assertEqual(status.HTTP_404_NOT_FOUND, response2.status_code)


class CsvDownloadViewMultiTenancyTestMixin(MultiTenancySetupMixin):
    """This test case extend the test case to check data separation between tenants (Companies) """

    base_url = None  # Override by child class. ex: '/your_app_name/resource_url'

    def test_get_csv_entry_count(self):
        response1 = self.client_1.get(path=f'{self._base_url}')
        response2 = self.client_2.get(path=f'{self._base_url}')
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.status_code, status.HTTP_200_OK)

        responses = (response1, response2)
        for response in responses:
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            response_type = type(response)
            if response_type is HttpResponse:
                # Check if a user can only see data that belong to the same tenant.
                csv_body = response.content.decode(encoding='utf-8')
                # Note: If an organization Factory contains line breaks, this assertion will fail.
                rows = csv_body.splitlines()
                entries = rows[1:]
                self.assertEqual(1, len(entries))
            elif response_type is StreamingHttpResponse:
                # Check if a user can only see data that belong to the same tenant.
                rows = list(response.streaming_content)
                entries = rows[1:]
                for e in entries:
                    self.assertEqual(1, len(entries))
