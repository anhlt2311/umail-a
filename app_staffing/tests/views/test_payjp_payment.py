from app_staffing.models.addon import Addon
from app_staffing.models.organization import CompanyAttribute
from app_staffing.models.purchase_history import PurchaseHistory
from app_staffing.models.user import User
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse

from django.conf import settings
from app_staffing.models import Plan, PlanPaymentError
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD
from app_staffing.tests.helpers.mock_payjp_api import MockingPayjpAPIHelper
import app_staffing.utils.payment
from datetime import datetime
from dateutil.relativedelta import relativedelta
import payjp
from unittest.mock import Mock, patch, MagicMock
import pytz
from django.core import mail

class PayjpPaymentListViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/payjp/company.json',
        'app_staffing/tests/fixtures/payjp/users.json',
        'app_staffing/tests/fixtures/payjp/plan.json',
        'app_staffing/tests/fixtures/addon_datas.json',
    ]
    COMPANY_ID = '3efd25d0-989a-4fba-aa8d-b91708760eb9'
    CARD = {
        'number' : '4242424242424242',
        'cvc' : '123',
        'exp_month' : '2',
        'exp_year' : '2024',
        'name': 'POST MAN',
    }
    CARD2 = {
            'number' : '4012888888881881',
            'cvc' : '123',
            'exp_month' : '2',
            'exp_year' : '2024',
            'name': 'POST MAN',
    }
    
    @classmethod
    def mock_Charge_all(cls, api_key=None, payjp_account=None, api_base=None, **params):
        cnt = 1
        data = [{
            'amount': 46000,
        }]

        return payjp.resource.Charge.construct_from({
            'count': cnt,
            'data': data
        }, 'mock_api_key')
        
    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all)
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.ListObject.retrieve", MockingPayjpAPIHelper.mock_ListObject_retrieve)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Customer.save", MockingPayjpAPIHelper.mock_Customer_save)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    @patch("payjp.resource.Card.save", MockingPayjpAPIHelper.mock_Card_save)
    @patch("payjp.resource.Card.delete", MockingPayjpAPIHelper.mock_Card_delete)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def test_get_post_patch(self):
        payjp.api_key = settings.PAYJP_API_KEY
        url = api_reverse('payjp_payment')

        # GET(run create customer)
        response_get = self.client.get(url)
        self.assertEqual(response_get.status_code, status.HTTP_200_OK)

        # POST
        res_post_token = payjp.Token.create(
            card=self.CARD,
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        post_data = { 'token': res_post_token['id'] }
        response_post = self.client.post(url, data=post_data)
        self.assertEqual(response_post.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(['apiuser@example.com'], email.to)
        self.assertEqual('[コモレビ]お支払い情報更新のご連絡', email.subject)
        self.assertIn('お支払い情報が更新されましたのでご連絡いたします。', email.message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, email.message().as_string())

        # GET(run retrieve customer)
        response_get = self.client.get(url)
        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        self.assertTrue(response_get.data.data[0].isMainCard)

        # PATCH
        patch_data = {'card_id': response_get.data['data'][0]['id'], 'defaultCardId': response_get.data['data'][0]['id']}
        response_patch = self.client.patch(url, data=patch_data)
        self.assertEqual(response_patch.status_code, status.HTTP_204_NO_CONTENT)

        customer = payjp.Customer.retrieve(self.COMPANY_ID)
        customer.delete()

    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all)
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def test_same_card_post_again(self):
        payjp.api_key = settings.PAYJP_API_KEY
        url = api_reverse('payjp_payment')
        response_get = self.client.get(url)
        self.assertEqual(response_get.status_code, status.HTTP_200_OK)
        res_post_token = payjp.Token.create(
            card=self.CARD,
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        post_data = { 'token': res_post_token['id'] }
        response_post = self.client.post(url, data=post_data)
        self.assertEqual(response_post.status_code, status.HTTP_201_CREATED)
        res_post_token = payjp.Token.create(
            card=self.CARD2,
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        post_data = { 'token': res_post_token['id'] }
        response_post = self.client.post(url, data=post_data)
        self.assertEqual(response_post.status_code, status.HTTP_201_CREATED)
        res_post_token = payjp.Token.create(
            card=self.CARD,
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        post_data = { 'token': res_post_token['id'] }
        response_post = self.client.post(url, data=post_data)
        self.assertEqual(response_post.status_code, status.HTTP_201_CREATED)
        customer = payjp.Customer.retrieve(self.COMPANY_ID)
        customer.delete()

    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all)
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def test_update_and_charge(self):
        now = datetime.now()
        plan = Plan.objects.create(
            company_id=self.COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=now.date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.COMPANY_ID,
            user_registration_limit=10,
        )
        Addon.objects.create(
            company_id=self.COMPANY_ID,
            addon_master_id=1,
        )
        Addon.objects.create(
            company_id=self.COMPANY_ID,
            addon_master_id=2,
        )
        PlanPaymentError.objects.create(company_id=self.COMPANY_ID, plan_id=plan.id)
        payjp.api_key = settings.PAYJP_API_KEY
        url = api_reverse('payjp_payment')
        res_post_token = payjp.Token.create(
            card=self.CARD,
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        post_data = { 'token': res_post_token['id'] }
        response_post = self.client.post(url, data=post_data)
        self.assertEqual(response_post.status_code, status.HTTP_201_CREATED)

        charges = payjp.Charge.all(customer=self.COMPANY_ID, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        # 30000(プランの料金) + 1000(500円のアドオン2つ分の料金) + 15000(プランのデフォルトユーザ5人分 + 3000円/人 * 5人分の料金)
        self.assertEqual(charges['data'][0]['amount'], 46000)

        # 新しいプランが作られていることを確認
        plans = Plan.objects.filter(company_id=self.COMPANY_ID, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, (now + relativedelta(months=1)).month)
        self.assertEqual(plans[0].payment_date.day, (now + relativedelta(months=1)).day)

        # PaymentHistoryが作られていることを確認する
        purchase_histories = PurchaseHistory.objects.filter(company_id=self.COMPANY_ID)
        self.assertEqual(len(purchase_histories), 1)
        # 30000(プランの料金) + 1000(500円のアドオン2つ分の料金) + 15000(プランのデフォルトユーザ5人分 + 3000円/人 * 5人分の料金) * tax(10%)
        self.assertEqual(purchase_histories[0].price, 50600)
        self.assertEqual(purchase_histories[0].period_start, now.date())
        self.assertEqual(purchase_histories[0].period_end, plans[0].expiration_date)

        self.assertEqual(len(mail.outbox), 2)
        user = User.objects.get(username=TEST_USER_NAME)

        self.assertEqual(['apiuser@example.com'], mail.outbox[0].to)
        self.assertEqual('[コモレビ]お支払い情報更新のご連絡', mail.outbox[0].subject)
        self.assertIn('お支払い情報が更新されましたのでご連絡いたします。', mail.outbox[0].message().as_string())
        self.assertIn(user.display_name, mail.outbox[0].message().as_string())

        self.assertEqual(['apiuser@example.com'], mail.outbox[1].to)
        self.assertEqual('[コモレビ]お支払い完了のご連絡', mail.outbox[1].subject)
        self.assertIn('今月分のご利用料金の決済が完了いたしましたのでご連絡いたします。', mail.outbox[1].message().as_string())
        self.assertIn(user.display_name, mail.outbox[1].message().as_string())

        customer = payjp.Customer.retrieve(self.COMPANY_ID)        
        customer.delete()

    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all_with_backup)
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    @patch("payjp.resource.Charge.create", MagicMock(side_effect=[Exception("DUMMY"), None]))
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def test_update_and_charge_with_backup_card(self):
        now = datetime.now()
        plan = Plan.objects.create(
            company_id=self.COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=now.date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.COMPANY_ID,
            user_registration_limit=10,
        )
        Addon.objects.create(
            company_id=self.COMPANY_ID,
            addon_master_id=1,
        )
        Addon.objects.create(
            company_id=self.COMPANY_ID,
            addon_master_id=2,
        )
        PlanPaymentError.objects.create(company_id=self.COMPANY_ID, plan_id=plan.id)
        payjp.api_key = settings.PAYJP_API_KEY
        url = api_reverse('payjp_payment')
        res_post_token = payjp.Token.create(
            card=self.CARD,
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        post_data = { 'token': res_post_token['id'] }
        response_post = self.client.post(url, data=post_data)
        self.assertEqual(response_post.status_code, status.HTTP_201_CREATED)

        charges = payjp.Charge.all(customer=self.COMPANY_ID, since=int(now.timestamp()))
        self.assertEqual(charges['count'], 1)
        # 30000(プランの料金) + 1000(500円のアドオン2つ分の料金) + 15000(プランのデフォルトユーザ5人分 + 3000円/人 * 5人分の料金)
        self.assertEqual(charges['data'][0]['amount'], 46000)

        # 新しいプランが作られていることを確認
        plans = Plan.objects.filter(company_id=self.COMPANY_ID, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, (now + relativedelta(months=1)).month)
        self.assertEqual(plans[0].payment_date.day, (now + relativedelta(months=1)).day)

        # PaymentHistoryが作られていることを確認する
        purchase_histories = PurchaseHistory.objects.filter(company_id=self.COMPANY_ID)
        self.assertEqual(len(purchase_histories), 1)
        # 30000(プランの料金) + 1000(500円のアドオン2つ分の料金) + 15000(プランのデフォルトユーザ5人分 + 3000円/人 * 5人分の料金) * tax(10%)
        self.assertEqual(purchase_histories[0].price, 50600)
        self.assertEqual(purchase_histories[0].period_start, now.date())
        self.assertEqual(purchase_histories[0].period_end, plans[0].expiration_date)

        self.assertEqual(len(mail.outbox), 3)
        user = User.objects.get(username=TEST_USER_NAME)

        self.assertEqual(['apiuser@example.com'], mail.outbox[0].to)
        self.assertEqual('[コモレビ]お支払い情報更新のご連絡', mail.outbox[0].subject)
        self.assertIn('お支払い情報が更新されましたのでご連絡いたします。', mail.outbox[0].message().as_string())
        self.assertIn(user.display_name, mail.outbox[0].message().as_string())

        self.assertEqual(['apiuser@example.com'], mail.outbox[1].to)
        self.assertEqual('[コモレビ]お支払い完了のご連絡', mail.outbox[1].subject)
        self.assertIn('今月分のご利用料金の決済が完了いたしましたのでご連絡いたします。', mail.outbox[1].message().as_string())
        self.assertIn(user.display_name, mail.outbox[1].message().as_string())

        self.assertEqual(['apiuser@example.com'], mail.outbox[2].to)
        self.assertEqual('[コモレビ] 予備クレジットカードでの決済完了のご連絡', mail.outbox[2].subject)
        self.assertIn('予備クレジットカードでの決済が行われましたのでご連絡いたします。', mail.outbox[2].message().as_string())
        self.assertIn(user.display_name, mail.outbox[2].message().as_string())

        customer = payjp.Customer.retrieve(self.COMPANY_ID)        
        customer.delete()

    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all)
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    @patch("payjp.resource.Charge.create", MagicMock(side_effect=[Exception("DUMMY")]))
    @patch("payjp.resource.Charge.all", mock_Charge_all)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def test_update_and_charge_failed(self):
        now = datetime.now()
        plan = Plan.objects.create(
            company_id=self.COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=now.date(),
        )
        CompanyAttribute.objects.create(
            company_id=self.COMPANY_ID,
            user_registration_limit=10,
        )
        Addon.objects.create(
            company_id=self.COMPANY_ID,
            addon_master_id=1,
        )
        Addon.objects.create(
            company_id=self.COMPANY_ID,
            addon_master_id=2,
        )
        PlanPaymentError.objects.create(company_id=self.COMPANY_ID, plan_id=plan.id)
        payjp.api_key = settings.PAYJP_API_KEY
        url = api_reverse('payjp_payment')
        res_post_token = payjp.Token.create(
            card=self.CARD,
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        post_data = { 'token': res_post_token['id'] }
        response_post = self.client.post(url, data=post_data)
        self.assertEqual(response_post.status_code, status.HTTP_400_BAD_REQUEST)

        # 新しいプランが作られていないことを確認
        plans = Plan.objects.filter(company_id=self.COMPANY_ID, is_active=True)
        self.assertEqual(len(plans), 1)
        self.assertEqual(plans[0].payment_date.month, now.month)
        self.assertEqual(plans[0].payment_date.day, now.day)

        # PaymentHistoryが作られていないことを確認する
        purchase_histories = PurchaseHistory.objects.filter(company_id=self.COMPANY_ID)
        self.assertEqual(len(purchase_histories), 0)

        self.assertEqual(len(mail.outbox), 2)
        user = User.objects.get(username=TEST_USER_NAME)

        self.assertEqual(['apiuser@example.com'], mail.outbox[0].to)
        self.assertEqual('[コモレビ]お支払い情報更新のご連絡', mail.outbox[0].subject)
        self.assertIn('お支払い情報が更新されましたのでご連絡いたします。', mail.outbox[0].message().as_string())
        self.assertIn(user.display_name, mail.outbox[0].message().as_string())

        self.assertEqual(['apiuser@example.com'], mail.outbox[1].to)
        self.assertEqual('[コモレビ]お支払い失敗のご連絡', mail.outbox[1].subject)
        self.assertIn('お支払いの処理に失敗いたしましたのでご連絡いたします。', mail.outbox[1].message().as_string())
        self.assertIn(user.display_name, mail.outbox[1].message().as_string())

        customer = payjp.Customer.retrieve(self.COMPANY_ID)        
        customer.delete()

    def test_get_with_exception(self):
        url = api_reverse('payjp_payment')
        mock = Mock(side_effect=[Exception('Boom!')])
        with patch('payjp.resource.ListObject.all', mock):
            response_get = self.client.get(url)
        self.assertEqual(response_get.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_post_invalid_card_token(self):
        url = api_reverse('payjp_payment')
        post_data = { 'token': 'this_is_invalid_token' }
        mock = Mock(side_effect=[Exception('Boom!')])
        with patch('payjp.resource.CreateableAPIResource.create', mock):
            response_post = self.client.post(url, data=post_data)
        self.assertEqual(response_post.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_patch_invalid_card_id(self):
        url = api_reverse('payjp_payment')
        patch_data = {'defaultCardId': 'this_is_invalid_card_id' }
        mock = Mock(side_effect=[Exception('Boom!')])
        with patch('payjp.resource.APIResource.retrieve', mock):
            response_patch = self.client.patch(url, data=patch_data)
        self.assertEqual(response_patch.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_delete_invalid_card_id(self):
        url = api_reverse('payjp_payment')
        delete_data = { 'card_id': 'this_is_invalid_card_id' }
        mock = Mock(side_effect=[Exception('Boom!')])
        with patch('payjp.resource.APIResource.retrieve', mock):
            response_delete = self.client.delete(url, data=delete_data)
        self.assertEqual(response_delete.status_code, status.HTTP_400_BAD_REQUEST)

    def test_unauth_user_cannot_get(self):
        self.client.logout()

        payjp.api_key = settings.PAYJP_API_KEY
        url = api_reverse('payjp_payment')

        response_get = self.client.get(url)
        self.assertEqual(response_get.status_code, status.HTTP_401_UNAUTHORIZED)

    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def test_unauth_user_cannot_post(self):
        self.client.logout()

        payjp.api_key = settings.PAYJP_API_KEY
        url = api_reverse('payjp_payment')

        res_post_token = payjp.Token.create(
            card=self.CARD,
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        post_data = { 'token': res_post_token['id'] }
        response_post = self.client.post(url, data=post_data)
        self.assertEqual(response_post.status_code, status.HTTP_401_UNAUTHORIZED)
