from schematics.types import BooleanType, IntType, StringType, EmailType, UUIDType, UTCDateTimeType
from schematics import Model


# Validators for API View tests.
class IsUser(Model):
    id = UUIDType(required=True)
    user_service_id = StringType(required=True)
    username = StringType(required=True)
    display_name = StringType(required=True)
    first_name = StringType(required=True)
    last_name = StringType(required=True)
    email = StringType(required=True)
    email_signature = StringType(required=True)
    last_login = StringType(required=False)  # Some user does not has this value.(e.g.:just created user)
    is_active = BooleanType(required=True)
    is_user_admin = BooleanType(required=True)
    editable = BooleanType(required=True)
    old_id = IntType(required=False)  # Need for migration process.
    role = StringType(required=True)
    tel1 = StringType(required=False)
    tel2 = StringType(required=False)
    tel3 = StringType(required=False)
    registed_at = UTCDateTimeType(required=False)
    modified_user__name = StringType(required=False)
    modified_time = UTCDateTimeType(required=False)
    avatar = StringType(required=False)


class IsUserProfile(Model):
    user_service_id = StringType(required=True)
    first_name = StringType(required=True)
    last_name = StringType(required=True)
    email = EmailType(required=True)
    email_signature = StringType(required=True)
    tel1 = StringType(required=False)
    tel2 = StringType(required=False)
    tel3 = StringType(required=False)
    role = StringType(required=True)
    modified_user__name = StringType(required=False)
    modified_time = UTCDateTimeType(required=False)
    avatar = StringType(required=False)
