from schematics.types import BaseType, IntType, StringType, ListType, DictType
from schematics import Model


# Validators for API View tests.
class IsPagenatedResponse(Model):
    count = IntType(required=True)
    results = ListType(required=True, field=DictType(BaseType))
    next = StringType(required=False)  # Return None when we reached the last page.
    previous = StringType(required=False)  # Return None when we see the first page.


class IsPageOutOfRangeResponse(Model):
    detail = StringType(required=True)
    lastPageNumber = IntType(required=True)
