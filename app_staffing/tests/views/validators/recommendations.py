from schematics.types import ListType, UUIDType
from schematics import Model


# Validators for API View tests.
class IsRecommendationResult(Model):
    items = ListType(required=True, field=UUIDType)
