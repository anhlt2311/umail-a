from schematics.types import BooleanType, IntType, StringType, EmailType, UUIDType, UTCDateTimeType, ListType
from schematics import Model


class IsContactPreferenceSummary(Model):
    id = UUIDType(required=True)
    name = StringType(required=True)
    email = EmailType(required=True)
    org_name = StringType(required=True)


class IsContactPreference(Model):
    contact = UUIDType(required=True)
    contact__last_name = StringType(required=True)
    contact__first_name = StringType(required=False)
    contact__display_name = StringType(required=True)
    contact__email = EmailType(required=True)
    contact__organization__name = StringType(required=True)
    contact__organization__is_blacklisted = BooleanType(required=True)
    wants_location_kanto_japan = BooleanType(required=True)
    wants_location_kansai_japan = BooleanType(required=True)
    created_user = UUIDType(required=True)
    created_user__name = StringType(required=True)
    created_user__avatar = StringType(required=False)
    created_time = UTCDateTimeType(required=True)
    modified_user = UUIDType(required=True)
    modified_user__name = StringType(required=True)
    modified_time = UTCDateTimeType(required=True)
    deleted_at = UTCDateTimeType(required=False)
    wants_location_hokkaido_japan = BooleanType(required=True)
    wants_location_touhoku_japan = BooleanType(required=True)
    wants_location_chubu_japan = BooleanType(required=True)
    wants_location_kyushu_japan = BooleanType(required=True)
    wants_location_other_japan = BooleanType(required=True)
    wants_location_chugoku_japan = BooleanType(required=True)
    wants_location_shikoku_japan = BooleanType(required=True)
    wants_location_toukai_japan = BooleanType(required=True)
    job_koyou_proper = BooleanType(required=True)
    job_koyou_free = BooleanType(required=True)
    job_syouryu = IntType(required=False)
    personnel_syouryu = IntType(required=False)
    personnel_country_japan = BooleanType(required=True)
    personnel_country_other = BooleanType(required=True)
    has_send_guide = BooleanType(required=True)
