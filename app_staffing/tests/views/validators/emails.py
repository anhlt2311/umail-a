from schematics.types import (
    BaseType,
    BooleanType,
    IntType,
    StringType,
    ListType,
    ModelType,
    UUIDType,
    UTCDateTimeType,
    EmailType,)
from schematics import Model

from app_staffing.utils.schematics.types import NoneType


# Validators for API View tests.
class EmailAttachmentSummary(Model):
    id = UUIDType(required=True)
    name = StringType(required=True)


class IsLabeledEmail(Model):
    id = UUIDType(required=True)
    mail_id = IntType(required=True)
    from_address = EmailType(required=True)
    subject = StringType(required=True)
    body = StringType(required=True)
    received_date = UTCDateTimeType(required=True)
    attachment_names = StringType(required=False)

    category = StringType(required=True)
    estimated_category = StringType(required=False)

    created_time = UTCDateTimeType(required=True)
    created_user = UUIDType(required=True)
    modified_time = UTCDateTimeType(required=True)
    modified_user = UUIDType(required=True)


class IsUnLabeledEmail(Model):
    id = UUIDType(required=True)
    mail_id = IntType(required=True)
    from_address = EmailType(required=True)
    subject = StringType(required=True)
    body = StringType(required=True)
    received_date = UTCDateTimeType(required=True)
    attachment_names = StringType(required=False)

    category = NoneType()
    estimated_category = StringType(required=False)

    created_time = UTCDateTimeType(required=True)
    created_user = UUIDType(required=True)
    modified_time = UTCDateTimeType(required=True)
    modified_user = UUIDType(required=True)


class IsEmail(Model):
    id = UUIDType(required=True)
    message_id = StringType(required=True)
    shared = BooleanType(required=True)
    from_address = EmailType(required=True)
    subject = StringType(required=True)
    text = StringType(required=True)
    html = StringType(required=True)
    sent_date = UTCDateTimeType(required=True)
    attachment_names = StringType(required=False)

    category = StringType(required=False)
    estimated_category = StringType(required=False)

    created_time = UTCDateTimeType(required=True)
    created_user = UUIDType(required=True)
    modified_time = UTCDateTimeType(required=True)
    modified_user = UUIDType(required=True)


class EmailComment(Model):
    id = UUIDType(required=True)
    content = StringType(required=True)


class SharedEmailTransferHistory(Model):
    id = UUIDType(required=True)
    email__subject = StringType()


class IsSummarizedEmail(Model):
    id = UUIDType(required=True)
    from_name = StringType(required=False)  # Nullable.
    from_address = EmailType(required=True)
    subject = StringType(required=True)
    text = StringType(required=True)
    sent_date = UTCDateTimeType(required=True)
    estimated_category = StringType(required=True)
    has_attachments = BooleanType(required=True)
    attachments = ListType(required=True, field=ModelType(EmailAttachmentSummary))
    staff_in_charge__name = StringType(required=False)
    comments = ListType(ModelType(EmailComment))
    histories = ListType(BaseType())


class IsEmailDetail(Model):
    id = UUIDType(required=True)
    message_id = StringType(required=True)
    shared = BooleanType(required=True)
    from_name = StringType(required=False)  # Nullable.
    from_address = EmailType(required=True)
    subject = StringType(required=True)
    text = StringType(required=True)
    html = StringType(required=True)
    sent_date = UTCDateTimeType(required=True)
    staff_in_charge__name = StringType(required=True)
    staff_in_charge__id = StringType(required=True)
    category = StringType(required=False)
    estimated_category = StringType(required=False)
    attachments = ListType(required=True, field=ModelType(EmailAttachmentSummary))


class IsEmailMonthlyRanking(Model):
    from_headers = ListType(field=StringType, required=True)
    counts = ListType(field=IntType, required=True)


class IsEmailRanking(Model):
    monthly = ModelType(IsEmailMonthlyRanking)


class IsEmailTrend(Model):
    labels = ListType(field=StringType, required=True)
    job_email_counts = ListType(field=IntType, required=True)
    personnel_email_counts = ListType(field=IntType, required=True)


class IsEmailReplyTrend(Model):
    labels = ListType(field=StringType, required=True)
    counts = ListType(field=IntType, required=True)


class IsEmailStats(Model):
    trend = ModelType(IsEmailTrend, required=True)
    ranking = ModelType(IsEmailRanking, required=True)
    reply = ModelType(IsEmailReplyTrend, required=True)
