from schematics import Model
from schematics.types import StringType, UUIDType, UTCDateTimeType, ListType, ModelType, BooleanType


class IsNotificationCondition(Model):
    extraction_type = StringType(required=True)
    target_type = StringType(required=True)
    condition_type = StringType(required=True)
    value = StringType(required=True)


class IsEmailNotificationRule(Model):
    id = UUIDType(required=True)
    name = StringType(required=True)
    master_rule = StringType(required=True)
    conditions = ListType(ModelType(IsNotificationCondition, required=True), required=True)
    created_user = UUIDType(required=True)
    created_user__name = StringType(required=True)
    created_time = UTCDateTimeType(required=True)
    modified_user = UUIDType(required=True)
    modified_user__name = StringType(required=True)
    modified_time = UTCDateTimeType(required=True)
    is_active = BooleanType(required=True)
