from schematics.types import BooleanType, StringType, ListType, ModelType,\
    UUIDType, UTCDateTimeType, IntType, DictType, BaseType
from schematics import Model


class IsScheduledEmailAttachment(Model):
    id = UUIDType(required=True)
    name = StringType(required=True)  # file name.
    attached_mail_id = UUIDType(required=True)
    gcp_link = StringType(required=False)
    status = IntType(required=True)
    file_type = IntType(required=True)


class IsScheduledEmailSummary(Model):
    id = UUIDType(required=True)
    sender = UUIDType(required=True)
    sender__name = StringType(required=True)
    subject = StringType(required=True)
    text = StringType(required=True)
    attachments = ListType(required=True, field=ModelType(IsScheduledEmailAttachment))
    status = StringType(required=True)
    date_to_send = UTCDateTimeType(required=False)
    sent_date = UTCDateTimeType(required=False)
    send_total_count = IntType(required=False)
    send_success_count = IntType(required=False)
    send_error_count = IntType(required=False)
    send_type = StringType(required=False)
    text_format = StringType(required=False)
    created_time = UTCDateTimeType(required=True)
    modified_time = UTCDateTimeType(required=True)
    open_ratio = IntType(required=False)
    open_count = IntType(required=False)
    password = StringType(required=False)
    expired_date = StringType(required=False)


class IsScheduledEmail(Model):
    id = UUIDType(required=True)
    sender = UUIDType(required=True)
    sender__name = StringType(required=True)
    subject = StringType(required=True)
    text = StringType(required=True)
    status = StringType(required=True)
    date_to_send = UTCDateTimeType(required=False)
    send_copy_to_sender = BooleanType(required=True)
    send_copy_to_share = BooleanType(required=True)
    sent_date = UTCDateTimeType(required=False)
    attachments = ListType(required=True, field=ModelType(IsScheduledEmailAttachment))
    target_contacts = ListType(required=True, field=UUIDType(required=True))

    created_user = UUIDType(required=True)
    created_user__name = StringType(required=True)
    created_user__avatar = StringType(required=False)
    created_time = UTCDateTimeType(required=True)
    modified_user = UUIDType(required=True)
    modified_user__name = StringType(required=True)
    modified_time = UTCDateTimeType(required=True)
    deleted_at = UTCDateTimeType(required=False)

    send_total_count = IntType(required=False)
    send_success_count = IntType(required=False)
    send_error_count = IntType(required=False)
    send_limit = IntType(required=True)
    send_type = StringType(required=False)
    search_condition = DictType(BaseType)
    text_format = StringType(required=False)
    open_count = IntType(required=False)

    scheduled_email_status = IntType(required=False)
    scheduled_email_send_type = IntType(required=False)
    file_type = IntType(required=False)
    password = StringType(required=False)
    expired_date = StringType(required=False)


class IsScheduledEmailPreview(Model):
    sender_email = StringType(required=True)
    subject = StringType(required=True)
    body = StringType(required=True)
    date_to_send = UTCDateTimeType(required=True)
    text_format = StringType(required=True)
    over_max_byte_size = BooleanType(required=True)
    password = StringType(required=False)
    url_list = ListType(required=False, field=StringType())
    file_type = IntType(required=False)
