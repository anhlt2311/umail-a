import datetime as dt
from uuid import uuid4

from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.conf import settings
from app_staffing.models.email import ScheduledEmail, ScheduledEmailAttachment, ScheduledEmailTarget
from app_staffing.models.organization import CompanyAttribute, Contact, Organization
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse

from app_staffing.models import Addon, Company, ScheduledEmailSetting, Plan, PurchaseHistory
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, DEFAULT_COMPANY_ID, API_USER_ID
from app_staffing.tests.helpers.mock_payjp_api import MockingPayjpAPIHelper
from app_staffing.utils.addon import comment_template_total_available_count, search_template_total_available_count, scheduled_email_limit_target_count, scheduled_email_max_byte_size_and_error_message
from django.conf import settings

from datetime import datetime

import payjp
import pytz
from unittest.mock import patch, MagicMock

from app_staffing.tests.factories.email import ScheduledEmailFactory
from django.core import mail

from django.test import override_settings

User = get_user_model()


class AddonPurchasedListAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json',
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    def setUp(self):
        company = User.objects.get(username=TEST_USER_NAME).company
        another_company = Company.objects.get(id='0639df05-8db5-40f2-a25d-6bcd3f8ae4ca')
        self.addon_1 = Addon.objects.create(
            company=company,
            addon_master_id=1,
        )
        self.addon_2 = Addon.objects.create(
            company=company,
            addon_master_id=2,
            expiration_time=None,
        )
        self.addon_3 = Addon.objects.create(
            company=another_company,
            addon_master_id=4,
            expiration_time=None,
        )
        Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=datetime(2021, 12, 21, 0, 0, 0, 0).astimezone(pytz.timezone(settings.TIME_ZONE)).date(),
        )
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get_purchased_addons(self):
        url = api_reverse('addon_purchase_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(len(data), 2)
        self.assertEqual(data[0]['next_payment_date'], datetime(2021, 12, 21, 0, 0, 0, 0).astimezone(pytz.timezone(settings.TIME_ZONE)).date())
        total_addons = Addon.objects.all()
        self.assertEqual(total_addons.count(), 3)

    def test_unauth_user_cannot_fetch_purchase_addons(self):
        self.client.logout()
        url = api_reverse('addon_purchase_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class AddonPushchaseAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/payjp/company.json',
        'app_staffing/tests/fixtures/payjp/users.json',
        'app_staffing/tests/fixtures/payjp/emails.json',
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    PAYJP_TEST_COMPANY_ID = '3efd25d0-989a-4fba-aa8d-b91708760eb9'

    @classmethod
    def mock_Charge_create_expired_card(cls, api_key=None, payjp_account=None, headers=None, **params):
        raise payjp.error.CardError('ERROR', None,'expired_card', json_body={'error':{'code':'expired_card'}})

    @classmethod
    def mock_Charge_create_card_declined(cls, api_key=None, payjp_account=None, headers=None, **params):
        raise payjp.error.CardError('ERROR', None,'card_declined', json_body={'error':{'code':'card_declined'}})

    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Customer.create", MockingPayjpAPIHelper.mock_Customer_create)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    def setUp(self):
        company = User.objects.get(username=TEST_USER_NAME).company
        another_company = Company.objects.get(id='0639df05-8db5-40f2-a25d-6bcd3f8ae4ca')
        self.addon_1 = Addon.objects.create(
            company=company,
            addon_master_id=1,
            expiration_time=dt.datetime.now() + dt.timedelta(days=7)
        )
        self.addon_2 = Addon.objects.create(
            company=company,
            addon_master_id=2,
            expiration_time=None,
        )
        self.addon_3 = Addon.objects.create(
            company=another_company,
            addon_master_id=4,
            expiration_time=None,
        )
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        payjp.api_key = settings.PAYJP_API_KEY

        try:
            customer = payjp.Customer.create(id=self.PAYJP_TEST_COMPANY_ID)
        except Exception:
            customer = payjp.Customer.retrieve(self.PAYJP_TEST_COMPANY_ID)

        res_post_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        customer.cards.create(card=res_post_token['id'])

        Plan.objects.create(
            company_id=self.PAYJP_TEST_COMPANY_ID,
            plan_master_id=1,
            is_active=True,
            payment_date=datetime.now().date(),
        )
    
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Customer.delete", MockingPayjpAPIHelper.mock_Customer_delete)
    def tearDown(self):
        payjp.api_key = settings.PAYJP_API_KEY
        customer = payjp.Customer.retrieve(self.PAYJP_TEST_COMPANY_ID)
        customer.delete()

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_purchase_addon(self):
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': 1,
            'expiration_time': dt.datetime.now() + dt.timedelta(weeks=4)
        }
        # 2回目の購入(上限: 3回) 初回実績はsetUpで生成
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.data
        self.assertEqual(post_data['addon_master_id'], data['addon_master_id'])
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 550)

        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual('[コモレビ]アドオン購入完了のご連絡', email.subject)
        self.assertIn('アドオンの購入が完了いたしましたのでご連絡いたします。', email.message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, email.message().as_string())

        # 3回目の購入(上限: 3回)
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(post_data['addon_master_id'], data['addon_master_id'])
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 2)

        # 4回目の購入(上限: 3回) 失敗する
        response = self.client.post(url, data=post_data)
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_purchase_addon_no_expiration_time(self):
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': 1,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.data
        self.assertEqual(post_data['addon_master_id'], data['addon_master_id'])
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 550)

    def test_purchase_addon_with_wrong_request_body(self):
        url = api_reverse('addon_purchase')
        post_data_1 = {
            'price': 30,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data_1)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        all_addons = Addon.objects.all()
        self.assertEqual(all_addons.count(), 3)
        post_data_2 = {
            'id': 3000,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data_2)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        all_addons = Addon.objects.all()
        self.assertEqual(all_addons.count(), 3)
        self.assertEqual(PurchaseHistory.objects.all().count(), 0)

    def test_unauth_user_cannot_purchase_addons(self):
        self.client.logout()
        url = api_reverse('addon_purchase')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(PurchaseHistory.objects.all().count(), 0)

    def test_wrong_addon_master_id(self):
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': 3000,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        all_addons = Addon.objects.all()
        self.assertEqual(all_addons.count(), 3)
        self.assertEqual(PurchaseHistory.objects.all().count(), 0)

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_use_remove_promotion_addon_master_id(self):
        ScheduledEmailSetting.objects.create(company_id=self.PAYJP_TEST_COMPANY_ID, use_remove_promotion=False)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_remove_promotion=False).exists())
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.REMOVE_PROMOTION_ADDON_MASTER_ID,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_remove_promotion=True).exists())
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 1100)

    def test_use_open_count_extra_period_addon(self):
        Addon.objects.create(
            company_id=self.PAYJP_TEST_COMPANY_ID,
            addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID,
            expiration_time=None,
        )
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 1100)

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_use_open_count_extra_period_addon(self):
        Addon.objects.create(
            company_id=self.PAYJP_TEST_COMPANY_ID,
            addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID,
            expiration_time=None,
        )
        ScheduledEmailSetting.objects.create(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count_extra_period=False)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count_extra_period=False).exists())
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.IS_OPEN_COUNT_EXTRA_PERIOD_AVAILABLE_ADDON_MASTER_ID,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count_extra_period=True).exists())
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 1100)
    
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_use_open_count_addon(self):
        ScheduledEmailSetting.objects.create(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count=False)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count=False).exists())
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count=True).exists())
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 4400)

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_use_attachment_max_size_addon_master_id(self):
        max_byte_size, max_byte_size_error_message = scheduled_email_max_byte_size_and_error_message(self.PAYJP_TEST_COMPANY_ID)
        self.assertEqual(max_byte_size, settings.SCHEDULED_EMAIL_MAX_BYTE_SIZE)
        self.assertEqual(max_byte_size_error_message, settings.MAX_BYTE_SIZE_ERROR_MESSAGE)

        ScheduledEmailSetting.objects.create(company_id=self.PAYJP_TEST_COMPANY_ID, use_attachment_max_size=False)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_attachment_max_size=False).exists())
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.SCHEDULED_EMAIL_BYTE_SIZE_ADDON_MASTER_ID,
                'expiration_time': ""
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_attachment_max_size=True).exists())
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 3300)

        max_byte_size, max_byte_size_error_message = scheduled_email_max_byte_size_and_error_message(self.PAYJP_TEST_COMPANY_ID)
        self.assertEqual(max_byte_size, settings.SCHEDULED_EMAIL_ADDON_MAX_BYTE_SIZE)
        self.assertEqual(max_byte_size_error_message, settings.MAX_BYTE_SIZE_WITH_ADDON_ERROR_MESSAGE)

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_purchase_search_template_addon(self):
        before_available_count = search_template_total_available_count(self.PAYJP_TEST_COMPANY_ID)
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.SEARCH_TEMPLATE_ADDON_ID,
            'expiration_time': dt.datetime.now() + dt.timedelta(weeks=4)
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.data
        self.assertEqual(post_data['addon_master_id'], data['addon_master_id'])
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        after_available_count = search_template_total_available_count(self.PAYJP_TEST_COMPANY_ID)
        self.assertEqual(before_available_count+5, after_available_count)

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_purchase_comment_template_addon(self):
        before_available_count = comment_template_total_available_count(self.PAYJP_TEST_COMPANY_ID)
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.COMMENT_TEMPLATE_ADDON_ID,
            'expiration_time': dt.datetime.now() + dt.timedelta(weeks=4)
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.data
        self.assertEqual(post_data['addon_master_id'], data['addon_master_id'])
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        after_available_count = comment_template_total_available_count(self.PAYJP_TEST_COMPANY_ID)
        self.assertEqual(before_available_count+5, after_available_count)

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_purchase_scheduled_email_target_count_addon(self):
        before_limit = scheduled_email_limit_target_count(self.PAYJP_TEST_COMPANY_ID)

        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID,
            'expiration_time': dt.datetime.now() + dt.timedelta(weeks=4)
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.data
        self.assertEqual(post_data['addon_master_id'], data['addon_master_id'])
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        after_limit = scheduled_email_limit_target_count(self.PAYJP_TEST_COMPANY_ID)
        self.assertEqual(before_limit+1000, after_limit)

    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", mock_Charge_create_expired_card)
    def test_expired_card(self):
        payjp.api_key = settings.PAYJP_API_KEY
        # 有効期限が切れているカードを登録
        res_post_invalid_card_token = payjp.Token.create(
            card={
                'number' : '4000000000004012',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        # そのカードをデフォルトに設定
        customer = payjp.Customer.retrieve(self.PAYJP_TEST_COMPANY_ID)
        customer.cards.create(card=res_post_invalid_card_token['id'], default=True)

        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': 1,
            'expiration_time': dt.datetime.now() + dt.timedelta(weeks=4)
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), settings.PAYJP_CHARGE_ERROR_MESSAGES['expired_card'])
        self.assertEqual(PurchaseHistory.objects.all().count(), 0)

    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", mock_Charge_create_card_declined)
    def test_invalid_card(self):
        payjp.api_key = settings.PAYJP_API_KEY
        # 支払い時にエラーを起こすカードを登録
        res_post_invalid_card_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        # そのカードをデフォルトに設定
        customer = payjp.Customer.retrieve(self.PAYJP_TEST_COMPANY_ID)
        customer.cards.create(card=res_post_invalid_card_token['id'], default=True)

        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': 1,
            'expiration_time': dt.datetime.now() + dt.timedelta(weeks=4)
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), settings.PAYJP_CHARGE_ERROR_MESSAGES['card_declined'])
        self.assertEqual(PurchaseHistory.objects.all().count(), 0)

    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all_with_backup)
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MagicMock(side_effect=[payjp.error.CardError('ERROR', None,'card_declined', json_body={'error':{'code':'card_declined'}}), None]))
    def test_charge_with_backup_card(self):
        payjp.api_key = settings.PAYJP_API_KEY
        # 支払い時にエラーを起こすカードを登録
        card_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        # そのカードをデフォルトに設定
        customer = payjp.Customer.retrieve(self.PAYJP_TEST_COMPANY_ID)
        customer.cards.create(card=card_token['id'], default=True)

        # 有効なエラーを起こすカードを登録
        card_token = payjp.Token.create(
            card={
                'number' : '4242424242424242',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        # そのカードをサブに設定
        customer = payjp.Customer.retrieve(self.PAYJP_TEST_COMPANY_ID)
        customer.cards.create(card=card_token['id'], default=False)
        
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': 1,
            'expiration_time': dt.datetime.now() + dt.timedelta(weeks=4)
        }

        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(PurchaseHistory.objects.all().count(), 1)
        self.assertEqual(len(mail.outbox), 2)
        email = mail.outbox[0]
        self.assertEqual('[コモレビ]アドオン購入完了のご連絡', email.subject)
        self.assertIn('アドオンの購入が完了いたしましたのでご連絡いたします。', email.message().as_string())
        email = mail.outbox[1]
        self.assertEqual('[コモレビ] 予備クレジットカードでの決済完了のご連絡', email.subject)
        self.assertIn('予備クレジットカードでの決済が行われましたのでご連絡いたします。', email.message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, email.message().as_string())

    @patch("payjp.resource.ListObject.all", MockingPayjpAPIHelper.mock_ListObject_all_with_backup)
    @patch("payjp.resource.ListObject.create", MockingPayjpAPIHelper.mock_ListObject_create)
    @patch("payjp.resource.Token.create", MockingPayjpAPIHelper.mock_Token_create)
    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", mock_Charge_create_card_declined)
    def test_charge_failed(self):
        payjp.api_key = settings.PAYJP_API_KEY
        # 支払い時にエラーを起こすカードを登録
        card_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        # そのカードをデフォルトに設定
        customer = payjp.Customer.retrieve(self.PAYJP_TEST_COMPANY_ID)
        customer.cards.create(card=card_token['id'], default=True)

        # 支払い時にエラーを起こすカードを登録
        card_token = payjp.Token.create(
            card={
                'number' : '4000000000080319',
                'cvc' : '123',
                'exp_month' : '2',
                'exp_year' : '2024',
                'name': 'POST MAN',
            },
            headers={'X-Payjp-Direct-Token-Generate': 'true'}
        )
        # そのカードをサブに設定
        customer = payjp.Customer.retrieve(self.PAYJP_TEST_COMPANY_ID)
        customer.cards.create(card=card_token['id'], default=False)
        
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': 1,
            'expiration_time': dt.datetime.now() + dt.timedelta(weeks=4)
        }

        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_parent_addon_not_active(self):
        ScheduledEmailSetting.objects.create(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count_extra_period=False)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count_extra_period=False).exists())
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.IS_OPEN_COUNT_EXTRA_PERIOD_AVAILABLE_ADDON_MASTER_ID,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertFalse(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count_extra_period=True).exists())
        self.assertEqual(PurchaseHistory.objects.all().count(), 0)

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_parent_addon_active(self):
        # 親アドオンの購入
        ScheduledEmailSetting.objects.create(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count=False, use_open_count_extra_period=False)
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count=True).exists())
        self.assertEqual(PurchaseHistory.objects.all().count(), 1)
        # 子アドオンの購入
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count_extra_period=False).exists())
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.IS_OPEN_COUNT_EXTRA_PERIOD_AVAILABLE_ADDON_MASTER_ID,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id=self.PAYJP_TEST_COMPANY_ID, use_open_count_extra_period=True).exists())
        self.assertEqual(PurchaseHistory.objects.all().count(), 2)
    
    def test_trial_tenant(self):
        CompanyAttribute.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb9',
            user_registration_limit=5,
            trial_expiration_date=(dt.datetime.now() + dt.timedelta(days=2)).date()
        )
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': 1,
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch("payjp.resource.Customer.retrieve", MockingPayjpAPIHelper.mock_Customer_retrieve)
    @patch("payjp.resource.Charge.create", MockingPayjpAPIHelper.mock_Charge_create)
    def test_use_delivery_interval_addon_master_id(self):
        url = api_reverse('addon_purchase')
        post_data = {
            'addon_master_id': settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_ADDON_MASTER_ID,
            'expiration_time': ""
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        purchase_histories = PurchaseHistory.objects.all()
        self.assertEqual(len(purchase_histories), 1)
        self.assertEqual(purchase_histories[0].price, 3300)


class AddonRevokeAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json',
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
        'app_staffing/tests/fixtures/organization_master_data.json'
    ]

    def setUp(self):
        user = User.objects.get(username=TEST_USER_NAME)
        user.user_role_id = 6
        user.save()

        company = user.company
        another_company = Company.objects.get(id='0639df05-8db5-40f2-a25d-6bcd3f8ae4ca')
        self.addon_1 = Addon.objects.create(
            company=company,
            addon_master_id=1,
            expiration_time=dt.datetime.now() + dt.timedelta(days=7)
        )
        self.addon_2 = Addon.objects.create(
            company=company,
            addon_master_id=2,
            expiration_time=None,
        )
        self.addon_3 = Addon.objects.create(
            company=another_company,
            addon_master_id=4,
            expiration_time=None,
        )
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_revoke_addon(self):
        addon = Addon.objects.all()[0]
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        addons = Addon.objects.all()
        self.assertEqual(addons.count(), 2)

        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual('[コモレビ]アドオン解約完了のご連絡', email.subject)
        self.assertIn('アドオンの解約が完了いたしましたのでご連絡いたします。', email.message().as_string())
        user = User.objects.get(username=TEST_USER_NAME)
        self.assertIn(user.display_name, email.message().as_string())

        # 未購入(削除済み)のアドオンを削除しようとするとエラー
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_cannot_revoke_addon_because_unauthorized(self):
        self.client.logout()
        addon = Addon.objects.all()[0]
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_cannot_revoke_addon_because_of_wrong_id(self):
        url = api_reverse('addon_revoke', kwargs={'pk': str(uuid4())})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    def test_cannot_revoke_addon_because_of_comment_template_limit(self):
        post_data = { 'templates': [
            {'title': 'hoge', 'content': 'hogehoge'},
            {'title': 'fuga', 'content': 'fugafuga'},
            {'title': 'hoge2', 'content': 'hogehoge2'},
            {'title': 'fuga2', 'content': 'fugafuga2'},
        ] }
        response = self.client.post(path='/app_staffing/comment_template/organization', data=post_data, format='json')
        response = self.client.post(path='/app_staffing/comment_template/contact', data=post_data, format='json')

        addon = Addon.objects.filter(addon_master_id=2)[0]
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], 'アドオンの解約に失敗しました。')
    
    def test_cannot_revoke_addon_because_of_search_template_limit(self):
        cache_key = f'{settings.CACHE_SEARCH_TEMPLATE_KEY_BASE}_3efd25d0-989a-4fba-aa8d-b91708760eb1_1957ab0f-b47c-455a-a7b7-4453cfffd05e_organization'
        default = [
            {
                "name": "testname1",
                "star": False,
                "display_name": "testname1",
                "template_name": "testname1",
                "values": { "hoge": "fuga" },
                "pageSize": 10,
                "sortKey": "",
                "sortOrder": "",
                "selectedColumnKeys": []
            },
            {
                "name": "testname2",
                "star": False,
                "display_name": "testname2",
                "template_name": "testname2",
                "values": { "hoge": "fuga" },
                "pageSize": 10,
                "sortKey": "",
                "sortOrder": "",
                "selectedColumnKeys": []
            },
            {
                "name": "testname3",
                "star": False,
                "display_name": "testname3",
                "template_name": "testname3",
                "values": { "hoge": "fuga" },
                "pageSize": 10,
                "sortKey": "",
                "sortOrder": "",
                "selectedColumnKeys": []
            },
            {
                "name": "testname4",
                "star": False,
                "display_name": "testname4",
                "template_name": "testname4",
                "values": { "hoge": "fuga" },
                "pageSize": 10,
                "sortKey": "",
                "sortOrder": "",
                "selectedColumnKeys": []
            },
        ]
        cache.set(cache_key, default)
        addon = Addon.objects.filter(addon_master_id=1)[0]
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], 'アドオンの解約に失敗しました。')

    def test_revoke_open_count_extra_period_addon(self):
        ScheduledEmailSetting.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_open_count_extra_period=True)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_open_count_extra_period=True).exists())
        addon = Addon.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
            addon_master_id=settings.IS_OPEN_COUNT_EXTRA_PERIOD_AVAILABLE_ADDON_MASTER_ID,
            expiration_time=None,
        )
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_open_count_extra_period=False).exists())

    def test_revoke_remove_promotion_addon(self):
        ScheduledEmailSetting.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_remove_promotion=True)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_remove_promotion=True).exists())
        addon = Addon.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
            addon_master_id=settings.REMOVE_PROMOTION_ADDON_MASTER_ID,
                expiration_time=None,
        )
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_remove_promotion=False).exists())

    def test_revoke_open_count_addon(self):
        ScheduledEmailSetting.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_open_count=True)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_open_count=True).exists())
        addon = Addon.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
            addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID,
            expiration_time=None,
        )
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_open_count=False).exists())

    def test_child_addon_is_active(self):
        ScheduledEmailSetting.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_open_count=True)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_open_count=True).exists())
        addon = Addon.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
            addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID,
            expiration_time=None,
        )
        Addon.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
            addon_master_id=settings.IS_OPEN_COUNT_EXTRA_PERIOD_AVAILABLE_ADDON_MASTER_ID,
            expiration_time=None,
        )
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(str(response.data['detail']), '子アドオンを削除していないため、削除できません')

    def test_revoke_scheduled_email_byte_size_addon(self):
        ScheduledEmailSetting.objects.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_attachment_max_size=True)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_attachment_max_size=True).exists())
        addon = Addon.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
            addon_master_id=settings.SCHEDULED_EMAIL_BYTE_SIZE_ADDON_MASTER_ID,
            expiration_time=None,
        )
        max_byte_size, max_byte_size_error_message = scheduled_email_max_byte_size_and_error_message('3efd25d0-989a-4fba-aa8d-b91708760eb1')
        self.assertEqual(max_byte_size, settings.SCHEDULED_EMAIL_ADDON_MAX_BYTE_SIZE)
        self.assertEqual(max_byte_size_error_message, settings.MAX_BYTE_SIZE_WITH_ADDON_ERROR_MESSAGE)

        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(ScheduledEmailSetting.objects.filter(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1', use_attachment_max_size=False).exists())

        max_byte_size, max_byte_size_error_message = scheduled_email_max_byte_size_and_error_message('3efd25d0-989a-4fba-aa8d-b91708760eb1')
        self.assertEqual(max_byte_size, settings.SCHEDULED_EMAIL_MAX_BYTE_SIZE)
        self.assertEqual(max_byte_size_error_message, settings.MAX_BYTE_SIZE_ERROR_MESSAGE)

    def test_revoke_search_template_addon(self):
        addon = Addon.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
            addon_master_id=settings.SEARCH_TEMPLATE_ADDON_ID,
            expiration_time=dt.datetime.now() + dt.timedelta(weeks=4),
        )
        before_available_count = search_template_total_available_count('3efd25d0-989a-4fba-aa8d-b91708760eb1')
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        after_available_count = search_template_total_available_count('3efd25d0-989a-4fba-aa8d-b91708760eb1')
        self.assertEqual(before_available_count, after_available_count+5)

    def test_revoke_comment_template_addon(self):
        addon = Addon.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
            addon_master_id=settings.COMMENT_TEMPLATE_ADDON_ID,
            expiration_time=dt.datetime.now() + dt.timedelta(weeks=4),
        )
        before_available_count = comment_template_total_available_count('3efd25d0-989a-4fba-aa8d-b91708760eb1')
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        after_available_count = comment_template_total_available_count('3efd25d0-989a-4fba-aa8d-b91708760eb1')
        self.assertEqual(before_available_count, after_available_count+5)

    def test_revoke_scheduled_email_target_count_addon(self):
        addon = Addon.objects.create(
            company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
            addon_master_id=settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID,
            expiration_time=dt.datetime.now() + dt.timedelta(weeks=4),
        )
        before_limit = scheduled_email_limit_target_count('3efd25d0-989a-4fba-aa8d-b91708760eb1')
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        after_limit = scheduled_email_limit_target_count('3efd25d0-989a-4fba-aa8d-b91708760eb1')
        self.assertEqual(before_limit, after_limit+1000)

    def test_revoke_use_delivery_term_addon(self):
        addon = Addon.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            addon_master_id=settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_ADDON_MASTER_ID,
            expiration_time=None,
        )
        ScheduledEmail.objects.create(
            company_id=DEFAULT_COMPANY_ID, scheduled_email_status_id=2, sender_id=API_USER_ID, date_to_send='2022-01-13T14:30:00Z'
        )
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_cannot_revoke_use_delivery_term_addon(self):
        addon = Addon.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            addon_master_id=settings.SCHEDULED_EMAIL_DELIVERY_INTERVAL_ADDON_MASTER_ID,
            expiration_time=None,
        )
        ScheduledEmail.objects.create(
            company_id=DEFAULT_COMPANY_ID, scheduled_email_status_id=2, sender_id=API_USER_ID, date_to_send='2022-01-13T14:10:00Z'
        )
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_revoke_remove_promotion_addon(self):
        addon = Addon.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            addon_master_id=settings.REMOVE_PROMOTION_ADDON_MASTER_ID,
            expiration_time=None,
        )
        ScheduledEmail.objects.create(
            company_id=DEFAULT_COMPANY_ID, scheduled_email_status_id=2, sender_id=API_USER_ID, date_to_send='2022-01-13T14:10:00Z', text_format='html'
        )
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @override_settings(SCHEDULED_EMAIL_DEFAULT_LIMIT_TARGET_COUNT=1)
    def test_cannot_revoke_target_count_addon(self):
        addon = Addon.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            addon_master_id=settings.SCHEDULED_EMAIL_TARGET_COUNT_ADDON_MASTER_ID,
            expiration_time=None,
        )
        email = ScheduledEmail.objects.create(
            company_id=DEFAULT_COMPANY_ID, scheduled_email_status_id=2, sender_id=API_USER_ID, date_to_send='2022-01-13T14:10:00Z'
        )
        organization = Organization.objects.create(
            name="test organization",
            category='client',
            score=3,
            is_blacklisted=False,
            company_id=DEFAULT_COMPANY_ID,
            organization_category_id=4 # client
        )
        contact_a = Contact.objects.create(
            last_name='test',
            email='test_a@example.com',
            organization=organization,
            company_id=DEFAULT_COMPANY_ID,
        )
        ScheduledEmailTarget.objects.create(
            email=email,
            contact=contact_a,
            company_id=DEFAULT_COMPANY_ID,
        )
        contact_b = Contact.objects.create(
            last_name='test',
            email='test_b@example.com',
            organization=organization,
            company_id=DEFAULT_COMPANY_ID,
        )
        ScheduledEmailTarget.objects.create(
            email=email,
            contact=contact_b,
            company_id=DEFAULT_COMPANY_ID,
        )
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_revoke_attachment_size_addon(self):
        addon = Addon.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            addon_master_id=settings.SCHEDULED_EMAIL_BYTE_SIZE_ADDON_MASTER_ID,
            expiration_time=None,
        )
        email = ScheduledEmail.objects.create(
            company_id=DEFAULT_COMPANY_ID, scheduled_email_status_id=2, sender_id=API_USER_ID, date_to_send='2022-01-13T14:10:00Z', text_format='html'
        )
        ScheduledEmailAttachment.objects.create(company_id=DEFAULT_COMPANY_ID, email=email, name='test', file='', size=3000000) # 約3MBの添付ファイル
        url = api_reverse('addon_revoke', kwargs={'pk': str(addon.id)})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class AddonMasterListAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/addon_datas.json'
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_get(self):
        response = self.client.get(path='/app_staffing/addonmaster', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 8)
        self.assertEqual(response.data[0]['title'], '検索条件テンプレート登録数')
        self.assertEqual(response.data[0]['is_dashboard'], False)
        self.assertEqual(response.data[0]['is_organizations'], True)
        self.assertEqual(response.data[0]['is_scheduled_mails'], True)
        self.assertEqual(response.data[0]['is_shared_mails'], False)
        self.assertEqual(response.data[0]['is_my_company_setting'], False)
        self.assertEqual(list(response.data[0]['targets']), ['取引先', '取引先担当者', '配信メール(一覧)', '配信メール(宛先検索)'])

        self.assertEqual(response.data[3]['title'], '配信開封情報の取得期間の延長')
        self.assertEqual(response.data[3]['is_dashboard'], False)
        self.assertEqual(response.data[3]['is_organizations'], False)
        self.assertEqual(response.data[3]['is_scheduled_mails'], True)
        self.assertEqual(response.data[3]['is_shared_mails'], False)
        self.assertEqual(response.data[3]['is_my_company_setting'], False)
        self.assertEqual(list(response.data[3]['targets']), ['配信メール'])
        self.assertEqual(list(response.data[3]['parents']), [4])
