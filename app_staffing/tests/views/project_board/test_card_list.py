from rest_framework import status
from rest_framework.test import APITestCase

from app_staffing.board.constants import CARD_LIST_ARRAY
from app_staffing.board.models.card_list import ProjectCardList
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD


class TestProjectActionCardListAPIViewTestCase(APITestCase):


    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/project_board/card_list.json',
        'app_staffing/tests/fixtures/project_board/project.json',
    ]
    card_list_id_1 = '185c2a9b-ec13-4194-bd85-386de64f5ee4'
    card_list_id_2 = '5737b0a8-5a99-4254-8798-b8c4a475cc2f'
    base_url = '/app_staffing/board/project/lists'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_list_api(self):
        response = self.client.get(path=self.base_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 7)

        for index in range(len(response.data)):
            self.assertEqual(response.data[index]['title'], CARD_LIST_ARRAY[index])

    def test_archive_card_list_board(self):
        response = self.client.patch(path=f'{self.base_url}/{self.card_list_id_1}/archive', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        obj = ProjectCardList.objects.get(pk=self.card_list_id_1).project_items.last()
        self.assertEqual(obj.is_archived, True)

    def test_move_card_list_board(self):
        count_card_list_1 = ProjectCardList.objects.get(pk=self.card_list_id_1).project_items.all().count()
        count_card_list_2 = ProjectCardList.objects.get(pk=self.card_list_id_2).project_items.all().count()
        response = self.client.patch(path=f'{self.base_url}/{self.card_list_id_2}/move', data={'listId': self.card_list_id_1}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        count_card_list = ProjectCardList.objects.get(pk=self.card_list_id_1).project_items.all().count()
        self.assertEqual((count_card_list_1+count_card_list_2), count_card_list)
