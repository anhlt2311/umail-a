from app_staffing.board.models import ProjectCheckList
from app_staffing.tests.factories.check_list import ProjectCheckListFactory
from app_staffing.tests.views.project_board import ProjectBoardTestBase


class CheckListProjectCardActionTest(ProjectBoardTestBase):

    def setUp(self):
        super().setUp()
        self.url = f"/app_staffing/board/project/checklists"
        self.data = dict(title="title", card_id=self.project.id)

    def test_create_check_list_successful(self):
        res = self.client.post(self.url, data=self.data, format='json')
        rows = ProjectCheckList.objects.filter(project_id=self.project.id)
        self.verify(res, rows, input_data=self.data, expected_keys=["title"], status_code=201)

    def test_get_check_list_successful(self):
        mock_check_list = ProjectCheckListFactory(project=self.project)
        res = self.client.get(f"{self.url}/{mock_check_list.id}")
        rows = ProjectCheckList.objects.filter(id=mock_check_list.id)
        self.verify(res, rows, is_get=True, input_data=mock_check_list, expected_keys=["content", "is_finished"])

    def test_update_check_list_successful(self):
        mock_check_list = ProjectCheckListFactory(project=self.project)
        data = dict(title="new_title", show_finished=False)
        res = self.client.patch(f"{self.url}/{mock_check_list.id}", data=data, format='json')
        rows = ProjectCheckList.objects.filter(id=mock_check_list.id)
        self.verify(res, rows, input_data=data, expected_keys=["content", "show_finished"])

    def test_delete_check_list_successful(self):
        mock_check_list = ProjectCheckListFactory(project=self.project)
        res = self.client.delete(f"{self.url}/{mock_check_list.id}")
        rows = ProjectCheckList.objects.filter(project_id=self.project.id)
        self.assertEqual(res.status_code, 204)
        self.assertEqual(rows.count(), 0)
