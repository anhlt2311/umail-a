from app_staffing.tests.factories.user import AppUserFactory
from app_staffing.board.models import ProjectAssignee, Project
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, ANOTHER_TENANT_NORMAL_USER_NAME, \
    ANOTHER_TENANT_NORMAL_USER_PASSWORD
from rest_framework.test import APITestCase

from rest_framework import status

from app_staffing.tests.views.project_board import ProjectBoardTestBase


class CardOrderUpdateAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/project_board/card_list.json',
        'app_staffing/tests/fixtures/project_board/project.json',
    ]

    base_url = '/app_staffing/board/project/cards'

    card_list_id_1 = '185c2a9b-ec13-4194-bd85-386de64f5ee4'
    card_list_id_2 = '5737b0a8-5a99-4254-8798-b8c4a475cc2f'

    card_id_1_card_list_1_order_0 = '185c2a9b-ec13-4194-bd85-386de64f5ee4'
    card_id_2_card_list_1_order_1 = '49d343a3-9520-43ec-aa61-46ffd7729af8'
    card_id_3_card_list_1_order_2 = '7fde8493-8aa4-41f6-808f-1ec6c9d4c25d'
    card_id_4_card_list_2_order_0 = '171ec336-a0b0-4a0a-8c48-0b7ab9148f73'
    card_id_5_card_list_2_order_1 = 'deb03711-5965-4592-aa8c-f8e277e5a8d8'

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_move_card_from_card_list_1_to_card_list_2(self):
        response = self.client.patch(path=f'{self.base_url}/{self.card_id_2_card_list_1_order_1}/position', data={
            'list_id': self.card_list_id_2,
            'position': 1
        })
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        projects = Project.objects.filter(card_list_id=self.card_list_id_2).order_by('order')
        self.assertEqual(str(projects[0].id), self.card_id_4_card_list_2_order_0)
        self.assertEqual(str(projects[1].id), self.card_id_2_card_list_1_order_1)
        self.assertEqual(str(projects[2].id), self.card_id_5_card_list_2_order_1)

    def test_move_card_from_top_to_bottom(self):
        response = self.client.patch(path=f'{self.base_url}/{self.card_id_1_card_list_1_order_0}/position', data={
            'list_id': self.card_list_id_1,
            'position': 2
        })
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        projects = Project.objects.filter(card_list_id=self.card_list_id_1).order_by('order')
        self.assertEqual(str(projects[0].id), self.card_id_2_card_list_1_order_1)
        self.assertEqual(str(projects[1].id), self.card_id_1_card_list_1_order_0)
        self.assertEqual(str(projects[2].id), self.card_id_3_card_list_1_order_2)

    def test_move_card_from_bottom_to_top(self):
        response = self.client.patch(path=f'{self.base_url}/{self.card_id_3_card_list_1_order_2}/position', data={
            'list_id': self.card_list_id_1,
            'position': 0
        })
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        projects = Project.objects.filter(card_list_id=self.card_list_id_1).order_by('order')
        self.assertEqual(str(projects[0].id), self.card_id_3_card_list_1_order_2)
        self.assertEqual(str(projects[1].id), self.card_id_1_card_list_1_order_0)
        self.assertEqual(str(projects[2].id), self.card_id_2_card_list_1_order_1)

    def test_another_user_not_same_company_can_not_move_card(self):
        self.client.logout()
        self.client.login(username=ANOTHER_TENANT_NORMAL_USER_NAME, password=ANOTHER_TENANT_NORMAL_USER_PASSWORD)
        response = self.client.patch(path=f'{self.base_url}/{self.card_id_2_card_list_1_order_1}/position', data={
            'list_id': self.card_list_id_2,
            'position': 1
        })
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_unauth_user_cannot_patch(self):
        self.client.logout()
        response = self.client.patch(path=f'{self.base_url}/{self.card_id_2_card_list_1_order_1}/position', data={
            'list_id': self.card_list_id_2,
            'position': 1
        })
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class CreateProjectAPIViewTestCase(ProjectBoardTestBase):

    def setUp(self):
        super().setUp()
        self.url = f"/app_staffing/board/project/cards"
        self.data = dict(
            detail="detail",
            list_id=self.card_list.id
        )

    def test_create_project_successful(self):
        res = self.client.post(self.url, data=self.data, format="json")
        projects = Project.objects.all()
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(res.data.get("id"))
        self.assertEqual(projects.count(), 2)
        if projects:
            self.assertEqual(projects[1].detail, self.data.get("detail"))

    def test_update_card_successful(self):
        url = f"/app_staffing/board/project/cards/{self.project.id}"
        user = AppUserFactory(
            username='Another User',
            first_name='User',
            last_name='Another',
            email='another@example.com'
        )
        data = dict(
            detail="detail",
            description="description",
            working_hours_remarks="test",
            people_amount=1,
            assignees=[user.id],
            price_amount=10
        )
        res = self.client.patch(url, data=data, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        projects = Project.objects.filter(id=self.project.id)
        assignees = ProjectAssignee.objects.filter(card_id=self.project.id).count()
        self.assertEqual(assignees, 1)
        self.verify(
            res, projects, input_data=data,
            expected_keys=["detail", "description", "people_amount", "working_hours_remarks", "price_amount"]
        )

    def test_delete_card_successful(self):
        url = f"/app_staffing/board/project/cards/{self.project.id}"
        res = self.client.delete(url)
        projects = Project.objects.all()
        self.assertEqual(res.status_code, 204)
        self.assertEqual(projects.count(), 1)
