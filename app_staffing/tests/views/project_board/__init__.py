from rest_framework.test import APITestCase

from app_staffing.estimators.loaders.models import model_to_dict
from app_staffing.tests.factories.project_card_list import ProjectCardListFactory
from app_staffing.tests.factories.project import ProjectFactory
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD
from rest_framework import status


class ProjectBoardTestBase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        self.mock_project_board_data()

    def mock_project_board_data(self):
        self.card_list = ProjectCardListFactory()
        self.project = ProjectFactory(card_list=self.card_list)

    def verify(self, res, rows, **kwargs):
        is_delete_action = kwargs.get("is_delete_action", False)
        is_get = kwargs.get("is_get", False)
        expected_keys = kwargs.get("expected_keys", [])
        input_data = kwargs.get("input_data", {})

        input_data = model_to_dict(input_data) if is_get else input_data

        self.assertEqual(res.status_code, int(kwargs.get("status_code", status.HTTP_200_OK)))
        self.assertEqual(rows.count(), int(kwargs.get("total_rows", 1)))
        if not is_delete_action:
            for key in expected_keys:
                self.assertEqual(model_to_dict(rows.first()).get(key), input_data.get(key))
