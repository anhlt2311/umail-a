from app_staffing.board.models import PersonnelCheckList
from app_staffing.tests.factories.check_list import PersonnelCheckListFactory
from app_staffing.tests.views.personnel_board import PersonnelBoardTestBase


class CheckListPersonnelCardActionTest(PersonnelBoardTestBase):

    def setUp(self):
        super().setUp()
        self.url = f"/app_staffing/board/personnel/checklists"
        self.data = dict(title="title", cardId=self.card.id)

    def test_create_check_list_successful(self):
        res = self.client.post(self.url, data=self.data, format="json")
        rows = PersonnelCheckList.objects.filter(personnel_id=self.card.id)
        self.verify(res, rows, input_data=self.data, expected_keys=["title"], status_code=201)

    def test_get_check_list_successful(self):
        mock_check_list = PersonnelCheckListFactory(personnel=self.card)
        res = self.client.get(f"{self.url}/{mock_check_list.id}")
        rows = PersonnelCheckList.objects.filter(id=mock_check_list.id)
        self.verify(res, rows, is_get=True, input_data=mock_check_list, expected_keys=["content", "is_finished"])

    def test_update_check_list_successful(self):
        mock_check_list = PersonnelCheckListFactory(personnel=self.card)
        data = dict(title="new_title", show_finished=True)
        res = self.client.patch(f"{self.url}/{mock_check_list.id}", data=data, format="json")
        rows = PersonnelCheckList.objects.filter(id=mock_check_list.id)
        self.verify(res, rows, input_data=data, expected_keys=["content", "show_finished"])

    def test_delete_check_list_successful(self):
        mock_check_list = PersonnelCheckListFactory(personnel=self.card)
        res = self.client.delete(f"{self.url}/{mock_check_list.id}")
        rows = PersonnelCheckList.objects.filter(personnel_id=self.card.id)
        self.assertEqual(res.status_code, 204)
        self.assertEqual(rows.count(), 0)
