from pathlib import Path
from django.test import override_settings
from rest_framework import status
from rest_framework.test import APITestCase
from app_staffing.board.models.card import Personnel, PersonnelSkillSheet
from app_staffing.tests.settings import (
    TEST_USER_NAME,
    TEST_USER_PASSWORD,
    TEST_PERSONNEL_ID,
    TEST_SKILL_SHEET_ID
)
from rest_framework.reverse import reverse
from django.core.files.uploadedfile import SimpleUploadedFile  # For file mocking.


def get_test_file_path(file_name):
        return Path(__file__).parent.parent.parent / 'files' / file_name


class CreatePersonnelSkillSheetAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/personnel_board/card_list.json',
        'app_staffing/tests/fixtures/personnel_board/card.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_create_skill_sheets_successfully(self):
        test_file_name = 'skill_sheet_test_1.3mb.csv'
        url = reverse(
                'personnel_skill_sheets',
                kwargs={
                    "parent_pk": TEST_PERSONNEL_ID
                }
            )

        with open(get_test_file_path(test_file_name)) as test_file:
            response = self.client.patch(url, data={
                'file': test_file
            })
            # check response ok
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

            # check database record
            record_count = PersonnelSkillSheet.objects.filter(
                personnel_id=TEST_PERSONNEL_ID,
                name=test_file_name
                ).count()
            self.assertGreaterEqual(record_count, 1)


class FailedCreatePersonnelSkillSheetAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/personnel_board/card_list.json',
        'app_staffing/tests/fixtures/personnel_board/card.json',
        'app_staffing/tests/fixtures/personnel_board/personnel_skill_sheet.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)


    # fixture personnel_skill_sheet has 2 record total 2.8mb
    @override_settings(PERSONNEL_MAX_BYTE_SIZE_SKILL_SHEET=3000000, PERSONNEL_MAX_SKILL_SHEET_FILE=10)
    def test_create_skill_sheets_greater_than_max_size(self):
        test_file_name = 'skill_sheet_test_1.3mb.csv'
        url = reverse(
                'personnel_skill_sheets',
                kwargs={
                    "parent_pk": TEST_PERSONNEL_ID
                }
            )

        with open(get_test_file_path(test_file_name)) as test_file:
            response = self.client.patch(url, data={
                'file': test_file
            })
            # check response failure
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    # fixture personnel_skill_sheet has 2 record
    @override_settings(PERSONNEL_MAX_SKILL_SHEET_FILE=2)
    def test_create_skill_sheets_number_of_file_greater_than_max(self):
        test_file_name = 'skill_sheet_test_1.3mb.csv'
        url = reverse(
            'personnel_skill_sheets',
            kwargs={
                "parent_pk": TEST_PERSONNEL_ID
            }
        )


        with open(get_test_file_path(test_file_name)) as test_file:
            response = self.client.patch(url, data={
                'file': test_file
            })
            # check response failure
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class WithSkillsheetFixtureAPIViewTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/personnel_board/card_list.json',
        'app_staffing/tests/fixtures/personnel_board/card.json',
        'app_staffing/tests/fixtures/personnel_board/personnel_skill_sheet.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    def test_soft_delete_skill_sheet_of_personnel_successfully(self):
        url = reverse('personnel_skill_sheet_detail', kwargs={
            'pk': TEST_SKILL_SHEET_ID})

        response = self.client.delete(url)

        # check response ok
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # check record has been soft deleted
        record_count = PersonnelSkillSheet.objects.filter(
                pk=TEST_SKILL_SHEET_ID,
                deleted_at__isnull=True
            ).count()
        self.assertEqual(record_count, 0)

    def test_get_skill_sheet_successfully(self):
        url = reverse('personnel_skill_sheet_detail', kwargs={
            'pk': TEST_SKILL_SHEET_ID})

        response = self.client.get(url)

        # check response ok
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("id", response.data)

    def test_download_skill_sheet_successfully(self):
        url = reverse('personnel_skill_sheet_detail', kwargs={
            'pk': TEST_SKILL_SHEET_ID})

        response = self.client.get(url, HTTP_ACCEPT='application/octet-stream')

        # check response ok
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn(
            "attachment",
            response.get('Content-Disposition')
        )
