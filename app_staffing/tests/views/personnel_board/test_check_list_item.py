from app_staffing.board.models import PersonnelCheckListItem
from app_staffing.tests.factories.check_list import PersonnelCheckListFactory
from app_staffing.tests.factories.check_list_item import PersonnelCheckListItemFactory
from app_staffing.tests.views.personnel_board import PersonnelBoardTestBase


class CheckListItemCardActionTest(PersonnelBoardTestBase):

    def setUp(self):
        super().setUp()
        self.check_list = PersonnelCheckListFactory(personnel=self.card)
        self.url = f"/app_staffing/board/personnel/checklists/{self.check_list.id}/items"
        self.data = dict(content="content", is_finished=True)

    def test_create_check_list_item_successful(self):
        res = self.client.post(self.url, data=self.data, format="json")
        rows = PersonnelCheckListItem.objects.filter(check_list_id=self.check_list.id)
        self.verify(res, rows, input_data=self.data, expected_keys=["content", "is_finished"], status_code=201)

    def test_get_check_list_item_successful(self):
        mock_check_list_item = PersonnelCheckListItemFactory(check_list=self.check_list)
        url = f"/app_staffing/board/personnel/checklists/{self.check_list.id}/items"
        res = self.client.get(url)
        rows = PersonnelCheckListItem.objects.filter(id=mock_check_list_item.id)
        self.verify(res, rows, is_get=True, input_data=mock_check_list_item, expected_keys=["content", "is_finished"])

    def test_update_check_list_item_successful(self):
        mock_check_list_item = PersonnelCheckListItemFactory(check_list=self.check_list)
        url = f"/app_staffing/board/personnel/checklists/{self.check_list.id}/items/{mock_check_list_item.id}"
        data = dict(content="new_content", is_finished=False)
        res = self.client.patch(url, data=data, format="json")
        rows = PersonnelCheckListItem.objects.filter(id=mock_check_list_item.id)
        self.verify(res, rows, input_data=data, expected_keys=["content", "is_finished"])

    def test_delete_check_list_item_successful(self):
        mock_check_list_item = PersonnelCheckListItemFactory(check_list=self.check_list)
        url = f"/app_staffing/board/personnel/checklists/{self.check_list.id}/items/{mock_check_list_item.id}"
        res = self.client.delete(url)
        rows = PersonnelCheckListItem.objects.filter(id=mock_check_list_item.id)
        self.assertEqual(res.status_code, 204)
        self.assertEqual(rows.count(), 0)
