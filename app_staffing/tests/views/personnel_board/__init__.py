from rest_framework.test import APITestCase

from app_staffing.estimators.loaders.models import model_to_dict
from app_staffing.tests.factories.card import PersonnelFactory
from app_staffing.tests.factories.personnel_card_list import PersonnelCardListFactory
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD
from rest_framework import status
from app_staffing.tests.factories.project import ProjectFactory
from app_staffing.tests.factories.project_card_list import ProjectCardListFactory
from app_staffing.tests.factories.contract import ProjectContractFactory


class PersonnelBoardTestBase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
    ]

    def setUp(self):
        super().setUp()
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)
        self.mock_personnel_board_data()

    def mock_personnel_board_data(self):
        self.card_list = PersonnelCardListFactory()
        self.card = PersonnelFactory(card_list=self.card_list)

    def mock_project_contract(self):
        self.project = ProjectFactory(card_list=ProjectCardListFactory())
        self.project_contract = ProjectContractFactory(project=self.project, personnel=self.card)

    def verify(self, res, rows, **kwargs):
        is_delete_action = kwargs.get("is_delete_action", False)
        is_get = kwargs.get("is_get", False)
        expected_keys = kwargs.get("expected_keys", [])
        input_data = kwargs.get("input_data", {})

        input_data = model_to_dict(input_data) if is_get else input_data

        self.assertEqual(res.status_code, int(kwargs.get("status_code", status.HTTP_200_OK)))
        self.assertEqual(rows.count(), int(kwargs.get("total_rows", 1)))
        if not is_delete_action:
            for key in expected_keys:
                c = model_to_dict(rows.first())
                a = model_to_dict(rows.first()).get(key)
                b = input_data.get(key)
                self.assertEqual(model_to_dict(rows.first()).get(key), input_data.get(key))
