from datetime import datetime, timedelta

import pytz
from django.conf import settings
from rest_framework import status

from app_staffing.board.models import PersonnelContract
from app_staffing.tests.factories.contract import PersonnelContractFactory
from app_staffing.tests.factories.organization import OrganizationFactory, ContactFactory
from app_staffing.tests.views.personnel_board import PersonnelBoardTestBase


class ContractCardActionTest(PersonnelBoardTestBase):

    def setUp(self):
        super().setUp()
        self.start = datetime.now(pytz.timezone(settings.TIME_ZONE))
        self.end = datetime.now(pytz.timezone(settings.TIME_ZONE)) + timedelta(1)
        self.start_utc = self.start.strftime("%Y-%m-%dT%H:%M:%S%z")
        self.end_utc = self.end.strftime("%Y-%m-%dT%H:%M:%S%z")
        self.url = f"/app_staffing/board/personnel/contracts"
        self.org = OrganizationFactory()
        self.contact = ContactFactory(organization=self.org)
        self.data = dict(
            detail="detail",
            price=11,
            cardId=self.card.id,
            higherOrganization=self.org.id,
            higherContact=self.contact.id,
            lowerOrganization=self.org.id,
            lowerContact=self.contact.id,
            projectPeriod=[self.start_utc, self.end_utc]
        )

    def test_create_contract_successful(self):
        res = self.client.post(self.url, data=self.data, format='json')
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.data.get('detail'), res.data.get('detail'))
        self.assertEqual(self.data.get('price'), res.data.get('price'))
        self.assertEqual(self.data.get('higherOrganization'), res.data.get('higher_organization'))
        self.assertEqual(self.data.get('higherContact'), res.data.get('higher_contact'))
        self.assertEqual(self.data.get('lowerOrganization'), res.data.get('lower_organization'))
        self.assertEqual(self.data.get('lowerContact'), res.data.get('lower_contact'))
        self.assertEqual(self.data.get('projectPeriod')[0], res.data.get('project_period')[0].strftime("%Y-%m-%dT%H:%M:%S%z"))
        self.assertEqual(self.data.get('projectPeriod')[1], res.data.get('project_period')[1].strftime("%Y-%m-%dT%H:%M:%S%z"))

    def test_get_contract_successful(self):
        mock_contract = \
            PersonnelContractFactory(personnel=self.card, higher_organization_id=self.org.id, higher_contact_id=self.contact.id,
                            lower_organization_id=self.org.id, lower_contact_id=self.contact.id,
                            detail=self.data.get('detail'), start=self.start,
                            end=self.end, price=22)
        res = self.client.get(f"{self.url}/{mock_contract.id}")
        self.assertEqual(str(mock_contract.id), res.data.get('id'))
        self.assertEqual(mock_contract.detail, res.data.get('detail'))
        self.assertEqual(mock_contract.price, res.data.get('price'))
        self.assertEqual(mock_contract.higher_organization_id, res.data.get('higher_organization'))
        self.assertEqual(mock_contract.higher_contact_id, res.data.get('higher_contact'))
        self.assertEqual(mock_contract.lower_organization_id, res.data.get('lower_organization'))
        self.assertEqual(mock_contract.lower_contact_id, res.data.get('lower_contact'))

    def test_update_contract_successful(self):
        mock_contracts = PersonnelContractFactory(personnel=self.card)
        data = dict(detail="new detail", price=22, lower_organization=self.org.id)
        res = self.client.patch(f"{self.url}/{mock_contracts.id}", data=data, format='json')
        rows = PersonnelContract.objects.filter(id=mock_contracts.id)
        self.verify(res, rows, input_data=data, expected_keys=["detail", "price", "lower_organization_id"])

    def test_delete_contract_successful(self):
        mock_contracts = PersonnelContractFactory(personnel=self.card)
        res = self.client.delete(f"{self.url}/{mock_contracts.id}")
        rows = PersonnelContract.objects.filter(personnel_id=self.card.id)
        self.assertEqual(res.status_code, 204)
        self.assertEqual(rows.count(), 0)
