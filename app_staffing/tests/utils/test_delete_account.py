from app_staffing.utils.delete_account import delete_with_chunk
from rest_framework.test import APITestCase
from app_staffing.tests.settings import TEST_USER_NAME, TEST_USER_PASSWORD, DEFAULT_COMPANY_ID
from app_staffing.models import Organization
from django.test import override_settings

class DeleteAccountUtilTestCase(APITestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
    ]

    def setUp(self):
        self.client.login(username=TEST_USER_NAME, password=TEST_USER_PASSWORD)

    @override_settings(DELETE_ACCOUNT_DATA_CHUNK_SIZE=1)
    def test_delete_with_chunk(self):
        count_before = Organization.objects.filter(company_id=DEFAULT_COMPANY_ID).count()
        print(Organization.objects.filter(company_id=DEFAULT_COMPANY_ID).all())
        self.assertEqual(count_before, 2)
        delete_with_chunk(Organization.objects.filter(company_id=DEFAULT_COMPANY_ID))
        count_after = Organization.objects.filter(company_id=DEFAULT_COMPANY_ID).count()
        self.assertEqual(count_after, 0)
