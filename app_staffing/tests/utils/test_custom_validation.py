from django.test import TestCase

from app_staffing.utils.custom_validation import password_validator
from django.conf import settings


class CustomValidationHelperTestCase(TestCase):

    password_valid = 'ChangeMe123@#$@*+~!@'

    over_max_password = 'ChangeMe123@#$@*+~!@ChangeMe123@#$@*+~!@ChangeMe123@#$@*+~!@ChangeMe123@#$@*+~!@ChangeMe123@#$@*+~!@ChangeMe123@#$@*+~!@ChangeMe123@#$@*+~!@ChangeMe123@#$@*+~!@'

    under_min_password = 'Abc12@#$'

    password_not_contain_uppercase = 'changeme123#$&'

    password_not_contain_number = 'ChangeMe#^%&$%#@#$'

    password_not_contain_special_character = 'ChangeMe123456'

    def test_password_validator_pass(self):
        response = password_validator(self.password_valid, settings.LENGTH_VALIDATIONS['user']['password'])
        self.assertFalse(response)

    def test_password_over_max_length(self):
        response = password_validator(self.over_max_password, settings.LENGTH_VALIDATIONS['user']['password'])
        self.assertTrue(response)

    def test_password_under_min_length(self):
        response = password_validator(self.under_min_password, settings.LENGTH_VALIDATIONS['user']['password'])
        self.assertTrue(response)

    def test_password_not_contain_uppercase(self):
        response = password_validator(self.test_password_not_contain_uppercase, settings.LENGTH_VALIDATIONS['user']['password'])
        self.assertTrue(response)

    def test_password_not_contain_number(self):
        response = password_validator(self.password_not_contain_number, settings.LENGTH_VALIDATIONS['user']['password'])
        self.assertTrue(response)

    def test_password_not_contain_special_character(self):
        response = password_validator(self.password_not_contain_special_character, settings.LENGTH_VALIDATIONS['user']['password'])
        self.assertTrue(response)
