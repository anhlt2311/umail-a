import pytz
from datetime import datetime
from freezegun import freeze_time
from django.test import TestCase
from django.conf import settings
from app_staffing.models.email import Email
from app_staffing.utils.email import (get_name_and_address_from_header, filter_email_by_sent_date_gte,
                                      get_due_date_of_shared_email)


class EmailHelperTestCase(TestCase):
    # Tuples of (label, data, expected_result_of_get_name, expected_result_of_get_address)
    from_header_patterns = [
        ('complete', 'ExampleCompanyStaff <staff@example.com>', 'ExampleCompanyStaff', 'staff@example.com'),
        ('address_only', '<staff@example.com>', '', 'staff@example.com'),
        ('space_included_name', 'Example User <staff@example.com>', 'Example User', 'staff@example.com'),
        ('missing_brackets', 'staff@example.com', '', 'staff@example.com')  # In this case,
        # name extraction looks like impossible because spaces cannot be used as an attribute separator.
    ]

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/sharedEmailWithExpirationDate/emails.json',
    ]
    fake_current_date = datetime(2022, 4, 25)

    def test_get_name_and_address(self):
        for label, data, name, address in self.from_header_patterns:
            with self.subTest(label=label):
                actual_name, actual_address = get_name_and_address_from_header(data)
                self.assertEqual(actual_name, name)
                self.assertEqual(actual_address, address)

    def test_filter_email_by_sent_date_gte(self):
        email_queryset = Email.objects.all().order_by('-sent_date')

        with freeze_time(self.fake_current_date):
            # when not pass sent_date to function => sent_date = default from settings
            email_qs = filter_email_by_sent_date_gte(email_queryset)
            sent_date, oldest_sent_date = get_due_date_of_shared_email(), email_qs.last().sent_date
            self.assertTrue(sent_date <= oldest_sent_date)
            self.assertEqual(len(email_qs), 7)

            # pass sent_date to function
            sent_date = datetime(2022, 4, 5).astimezone(pytz.timezone(settings.TIME_ZONE))
            email_qs = filter_email_by_sent_date_gte(email_queryset, sent_date)
            oldest_sent_date = email_qs.last().sent_date
            self.assertTrue(sent_date <= oldest_sent_date)
            self.assertEqual(len(email_qs), 5)
