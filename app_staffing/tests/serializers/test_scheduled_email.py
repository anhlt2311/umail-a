import datetime
import os
import jpholiday

from uuid import UUID
from unittest.mock import MagicMock

from django.core.files.uploadedfile import SimpleUploadedFile
import pytz

from rest_framework.serializers import ValidationError

from app_staffing.models import ScheduledEmail, ScheduledEmailAttachment, Contact, ScheduledEmailTarget
from app_staffing.models.email import SENT, QUEUED
from app_staffing.serializers.email import ScheduledEmailSerializer, ScheduledEmailAttachmentSerializer,\
    ScheduledEmailPreviewSerializer

from app_staffing.tests.serializers.base import SerializerTestCase
from app_staffing.tests.helpers.setup import MediaFolderSetupMixin
from app_staffing.tests.settings import DEFAULT_COMPANY_ID
import os
import shutil
from django.conf import settings
from django.core.files import File

from app_staffing.exceptions.scheduled_email import ScheduledEmailAlreadySentException
from app_staffing.utils.utils import generate_day_to_date

TEST_FILE_PATH = 'app_staffing/tests/__temp__/SendMailAttachment.txt'
TEST_MAIL_SENDER_ADDRESS = 'testMailSender@example.com'
TEST_MAIL_SENDER_FIRST_NAME = 'First Name'
TEST_MAIL_SUBJECT = 'This is a Test Email'
TEST_MAIL_TEXT = 'This is a main content of the email'
TEST_MAIL_ATTACHMENT_BODY = 'file content'
TEST_MAIL_ATTACHMENT_TYPE = 'text/plain'

# Constants for attachment test.
SOURCE_EMAIL_ID = "185c2a9b-ec13-4194-bd85-386de64f5ee4"
EMAIL_ATTACHMENTS_ID = "e57a0a67-a005-4cf7-9f26-62d089bfdd32"
MAIL_FIXTURE_ATTACHMENT_DIR_PATH = "email/attachments/scheduled_email/185c2a9b-ec13-4194-bd85-386de64f5ee4"
MAIL_FIXTURE_ATTACHMENT_PATH = MAIL_FIXTURE_ATTACHMENT_DIR_PATH + "/sample.txt"
FILE_NAME_TO_CREATE = "newFile.txt"


class ScheduledEmailSerializerReadTestCase(SerializerTestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_targets.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json'
    ]

    _serializer_class = ScheduledEmailSerializer
    maxDiff = None

    # Reference to fixture definitions.
    TEST_INSTANCE_ID = '5737b0a8-5a99-4254-8798-b8c4a475cc2f'
    TEST_INSTANCE_S_TARGET_CONTACTS = ['78630567-72ce-49a3-84e6-8947b5b057f3']

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        # Check fixture conditions.
        assert ScheduledEmail.objects.get(id=cls.TEST_INSTANCE_ID).scheduled_email_status.name == SENT

    def test_prohibit_updating_of_sent_emails(self):
        instance = ScheduledEmail.objects.get(id=self.TEST_INSTANCE_ID)
        serializer = ScheduledEmailSerializer(instance=instance, data={'status': QUEUED}, partial=True)
        with self.assertRaises(Exception) as e:
            serializer.is_valid()
            serializer.save()
        self.assertEqual(e.exception.detail, ScheduledEmailAlreadySentException().default_detail)

    def test_representation(self):
        contacts = [UUID(cid) for cid in self.TEST_INSTANCE_S_TARGET_CONTACTS]

        expected = {
            'attachments': [],
            'created_time': '2019-05-14T14:59:08.878000+09:00',
            'created_user': UUID('1dae6212-0ff4-40e4-86e1-bd73664a8b13'),
            'created_user__avatar': None,
            'created_user__name': 'TEST USER ADMIN',
            'date_to_send': '2019-05-14T14:58:46+09:00',
            'id': '5737b0a8-5a99-4254-8798-b8c4a475cc2f',
            'modified_time': '2019-05-14T14:59:08.878000+09:00',
            'modified_user': UUID('1dae6212-0ff4-40e4-86e1-bd73664a8b13'),
            'modified_user__name': 'TEST USER ADMIN',
            'send_copy_to_sender': False,
            'send_copy_to_share': False,
            'sender': UUID('1dae6212-0ff4-40e4-86e1-bd73664a8b13'),
            'sender__name': 'TEST USER ADMIN',
            'sent_date': '2019-05-14T14:59:04+09:00',
            'status': 'sent',
            'subject': 'Sent email',
            'target_contacts': contacts,
            'text': 'blah blah',
            'deleted_at': None,
            'send_error_count': None,
            'send_limit': 1000,
            'send_success_count': None,
            'send_total_count': None,
            'send_type': 'job',
            'search_condition': {},
            'open_count': 0,
            'text_format': 'text',
            'scheduled_email_send_type': 1,
            'scheduled_email_status': 4,
            'expired_date': None,
            'file_type': 1,
            'password': None,
        }
        instance = ScheduledEmail.objects.get(id=self.TEST_INSTANCE_ID)
        serializer = ScheduledEmailSerializer(instance=instance)
        self.assertDictEqual(expected, serializer.data)


class ScheduledEmailSerializerWriteTestCase(SerializerTestCase):

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_email_master_data.json'
    ]

    _serializer_class = ScheduledEmailSerializer
    maxDiff = None

    IDs_OF_CONTACTS_INITIAL_TARGET = ['78630567-72ce-49a3-84e6-8947b5b057f3', 'aa8f0a2b-acc5-4908-b4d0-918078a9373a']
    IDs_OF_CONTACTS_UPDATED_TARGET = ['4acfeb11-e071-4901-ba90-4aeed52895d1']

    data_for_create = {
        'sender': UUID('1dae6212-0ff4-40e4-86e1-bd73664a8b13'),
        'send_copy_to_sender': True,
        'status': 'draft',
        'subject': 'Scheduled Email for Creation Test',
        'target_contacts': IDs_OF_CONTACTS_INITIAL_TARGET,
        'text': 'Data Creation Test',
        'company': DEFAULT_COMPANY_ID,
    }

    update_date_to_send = generate_day_to_date('python_format')

    data_for_update = {
        'target_contacts': IDs_OF_CONTACTS_UPDATED_TARGET,  # Reduce the number of targets to 1 and add a new contact.
        'status': 'queued',
        'date_to_send': update_date_to_send,
    }

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        for contact_id in (cls.IDs_OF_CONTACTS_INITIAL_TARGET + cls.IDs_OF_CONTACTS_UPDATED_TARGET):
            assert Contact.objects.get(id=contact_id)

    def test_create_and_update_cycles(self):

        # Data Creation
        serializer = ScheduledEmailSerializer(data=self.data_for_create)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()

        # Assertion Related To Created Instance.
        self.assertTrue(isinstance(instance.id, UUID))
        self.assertEqual(self.data_for_create['subject'], instance.subject)
        self.assertEqual(self.data_for_create['send_copy_to_sender'], instance.send_copy_to_sender)
        self.assertEqual(self.data_for_create['status'], instance.scheduled_email_status_name)
        self.assertEqual(self.data_for_create['subject'], instance.subject)
        self.assertCountEqual(
            self.data_for_create['target_contacts'],
            [str(target.contact.id) for target in instance.targets.all()]  # Cast UUID object to str.
        )
        self.assertEqual(self.data_for_create['text'], instance.text)
        self.assertEqual(self.data_for_create['company'], str(instance.company.id))  # Cast UUID object to str.
        self.assertIsNone(instance.date_to_send)

        # Assertion Related To Database State.
        self.assertEqual(1, ScheduledEmail.objects.count())
        self.assertEqual(2, ScheduledEmailTarget.objects.count())
        self.assertEqual(2, ScheduledEmailTarget.objects.filter(email=instance.id, contact_id__in=self.IDs_OF_CONTACTS_INITIAL_TARGET).count())

        # Next, update the Email.
        serializer_for_update = ScheduledEmailSerializer(
            instance=instance,
            data=self.data_for_update,
            partial=True
        )
        serializer_for_update.is_valid(raise_exception=True)
        instance = serializer_for_update.save()

        # Assertion Related To Updated Instance.
        # Check the updated fields.
        self.assertEqual(self.data_for_update['status'], instance.scheduled_email_status_name)

        self.assertEqual(self.data_for_update['date_to_send'], str(instance.date_to_send))
        self.assertCountEqual(
            self.data_for_update['target_contacts'],
            [str(target.contact.id) for target in instance.targets.all()]  # Cast UUID object to str.
        )

        # Make sure that other field is not affected by update.
        self.assertTrue(isinstance(instance.id, UUID))
        self.assertEqual(self.data_for_create['subject'], instance.subject)
        self.assertEqual(self.data_for_create['send_copy_to_sender'], instance.send_copy_to_sender)
        self.assertEqual(self.data_for_create['text'], instance.text)
        self.assertEqual(self.data_for_create['company'], str(instance.company.id))  # Cast UUID object to str.

        # Assertion Related To Database State.
        self.assertEqual(1, ScheduledEmail.objects.count())
        self.assertEqual(1, ScheduledEmailTarget.objects.count())
        self.assertEqual(0, ScheduledEmailTarget.objects.filter(email=instance.id,
                                                                contact_id__in=self.IDs_OF_CONTACTS_INITIAL_TARGET).count())
        self.assertEqual(1, ScheduledEmailTarget.objects.filter(email=instance.id,
                                                                contact_id__in=self.IDs_OF_CONTACTS_UPDATED_TARGET).count())


class ScheduledEmailAttachmentSerializerTestCase(MediaFolderSetupMixin, SerializerTestCase):
    _serializer_class = ScheduledEmailAttachmentSerializer

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
        'app_staffing/tests/fixtures/scheduled_email_attachments.json',
    ]

    maxDiff = None

    def setUp(self):
        os.makedirs(settings.MEDIA_ROOT + '/' + MAIL_FIXTURE_ATTACHMENT_DIR_PATH, exist_ok=True)
        shutil.copy2('app_staffing/tests/files/sample.txt', settings.MEDIA_ROOT + '/' + MAIL_FIXTURE_ATTACHMENT_PATH)

        # Assert fixture data
        obj = ScheduledEmailAttachment.objects.get(id=EMAIL_ATTACHMENTS_ID)
        # Check if an attachment object for test exists.
        assert (obj.file == MAIL_FIXTURE_ATTACHMENT_PATH), "Fixture's attachment is different from test constant."
        assert (str(obj.email.id) == SOURCE_EMAIL_ID), "Fixture's email ID (reference) is different from test constant."

    def test_attachment_representation(self):

        expected = {
            'id': EMAIL_ATTACHMENTS_ID,
            'name': "sample.txt",
            'attached_mail_id': UUID(SOURCE_EMAIL_ID),
            'file_type': 1,
            'gcp_link': None,
            'status': 1
        }

        obj = ScheduledEmailAttachment.objects.get(id=EMAIL_ATTACHMENTS_ID)
        serializer = self._serializer_class(obj)
        self.assertDictEqual(expected, serializer.data)

    def test_create_attachment(self):
        f = open('app_staffing/tests/files/sample.txt', "rb")

        # Create a test data.
        data = {
           'file': File(f, 'sample.txt'),
           'company': DEFAULT_COMPANY_ID,
        }

        email = ScheduledEmail.objects.get(id=SOURCE_EMAIL_ID)

        # Execute test.
        serializer = ScheduledEmailAttachmentSerializer(data=data)
        self.assertTrue(serializer.is_valid(), msg=serializer.errors)
        created_instance = serializer.save(email=email) # Pass email as an additional parameter.

        # Assert if the created model is correct.
        db_instance = ScheduledEmailAttachment.objects.get(id=created_instance.id)

        self.assertTrue(os.path.basename(db_instance.file.name).startswith('sample'))
        self.assertTrue(os.path.basename(db_instance.file.name).endswith('.txt'))
        self.assertEqual(SOURCE_EMAIL_ID, str(db_instance.email.id))


class ScheduledEmailPreviewSerializerTestCase(SerializerTestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/scheduled_emails.json',
    ]

    _serializer_class = ScheduledEmailPreviewSerializer
    maxDiff = None
    TEST_INSTANCE_ID = '5737b0a8-5a99-4254-8798-b8c4a475cc2f'

    # Node: Expected result depends on fixture data.
    EXPECTED_BODY = '所属取引先\n'\
                    '取引先担当者 様\n' \
                    '\n' \
                    'blah blah\n' \
                    '\n' \
                    '***********\n' \
                    'An Admin of example.com\n' \
                    '***********\n\n'

    EXPECTED_HTML_BODY = '''
<pre>
所属取引先
取引先担当者 様
</pre>
<br />
blah blah<br />
<br />
<br />
    <span style="padding: 5px;display: inline-block;background-color:#f5f5f5;width: 68%;">
        <img src='https://storage.googleapis.com/umail-development-static/cmrb.png' style="width: 29%;"/>
        <span style="font-size:x-small;vertical-align:bottom;height: 20px;display: inline-block;">
            普段のメール業務を、より簡単に
        </span>
        <span style="margin-left:50px;margin-right:10px;margin-top:15px;background-color:#ffffe0;padding-left:15px;padding-right:15px;display: inline-block;float: right;">
            <a style="color:#000000;" href='https://cmrb.jp' target="_blank" rel="noopener noreferrer">
                詳しく
            </a>
        </span>
    </span>
<br />

<pre>***********
An Admin of example.com
***********
</pre>
'''

    def test_representation(self):
        instance = ScheduledEmail.objects.get(id=self.TEST_INSTANCE_ID)
        serializer = self._serializer_class(instance=instance)

        self.assertEqual(instance.subject, serializer.data['subject'])
        self.assertEqual(instance.sender.email, serializer.data['sender_email'])
        self.assertEqual(self.EXPECTED_BODY, serializer.data['body'])

    def test_html_representation(self):
        instance = ScheduledEmail.objects.get(id=self.TEST_INSTANCE_ID)
        instance.text_format = 'html'
        serializer = self._serializer_class(instance=instance)
        self.assertEqual(instance.subject, serializer.data['subject'])
        self.assertEqual(instance.sender.email, serializer.data['sender_email'])
        self.assertEqual(self.EXPECTED_HTML_BODY, serializer.data['body'])
