from django.test import TestCase

from django.core.management import call_command
from unittest.mock import Mock, patch
from django.conf import settings
from app_staffing.models import Company


class CalculateCommandTestCase(TestCase):

    def test_calculate_stats(self):
        with self.assertRaises(SystemExit) as command:
            call_command(command_name='calculate_stats')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)


class CalculateCommandErrorTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
    ]

    def test_calculate_stats(self):
        mock = Mock(side_effect=[Exception('Boom!'), {}, {}])
        with patch('app_staffing.services.stats.OrganizationStats.calc_stats', mock):
            with self.assertLogs(logger='', level='ERROR') as cm:
                with self.assertRaises(SystemExit) as command:
                    call_command('calculate_stats')

                self.assertEqual(command.exception.code, 0)
                self.assertEqual(len(cm.output), 2)
                self.assertIn('Exception occurred. ErrorDetails:Boom!', cm.output[0])
                self.assertIn(str(Company.objects.all()[0].id), cm.output[0])
