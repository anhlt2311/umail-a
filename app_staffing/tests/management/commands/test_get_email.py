from django.core.management import call_command
from django.test import TestCase

import django_mailbox
from django_mailbox.models import Mailbox, Message, MessageAttachment

from app_staffing.models import Company, MailboxMapping, Plan
from app_staffing.tests.settings import DEFAULT_COMPANY_ID

from unittest.mock import Mock, patch
from django.conf import settings

from email import message
from app_staffing.management.commands.get_mail import _set_payload
from email import charset as _charset
from email import header
from django_mailbox import utils

class GetMailCommandTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/django_mailbox_mailboxes.json',
        'app_staffing/tests/fixtures/mailbox_mappings.json',
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        assert Company.objects.get(id=DEFAULT_COMPANY_ID)
        assert Mailbox.objects.get(id=1)
        assert MailboxMapping.objects.get(company__id=DEFAULT_COMPANY_ID, mailbox__id=1)

    def test_command_calls(self):
        with self.assertRaises(SystemExit) as command:
            # Use dry-run, or test will try to connect the actual IMAP server, and fails.
            call_command('get_mail', '--dry-run')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)

class GetMailCommandErrorTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/django_mailbox_mailboxes.json',
        'app_staffing/tests/fixtures/mailbox_mappings.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        assert Company.objects.get(id=DEFAULT_COMPANY_ID)
        assert Mailbox.objects.get(id=1)
        assert MailboxMapping.objects.get(company__id=DEFAULT_COMPANY_ID, mailbox__id=1)

    def test_command_calls_with_error(self):
        Plan.objects.create(
            company_id=DEFAULT_COMPANY_ID,
            plan_master_id=2,
            is_active=True,
        )
        mock = Mock(side_effect=[Exception('Boom!'), []])
        with patch('django_mailbox.models.Mailbox.get_new_mail', mock):
            with self.assertLogs(logger='', level='ERROR') as cm:
                with self.assertRaises(SystemExit) as command:
                    call_command('get_mail')

                self.assertEqual(command.exception.code, 0)
                self.assertEqual(len(cm.output), 1)
                self.assertIn('Exception: Boom!', cm.output[0])
                self.assertIn(str(MailboxMapping.objects.all()[0].company.id), cm.output[0])

class MessageEncodeTestCase(TestCase):
    def test_encode(self):
        payload_utf_8 = '\r\n\u682a\u5f0f\u4f1a\u793e\u30d8\u30eb\u30b9\u30d9\u30a4\u30b7\u30b9\r\n\u3000\u3054\u62c5\u5f53\u8005\u69d8\r\n\r\n\u3044\u3064\u3082\u304a\u4e16\u8a71\u306b\u306a\u3063\u3066\u304a\u308a\u307e\u3059\u3002\r\n\u30cd\u30af\u30b9\u30c8\u30d7\u30e9\u30b9\u306e\u5c71\u7530\u3067\u3059\u3002\r\n\r\n\u4e0b\u8a18\u3001\u6848\u4ef6\u306b\u3064\u304d\u307e\u3057\u3066\r\n\u30a8\u30f3\u30b8\u30cb\u30a2\u3092\u52df\u96c6\u3057\u3066\u304a\u308a\u307e\u3059\r\n\r\n\u30b9\u30ad\u30eb\u30de\u30c3\u30c1\u3059\u308b\u65b9\u304c\u3044\u3089\u3063\u3057\u3083\u3044\u307e\u3057\u305f\u3089\r\n\u305c\u3072\u3001\u3054\u7d39\u4ecb\u3092\u304a\u9858\u3044\u81f4\u3057\u307e\u3059\u3002\r\n\r\n\u6050\u7e2e\u3067\u306f\u3054\u3056\u3044\u307e\u3059\u304c\r\n\u5f53\u30e1\u30fc\u30eb\u306e\u8fd4\u4fe1\u306b\u3066\r\n\u3054\u63d0\u6848\u3092\u304a\u9858\u3044\u81f4\u3057\u307e\u3059\u3002\r\n\r\n\r\n\u3010\uff2e\uff4f\u3011\u3000\u3000\u3000\u300020210507-16\r\n\u3010\u8077\u7a2e\u3011\u3000\u3000\u3000\u3000\u6d77\u5916EC\u30b5\u30a4\u30c8 PM/SE\u52df\u96c6\r\n\u3010\u4f5c\u696d\u5834\u6240\u3011\u3000\u3000\u65b0\u5bbf \uff08\u203b\u57fa\u672c\u30d5\u30eb\u30ea\u30e2\u30fc\u30c8\u52e4\u52d9\u3002\u6708\u306b\u6570\u56de\u306e\u6253\u3061\u5408\u308f\u305b\u3067\u51fa\u793e\u306e\u53ef\u80fd\u6027\u6709\u308a\u3002\uff09\r\n\u3010\u4f5c\u696d\u671f\u9593\u3011\u3000\u3000\u5373\u65e5 or 5\u6708\u301c\u9577\u671f\r\n\u3010\u5951\u7d04\u91d1\u984d\u3011\u3000\u3000PM 95\u4e07\uff0f\u6708\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000SE 80\u4e07\uff0f\u6708\r\n\u3010\u7cbe\u7b97\u3011\u3000\u3000\u3000\u3000140-180\r\n\u3010\u696d\u52d9\u5185\u5bb9\u3011\u3000\u3000\u6d77\u5916EC\u30b5\u30a4\u30c8\u69cb\u7bc9\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fb\u8981\u4ef6\u306e\u30d2\u30a2\u30ea\u30f3\u30b0\u304b\u3089\u3001\u696d\u52d9\u30d5\u30ed\u30fc\u3084\u6a5f\u80fd\u4e00\u89a7\u3092\u4f5c\u6210 \r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fb\u8ab2\u984c\u7ba1\u7406\u8868\u3001\u9032\u6357\u7ba1\u7406\u8868\u3092\u4f5c\u6210\u3057\u30d7\u30ed\u30b8\u30a7\u30af\u30c8\u7ba1\u7406 \r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fb\u30c6\u30b9\u30c8\u4ed5\u69d8\u66f8\u3092\u542b\u3080\u3001\u82f1\u6587\u3067\u306e\u9ad8\u3044\u30c9\u30ad\u30e5\u30e1\u30f3\u30c6\u30fc\u30b7\u30e7\u30f3 \u203bPM\u306e\u307f\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fb\u6d77\u5916\u306e\u30d9\u30f3\u30c0\u30fc\u306b\u82f1\u8a9e\u3067\u306e\u4ed5\u69d8\u8aac\u660e\u30fb\u8981\u4ef6\u8abf\u6574 \u203bSE\r\n\u3010\u5fc5\u8981\u30b9\u30ad\u30eb\u3011\u3000\u30fb\u8981\u4ef6\u306e\u30d2\u30a2\u30ea\u30f3\u30b0\u304b\u3089\u3001\u696d\u52d9\u30d5\u30ed\u30fc\u3084\u6a5f\u80fd\u4e00\u89a7\u3092\u4f5c\u6210  \u203b\u5171\u901a\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fbWEB\u7cfb\u30b7\u30b9\u30c6\u30e0\u306e\u958b\u767a\u7d4c\u9a13  \u203b\u5171\u901a\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fb\u5c0f\u58f2\u308a\uff08EC\uff09\u3001\u6d41\u901a\u95a2\u9023\u30b7\u30b9\u30c6\u30e0\u306e\u30d7\u30ed\u30b8\u30a7\u30af\u30c8\u7d4c\u9a13 \u203b\u5171\u901a\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fb\u82f1\u8a9e\u529b\uff08\u8981\u4ef6\u5b9a\u7fa9\u53ef\u80fd\u306a\u30d3\u30b8\u30cd\u30b9\u30ec\u30d9\u30eb\uff09 \u203b\u5171\u901a\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fb\u30c6\u30b9\u30c8\u4ed5\u69d8\u66f8\u3092\u542b\u3080\u3001\u82f1\u6587\u3067\u306e\u9ad8\u3044\u30c9\u30ad\u30e5\u30e1\u30f3\u30c6\u30fc\u30b7\u30e7\u30f3\u529b \u203bPM\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fbEC\u30b5\u30a4\u30c8\u30d7\u30ed\u30b8\u30a7\u30af\u30c8\u306ePM\u7d4c\u9a13 \u203bPM\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fb\u6d77\u5916\u306e\u30d9\u30f3\u30c0\u30fc\u306b\u82f1\u8a9e\u3067\u306e\u4ed5\u69d8\u8aac\u660e\u30fb\u8981\u4ef6\u8abf\u6574 \u203bSE\r\n\u3010\u5c1a\u53ef\u30b9\u30ad\u30eb\u3011\u3000\u30fbEC\u30d7\u30e9\u30c3\u30c8\u30d5\u30a9\u30fc\u30e0\u300cMagento\u300d\u306e\u958b\u767a\u30d7\u30ed\u30b8\u30a7\u30af\u30c8\u7d4c\u9a13\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u30fbAWS\u306e\u57fa\u672c\u7684\u306a\u77e5\u898b\uff08\u30a4\u30f3\u30d5\u30e9\u30a8\u30f3\u30b8\u30cb\u30a2\u3078\u306e\u6307\u793a\u51fa\u3057\u304c\u53ef\u80fd\u306a\u30ec\u30d9\u30eb\uff09\r\n\u3010\u4eba\u6570\u3011\u3000\u3000\u3000\u3000PM 1\uff642\u540d\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000SE 2\u540d\r\n\u3010\u9762\u8ac7\u3011\u3000\u3000\u3000\u30001\u56de\uff08\u5373\u6c7a\uff09 \u203b\u30ea\u30e2\u30fc\u30c8\u9762\u8ac7\uff08Zoom\uff09\r\n\u3010\u305d\u306e\u4ed6\u3011\u3000\u3000\u3000\u52e4\u52d9\u6642\u9593\uff1a10:00\u301c19:00\uff08\u4f11\u61a960\u5206\uff09\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u52e4\u52d9\u66dc\u65e5\uff1a\u90315\u65e5\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u5e74\u9f62\uff1a45\u6b73\u304f\u3089\u3044\u307e\u3067\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u5099\u8003\uff1a\u670d\u88c5\u306f\u79c1\u670d\u3002\u5916\u56fd\u7c4d\u53ef\uff08\u30a4\u30f3\u30c9\u56fd\u7c4d\u306f\u9664\u304f\uff09\r\n\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000      \u8cb4\u793e\u6240\u5c5e\u69d8\u307e\u3067(\u500b\u4eba\u53ef)\r\n\r\n\r\n\u5b9c\u3057\u304f\u304a\u9858\u3044\u3044\u305f\u3057\u307e\u3059\u3002\r\n+++++++++++++++++++++++++++\r\n \u682a\u5f0f\u4f1a\u793e\u30cd\u30af\u30b9\u30c8\u30d7\u30e9\u30b9\r\n \u55b6\u696d\u90e8\u3000\u5c71\u7530\u667a\u4e4b\r\n Tel: 03-5577-5266  \u643a\u5e2f: 080-8107-7014\r\n HP: http://www.next-plus.jp\r\n Mail: yamada@next-plus.jp\r\n+++++++++++++++++++++++++++\r\n\r\n\r\n\r\n\r\n\r\n\r\n'

        # 最初から正しい文字コードが指定されていれば問題なくエンコードできる
        m = message.Message()
        m.set_payload(payload_utf_8, 'utf-8')
        self.assertEqual(m.get_charset(), 'utf-8')

        # 実際の文字コードと違うコードが指定された場合、元々の実装だとエンコードに失敗してExceptionを吐いてしまい、エンコードを諦めてしまう
        m = message.Message()
        try:
            m.set_payload(payload_utf_8, 'shift_jis')
            self.fail()
        except Exception as e:
            pass

        # patchしたコードに置き換えた場合は文字コードを総当たりでチェックして、複合に成功した文字コードがセットされる
        message.Message.set_payload = _set_payload
        m.set_payload(payload_utf_8, 'shift_jis')
        self.assertEqual(m.get_charset(), 'utf-8')

class GetNewMailTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/django_mailbox_mailboxes.json',
        'app_staffing/tests/fixtures/mailbox_mappings.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

    def test_connection_error(self):
        # get_connectionを呼び出した際、2回エラーを発生させ、3回目で突破できる
        mock = Mock(side_effect=[Exception('Boom!'), Exception('Boom!'), None])
        with patch('django_mailbox.models.Mailbox.get_connection', mock):
            with self.assertLogs(logger='', level='INFO') as cm:
                mailbox = django_mailbox.models.Mailbox.objects.get(id=1)
                # コネクションを貼るのに失敗した場合、リトライが発生する
                messages = mailbox.get_new_mail()
                for message in messages:
                    pass
                self.assertEqual(len(cm.output), 2)
                self.assertIn('Exception: Boom!', cm.output[0])
                self.assertIn('Location: example.com', cm.output[0])
                self.assertIn('Retry:0', cm.output[0])
                self.assertIn('Exception: Boom!', cm.output[1])
                self.assertIn('Location: example.com', cm.output[1])
                self.assertIn('Retry:1', cm.output[1])

class DehydratedMessageTestCase(TestCase):
    def test_dehydrate(self):
        msg = message.Message()
        h = header.Header(
            'attachment; hogehogehogehoge.xls',
            charset=_charset.UNKNOWN8BIT,
            header_name='Content-Disposition'
        )
        msg.set_raw('Content-Disposition', h)
        mailbox = Mailbox.objects.create(
            name='hogehoge',
            active=True,
        )
        record = Message.objects.create(
            subject='subject',
            message_id='test_message_id',
            from_header='',
            to_header='',
            outgoing=False,
            body='',
            encoded=True,
            processed=False,
            mailbox_id=mailbox.id
        )
        try:
            mailbox._get_dehydrated_message(msg, record)
            attachments = MessageAttachment.objects.all()
            self.assertEquals(len(attachments), 1)
            self.assertIn('Content-Disposition: =?unknown-8bit?q?attachment=3B_hogehogehogehoge=2Exls?=', str(attachments[0].headers))
        except TypeError:
            self.fail('content disposition cannot convert to str')

class GetBodyFromMessageTestCase(TestCase):
    def test_get_body(self):
        text = 'hogehoge'
        action = 'django_mailbox.models.Mailbox.get_connection'
        main_type = 'text'
        sub_type = 'plain'

        # content_dispositionをdecodeするとattachmentの文字列が入っているので、bodyはrenderされない
        msg1 = message.Message()
        msg1.set_raw(
            'Content-Disposition',
            '=?unknown-8bit?b?YXR0YWNobWVudDsgZmlsZW5hbWU9IuOCueOCreODq+OCt+ODvOODiF9ILlTvvIjnq4vpo5vvvIkueGxzeCI=?='
        )
        msg1.set_payload(text)
        mock1 = Mock(side_effect=[msg1])
        with patch(action, mock1):
            ret1 = utils.get_body_from_message(msg1, main_type, sub_type)
            self.assertEquals(ret1, '')

        # 通常であればbodyがrenderされる
        msg2 = message.Message()
        msg2.set_payload(text)
        mock2 = Mock(side_effect=[msg2])
        with patch(action, mock2):
            ret2 = utils.get_body_from_message(msg2, main_type, sub_type)
            self.assertEquals(ret2, text)
