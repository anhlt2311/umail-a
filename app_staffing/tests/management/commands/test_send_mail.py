from unittest.mock import Mock, patch

from django.core.management import call_command
from django.test import TestCase

from app_staffing.models import ScheduledEmail, Contact
from app_staffing.models.email import QUEUED, SENDING, SENT, ERROR, TIMEOUT, ScheduledEmailStatus, ScheduledEmailSendError
from app_staffing.models.organization import Company, CompanyAttribute

from app_staffing.tests.factories.company import CompanyFactory
from app_staffing.tests.factories.email import SMTPServerFactory, create_scheduled_mail_data

from uuid import uuid4
from app_staffing.tests.factories.user import AppUserFactory
from django.conf import settings

from app_staffing.tests.factories.organization import ContactFactory

from datetime import datetime, timedelta
from django.utils import timezone
from unittest import skip
from app_staffing.tests.settings import DEFAULT_COMPANY_ID

TEST_FILE_PATH = 'SendMailAttachment.txt'
TEST_MAIL_SENDER_ADDRESS = 'testMailSender@example.com'
TEST_MAIL_SENDER_FIRST_NAME = 'First Name'
TEST_MAIL_SENDER_SIGNATURE = "***********\n A Signature\n ***********\n"
TEST_MAIL_SUBJECT = 'This is a Test Email'
TEST_MAIL_TEXT = 'This is a main content of the email'
TEST_MAIL_ATTACHMENT_BODY = 'file content'
TEST_MAIL_ATTACHMENT_TYPE = 'text/plain'


SCHEDULED_MAIL_COUNT = 1

class PutMailTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    attachment = None

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.company = CompanyFactory.create()
        # send_mail function will search an SMTP server which corresponds to sender's company.
        SMTPServerFactory.create(company=cls.company)

        cls.scheduled_mail, cls.attachment = create_scheduled_mail_data(
            company=cls.company,
            sender_address=TEST_MAIL_SENDER_ADDRESS,
            sender_first_name=TEST_MAIL_SENDER_FIRST_NAME,
            sender_signature=TEST_MAIL_SENDER_SIGNATURE,
            subject=TEST_MAIL_SUBJECT,
            content=TEST_MAIL_TEXT,
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
        )
        cls.scheduled_mail.scheduled_email_status = ScheduledEmailStatus.objects.get(name=QUEUED)
        cls.scheduled_mail.save()
        # Assert test data first.
        assert ScheduledEmail.objects.count() == SCHEDULED_MAIL_COUNT
        assert ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).count() == SCHEDULED_MAIL_COUNT

    @classmethod
    def tearDownClass(cls):
        if cls.attachment:
            cls.attachment.file.delete()
        super().tearDownClass()

    def test_put_a_mail(self):
        mock = Mock(return_value={ 'has_error': False })
        with patch('app_staffing.management.commands.send_mail.put_mails', mock):
            with self.assertRaises(SystemExit) as command:
                call_command('send_mail')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)

        # Make sure that all mail is marked as SENT.
        self.assertEqual(ScheduledEmail.objects.count(), SCHEDULED_MAIL_COUNT)
        self.assertEqual(ScheduledEmail.objects.filter(status='tasks').count(), SCHEDULED_MAIL_COUNT)

        sent_mail = ScheduledEmail.objects.get(status='tasks')
        self.assertEqual(sent_mail.send_total_count, None)

class PutQueuedMailsTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    attachment = None

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.company = CompanyFactory.create()
        # send_mail function will search an SMTP server which corresponds to sender's company.
        SMTPServerFactory.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1')

        date_to_send = timezone.now()
        for i in range(1, 10):
            scheduled_mail = ScheduledEmail(
                sender_id='1957ab0f-b47c-455a-a7b7-4453cfffd05e',
                subject='test subject',
                text=f'{i}',
                date_to_send=(date_to_send - timedelta(days=i)),
                company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
                text_format='text',
                scheduled_email_status_id=2
            )
            scheduled_mail.save()

    def test_command_send(self):
        mock = Mock(return_value={'has_error': False})
        with patch('app_staffing.management.commands.send_mail.put_mails', mock):
            with self.assertRaises(SystemExit) as command:
                call_command('send_mail')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)

        # Make sure that all mail is marked as SENT.
        self.assertEqual(ScheduledEmail.objects.count(), 9)
        self.assertEqual(ScheduledEmail.objects.filter(status='tasks').count(), 9)

class PutMailCommandWithMultiTennantTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    attachment = None

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.company = CompanyFactory.create()
        cls.another_company = CompanyFactory.create(id=uuid4())  # For multi-tenancy test.
        cls.user_of_another_company = AppUserFactory(
            company=cls.another_company,
            username='user@another.com',
            email='user@another.com'
        )
        # send_mail function will search an SMTP server which corresponds to sender's company.
        SMTPServerFactory.create(company=cls.company)
        SMTPServerFactory.create(company=cls.another_company)

        cls.scheduled_mail, cls.attachment = create_scheduled_mail_data(
            company=cls.company,
            sender_address=TEST_MAIL_SENDER_ADDRESS,
            sender_first_name=TEST_MAIL_SENDER_FIRST_NAME,
            sender_signature=TEST_MAIL_SENDER_SIGNATURE,
            subject=TEST_MAIL_SUBJECT,
            content=TEST_MAIL_TEXT,
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
        )
        cls.another_scheduled_mail, cls.another_attachment = create_scheduled_mail_data(
            company=cls.another_company,
            sender_address='testAnotherMailSender@example.com',
            sender_first_name=TEST_MAIL_SENDER_FIRST_NAME,
            sender_signature=TEST_MAIL_SENDER_SIGNATURE,
            subject=TEST_MAIL_SUBJECT,
            content=TEST_MAIL_TEXT,
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
        )

        cls.scheduled_mail.scheduled_email_status = ScheduledEmailStatus.objects.get(name=QUEUED)
        cls.another_scheduled_mail.scheduled_email_status = ScheduledEmailStatus.objects.get(name=QUEUED)

        cls.scheduled_mail.save()
        cls.another_scheduled_mail.save()
        # Assert test data first.
        assert ScheduledEmail.objects.count() == 2
        assert ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).count() == 2

    def test_put_mails(self):
        mock = Mock(return_value={ 'has_error': False })
        with patch('app_staffing.management.commands.send_mail.put_mails', mock):
            with self.assertRaises(SystemExit) as command:
                call_command('send_mail')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)

        # Make sure that all mail is marked as SENT.
        self.assertEqual(ScheduledEmail.objects.count(), 2)
        self.assertEqual(ScheduledEmail.objects.filter(status='tasks').count(), 2)

    def test_one_of_them_causes_error(self):
        mock = Mock(side_effect=[
            { 'has_error': False },
            Exception('Unknown Cloud Tasks Error')
        ])

        with patch('app_staffing.management.commands.send_mail.put_mails', mock):
            with self.assertRaises(SystemExit) as command:
                call_command('send_mail')

        self.assertEqual(command.exception.code, 0)

        # Make sure that all mail is marked as SENT.
        self.assertEqual(ScheduledEmail.objects.count(), 2)
        self.assertEqual(ScheduledEmail.objects.filter(status='tasks').count(), 1)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=ERROR).count(), 1)

    def test_all_of_them_causes_error(self):
        mock = Mock(side_effect=Exception('Unknown Cloud Tasks Error'))

        with patch('app_staffing.management.commands.send_mail.put_mails', mock):
            with self.assertRaises(SystemExit) as command:
                call_command('send_mail')

        self.assertEqual(command.exception.code, 0)

        # Make sure that all mail is marked as SENT.
        self.assertEqual(ScheduledEmail.objects.count(), 2)
        self.assertEqual(ScheduledEmail.objects.filter(status='tasks').count(), 0)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=ERROR).count(), 2)



class SendMailCommandTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    attachment = None

    @classmethod
    def setUpTestData(cls):
        settings.SENDING_MAIL_METHOD = 'SMTP'
        super().setUpTestData()

        cls.company = CompanyFactory.create()
        # send_mail function will search an SMTP server which corresponds to sender's company.
        SMTPServerFactory.create(company=cls.company)

        cls.scheduled_mail, cls.attachment = create_scheduled_mail_data(
            company=cls.company,
            sender_address=TEST_MAIL_SENDER_ADDRESS,
            sender_first_name=TEST_MAIL_SENDER_FIRST_NAME,
            sender_signature=TEST_MAIL_SENDER_SIGNATURE,
            subject=TEST_MAIL_SUBJECT,
            content=TEST_MAIL_TEXT,
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
        )
        cls.scheduled_mail.scheduled_email_status = ScheduledEmailStatus.objects.get(name=QUEUED)
        cls.scheduled_mail.save()
        # Assert test data first.
        assert ScheduledEmail.objects.count() == SCHEDULED_MAIL_COUNT
        assert ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).count() == SCHEDULED_MAIL_COUNT

    @classmethod
    def tearDownClass(cls):
        if cls.attachment:
            cls.attachment.file.delete()
        super().tearDownClass()

    def test_command_send(self):
        mock = Mock(return_value={ 'send_total_count': 1 })
        with patch('app_staffing.management.commands.send_mail.send_mails', mock):
            with self.assertRaises(SystemExit) as command:
                call_command('send_mail')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)

        # Make sure that all mail is marked as SENT.
        self.assertEqual(ScheduledEmail.objects.count(), SCHEDULED_MAIL_COUNT)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).count(), 0)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=SENT).count(), SCHEDULED_MAIL_COUNT)

        sent_mail = ScheduledEmail.objects.get(scheduled_email_status__name=SENT)
        self.assertEqual(sent_mail.send_total_count, SCHEDULED_MAIL_COUNT)

    def test_command_send_deactivated_tenant(self):
        company = Company.objects.all()[0]
        company.deactivated_time = datetime.now()
        company.save()

        CompanyAttribute.objects.create(
            company_id=company.id,
            user_registration_limit=5,
        )

        mock = Mock(return_value={ 'send_total_count': 1 })
        with patch('app_staffing.management.commands.send_mail.send_mails', mock):
            with self.assertRaises(SystemExit) as command:
                call_command('send_mail')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)

        self.assertEqual(ScheduledEmail.objects.count(), SCHEDULED_MAIL_COUNT)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).count(), 1)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=SENT).count(), 0)


class SendMailCommandCleanUpTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    attachment = None

    @classmethod
    def setUpTestData(cls):
        settings.SENDING_MAIL_METHOD = 'SMTP'
        super().setUpTestData()

        cls.company = CompanyFactory.create()
        # send_mail function will search an SMTP server which corresponds to sender's company.
        SMTPServerFactory.create(company=cls.company)

        cls.scheduled_mail, cls.attachment = create_scheduled_mail_data(
            company=cls.company,
            sender_address=TEST_MAIL_SENDER_ADDRESS,
            sender_first_name=TEST_MAIL_SENDER_FIRST_NAME,
            sender_signature=TEST_MAIL_SENDER_SIGNATURE,
            subject=TEST_MAIL_SUBJECT,
            content=TEST_MAIL_TEXT,
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
            create_timeout_data=True,
        )
        cls.scheduled_mail.scheduled_email_status = ScheduledEmailStatus.objects.get(name=SENDING)
        cls.scheduled_mail.save()
        # Assert test data first.
        assert ScheduledEmail.objects.count() == SCHEDULED_MAIL_COUNT
        assert ScheduledEmail.objects.filter(scheduled_email_status__name=SENDING).count() == SCHEDULED_MAIL_COUNT

    @classmethod
    def tearDownClass(cls):
        if cls.attachment:
            cls.attachment.file.delete()
        super().tearDownClass()

    def test_command_send(self):
        with self.assertRaises(SystemExit) as command:
            call_command('send_mail')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)

        # Make sure that all sending mail is marked as TIMEOUT.
        self.assertEqual(ScheduledEmail.objects.count(), SCHEDULED_MAIL_COUNT)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=SENDING).count(), 0)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=ERROR).count(), SCHEDULED_MAIL_COUNT)


class SendMailCommandErrorTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    attachment = None

    @classmethod
    def setUpTestData(cls):
        settings.SENDING_MAIL_METHOD = 'SMTP'
        super().setUpTestData()

        cls.company = CompanyFactory.create()
        # send_mail function will search an SMTP server which corresponds to sender's company.
        SMTPServerFactory.create(company=cls.company)

        cls.scheduled_mail, cls.attachment = create_scheduled_mail_data(
            company=cls.company,
            sender_address=TEST_MAIL_SENDER_ADDRESS,
            sender_first_name=TEST_MAIL_SENDER_FIRST_NAME,
            sender_signature=TEST_MAIL_SENDER_SIGNATURE,
            subject=TEST_MAIL_SUBJECT,
            content=TEST_MAIL_TEXT,
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
        )
        cls.scheduled_mail.scheduled_email_status = ScheduledEmailStatus.objects.get(name=QUEUED)
        cls.scheduled_mail.save()
        # Assert test data first.
        assert ScheduledEmail.objects.count() == SCHEDULED_MAIL_COUNT
        assert ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).count() == SCHEDULED_MAIL_COUNT

        cls.contact = ContactFactory(company=cls.company)

    @classmethod
    def tearDownClass(cls):
        if cls.attachment:
            cls.attachment.file.delete()
        super().tearDownClass()

    def test_command_behavior_on_send_mail_error(self):
        mock = Mock(side_effect=Exception('Boom!'))
        with patch('app_staffing.management.commands.send_mail.send_mails', mock):
            with self.assertLogs(logger='', level='ERROR') as cm:
                with self.assertRaises(SystemExit) as command:
                    call_command('send_mail')

            self.assertEqual(len(cm.output), 3)
            self.assertIn('ERROR', cm.output[0])
            self.assertIn(DEFAULT_COMPANY_ID, cm.output[0])
            self.assertIn('ERROR', cm.output[1])
            self.assertIn(DEFAULT_COMPANY_ID, cm.output[1])
            self.assertIn('ERROR', cm.output[2])
            self.assertIn(DEFAULT_COMPANY_ID, cm.output[2])

        # Make sure that all mail is marked as ERROR.
        self.assertEqual(ScheduledEmail.objects.count(), SCHEDULED_MAIL_COUNT)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).count(), 0)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=ERROR).count(), SCHEDULED_MAIL_COUNT)

    def test_command_behavior_on_email_message_send_error(self):
        contact_id = Contact.objects.all()[0].id
        mock = Mock(return_value={ 'send_total_count': 1 })
        with patch('app_staffing.management.commands.send_mail.send_mails', mock):
            with self.assertRaises(SystemExit) as command:
                call_command('send_mail')
            self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)

        # Make sure that all mail is marked as ERROR.
        self.assertEqual(ScheduledEmail.objects.count(), SCHEDULED_MAIL_COUNT)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).count(), 0)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=SENT).count(), SCHEDULED_MAIL_COUNT)

class SendMailCommandWithMultiTennantTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    attachment = None

    @classmethod
    def setUpTestData(cls):
        settings.SENDING_MAIL_METHOD = 'SMTP'
        super().setUpTestData()

        cls.company = CompanyFactory.create()
        cls.another_company = CompanyFactory.create(id=uuid4())  # For multi-tenancy test.
        cls.user_of_another_company = AppUserFactory(
            company=cls.another_company,
            username='user@another.com',
            email='user@another.com'
        )
        # send_mail function will search an SMTP server which corresponds to sender's company.
        SMTPServerFactory.create(company=cls.company)
        SMTPServerFactory.create(company=cls.another_company)

        cls.scheduled_mail, cls.attachment = create_scheduled_mail_data(
            company=cls.company,
            sender_address=TEST_MAIL_SENDER_ADDRESS,
            sender_first_name=TEST_MAIL_SENDER_FIRST_NAME,
            sender_signature=TEST_MAIL_SENDER_SIGNATURE,
            subject=TEST_MAIL_SUBJECT,
            content=TEST_MAIL_TEXT,
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
        )
        cls.another_scheduled_mail, cls.another_attachment = create_scheduled_mail_data(
            company=cls.another_company,
            sender_address='testAnotherMailSender@example.com',
            sender_first_name=TEST_MAIL_SENDER_FIRST_NAME,
            sender_signature=TEST_MAIL_SENDER_SIGNATURE,
            subject=TEST_MAIL_SUBJECT,
            content=TEST_MAIL_TEXT,
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
        )

        cls.scheduled_mail.scheduled_email_status = ScheduledEmailStatus.objects.get(name=QUEUED)
        cls.another_scheduled_mail.scheduled_email_status = ScheduledEmailStatus.objects.get(name=QUEUED)

        cls.scheduled_mail.save()
        cls.another_scheduled_mail.save()
        # Assert test data first.
        assert ScheduledEmail.objects.count() == 2
        assert ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).count() == 2

    @classmethod
    def tearDownClass(cls):
        if cls.attachment:
            cls.attachment.file.delete()
        super().tearDownClass()

    def test_command_behavior_on_send_mail_error(self):
        mock = Mock(side_effect=[
            'hogehoge',
            { 'send_total_count': 1 },
        ])
        with patch('app_staffing.management.commands.send_mail.send_mails', mock):
            with self.assertLogs(logger='', level='ERROR') as cm:
                with self.assertRaises(SystemExit) as command:
                    call_command('send_mail')

            self.assertEqual(ScheduledEmail.objects.count(), 2)
            self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=SENT).count(), 1)
            self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=ERROR).count(), 1)
            self.assertNotEqual(
                ScheduledEmail.objects.filter(scheduled_email_status__name=ERROR)[0].company.id,
                ScheduledEmail.objects.filter(scheduled_email_status__name=SENT)[0].company.id
            )

            error_company_id = ScheduledEmail.objects.filter(scheduled_email_status__name=ERROR)[0].company.id
            self.assertEqual(len(cm.output), 3)
            self.assertIn('ERROR', cm.output[0])
            self.assertIn(str(error_company_id), cm.output[0])
            self.assertIn('ERROR', cm.output[1])
            self.assertIn(str(error_company_id), cm.output[1])
            self.assertIn('ERROR', cm.output[2])
            self.assertIn(str(error_company_id), cm.output[2])

class SendQueuedMailTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/organizations.json',
        'app_staffing/tests/fixtures/contacts.json',
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    attachment = None

    @classmethod
    def setUpTestData(cls):
        settings.SENDING_MAIL_METHOD = 'SMTP'
        super().setUpTestData()

        cls.company = CompanyFactory.create()
        # send_mail function will search an SMTP server which corresponds to sender's company.
        SMTPServerFactory.create(company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1')

        date_to_send = timezone.now()
        for i in range(1, 10):
            scheduled_mail = ScheduledEmail(
                sender_id='1957ab0f-b47c-455a-a7b7-4453cfffd05e',
                subject='test subject',
                text=f'{i}',
                date_to_send=(date_to_send - timedelta(days=i)),
                company_id='3efd25d0-989a-4fba-aa8d-b91708760eb1',
                text_format='text',
                scheduled_email_status_id=2
            )
            scheduled_mail.save()

    def test_command_send(self):
        mock = Mock(return_value={ 'send_total_count': 1, 'send_success_count': 1, 'send_error_count': 0, 'send_error_contacts': [] })
        with patch('app_staffing.management.commands.send_mail.send_mails', mock):
            with self.assertRaises(SystemExit) as command:
                call_command('send_mail')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)

        # Make sure that all mail is marked as SENT.
        self.assertEqual(ScheduledEmail.objects.count(), 9)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=QUEUED).count(), 0)
        self.assertEqual(ScheduledEmail.objects.filter(scheduled_email_status__name=SENT).count(), 9)
        scheduled_emails = ScheduledEmail.objects.filter(scheduled_email_status__name=SENT).order_by('sent_date')

        self.assertEqual(scheduled_emails[0].text, '9')
        self.assertEqual(scheduled_emails[1].text, '8')
        self.assertEqual(scheduled_emails[2].text, '7')
        self.assertEqual(scheduled_emails[3].text, '6')
        self.assertEqual(scheduled_emails[4].text, '5')
        self.assertEqual(scheduled_emails[5].text, '4')
        self.assertEqual(scheduled_emails[6].text, '3')
        self.assertEqual(scheduled_emails[7].text, '2')
        self.assertEqual(scheduled_emails[8].text, '1')

        self.assertTrue(scheduled_emails[0].date_to_send < scheduled_emails[1].date_to_send)
        self.assertTrue(scheduled_emails[1].date_to_send < scheduled_emails[2].date_to_send)
        self.assertTrue(scheduled_emails[2].date_to_send < scheduled_emails[3].date_to_send)
        self.assertTrue(scheduled_emails[3].date_to_send < scheduled_emails[4].date_to_send)
        self.assertTrue(scheduled_emails[4].date_to_send < scheduled_emails[5].date_to_send)
        self.assertTrue(scheduled_emails[5].date_to_send < scheduled_emails[6].date_to_send)
        self.assertTrue(scheduled_emails[6].date_to_send < scheduled_emails[7].date_to_send)
        self.assertTrue(scheduled_emails[7].date_to_send < scheduled_emails[8].date_to_send)
