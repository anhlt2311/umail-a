from django.test import TestCase

from django.core.management import call_command
from django.conf import settings
from rest_framework.authtoken.models import Token


class ForceLogoutCommandTestCase(TestCase):

    def test_calculate_stats(self):
        with self.assertRaises(SystemExit) as command:
            call_command(command_name='force_logout')
        self.assertEqual(command.exception.code, settings.JOB_RESULT_SUCCESS)
        # Tokenがないことを確認する
        self.assertEqual(Token.objects.all().count(), 0)
