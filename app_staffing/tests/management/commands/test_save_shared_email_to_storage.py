import json

from datetime import datetime, timedelta
from unittest.mock import Mock, patch
from freezegun import freeze_time
from softdelete.models import ChangeSet
from django_mailbox.models import Mailbox, Message

from django.conf import settings
from django.test import TestCase
from django.core.management import call_command
from django.core.files.storage import default_storage
from django.test import override_settings
from django.contrib.contenttypes.models import ContentType

from app_staffing.management.commands.save_shared_email_to_storage import Command
from app_staffing.models.organization import Company
from app_staffing.models.email import Email
from app_staffing.tests.factories.email import SharedEmailFactory


class SaveSharedEmailCommandTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/sharedEmailWithExpirationDate/emails.json',
    ]
    fake_current_date = datetime(2022, 4, 25)
    company_id = '3efd25d0-989a-4fba-aa8d-b91708760eb1'

    def test_save_shared_email_success(self):
        company_ids = ['3efd25d0-989a-4fba-aa8d-b91708760eb1', '0639df05-8db5-40f2-a25d-6bcd3f8ae4ca']
        expected_data = [
            {
                'count': 2,
                'email_ids': ['90ba907b-3102-4b71-a76d-640a0db3cf17', '90ba907b-3102-4b71-a76d-640a0db3cf18']
            },
            {
                'count': 1,
                'email_ids': ['90ba907b-3102-4b71-a76d-640a0db3cf19']
            },
        ]

        # run batch save shared email to storage
        with freeze_time(self.fake_current_date):
            path_key = call_command(command_name='save_shared_email_to_storage')
            file_paths = [f'{settings.MEDIA_ROOT}/' + Command.generate_upload_path(path_key, company_id, 1)
                          for company_id in company_ids]

        # check file upload success
        for index, path in enumerate(file_paths):
            self.assertEqual(default_storage.exists(path), True)

            # read file from storage and compare with expected data
            json_string = default_storage.open(path).read().decode()
            data = json.loads(json_string)
            expected = expected_data[index]
            self.assertEqual(data.get('count'), expected['count'])
            self.assertEqual(len(data.get('items')), len(expected['email_ids']))
            for email in data.get('items'):
                self.assertTrue(email.get('id') in expected['email_ids'])

            # delete file - only for the next test run, NOT IN ACTUAL FUNCTION
            default_storage.delete(path)

    def test_save_shared_email_error(self):
        mock = Mock(side_effect=Exception('Boom!'))
        with patch('django.core.files.storage.Storage.save', mock), freeze_time(self.fake_current_date):
            with self.assertLogs(logger='', level='ERROR') as cm:
                call_command('save_shared_email_to_storage')
                self.assertIn('ErrorDetails: Boom!', cm.output[0])

    @override_settings(SHARED_EMAIL_JSON_FILE_MAX_COUNT=10)
    def test_save_shared_email_with_large_data(self):
        total_email = 22
        self.prepare_shared_email(total_email, [self.company_id])

        count_of_shared_email_before_delete = Email.objects.filter(shared=True).count()
        self.assertEqual(count_of_shared_email_before_delete, total_email)

        # run batch save shared email to storage
        with freeze_time(self.fake_current_date):
            path_key = call_command(command_name='save_shared_email_to_storage')
            file_paths = [f'{settings.MEDIA_ROOT}/' + Command.generate_upload_path(path_key, self.company_id, index + 1)
                          for index in range(3)]

        # check file upload success
        for index, path in enumerate(file_paths):
            self.assertEqual(default_storage.exists(path), True)

            expected_count = settings.SHARED_EMAIL_JSON_FILE_MAX_COUNT \
                if total_email > settings.SHARED_EMAIL_JSON_FILE_MAX_COUNT else total_email
            total_email -= expected_count

            # read file from storage and compare with expected data
            json_string = default_storage.open(path).read().decode()
            data = json.loads(json_string)
            self.assertEqual(data.get('count'), expected_count)
            self.assertEqual(len(data.get('items')), expected_count)

            # delete file - only for the next test run, NOT IN ACTUAL FUNCTION
            default_storage.delete(path)

        count_of_shared_email_after_delete = Email.objects.filter(shared=True).count()
        self.assertEqual(count_of_shared_email_after_delete, 0)

    def test_roll_back_transaction(self):
        company_1, company_2 = self.company_id, '0639df05-8db5-40f2-a25d-6bcd3f8ae4ca'
        Company.objects.exclude(id__in=[company_1, company_2]).delete()
        self.prepare_shared_email(1, [company_1, company_2])

        msg_id_1, msg_id_2 = 'm1', 'm2'
        mail1 = Email.objects.filter(company_id=company_1).first()
        mail1.message_id = msg_id_1
        mail1.save()
        mail2 = Email.objects.filter(company_id=company_2).first()
        mail2.message_id = msg_id_2
        mail2.save()
        if 'sqlite3' in settings.DATABASES['default']['ENGINE']:
            email1_id = str(mail1.id).replace('-', '')
            email2_id = str(mail2.id).replace('-', '')
        else:
            email1_id = str(mail1.id)
            email2_id = str(mail2.id)

        # Fake Soft delete ChangeSet, for test roll back
        content_type = ContentType.objects.get(app_label='app_staffing', model='email')
        ChangeSet.objects.create(content_type=content_type, object_id=email1_id)
        ChangeSet.objects.create(content_type=content_type, object_id=email2_id)

        # Fake django_mailbox_message, for test roll back
        m1 = Mailbox.objects.create(name='Mailbox 1')
        Message.objects.create(mailbox=m1, subject='1', from_header='1', to_header='1', body='1', message_id=msg_id_1)
        m2 = Mailbox.objects.create(name='Mailbox 2')
        Message.objects.create(mailbox=m2, subject='2', from_header='2', to_header='2', body='2', message_id=msg_id_2)

        # Check data of company 1 before delete (ChangeSet, MailboxMessage, Email)
        kwarg1 = {'content_type': content_type, 'email_id': email1_id, 'message_id': 'm1'}
        self.compare_data(1, **kwarg1)
        count_of_email_1_before_delete = Email.objects.filter(company_id=company_1).count()
        self.assertEqual(count_of_email_1_before_delete, 1)

        # Check data of company 2 before delete (ChangeSet, MailboxMessage, Email)
        kwarg2 = {'content_type': content_type, 'email_id': email2_id, 'message_id': 'm2'}
        self.compare_data(1, **kwarg2)
        count_of_email_2_before_delete = Email.objects.filter(company_id=company_2).count()
        self.assertEqual(count_of_email_2_before_delete, 1)

        # run batch save shared email to storage
        mock = Mock(side_effect=[Exception('Boom!'), True])
        function_name = 'app_staffing.management.commands.save_shared_email_to_storage.Command.delete_email'
        with patch(function_name, mock), freeze_time(self.fake_current_date):
            path_key = call_command(command_name='save_shared_email_to_storage')
            file_path1 = f'{settings.MEDIA_ROOT}/' + Command.generate_upload_path(path_key, company_1, 1)
            file_path2 = f'{settings.MEDIA_ROOT}/' + Command.generate_upload_path(path_key, company_2, 1)

        default_storage.delete(file_path1)
        default_storage.delete(file_path2)

        # When error in transaction of company_1, Transaction will roll back => Data still exist
        self.compare_data(1, **kwarg1)
        count_of_email_1_after_delete = Email.objects.filter(company_id=company_1).count()
        self.assertEqual(count_of_email_1_after_delete, 1)

        # Transaction of company_2 execute success
        # => All data was removed, except Email because using Mock function with delete_email
        self.compare_data(0, **kwarg2)
        count_of_email_2_after_delete = Email.objects.filter(company_id=company_2).count()
        self.assertEqual(count_of_email_2_after_delete, 1)

    def prepare_shared_email(self, total_email, company_ids):
        # clear old email
        Email.objects.filter(shared=True).delete()

        # fake new Shared Email
        for company_id in company_ids:
            c = Company.objects.get(pk=self.company_id if not company_id else company_id)
            for _ in range(total_email):
                SharedEmailFactory(company=c, sent_date=self.fake_current_date - timedelta(days=200))

    def compare_data(self, expected_value, **kwargs):
        count_of_change_set = ChangeSet.objects.filter(
            content_type=kwargs.get('content_type'), object_id=kwargs.get('email_id')).count()
        self.assertEqual(count_of_change_set, expected_value)
        count_of_mailbox_message = Message.objects.filter(message_id=kwargs.get('message_id')).count()
        self.assertEqual(count_of_mailbox_message, expected_value)
