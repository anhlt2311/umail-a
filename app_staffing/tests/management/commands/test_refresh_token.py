from django.core.management import call_command
from django.test import TestCase
from rest_framework.authtoken.models import Token


class RefreshTokenTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/authtokens.json',
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        tokens = Token.objects.filter(user__is_staff=False)
        assert len(tokens) > 0

    def test_command_send(self):
        old_token_values = sorted(Token.objects.filter(user__is_staff=False).values_list('key', flat=True))

        with self.assertRaises(SystemExit) as command:
            call_command('refresh_token')
        self.assertEqual(command.exception.code, 0)

        new_token_values = sorted(Token.objects.filter(user__is_staff=False).values_list('key', flat=True))

        self.assertNotEqual(old_token_values, new_token_values)
