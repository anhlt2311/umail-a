from django.core.management import call_command
from django.test import TestCase
from unittest.mock import Mock, patch
from django.conf import settings
from app_staffing.models import Company, Plan
from app_staffing.tests.settings import DEFAULT_COMPANY_ID

class NotifyMailCommandTestCase(TestCase):

    def test_call_command(self):
        with self.assertRaises(SystemExit) as e:
            call_command('notify_mail')
        self.assertEqual(e.exception.code, 0)

class NotifyMailCommandErrorTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/plan_master_data.json',
    ]

    TEST_COMPANY_ID_LIST = [
        '323d0e3d-61a2-4cb1-b094-4338c90aa95a',
        '3efd25d0-989a-4fba-aa8d-b91708760eb1',
        '0639df05-8db5-40f2-a25d-6bcd3f8ae4ca',
        '1649df06-8db5-49f2-a25d-6bcd4f8ae3ca',
    ]

    def test_call_command(self):
        for company_id in self.TEST_COMPANY_ID_LIST:
            Plan.objects.create(
                company_id=company_id,
                plan_master_id=2,
                is_active=True,
            )

        mock = Mock(side_effect=[Exception('Boom!'), 0, 0])
        with patch('app_staffing.services.email.notify_mail.send_notification_emails', mock):
            with self.assertLogs(logger='', level='ERROR') as cm:
                with self.assertRaises(SystemExit) as command:
                    call_command('notify_mail')

                self.assertEqual(command.exception.code, 0)
                self.assertEqual(len(cm.output), 2)
                self.assertIn('Exception occurred. ErrorDetails:Boom!', cm.output[0])
                self.assertIn(str(Company.objects.all()[0].id), cm.output[0])
