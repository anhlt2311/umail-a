from uuid import uuid4

from django.test import TestCase
from datetime import datetime
from freezegun import freeze_time
from dateutil.relativedelta import relativedelta

from app_staffing.services.stats import OrganizationStats, ContactStats, ScheduledEmailStats, SharedEmailStats, StaffInChargeStats

from app_staffing.tests.factories.company import CompanyFactory
from app_staffing.tests.factories.organization import OrganizationFactory, ContactFactory, ContactJobTypePreferenceFactory, ContactPersonnelTypePreferenceFactory
from app_staffing.tests.factories.stats import (
    OrganizationStatsFactory, ContactStatsFactory, ScheduledEmailStatsFactory, SharedEmailStatsFactory,
    StaffInChargeStatsFactory
)
from app_staffing.tests.factories.email import ScheduledEmailFactory, SharedEmailFactory
from app_staffing.tests.factories.user import AppUserFactory

from app_staffing.models import Organization

class OrganizationStatsTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/organization_master_data.json'
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyFactory.create()

    def test_get_stats(self):
        today = datetime.today()
        base_datetime = datetime(year=today.year, month=today.month, day=1)

        expected_labels = []
        for x in reversed(range(1, 7)):
            target_date = base_datetime - relativedelta(months=x)
            OrganizationStatsFactory.create(
                company_id=self.company.id,
                target_date=target_date,
                prospective=x,
                approached=x,
                exchanged=x,
                client=x,
            )
            expected_labels.append(
                '{year}/{month}'.format(
                    year=(base_datetime - relativedelta(months=x)).year,
                    month=(base_datetime - relativedelta(months=x)).month,
                )
            )

        expected = {
            'labels': expected_labels,
            'values': {
                'prospective': [6, 5, 4, 3, 2, 1],
                'approached': [6, 5, 4, 3, 2, 1],
                'exchanged': [6, 5, 4, 3, 2, 1],
                'client': [6, 5, 4, 3, 2, 1],
            }
        }

        actual = OrganizationStats(company_id=self.company.id).get_stats()
        self.assertDictEqual(expected, actual)

    def test_calc_stats(self):
        today = datetime.today()
        item_created_datetime = datetime(year=today.year, month=(today.month), day=1) - relativedelta(months=1)

        with freeze_time(item_created_datetime):
            OrganizationFactory.create_batch(size=1, company=self.company, organization_category_id=1)
            OrganizationFactory.create_batch(size=2, company=self.company, organization_category_id=2)
            OrganizationFactory.create_batch(size=3, company=self.company, organization_category_id=3)
            OrganizationFactory.create_batch(size=4, company=self.company, organization_category_id=4)

        OrganizationStats(company_id=self.company.id).calc_stats(None)
        expected = {
            'labels': ['{year}/{month}'.format(
                year=item_created_datetime.year,
                month=item_created_datetime.month,
            )],
            'values': {
                'prospective': [1],
                'approached': [2],
                'exchanged': [3],
                'client': [4],
            }
        }
        actual = OrganizationStats(company_id=self.company.id).get_stats()
        self.assertDictEqual(expected, actual)

    def test_calc_stats_target_date(self):
        today = datetime.today()
        item_created_datetime = datetime(year=today.year, month=(today.month), day=1) - relativedelta(months=3)

        with freeze_time(item_created_datetime):
            OrganizationFactory.create_batch(size=1, company=self.company, organization_category_id=1)
            OrganizationFactory.create_batch(size=2, company=self.company, organization_category_id=2)
            OrganizationFactory.create_batch(size=3, company=self.company, organization_category_id=3)
            OrganizationFactory.create_batch(size=4, company=self.company, organization_category_id=4)

        OrganizationStats(company_id=self.company.id).calc_stats(item_created_datetime)
        expected = {
            'labels': ['{year}/{month}'.format(
                year=item_created_datetime.year,
                month=item_created_datetime.month,
            )],
            'values': {
                'prospective': [1],
                'approached': [2],
                'exchanged': [3],
                'client': [4],
            }
        }
        actual = OrganizationStats(company_id=self.company.id).get_stats()
        self.assertDictEqual(expected, actual)

class ContactStatsTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/organization_master_data.json',
        'app_staffing/tests/fixtures/type_preferences.json',
        'app_staffing/tests/fixtures/skill_preferences.json',
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyFactory.create()

    def test_get_stats(self):
        today = datetime.today()
        base_datetime = datetime(year=today.year, month=today.month, day=1)

        expected_labels = []
        for x in reversed(range(1, 7)):
            target_date = base_datetime - relativedelta(months=x)
            ContactStatsFactory.create(
                company_id=self.company.id,
                target_date=target_date,
                jobtype_dev=x,
                jobtype_infra=x,
                jobtype_other=x,
                personneltype_dev=x,
                personneltype_infra=x,
                personneltype_other=x,
                other=x,
            )
            expected_labels.append(
                '{year}/{month}'.format(
                    year=(base_datetime - relativedelta(months=x)).year,
                    month=(base_datetime - relativedelta(months=x)).month,
                )
            )

        expected = {
            'labels': expected_labels,
            'values': {
                'jobs': {
                    'development': [6, 5, 4, 3, 2, 1],
                    'infrastructure': [6, 5, 4, 3, 2, 1],
                    'others': [6, 5, 4, 3, 2, 1],
                },
                'personnel': {
                    'development': [6, 5, 4, 3, 2, 1],
                    'infrastructure': [6, 5, 4, 3, 2, 1],
                    'others': [6, 5, 4, 3, 2, 1],
                },
                'announcement': [6, 5, 4, 3, 2, 1],
            }
        }

        actual = ContactStats(company_id=self.company.id).get_stats()
        self.assertDictEqual(expected, actual)

    def test_calc_stats(self):
        today = datetime.today()
        item_created_datetime = datetime(year=today.year, month=(today.month), day=1) - relativedelta(months=1)

        with freeze_time(item_created_datetime):
            organization = OrganizationFactory(name='テスト取引先', company=self.company)

            contact_1 = ContactFactory(email='sampleuser1@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')

            contact_2 = ContactFactory(email='sampleuser2@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_2, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_2, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_2, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_2, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_2, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')

            contact_3 = ContactFactory(email='sampleuser3@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_3, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_3, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_3, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_3, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')

            contact_4 = ContactFactory(email='sampleuser4@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_4, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_4, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_4, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')

            contact_5 = ContactFactory(email='sampleuser5@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_5, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_5, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')

            contact_6 = ContactFactory(email='sampleuser6@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_6, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')

            ContactFactory(email='sampleuser7@example.com', company=self.company, organization=organization)

        ContactStats(company_id=self.company.id).calc_stats(None)
        expected = {
            'labels': ['{year}/{month}'.format(
                year=item_created_datetime.year,
                month=item_created_datetime.month,
            )],
            'values': {
                'jobs': {
                    'development': [6],
                    'infrastructure': [5],
                    'others': [4],
                },
                'personnel': {
                    'development': [3],
                    'infrastructure': [2],
                    'others': [1],
                },
                'announcement': [7],
            }
        }
        actual = ContactStats(company_id=self.company.id).get_stats()
        self.assertDictEqual(expected, actual)

    def test_calc_stats_target_date(self):
        today = datetime.today()
        item_created_datetime = datetime(year=today.year, month=(today.month), day=1) - relativedelta(months=3)

        with freeze_time(item_created_datetime):
            organization = OrganizationFactory(name='テスト取引先', company=self.company)

            contact_1 = ContactFactory(email='sampleuser1@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_1, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')

            contact_2 = ContactFactory(email='sampleuser2@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_2, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_2, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_2, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_2, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_2, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')

            contact_3 = ContactFactory(email='sampleuser3@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_3, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_3, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_3, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')
            ContactPersonnelTypePreferenceFactory(company=self.company, contact=contact_3, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')

            contact_4 = ContactFactory(email='sampleuser4@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_4, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_4, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_4, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc28')

            contact_5 = ContactFactory(email='sampleuser5@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_5, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_5, type_preference_id='5737b0a8-5a99-4254-8798-b8c4a475cc22')

            contact_6 = ContactFactory(email='sampleuser6@example.com', company=self.company, organization=organization)
            ContactJobTypePreferenceFactory(company=self.company, contact=contact_6, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')

            ContactFactory(email='sampleuser7@example.com', company=self.company, organization=organization)

        ContactStats(company_id=self.company.id).calc_stats(item_created_datetime)
        expected = {
            'labels': ['{year}/{month}'.format(
                year=item_created_datetime.year,
                month=item_created_datetime.month,
            )],
            'values': {
                'jobs': {
                    'development': [6],
                    'infrastructure': [5],
                    'others': [4],
                },
                'personnel': {
                    'development': [3],
                    'infrastructure': [2],
                    'others': [1],
                },
                'announcement': [7],
            }
        }
        actual = ContactStats(company_id=self.company.id).get_stats()
        self.assertDictEqual(expected, actual)

class ScheduledEamilStatsTestCase(TestCase):
    fixtures = [
        'app_staffing/tests/fixtures/organization_master_data.json',
        'app_staffing/tests/fixtures/scheduled_email_master_data.json',
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyFactory.create()

    def test_get_stats(self):
        today = datetime.today()
        base_datetime = datetime(year=today.year, month=today.month, day=1)

        expected_labels = []
        for x in reversed(range(1, 7)):
            target_date = base_datetime - relativedelta(months=x)
            ScheduledEmailStatsFactory.create(
                company_id=self.company.id,
                target_date=target_date,
                job=x,
                personnel=x,
                other=x,
            )
            expected_labels.append(
                '{year}/{month}'.format(
                    year=(base_datetime - relativedelta(months=x)).year,
                    month=(base_datetime - relativedelta(months=x)).month,
                )
            )

        expected = {
            'labels': expected_labels,
            'values': {
                'jobs': [6, 5, 4, 3, 2, 1],
                'personnel': [6, 5, 4, 3, 2, 1],
                'announcement': [6, 5, 4, 3, 2, 1],
            }
        }

        actual = ScheduledEmailStats(company_id=self.company.id).get_stats()
        self.assertDictEqual(expected, actual)

    def test_calc_stats(self):
        today = datetime.today()
        item_created_datetime = datetime(year=today.year, month=(today.month), day=1) - relativedelta(months=1)

        with freeze_time(item_created_datetime):
            user = AppUserFactory(company=self.company)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=1, sender=user, sent_date=item_created_datetime)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=1, sender=user, sent_date=item_created_datetime)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=1, sender=user, sent_date=item_created_datetime)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=2, sender=user, sent_date=item_created_datetime)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=2, sender=user, sent_date=item_created_datetime)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=3, sender=user, sent_date=item_created_datetime)

        ScheduledEmailStats(company_id=self.company.id).calc_stats(None)
        expected = {
            'labels': ['{year}/{month}'.format(
                year=item_created_datetime.year,
                month=item_created_datetime.month,
            )],
            'values': {
                'jobs': [3],
                'personnel': [2],
                'announcement': [1],
            }
        }
        actual = ScheduledEmailStats(company_id=self.company.id).get_stats()
        self.assertDictEqual(expected, actual)

    def test_calc_stats_target_date(self):
        today = datetime.today()
        item_created_datetime = datetime(year=today.year, month=(today.month), day=1) - relativedelta(months=3)

        with freeze_time(item_created_datetime):
            user = AppUserFactory(company=self.company)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=1, sender=user, sent_date=item_created_datetime)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=1, sender=user, sent_date=item_created_datetime)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=1, sender=user, sent_date=item_created_datetime)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=2, sender=user, sent_date=item_created_datetime)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=2, sender=user, sent_date=item_created_datetime)
            ScheduledEmailFactory(company=self.company, scheduled_email_send_type_id=3, sender=user, sent_date=item_created_datetime)

        ScheduledEmailStats(company_id=self.company.id).calc_stats(item_created_datetime)
        expected = {
            'labels': ['{year}/{month}'.format(
                year=item_created_datetime.year,
                month=item_created_datetime.month,
            )],
            'values': {
                'jobs': [3],
                'personnel': [2],
                'announcement': [1],
            }
        }
        actual = ScheduledEmailStats(company_id=self.company.id).get_stats()
        self.assertDictEqual(expected, actual)


class StaffInChargeStatsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyFactory.create()

    def test_get_stats(self):
        today = datetime.today()
        base_datetime = datetime(year=today.year, month=today.month, day=1)

        expected = {}
        for x in reversed(range(1, 7)):
            target_date = base_datetime - relativedelta(months=x)
            StaffInChargeStatsFactory.create(
                company_id=self.company.id,
                target_date=target_date,
                top_values=f'{x + 5}|{x + 4}|{x + 3}|{x + 2}|{x + 1}|{x}',
                staff_in_charge='Top 01|Top 02|Top 03|Top 04|Top 05|other',
            )

            expected[f'{target_date.year}/{target_date.month}'] = {
                'top_values': [x + 5, x + 4, x + 3, x + 2, x + 1, x],
                'staff': ['Top 01', 'Top 02', 'Top 03', 'Top 04', 'Top 05', 'other'],
            }

        actual = StaffInChargeStats(company_id=self.company.id).get_stats()

        self.assertDictEqual(expected, actual)

    def test_calc_stats(self):
        today = datetime.today()
        sent_date = datetime(year=today.year, month=today.month, day=1) - relativedelta(months=1)

        expected = self.fake_shared_email(sent_date)

        StaffInChargeStats(company_id=self.company.id).calc_stats(None)
        actual = StaffInChargeStats(company_id=self.company.id).get_stats()

        self.assertDictEqual(expected, actual)

    def test_calc_stats_target_date(self):
        today = datetime.today()
        sent_date = datetime(year=today.year, month=today.month, day=1) - relativedelta(months=3)

        expected = self.fake_shared_email(sent_date)

        StaffInChargeStats(company_id=self.company.id).calc_stats(sent_date)
        actual = StaffInChargeStats(company_id=self.company.id).get_stats()

        self.assertDictEqual(expected, actual)

    def test_calc_stats_with_small_number_of_users(self):
        today = datetime.today()
        sent_date = datetime(year=today.year, month=today.month, day=1) - relativedelta(months=1)

        c = self.company
        u1 = AppUserFactory(username='user1', user_service_id='user_14432', company_id=c.id,
                            email="a@gmail.com", first_name="01", last_name="User")
        u2 = AppUserFactory(username='user2', user_service_id='user_15123', company_id=c.id,
                            email="b@gmail.com", first_name="02", last_name="User")

        SharedEmailFactory(company=c, sent_date=sent_date, staff_in_charge=u1)
        SharedEmailFactory(company=c, sent_date=sent_date, staff_in_charge=u1)
        SharedEmailFactory(company=c, sent_date=sent_date, staff_in_charge=u2)

        expected = {
            f'{sent_date.year}/{sent_date.month}': {
                'top_values': [2, 1, 0, 0, 0, 0],
                'staff': ['User 01', 'User 02', '_', '_', '_', 'other'],
            }
        }
        StaffInChargeStats(company_id=self.company.id).calc_stats(sent_date)
        actual = StaffInChargeStats(company_id=self.company.id).get_stats()

        self.assertDictEqual(expected, actual)

    def fake_shared_email(self, d):
        c = self.company
        u1 = AppUserFactory(username='user1', user_service_id='user_14432', company_id=self.company.id,
                            email="a@gmail.com", first_name="01", last_name="User")
        u2 = AppUserFactory(username='user2', user_service_id='user_15123', company_id=self.company.id,
                            email="b@gmail.com", first_name="02", last_name="User")
        u3 = AppUserFactory(username='user3', user_service_id='user_5252', company_id=self.company.id,
                            email="c@gmail.com", first_name="03", last_name="User")
        u4 = AppUserFactory(username='user4', user_service_id='user_4232', company_id=self.company.id,
                            email="d@gmail.com", first_name="04", last_name="User")
        u5 = AppUserFactory(username='user5', user_service_id='user_6573452', company_id=self.company.id,
                            email="e@gmail.com", first_name="05", last_name="User")
        u6 = AppUserFactory(username='user6', user_service_id='user_5255341', company_id=self.company.id,
                            email="f@gmail.com", first_name="06", last_name="User")
        u7 = AppUserFactory(username='user7', user_service_id='user_24132', company_id=self.company.id,
                            email="g@gmail.com", first_name="07", last_name="User")
        u8 = AppUserFactory(username='user8', user_service_id='user_423254', company_id=self.company.id,
                            email="h@gmail.com", first_name="08", last_name="User")
        u9 = AppUserFactory(username='user9', user_service_id='user_32432', company_id=self.company.id,
                            email="i@gmail.com", first_name="09", last_name="User")

        with freeze_time(d):
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u1)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u1)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u1)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u1)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u1)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u2)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u2)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u2)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u2)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u3)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u3)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u3)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u4)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u4)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u6)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u7)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u5)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u8)
            SharedEmailFactory(company=c, sent_date=d, staff_in_charge=u9)
            SharedEmailFactory(company=c, sent_date=d - relativedelta(months=1), staff_in_charge=u1)
            SharedEmailFactory(company=c, sent_date=d - relativedelta(months=2), staff_in_charge=u2)
            SharedEmailFactory(company=c, sent_date=d - relativedelta(months=3), staff_in_charge=u3)

        return {
            f'{d.year}/{d.month}': {
                'top_values': [5, 4, 3, 2, 1, 4],
                'staff': ['User 01', 'User 02', 'User 03', 'User 04', 'User 05', 'other'],
            }
        }


class SharedEmailStatsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.company = CompanyFactory.create()

    def test_get_stats(self):
        today = datetime.today()
        base_datetime = datetime(year=today.year, month=today.month, day=1)

        expected = {}
        AppUserFactory.create(company_id=self.company.id, email="c@gmail.com", first_name="name", last_name="updated")
        for x in reversed(range(1, 7)):
            target_date = base_datetime - relativedelta(months=x)
            SharedEmailStatsFactory.create(
                company_id=self.company.id,
                target_date=target_date,
                top_values=f'{x + 5}|{x + 4}|{x + 3}|{x + 2}|{x + 1}|{x}',
                from_address='a@gmail.com|b@gmail.com|c@gmail.com|d@gmail.com|e@gmail.com|other',
                from_name='a|b|c|d|e|other',
            )

            expected[f'{target_date.year}/{target_date.month}'] = {
                'top_values': [x + 5, x + 4, x + 3, x + 2, x + 1, x],
                'from_address': ['a@gmail.com', 'b@gmail.com', 'c@gmail.com', 'd@gmail.com', 'e@gmail.com', 'other'],
                'from_name': ['a', 'b', 'updated name', 'd', 'e', 'other'],
            }

        actual = SharedEmailStats(company_id=self.company.id).get_stats()

        self.assertDictEqual(expected, actual)

    def test_calc_stats(self):
        today = datetime.today()
        sent_date = datetime(year=today.year, month=today.month, day=1) - relativedelta(months=1)

        expected = self.fake_shared_email(sent_date)

        SharedEmailStats(company_id=self.company.id).calc_stats(None)
        actual = SharedEmailStats(company_id=self.company.id).get_stats()

        self.assertDictEqual(expected, actual)

    def test_calc_stats_target_date(self):
        today = datetime.today()
        sent_date = datetime(year=today.year, month=today.month, day=1) - relativedelta(months=3)

        expected = self.fake_shared_email(sent_date)

        SharedEmailStats(company_id=self.company.id).calc_stats(sent_date)
        actual = SharedEmailStats(company_id=self.company.id).get_stats()

        self.assertDictEqual(expected, actual)

    def test_calc_stats_with_small_number_of_users(self):
        today = datetime.today()
        sent_date = datetime(year=today.year, month=today.month, day=1) - relativedelta(months=1)

        c = self.company
        SharedEmailFactory(company=c, sent_date=sent_date, from_address="dummy01@example.com", from_name="dummy01")
        SharedEmailFactory(company=c, sent_date=sent_date, from_address="dummy01@example.com", from_name="dummy01")
        SharedEmailFactory(company=c, sent_date=sent_date, from_address="dummy02@example.com", from_name="dummy02")
        SharedEmailFactory(company=c, sent_date=sent_date, from_address="dummy02@example.com", from_name="dummy02")
        SharedEmailFactory(company=c, sent_date=sent_date, from_address="dummy03@example.com", from_name="dummy03")

        expected = {
            f'{sent_date.year}/{sent_date.month}': {
                'top_values': [2, 2, 1, 0, 0, 0],
                'from_address': ['dummy01@example.com', 'dummy02@example.com', 'dummy03@example.com',
                                 '_', '_', 'other'],
                'from_name': ['dummy01', 'dummy02', 'dummy03', '_', '_', 'other'],
            }
        }
        SharedEmailStats(company_id=self.company.id).calc_stats(sent_date)
        actual = SharedEmailStats(company_id=self.company.id).get_stats()

        self.assertDictEqual(expected, actual)

    def fake_shared_email(self, d):
        c = self.company
        with freeze_time(d):
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy01@example.com", from_name="dummy01")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy01@example.com", from_name="dummy01")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy01@example.com", from_name="dummy01")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy01@example.com", from_name="dummy01")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy01@example.com", from_name="dummy01")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy02@example.com", from_name="dummy02")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy02@example.com", from_name="dummy02")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy02@example.com", from_name="dummy02")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy02@example.com", from_name="dummy02")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy03@example.com", from_name="dummy03")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy03@example.com", from_name="dummy03")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy03@example.com", from_name="dummy03")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy04@example.com", from_name="dummy04")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy04@example.com", from_name="dummy04")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy06@example.com", from_name="dummy06")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy07@example.com", from_name="dummy07")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy05@example.com", from_name="dummy05")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy08@example.com", from_name="dummy08")
            SharedEmailFactory(company=c, sent_date=d, from_address="dummy09@example.com", from_name="dummy09")
            SharedEmailFactory(company=c, sent_date=d - relativedelta(months=1), from_address="dummy01@example.com",
                               from_name="dummy01")
            SharedEmailFactory(company=c, sent_date=d - relativedelta(months=2), from_address="dummy01@example.com",
                               from_name="dummy01")
            SharedEmailFactory(company=c, sent_date=d - relativedelta(months=3), from_address="dummy01@example.com",
                               from_name="dummy01")

        return {
            f'{d.year}/{d.month}': {
                'top_values': [5, 4, 3, 2, 1, 4],
                'from_address': ['dummy01@example.com', 'dummy02@example.com', 'dummy03@example.com',
                                 'dummy04@example.com', 'dummy05@example.com', 'other'],
                'from_name': ['dummy01', 'dummy02', 'dummy03', 'dummy04', 'dummy05', 'other'],
            }
        }
