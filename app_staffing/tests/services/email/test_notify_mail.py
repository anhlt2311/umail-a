import re

from uuid import uuid4
from copy import deepcopy
from django.test import TestCase
from django.core import mail

from app_staffing.models import Email
from app_staffing.services.email.notify_mail import get_unprocessed_emails, get_notify_rules,\
    match, send_notification_emails
from app_staffing.tests.factories.email import SharedEmailFactory as Mf, SMTPServerFactory
from app_staffing.tests.factories.user import AppUserFactory
from app_staffing.tests.factories.company import CompanyFactory
from app_staffing.tests.factories.notifications import EmailNotificationRuleFactory as Rf,\
    EmailNotificationConditionFactory as Cf

# Definitions of original emails.
KEYWORD_1 = 'Keyword_1'
KEYWORD_2 = 'Keyword_2'
KEYWORD_3 = 'Keyword_3'

SUBJECT_1 = f'Subject : {KEYWORD_1}'
SUBJECT_2 = f'Subject : {KEYWORD_2}'
SUBJECT_3 = f'Subject : {KEYWORD_3}'

TEXT_1 = f'Text : {KEYWORD_1}..Blah'
TEXT_2 = f'Text : {KEYWORD_2}..Blah'
TEXT_3 = f'Text : {KEYWORD_3}..Blah'

LOOKUP_INCLUDES_KEYWORD1_ON_SUBJECT = {
    'extraction_type': 'keyword',
    'target_type': 'subject',
    'condition_type': 'include',
    'value': KEYWORD_1
}
SUBJECT_PREFIX = 'Subject'
EMAIL_1 = 'dummy@example.com'
EMAIL_2 = 'hogehoge@fugafuga.com'
EMAIL_PREFIX = 'dummy'
EMAIL_SUFFIX = '@fugafuga.com'

R1_RULE_NAME = 'r1: KEYWORD 1 On Subject'
R2_RULE_NAME = 'r2: KEYWORD 2 On Content'
R3_RULE_NAME = 'r3: KEYWORD 1 On Content but not KEYWORD 2 on Content'
R4_RULE_NAME = 'r4: KEYWORD 1 On Subject but not KEYWORD 2 on Subject'
R5_RULE_NAME = 'r5: KEYWORD 1 On Subject but not KEYWORD 2 on Content'
R6_RULE_NAME = 'r6: KEYWORD 1 On Content but not KEYWORD 2 on Subject'
R7_RULE_NAME = 'r7: file attachment is true'
R8_RULE_NAME = 'r8: file attachment is false'
R9_RULE_NAME = 'r9: Subject EQUAL KEYWORD 1'
R10_RULE_NAME = 'r10: Subject NOT EQUAL KEYWORD 1'
R11_RULE_NAME = 'r11: Subject PREFIX KEYWORD 1'
R12_RULE_NAME = 'r12: Subject SUFFIX KEYWORD 1'
R13_RULE_NAME = 'r13: From Address INCLUDE TEST FROM ADDRESS 1'
R14_RULE_NAME = 'r14: From Address EXCLUDE TEST FROM ADDRESS 1'
R15_RULE_NAME = 'r15: From Address EQUAL TEST FROM ADDRESS 1'
R16_RULE_NAME = 'r16: From Address NOT EQUAL TEST FROM ADDRESS 1'
R17_RULE_NAME = 'r17: From Address PREFIX TEST FROM ADDRESS 1'
R18_RULE_NAME = 'r18: From Address SUFFIX TEST FROM ADDRESS 1'
R19_RULE_NAME = 'r19: KEYWORD 1 or KEYWORD 2  or KEYWORD 3 On Subject'
R20_RULE_NAME = 'r20: invalid rule'

class EmailNotificationServiceTestCase(TestCase):
    """A Test case for the Email notification service."""

    @classmethod
    def setUpTestData(cls):
        cls.company = CompanyFactory()  # A company (tenant) object that defines the owner of the rules.
        cls.another_company = CompanyFactory(id=uuid4())
        SMTPServerFactory.create(company=cls.company)
        user = AppUserFactory(company=cls.company)

        # Define Emails.
        cls.e1 = Mf.create(company=cls.company, subject=SUBJECT_1, text=TEXT_1, from_address=EMAIL_1)
        cls.e2 = Mf.create(company=cls.company, subject=SUBJECT_2, text=TEXT_1, from_address=EMAIL_2)
        cls.e3 = Mf.create(company=cls.company, subject=SUBJECT_1, text=TEXT_2, from_address=EMAIL_1)
        cls.e4 = Mf.create(company=cls.company, subject=SUBJECT_2, text=TEXT_2, from_address=EMAIL_2)
        cls.e5 = Mf.create(company=cls.company, subject=SUBJECT_3, text=TEXT_3, has_attachments=True, from_address=EMAIL_1)
        cls.e6 = Mf.create(company=cls.company, subject=SUBJECT_3, text=TEXT_3, has_attachments=False, from_address=EMAIL_2)

        # Emails for another tenant. Those emails must not be processed.
        cls.e1_another_tenant = Mf.create(company=cls.another_company, subject=SUBJECT_1, text=TEXT_1)
        cls.e2_another_tenant = Mf.create(company=cls.another_company, subject=SUBJECT_2, text=TEXT_1)

        # Define Rules.
        cls.r1 = Rf.create(company=cls.company, name=R1_RULE_NAME, master_rule='all', created_user=user)
        cls.r2 = Rf.create(company=cls.company, name=R2_RULE_NAME, master_rule='all', created_user=user)
        cls.r3 = Rf.create(company=cls.company, name=R3_RULE_NAME, master_rule='all', created_user=user)
        cls.r4 = Rf.create(company=cls.company, name=R4_RULE_NAME, master_rule='all', created_user=user)
        cls.r5 = Rf.create(company=cls.company, name=R5_RULE_NAME, master_rule='all', created_user=user)
        cls.r6 = Rf.create(company=cls.company, name=R6_RULE_NAME, master_rule='all', created_user=user)
        cls.r7 = Rf.create(company=cls.company, name=R7_RULE_NAME, master_rule='all', created_user=user)
        cls.r8 = Rf.create(company=cls.company, name=R8_RULE_NAME, master_rule='all', created_user=user)
        cls.r9 = Rf.create(company=cls.company, name=R9_RULE_NAME, master_rule='all', created_user=user)
        cls.r10 = Rf.create(company=cls.company, name=R10_RULE_NAME, master_rule='all', created_user=user)
        cls.r11 = Rf.create(company=cls.company, name=R11_RULE_NAME, master_rule='all', created_user=user)
        cls.r12 = Rf.create(company=cls.company, name=R12_RULE_NAME, master_rule='all', created_user=user)
        cls.r13 = Rf.create(company=cls.company, name=R13_RULE_NAME, master_rule='all', created_user=user)
        cls.r14 = Rf.create(company=cls.company, name=R14_RULE_NAME, master_rule='all', created_user=user)
        cls.r15 = Rf.create(company=cls.company, name=R15_RULE_NAME, master_rule='all', created_user=user)
        cls.r16 = Rf.create(company=cls.company, name=R16_RULE_NAME, master_rule='all', created_user=user)
        cls.r17 = Rf.create(company=cls.company, name=R17_RULE_NAME, master_rule='all', created_user=user)
        cls.r18 = Rf.create(company=cls.company, name=R18_RULE_NAME, master_rule='all', created_user=user)
        cls.r19 = Rf.create(company=cls.company, name=R19_RULE_NAME, master_rule='any', created_user=user)
        cls.r20 = Rf.create(company=cls.company, name=R20_RULE_NAME, master_rule='', created_user=user)

        # Define Conditions.
        # Note: content means 'text'
        Cf.create(rule=cls.r1, order=1, **LOOKUP_INCLUDES_KEYWORD1_ON_SUBJECT)
        Cf.create(rule=cls.r2, order=1, extraction_type='keyword', target_type='content', condition_type='include', value=KEYWORD_2)
        Cf.create(rule=cls.r3, order=1, extraction_type='keyword', target_type='content', condition_type='include', value=KEYWORD_1)
        Cf.create(rule=cls.r3, order=2, extraction_type='keyword', target_type='content', condition_type='exclude', value=KEYWORD_2)
        Cf.create(rule=cls.r4, order=1, extraction_type='keyword', target_type='subject', condition_type='include', value=KEYWORD_1)
        Cf.create(rule=cls.r4, order=2, extraction_type='keyword', target_type='subject', condition_type='exclude', value=KEYWORD_2)
        Cf.create(rule=cls.r5, order=1, extraction_type='keyword', target_type='subject', condition_type='include', value=KEYWORD_1)
        Cf.create(rule=cls.r5, order=2, extraction_type='keyword', target_type='content', condition_type='exclude', value=KEYWORD_2)
        Cf.create(rule=cls.r6, order=1, extraction_type='keyword', target_type='content', condition_type='include', value=KEYWORD_1)
        Cf.create(rule=cls.r6, order=2, extraction_type='keyword', target_type='subject', condition_type='exclude', value=KEYWORD_2)
        Cf.create(rule=cls.r7, order=1, extraction_type='attachment', target_type='attachment', condition_type='exists', value=0)
        Cf.create(rule=cls.r7, order=2, extraction_type='keyword', target_type='content', condition_type='include', value=KEYWORD_3)
        Cf.create(rule=cls.r8, order=1, extraction_type='attachment', target_type='attachment', condition_type='not_exists', value=0)
        Cf.create(rule=cls.r8, order=2, extraction_type='keyword', target_type='content', condition_type='include', value=KEYWORD_3)
        Cf.create(rule=cls.r9, order=1, extraction_type='keyword', target_type='subject', condition_type='equal', value=SUBJECT_1)
        Cf.create(rule=cls.r10, order=1, extraction_type='keyword', target_type='subject', condition_type='not_equal', value=SUBJECT_1)
        Cf.create(rule=cls.r11, order=1, extraction_type='keyword', target_type='subject', condition_type='start_with', value=SUBJECT_PREFIX)
        Cf.create(rule=cls.r12, order=1, extraction_type='keyword', target_type='subject', condition_type='end_with', value=KEYWORD_1)
        Cf.create(rule=cls.r13, order=1, extraction_type='keyword', target_type='from_address', condition_type='include', value=EMAIL_PREFIX)
        Cf.create(rule=cls.r14, order=1, extraction_type='keyword', target_type='from_address', condition_type='exclude', value=EMAIL_PREFIX)
        Cf.create(rule=cls.r15, order=1, extraction_type='keyword', target_type='from_address', condition_type='equal', value=EMAIL_1)
        Cf.create(rule=cls.r16, order=1, extraction_type='keyword', target_type='from_address', condition_type='not_equal', value=EMAIL_1)
        Cf.create(rule=cls.r17, order=1, extraction_type='keyword', target_type='from_address', condition_type='start_with', value=EMAIL_PREFIX)
        Cf.create(rule=cls.r18, order=1, extraction_type='keyword', target_type='from_address', condition_type='end_with', value=EMAIL_SUFFIX)
        Cf.create(rule=cls.r19, order=1, extraction_type='keyword', target_type='subject', condition_type='include', value=KEYWORD_1)
        Cf.create(rule=cls.r19, order=2, extraction_type='keyword', target_type='subject', condition_type='include', value=KEYWORD_2)
        Cf.create(rule=cls.r19, order=3, extraction_type='keyword', target_type='subject', condition_type='include', value=KEYWORD_3)
        Cf.create(rule=cls.r20, order=1, extraction_type='keyword', target_type='content', condition_type='include', value=KEYWORD_1)

    @property
    def test_patterns(self):
        return [  # (email, rule, expected_match_result)
            (self.e1, self.r1, True),
            (self.e1, self.r2, False),
            (self.e1, self.r3, True),
            (self.e1, self.r4, True),
            (self.e1, self.r5, True),
            (self.e1, self.r6, True),
            (self.e1, self.r9, True),
            (self.e1, self.r10, False),
            (self.e1, self.r11, True),
            (self.e1, self.r12, True),
            (self.e1, self.r13, True),
            (self.e1, self.r14, False),
            (self.e1, self.r15, True),
            (self.e1, self.r16, False),
            (self.e1, self.r17, True),
            (self.e1, self.r18, False),
            (self.e1, self.r19, True),

            (self.e2, self.r1, False),
            (self.e2, self.r2, False),
            (self.e2, self.r3, True),
            (self.e2, self.r4, False),
            (self.e2, self.r5, False),
            (self.e2, self.r6, False),
            (self.e2, self.r9, False),
            (self.e2, self.r10, True),
            (self.e2, self.r11, True),
            (self.e2, self.r12, False),
            (self.e2, self.r13, False),
            (self.e2, self.r14, True),
            (self.e2, self.r15, False),
            (self.e2, self.r16, True),
            (self.e2, self.r17, False),
            (self.e2, self.r18, True),
            (self.e2, self.r19, True),

            (self.e3, self.r1, True),
            (self.e3, self.r2, True),
            (self.e3, self.r3, False),
            (self.e3, self.r4, True),
            (self.e3, self.r5, False),
            (self.e3, self.r6, False),
            (self.e3, self.r9, True),
            (self.e3, self.r10, False),
            (self.e3, self.r11, True),
            (self.e3, self.r12, True),
            (self.e3, self.r13, True),
            (self.e3, self.r14, False),
            (self.e3, self.r15, True),
            (self.e3, self.r16, False),
            (self.e3, self.r17, True),
            (self.e3, self.r18, False),
            (self.e3, self.r19, True),

            (self.e4, self.r1, False),
            (self.e4, self.r2, True),
            (self.e4, self.r3, False),
            (self.e4, self.r4, False),
            (self.e4, self.r5, False),
            (self.e4, self.r6, False),
            (self.e4, self.r9, False),
            (self.e4, self.r10, True),
            (self.e4, self.r11, True),
            (self.e4, self.r12, False),
            (self.e4, self.r13, False),
            (self.e4, self.r14, True),
            (self.e4, self.r15, False),
            (self.e4, self.r16, True),
            (self.e4, self.r17, False),
            (self.e4, self.r18, True),
            (self.e4, self.r19, True),

            (self.e5, self.r7, True),
            (self.e5, self.r8, False),
            (self.e5, self.r9, False),
            (self.e5, self.r10, True),
            (self.e5, self.r11, True),
            (self.e5, self.r12, False),
            (self.e5, self.r13, True),
            (self.e5, self.r14, False),
            (self.e5, self.r15, True),
            (self.e5, self.r16, False),
            (self.e5, self.r17, True),
            (self.e5, self.r18, False),
            (self.e5, self.r19, True),

            (self.e6, self.r7, False),
            (self.e6, self.r8, True),
            (self.e6, self.r9, False),
            (self.e6, self.r10, True),
            (self.e6, self.r11, True),
            (self.e6, self.r12, False),
            (self.e6, self.r13, False),
            (self.e6, self.r14, True),
            (self.e6, self.r15, False),
            (self.e6, self.r16, True),
            (self.e6, self.r17, False),
            (self.e6, self.r18, True),
            (self.e6, self.r19, True),
        ]

    def test_get_unprocessed_email(self):
        assert Email.objects.count() == 8  # Make sure that test data contains 6 emails + 2 emails of another tenant.
        qs = get_unprocessed_emails(company=self.company)
        self.assertEqual(6, qs.count())

    def test_get_notify_rules(self):
        qs = get_notify_rules(company=self.company)
        self.assertEqual(20, qs.count())

    def test_match(self):
        """Test matcher function with various conditions."""
        for email, rule, expected_result in self.test_patterns:
            with self.subTest(email=f'{email.subject}:{email.text}', rule=f'{rule.name}'):
                self.assertEqual(expected_result, match(email, rule))

    def test_send_notification_emails(self):
        """Test whole process of notification."""

        # Check the number of sent email.
        with self.assertLogs(logger='', level='ERROR') as cm:
            sent_emails = send_notification_emails(company=self.company)
            self.assertIn('ErrorDetails:Invalid master rule has specified.', cm.output[0])
        self.assertEqual(sent_emails, 1)

        # Check the ACTUAL number of sent email.
        self.assertEqual(len(mail.outbox), 1)

        # Check the content of the sent email.
        sent_email = mail.outbox[0]
        print('===========Preview the subject of the sent Email============')
        print(sent_email.subject)
        print('===========Preview the body of the sent Email============')
        print(sent_email.body)
        print('===========          Preview end           ============')

        # Check if both the subject and the text of the sent email contains the original subject.
        self.assertTrue((SUBJECT_1 in sent_email.body) or (SUBJECT_2 in sent_email.body) or (SUBJECT_3 in sent_email.body))

        # Check if the sent email text contains the link to the original email.
        regex = r'.*https?://[A-z0-9/.:-]*/sharedMails/[A-z0-9-]*'
        self.assertTrue(re.match(pattern=regex, string=sent_email.body, flags=re.DOTALL))

        # Check if the all Email instances related to this company is marked as notified.
        assert Email.objects.filter(company=self.company).count() > 0  # Checking preconditions of test data.
        self.assertEqual(
            Email.objects.filter(company=self.company).count(),
            Email.objects.filter(notifications_have_done=True).count()
        )

        # [Multi-tenancy test] Check if the all Email instances related to another company is not processed.
        assert Email.objects.filter(company=self.another_company).count() > 0  # Checking preconditions of test data.
        self.assertEqual(
            Email.objects.filter(company=self.another_company).count(),
            Email.objects.filter(notifications_have_done=False).count()
        )

        # メール本文に全ての条件が記載されているかのチェックを追加する
        self.assertTrue(R1_RULE_NAME in sent_email.body)
        self.assertTrue(R2_RULE_NAME in sent_email.body)
        self.assertTrue(R3_RULE_NAME in sent_email.body)
        self.assertTrue(R4_RULE_NAME in sent_email.body)
        self.assertTrue(R5_RULE_NAME in sent_email.body)
        self.assertTrue(R6_RULE_NAME in sent_email.body)
        self.assertTrue(R7_RULE_NAME in sent_email.body)
        self.assertTrue(R8_RULE_NAME in sent_email.body)
        self.assertTrue(R9_RULE_NAME in sent_email.body)
        self.assertTrue(R10_RULE_NAME in sent_email.body)
        self.assertTrue(R11_RULE_NAME in sent_email.body)
        self.assertTrue(R12_RULE_NAME in sent_email.body)
        self.assertTrue(R13_RULE_NAME in sent_email.body)
        self.assertTrue(R14_RULE_NAME in sent_email.body)
        self.assertTrue(R15_RULE_NAME in sent_email.body)
        self.assertTrue(R16_RULE_NAME in sent_email.body)
        self.assertTrue(R17_RULE_NAME in sent_email.body)
        self.assertTrue(R18_RULE_NAME in sent_email.body)
        self.assertTrue(R19_RULE_NAME in sent_email.body)


    def test_send_notification_with_multiple_user(self):
        """This test case check the behavior when notify email service notify 1 email to multiple user.
        Note: This test case is created for the incident that stops notification batch job.
        WARN: This test will also fail if test_send_notification_emails has failed.
        """
        # Setup Additional data.
        rule = deepcopy(self.r1)
        rule.pk = None
        rule.created_user = AppUserFactory(
            user_service_id='user_1348592402',
            username='Another User',
            first_name='User',
            last_name='Another',
            email='another@example.com'
        )
        rule.save()
        Cf.create(rule=rule, order=1, **LOOKUP_INCLUDES_KEYWORD1_ON_SUBJECT)

        # Check if some exception raises or not.
        sent_emails = send_notification_emails(company=self.company)
        self.assertEqual(sent_emails, 2)
