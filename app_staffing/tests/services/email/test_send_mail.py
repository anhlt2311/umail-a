import re
import os
import shutil
from unittest.mock import MagicMock

from django.test import TestCase
from django.core.mail import EmailMessage
from django.core import mail
from django.conf import settings

from app_staffing.constants.email import ScheduledEmailSettingFileStatus
from app_staffing.models import ScheduledEmail, ScheduledEmailSetting, ScheduledEmailTarget, Contact,\
    ContactPreference, ContactJobTypePreference, ContactJobSkillPreference, ContactPersonnelTypePreference,\
    ContactPersonnelSkillPreference, email, Organization, User
from app_staffing.models.email import ScheduledEmailSearchCondition
from app_staffing.models.organization import ContactCategory, ContactTagAssignment, Tag
from app_staffing.services.email.send_mail import _create_email_instance, _generate_emails,\
    send_mails, EMAIL_TEMPLATE_NAME, DUMMY_CONTACT_LAST_NAME, DUMMY_CONTACT_FIRST_NAME, DUMMY_ORGANIZATION_NAME
from app_staffing.models.addon import Addon

from app_staffing.tests.helpers.setup import MediaFolderSetupMixin
from app_staffing.tests.factories.company import CompanyFactory
from app_staffing.tests.factories.email import SMTPServerFactory, create_scheduled_mail_data, TEST_FILE_NAME, ScheduledEmailSettingFactory
from app_staffing.tests.factories.addon import AddonFactory

from unittest.mock import Mock, patch

import json
from datetime import datetime
from dateutil.relativedelta import relativedelta

TEST_FILE_PATH = 'app_staffing/tests/__temp__/SendMailAttachment.txt'
TEST_MAIL_SENDER_ADDRESS = 'testMailSender@example.com'
TEST_MAIL_SENDER_FIRST_NAME = 'First Name'
TEST_MAIL_SENDER_SIGNATURE = "***********\n A Signature\n ***********\n"
TEST_MAIL_SUBJECT = 'This is a Test Email'
TEST_MAIL_TEXT = 'This is a main content of the email\n'
REPLACED_TEST_MAIL_TEXT = 'This is a main content of the email<br>'
TEST_MAIL_ATTACHMENT_BODY = 'file content'
TEST_MAIL_ATTACHMENT_TYPE = 'text/plain'
TEST_SHARED_EMAIL_ADDRESS = 'testsharedmail@example.com'
TEST_MAIL_TEXT_FORMAT = 'text'

TEST_HTML_TEXT = '<b>これはテストメッセージです。</b>\n<script>スクリプトは許可されていない</script>'
REPLACED_TEST_HTML_TEXT = '<b>これはテストメッセージです。</b><br>'


class SendMailServiceTestCase(MediaFolderSetupMixin, TestCase):
    attachment = None
    valid_contacts_num = 1  # Depends on Test Data.

    fixtures = [
        'app_staffing/tests/fixtures/addon_datas.json',
        'app_staffing/tests/fixtures/type_preferences.json',
        'app_staffing/tests/fixtures/skill_preferences.json',
        'app_staffing/tests/fixtures/organization_master_data.json'
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        if not os.path.exists(os.path.dirname(TEST_FILE_PATH)):
            os.makedirs(os.path.dirname(TEST_FILE_PATH))

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.company = CompanyFactory.create()
        # send_mail function will search an SMTP server which corresponds to sender's company.
        SMTPServerFactory.create(company=cls.company)
        ScheduledEmailSettingFactory.create(company=cls.company, use_open_count=True)
        AddonFactory.create(company=cls.company, addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID)

        scheduled_mail, cls.attachment = create_scheduled_mail_data(
            company=cls.company,
            sender_address=TEST_MAIL_SENDER_ADDRESS,
            sender_first_name=TEST_MAIL_SENDER_FIRST_NAME,
            sender_signature=TEST_MAIL_SENDER_SIGNATURE,
            subject=TEST_MAIL_SUBJECT,
            content=TEST_MAIL_TEXT,
            attachment_body=TEST_MAIL_ATTACHMENT_BODY,
            text_format=TEST_MAIL_TEXT_FORMAT,
        )

        cls.scheduled_mail_id = scheduled_mail.id

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(path=os.path.dirname(TEST_FILE_PATH))
        cls.attachment.file.delete()
        super().tearDownClass()

    def setUp(self):
        # Load the scheduled email instance from DB before every test to avoid the side effects of each test cases.
        self.instance = ScheduledEmail.objects.get(id=self.scheduled_mail_id)

    def test_create_email_instance(self):

        with open(TEST_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        from_address = 'testSender@example.com'
        contact_last_name = 'ご担当者'
        contact_first_name = '氏名'
        contact_display_name = 'ご担当者 氏名'
        contact_organization_name = "株式会社Example"
        to_addresses = ['Receiver@example.com']
        cc_addresses = ['ReceiverCC1@example.com', 'ReceiverCC2@example.com']
        subject = 'メール送信テスト'
        content = 'これはテストメッセージです。'
        text_format='text'
        # Note: Check if HTML tags like <, > must be rendered correctly, and not sanitized.
        sender_signature = '< **************** >\n' \
                           'A Test Send User\n' \
                           '< **************** >\n'

        attachments = [{'filename': attachment.file.name, 'content': open(TEST_FILE_PATH, "r")} for attachment in self.instance.attachments.all()]


        instance = _create_email_instance(
            from_address=from_address, contact_last_name=contact_last_name, contact_first_name=contact_first_name, contact_display_name=contact_display_name, contact_organization_name=contact_organization_name,
            to_addresses=to_addresses, cc_addresses=cc_addresses, subject=subject,
            content=content, sender_signature=sender_signature, attachments=attachments, text_format=text_format,
            connection=MagicMock()  # An Actual connection is not required for this test.
        )

        # Check the content of the generated email message instance.
        self.assertTrue(isinstance(instance, EmailMessage))
        self.assertTemplateUsed(template_name=EMAIL_TEMPLATE_NAME)

        print('===========Preview the content of the Email============')
        print(instance.body)
        print('===========          Preview end           ============')

        self.assertTrue(contact_organization_name in instance.body)
        self.assertTrue(contact_last_name in instance.body)
        self.assertTrue(contact_first_name in instance.body)
        self.assertTrue(contact_display_name in instance.body)
        self.assertTrue(sender_signature in instance.body)
        self.assertEqual(from_address, instance.from_email)
        self.assertEqual(to_addresses, instance.to)
        self.assertEqual(cc_addresses, instance.cc)
        self.assertTrue(instance.attachments[0])  # TODO: Make this assertion precise.
        self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in instance.body)
        self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' not in instance.body)
        self.assertTrue('<img src=' not in instance.body)

    def test_create_html_email_instance(self):

        with open(TEST_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        from_address = 'testSender@example.com'
        contact_last_name = 'ご担当者'
        contact_first_name = '氏名'
        contact_display_name = 'ご担当者 氏名'
        contact_organization_name = "株式会社Example"
        to_addresses = ['Receiver@example.com']
        cc_addresses = ['ReceiverCC1@example.com', 'ReceiverCC2@example.com']
        subject = 'メール送信テスト'
        content = TEST_HTML_TEXT
        text_format='html'

        sender_signature = '****************\n' \
                           'A Test Send User\n' \
                           '****************\n'

        attachments = [{'filename': attachment.file.name, 'content': open(TEST_FILE_PATH, "r")} for attachment in self.instance.attachments.all()]

        html_instance = _create_email_instance(
            from_address=from_address, contact_last_name=contact_last_name, contact_first_name=contact_first_name, contact_display_name=contact_display_name, contact_organization_name=contact_organization_name,
            to_addresses=to_addresses, cc_addresses=cc_addresses, subject=subject,
            content=content, sender_signature=sender_signature, attachments=attachments, text_format=text_format,
            connection=MagicMock()
        )

        self.assertTrue(isinstance(html_instance, EmailMessage))
        self.assertTemplateUsed(template_name=EMAIL_TEMPLATE_NAME)

        self.assertTrue(contact_organization_name in html_instance.body)
        self.assertTrue(contact_last_name in html_instance.body)
        self.assertTrue(contact_first_name in html_instance.body)
        self.assertTrue(contact_display_name in html_instance.body)
        self.assertTrue(sender_signature in html_instance.body)
        self.assertEqual(from_address, html_instance.from_email)
        self.assertEqual(to_addresses, html_instance.to)
        self.assertEqual(cc_addresses, html_instance.cc)
        self.assertTrue(html_instance.attachments[0])
        self.assertTrue('株式会社Example' in html_instance.body)
        self.assertTrue('ご担当者 氏名 様' in html_instance.body)
        self.assertTrue('<b>これはテストメッセージです。</b>\n' in html_instance.body)
        self.assertTrue('&lt;script&gt;スクリプトは許可されていない&lt;/script&gt;' in html_instance.body)
        self.assertTrue('<pre>***********' in html_instance.body)
        self.assertTrue('A Test Send User' in html_instance.body)
        self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in html_instance.body)
        self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' in html_instance.body)

    def test_create_html_email_instance_with_addon(self):

        with open(TEST_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        from_address = 'testSender@example.com'
        contact_last_name = 'ご担当者'
        contact_first_name = '氏名'
        contact_display_name = 'ご担当者 氏名'
        contact_organization_name = "株式会社Example"
        to_addresses = ['Receiver@example.com']
        cc_addresses = ['ReceiverCC1@example.com', 'ReceiverCC2@example.com']
        subject = 'メール送信テスト'
        content = TEST_HTML_TEXT
        text_format='html'

        sender_signature = '****************\n' \
                           'A Test Send User\n' \
                           '****************\n'
        attachments = [{'filename': attachment.file.name, 'content': open(TEST_FILE_PATH, "r")} for attachment in self.instance.attachments.all()]

        addon = Addon.objects.create(company=self.instance.company, addon_master_id=settings.REMOVE_PROMOTION_ADDON_MASTER_ID)
        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.use_remove_promotion = True
        scheduled_email_setting.file_type = ScheduledEmailSettingFileStatus.FILE
        scheduled_email_setting.save()

        html_instance = _create_email_instance(
            from_address=from_address, contact_last_name=contact_last_name, contact_first_name=contact_first_name, contact_display_name=contact_display_name, contact_organization_name=contact_organization_name,
            to_addresses=to_addresses, cc_addresses=cc_addresses, subject=subject,
            content=content, sender_signature=sender_signature, attachments=attachments, text_format=text_format,
            connection=MagicMock(), scheduled_email_setting=scheduled_email_setting, is_use_remove_promotion_available=addon
        )

        self.assertTrue(isinstance(html_instance, EmailMessage))
        self.assertTemplateUsed(template_name=EMAIL_TEMPLATE_NAME)

        self.assertTrue(contact_organization_name in html_instance.body)
        self.assertTrue(contact_last_name in html_instance.body)
        self.assertTrue(contact_first_name in html_instance.body)
        self.assertTrue(contact_display_name in html_instance.body)
        self.assertTrue(sender_signature in html_instance.body)
        self.assertEqual(from_address, html_instance.from_email)
        self.assertEqual(to_addresses, html_instance.to)
        self.assertEqual(cc_addresses, html_instance.cc)
        self.assertTrue(html_instance.attachments[0])
        self.assertTrue('株式会社Example' in html_instance.body)
        self.assertTrue('ご担当者 氏名 様' in html_instance.body)
        self.assertTrue('<b>これはテストメッセージです。</b>\n' in html_instance.body)
        self.assertTrue('&lt;script&gt;スクリプトは許可されていない&lt;/script&gt;' in html_instance.body)
        self.assertTrue('<pre>***********' in html_instance.body)
        self.assertTrue('A Test Send User' in html_instance.body)
        self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in html_instance.body)
        self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' not in html_instance.body)

    def test_create_html_email_instance_with_open_count_tag(self):

        with open(TEST_FILE_PATH, 'w') as f:
            f.write('This is a test data for attachments.')

        from_address = 'testSender@example.com'
        contact_last_name = 'ご担当者'
        contact_first_name = '氏名'
        contact_display_name = 'ご担当者 氏名'
        contact_organization_name = "株式会社Example"
        to_addresses = ['Receiver@example.com']
        cc_addresses = ['ReceiverCC1@example.com', 'ReceiverCC2@example.com']
        subject = 'メール送信テスト'
        content = TEST_HTML_TEXT
        text_format='html'
        contact_id = 'this_is_test_contact_id'
        email_id = 'this_is_test_email_id'

        sender_signature = '****************\n' \
                           'A Test Send User\n' \
                           '****************\n'

        attachments = [{'filename': attachment.file.name, 'content': open(TEST_FILE_PATH, "r")} for attachment in self.instance.attachments.all()]

        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.file_type = ScheduledEmailSettingFileStatus.FILE
        scheduled_email_setting.save()
        addon = Addon.objects.get(company=self.instance.company, addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID)
        html_instance = _create_email_instance(
            from_address=from_address, contact_last_name=contact_last_name, contact_first_name=contact_first_name, contact_display_name=contact_display_name, contact_organization_name=contact_organization_name,
            to_addresses=to_addresses, cc_addresses=cc_addresses, subject=subject,
            content=content, sender_signature=sender_signature, attachments=attachments, text_format=text_format, contact_id=contact_id, email_id=email_id, scheduled_email_setting=scheduled_email_setting,
            is_open_count_available=addon,connection=MagicMock()
        )

        print('===========Preview the content of the Email============')
        print(html_instance.body)
        print('===========          Preview end           ============')

        self.assertTrue(isinstance(html_instance, EmailMessage))
        self.assertTemplateUsed(template_name=EMAIL_TEMPLATE_NAME)

        self.assertTrue(contact_organization_name in html_instance.body)
        self.assertTrue(contact_last_name in html_instance.body)
        self.assertTrue(contact_first_name in html_instance.body)
        self.assertTrue(contact_display_name in html_instance.body)
        self.assertTrue(sender_signature in html_instance.body)
        self.assertEqual(from_address, html_instance.from_email)
        self.assertEqual(to_addresses, html_instance.to)
        self.assertEqual(cc_addresses, html_instance.cc)
        self.assertTrue(html_instance.attachments[0])
        self.assertTrue('株式会社Example' in html_instance.body)
        self.assertTrue('ご担当者 氏名 様' in html_instance.body)
        self.assertTrue('<b>これはテストメッセージです。</b>\n' in html_instance.body)
        self.assertTrue('&lt;script&gt;スクリプトは許可されていない&lt;/script&gt;' in html_instance.body)
        self.assertTrue('<pre>***********' in html_instance.body)
        self.assertTrue('A Test Send User' in html_instance.body)

        tag = '<img src=\'{0}?cid={1}&eid={2}\' />'.format(settings.MAIL_OPEN_COUNT_ACCESS_POINT, contact_id, email_id)
        self.assertTrue( tag in html_instance.body)

    def test_create_email_instances_from_db_record(self):
        # This email has two targets. so the number of email instance will be 2.
        copies, email_message_instances = _generate_emails(
            self.instance,
            connection=MagicMock(),  # An Actual connection is not required for this test.
            shared_email_address=None,
        )

        # Check the number of email instances generated from a scheduled Email.
        self.assertEqual(
            self.valid_contacts_num,
            len(email_message_instances)
        )

        # Check Each attribute value in EmailMessage instances.
        for email_message_instance in email_message_instances:

            with self.subTest(email_message=f'TO:{email_message_instance.to}'):

                # Check the EmailMessage subject.
                self.assertEqual(self.instance.subject, email_message_instance.subject)

                # Check the from address is same as the ScheduledEmail instance.
                self.assertEqual(self.instance.sender.email, email_message_instance.from_email)

                # Important: Check the To address length is 1.
                self.assertEqual(len(email_message_instance.to), 1)
                # Check all CC email addresses is related to the TO email address.
                receiver_email_username = email_message_instance.to[0].split('@')[0]  # Extract a strings before @ mark of the email address.
                cc_regex = r'^{0}_cc[0-9]@example.com$'.format(receiver_email_username)  # Warning: this depends on fixtures.

                for cc_address in email_message_instance.cc:
                    with self.subTest(
                            to_address=email_message_instance.to[0],
                            cc_address=cc_address
                    ):
                        self.assertTrue(re.match(pattern=cc_regex, string=cc_address))

                # Check content is rendered in the message body.
                self.assertTrue(self.instance.text in email_message_instance.body)

                # Check content is rendered in the message body.
                self.assertTrue(self.instance.sender.email_signature in email_message_instance.body)

                # Check if receiver name is rendered in the message body.
                self.assertTrue('Receiver' in email_message_instance.body)

    def test_send_mail(self):
        """
        WARNING: The outbox attribute is a special attribute that is created only when the locmem email backend is used.
        So this test only runs in a local environment.
        """
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        # Test that one message has been sent.

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)
        url_download = f"https://{settings.HOST_NAME}/scheduledMail/files/download"

        # Verify that the common parameter of messages is correct.
        # Note: Non common parameters like To and CC address are verified in other unit tests.
        for email in mail.outbox:
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.
            self.assertNotIn(url_download, email.body)

    def test_send_mail_with_url_setting(self):
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        # Test that one message has been sent.

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)
        url_download = f"https://{settings.HOST_NAME}/scheduledMail/files/download"
        # Body should contain url download and without attachments
        for email in mail.outbox:
            self.assertIn(url_download, email.body)
            self.assertIsNotNone(email.body)
            self.assertEqual(email.attachments, [])  # Checking the attachment content.
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)

    def test_send_email_with_send_copy_for_sender_option(self):
        # Update an email instance to have a send_copy option.
        self.instance.send_copy_to_sender = True
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1) # 控えメールは送信件数にカウントされない

        # Note: Expecting the copy of the email for the sender is sent first.
        copy_email = mail.outbox[0]

        # Note: The subject and sender of the copied email is a little bit different from the original one.
        self.assertEqual(settings.DEFAULT_FROM_EMAIL, copy_email.from_email)
        self.assertEqual(f'配信メール控え: {TEST_MAIL_SUBJECT}', copy_email.subject)
        # The rest of the things are same as the original one.
        self.assertTrue(DUMMY_CONTACT_LAST_NAME in copy_email.body)
        self.assertTrue(DUMMY_CONTACT_FIRST_NAME in copy_email.body)
        self.assertTrue(DUMMY_ORGANIZATION_NAME in copy_email.body)
        self.assertTrue(TEST_MAIL_TEXT in copy_email.body)
        self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in copy_email.body)
        self.assertEqual(TEST_FILE_NAME, copy_email.attachments[0][0])
        self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, copy_email.attachments[0][1])  # Checking the attachment content.
        self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, copy_email.attachments[0][2])  # Checking the attachment type.
        self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in copy_email.body)

        for email in mail.outbox[1:]:  # Rest of the emails are the emails to send others.
            self.assertEqual(TEST_MAIL_SENDER_ADDRESS, email.from_email)
            self.assertEqual(TEST_MAIL_SUBJECT, email.subject)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_email_with_send_copy_for_share_option(self):
        # Update an email instance to have a send_copy option.
        self.instance.send_copy_to_share = True
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=TEST_SHARED_EMAIL_ADDRESS)

        self.assertEqual(result['send_total_count'], 1) # 控えメールは送信件数にカウントされない

        copy_email = mail.outbox[0]

        self.assertEqual([TEST_SHARED_EMAIL_ADDRESS], copy_email.to)
        self.assertEqual(TEST_MAIL_SENDER_ADDRESS, copy_email.from_email)
        self.assertEqual(f'配信メール控え: {TEST_MAIL_SUBJECT}', copy_email.subject)
        self.assertTrue(DUMMY_CONTACT_LAST_NAME in copy_email.body)
        self.assertTrue(DUMMY_CONTACT_FIRST_NAME in copy_email.body)
        self.assertTrue(DUMMY_ORGANIZATION_NAME in copy_email.body)
        self.assertTrue(TEST_MAIL_TEXT in copy_email.body)
        self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in copy_email.body)
        self.assertEqual(TEST_FILE_NAME, copy_email.attachments[0][0])
        self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, copy_email.attachments[0][1])  # Checking the attachment content.
        self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, copy_email.attachments[0][2])  # Checking the attachment type.
        self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in copy_email.body)

        for email in mail.outbox[1:]:  # Rest of the emails are the emails to send others.
            self.assertEqual(TEST_MAIL_SENDER_ADDRESS, email.from_email)
            self.assertEqual(TEST_MAIL_SUBJECT, email.subject)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_html_email(self):
        self.instance.text_format = 'html'
        self.instance.text = TEST_HTML_TEXT

        send_mails(scheduled_email_instance=self.instance, shared_email_address=TEST_SHARED_EMAIL_ADDRESS)

        copy_email = mail.outbox[0]

        self.assertTrue('Content-Type: text/html;' in copy_email.message().as_string())

        self.assertTrue("Receiver's Corporation" in copy_email.body)
        self.assertTrue('Receiver1 FirstName1 様' in copy_email.body)
        self.assertTrue('<b>これはテストメッセージです。</b><br>&lt;script&gt;スクリプトは許可されていない&lt;/script&gt;' in copy_email.body)
        self.assertTrue('<pre>***********' in copy_email.body)
        self.assertTrue('A Signature' in copy_email.body)

    def test_send_mail_with_error(self):
        """
        WARNING: The outbox attribute is a special attribute that is created only when the locmem email backend is used.
        So this test only runs in a local environment.
        """
        mock = Mock(side_effect=Exception('Boom!'))
        with patch('django.core.mail.EmailMessage.send', mock):
            result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        # Test that one message has not been sent.
        self.assertEqual(len(mail.outbox), 0)

    def test_send_html_email_with_send_copy_for_sender_option(self):
        # Update an email instance to have a send_copy option.
        self.instance.send_copy_to_sender = True
        self.instance.text_format = 'html'

        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.file_type = ScheduledEmailSettingFileStatus.FILE
        scheduled_email_setting.save()
        addon = Addon.objects.get(company=self.instance.company, addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None, scheduled_email_setting=scheduled_email_setting, is_open_count_available=addon)

        self.assertEqual(result['send_total_count'], 1) # 控えメールは送信件数にカウントされない

        # Note: Expecting the copy of the email for the sender is sent first.
        copy_email = mail.outbox[0]

        # Note: The subject and sender of the copied email is a little bit different from the original one.
        self.assertEqual(settings.DEFAULT_FROM_EMAIL, copy_email.from_email)
        self.assertEqual(f'配信メール控え: {TEST_MAIL_SUBJECT}', copy_email.subject)
        # The rest of the things are same as the original one.
        self.assertTrue(DUMMY_CONTACT_LAST_NAME in copy_email.body)
        self.assertTrue(DUMMY_CONTACT_FIRST_NAME in copy_email.body)
        self.assertTrue(DUMMY_ORGANIZATION_NAME in copy_email.body)
        self.assertTrue(REPLACED_TEST_MAIL_TEXT in copy_email.body)
        self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in copy_email.body)
        self.assertEqual(TEST_FILE_NAME, copy_email.attachments[0][0])
        self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, copy_email.attachments[0][1])  # Checking the attachment content.
        self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, copy_email.attachments[0][2])  # Checking the attachment type.
        self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in copy_email.body)

        for email in mail.outbox[1:]:  # Rest of the emails are the emails to send others.
            self.assertEqual(TEST_MAIL_SENDER_ADDRESS, email.from_email)
            self.assertEqual(TEST_MAIL_SUBJECT, email.subject)
            self.assertTrue(REPLACED_TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.
            self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' in email.body)

    def test_send_html_email_with_send_copy_for_share_option(self):
        # Update an email instance to have a send_copy option.
        self.instance.send_copy_to_share = True
        self.instance.text_format = 'html'

        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.file_type = ScheduledEmailSettingFileStatus.FILE
        scheduled_email_setting.save()
        addon = Addon.objects.get(company=self.instance.company, addon_master_id=settings.IS_OPEN_COUNT_AVAILABLE_ADDON_MASTER_ID)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=TEST_SHARED_EMAIL_ADDRESS, scheduled_email_setting=scheduled_email_setting, is_open_count_available=addon)

        self.assertEqual(result['send_total_count'], 1) # 控えメールは送信件数にカウントされない

        copy_email = mail.outbox[0]

        self.assertEqual([TEST_SHARED_EMAIL_ADDRESS], copy_email.to)
        self.assertEqual(TEST_MAIL_SENDER_ADDRESS, copy_email.from_email)
        self.assertEqual(f'配信メール控え: {TEST_MAIL_SUBJECT}', copy_email.subject)
        self.assertTrue(DUMMY_CONTACT_LAST_NAME in copy_email.body)
        self.assertTrue(DUMMY_CONTACT_FIRST_NAME in copy_email.body)
        self.assertTrue(DUMMY_ORGANIZATION_NAME in copy_email.body)
        self.assertTrue(REPLACED_TEST_MAIL_TEXT in copy_email.body)
        self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in copy_email.body)
        self.assertEqual(TEST_FILE_NAME, copy_email.attachments[0][0])
        self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, copy_email.attachments[0][1])  # Checking the attachment content.
        self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, copy_email.attachments[0][2])  # Checking the attachment type.
        self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in copy_email.body)

        for email in mail.outbox[1:]:  # Rest of the emails are the emails to send others.
            self.assertEqual(TEST_MAIL_SENDER_ADDRESS, email.from_email)
            self.assertEqual(TEST_MAIL_SUBJECT, email.subject)
            self.assertTrue(REPLACED_TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.
            self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' in email.body)

    def test_send_html_email_with_setting_not_allowed_open_count(self):
        # Update an email instance to have a send_copy option.
        self.instance.send_copy_to_share = True
        self.instance.text_format = 'html'

        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.file_type = ScheduledEmailSettingFileStatus.FILE
        scheduled_email_setting.save()

        send_mails(scheduled_email_instance=self.instance, shared_email_address=TEST_SHARED_EMAIL_ADDRESS, scheduled_email_setting=scheduled_email_setting)

        for email in mail.outbox[1:]:  # Rest of the emails are the emails to send others.
            self.assertEqual(TEST_MAIL_SENDER_ADDRESS, email.from_email)
            self.assertEqual(TEST_MAIL_SUBJECT, email.subject)
            self.assertTrue(REPLACED_TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.
            self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in email.body)
            self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' in email.body)

    def test_send_html_email_with_setting_not_use_open_count(self):
        # Update an email instance to have a send_copy option.
        self.instance.send_copy_to_share = True
        self.instance.text_format = 'html'

        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.use_open_count = False
        scheduled_email_setting.file_type = ScheduledEmailSettingFileStatus.FILE
        scheduled_email_setting.save()
        send_mails(scheduled_email_instance=self.instance, shared_email_address=TEST_SHARED_EMAIL_ADDRESS, scheduled_email_setting=scheduled_email_setting)

        for email in mail.outbox[1:]:  # Rest of the emails are the emails to send others.
            self.assertEqual(TEST_MAIL_SENDER_ADDRESS, email.from_email)
            self.assertEqual(TEST_MAIL_SUBJECT, email.subject)
            self.assertTrue(REPLACED_TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.
            self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in email.body)
            self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' in email.body)

    def test_send_html_email_with_setting_not_use_remove_promotion(self):
        # Update an email instance to have a send_copy option.
        self.instance.send_copy_to_share = True
        self.instance.text_format = 'html'

        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.file_type = ScheduledEmailSettingFileStatus.FILE
        scheduled_email_setting.save()
        send_mails(scheduled_email_instance=self.instance, shared_email_address=TEST_SHARED_EMAIL_ADDRESS, scheduled_email_setting=scheduled_email_setting, )

        for email in mail.outbox[1:]:  # Rest of the emails are the emails to send others.
            self.assertEqual(TEST_MAIL_SENDER_ADDRESS, email.from_email)
            self.assertEqual(TEST_MAIL_SUBJECT, email.subject)
            self.assertTrue(REPLACED_TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.
            self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in email.body)
            self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' in email.body)

    def test_send_html_email_with_setting_not_allowed_remove_promotion(self):
        # Update an email instance to have a send_copy option.
        self.instance.send_copy_to_share = True
        self.instance.text_format = 'html'

        addon = Addon.objects.create(company=self.instance.company, addon_master_id=settings.REMOVE_PROMOTION_ADDON_MASTER_ID)
        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.use_remove_promotion = False
        scheduled_email_setting.file_type = ScheduledEmailSettingFileStatus.FILE
        scheduled_email_setting.save()

        send_mails(scheduled_email_instance=self.instance, shared_email_address=TEST_SHARED_EMAIL_ADDRESS, scheduled_email_setting=scheduled_email_setting, is_use_remove_promotion_available=addon)

        for email in mail.outbox[1:]:  # Rest of the emails are the emails to send others.
            self.assertEqual(TEST_MAIL_SENDER_ADDRESS, email.from_email)
            self.assertEqual(TEST_MAIL_SUBJECT, email.subject)
            self.assertTrue(REPLACED_TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.
            self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in email.body)
            self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' in email.body)

    def test_send_html_email_with_setting_use_remove_promotion(self):
        # Update an email instance to have a send_copy option.
        self.instance.send_copy_to_share = True
        self.instance.send_copy_to_sender = True
        self.instance.text_format = 'html'

        addon = Addon.objects.create(company=self.instance.company, addon_master_id=settings.REMOVE_PROMOTION_ADDON_MASTER_ID)
        scheduled_email_setting = ScheduledEmailSetting.objects.get(company=self.instance.company)
        scheduled_email_setting.use_remove_promotion = True
        scheduled_email_setting.file_type = ScheduledEmailSettingFileStatus.FILE
        scheduled_email_setting.save()
        send_mails(scheduled_email_instance=self.instance, shared_email_address=TEST_SHARED_EMAIL_ADDRESS, scheduled_email_setting=scheduled_email_setting, is_use_remove_promotion_available=addon)

        for email in mail.outbox[0:]:
            self.assertTrue(REPLACED_TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.
            self.assertTrue(f'<img src=\'{settings.MAIL_OPEN_COUNT_ACCESS_POINT}' not in email.body)
            self.assertTrue(f'<img src=\'{settings.SCHEDULED_EMAIL_PROMOTION_IMAGE_URL}' not in email.body)

    def test_send_mail_with_search_condition_job(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=target.contact.organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        new_contact_preference = ContactPreference.objects.get(contact=new_contact)
        new_contact_preference.wants_location_kanto_japan = True
        new_contact_preference.job_syouryu = 1
        new_contact_preference.save()
        ContactJobTypePreference.objects.create(contact=new_contact, company=self.instance.company, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
        ContactJobSkillPreference.objects.create(contact=new_contact, company=self.instance.company, skill_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "job",
                "wants_location_kanto_japan": True,
                "jobtype": "dev",
                "jobtype_dev": "jobtype_dev_designer",
                "jobskill_dev_youken": True,
                "job_syouryu": 1
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_personnel(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=target.contact.organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        new_contact_preference = ContactPreference.objects.get(contact=new_contact)
        new_contact_preference.wants_location_kanto_japan = True
        new_contact_preference.personnel_syouryu = 4
        new_contact_preference.job_koyou_proper = True
        new_contact_preference.save()
        ContactPersonnelTypePreference.objects.create(contact=new_contact, company=self.instance.company, type_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
        ContactPersonnelSkillPreference.objects.create(contact=new_contact, company=self.instance.company, skill_preference_id='185c2a9b-ec13-4194-bd85-386de64f5ee2')
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "personnel",
                "wants_location_kanto_japan": True,
                "personneltype_dev": True,
                "job_koyou": "proper",
                "personnel_syouryu": 4,
                "personneltype_dev_designer": True,
                "personnelskill_dev_youken": True
            })

        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_other(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=target.contact.organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other"
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        # otherの場合は何もフィルターが動かないので、元々の1件プラス上部で作成した1件の、合計2件が送信される
        self.assertEqual(result['send_total_count'], 2)
        self.assertEqual(len(mail.outbox), 2)

        for email in mail.outbox:
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_organization_category(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')
        organization = Organization.objects.create(
            name="Receiver's Corporation",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=False,
            company=target.company,
            organization_category_id=4 # client
        )
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
                "contact__organization__category_client": True,
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_staff(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')
        staff = User.objects.create(
            username='test_send_mail_staff@example.com',
            first_name='Hoge',
            last_name='Last Name',
            email='test_send_mail_staff@example.com',
            email_signature='hoge',
            password='ajke9gk!ks',
            is_superuser=False,
            is_user_admin=False,
            is_active=True,
            company=self.instance.company,
        )
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=target.contact.organization,
            staff=staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
                "contact__staff": str(staff.id)
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)
        self.assertEqual(len(mail.outbox), 1)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_contact_category(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=target.contact.organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ContactCategory.objects.create(
            user=self.instance.sender,
            contact=new_contact,
            category='heart',
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
                "contact__category": "heart",
                "category_inequality": "eq",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)
        self.assertEqual(len(mail.outbox), 1)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_contact_category_not_eq(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=target.contact.organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ContactCategory.objects.create(
            user=self.instance.sender,
            contact=new_contact,
            category='heart',
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
                "contact__category": "heart",
                "category_inequality": "not_eq",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)
        self.assertEqual(len(mail.outbox), 1)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver1@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_tag_and(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=target.contact.organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        tag1 = Tag.objects.create(
            company=self.instance.company,
            value='hogehoge',
            internal_value='hogehoge'
        )
        tag2 = Tag.objects.create(
            company=self.instance.company,
            value='fugafuga',
            internal_value='fugafuga'
        )
        ContactTagAssignment.objects.create(
            tag=tag1,
            contact=new_contact,
            company=self.instance.company,
        )
        ContactTagAssignment.objects.create(
            tag=tag2,
            contact=new_contact,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
                "contact__tags": [str(tag1.id), str(tag2.id)],
                "contact__tags__suffix": "and",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)
        self.assertEqual(len(mail.outbox), 1)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_tag_and(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=target.contact.organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        tag1 = Tag.objects.create(
            company=self.instance.company,
            value='hogehoge',
            internal_value='hogehoge'
        )
        tag2 = Tag.objects.create(
            company=self.instance.company,
            value='fugafuga',
            internal_value='fugafuga'
        )
        ContactTagAssignment.objects.create(
            tag=tag1,
            contact=target.contact,
            company=self.instance.company,
        )
        ContactTagAssignment.objects.create(
            tag=tag2,
            contact=new_contact,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
                "contact__tags": [str(tag1.id), str(tag2.id)],
                "contact__tags__suffix": "or",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )

        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)

        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 2)
        self.assertEqual(len(mail.outbox), 2)

        for email in mail.outbox:
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_capital_man_yen(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')

        company = target.company
        company.capital_man_yen_required_for_transactions = 1000
        company.save()

        organization = target.contact.organization
        organization.capital_man_yen = 100
        organization.save()

        organization = Organization.objects.create(
            name="Receiver's Corporation",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=False,
            company=target.company,
            organization_category_id=4, # client
            capital_man_yen=2000
        )
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_pmark_or_isms(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')

        company = target.company
        company.p_mark_or_isms = True
        company.save()

        organization = target.contact.organization
        organization.has_p_mark_or_isms = False
        organization.save()

        organization = Organization.objects.create(
            name="Receiver's Corporation",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=False,
            company=target.company,
            organization_category_id=4, # client
            has_p_mark_or_isms=True
        )
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_invoice_system(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')

        company = target.company
        company.invoice_system = True
        company.save()

        organization = target.contact.organization
        organization.has_invoice_system = False
        organization.save()

        organization = Organization.objects.create(
            name="Receiver's Corporation",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=False,
            company=target.company,
            organization_category_id=4, # client
            has_invoice_system=True
        )
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_haken(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')

        company = target.company
        company.haken = True
        company.save()

        organization = target.contact.organization
        organization.has_haken = False
        organization.save()

        organization = Organization.objects.create(
            name="Receiver's Corporation",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=False,
            company=target.company,
            organization_category_id=4, # client
            has_haken=True
        )
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_establishment_year(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')

        company = target.company
        company.establishment_year = 10
        company.save()

        organization = target.contact.organization
        organization.establishment_date = datetime.now() - relativedelta(years=1)
        organization.save()

        organization = Organization.objects.create(
            name="Receiver's Corporation",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=False,
            company=target.company,
            organization_category_id=4, # client
            establishment_date=datetime.now() - relativedelta(years=15)
        )
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_search_condition_distribution(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')

        company = target.company
        company.capital_man_yen_required_for_transactions = 1000
        company.p_mark_or_isms = True
        company.invoice_system = True
        company.haken = True
        company.establishment_year = 10
        company.save()

        organization = target.contact.organization
        organization.capital_man_yen = 100
        organization.has_p_mark_or_isms = False
        organization.has_invoice_system = False
        organization.has_haken = False
        organization.establishment_date = datetime.now() - relativedelta(years=1)
        organization.save()

        organization = Organization.objects.create(
            name="Receiver's Corporation",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=False,
            company=target.company,
            organization_category_id=4, # client
            capital_man_yen=300,
            has_p_mark_or_isms=False,
            has_invoice_system=False,
            has_haken=False,
            establishment_date=datetime.now() - relativedelta(years=2),
            has_distribution=True,
        )
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_organization_is_blacklisted(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')
        organization = Organization.objects.create(
            name="Receiver's Corporation",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=True,
            company=target.company,
            organization_category_id=4 # client
        )
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver1@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_send_mail_with_organization_param_null(self):
        target = self.instance.targets.get(contact__email='receiver1@example.com')

        company = target.company
        company.capital_man_yen_required_for_transactions = 1000
        company.p_mark_or_isms = True
        company.invoice_system = True
        company.haken = True
        company.establishment_year = 10
        company.save()

        organization = target.contact.organization
        organization.capital_man_yen = 1
        organization.save()

        organization = Organization.objects.create(
            name="Receiver's Corporation",
            category='client',
            country='JP',
            score=3,
            is_blacklisted=False,
            company=target.company,
            organization_category_id=4, # client
            capital_man_yen=None,
            has_p_mark_or_isms=None,
            has_invoice_system=None,
            has_haken=None,
            establishment_date=None,
            has_distribution=True,
        )
        new_contact = Contact.objects.create(
            last_name='Receiver',
            email='receiver999@example.com',
            organization=organization,
            staff=target.contact.staff,
            company=self.instance.company,
        )
        ScheduledEmailSearchCondition.objects.create(
            email=self.instance,
            search_condition=json.dumps({
                "searchtype": "other",
            })
        )
        ScheduledEmailTarget.objects.create(
            email=self.instance,
            contact=new_contact,
            company=self.instance.company,
        )
        # without attachment when setting is url
        ScheduledEmailSetting.objects.filter(company=self.company).update(file_type=ScheduledEmailSettingFileStatus.FILE)
        result = send_mails(scheduled_email_instance=self.instance, shared_email_address=None)

        self.assertEqual(result['send_total_count'], 1)

        self.assertEqual(len(mail.outbox), self.valid_contacts_num)

        for email in mail.outbox:
            self.assertEqual(email.to, ['receiver999@example.com'])
            self.assertEqual(email.from_email, TEST_MAIL_SENDER_ADDRESS)
            self.assertEqual(email.subject, TEST_MAIL_SUBJECT)
            self.assertTrue(TEST_MAIL_TEXT in email.body)
            self.assertTrue(TEST_MAIL_SENDER_SIGNATURE in email.body)
            self.assertEqual(TEST_FILE_NAME, email.attachments[0][0])
            self.assertEqual(TEST_MAIL_ATTACHMENT_BODY, email.attachments[0][1])  # Checking the attachment content.
            self.assertEqual(TEST_MAIL_ATTACHMENT_TYPE, email.attachments[0][2])  # Checking the attachment type.

    def test_generate_mail_with_send_type_other(self):
        # mock search condition
        ScheduledEmailSearchCondition(
            email=self.instance,
            search_condition=json.dumps(dict(searchtype="other"))
        ).save()
        copies, email_message_instances = _generate_emails(
            self.instance,
            connection=MagicMock(),
            shared_email_address=None,
        )
        # Check the number of email instances generated from a scheduled Email.
        self.assertEqual(
            self.valid_contacts_num,  # one has no has_send_guide
            len(email_message_instances)
        )
        # Check Each attribute value in EmailMessage instances.
        for email_message_instance in email_message_instances:

            with self.subTest(email_message=f'TO:{email_message_instance.to}'):

                # Check the EmailMessage subject.
                self.assertEqual(self.instance.subject, email_message_instance.subject)

                # Check the from address is same as the ScheduledEmail instance.
                self.assertEqual(self.instance.sender.email, email_message_instance.from_email)

                # Important: Check the To address length is 1.
                self.assertEqual(len(email_message_instance.to), 1)
                # Check all CC email addresses is related to the TO email address.
                receiver_email_username = email_message_instance.to[0].split('@')[
                    0]  # Extract a strings before @ mark of the email address.
                cc_regex = r'^{0}_cc[0-9]@example.com$'.format(
                    receiver_email_username)  # Warning: this depends on fixtures.

                for cc_address in email_message_instance.cc:
                    with self.subTest(
                            to_address=email_message_instance.to[0],
                            cc_address=cc_address
                    ):
                        self.assertTrue(re.match(pattern=cc_regex, string=cc_address))

                # Check content is rendered in the message body.
                self.assertTrue(self.instance.text in email_message_instance.body)

                # Check content is rendered in the message body.
                self.assertTrue(self.instance.sender.email_signature in email_message_instance.body)

                # Check if receiver name is rendered in the message body.
                self.assertTrue('Receiver' in email_message_instance.body)
        # mock has send guide all false
        ContactPreference.objects.all().update(has_send_guide=False)
        copies, email_message_instances = _generate_emails(
            self.instance,
            connection=MagicMock(),
            shared_email_address=None,
        )
        # Check the number of email instances generated from a scheduled Email.
        self.assertEqual(
            0,  # no email has generated
            len(email_message_instances)
        )
