from django.core import mail
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.conf import settings

from app_staffing.models import Email, EmailAttachment
from app_staffing.services.email.forward_mail import forward_mail
from app_staffing.tests.helpers.setup import MediaFolderSetupMixin


class EmailForwardingTestCase(MediaFolderSetupMixin, TestCase):

    FIXTURE_TEST_EMAIL_ID = 'bcda5407-54ad-4637-bd5c-1d830f4e9fb5'
    FIXTURE_TEST_EMAIL_ID_NO_CC_NO_REPLY_TO = 'e0d2edbc-efd7-4f23-ab64-2d0dd405cd59'

    FORWARD_TO = 'forward@example.com'

    TEST_FILE_NAME = 'Sample.txt'  # Correspond to DB record name, not file name on the filesystem.
    TEXT_FILE_BODY = 'forwarding test'
    EML_FILE_NAME = 'ClickToReply.eml'

    ORIGINAL_CC_ON_FIXTURE = ['staff2@example.com', 'Staff3 <staff3@example.com>']

    fixtures = [
        'app_staffing/tests/fixtures/company.json',
        'app_staffing/tests/fixtures/users.json',
        'app_staffing/tests/fixtures/emails.json',
        'app_staffing/tests/fixtures/email_attachments.json'
    ]

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        # Check Fixture conditions
        instance = Email.objects.get(pk=cls.FIXTURE_TEST_EMAIL_ID)
        assert EmailAttachment.objects.filter(email=instance.id).exists()
        assert EmailAttachment.objects.get(email=instance.id).name == cls.TEST_FILE_NAME

        assert Email.objects.get(pk=cls.FIXTURE_TEST_EMAIL_ID_NO_CC_NO_REPLY_TO)

    def setUp(self):
        self.instance = Email.objects.get(pk=self.FIXTURE_TEST_EMAIL_ID)
        self.min_instance = Email.objects.get(pk=self.FIXTURE_TEST_EMAIL_ID_NO_CC_NO_REPLY_TO)

        # Mocking attachments
        # Create Actual Attachment Files for the test. (Overriding fixture data)
        self.attachment_instance = EmailAttachment.objects.get(email=self.FIXTURE_TEST_EMAIL_ID)
        self.attachment_instance.file = SimpleUploadedFile('sample_0123.txt', bytes(self.TEXT_FILE_BODY, encoding='utf-8'))
        self.attachment_instance.save()

    def tearDown(self):
        self.attachment_instance.file.delete()

    def test_forward_email(self):
        forward_mail(original_email=self.instance, forward_to=self.FORWARD_TO)

        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)

        # Verify that the common parameter of messages is correct.
        for message in mail.outbox:
            self.assertEqual(settings.DEFAULT_FROM_EMAIL, message.from_email)
            self.assertEqual(f'共有メール転送: {self.instance.subject}', message.subject)
            self.assertEqual(self.FORWARD_TO, message.to[0])
            self.assertFalse(message.reply_to)
            self.assertFalse(message.cc)

            # Check If there is an eml file as an attachment, which corresponds to the original email.
            self.assertEqual(self.EML_FILE_NAME, message.attachments[0][0])  # Checking the attachment name.
            self.assertEqual('message/rfc822', message.attachments[0][2])  # Checking the attachment type.

            # Check If the eml file contains the values of the original received email's one, except To header.
            eml = message.attachments[0][1]  # Extracting the body of the attachment.

            self.assertEqual(self.instance.from_header, eml.from_email)
            self.assertFalse(eml.to)  # To header must be empty in eml because if not empty,
            # the original receiver receives a reply email.
            self.assertCountEqual(self.instance.reply_to_values, eml.reply_to)
            self.assertCountEqual(self.instance.cc_values, eml.cc)
            self.assertEqual(self.instance.subject, eml.subject)
            self.assertEqual(self.instance.text, eml.body)
            self.assertEqual(self.TEST_FILE_NAME, eml.attachments[0][0])
            self.assertEqual(self.TEXT_FILE_BODY, eml.attachments[0][1])
