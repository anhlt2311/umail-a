from datetime import datetime, timedelta
from uuid import uuid4

import factory

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils import timezone

from app_staffing.models.email import SMTPServer, INBOUND, Email, EmailAttachment,\
    ScheduledEmail, ScheduledEmailAttachment, ScheduledEmailTarget, QUEUED, SENDING, ScheduledEmailSetting
from app_staffing.models.organization import Organization, Contact, ContactCC
from app_staffing.models.preferences import ContactPreference
from app_staffing.models.user import User

from app_staffing.tests.factories.company import CompanyFactory

TEST_FILE_NAME = 'test.txt'


class SMTPServerFactory(factory.DjangoModelFactory):
    company = None  # Please give a instance of company.
    hostname = 'smtp@example.com'
    port_number = 25
    use_tls = False
    use_ssl = False
    username = 'user@example.com'
    password = 'password'

    class Meta:
        model = SMTPServer


class ScheduledEmailSettingFactory(factory.DjangoModelFactory):
    company = None  # Please give a instance of company.
    use_open_count = False

    class Meta:
        model = ScheduledEmailSetting


class SharedEmailFactory(factory.DjangoModelFactory):
    message_id = factory.LazyFunction(uuid4)
    shared = True
    direction = INBOUND
    from_header = 'Dummy <dummy@example.com>'
    reply_to_header = 'DummyReplyTo <dummy_reply_to@example.com>'
    from_name = 'Dummy <dummy@example.com>'
    from_address = 'dummy@example.com'
    cc_header = 'CC <cc@example.com>'
    subject = 'Test Data'
    text = factory.Faker('sentence')  # We must use random text to make sure to avoid conflict of the message digest.
    html = '<p>This is a test email</p>'
    sent_date = factory.LazyFunction(datetime.now)
    has_attachments = True
    category = None
    estimated_category = None
    message_digest = None
    staff_in_charge = None
    company = CompanyFactory()
    created_time = factory.LazyFunction(datetime.now)
    modified_time = factory.LazyFunction(datetime.now)
    notifications_have_done = False

    class Meta:
        model = Email


class SharedEmailAttachmentFactory(factory.DjangoModelFactory):
    company = CompanyFactory()
    email = factory.SubFactory(SharedEmailFactory, company=factory.SelfAttribute('..company'))
    name = 'attachment.txt'
    category = INBOUND
    file = SimpleUploadedFile(content=b'abc', content_type='text/plain', name=name)

    class Meta:
        model = EmailAttachment


def create_scheduled_mail_data(
        company, # Pass a company object
        sender_first_name,
        sender_address,
        sender_signature,
        subject,
        content,
        attachment_body,
        text_format='text',
        create_timeout_data=False,
):

    user = User(
        user_service_id='user_4327893821',
        username=sender_address,
        first_name=sender_first_name,
        last_name='Last Name',
        email=sender_address,
        email_signature=sender_signature,
        password='ajke9gk!ks',
        is_superuser=False,
        is_user_admin=False,
        is_active=True,
        company=company,

    )
    user.save()

    organization1 = Organization(
        name="Receiver's Corporation",
        category='client',
        country='JP',
        score=3,
        is_blacklisted=False,
        company=company,
    )
    organization1.save()

    organization2 = Organization(
        name="Black Corporation",
        category='client',
        country='JP',
        score=3,
        is_blacklisted=True,
        company=company,
    )
    organization2.save()

    # Create receivers.

    contact1 = Contact(
        last_name='Receiver1',
        first_name='FirstName1',
        email='receiver1@example.com',
        organization=organization1,
        staff=user,
        company=company,
    )
    contact1.save()

    contact2 = Contact(
        last_name='Receiver2',
        email='receiver2@example.com',
        organization=organization2,
        staff=user,
        company=company,
    )
    contact2.save()

    # Create CC addresses for contacts.
    contact1cc1 = ContactCC(
        contact=contact1,
        email='receiver1_cc1@example.com',
        company=company,
    )
    contact1cc1.save()

    contact1cc2 = ContactCC(
        contact=contact1,
        email='receiver1_cc2@example.com',
        company=company,
    )
    contact1cc2.save()

    contact2cc1 = ContactCC(
        contact=contact2,
        email='receiver2_cc1@example.com',
        company=company,
    )
    contact2cc1.save()

    # Create Contact Profile Data
    # Note: Preference is created automatically with default values when we create a contact record.
    contact1_preference = ContactPreference.objects.get(contact=contact1)
    contact1_preference.save()

    contact2_preference = ContactPreference.objects.get(contact=contact2)
    contact2_preference.save()

    # Create Scheduled email
    if create_timeout_data:
        date_to_send = timezone.now() - timedelta(minutes=settings.EMAIL_SENDING_STATUS_TIMEOUT_MINUTES)
    else:
        date_to_send = timezone.now()

    scheduled_mail = ScheduledEmail(
        sender=user,
        subject=subject,
        text=content,
        date_to_send=date_to_send,
        company=company,
        text_format=text_format,
    )
    scheduled_mail.save()

    # Define targets of the scheduled email

    target1 = ScheduledEmailTarget(
        email=scheduled_mail,
        contact=contact1,
        company=company,
    )
    target1.save()

    target2 = ScheduledEmailTarget(
        email=scheduled_mail,
        contact=contact2,
        company=company,
    )
    target2.save()

    # Define attachments of the email.
    f = ContentFile(bytes(attachment_body, encoding='utf-8'), TEST_FILE_NAME)

    attachment = ScheduledEmailAttachment(
        email=scheduled_mail,
        file=f,
        name=TEST_FILE_NAME,
        company=company,
    )

    attachment.save()

    return scheduled_mail, attachment

class ScheduledEmailFactory(factory.DjangoModelFactory):
    sender = None
    subject = 'Test Data'
    text = 'this is test text'
    status = 'sent'
    date_to_send = factory.LazyFunction(datetime.now)
    send_copy_to_sender = False
    sent_date = factory.LazyFunction(datetime.now)
    send_total_count = 0
    send_success_count = 0
    send_error_count = 0
    send_type = 'job'
    text_format = 'text'
    open_count = 0
    send_copy_to_share = False
    scheduled_email_status = None
    scheduled_email_send_type = None

    class Meta:
        model = ScheduledEmail
