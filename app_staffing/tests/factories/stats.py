from app_staffing.models.stats import OrganizationStat, ContactStat, ScheduledEmailStat, StaffInChargeStat, SharedEmailStat
import factory

from app_staffing.tests.factories.company import CompanyFactory

class OrganizationStatsFactory(factory.DjangoModelFactory):
    company = CompanyFactory()
    target_date = '2000-01-01'
    prospective = 0
    approached = 0
    exchanged = 0
    client = 0

    class Meta:
        model = OrganizationStat

class ContactStatsFactory(factory.DjangoModelFactory):
    company = CompanyFactory()
    target_date = '2000-01-01'
    jobtype_dev = 0
    jobtype_infra = 0
    jobtype_other = 0
    personneltype_dev = 0
    personneltype_infra = 0
    personneltype_other = 0
    other = 0

    class Meta:
        model = ContactStat

class ScheduledEmailStatsFactory(factory.DjangoModelFactory):
    company = CompanyFactory()
    target_date = '2000-01-01'
    job = 0
    personnel = 0
    other = 0

    class Meta:
        model = ScheduledEmailStat


class SharedEmailStatsFactory(factory.DjangoModelFactory):
    company = CompanyFactory()
    target_date = '2000-01-01'
    top_values = ''
    from_address = ''
    from_name = ''

    class Meta:
        model = SharedEmailStat


class StaffInChargeStatsFactory(factory.DjangoModelFactory):
    company = CompanyFactory()
    target_date = '2000-01-01'
    top_values = ''
    staff_in_charge = ''

    class Meta:
        model = StaffInChargeStat
