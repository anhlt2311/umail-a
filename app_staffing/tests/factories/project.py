from app_staffing.board.models import Project
from app_staffing.tests.factories.company import CompanyFactory
from faker import Factory
import factory


faker = Factory.create()


class ProjectFactory(factory.DjangoModelFactory):
    company = CompanyFactory()

    class Meta:
        model = Project
