from app_staffing.board.models import Personnel
from app_staffing.tests.factories.company import CompanyFactory
from faker import Factory
import factory


faker = Factory.create()


class PersonnelFactory(factory.DjangoModelFactory):
    company = CompanyFactory()

    request = faker.text()
    train_station = 'asdasd'
    last_name = 'daan'
    first_name = 'choi'
    birthday = faker.date()
    age = 20

    class Meta:
        model = Personnel
