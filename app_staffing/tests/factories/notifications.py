from factory import fuzzy
import factory

from app_staffing.models import EmailNotificationRule, EmailNotificationCondition
from app_staffing.models.notification import CONDITION_TYPES, TARGET_TYPES, EXTRACTION_TYPES

from app_staffing.tests.factories.company import CompanyFactory


class EmailNotificationRuleFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda x: 'Email Notification Rule: {}'.format(x))
    master_rule = 'all'
    is_active = True
    created_user = None
    modified_user = None
    company = CompanyFactory()

    class Meta:
        model = EmailNotificationRule


class EmailNotificationConditionFactory(factory.DjangoModelFactory):
    rule = factory.SubFactory(EmailNotificationRuleFactory)
    order = factory.Sequence(lambda x: x)
    extraction_type = fuzzy.FuzzyChoice(choices=[tpl[1] for tpl in EXTRACTION_TYPES])
    target_type = fuzzy.FuzzyChoice(choices=[tpl[1] for tpl in TARGET_TYPES])
    condition_type = fuzzy.FuzzyChoice(choices=[tpl[1] for tpl in CONDITION_TYPES])
    value = factory.Sequence(lambda x: '{0}'.format(x))

    class Meta:
        model = EmailNotificationCondition
