import factory
from app_staffing.models.addon import Addon

class AddonFactory(factory.DjangoModelFactory):
    company = None  # Please give a instance of company.
    addon_master_id = 1

    class Meta:
        model = Addon