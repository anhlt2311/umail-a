from app_staffing.board.models import ProjectCheckListItem, PersonnelCheckListItem
from app_staffing.tests.factories.company import CompanyFactory
from faker import Factory
import factory


faker = Factory.create()


class ProjectCheckListItemFactory(factory.DjangoModelFactory):
    company = CompanyFactory()
    content = faker.text()

    class Meta:
        model = ProjectCheckListItem


class PersonnelCheckListItemFactory(factory.DjangoModelFactory):
    company = CompanyFactory()
    content = faker.text()

    class Meta:
        model = PersonnelCheckListItem
