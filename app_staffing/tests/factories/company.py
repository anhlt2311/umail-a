import factory

from app_staffing.models.organization import Company
from app_staffing.tests.settings import DEFAULT_COMPANY_ID

import datetime

class CompanyFactory(factory.DjangoModelFactory):
    id = DEFAULT_COMPANY_ID  # Override if necessary.
    name = 'Sample Tenant'
    domain_name = 'sample.co.jp'
    address: '東京都渋谷区恵比寿'
    building: 'ほげビル'
    capital_man_yen = 2000
    establishment_date = datetime.date(2000, 1, 1)

    class Meta:
        model = Company
        django_get_or_create = ('id',)
