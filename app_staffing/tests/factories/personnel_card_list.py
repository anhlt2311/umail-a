from app_staffing.board.models import PersonnelCardList
from app_staffing.tests.factories.company import CompanyFactory
from faker import Factory
import factory


faker = Factory.create()


class PersonnelCardListFactory(factory.DjangoModelFactory):
    company = CompanyFactory()

    title = faker.text()

    class Meta:
        model = PersonnelCardList
