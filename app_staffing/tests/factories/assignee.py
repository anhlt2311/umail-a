from app_staffing.board.models import PersonnelAssignee, ProjectAssignee
from app_staffing.tests.factories.company import CompanyFactory
from faker import Factory
import factory


faker = Factory.create()


class PersonnelAssigneeFactory(factory.DjangoModelFactory):
    company = CompanyFactory()

    class Meta:
        model = PersonnelAssignee


class ProjectAssigneeFactory(factory.DjangoModelFactory):
    company = CompanyFactory()

    class Meta:
        model = ProjectAssignee
