"""A helper functions for mocking Payjp API calls in unittest ."""

import payjp

class MockingPayjpAPIHelper(object):
    """
    Paypy のAPIのmock関数を定義する。
    何もせずに終了するかmockオブジェクトを返す関数群のみを定義しているので
    異常系や設定値毎に異なる値を返す必要がある場合などはテストクラス側で実装すること。
    """
    
    def mock_ListObject_create(self, **params):
        return payjp.resource.Card.construct_from({
        'data': [{
            'id': 'mock_card_id',
            'last4': '4242',
        }],
        'count': 1,
    }, 'mock_api_key')

    def mock_ListObject_all(self, **params):
        return payjp.resource.Card.construct_from({
        'data': [{
            'id': 'mock_card_id',
            'last4': '4242',
        }],
        'count': 1,
    }, 'mock_api_key')

    def mock_ListObject_all_with_backup(self, **params):
        return payjp.resource.Card.construct_from({
        'data': [{
            'id': 'mock_card_id_main',
            'last4': '4242',
        },{
            'id': 'mock_card_id_backup',
            'last4': '5353',
        }],
        'count': 2,
        'default_card': 'mock_card_id_main',
    }, 'mock_api_key')

    def mock_ListObject_all_no_content(self, **params):
        return payjp.resource.Card.construct_from({
        'data': [],
        'count': 0,
    }, 'mock_api_key')

    def mock_ListObject_retrieve(self, id, **params):
        return payjp.resource.Card.construct_from({
        'data': [{
            'id': 'mock_card_id',
            'last4': '4242',
        }],
        'count': 1,
    }, 'mock_api_key')

    def mock_Card_save(self):
        return

    def mock_Card_delete(self, **params):
        return
    
    @classmethod
    def mock_Customer_create(cls, api_key=None, payjp_account=None, headers=None, **params):
        return payjp.Customer.construct_from({
            'id': params['id'],
            'cards': {
                'data': [
                    {
                        'id': 'mock_card_id',
                        'last4': '4242'
                    },
                ],
                'object': 'list',
            },
        }, 'mock_api_key')

    @classmethod
    def mock_Customer_retrieve(cls, id, api_key=None, payjp_account=None, api_base=None, **kwargs):
        return payjp.Customer.construct_from({
            'id': id,
            'cards': {
                'data': [
                    {
                        'id': 'mock_card_id',
                        'last4': '4242'
                    },
                ],
                'object': 'list',
            },
            'default_card': 'mock_card_id',
        }, 'mock_api_key')

    @classmethod
    def mock_Customer_retrieve_with_backup_card(cls, id, api_key=None, payjp_account=None, api_base=None, **kwargs):
        return payjp.Customer.construct_from({
            'id': id,
            'cards': {
                'data': [{
                    'id': 'mock_card_id_main',
                    'last4': '4242',
                },{
                    'id': 'mock_card_id_backup',
                    'last4': '5353',
                }],
                'object': 'list',
            },
            'default_card': 'mock_card_id_main',
        }, 'mock_api_key')

    @classmethod
    def mock_Customer_retrieve_no_cards(cls, id, api_key=None, payjp_account=None, api_base=None, **kwargs):
        return payjp.Customer.construct_from({
            'id': id,
            'cards': {
                'data': [],
                'object': 'list',
            },
        }, 'mock_api_key')

    def mock_Customer_delete(self, **params):
        return 

    def mock_Customer_save(self, **params):
        return 

    @classmethod
    def mock_Token_create(cls, api_key=None, payjp_account=None, headers=None, **params): 
        return payjp.Token.construct_from({
            "id": "mock_token_id",
        }, 'mock_api_key')

    @classmethod
    def mock_Charge_create(cls, api_key=None, payjp_account=None, headers=None, **params):
        return
