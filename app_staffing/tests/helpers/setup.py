"""A helper functions for unittest Setup."""
import os
import shutil

from django.core.management import call_command
from django.conf import settings

from django_mailbox.models import MessageAttachment
from app_staffing.tests.helpers.file import create_text_file

DJANGO_MAILBOXs_ATTACHMENT_ID = 1


def setup_estimators():
    """Call a batch train command"""

    args = []
    opts = {}
    call_command('train', *args, **opts)
    call_command('predict', *args, **opts)


def reset_estimators():
    """Call a batch train command"""

    args = []
    opts = {}
    call_command('reset', *args, **opts)


class MediaFolderSetupMixin(object):
    """A Mixin to setup test directories for email attachment test.
    WARNING: You need to make sure that MEDIA_ROOT is switched in a settings.py if the app runs in a unit test.
    """

    @classmethod
    def setUpClass(cls):  # This is essential for attachment test.
        super().setUpClass()
        os.makedirs(settings.MEDIA_ROOT, exist_ok=True)

    @classmethod
    def tearDownClass(cls):  # This is essential for attachment test.
        shutil.rmtree(path=settings.MEDIA_ROOT)
        super().tearDownClass()


class AttachmentPathMockingMixin(object):
    """ This mixin allows access an mocked email attachment object for the test via self.original_attachment.
    (The object's file parent path will automatically be replaced with the created test folder.)
    WARNING: A fixture of django-mailbox MessageAttachment that has an item with pk=1 is required for the test.
    """

    def setUp(self):  # This is essential for attachment test.
        file_name = 'testFile.txt'
        self.attachment_path = settings.MEDIA_ROOT + '/' + file_name
        create_text_file(path=self.attachment_path)

        # Override fixture's FileField's value (path) to created file.
        # TODO: Check if MEDIA_ROOT is defined in setting.py
        self.original_attachment = MessageAttachment.objects.get(pk=DJANGO_MAILBOXs_ATTACHMENT_ID)
        self.original_attachment.document = self.attachment_path  # Override for testing.
        self.original_attachment.save()
