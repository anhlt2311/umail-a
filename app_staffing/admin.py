from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from .forms.user_admin_forms import CustomUserCreationForm, CustomUserChangeForm
from app_staffing.admins import *

User = get_user_model()

# Extend default UserAdmin


class ExtendedUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {  # Adding custom attributes.
            'fields': (
                'is_active',
                'is_staff',
                'is_superuser',
                'groups',
                'user_permissions',),
        }),
        (_('cmrb'), {
            'fields': (
                'is_user_admin',
                'company',
                'tel1',
                'tel2',
                'tel3',
                'user_role',
                'user_service_id',
            ),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


admin.site.register(User, ExtendedUserAdmin)
