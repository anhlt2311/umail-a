#!/bin/bash
set -eu

# This is a deploy script for a Local environment.

# Definitions of constants.
## Exit codes
readonly PRECONDITION_FAILED=1
## File names
readonly ENV_FILE=".env"

# Definitions of functions.
function RaiseError () {
  code=$1
  echo "Exit with code ${code} . Deploy has Failed."
  exit ${code}
}

function checkFileExistence () {
  file=$1
  echo -n "Checking existence of ${file} ..."
  if ! [[ -f ${file} ]]; then
    echo "" # Line feed.
    echo "ERROR: Could not find a file ${file}"
    RaiseError ${PRECONDITION_FAILED}
  fi
  echo " => found."
}

# The Main Logic starts from here.
## Check preconditions of this script
checkFileExistence ${ENV_FILE}
checkFileExistence "./docker-compose.yml"

## Increase the number of app revision that is defined in .env file.
echo "Increasing the revision number which is defined in the .env file ... "
revision=$(grep "REVISION_NUMBER" ${ENV_FILE} | awk -F'=' '{ print $2}')
new_revision=$((revision + 1))
sed -i -e "s/REVISION_NUMBER=${revision}/REVISION_NUMBER=${new_revision}/g" ${ENV_FILE}
echo "Replacement success."

## Shutdown existing containers and Boot new containers
echo "Shutting down the existing containers ... "
docker-compose down --timeout 60
echo "Shutdown success."

echo "Booting new containers ..."
docker-compose up -d
echo "Boot success."

echo "Clearning up dangling images ... "
docker image prune -f
echo "Cleaned up the dangling images."

echo "SUCCESS: Deploy has finished, but please check the status of containers via docker-compose logs or docker ps command."
exit 0
