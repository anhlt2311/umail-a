# python manage.py collectstatic --noinput
rm -rf ./static/main.*.js
rm -rf ./static/main.*.js.gz

python manage.py collectstatic --noinput

SETTINGS_FILE=config.settings.base

if [ $# != 0 ]; then
    SETTINGS_FILE=$1
fi

python manage.py runserver --settings=$SETTINGS_FILE
