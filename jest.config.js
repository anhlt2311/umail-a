module.exports = {
    globals: {},
    moduleFileExtensions: ["ts", "tsx", "js", "jsx"],
    moduleDirectories: ["node_modules"],
    moduleNameMapper: {
        "^~/(.*)$": "<rootDir>/frontend/src/$1",
        "\\.(css|less|scss|sass)$": "identity-obj-proxy",
        "\\.(gif|ttf|eot|svg)$": "<rootDir>/__mocks__/fileMock.js",
        "react-pdf/dist/esm/entry.webpack": "react-pdf",
    },
    testEnvironment: "jsdom",
    transformIgnorePatterns: [
        "/node_modules/(?!antd|@ant-design|rc-.+?|@babel/runtime).+(js|jsx|less|css)$",
    ],
    setupFilesAfterEnv: ["<rootDir>/frontend/src/test/setupTests.js"],
    testTimeout: 20 * 1000,
};
